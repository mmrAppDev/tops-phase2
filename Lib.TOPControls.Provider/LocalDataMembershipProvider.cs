﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.Configuration;
using System.Web.Security;
using Lib.TOPControls.DataAccess;
using Lib.TOPControls.Interfaces.DataAccess;
using Lib.TOPControls.Security;


namespace Lib.TOPControls.Provider
{
    /// <summary>
    /// Provides MembershipProvider functions based on credentials stored in the database.
    /// </summary>
    public class LocalDataMembershipProvider:MembershipProvider
    {
        /// <summary>
        /// The Data Access Layer to use.
        /// </summary>
        protected ITOPControlDAL dal;

        /// <summary>
        /// The TOPControl Library to be used.
        /// </summary>
        protected ITOPControlLibrary lib;


        #region Constructors
        /// <summary>
        /// Creatnes an instance of this provider using a supplied ITOPControlDAL instance.
        /// </summary>
        /// <param name="useDAL">The DAL to use.</param>
        internal LocalDataMembershipProvider(ITOPControlDAL useDAL)
        {
            ApplicationName = "Lib.TOPControls";

            dal = useDAL;

            lib = new TOPControlLibrary(dal);
        }
        /// <summary>
        /// Creates an instance of this provider using the connection.
        /// </summary>
        /// <param name="conn">The connection to the database.</param>
        public LocalDataMembershipProvider(IDbConnection conn)
            : this(new TOPControlSQLDAL(new TOPControlDataContext(conn as DbConnection)))
        { }
        /// <summary>
        /// Creates and instance of this provider using the connection string.
        /// </summary>
        /// <param name="connectionString">A standard conneciton string.</param>
        public LocalDataMembershipProvider(string connectionString)
            : this(new SqlConnection(connectionString))
        { }

        /// <summary>
        /// Creates a default instance of this provider.  Will attempt to use a connection string from settings named TopControlDataContext.
        /// </summary>
        public LocalDataMembershipProvider()
            : this(new TOPControlSQLDAL(new TOPControlDataContext("name=TopControlDataContext")))
        { }
        
        
        #endregion

        /// <summary>
        /// The Application name
        /// </summary>
        public override string ApplicationName { get; set; }
        /// <summary>
        /// Changes a password for a specific username.
        /// </summary>
        /// <param name="username">The username with the changing password</param>
        /// <param name="oldPassword">The password currently in use.</param>
        /// <param name="newPassword">The new password for this username.</param>
        /// <returns></returns>
        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            var result = false;
            if (ValidateUser(username, oldPassword))
            {
                var repository = dal.GetRepository<Credential>();
                var userCred = repository.All.SingleOrDefault(c => c.Login == username);
                if (userCred != null)
                {
                    var encPW = EncryptPassword(newPassword);
                    userCred.Password = encPW;

                    repository.Save();
                    result = true;

                }

            }
            return result;
        }

        /// <summary>
        /// Encrypts the password.
        /// </summary>
        /// <param name="newPassword">The unencrypted password.</param>
        /// <returns></returns>
        private string EncryptPassword(string newPassword)
        {
            return SecurityHelper.EncryptString(newPassword);
        }

        /// <summary>
        /// Changes the Password question associated with the user credentials.
        /// </summary>
        /// <param name="username">User name of the user.</param>
        /// <param name="password">This users password for validation.</param>
        /// <param name="newPasswordQuestion">The new question to be used.</param>
        /// <param name="newPasswordAnswer">The new answer to be used.</param>
        /// <returns></returns>
        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            var result = false;
            if (ValidateUser(username, password))
            {
                var repository = dal.GetRepository<Credential>();
                var userCred = repository.All.SingleOrDefault(c => c.Login == username);
                if (userCred != null)
                {
                    userCred.PasswordQuestion = newPasswordQuestion;
                    userCred.PasswordAnswer = newPasswordAnswer;
                    try
                    {
                        repository.Save();
                        result = true;
                    }
                    catch
                    {
                        result = false;
                    }

                }

            }
            return result;
        }
        /// <summary>
        /// Creates a new user.
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">password</param>
        /// <param name="email">This is ignored.</param>
        /// <param name="passwordQuestion">Password question (string)</param>
        /// <param name="passwordAnswer">Password answer (string)</param>
        /// <param name="isApproved">A boolean.</param>
        /// <param name="providerUserKey"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            MembershipUser result = null;
            var credRepository = dal.GetRepository<Credential>();
            var userRepository = dal.GetRepository<TOPUser>();

            var dupCred = credRepository.All.Where(c => c.Login == username);
            var dupUser = userRepository.All.Where(tuser => tuser.Login == username);

            if (dupCred.Count() > 0 || dupUser.Count() > 0)
            {
                status = MembershipCreateStatus.DuplicateUserName;
            }
            else
            {

                var newTuser = lib.UserActions.AddWithCredentials(username, username, username, password, passwordQuestion, passwordAnswer, 1, isApproved);
                if (newTuser != null)
                {
                    var newCred = credRepository.All.SingleOrDefault(c => c.UserID == newTuser.ID);

                    result = ConvertToMembershipUser(newCred);
                    status = MembershipCreateStatus.Success;
                }
                else
                {
                    status = MembershipCreateStatus.ProviderError;
                }
            }
            return result;
        }
        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="deleteAllRelatedData"></param>
        /// <returns></returns>
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Always false.
        /// </summary>
        public override bool EnablePasswordReset
        {
            get { return false; }
        }

        /// <summary>
        /// Always false.
        /// </summary>
        public override bool EnablePasswordRetrieval
        {
            get { return false; }
        }

        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="emailToMatch"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            return new MembershipUserCollection();
        }
        /// <summary>
        /// Returns users matching a specific user name.
        /// </summary>
        /// <param name="usernameToMatch"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            var result = new MembershipUserCollection();

            var credRepository = dal.GetRepository<Credential>();

            var matches = credRepository.All.Where(c => c.Login == usernameToMatch);
            totalRecords = matches.Count();

            foreach(var tuser in matches)
            {
                result.Add(ConvertToMembershipUser(tuser));
            }

            return result;
        }
        /// <summary>
        /// All... users.
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            var result = new MembershipUserCollection();

            var credRepository = dal.GetRepository<Credential>();

            var matches = credRepository.All;
            totalRecords = matches.Count();

            foreach (var tuser in matches)
            {
                result.Add(ConvertToMembershipUser(tuser));
            }

            return result;
            
        }
        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <returns></returns>
        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="answer"></param>
        /// <returns></returns>
        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Returns the membership user matching the username.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userIsOnline"></param>
        /// <returns></returns>
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            MembershipUser result = null;

            var credRepository = dal.GetRepository<Credential>();

            var tuser = credRepository.All.SingleOrDefault(c => c.Login == username);

            if (tuser != null)
            {
                
                result = ConvertToMembershipUser(tuser);

            }

            return result;

        }
        /// <summary>
        /// Returnes the membership user matching the key.
        /// </summary>
        /// <param name="providerUserKey"></param>
        /// <param name="userIsOnline"></param>
        /// <returns></returns>
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            MembershipUser result = null;
            var userID = (long)providerUserKey;
            var credRepository = dal.CredentialRep;

            var tuser = credRepository.All.SingleOrDefault(c => c.UserID == userID);

            if (tuser != null)
            {

                result = ConvertToMembershipUser(tuser);

            }

            return result;
        }
        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns a number.  A very high, high number.
        /// </summary>
        public override int MaxInvalidPasswordAttempts
        {
            get { return 200; }
        }

        /// <summary>
        /// Returns zero
        /// </summary>
        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return 0; }
        }

        /// <summary>
        /// Gets minimum password length.
        /// </summary>
        public override int MinRequiredPasswordLength
        {
            get { return 2; }
        }

        /// <summary>
        /// Gets password attempt window.
        /// </summary>
        public override int PasswordAttemptWindow
        {
            get { return 15; }
        }
        /// <summary>
        /// Not implemented.
        /// </summary>
        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }
        /// <summary>
        /// Not implemented.
        /// </summary>
        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }
        /// <summary>
        /// Gets false.
        /// </summary>
        public override bool RequiresQuestionAndAnswer
        {
            get { return false; }
        }
        /// <summary>
        /// Gets false.
        /// </summary>
        public override bool RequiresUniqueEmail
        {
            get { return false; }
        }
        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="answer"></param>
        /// <returns></returns>
        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Removes the locked state on the user.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public override bool UnlockUser(string userName)
        {
            var result = false;
            var repCred = dal.CredentialRep;
            var item = repCred.All.SingleOrDefault(c => c.Login == userName && !c.TOPUser.IsDeleted);

            if (item != null && item.IsLockedOut)
            {
                item.IsLockedOut = false;
                repCred.Save();
                result = true;
            }
            return result;            
        }
        /// <summary>
        /// Updates a user based on the membership user passed.
        /// </summary>
        /// <param name="user"></param>
        public override void UpdateUser(MembershipUser user)
        {
            var repCred = dal.CredentialRep;
            var item = repCred.All.SingleOrDefault(c => c.Login == user.UserName);
            if (item != null)
            {
                
                item.CreationDate = user.CreationDate;

                item.IsApproved = user.IsApproved;
                item.IsLockedOut = user.IsLockedOut;
                
                
                
                item.LastLogin = user.LastLoginDate;
                item.LastChanged = user.LastPasswordChangedDate;
                item.PasswordQuestion = user.PasswordQuestion;
                repCred.Save();
            }
            
        }
        /// <summary>
        /// Performs validation on the user based on the credentials stored in the database.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public override bool ValidateUser(string username, string password)
        {
            var result = false;

            var credRepository = dal.CredentialRep;

            var encPassword = EncryptPassword(password);

            var tuser = credRepository.All.SingleOrDefault(c => c.Login == username && c.Password == encPassword && c.IsApproved && !c.IsLockedOut);
            if (tuser != null)
            {
                tuser.LastLogin = DateTime.Now;
                credRepository.Save();
                result = true;
            } 

            return result;
        }


        /// <summary>
        /// Returns the indicated user based on the credential value passed.
        /// </summary>
        /// <param name="tuser"></param>
        /// <returns></returns>
        public MembershipUser ConvertToMembershipUser(Credential tuser)
        {
            return new MembershipUser(ApplicationName, tuser.Login, tuser.UserID, "", tuser.PasswordQuestion, tuser.PasswordAnswer, tuser.IsApproved, tuser.IsLockedOut, tuser.CreationDate, tuser.LastLogin, tuser.LastLogin, tuser.LastChanged, tuser.CreationDate);
        }
    }
}
