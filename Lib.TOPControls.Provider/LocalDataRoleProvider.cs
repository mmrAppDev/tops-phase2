﻿using System;
using System.Collections.Generic;
using System.Web.Security;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using Lib.TOPControls.DataAccess;
using Lib.TOPControls.Interfaces.DataAccess;
using Lib.TOPControls.Security;

namespace Lib.TOPControls.Provider
{
    /// <summary>
    /// Retrieves user role information based on information stored in a database.
    /// </summary>
    public class LocalDataRoleProvider : RoleProvider
    {
        #region Properties

        private string _ApplicationName;
        /// <summary>
        /// The name of the application.
        /// </summary>
        public override string ApplicationName
        {
            get
            {
                return _ApplicationName;
            }
            set
            {
                _ApplicationName = value;
            }
        }

        private ITOPControlDAL _dal;
        /// <summary>
        /// Data Access Layer
        /// </summary>
        public ITOPControlDAL DAL
        {
            get { return _dal; }
            set { _dal = value; }
        }

        #endregion


        #region Constructors

        /// <summary>
        /// Creates the default role provider. Attempts to use a connection string from settings named TOPControlDataContext.
        /// </summary>
        public LocalDataRoleProvider()
        {
            
            _ApplicationName = "Lib.TOPControls";
            DAL = new TOPControlSQLDAL(new TOPControlDataContext("name=TOPControlDataContext"));

        }

        

        #endregion

        /// <summary>
        /// Returns an array of string values naming the roles of the user for the u ser name passed.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public override string[] GetRolesForUser(string username)
        {
            
            var repository = DAL.GetRepository<UserRole>();
            var result = (from ur in repository.All
                              where ur.TOPUser.Login == username
                              select ur.Role.RoleName);
            
            return result.ToArray();
        }

        /// <summary>
        /// Returns a boolean that is true if the user has the role with the name passed..
        /// </summary>
        /// <param name="username"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public override bool IsUserInRole(string username, string roleName)
        {
            var repository = DAL.GetRepository<UserRole>();

            return (repository.All.Count(ur => ur.TOPUser.Login == username && ur.Role.RoleName == roleName) > 0);
        }



        /// <summary>
        /// Adds a list of users to all of the roles that are passed.
        /// </summary>
        /// <param name="usernames"></param>
        /// <param name="roleNames"></param>
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            var repRoles = DAL.GetRepository<Role>();
            var repUR = DAL.GetRepository<UserRole>();
            var roleList = new List<Role>();

            foreach (var roleName in roleNames)
            {
                var item = repRoles.All.SingleOrDefault(r => r.RoleName == roleName);
                if (item != null) roleList.Add(item);
            }

            foreach (var userName in usernames)
            {
                var usr = DAL.TOPUserRep.All.SingleOrDefault(u => !u.IsDeleted && u.Login == userName);
                if (usr != null)
                {
                    foreach (var role in roleList)
                    {
                        var urole = repUR.All.SingleOrDefault(ur => ur.UserID == usr.ID && ur.RoleID == role.ID);
                        if (urole == null)
                        {
                            urole = new UserRole { UserID = usr.ID, RoleID = role.ID };
                            repUR.Insert(urole);
                        }

                    }
                }

            }
            repUR.Save();
        }

        /// <summary>
        /// Creates a new role with the name passed.
        /// </summary>
        /// <param name="roleName"></param>
        public override void CreateRole(string roleName)
        {
            var repRole = DAL.GetRepository<Role>();
            var role = repRole.All.SingleOrDefault(ur => ur.RoleName == roleName);
            if (role == null)
            {
                role = new Role { RoleName = roleName };
                repRole.Insert(role);
                repRole.Save();
            }

        }

        /// <summary>
        /// Removes a role from the database.
        /// </summary>
        /// <param name="roleName">Name of the role to remove.</param>
        /// <param name="throwOnPopulatedRole">If true, will throw an exception if there are users associated with that role.</param>
        /// <returns></returns>
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            var result = false;
            var repRole = DAL.GetRepository<Role>();

            var item = repRole.All.SingleOrDefault(r => r.RoleName == roleName);
            if (item != null && item.UserRoles != null && item.UserRoles.Count > 0)
            {
                if (throwOnPopulatedRole) throw new ArgumentException("Role is currently associated with one or more users.", "roleName");
            } 
            else if (item != null)
            {
                repRole.Delete(item.ID);
                repRole.Save();
                result = true;
            }
            
            return result;

        }

        /// <summary>
        /// Returns a list of usernames that both have the role name passed, and also are a partial match for the username string passed.
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="usernameToMatch"></param>
        /// <returns></returns>
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            var repUR = DAL.GetRepository<UserRole>();

            var users = from UserRole ur in repUR.All
                        where ur.Role.RoleName == roleName 
                        && (usernameToMatch == string.Empty || ur.TOPUser.Login.Contains(usernameToMatch))
                        select ur.TOPUser.Login;
            return users.ToArray();
        }

        /// <summary>
        /// Returns a list of all roles.
        /// </summary>
        /// <returns></returns>
        public override string[] GetAllRoles()
        {
            var repRole = DAL.GetRepository<Role>();
            var roles = from Role r in repRole.All
                        select r.RoleName;
            return roles.ToArray();
        }

        /// <summary>
        /// Returns a list of all usernames associated with a specific role.
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public override string[] GetUsersInRole(string roleName)
        {
            
            var repRole = DAL.GetRepository<Role>();
            var role = repRole.All.SingleOrDefault(r => r.RoleName == roleName);
            if (role != null)
            {
                var result = from UserRole ur in role.UserRoles
                             select ur.TOPUser.Login;
                return result.ToArray();
            }
            else
            {
                return new string[] {};
            }
        }

        /// <summary>
        /// Removes each user with a username in the list of usernames from all of roles in the list of roles.
        /// </summary>
        /// <param name="usernames"></param>
        /// <param name="roleNames"></param>
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            var repRoles = DAL.GetRepository<Role>();
            var repUR = DAL.GetRepository<UserRole>();

            foreach (var roleName in roleNames)
            {
                var item = repRoles.All.SingleOrDefault(r => r.RoleName == roleName);
                if (item != null)
                {
                    foreach (var userName in usernames)
                    {
                        var urole = item.UserRoles.SingleOrDefault(ur => ur.TOPUser.Login == userName);
                        if (urole != null)
                        {
                            repUR.Delete(urole.ID);
                        }
                    }                    
                }
            }
            repUR.Save();
        }

        /// <summary>
        /// Returns true if there is a role with the name passed, and false in all other cases.
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public override bool RoleExists(string roleName)
        {
            var roleCount = DAL.GetRepository<Role>().All.Count(r => r.RoleName == roleName);
            return (roleCount > 0);
        }
    }
}
