using Autofac;
using Envoc.Core.Shared.Security;
using Envoc.Core.Shared.Security.Validators;
using FluentValidation;

namespace MMR.TOPControls.Models.Configuration
{
    public class ModelsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(IValidator<>))
                .AsImplementedInterfaces();

            builder.RegisterType<UserValidator<User>>().AsSelf();
            builder.RegisterType<RoleValidator>().AsSelf();
        }
    }
}
