using System;
using AutoMapper;
using Envoc.Core.Shared.Extensions;

namespace MMR.TOPControls.Models.Configuration
{
    public static class AutoMapperConfiguration
    {
        public static void Configure(Action<IConfiguration> additionalConfiguration = null)
        {
            Mapper.Initialize(configuration =>
            {
                /* Model specific maps go here
                 * configuraiton.CreateMap<,>(); */

                if (additionalConfiguration.IsNotNull())
                {
                    additionalConfiguration.Invoke(configuration);
                }
            });
        }
    }
}
