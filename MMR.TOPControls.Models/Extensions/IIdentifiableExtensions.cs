using Envoc.Core.Shared.Model;

namespace MMR.TOPControls.Models.Extensions
{
    public static class IIdentifiableExtensions
    {
        public static bool IsNew(this IIdentifiable item)
        {
            return item.Id.Equals(0);
        }
    }
}
