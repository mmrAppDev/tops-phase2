using System.Linq;
using Envoc.Core.Shared.Data;

namespace MMR.TOPControls.Common.Extensions
{
    public static class IRepositoryExtensions
    {
        public static void DeleteAll<T>(this IRepository<T> repository) where T : class
        {
            var items = repository.Select().ToList();
            foreach (var topic in items)
            {
                repository.Delete(topic);
            }
        }
    }
}
