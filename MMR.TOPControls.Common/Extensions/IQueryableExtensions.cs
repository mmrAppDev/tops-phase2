using System;
using System.Linq;
using System.Linq.Expressions;

namespace MMR.TOPControls.Common.Extensions
{
    public static class IQueryableExtensions
    {
        public static IQueryOptions QueryOptions = new NullQueryOptions();

        public static IQueryable<T> Include<T, TProperty>(this IQueryable<T> source, Expression<Func<T, TProperty>> path)
             where T : class
        {
            return QueryOptions.Include(source, path);
        }

        public static IQueryable<T> AsNoTracking<T>(this IQueryable<T> source) where T : class
        {
            return QueryOptions.AsNoTracking(source);
        }

        public interface IQueryOptions
        {
            IQueryable<T> Include<T, TProperty>(IQueryable<T> source, Expression<Func<T, TProperty>> path) where T : class;
            IQueryable<T> AsNoTracking<T>(IQueryable<T> source) where T : class;
        }

        internal class NullQueryOptions : IQueryOptions
        {
            IQueryable<T> IQueryOptions.Include<T, TProperty>(IQueryable<T> source, Expression<Func<T, TProperty>> path)
            {
                return source;
            }

            IQueryable<T> IQueryOptions.AsNoTracking<T>(IQueryable<T> source)
            {
                return source;
            }
        }
    }
}
