using System;

namespace MMR.TOPControls.Common.Extensions
{
    public static class DateTimeExtentions
    {
        public static long ToUnixTime(this DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date - epoch).TotalMilliseconds);
        }

        public static DateTime ToDayStart(this DateTime date)
        {
            return date.Date;
        }

        public static DateTime ToDayEnd(this DateTime date)
        {
            if (date.Equals(DateTime.MaxValue))
            {
                return DateTime.MaxValue;
            }
            return date.Date.AddDays(1).AddSeconds(-1);
        }

        public static string ToShortDateTimeString(this DateTime date)
        {
            return String.Format("{0:g}", date);
        }
    }
}
