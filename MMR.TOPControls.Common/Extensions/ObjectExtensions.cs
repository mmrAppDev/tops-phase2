using System;
using Envoc.Core.Shared.Extensions;

namespace MMR.TOPControls.Common.Extensions
{
    public static class ObjectExtensions
    {
        public static void CheckForNull(this object obj, string paramName)
        {
            if (obj.IsNull())
            {
                throw new ArgumentNullException(paramName);
            }
        }
    }
}
