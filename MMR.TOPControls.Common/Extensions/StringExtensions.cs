using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace MMR.TOPControls.Common.Extensions
{
    public static class StringExtensions
    {
        public static string ToSeparatedWords(this string value)
        {
            if (value != null)
                return Regex.Replace(value, "([A-Z][a-z]?)", " $1").Trim();
            return value;
        }

        public static string[] CommaSplit(this string original)
        {
            if (String.IsNullOrEmpty(original))
            {
                return new string[0];
            }

            var split = from piece in original.Split(',')
                        let trimmed = piece.Trim()
                        where !String.IsNullOrEmpty(trimmed)
                        select trimmed;
            return split.ToArray();
        }

        public static string Slugify(this string value)
        {
            string str = value.RemoveAccent().ToLower();
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            str = Regex.Replace(str, @"\s+", " ").Trim();
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-");
            return str;
        }

        public static string RemoveAccent(this string value)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(value);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }
    }
}
