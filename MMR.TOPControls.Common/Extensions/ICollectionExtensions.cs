using System;
using System.Collections.Generic;
using System.Linq;

namespace MMR.TOPControls.Common.Extensions
{
    public static class ICollectionExtensions
    {
        public static void UpdateFrom<T>(this ICollection<T> destination, ICollection<T> source, Action<T, T> map)
        {
            var dest = destination.ToList();
            var src = source.ToList();

            src.Skip(dest.Count).ForEach(destination.Add);

            int countToUpdate = Math.Min(dest.Count, src.Count);
            for (int i = 0; i < countToUpdate; i++)
            {
                var updated = src[i];
                var existing = dest[i];
                map.Invoke(updated, existing);
            }

            dest.Skip(src.Count).ForEach(x => destination.Remove(x));
        }
    }
}
