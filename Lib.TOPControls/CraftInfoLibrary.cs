﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMRCommon.Data;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Interfaces.DataAccess;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;

namespace Lib.TOPControls
{
    /// <summary>
    /// A library class that performs operations specific to the CraftInfo class and its instances.
    /// </summary>
    public class CraftInfoLibrary : JobObjectLibraryBase<CraftInfo>, ICraftInfoLibrary
    {

        #region Constructors

        /// <summary>
        /// Creates an instance of CraftInfoLibrary with default values.
        /// </summary>
        public CraftInfoLibrary()
            : base()
        { }
        /// <summary>
        /// Creates an instance of CraftInfoLibrary, setting the Root library and pulling all other resources from it.
        /// </summary>
        /// <param name="rootLibrary">An object that implements ITOPControlLibrary.</param>
        public CraftInfoLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }
        /// <summary>
        /// Creates an instance of CraftInfoLibrary with the resource values passed.
        /// </summary>
        /// <param name="useCraftInfoRepository">The IRepository to use.</param>
        /// <param name="permissionManager">The IPermissionManager to use.</param>
        public CraftInfoLibrary(IRepository<CraftInfo> useCraftInfoRepository = null, IPermissionManager permissionManager = null)
            : base(useCraftInfoRepository, permissionManager)
        { }

        #endregion

        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public override ValidationResult Validate(CraftInfo item)
        {
            var result = base.Validate(item);

            if (item != null)
            {
                result.ValidateStringLength(item.CraftCode, 1, 200, "CraftCode");
                var dups = Repository.All.Count(ci => ci.ID != item.ID && ci.JobID == item.JobID && ci.CraftCode == item.CraftCode);
                if (dups > 0)
                {
                    result.AddViolation("There is already a craft with the craft code \"" + item.CraftCode + "\" in the job.", "CraftCode");
                }
                result.ValidateStringLength(item.Description, 0, 200, "Description");
            }

            return result;
        }

       


        #region ICraftInfoLibrary Members

        /// <summary>
        /// Returns a set of CraftInfo objects within the Job indicated by jobID that match the 
        /// searchTerm passed.  Does not return Crafts from deleted jobs.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">A string to search for.  If the searchTerm is an empty string, 
        /// returns all CraftInfo records within the job.</param>
        /// <returns></returns>
        public IEnumerable<CraftInfo> Search(long jobID, string searchTerm)
        {
            return Repository.All.Where(ci => ci.JobID == jobID
                                    && (searchTerm == ""
                                        || ci.CraftCode == searchTerm
                                        || ci.Description.Contains(searchTerm))).ToList();
        }

        #endregion

        /// <summary>
        /// Adds a condition to the IQueryable passed that filters items down to records that have CraftInfo ID values
        /// matching the list of values passed.
        /// </summary>
        /// <param name="filteredList">A reference to an IQueryable of TaskProgress to be filtered.</param>
        /// <param name="values">A set of CraftInfo ID values to use to filter records.</param>
        public static void AttachTaskProgressFilter(ref IQueryable<TaskProgress> filteredList, IEnumerable<long> values)
        {
            if (values != null && values.Count() > 0)
            {
                filteredList = filteredList.Where(tp => tp.TrackedItem != null
                    && tp.TrackedItem.CraftID.HasValue
                    && values.Contains(tp.TrackedItem.CraftID.Value));
            }
        }

        /// <summary>
        /// Copies values specific to CraftInfo class.
        /// </summary>
        /// <param name="from">Object to copy from.</param>
        /// <param name="to">Object to copy to.</param>
        protected override void copyClassValues(CraftInfo from, CraftInfo to)
        {
            to.JobID = from.JobID;
            to.CraftCode = from.CraftCode;
            to.Description = from.Description;
        }


        /// <summary>
        /// Returns an instance of the target type if there is a valid item with a primary descriptive string matching
        /// the name passed, unless the item is marked deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job the object should belong to.</param>
        /// <param name="name">The full name string to search for.</param>
        /// <returns></returns>
        public override CraftInfo FindByName(long jobID, string name)
        {
            return Repository.All.FirstOrDefault(ci => ci.JobID == jobID && ci.CraftCode == name);
        }

        /// <summary>
        /// Returns a ValidationResult for the delete operation on the CraftInfo Item.
        /// </summary>
        /// <param name="id">ID of the item to delete.</param>
        /// <returns></returns>
        public override ValidationResult ValidateDelete(long id)
        {
            var result = base.ValidateDelete(id);
            var matches = Root.GetQueryable<TrackedItem>().Count(x => x.CraftID.HasValue && x.CraftID.Value == id);
            if (matches > 0)
                result.AddViolation(string.Format("There are {0} Tracked Items that are using this Craft.", matches));
            return result;
        }



    }
}
