﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMRCommon.Data;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Interfaces.DataAccess;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides management functiosn for ProjectPhase
    /// </summary>
    public class ProjectPhaseLibrary:JobObjectLibraryBase<ProjectPhase>, IProjectPhaseLibrary
    {
        /// <summary>
        /// Repository to use.
        /// </summary>
        public override IRepository<ProjectPhase> Repository
        {
            get
            {
                if (base.Repository == null)
                {
                    if (Root != null)
                    {
                        base.Repository = Root.GetRepository<ProjectPhase>();
                    }
                }
                return base.Repository;
            }
            set
            {
                base.Repository = value;
            }
        }

        #region Constructors

        /// <summary>
        /// Creates a ProjectPhaseLibrary
        /// </summary>
        public ProjectPhaseLibrary()
            : base()
        { }
        /// <summary>
        /// Creates a ProjectPhaseLibrary based on the root library passed.
        /// </summary>
        /// <param name="rootLibrary">Object implementing ITOPControlLibrary</param>
        public ProjectPhaseLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }
        /// <summary>
        /// Creates a ProjectPhaseLibrary with the resources passed.
        /// </summary>
        /// <param name="projectPhaseRepository">Repository to use.</param>
        /// <param name="permissionManager">IPermissionManager to use.</param>
        public ProjectPhaseLibrary(IRepository<ProjectPhase> projectPhaseRepository = null, IPermissionManager permissionManager = null)
            : base(projectPhaseRepository, permissionManager)
        { }

        #endregion

        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public override ValidationResult Validate(ProjectPhase item)
        {
            var result = base.Validate(item);

            if (item.ID != 0 && item.DisplayOrder < 1)
            {
                result.AddViolation("Cannot assign a display order value that is negative or zero.", "DisplayOrder");
            }
            
            if (string.IsNullOrWhiteSpace(item.Title))
            {
                result.AddViolation("The title is empty and must be set to continue.", "Title");
            }
            else if (item.Title.Trim().Length > 50)
            {
                result.AddViolation("The title is too long.  The phase title must be no longer than 50 characters.", "Title");
            }
            else
            {
                // Having establshed that the title is a valid type of value, determine if there are others with the same Title within the job.
                var dups = Repository.All.Where(pp => pp.ID != item.ID && pp.JobID == item.JobID && pp.Title == item.Title);
                if (dups.Count() > 0)
                {
                    result.AddViolation("There is already a Phase in the current job with that title.", "Title");
                }
            }
            

            return result;
        }

        /// <summary>
        /// Adds an item to the database.
        /// </summary>
        /// <param name="newItem">The ProjectPhase to add.</param>
        /// <param name="userID">The UserID performing the operation.</param>
        /// <returns></returns>
        public override ProjectPhase Add(ProjectPhase newItem, long userID)
        {
            var result = base.Add(newItem, userID);
            AddDefaultTaskToPhase(result, userID);
            return result;
        }

        /// <summary>
        /// Creates and stores the set of default Task items for a new phase.
        /// </summary>
        /// <param name="newItem">The project phase to which the new Task will be added.</param>
        /// <param name="userID">The ID of the user that added the phase.</param>
        private void AddDefaultTaskToPhase(ProjectPhase newItem, long userID)
        {
            if (Root != null)
            {
                var task1 = new Task { PhaseID = newItem.ID, JobID = newItem.JobID, Title = "Default Task", DisplayOrder = 1 };
                Root.TaskActions.Add(task1, userID);

            }
        }

        /// <summary>
        /// Removes a Project Phase from the database, when there are no TaskProgress items that have progress entered against them.
        /// </summary>
        /// <param name="id">ID of the ProjectPhase item to remove.</param>
        /// <param name="userID">ID of the TOPUser performing this operation.</param>
        public override void Delete(long id, long userID)
        {
            var item = Find(id);
            if (item != null && item.Tasks != null && item.Tasks.Count >= 0)
            {
                var tpCount = Root.GetRepository<TaskProgress>().All.Count(tp => !tp.IsDeleted && !tp.Task.IsDeleted && tp.Task.PhaseID == id);
                if (tpCount > 0)
                {
                    throw new ArgumentException("Cannot delete the selected ProjectPhase because there are Tasks with related TaskProgress items.", "id");
                }
                else
                {
                    foreach (var t in item.Tasks)
                    {
                        Root.TaskActions.Delete(t.ID, userID);
                    }
                }
            }
            base.Delete(id, userID);
        }

        #region IProjectPhaseLibrary Members

        /// <summary>
        /// Returns a set of ProjectPhase objects. Modifies the DisplayOrder property of the project 
        /// phase identified so that it is one item closer to the end of the list, also modifying the 
        /// DisplayOrder property of the phase that occupies its new position.  No values are changed 
        /// if the phase is already at the bottom of the list. 
        /// </summary>
        /// <param name="projectPhaseID">The ID value of the ProjectPhase item being moved down.</param>
        /// <param name="userID">The ID of the TOPUser performing this operation. This is used to both 
        /// log the action and validate the user’s right to perform the operation.</param>
        /// <returns></returns>
        public IEnumerable<ProjectPhase> MoveDownInDisplayOrder(long projectPhaseID, long userID)
        {
            var target = Find(projectPhaseID);
            if (target == null) throw new ArgumentException("Could not find a phase matching the ID passed.");
            if (VerifyUserPermission(UserActionType.Update, target, userID))
            {
                var phaseList = Search(target.JobID, string.Empty).OrderBy(pp => pp.DisplayOrder);
                var nextItem = phaseList.Where(pp => pp.DisplayOrder > target.DisplayOrder).FirstOrDefault();
                if(nextItem != null)
                {
                    var ord = nextItem.DisplayOrder;
                    nextItem.DisplayOrder = target.DisplayOrder;
                    target.DisplayOrder = ord;
                    base.SetDomainAutomaticValues(target);
                    base.SetDomainAutomaticValues(nextItem);
                    
                    var validNext = Validate(nextItem);
                    var validTarg = Validate(target);

                    if (validNext.IsValid && validTarg.IsValid)
                    {
                        Repository.Save();
                    }
                }

                
            }
            return Search(target.JobID, string.Empty);
        }

        /// <summary>
        /// Returns a set of ProjectPhase objects. Modifies the DisplayOrder property of the project 
        /// phase identified so that it is one item closer to the start of the list, also modifying 
        /// the DisplayOrder property of the phase that occupies its new position.  No values are changed 
        /// if the phase is already at the top of the list.
        /// </summary>
        /// <param name="projectPhaseID">The ID value of the ProjectPhase item being moved up.</param>
        /// <param name="userID">The ID of the TOPUser performing this operation. This is used to both 
        /// log the action and validate the user’s right to perform the operation.</param>
        /// <returns></returns>
        public IEnumerable<ProjectPhase> MoveUpInDisplayOrder(long projectPhaseID, long userID)
        {
            var target = Find(projectPhaseID);
            if (target == null) throw new ArgumentException("Could not find a phase matching the ID passed.");
            if (VerifyUserPermission(UserActionType.Update, target, userID))
            {
                var phaseList = Search(target.JobID, string.Empty).OrderBy(pp => pp.DisplayOrder);
                var nextItem = phaseList.Where(pp => pp.DisplayOrder < target.DisplayOrder).LastOrDefault();
                if (nextItem != null)
                {
                    var ord = nextItem.DisplayOrder;
                    nextItem.DisplayOrder = target.DisplayOrder;
                    target.DisplayOrder = ord;
                    base.SetDomainAutomaticValues(target);
                    base.SetDomainAutomaticValues(nextItem);

                    var validNext = Validate(nextItem);
                    var validTarg = Validate(target);

                    if (validNext.IsValid && validTarg.IsValid)
                    {
                        Repository.Save();
                    }
                }
            }
            return Search(target.JobID, string.Empty);
        }

        /// <summary>
        /// Returns a set of ProjectPhase objects within the Job indicated by jobID that match 
        /// the searchTerm passed.    Does not return Phases from deleted jobs.
        /// </summary>
        /// <param name="jobID">The ID of the job to search</param>
        /// <param name="searchTerm">The string value to search for.  If the searchTerm is an 
        /// empty string, returns all ProjectPhase records.</param>
        /// <returns></returns>
        public IEnumerable<ProjectPhase> Search(long jobID, string searchTerm)
        {
            var job = Root.GetLibrary<JobInfo>().Find(jobID);

            if (job == null) return new List<ProjectPhase>();

            return SearchAnyQueryable(jobID, searchTerm).ToList();
            
        }

        /// <summary>
        /// Returns a set of ProjectPhase objects, within the Job indicated by jobID (even if 
        /// the job is deleted) that match the searchTerm passed.  
        /// </summary>
        /// <param name="jobID">The ID of the job to search</param>
        /// <param name="searchTerm">The string value to search for.  If the searchTerm is an 
        /// empty string, returns all ProjectPhase records.</param>
        /// <param name="userID">The ID of the TOPUser performing the search. Will fail if the 
        /// referenced user does not have rights to view deleted job info.</param>
        /// <returns></returns>
        public IEnumerable<ProjectPhase> SearchAny(long jobID, string searchTerm, long userID)
        {
            VerifyUserPermission(UserActionType.ViewDeleted, new JobInfo(), userID);
            return SearchAnyQueryable(jobID, searchTerm);
        }


        /// <summary>
        /// Returns a set of ProjectPhase objects within the Job indicated by jobID that match 
        /// the searchTerm passed.  Ignores deletion state of jobs.
        /// </summary>
        /// <param name="jobID">The ID of the job to search</param>
        /// <param name="searchTerm">The string value to search for.  If the searchTerm is an 
        /// empty string, returns all ProjectPhase records.</param>
        /// <returns></returns>
        protected IQueryable<ProjectPhase> SearchAnyQueryable(long jobID, string searchTerm)
        {
            return Repository.All.Where(pp => pp.JobID == jobID
                                && (searchTerm == ""
                                    || pp.Title.Contains(searchTerm))).OrderBy(pp => pp.DisplayOrder);

        }




        #endregion


        /// <summary>
        /// Returns an instance of the target type if there is a valid item with a primary descriptive string matching
        /// the name passed, unless the item is marked deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job the object should belong to.</param>
        /// <param name="name">The full name string to search for.</param>
        /// <returns></returns>
        public override ProjectPhase FindByName(long jobID, string name)
        {
            return Repository.All.FirstOrDefault(pp => pp.JobID == jobID && pp.Title == name);
        }

        /// <summary>
        /// Adds a condition to the IQueryable passed that filters items down to records that have Phase ID values
        /// matching the list of values passed.
        /// </summary>
        /// <param name="filteredList">A reference to an IQueryable of TaskProgress to be filtered.</param>
        /// <param name="values">A set of Phase ID values to use to filter records.</param>
        public static void AttachTaskProgressFilter(ref IQueryable<TaskProgress> filteredList, IEnumerable<long> values)
        {
            if (values != null && values.Count() > 0)
            {
                filteredList = filteredList.Where(tp => tp.Task != null
                    && values.Contains(tp.Task.PhaseID));
            }
        }

        /// <summary>
        /// Copies values specific to the Phase.
        /// </summary>
        /// <param name="from">Item to copy from.</param>
        /// <param name="to">Item to copy to.</param>
        protected override void copyClassValues(ProjectPhase from, ProjectPhase to)
        {

            to.DisplayOrder = from.DisplayOrder;
            to.JobID = from.JobID;
            to.Title = from.Title;
           
        }


        /// <summary>
        /// Sets the values that are determined automatically by the domain, as well as handling the relative values of DisplayOrder values.
        /// </summary>
        /// <param name="item"></param>
        protected override void SetDomainAutomaticValues(ProjectPhase item)
        {
            base.SetDomainAutomaticValues(item);
            if (item.ID == 0)
            {
                var fullList = RenumberItems(item.JobID);
                item.DisplayOrder = fullList.Count() + 1;
            }
            else
            {
                var discrim = GetSiblings(item).Count(ob => ob.DisplayOrder == item.DisplayOrder);
                if (discrim > 0)
                {

                    var fullList = GetSiblings(item).Where(ob => ob.DisplayOrder >= item.DisplayOrder).ToList();

                    foreach (var sib in fullList)
                    {
                        sib.DisplayOrder = sib.DisplayOrder + 1;
                        sib.LastChanged = DateTime.Now;
                    }

                }
                RenumberItems(item.JobID);
            }
            
        }

        /// <summary>
        /// Returns a set of items of this type that are all children of the item with the parentID passed.
        /// </summary>
        /// <param name="parentID">The ID of the item that the targeted IDisplayOrdered items belong to.</param>
        /// <returns></returns>
        public IQueryable<ProjectPhase> GetSiblings(long parentID)
        {
            return Repository.All.Where(t => t.JobID == parentID);
        }

        /// <summary>
        /// Returns all items of this type that are in the same list of IDisplayOrdered as the item passed.
        /// </summary>
        /// <param name="item">The source item for which the siblings are being returned.</param>
        /// <param name="excludeOriginal">A boolean that if true causes the items to be returned without the 
        /// instance of the item originally passed.</param>
        /// <returns></returns>
        public IQueryable<ProjectPhase> GetSiblings(ProjectPhase item, bool excludeOriginal = true)
        {
            return GetSiblings(item.JobID).Where(t => (!excludeOriginal || t.ID != item.ID));
        }


        /// <summary>
        /// Modifies the DisplayOrder property of all items of this type within the Job indicated by JobID
        /// </summary>
        /// <param name="jobID">An ID representing the Job to which this item belongs.</param>
        /// <param name="commitValues">A boolean that when passed as true will trigger a Save operation to commit the altered 
        /// DisplayOrder values to the permanent data.  Otherwise, the changes are left to be included in the Unit of Work that 
        /// commits next.</param>
        /// <returns></returns>
        public virtual IEnumerable<ProjectPhase> RenumberItems(long jobID, bool commitValues = false)
        {
            var allSibs = GetSiblings(jobID).ToList().OrderBy(x => x.DisplayOrder).ThenBy(y => y.ID);
            
            
            for (int cnt = 0; cnt < allSibs.Count(); cnt++)
            {
                if (allSibs.ElementAt(cnt).DisplayOrder != cnt + 1)
                {
                    var thisitem = allSibs.ElementAt(cnt);
                    thisitem.DisplayOrder = cnt + 1;
                    thisitem.LastChanged = DateTime.Now;
                }
            }

            if (commitValues)
            {
                Repository.Save();
            }

            return allSibs;
        }

        /// <summary>
        /// Validates deletion of a ProjectPhase
        /// </summary>
        /// <param name="id">ID of the item to be deleted</param>
        /// <returns></returns>
        public override ValidationResult ValidateDelete(long id)
        {
            var result = base.ValidateDelete(id);
            var tpCount = Root.GetRepository<TaskProgress>().All.Count(tp => !tp.IsDeleted && !tp.Task.IsDeleted && tp.Task.PhaseID == id);
            if (tpCount > 0)
            {
                result.AddViolation(String.Format("There are {0} Tracked Items in Tasks that belong to this phase.", tpCount));
            }
            return result;
        }

    }
}
