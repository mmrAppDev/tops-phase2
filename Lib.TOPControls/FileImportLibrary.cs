﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.DataImport;
using System.Xml;

namespace Lib.TOPControls
{
    /// <summary>
    /// A library class that performs operations specific to the FileImport class and its instances.
    /// </summary>
    public class FileImportLibrary : JobObjectLibraryBase<FileImport>, IFileImportLibrary
    {
        #region Constructors
        /// <summary>
        /// Creates default version.
        /// </summary>
        public FileImportLibrary()
            : base()
        { }
        /// <summary>
        /// Creates a version based on a supplied root library.
        /// </summary>
        /// <param name="rootLibrary"></param>
        public FileImportLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }
        /// <summary>
        /// Creates a version using specific resources.
        /// </summary>
        /// <param name="repository">Repository to use</param>
        /// <param name="permissionManager">Permission Manager to use.</param>
        public FileImportLibrary(IRepository<FileImport> repository, IPermissionManager permissionManager = null)
            : base(repository, permissionManager)
        { }

        #endregion

        /// <summary>
        /// Validates the object passed for saving.
        /// </summary>
        /// <param name="item">An item to check for validity.</param>
        /// <returns></returns>
        public override ValidationResult Validate(FileImport item)
        {
            var result = base.Validate(item);

            

            if (Root.UserActions.Find(item.UserID) == null)
                result.AddViolation("Could not find the user in the system.", "UserID");
            if (item.DateSubmitted > DateTime.Now || item.DateSubmitted == default(DateTime))
                result.AddViolation("The date submitted value is set to an invalid value.", "DateSubmitted");
            if (string.IsNullOrWhiteSpace(item.Filename))
                result.AddViolation("Cannot save a blank file name.", "Filename");
            else result.ValidateStringLength(item.Filename, 5, 250, "Filename");

            if (string.IsNullOrWhiteSpace(item.Filepath))
                result.AddViolation("Cannot save a blank file path.", "Filepath");
            else result.ValidateStringLength(item.Filepath, 5, 500, "Filepath");
            if (!IsValidFileType(item.FileType))
                result.AddViolation(string.Format("The FileType value \"{0}\" is invalid.", item.FileType), "FileType");
            if (string.IsNullOrWhiteSpace(item.RecordsetName))
                result.AddViolation("Cannot save the FileImport without a Recordset name.", "RecordsetName");
            else result.ValidateStringLength(item.RecordsetName, 1, 100, "RecordsetName");
            if (!string.IsNullOrWhiteSpace(item.RecordsetXml))
            {
                try
                {
                    var xtest = item.RecordsetXmlDocument;
                }
                catch
                {
                    result.AddViolation("The recordset is not properly translated to Xml format.", "RecordsetXml");
                }
            }
            
            return result;
        }

        /// <summary>
        /// Returns true if the file type being passed is one of the accepted values for the FileType property.
        /// </summary>
        /// <param name="fileType">A string representing whether the file is an Excel or a CSV file.</param>
        /// <returns></returns>
        public bool IsValidFileType(string fileType)
        {
            var temp = fileType.ToLower();
            return (temp == "excel" || temp == "csv" || temp == "unknown");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        protected override void copyClassValues(FileImport from, FileImport to)
        {
            to.DateSubmitted = from.DateSubmitted;
            to.JobID = from.JobID;
            to.UserID = from.UserID;
            to.Filepath = from.Filepath;
            to.Filename = from.Filename;
            to.FileType = from.FileType;
            to.RecordsetName = from.RecordsetName;
            to.RecordsetXml = from.RecordsetXml;
            to.LastChanged = from.LastChanged;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobID"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public override FileImport FindByName(long jobID, string name)
        {
            return Repository.All.FirstOrDefault(fi => fi.JobID == jobID && fi.ID.ToString() == name);
        }
        
    }
}
