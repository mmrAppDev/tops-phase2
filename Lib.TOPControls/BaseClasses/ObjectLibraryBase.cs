﻿using System;
using Lib.TOPControls.Exceptions;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls.BaseClasses
{
    /// <summary>
    /// Implements basic operations for librarys that implement the IObjectLibrary interface.
    /// </summary>
    /// <typeparam name="T">A type that is a subclass of DomainObject, has a parameterless constructor.</typeparam>
    public abstract class ObjectLibraryBase<T> : IObjectLibrary<T> where T : DomainObject, new()
    {
        /// <summary>
        /// The root library for this instance.  Outside resources, such as related repositories
        /// and missing properties might be pulled from this Root library for certain operations.
        /// </summary>
        public virtual ITOPControlLibrary Root { get; set; }

        /// <summary>
        /// The repository this library will use to perform the basic operations on the target class.
        /// </summary>
        public virtual IRepository<T> Repository { get; set; }

        /// <summary>
        /// The IPermissionManager to use when verifying that a user can perform a specific action.
        /// </summary>
        public virtual IPermissionManager PermManager { get; set; }

        #region Constructors

        /// <summary>
        /// Performs default setup of ObjectLibraryBase properties.
        /// </summary>
        public ObjectLibraryBase()
        {
            Repository = null;
        }

        /// <summary>
        /// Performs setup of ObjectLibraryBase using the provided environment values.
        /// </summary>
        /// <param name="useRepository">An object that implements IRepository for items of this type.</param>
        /// <param name="permissionManager">An object that implements IPermissionManager.</param>
        public ObjectLibraryBase(IRepository<T> useRepository, IPermissionManager permissionManager)
        {
            Repository = useRepository;
            PermManager = permissionManager;
        }

        /// <summary>
        /// Performs setup of ObjectLibraryBase with a reference to an ITOPControlLibrary instance,
        /// and by default using its resources.
        /// </summary>
        /// <param name="rootLibrary">An object that implements the ITOPControlLibrary interface</param>
        public ObjectLibraryBase(ITOPControlLibrary rootLibrary)
        {
            Root = rootLibrary;
            Repository = Root.GetRepository<T>();
            PermManager = Root.PermissionManager;
        }

        #endregion Constructors

        #region IObjectLibrary<T> Members

        /// <summary>
        /// Returns an instance of target type if there is a valid item with an ID value matching the value passed.  If
        /// the matching item is marked as deleted, or if there is no item with a matching ID, retuns NULL.
        /// </summary>
        /// <param name="id">The ID value to find.</param>
        /// <returns></returns>
        public virtual T Find(long id)
        {
            if (id <= 0)
            {
                throw new ArgumentException("ID outside of logical range.  This typically happens when an operation is performed on an item with an ID that is negative or zero.", "id");
            }

            return Repository.Find(id);
        }

        /// <summary>
        /// Returns an instance of the target type. Creates a new record for an item of the specified type, if that item
        /// passes validation and the user indicated by the User ID has the right to create an item of this type.
        /// </summary>
        /// <param name="newItem">An instance of the type to be added.</param>
        /// <param name="userID">The ID of the TOPUser performing this operation, to verify permissions and perform activity
        /// logging.  Must match a valid user record.</param>
        /// <returns></returns>
        public virtual T Add(T newItem, long userID)
        {
            if (newItem == null) throw new ArgumentNullException("newItem", "Cannot insert a NULL item.");

            throwOnInvalidUserIDRange(userID);

            VerifyUserPermission(UserActionType.Insert, newItem, userID);

            var valResult = Validate(newItem);
            if (!valResult.IsValid) throw new ValidationFailureException("New Item failed validation.", valResult);

            if (newItem.ID == 0)
            {
                SetDomainAutomaticValues(newItem);
                Repository.Insert(newItem);
            }
            Repository.Save();

            return newItem;
        }

        /// <summary>
        /// Updates the record of this type that matches the item's ID with the rest of the item properties.
        /// </summary>
        /// <param name="item">An item to update in the database. This must not be NULL.</param>
        /// <param name="userID">The ID of the TOPUser performing this operation, for both logging and security purposes.
        /// Must match a valid user record.</param>
        /// <returns></returns>
        public virtual T Update(T item, long userID)
        {
            if (item == null) throw new ArgumentNullException("item", "Cannot update a NULL item.");

            if (item.ID == 0) throw new ArgumentException("Item to update does not have a valid database identifier. This item may need to be added instead.", "item");

            var tempItem = Find(item.ID);
            if (tempItem == null) throw new ArgumentException("The item to update has an ID value that does not exist in the database.", "item");

            throwOnInvalidUserIDRange(userID);

            VerifyUserPermission(UserActionType.Update, tempItem, userID);

            var valResult = Validate(item);
            if (!valResult.IsValid) throw new ValidationFailureException("Update item failed validation.", valResult);

            if (!(item == tempItem))
            {
                copyValues(item, tempItem);
            }

            SetDomainAutomaticValues(tempItem);

            Repository.Save();

            return item;
        }

        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public virtual ValidationResult Validate(T item)
        {
            var result = new ValidationResult();

            if (item == null) throw new ArgumentNullException("item");

            if (item.ID < 0)
            {
                result.AddViolation("Item has a negative ID value.  ID must be zero for new items and greater than zero for existing items.", "ID");
            }

            if (result.IsValid && Repository != null)
            {
                if (item.ID > 0 && Repository.Find(item.ID) == null)
                {
                    result.AddViolation("ID value must be zero or an ID that already exists in the database.", "ID");
                }
            }

            return result;
        }

        /// <summary>
        /// Performs a delete operation on the item indicated.
        /// </summary>
        /// <param name="id">The ID of the item of this type to remove from the database.</param>
        /// <param name="userID">The ID of the TOPUser performing this deletion, to verify permissions and perform activity
        /// logging.  Must match a valid user record.</param>
        public virtual void Delete(long id, long userID)
        {
            if (id <= 0) throw new ArgumentException(string.Format("Delete operation failed, out of valid range for ID value: {0}", id), "id");

            throwOnInvalidUserIDRange(userID);

            var item = Find(id);
            if (VerifyUserPermission(UserActionType.Delete, item, userID))
            {
                if (item != null)
                {
                    Repository.Delete(id);
                    Repository.Save();
                }
                else
                {
                    throw new ArgumentException(string.Format("Cannot find an item of the correct type matching the ID: {0}", id), "id");
                }
            }
        }

        #endregion IObjectLibrary<T> Members

        /// <summary>
        /// Peforms a check against available resources to verify that a specific user has the permissions to perform a specific action.
        /// </summary>
        /// <param name="action">UserActionType value indicating the action to be performed.</param>
        /// <param name="target">An instance of the class the user action will target. The specific item being affected should be passed when
        /// available for single-target actions.  Otherwise, a default instance can be passed.</param>
        /// <param name="userID">The ID of the user performing the operation.</param>
        /// <param name="throwError">A boolean that if true (default) will cause an exception to be thrown if the the user fails the
        /// verification.</param>
        /// <returns></returns>
        public virtual bool VerifyUserPermission(UserActionType action, DomainObject target, long userID, bool throwError = true)
        {
            var result = false;
            if (PermManager == null)
            {
                if (throwError)
                {
                    throw new UserPermissionsException(
                        string.Format("No permission manager was found for action {0} on type {1} for user ID {2}.",
                                action.ToString(),
                                (target == null ? "null" : target.GetType().Name),
                                userID),
                        userID);
                }
            }
            else
            {
                result = PermManager.IsValidUserAction(action, target, userID);
            }

            if (!result)
            {
                if (throwError)
                {
                    throw new UserPermissionsException(
                        string.Format("User has insufficient permissions for {0} on {1}.",
                                    action.ToString(),
                                    (target == null ? "null" : target.GetType().Name)),
                        userID);
                }
            }
            return result;
        }

        /// <summary>
        /// Throws an exception if the long integer passed is outside of hte valid range for a user ID value.
        /// </summary>
        /// <param name="userID">A value that is expected to be a user ID.</param>
        internal static void throwOnInvalidUserIDRange(long userID)
        {
            if (userID < 1)
            {
                throw new ArgumentException("The required User ID argument passed an invalid value.", "userID");
            }
        }

        /// <summary>
        /// If the value passed is null, throws an exception saying that the item cannot be found.
        /// </summary>
        /// <param name="result">The result of a Find or Get operation.</param>
        internal static void throwOnItemNotFound(T result)
        {
            if (result == null) throw new ArgumentException(string.Format("The {0} item specified cannot be found.", typeof(T).Name), "id");
        }

        /// <summary>
        /// Copies values from one instance of the target type to another.
        /// </summary>
        /// <param name="from">The instance to copy values from.</param>
        /// <param name="to">The instance to copy values to.</param>
        protected virtual void copyValues(T from, T to)
        {
            to.ID = from.ID;
            to.LastChanged = from.LastChanged;
            copyClassValues(from, to);
        }

        /// <summary>
        /// Modifies the values of an item to be stored in the data base, and possibly some related values, to handle any 
        /// automatic value settings that are required.
        /// </summary>
        /// <param name="item">The item that is being prepped for storage.</param>
        protected virtual void SetDomainAutomaticValues(T item)
        {
            item.LastChanged = DateTime.Now;
        }


        /// <summary>
        /// Copy values from one instance of the target class to another. Copies properties not included in the
        /// base class for this library type.
        /// </summary>
        /// <param name="from">The instance to copy values from.</param>
        /// <param name="to">The instance to copy values to.</param>
        protected abstract void copyClassValues(T from, T to);




        /// <summary>
        /// Returns a ValidationResult that is Valid if the item of this type with the ID passed can be deleted
        /// without violating business rules or creating data inconsitencies.
        /// </summary>
        /// <param name="id">The ID of the item to be deleted.</param>
        /// <returns></returns>
        public virtual ValidationResult ValidateDelete(long id)
        {
            var result = new ValidationResult();

            return result;
        }

    }
}