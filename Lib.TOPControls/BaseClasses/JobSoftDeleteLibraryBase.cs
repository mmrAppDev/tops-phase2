﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls.BaseClasses
{
    /// <summary>
    /// Implements basic operations for librarys that implment both the DomainJobObject class and the ISoftDeleted interface.
    /// </summary>
    /// <typeparam name="JObType">A type that is a subclass of DomainJobObject, implements ISoftDeleted, and have a parameterless constructor.</typeparam>
    public abstract class JobSoftDeleteLibraryBase<JObType> : SoftDeleteLibraryBase<JObType>, IJobObjectLibrary<JObType>
        where JObType : DomainJobObject, ISoftDeleted, new()
    {
        #region Constructors

        /// <summary>
        /// Performs default setup of JobSoftDeleteLibraryBase properties.
        /// </summary>
        public JobSoftDeleteLibraryBase()
            : base()
        { }

        /// <summary>
        /// Performs setup of JobSoftDeleteLibraryBase with a reference to an ITOPControlLibrary instance,
        /// and by default using its resources.
        /// </summary>
        /// <param name="rootLibrary">An object that implements the ITOPControlLibrary interface</param>
        public JobSoftDeleteLibraryBase(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }

        /// <summary>
        /// Performs setup of JobSoftDeleteLibraryBase using the provided environment values.
        /// </summary>
        /// <param name="useRepository">An object that implements IRepository for items of this type.</param>
        /// <param name="permissionManager">An object that implements IPermissionManager.</param>
        public JobSoftDeleteLibraryBase(IRepository<JObType> useRepository, IPermissionManager permissionManager)
            : base(useRepository, permissionManager)
        { }

        #endregion Constructors

        #region IJobObjectLibrary<JObType> Members

        /// <summary>
        /// Returns all items associated with the Job ID passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use as the item filter.</param>
        /// <returns></returns>
        public IEnumerable<JObType> AllForJob(long jobID)
        {
            if (Root != null && Root.JobActions != null)
            {
                var job = Root.JobActions.Find(jobID);
                if (job == null) return new List<JObType>();
            }
            return JobFilteredQueryable(jobID).Where(j => j.IsDeleted == false);
        }


        /// <summary>
        /// Returns an instance of the target type if there is a valid item with a primary descriptive string matching
        /// the name passed, unless the item is marked deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job the object should belong to.</param>
        /// <param name="name">The full name string to search for.</param>
        /// <returns></returns>
        public abstract JObType FindByName(long jobID, string name);

        #endregion IJobObjectLibrary<JObType> Members

        /// <summary>
        /// Returns all items associated with teh Job ID passed, regardless
        /// of their deleted state, or the deleted state of the respective job.
        /// </summary>
        /// <param name="jobID">The ID of the job to use as the item filter.</param>
        /// <param name="userID">The ID of the user attempting this action.</param>
        /// <returns></returns>
        public IEnumerable<JObType> AllForJobAny(long jobID, long userID)
        {
            throwOnInvalidUserIDRange(userID);

            VerifyUserPermission(UserActionType.ViewDeleted, new JObType(), userID);
            return JobFilteredQueryable(jobID).ToList();
        }

        /// <summary>
        /// Returns all items associated with the Job ID, regardless of deletion states,
        /// as an IQueryable.
        /// </summary>
        /// <param name="jobID">The ID of the job to use as the item filter.</param>
        /// <returns></returns>
        protected IQueryable<JObType> JobFilteredQueryable(long jobID)
        {
            if (jobID < 1) throw new ArgumentException("Job ID is outside of the valid range.", "jobID");
            return Repository.All.Where(j => j.JobID == jobID);
        }
    }
}