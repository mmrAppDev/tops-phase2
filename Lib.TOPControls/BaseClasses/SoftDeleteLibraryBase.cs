﻿using System;
using Lib.TOPControls.Exceptions;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls.BaseClasses
{
    /// <summary>
    /// Implements functionality for libraries for DomainObject classes that implement the ISoftDeleted interface.
    /// </summary>
    /// <typeparam name="T">A type the inherits from DomainObject, implements ISoftDelted, and has a parameterless constructor.</typeparam>
    public abstract class SoftDeleteLibraryBase<T> : ObjectLibraryBase<T>, ISoftDeleteLibrary<T> where T : DomainObject, ISoftDeleted, new()
    {
        #region Constructors

        /// <summary>
        /// Performs default setup of SoftDeleteLibraryBase properties.
        /// </summary>
        public SoftDeleteLibraryBase()
            : base()
        {
        }

        /// <summary>
        /// Performs setup of SoftDeleteLibraryBase using the provided environment values.
        /// </summary>
        /// <param name="useRepository">An object that implements IRepository for items of this type.</param>
        /// <param name="permissionManager">An object that implements IPermissionManager.</param>
        public SoftDeleteLibraryBase(IRepository<T> useRepository, IPermissionManager permissionManager)
            : base(useRepository, permissionManager)
        { }

        /// <summary>
        /// Performs setup of SoftDeleteLibraryBase with a reference to an ITOPControlLibrary instance,
        /// and by default using its resources.
        /// </summary>
        /// <param name="rootLibrary">An object that implements the ITOPControlLibrary interface</param>
        public SoftDeleteLibraryBase(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }

        #endregion Constructors

        /// <summary>
        /// Returns an instance of target type if there is a valid item with an ID value matching the value passed.  If
        /// the matching item is marked as deleted, or if there is no item with a matching ID, retuns NULL.
        /// </summary>
        /// <param name="id">The ID value to find.</param>
        /// <returns></returns>
        public override T Find(long id)
        {
            var result = base.Find(id);
            if (result != null && result.IsDeleted)
            {
                result = null;
            }
            return result;
        }

        /// <summary>
        /// Returns an instance of the target type. Creates a new record for an item of the specified type, if that item
        /// passes validation and the user indicated by the User ID has the right to create an item of this type.
        /// </summary>
        /// <param name="newItem">An instance of the type to be added.</param>
        /// <param name="userID">The ID of the TOPUser performing this operation, to verify permissions and perform activity
        /// logging.  Must match a valid user record.</param>
        /// <returns></returns>
        public override T Add(T newItem, long userID)
        {
            throwUpdateErrorOnDeletionState(newItem);
            return base.Add(newItem, userID);
        }

        /// <summary>
        /// Updates the record of this type that matches the item's ID with the rest of the item properties.
        /// </summary>
        /// <param name="item">An item to update in the database. This must not be NULL.</param>
        /// <param name="userID">The ID of the TOPUser performing this operation, for both logging and security purposes.
        /// Must match a valid user record.</param>
        /// <returns></returns>
        public override T Update(T item, long userID)
        {
            throwUpdateErrorOnDeletionState(item);
            return base.Update(item, userID);
        }

        /// <summary>
        /// Performs a delete operation on the item indicated.
        /// </summary>
        /// <param name="id">The ID of the item of this type to remove from the database.</param>
        /// <param name="userID">The ID of the TOPUser performing this deletion, to verify permissions and perform activity
        /// logging.  Must match a valid user record.</param>
        public override void Delete(long id, long userID)
        {
            var result = base.Find(id);
            throwOnInvalidUserIDRange(userID);
            throwOnItemNotFound(result);
            if (result != null && result.IsDeleted == false)
            {
                VerifyUserPermission(Permissions.UserActionType.Delete, result, userID);
                result.SetDeletedState(true);
                Repository.Save();
            }
        }

        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public override ValidationResult Validate(T item)
        {
            var result = base.Validate(item);

            if (item.IsDeleted)
                result.AddViolation("Cannot insert or update an item marked as deleted.", "IsDeleted");

            return result;
        }

        #region ISoftDeleteLibrary<T> Members

        /// <summary>
        /// Returns any item in the database that matches the ID requested, even if that item is marked as deleted.
        /// </summary>
        /// <param name="id">The ID value to find.</param>
        /// <param name="userID">The ID of a valid TOPUser item. If the user does not have the right to view deleted items
        /// of this type, returns a NULL.</param>
        /// <returns></returns>
        public T FindAny(long id, long userID)
        {
            throwOnInvalidUserIDRange(userID);
            var result = base.Find(id);
            if (!VerifyUserPermission(Permissions.UserActionType.ViewDeleted, result, userID))
            {
                throw new UserPermissionsException(string.Format("Permissions failure while attempting {0} operation for user id {1}.", "ViewDeleted", userID), userID);
            }

            return result;
        }

        /// <summary>
        /// Restores an item that has been marked as deleted, if it has not been destroyed.
        /// </summary>
        /// <param name="id">The ID value of the item to Undelete.</param>
        /// <param name="userID">The ID of a valid TOPUser item. If the user does not have the right to restore deleted items
        /// of this type, the operation will fail.</param>
        /// <returns>The undeleted instance of this item.</returns>
        public T Undelete(long id, long userID)
        {
            var result = base.Find(id);
            throwOnInvalidUserIDRange(userID);

            throwOnItemNotFound(result);

            if (result.IsDeleted)
            {
                VerifyUserPermission(Permissions.UserActionType.Undelete, result, userID);
                result.SetDeletedState(false);
                Repository.Save();
            }
            return result;
        }

        /// <summary>
        /// Completely removes the database record of an item that is marked as deleted.
        /// </summary>
        /// <param name="id">The ID of the item to remove.  The item must aready be marked as deleted.  To destroy active
        /// items, the Delete operation must first by performed.</param>
        /// <param name="userID">The ID of a valid TOPUser item. If the user does not have the right to destroy deleted items
        /// of this type, the operation will fail.</param>
        public void Destroy(long id, long userID)
        {
            var item = base.Find(id);
            throwOnInvalidUserIDRange(userID);
            throwOnItemNotFound(item);

            if (item.IsDeleted)
            {
                VerifyUserPermission(Permissions.UserActionType.Destroy, item, userID);

                Repository.Delete(item.ID);
                Repository.Save();
            }
        }

        #endregion ISoftDeleteLibrary<T> Members

        /// <summary>
        /// Throws an exception if the new item passed is marked as deleted.
        /// </summary>
        /// <param name="newItem">The item to be added or updated through the normal library operations.</param>
        private static void throwUpdateErrorOnDeletionState(T newItem)
        {
            if (newItem != null && newItem.IsDeleted) throw new ArgumentException("Items that are marked as deleted cannot be Added or Updated.", "newItem");
        }

        /// <summary>
        /// Copies values from one instance of the target type to another.
        /// </summary>
        /// <param name="from">The instance to copy values from.</param>
        /// <param name="to">The instance to copy values to.</param>
        protected override void copyValues(T from, T to)
        {
            base.copyValues(from, to);
            to.SetDeletedState(from.IsDeleted);
        }
    }
}