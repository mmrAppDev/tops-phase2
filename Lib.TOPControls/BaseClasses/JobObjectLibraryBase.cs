﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls.BaseClasses
{
    /// <summary>
    /// Provides implementations of the basic library operations for subclasses of the DomainJobObject class.
    /// </summary>
    /// <typeparam name="JObType">The type of the class this library affects.  Must be a subclass of
    /// DomainJobObject and have a parameterless constructor.</typeparam>
    public abstract class JobObjectLibraryBase<JObType> : ObjectLibraryBase<JObType>, IJobObjectLibrary<JObType>
        where JObType : DomainJobObject, new()
    {
        #region Constructors

        /// <summary>
        /// Performs default setup of JobObjectLibraryBase properties.
        /// </summary>
        public JobObjectLibraryBase()
            : base()
        {
        }

        /// <summary>
        /// Performs setup of JobObjectLibraryBase using the provided environment values.
        /// </summary>
        /// <param name="useRepository">An object that implements IRepository for items of this type.</param>
        /// <param name="permissionManager">An object that implements IPermissionManager.</param>
        public JobObjectLibraryBase(IRepository<JObType> useRepository = null, IPermissionManager permissionManager = null)
            : base(useRepository, permissionManager)
        { }

        /// <summary>
        /// Performs setup of JobObjectLibraryBase with a reference to an ITOPControlLibrary instance,
        /// and by default using its resources.
        /// </summary>
        /// <param name="rootLibrary">An object that implements the ITOPControlLibrary interface</param>
        public JobObjectLibraryBase(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }

        #endregion Constructors

        #region IJobObjectLibrary<JObType> Members

        /// <summary>
        /// Returns all items associated with the Job ID passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use as the item filter.</param>
        /// <returns></returns>
        public virtual IEnumerable<JObType> AllForJob(long jobID)
        {
            if (jobID < 1) throw new ArgumentException("Job ID is outside of the valid range.", "jobID");
            if (Root != null && Root.JobActions != null)
            {
                var job = Root.JobActions.Find(jobID);
                if (job == null) return new List<JObType>();
            }

            return Repository.All.Where(j => j.JobID == jobID).ToList();
        }



        /// <summary>
        /// Returns an instance of the target type if there is a valid item with a primary descriptive string matching
        /// the name passed, unless the item is marked deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job the object should belong to.</param>
        /// <param name="name">The full name string to search for.</param>
        /// <returns></returns>
        public abstract JObType FindByName(long jobID, string name);

        #endregion IJobObjectLibrary<JObType> Members

        #region Overridden operations


        /// <summary>
        /// Returns a validation result for the item passed.
        /// </summary>
        /// <param name="item">The item to validate.</param>
        /// <returns></returns>
        public override ValidationResult Validate(JObType item)
        {
            var result = base.Validate(item);

            if (item.JobID <= 0)
            {
                result.AddViolation("The Job ID is outside of the valid range.", "JobID");
            }
            if (Root != null && Root.JobActions != null)
            {
                if (Root.JobActions.Find(item.JobID) == null)
                {
                    result.AddViolation("The Job ID being used does not exist in the database.", "JobID");
                }
            }

            return result;
        }




        #endregion
    }
}