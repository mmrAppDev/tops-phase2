﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMRCommon.Data;
using Lib.TOPControls.Interfaces.DataAccess;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;

namespace Lib.TOPControls
{
    /// <summary>
    /// Implements ProgressLogEntry management functions
    /// </summary>
    public class ProgressLogEntryLibrary:ObjectLibraryBase<ProgressLogEntry>,IProgressLogEntryLibrary
    {
        #region Constructors
        /// <summary>
        /// Creates a ProgressLogEntryLibrary with default values.
        /// </summary>
        public ProgressLogEntryLibrary()
            : base()
        { }
        /// <summary>
        /// Creates a ProgressLogEntryLibrary using values and resources from the Root library passed
        /// </summary>
        /// <param name="rootLibrary">An object that implements ITOPControlLibrary</param>
        public ProgressLogEntryLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }
        /// <summary>
        /// Creates a ProgressLogEntryLibrary with the resources passed.
        /// </summary>
        /// <param name="ProgressLogEntryRepository">The repository to use.</param>
        /// <param name="permissionManager">The IPermissionManager to use.</param>
        public ProgressLogEntryLibrary(IRepository<ProgressLogEntry> ProgressLogEntryRepository = null, IPermissionManager permissionManager = null)
            : base(ProgressLogEntryRepository, permissionManager)
        { }

        #endregion

        /// <summary>
        /// Performs validation on whether the item passed can be saved.  Returns a ValidationResult object.
        /// </summary>
        /// <param name="item">A ProgressLogEntry instance to validate.</param>
        /// <returns></returns>
        public override ValidationResult Validate(ProgressLogEntry item)
        {
            var result = base.Validate(item);

            if (item.CommodityCode != null) result.ValidateStringLength(item.CommodityCode, 0, 200, "CommodityCode");
            if (item.CommodityDescription != null) result.ValidateStringLength(item.CommodityDescription, 0, 200, "CommodityDescription");
            if (item.CraftCode != null) result.ValidateStringLength(item.CraftCode, 0, 200, "CraftCode");
            if (item.EntryUserFirstName != null) result.ValidateStringLength(item.EntryUserFirstName, 0, 100, "EntryUserFirstName");
            if (item.EntryUserLastName != null) result.ValidateStringLength(item.EntryUserLastName, 0, 100, "EntryUserLastName");
            if (item.EntryUsername != null) result.ValidateStringLength(item.EntryUsername, 0, 50, "Entryusername");
            if (item.JobName != null) result.ValidateStringLength(item.JobName, 0, 100, "JobName");
            if (item.ObjectTypeName != null) result.ValidateStringLength(item.ObjectTypeName, 0, 200, "ObjectTypeName");
            if (item.PhaseTitle != null) result.ValidateStringLength(item.PhaseTitle, 0, 50, "PhaseTitle");
            if (item.ProjectAreaNumber != null) result.ValidateStringLength(item.ProjectAreaNumber, 0, 200, "ProjectAreaNumber");
            if (item.ProjectSystemNumber != null) result.ValidateStringLength(item.ProjectSystemNumber, 0, 200, "ProjectSystemNumber");
            if (item.TaskTitle != null) result.ValidateStringLength(item.TaskTitle, 0, 50, "TaskTitle");
            if (item.UDP1_Name != null) result.ValidateStringLength(item.UDP1_Name, 0, 50, "UDP1_Name");
            if (item.UDP1_Value != null) result.ValidateStringLength(item.UDP1_Value, 0, 200, "UDP1_Value");
            if (item.UDP2_Name != null) result.ValidateStringLength(item.UDP1_Name, 0, 50, "UDP2_Name");
            if (item.UDP2_Value != null) result.ValidateStringLength(item.UDP1_Value, 0, 200, "UDP2_Value");
            if (item.UDP3_Name != null) result.ValidateStringLength(item.UDP1_Name, 0, 50, "UDP3_Name");
            if (item.UDP3_Value != null) result.ValidateStringLength(item.UDP1_Value, 0, 200, "UDP3_Value");
            if (item.WorkCrewDescription != null) result.ValidateStringLength(item.WorkCrewDescription, 0, 200, "WorkCrewDescription");



            if (item.JobID < 1)
            {
                result.AddViolation("The JobID value is invalid.", "JobID");
            }
            // Test for valid system number

            return result;
        }

        /// <summary>
        /// Removes a Progress Log entry including making needed changes to other historical values that are based on the deleted item.
        /// </summary>
        /// <param name="id">The ID of the ProgressLogEntry to be deleted.</param>
        /// <param name="userID">The ID of the User Performing the change.</param>
        public override void Delete(long id, long userID)
        {
            DateTime? workDate = null;
            DateTime entryDate;
            long? taskProgressID = null;

            var item = Find(id);
            if (item != null)
            {
                workDate = item.WorkDate;
                entryDate = item.EntryDate;
                taskProgressID = item.TaskProgressID;
                
                if (item.TaskProgressID.HasValue 
                        && item.OldPercentComplete.HasValue 
                        && item.NewPercentComplete.HasValue 
                        && item.OldPercentComplete.Value != item.NewPercentComplete.Value)
                {
                    ProgressLogEntry prevEntry;
                    ProgressLogEntry nextEntry;

                    if (workDate.HasValue)
                    {
                        prevEntry = Repository.All.Where(ple => ple.ID != id && ple.TaskProgressID == taskProgressID
                                                                && ple.WorkDate.HasValue && (ple.WorkDate.Value < workDate.Value ||
                                                                  (ple.WorkDate.Value == workDate.Value && ple.EntryDate <= entryDate)))
                                                                .OrderByDescending(x => x.WorkDate.Value)
                                                                .ThenByDescending(y => y.EntryDate)
                                                                .ThenByDescending(y => y.ID)
                                                                .FirstOrDefault();
                        nextEntry = Repository.All.Where(ple => ple.ID != id && ple.TaskProgressID == taskProgressID
                                                                && ple.WorkDate.HasValue && (ple.WorkDate.Value > workDate.Value ||
                                                                    (ple.WorkDate.Value == workDate.Value && ple.EntryDate > entryDate)))
                                                                .OrderBy(x => x.WorkDate.Value)
                                                                .ThenBy(y => y.EntryDate)
                                                                .ThenBy(z => z.ID)
                                                                .FirstOrDefault();

                        if (nextEntry != null)
                        {
                            if (prevEntry != null)
                            {
                                nextEntry.OldPercentComplete = (prevEntry.NewPercentComplete ?? 0m);
                            }
                            else
                            {
                                nextEntry.OldPercentComplete = 0m;
                            }
                            nextEntry.LastChanged = DateTime.Now;
                        }
                    }
                }
            }
            base.Delete(id, userID);
        }


        /// <summary>
        /// Adds a condition to the IQueryable passed that filters items down to records that have Progress entries by
        /// one of the WorkCrew IDs passed.
        /// </summary>
        /// <param name="filteredList">A reference to an IQueryable of TaskProgress to be filtered.</param>
        /// <param name="values">A set of WorkCrew ID values to use to filter records.</param>
        /// <param name="repositoryProgressLog">An object that implements the IRepository interface for ProgessLogEntry items.</param>
        public static void AttachTaskProgressWorkCrewFilter(ref IQueryable<TaskProgress> filteredList, 
                                                        IEnumerable<long> values,
                                                        IRepository<ProgressLogEntry> repositoryProgressLog)
        {
            if (values != null && values.Count() > 0)
            {
                var tplist = from ple in repositoryProgressLog.All
                             where ple.WorkCrewID != null && values.Contains(ple.WorkCrewID.Value)
                                    && ple.TaskProgressID.HasValue && ple.TaskProgress != null
                             select ple.TaskProgress.ID;
                filteredList = filteredList.Where(tp => tplist.Contains(tp.ID));
            }
        }


        /// <summary>
        /// Copies ProgressLogEntry values from one object to another.
        /// </summary>
        /// <param name="from">Object to copy from.</param>
        /// <param name="to">Object to copy to.</param>
        protected override void copyClassValues(ProgressLogEntry from, ProgressLogEntry to)
        {
            to.EntryDate = from.EntryDate;
            to.WorkDate = from.WorkDate;
            to.JobID = from.JobID;
            to.JobName = from.JobName;
            to.UserID = from.UserID;
            to.EntryUsername = from.EntryUsername;
            to.CommodityID = from.CommodityID;
            to.CommodityCode = from.CommodityCode;
            to.CommodityDescription = from.CommodityDescription;
            to.TaskID = from.TaskID;
            to.TaskTitle = from.TaskTitle;
            to.TaskProgressID = from.TaskProgressID;
            to.TaskManHours = from.TaskManHours;
            to.PhaseID = from.PhaseID;
            to.PhaseTitle = from.PhaseTitle;
            to.TrackedItemID = from.TrackedItemID;
            to.TrackingID = from.TrackingID;
            to.ObjectTypeID = from.ObjectTypeID;
            to.ObjectTypeName = from.ObjectTypeName;
            to.ProjectSystemID = from.ProjectSystemID;
            to.ProjectSystemNumber = from.ProjectSystemNumber;
            to.ProjectAreaID = from.ProjectAreaID;
            to.ProjectAreaNumber = from.ProjectAreaNumber;
            to.CraftInfoID = from.CraftInfoID;
            to.CraftCode = from.CraftCode;
            to.UDP1_Name = from.UDP1_Name;
            to.UDP1_Value = from.UDP1_Value;
            to.UDP2_Name = from.UDP2_Name;
            to.UDP2_Value = from.UDP2_Value;
            to.UDP3_Name = from.UDP3_Name;
            to.UDP3_Value = from.UDP3_Value;
            to.WorkCrewID = from.WorkCrewID;
            to.WorkCrewNumber = from.WorkCrewNumber;
            to.WorkCrewDescription = from.WorkCrewDescription;
            to.OldPercentComplete = from.OldPercentComplete;
            to.NewPercentComplete = from.NewPercentComplete;
            
        }

    }
}
