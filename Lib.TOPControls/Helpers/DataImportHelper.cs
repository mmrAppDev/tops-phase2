﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Lib.TOPControls.DataImport;

namespace Lib.TOPControls.Helpers
{
    /// <summary>
    /// Provides helper functions for moving data retrieved from a file to the 
    /// TopControls library.
    /// </summary>
    public class DataImportHelper
    {
        /// <summary>
        /// Gets sets the component set of Schema aliases.
        /// </summary>
        public TaskImportColumnNames Aliases { get; set; }

        public SubsystemColumnNames SubsystemAliases { get; set; }

        /// <summary>
        /// The uploaded file as a FileImport item, to be imported.
        /// </summary>
        public  FileImport TargetFile { get; set; }

        private Lazy<Dictionary<FilterValueType, int>> _map;

        /// <summary>
        /// A map identifying the column locations by the FilterValueType enumeration.
        /// </summary>
        public Dictionary<FilterValueType, int> Map
        {
            get
            {
                return _map.Value;
            }
        }

        private Lazy<int[]> _subsystemMap;

        public int[] SubsystemMap
        {
            get { return _subsystemMap.Value; }
        }


        private Lazy<DataTable> _rawTable;

        private DataTable getTable()
        {
            return TargetFile.GetAsDataSet().Tables[0];
        }

        /// <summary>
        /// Gets the DataTable imported from the associated file.
        /// </summary>
        public DataTable RawTable
        {
            get { return _rawTable.Value; }
        }

        /// <summary>
        /// Creates an instance of the DataImportHelper based on the FileImport instance passed.
        /// </summary>
        /// <param name="fileImport"></param>
        public DataImportHelper(FileImport fileImport)
        {
            TargetFile = fileImport;

            _rawTable = new Lazy<DataTable>(() => getTable());

            _map = new Lazy<Dictionary<FilterValueType, int>>(() => buildColumnIndices());
            _subsystemMap = new Lazy<int[]>(() => buildSubsystemIndices());

            Aliases = new TaskImportColumnNames();
            SubsystemAliases = new SubsystemColumnNames(Aliases.GetAliases(FilterValueType.ProjectSystem));

            if (fileImport.JobInfo != null)
            {
                Aliases.TransformForJob(fileImport.JobInfo);
            }
        }


        /// <summary>
        /// Gets a list of distinct values for a specific column 
        /// </summary>
        /// <param name="aliases">The list of aliases to use when loading a single column.</param>
        /// <returns></returns>
        public IEnumerable<string> GetDistinctList(IEnumerable<string> aliases)
        {
            IEnumerable<string> result = new List<string>();
            var colIdx = -1;
            foreach (var tryName in aliases)
            {
                colIdx = RawTable.Columns.IndexOf(tryName);
                if (colIdx > -1) break;
            }
            if (colIdx > -1)
            {
                var x = from DataRow dr in RawTable.Rows
                        select dr[colIdx].ToString();
                result = x.Distinct();
            }
            return result;
        }

        /// <summary>
        /// Retrieves a distinct list of values for a column matching the name passed.
        /// </summary>
        /// <param name="valueName"></param>
        /// <returns></returns>
        public IEnumerable<string> GetDistinctList(string valueName)
        {
            return GetDistinctList(new string[] { valueName });
        }

        /// <summary>
        /// Retreives the distinct list of values in the column indicated by the FilterValueType
        /// </summary>
        /// <param name="ftype"></param>
        /// <returns></returns>
        public IEnumerable<string> GetDistinctList(FilterValueType ftype)
        {
            return GetDistinctList(Aliases.GetAliases(ftype));
        }

        /// <summary>
        /// Retrieves a distinct list of values.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetCommodityList()
        {
            return GetDistinctList(FilterValueType.Commodity);
        }

        /// <summary>
        /// Retrieves a distinct list of values.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetCraftList()
        {
            return GetDistinctList(FilterValueType.CraftInfo);
        }

        /// <summary>
        /// Retrieves a distinct list of values.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetObjectTypeList()
        {
            return GetDistinctList(FilterValueType.ObjectType);
        }

        /// <summary>
        /// Retrieves a distinct list of values.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetAreaList()
        {
            return GetDistinctList(FilterValueType.ProjectArea);
        }

        /// <summary>
        /// Retrieves a distinct list of values.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetPhaseList()
        {
            return GetDistinctList(FilterValueType.Phase);
        }

        /// <summary>
        /// Retrieves a distinct list of values.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetSystemList()
        {
            return GetDistinctList(FilterValueType.ProjectSystem);
        }

        /// <summary>
        /// Retrieves a distinct list of values.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetTaskList()
        {
            return GetDistinctList(FilterValueType.Task);
        }

        /// <summary>
        /// Retrieves a distinct list of values.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetTrackingIDList()
        {
            return GetDistinctList(FilterValueType.TrackingID);
        }

        /// <summary>
        /// Retrieves a distinct list of values that appear more than once in the TrackingID column.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetTrackingIDDuplicates()
        {
            var map = GetColumnIndices();
            var comparer = StringComparer.CurrentCultureIgnoreCase;
            
            if (map.ContainsKey(FilterValueType.TrackingID))
            {
                var duplicates = new List<string>();
                var identified = new List<string>();
                var tidIdx = map[FilterValueType.TrackingID];
                foreach (DataRow dr in RawTable.Rows)
                {
                    var tidVal = dr[tidIdx].ToString();
                    if (identified.Contains(tidVal, comparer))
                    {
                        if (!duplicates.Contains(tidVal, comparer))
                        {
                            duplicates.Add(tidVal);
                        }
                    }
                    else
                    {
                        identified.Add(tidVal);
                    }
                }
                return duplicates;
            }
            else
            {
                return new List<string>();
            }

        }

        /// <summary>
        /// Retrieves a distinct list of values.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PhaseTaskName> GetPhaseTaskNames()
        {
            var result = new List<PhaseTaskName>();
            var phaseColIdx = -1;
            var taskColIdx = -1;

            foreach (var tryName in Aliases.GetAliases(FilterValueType.Phase))
            {
                phaseColIdx = RawTable.Columns.IndexOf(tryName);
                if (phaseColIdx > -1) break;
            }
            foreach (var tryName in Aliases.GetAliases(FilterValueType.Task))
            {
                taskColIdx = RawTable.Columns.IndexOf(tryName);
                if (taskColIdx > -1) break;
            }
            if (phaseColIdx > -1 && taskColIdx > -1)
            {
                foreach (DataRow dr in RawTable.Rows)
                {
                    if (result.Count(ptn => ptn.PhaseName == dr[phaseColIdx].ToString()
                            && ptn.TaskName == dr[taskColIdx].ToString()) == 0)
                    {
                        result.Add(new PhaseTaskName(dr[phaseColIdx].ToString(), dr[taskColIdx].ToString()));
                    }
                }
            }
            return result;
        }
        // find items by name is looking like a good ObjectTypeLibrary function.

        /// <summary>
        /// Retrieves a dictionary returning the index of each column according to the FilterValueType it matches.
        /// </summary>
        /// <returns></returns>
        public Dictionary<FilterValueType, int> GetColumnIndices()
        {
            return Map;
        }


        private Dictionary<FilterValueType, int> buildColumnIndices()
        {
            var result = new Dictionary<FilterValueType, int>();

            SetColumnIdx(result, FilterValueType.Commodity);
            SetColumnIdx(result, FilterValueType.CraftInfo);
            SetColumnIdx(result, FilterValueType.ObjectType);
            SetColumnIdx(result, FilterValueType.Phase);
            SetColumnIdx(result, FilterValueType.ProjectArea);
            SetColumnIdx(result, FilterValueType.ProjectSystem);
            SetColumnIdx(result, FilterValueType.Task);
            SetColumnIdx(result, FilterValueType.TrackingID);
            SetColumnIdx(result, FilterValueType.UserDefinedProperty1);
            SetColumnIdx(result, FilterValueType.UserDefinedProperty2);
            SetColumnIdx(result, FilterValueType.UserDefinedProperty3);
            SetColumnIdx(result, FilterValueType.WorkCrew);

            return result;

        }

        private int[] buildSubsystemIndices()
        {
            List<int> result = new List<int>();

            int depth = 0;

            int newIdx = GetSubsystemIndex(depth);
            while (newIdx > -1)
            {
                result.Add(newIdx);
                depth++;
                newIdx = GetSubsystemIndex(depth);
            }

            return result.ToArray();
        }


        private void SetColumnIdx(Dictionary<FilterValueType,int> result, FilterValueType thisType)
        {
                    var thisIdx = GetColumnIndex(thisType);
                    if (thisIdx > -1) result.Add(thisType, thisIdx);
        }


        /// <summary>
        /// Gets the column index for a specific filter value type.
        /// </summary>
        /// <param name="forValue"></param>
        /// <returns></returns>
        public int GetColumnIndex(FilterValueType forValue)
        {
            var result = -1;

            foreach (var tryName in Aliases.GetAliases(forValue))
            {
                result = RawTable.Columns.IndexOf(tryName);
                if (result > -1) break;
            }
            return result;
        }


        public int GetSubsystemIndex(int level)
        {
            return GetSequenceIndex(SubsystemAliases, level);
        }

        /// <summary>
        /// Gets the column index for the specified level of the sequence aliases passed.
        /// Returns -1 if none of the aliases can be matched.
        /// </summary>
        /// <param name="seqAliases">The sequence column names object for the sequence</param>
        /// <param name="sequenceLevel">The depth in the sequence from lowest to highest.</param>
        /// <returns></returns>
        public int GetSequenceIndex(SequenceColumnNames seqAliases, int sequenceLevel)
        {
            int result = -1;

            foreach (string tryName in seqAliases.GetAliases(sequenceLevel))
            {
                result = RawTable.Columns.IndexOf(tryName);
                if (result > -1) break;
            }

            return result;
        }

        /// <summary>
        /// Retrieves and trims a string value from a datarow, based on the Mapped position of the type requested.
        /// </summary>
        /// <param name="dr">A datarow from the File Import table.</param>
        /// <param name="forValue">A value from the FilterValueType enumeration indicating the import column to check.</param>
        /// <returns></returns>
        public string GetImportedRowValue(DataRow dr, FilterValueType forValue)
        {
            
            string result = null;
            if (!dr.IsNull(Map[forValue]))
            {
                result = GetImportedRowValueByIdx(dr, Map[forValue]);
            }
            return result;
        }


        internal string GetImportedRowValueByIdx(DataRow dr, int idx)
        {
            string result = null;
            if (!dr.IsNull(idx))
            {
                result = dr[idx].ToString().Trim();
            }
            return result;
        }
    }
}
