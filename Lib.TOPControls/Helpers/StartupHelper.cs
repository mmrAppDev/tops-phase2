﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.Charting;
using Lib.TOPControls.Exceptions;
using Lib.TOPControls.Security;
using MMRCommon.Data;

namespace Lib.TOPControls.Helpers
{
    /// <summary>
    /// Provides simple functions for initial application setup.
    /// </summary>
    public class StartupHelper
    {

        ITOPControlLibrary Root;

        /// <summary>
        /// Creates an instance of the StartupHelper.
        /// </summary>
        /// <param name="useTOPControlLibrary"></param>
        public StartupHelper(ITOPControlLibrary useTOPControlLibrary)
        {
            Root = useTOPControlLibrary;
        }

        /// <summary>
        /// Performs the basic startup operations for the job.
        /// </summary>
        /// <param name="jobTitle"></param>
        /// <param name="adminPassword"></param>
        public void StartupJob(string jobTitle, string adminPassword)
        {

            // create the admin user
            var admin = new TOPUser
            {
                Login = "sysadmin",
                FirstName = "System",
                LastName = "Administrator"
            };

            var vr = Root.UserActions.Validate(admin);
            if (!vr.IsValid) throw new ValidationFailureException("Validation error.", vr);

            Root.UserActions.AddWithCredentials(admin.Login,
                    admin.FirstName, admin.LastName, adminPassword,
                    "1-1-2-3-5-?", "8", 1);
            admin = Root.UserActions.FindByUsername(admin.Login);

            startupDefaultRoleCreation();
            startupDefaultChartDefinitionCreation();

            var repRole = Root.GetRepository<Role>();
            var adminRole = repRole.All.Where(x => x.RoleName == "TOPAdmin").First();
            var repUR = Root.GetRepository<UserRole>();
            var UR =new UserRole { RoleID = adminRole.ID, UserID = admin.ID };
            UR.LastChanged = DateTime.Now;

            repUR.Insert(UR);
            repUR.Save();

            JobInfo job = null;
            if (admin != null && admin.ID > 0)
            {
                // create the key job
                job = new JobInfo
                {
                    Title = jobTitle
                };
                vr = Root.JobActions.Validate(job);
                if (vr.IsValid)
                {
                    job = Root.JobActions.Add(job, admin.ID);
                    if (job != null)
                    {
                        admin.DefaultJobID = job.ID;
                        Root.UserActions.Update(admin, admin.ID);

                    }
                    else
                    {
                        CleanupFailedStartupAction();
                        throw new ApplicationException("There was a problem creating the job.");
                    }
                }
                else
                {
                    CleanupFailedStartupAction();
                    throw new ValidationFailureException("Validation error.", vr);
                }
            }
            else
            {
                CleanupFailedStartupAction();
                throw new ApplicationException("There was a problem creating the user account: sysadmin");
            }

        }


        /// <summary>
        /// Removes items added during a failed startup to prevent them from being in the way when it is attempted again.
        /// </summary>
        public void CleanupFailedStartupAction()
        {
            emptyRepository<Credential>();
            emptyRepository<TOPUser>();
            emptyRepository<WorkCrew>();
            emptyRepository<FilterConfiguration>();
            emptyRepository<Task>();
            emptyRepository<ProjectPhase>();
            emptyRepository<CraftInfo>();
            emptyRepository<JobInfo>();

        }

        private void emptyRepository<DJOType>()
            where DJOType:DomainObject, new()
        {
            var rep = Root.GetRepository<DJOType>();
            var items = rep.All.ToArray();
            foreach(var item in items)
            {
                rep.Delete(item.ID);
            }
            rep.Save();
        }


        /// <summary>
        /// Sets up any Roles not currenlty in the database.
        /// </summary>
        private void startupDefaultRoleCreation()
        {
            var repRole = Root.GetRepository<Role>();

            var setupDate = DateTime.Now;

            if (repRole.All.Count(x => x.RoleName == "TOPAdmin") == 0)
            {
                var adminRole = new Role { RoleName = "TOPAdmin", Description = "System Administrator", LastChanged = setupDate };
                repRole.Insert(adminRole);
            }

            if (repRole.All.Count(x => x.RoleName == "TOPManager") == 0)
            {
                var manageRole = new Role { RoleName = "TOPManager", Description = "Project Manager", LastChanged = setupDate };
                repRole.Insert(manageRole);
            }

            if (repRole.All.Count(x => x.RoleName == "TOPEntry") == 0)
            {
                var entryRole = new Role { RoleName = "TOPEntry", Description = "Data Entry User", LastChanged = setupDate };
                repRole.Insert(entryRole);
            }

            if (repRole.All.Count(x => x.RoleName == "TOPClient") == 0)
            {
                var clientRole = new Role { RoleName = "TOPClient", Description = "Client", LastChanged = setupDate };
                repRole.Insert(clientRole);
            }
            repRole.Save();
        }


        private void startupDefaultChartDefinitionCreation()
        {
            var repCD = Root.GetRepository<ChartDefinition>();
            if (repCD.All.Count(x => x.ChartTypeValue == (int)ChartType.Historical && x.FilterValueTypeValue == (int)FilterValueType.None) == 0)
            {
                var jobHist = new ChartDefinition { ChartType = ChartType.Historical, FilterValueType = FilterValueType.None, IsGlobal = true, LastChanged = DateTime.Now };
                repCD.Insert(jobHist);
            }
            if (repCD.All.Count(x => x.ChartTypeValue == (int)ChartType.Historical && x.FilterValueTypeValue == (int)FilterValueType.Phase) == 0)
            {
                var phaseHist = new ChartDefinition { ChartType = ChartType.Historical, FilterValueType = FilterValueType.Phase, IsGlobal = true, LastChanged = DateTime.Now };
                repCD.Insert(phaseHist);
            }
            if (repCD.All.Count(x => x.ChartTypeValue == (int)ChartType.Historical && x.FilterValueTypeValue == (int)FilterValueType.Task) == 0)
            {
                var taskHist = new ChartDefinition { ChartType = ChartType.Historical, FilterValueType = FilterValueType.Task, IsGlobal = true, LastChanged = DateTime.Now };
                repCD.Insert(taskHist);
            }
            if (repCD.All.Count(x => x.ChartTypeValue == (int)ChartType.GroupedCompletion && x.FilterValueTypeValue == (int)FilterValueType.None) == 0)
            {
                var jobGroup = new ChartDefinition { ChartType = ChartType.GroupedCompletion, FilterValueType = FilterValueType.None, IsGlobal = true, LastChanged = DateTime.Now };
                repCD.Insert(jobGroup);
            }
            if (repCD.All.Count(x => x.ChartTypeValue == (int)ChartType.GroupedCompletion && x.FilterValueTypeValue == (int)FilterValueType.ProjectSystem) == 0)
            {
                var sysGroup = new ChartDefinition { ChartType = ChartType.GroupedCompletion, FilterValueType = FilterValueType.ProjectSystem, IsGlobal = true, LastChanged = DateTime.Now };
                repCD.Insert(sysGroup);
            }
            repCD.Save();
        }

    }
}
