﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Envoc.Core.Shared.Extensions;
using Lib.TOPControls.Charting;
using MMRCommon.Data;

namespace Lib.TOPControls.Helpers
{
    /// <summary>
    /// Provides helper functions for performing charting operations.
    /// </summary>
    public class ChartingHelper
    {
        /// <summary>
        /// Gets/Sets the TOPControlLibrary being used.
        /// </summary>
        public ITOPControlLibrary Root { get; set; }

        #region Constructors

        /// <summary>
        /// Create an instance.
        /// </summary>
        /// <param name="root">TOPControlLibrary being used</param>
        public ChartingHelper(ITOPControlLibrary root)
        {
            Root = root;
        }

        #endregion

        /// <summary>
        /// Returns the Latest update date and time for all Historical records matching the job passed.
        /// </summary>
        /// <param name="jobID">ID of the job to query.</param>
        /// <returns></returns>
        public DateTime GetJobHistoryCheckpoint(long jobID)
        {
            DateTime result = DateTime.MaxValue;
            var repProgHist = Root.GetRepository<ProgressHistory>();
            var set = repProgHist.All.Where(x => x.JobID == jobID);
            if (set.Count() > 0)
            {
                result = set.Max(y => y.LastChanged);
            }
            return result;
        }

        /// <summary>
        /// Returns the practical run date of the Chart and Job requested.
        /// </summary>
        /// <param name="jobID">ID of the job to query.</param>
        /// <param name="chartDefinitionID">The ID of the Chart Definition to check.</param>
        /// <returns></returns>
        public DateTime GetHistoryChartExpiration(long jobID, long chartDefinitionID)
        {
            DateTime result = DateTime.MinValue;
            var rep = Root.GetRepository<HistoryChartData>();
            var set = rep.All.Where(x => x.JobID == jobID && x.ChartDefinitionID == chartDefinitionID);
            if (set.Count() > 0)
            {
                result = set.Min(y => y.RunDate);
            }
            return result;
        }

        /// <summary>
        /// Returns the practical run date of the specified chart and job.
        /// </summary>
        /// <param name="jobID">ID of the job to query.</param>
        /// <param name="chartDefinitionID">The ID of the Chart Definition to check.</param>
        /// <returns></returns>
        public DateTime GetGroupingChartExpiration(long jobID, long chartDefinitionID)
        {
            DateTime result = DateTime.MinValue;
            var rep = Root.GetRepository<GroupedCompletionChart>();
            var set = rep.All.Where(x => x.JobID == jobID && x.ChartDefinitionID == chartDefinitionID);
            if (set.Count() > 0)
            {
                result = set.Min(y => y.RunDate);
            }
            return result;
        }

        /// <summary>
        /// Updates all pre-generated progress data for all items. 
        /// </summary>
        public void UpdateAllCharts()
        {
            var fullLib = (TOPControlLibrary) Root;
            fullLib.UpdateAllProgressHistory();
            var jobs = Root.JobActions.Search("");
            var charts = Root.ChartDefinitionActions.GetAll();
            foreach (var j in jobs)
            {
                foreach (var ch in charts)
                {
                    if (ch.ChartType == ChartType.Historical)
                    {
                        UpdateHistoryChartData(j.ID, ch.ID);
                    }
                    else if (ch.ChartType == ChartType.GroupedCompletion)
                    {
                        UpdateGroupedCompletionChartData(j.ID, ch.ID);
                    }
                }
            }
        }

        /// <summary>
        /// Updates all pre-generated progress data for all items. 
        /// </summary>
        public void UpdateAllCharts(long? jobID = null)
        {
            var fullLib = (TOPControlLibrary) Root;
            fullLib.UpdateAllProgressHistory();
            var jobs = Root.JobActions.Search("");
            var charts = Root.ChartDefinitionActions.GetAll();
            foreach (var j in jobs)
            {
                foreach (var ch in charts)
                {
                    if (ch.ChartType == ChartType.Historical)
                    {
                        UpdateHistoryChartData(j.ID, ch.ID);
                    }
                    else if (ch.ChartType == ChartType.GroupedCompletion)
                    {
                        UpdateGroupedCompletionChartData(j.ID, ch.ID);
                    }
                }
            }
        }

        /// <summary>
        /// Repository for the Grouped Completion Chart.
        /// </summary>
        protected IRepository<GroupedCompletionChart> RepGCC
        {
            get { return Root.GetRepository<GroupedCompletionChart>(); }
        }

        /// <summary>
        /// Repository for the Grouped Completion Chart Data Point.
        /// </summary>
        protected IRepository<GroupedCompletionChartDataPoint> RepGCCDP
        {
            get { return Root.GetRepository<GroupedCompletionChartDataPoint>(); }
        }

        /// <summary>
        /// Triggers an update operation for the Grouped Completion Chart data represented by the Job and Chart Definition ID.
        /// </summary>
        /// <param name="jobID">ID of the target job.</param>
        /// <param name="chartDefinitionID">ID of the ChartDefinition that describes the chart.</param>
        public void UpdateGroupedCompletionChartData(long jobID, long chartDefinitionID)
        {
            ChartDefinition chart = Root.ChartDefinitionActions.Find(chartDefinitionID);
            if (chart == null)
                throw new ArgumentException("Attempted to generate a chart that does not exist.", "chartDefinitionID");
            if (chart.ChartType != ChartType.GroupedCompletion)
                throw new ArgumentException(
                    string.Format("Attempted Grouped Chart update of chart ID {0}, which is a {1} chart.",
                        chartDefinitionID, chart.ChartType.ToString()), "chartDefinitionID");

            var fullLib = (TOPControlLibrary) Root;
            fullLib.UpdateAllProgressHistory();

            var chartUpdated = GetGroupingChartExpiration(jobID, chartDefinitionID);
            var progressUpdated = GetJobHistoryCheckpoint(jobID);

            if (progressUpdated >= chartUpdated)
            {
                // The data for this chart is out of date. Update it.
                GroupedCompletionChart groupChart = Root.GroupedCompletionChartActions.GetByDefinition(jobID,
                    chart.FilterValueType);
                if (groupChart == null)
                {
                    // This chart has no existing data.
                    groupChart = new GroupedCompletionChart
                    {
                        ChartDefinition = chart,
                        JobID = jobID,
                        LastChanged = DateTime.Now,
                        RunDate = DateTime.Now
                    };

                    RepGCC.Insert(groupChart);
                    RepGCC.Save();
                }
                else
                {
                    groupChart.LastChanged = DateTime.Now;
                    groupChart.RunDate = DateTime.Now;
                }

                // set all of the data points
                var newPoints = BuildGroupedCompletions(jobID, chart.FilterValueType);
                GroupedCompletionChartDataPoint oldPoint = null;
                foreach (var gccdp in newPoints)
                {
                    if (groupChart.GroupedCompletionChartDataPoints != null)
                    {
                        oldPoint =
                            groupChart.GroupedCompletionChartDataPoints.FirstOrDefault(x => x.GroupKey == gccdp.GroupKey);
                    }
                    if (oldPoint == null)
                    {
                        oldPoint = gccdp;
                        oldPoint.GroupedCompletionChart = groupChart;
                        oldPoint.LastChanged = groupChart.RunDate;
                        Root.GetRepository<GroupedCompletionChartDataPoint>().Insert(oldPoint);
                    }
                    else
                    {
                        oldPoint.GroupTitle = gccdp.GroupTitle;
                        oldPoint.ItemCount = gccdp.ItemCount;
                        oldPoint.PercentComplete = gccdp.PercentComplete;
                        oldPoint.CompletedManHours = gccdp.CompletedManHours;
                        oldPoint.TotalManHours = gccdp.TotalManHours;
                        oldPoint.GroupKey = gccdp.GroupKey;
                        oldPoint.LastChanged = groupChart.RunDate;
                    }
                }
                Root.GetRepository<GroupedCompletionChartDataPoint>().Save();

                // Remove any detail items that were not updated.
                if (groupChart.GroupedCompletionChartDataPoints != null)
                {
                    var notchanged =
                        groupChart.GroupedCompletionChartDataPoints.Where(dp => dp.LastChanged < groupChart.RunDate);
                    foreach (var delItem in notchanged)
                    {
                        Root.GetRepository<GroupedCompletionChartDataPoint>().Delete(delItem.ID);
                    }
                    Root.GetRepository<GroupedCompletionChartDataPoint>().Save();
                }
            }
        }

        /// <summary>
        /// Updates History Chart information for the Job and ChartDefinition specified.
        /// </summary>
        /// <param name="jobID">ID of the Job to use.</param>
        /// <param name="chartDefinitionID">ID of the ChartDefinition to use.</param>
        public void UpdateHistoryChartData(long jobID, long chartDefinitionID, HistoryChartData instance = null)
        {
            if (instance != null && instance.ChartDefinitionID > 0 && instance.ChartDefinitionID != chartDefinitionID)
            {
                throw new ArgumentException(
                    "The history chart instance passes does not match the chartDefinitionID passed.");
            }
            ChartDefinition chart = Root.ChartDefinitionActions.Find(chartDefinitionID);
            if (chart == null)
                throw new ArgumentException("Attempted to generate a chart that does not exist.", "chartDefinitionID");
            if (chart.ChartType != ChartType.Historical)
                throw new ArgumentException(
                    string.Format("Attempted History Chart update of chart ID {0}, which is a {1} chart.",
                        chartDefinitionID, chart.ChartType.ToString()), "chartDefinitionID");

            // Since currently the chart information doesn't include filter parameters:
            var fParams = new FilterParameters();

            var fullLib = (TOPControlLibrary) Root;
            fullLib.UpdateAllProgressHistory();

            var chartUpdated = GetHistoryChartExpiration(jobID, chartDefinitionID);
            var progressUpdated = GetJobHistoryCheckpoint(jobID);

            if (progressUpdated >= chartUpdated || instance != null)
            {
                // Update the chart
                var groupingList = GetGroupKeyListByFilterValueType(jobID, fParams, chart.FilterValueType);
                if (!groupingList.Any() && chart.FilterValueType == FilterValueType.None)
                {
                    var templist = new List<object>();
                    templist.Add(null);
                    groupingList = templist;
                }
                foreach (var groupKey in groupingList)
                {
                    if (instance == null || (groupKey.IsNotNull() && instance.GroupKey == (groupKey.ToString())))
                    {
                        // TODO: If an instance is passed in, fill values and save that instance.
                        var newData =
                            (HistoryChartDataConcrete)
                                BuildHistoryChartData(jobID, fParams, chart.FilterValueType, groupKey);
                        HistoryChartData hchart;
                        if (instance == null)
                        {
                            hchart = Root.HistoryChartDataActions.GetByDefinition(jobID, fParams, chart.FilterValueType,
                                groupKey);
                        }
                        else
                        {
                            hchart = instance;
                        }

                        if (hchart == null)
                        {
                            hchart = newData.ToModel();
                            hchart.ChartDefinitionID = chartDefinitionID;
                            Root.GetRepository<HistoryChartData>().Insert(hchart);
                        }
                        else
                        {
                            hchart.ChartDefinitionID = chartDefinitionID;
                            hchart.CompleteCount = newData.CompleteCount;
                            hchart.ContextTitle = newData.ContextTitle;
                            hchart.GroupKey = newData.GroupKey;
                            hchart.GroupTitle = newData.GroupTitle;
                            hchart.ItemCount = newData.ItemCount;
                            hchart.PercentComplete = newData.PercentComplete;
                            hchart.CompletedManHours = newData.CompletedManHours;
                            hchart.TotalManHours = newData.TotalManHours;
                            hchart.RunDate = newData.RunDate;
                            hchart.LastChanged = newData.LastChanged;

                            // Update ChartValues and DataPoints

                            var oldDP = hchart.HistoryChartDataPoints;

                            foreach (var dp in newData.HistoryChartDataPoints)
                            {
                                var target = oldDP.FirstOrDefault(x => x.WorkDate == dp.WorkDate);
                                if (target == null)
                                {
                                    dp.HistoryChartData = hchart;
                                    hchart.HistoryChartDataPoints.Add(dp);
                                }
                                else
                                {
                                    // copy values to the extracted data point
                                    target.PercentComplete = dp.PercentComplete;
                                    target.CompletedManHours = dp.CompletedManHours;
                                    target.TotalManHours = dp.TotalManHours;
                                    target.LastChanged = DateTime.Now;
                                }
                            }
                        }
                        // Store the data
                        Root.GetRepository<HistoryChartData>().Save();
                    }
                }
            }
        }

        private bool CanUpdateChartValues(HistoryChartData instance, object groupKey)
        {
            return instance == null || (groupKey.IsNotNull() && instance.GroupKey == (groupKey.ToString()));
        }

        /// <summary>
        /// Builds the dataset needed to display a historical set of values for the filtered, grouped items.
        /// </summary>
        /// <param name="jobID"></param>
        /// <param name="parms"></param>
        /// <param name="grouping"></param>
        /// <param name="groupValue"></param>
        /// <returns></returns>
        public HistoryChartData GetHistoryChartData(long jobID, FilterParameters parms, FilterValueType grouping,
            object groupValue)
        {
            HistoryChartData result;
            // determine the ID of the chart.
            ChartDefinition chart = null;

            if (parms == null || parms.Count == 0)
            {
                chart = Root.ChartDefinitionActions.GetAll().FirstOrDefault(cd => cd.IsGlobal
                                                                                  &&
                                                                                  cd.FilterValueTypeValue ==
                                                                                  (int) grouping
                                                                                  &&
                                                                                  cd.ChartTypeValue ==
                                                                                  (int) ChartType.Historical);
            }
            if (chart == null)
            {
                result = BuildHistoryChartData(jobID, parms, grouping, groupValue);
            }
            else
            {
                var historyUpdated = GetHistoryChartExpiration(jobID, chart.ID);
                var progressUpdated = GetJobHistoryCheckpoint(jobID);

                result = Root.HistoryChartDataActions.GetByDefinition(jobID, parms, grouping, groupValue);

                if (result != null)
                {
                    if (progressUpdated > historyUpdated)
                    {
                        UpdateHistoryChartData(jobID, chart.ID, result);
                    }
                }
                else
                {
                    result = BuildHistoryChartData(jobID, parms, grouping, groupValue);
                    result.ChartDefinitionID = chart.ID;
                    result = ((HistoryChartDataConcrete) result).ToModel();
                    Root.GetRepository<HistoryChartData>().Insert(result);
                    Root.GetRepository<HistoryChartData>().Save();
                }
            }
            return result;
        }

        // The method below should be changed to be the "BuildHistoryChartData" and another method written to 
        // that checks to see if the stored data for the history chart is up-to-date and then "GetHistoryChartData
        // gets the data from the database (buy) or reconstructs it (build).

        /// <summary>
        /// Builds the dataset needed to display a historical set of values for the filtered, grouped items.
        /// </summary>
        /// <param name="jobID"></param>
        /// <param name="parms"></param>
        /// <param name="grouping"></param>
        /// <param name="groupValue"></param>
        /// <returns></returns>
        public HistoryChartData BuildHistoryChartData(long jobID, FilterParameters parms, FilterValueType grouping,
            object groupValue)
        {
            var result = new HistoryChartDataConcrete();

            var thisJob = Root.JobActions.Find(jobID);
            if (thisJob == null)
                throw new ArgumentException("Cannot build a chart for a job that does not exist.", "jobID");

            // Get the title value for an item in this group.

            if (parms == null) parms = new FilterParameters();

            SetGrouping(ref result, grouping, groupValue);
            result.JobID = jobID;
            result.GroupKey = (groupValue == null ? string.Empty : groupValue.ToString());
            result.GroupTitle = GetGroupTitle(result, grouping, groupValue);
            result.ContextTitle = GetContextTitle(result, grouping, groupValue);

            result.RunDate = DateTime.Now;

            var typeGrp = FilterParameters.GetRelatedType(grouping);
            var fullParms = parms.Clone();
            if (typeGrp.Name == "String") fullParms.AddFilterValue<string>(grouping, groupValue.ToString());
            if (typeGrp.Name == "long" || typeGrp.Name == "Int64")
                fullParms.AddFilterValue<long>(grouping, (long) groupValue);
            if (typeGrp.Name == "CompletionState")
                fullParms.AddFilterValue<CompletionState>(grouping, (CompletionState) groupValue);
            if (typeGrp.Name == "Object")
            {
                fullParms.AddFilterValue<object>(grouping, (groupValue ?? new object()));
            }

            var taskProgFiltered = Root.FilterTaskProgress(jobID, fullParms);

            result.ItemCount = taskProgFiltered.Count();
            result.CompleteCount = taskProgFiltered.Count(tp => tp.PercentComplete == 1m);

            result.CompletedManHours =
                taskProgFiltered.Sum(tp => (decimal?) tp.ManHoursPerItem * (decimal?) tp.PercentComplete) ?? 0;
            result.TotalManHours = taskProgFiltered.Sum(tp => (decimal?) tp.ManHoursPerItem) ?? 0;
            result.PercentComplete = Root.CalculateTotalPercentCompleteForFilter(jobID, fullParms);

            var phRep = Root.GetQueryable<ProgressHistory>();

            if (result.HistoryChartDataPoints == null)
            {
                result.HistoryChartDataPoints = new Collection<HistoryChartDataPoint>();
            }

            List<RawHistoryDataPoint> rawDataPoints = null;

            IQueryable<ProgressHistory> filtered;
            if (parms.Count == 0)
            {
                filtered = phRep.Where(ph => ph.JobID == jobID && !ph.TaskProgress.IsInDeletedScope);
                foreach (var parm in fullParms)
                {
                    switch (parm.Key)
                    {
                        case FilterValueType.None:
                            rawDataPoints = Root.SelectNewHistoryChartData(jobID: jobID);
                            break;
                        case FilterValueType.TrackingID:
                            rawDataPoints =
                                Root.SelectNewHistoryChartData(trackedItemID: ((IEnumerable<long>) parm.Value).First());
                            break;
                        case FilterValueType.ObjectType:
                            rawDataPoints =
                                Root.SelectNewHistoryChartData(objectTypeID: ((IEnumerable<long>) parm.Value).First());
                            break;
                        case FilterValueType.ProjectSystem:
                            rawDataPoints =
                                Root.SelectNewHistoryChartData(systemID: ((IEnumerable<long>) parm.Value).First());
                            break;
                        case FilterValueType.ProjectArea:
                            rawDataPoints =
                                Root.SelectNewHistoryChartData(areaID: ((IEnumerable<long>) parm.Value).First());
                            break;
                        case FilterValueType.CraftInfo:
                            rawDataPoints =
                                Root.SelectNewHistoryChartData(craftID: ((IEnumerable<long>) parm.Value).First());
                            break;
                        case FilterValueType.UserDefinedProperty1:
                            rawDataPoints =
                                Root.SelectNewHistoryChartData(udpValue1: ((IEnumerable<string>) parm.Value).First());
                            break;
                        case FilterValueType.UserDefinedProperty2:
                            rawDataPoints =
                                Root.SelectNewHistoryChartData(udpValue2: ((IEnumerable<string>) parm.Value).First());
                            break;
                        case FilterValueType.UserDefinedProperty3:
                            rawDataPoints =
                                Root.SelectNewHistoryChartData(udpValue3: ((IEnumerable<string>) parm.Value).First());
                            break;
                        case FilterValueType.Commodity:
                            rawDataPoints =
                                Root.SelectNewHistoryChartData(commodityID: ((IEnumerable<long>) parm.Value).First());
                            break;
                        case FilterValueType.WorkCrew:
                            break;
                        case FilterValueType.CompletionState:
                            break;
                        case FilterValueType.Phase:
                            rawDataPoints =
                                Root.SelectNewHistoryChartData(phaseID: ((IEnumerable<long>) parm.Value).First());
                            break;
                        case FilterValueType.Task:
                            rawDataPoints =
                                Root.SelectNewHistoryChartData(taskID: ((IEnumerable<long>) parm.Value).First());
                            break;
                        case FilterValueType.Search:
                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                filtered =
                    phRep.Where(
                        ph => ph.JobID == jobID && taskProgFiltered.Select(tp => tp.ID).Contains(ph.TaskProgressID));
            }

            if (rawDataPoints.Count > 0)
            {
                foreach (var dp in rawDataPoints)
                {
                    var newPoint = new HistoryChartDataPoint();
                    newPoint.HistoryChartDataID = result.ID;
                    newPoint.WorkDate = dp.WorkDate;
                    newPoint.CompletedManHours = dp.CompletedManHours;
                    newPoint.PercentComplete = dp.PercentComplete;
                    newPoint.TotalManHours = dp.TotalManHours;
                    result.HistoryChartDataPoints.Add(newPoint);
                }
            }
            else
            {
                var grouped = filtered.GroupBy(ph => ph.WorkDate);

                var progressValues = grouped.Select(pg => new
                {
                    WorkDate = pg.Key,
                    PercentComplete = pg.Average(ph => ph.PCTD),
                    MHRequired = pg.Sum(ph => ph.ManHours),
                    MHRequiredCurrent = pg.Sum(ph => ph.TaskProgress.ManHoursPerItem),
                    MHCompleted = pg.Sum(ph => ph.ManHours * ph.PCTD),
                    MHCompletedCurrent = pg.Sum(ph => ph.TaskProgress.ManHoursPerItem * ph.PCTD)
                });

                foreach (var pv in progressValues)
                {
                    var completeMh = (thisJob.HistoricalScopeCurve ? pv.MHCompleted : pv.MHCompletedCurrent);
                    var totalMh = (thisJob.HistoricalScopeCurve ? pv.MHRequired : pv.MHRequiredCurrent);

                    var hcdp = new HistoryChartDataPoint
                    {
                        HistoryChartData = result,
                        WorkDate = pv.WorkDate,
                        PercentComplete = (!thisJob.WeightedCalculations || totalMh == 0
                            ? pv.PercentComplete
                            : completeMh / totalMh),
                        CompletedManHours = completeMh,
                        TotalManHours = totalMh,
                        LastChanged = result.RunDate
                    };
                    result.HistoryChartDataPoints.Add(hcdp);
                }
            }
            return (HistoryChartData) result;
        }

        private string GetContextTitle(HistoryChartDataConcrete target, FilterValueType grouping, object groupValue)
        {
            if (grouping == FilterValueType.Task)
            {
                target = ensureGrouping(target, grouping, groupValue);
                return target.CastGroupingObject<Task>().ProjectPhase.Title;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Sets a value on which to group results.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="grouping"></param>
        /// <param name="groupValue"></param>
        protected void SetGrouping(ref HistoryChartDataConcrete target, FilterValueType grouping, object groupValue)
        {
            target.GroupingObject = null;
            target.GroupingValue = null;

            switch (grouping)
            {
                case FilterValueType.Commodity:
                    target.GroupingObject = Root.CommodityActions.Find((long) groupValue);
                    break;
                case FilterValueType.CraftInfo:
                    target.GroupingObject = Root.CraftActions.Find((long) groupValue);
                    break;
                case FilterValueType.ObjectType:
                    target.GroupingObject = Root.ObjectTypeActions.Find((long) groupValue);
                    break;
                case FilterValueType.Phase:
                    target.GroupingObject = Root.PhaseActions.Find((long) groupValue);
                    break;
                case FilterValueType.ProjectArea:
                    target.GroupingObject = Root.AreaActions.Find((long) groupValue);
                    break;
                case FilterValueType.ProjectSystem:
                    target.GroupingObject = Root.SystemActions.Find((long) groupValue);
                    break;
                case FilterValueType.Task:
                    target.GroupingObject = Root.TaskActions.Find((long) groupValue);
                    break;
                case FilterValueType.WorkCrew:
                    target.GroupingObject = Root.WorkCrewActions.Find((long) groupValue);
                    break;
                case FilterValueType.None:
                    target.GroupingValue = "All";
                    break;
                default:
                    target.GroupingValue = groupValue.ToString();
                    break;
            }
        }

        /// <summary>
        /// Retreives the Title for  a specific grouping item.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="grouping"></param>
        /// <param name="groupValue"></param>
        /// <returns></returns>
        protected string GetGroupTitle(HistoryChartDataConcrete target, FilterValueType grouping, object groupValue)
        {
            string result = "";
            // Some items can be determined by simply being a string or enumeration.
            // Some will need to be retrieved.

            target = ensureGrouping(target, grouping, groupValue);

            switch (grouping)
            {
                case FilterValueType.Commodity:
                    result = target.CastGroupingObject<Commodity>().CommodityCode;
                    break;
                case FilterValueType.CraftInfo:
                    result = target.CastGroupingObject<CraftInfo>().CraftCode;
                    break;
                case FilterValueType.ObjectType:
                    result = target.CastGroupingObject<ObjectType>().Name;
                    break;
                case FilterValueType.Phase:
                    result = target.CastGroupingObject<ProjectPhase>().Title;
                    break;
                case FilterValueType.ProjectArea:
                    result = target.CastGroupingObject<ProjectArea>().AreaNumber;
                    break;
                case FilterValueType.ProjectSystem:
                    result = target.CastGroupingObject<ProjectSystem>().SystemNumber;
                    break;
                case FilterValueType.Task:
                    result = target.CastGroupingObject<Task>().Title;
                    break;
                case FilterValueType.WorkCrew:
                    var crewInfo = target.CastGroupingObject<WorkCrew>();
                    result = string.Format("Crew {0} - {1}", crewInfo.CrewNumber, crewInfo.Description);
                    break;
                case FilterValueType.None:
                    result = "All";
                    break;
                    //case FilterValueType.CompletionState:
                    //    break;
                    //case FilterValueType.Search:
                    //    break;
                    //case FilterValueType.TrackingID:
                    //    break;
                    //case FilterValueType.UserDefinedProperty1:
                    //    break;
                    //case FilterValueType.UserDefinedProperty2:
                    //    break;
                    //case FilterValueType.UserDefinedProperty3:
                    //    break;
                default:
                    result = target.GroupingValue;
                    break;
            }

            return result;
        }

        private HistoryChartDataConcrete ensureGrouping(HistoryChartDataConcrete target, FilterValueType grouping,
            object groupValue)
        {
            if (target.GroupingObject == null && target.GroupingValue == null)
            {
                SetGrouping(ref target, grouping, groupValue);
            }
            return target;
        }

        /// <summary>
        /// Returns a set of data points representing the grouped completion data requested.  
        /// </summary>
        /// <param name="jobID">The ID of the Job whose status the data points should reflect.</param>
        /// <param name="grouping">A value from the FilterValueType enumeration, indicating a kind of value to use for grouping.</param>
        /// <returns></returns>
        internal IEnumerable<GroupedCompletionChartDataPoint> GetGroupedCompletions(long jobID, FilterValueType grouping)
        {
            IEnumerable<GroupedCompletionChartDataPoint> result;

            // Check for a matching chart definition.
            var cd = Root.ChartDefinitionActions.GetAll()
                .FirstOrDefault(x => x.ChartTypeValue == (int) ChartType.GroupedCompletion
                                     && x.FilterValueTypeValue == (int) grouping
                                     && x.IsGlobal);

            GroupedCompletionChart groupChart = null; // default the chart data to null
            if (cd != null)
            {
                // If the chart data seems to exist, use Update...ChartData to make it up-to-date.
                UpdateGroupedCompletionChartData(jobID, cd.ID);

                groupChart = Root.GroupedCompletionChartActions.GetByDefinition(jobID, grouping);
            }

            if (groupChart == null)
            {
                // the chart data is not being stored OR there is no Cached storage for this kind of chart.
                // build and return the dataset.
                result = BuildGroupedCompletions(jobID, grouping);
            }
            else
            {
                result = groupChart.GroupedCompletionChartDataPoints.ToList();
            }
            return result;
        }

        /// <summary>
        /// Uses the raw data in the database to define a set of GroupedCompletionChartDataPoints.  Does not check for 
        /// any cached version of the data, so performance may be inferior in an environment in which the data has not changed.
        /// </summary>
        /// <param name="jobID">The ID of the Job for which to build the data.</param>
        /// <param name="grouping">A value from the FilterValueType enumeration indicating a property value to group on.</param>
        /// <returns></returns>
        internal IEnumerable<GroupedCompletionChartDataPoint> BuildGroupedCompletions(long jobID,
            FilterValueType grouping)
        {
            var selectType = grouping;

            switch (selectType)
            {
                case FilterValueType.Commodity:
                    return GetGroupedCompletions<Commodity>(jobID);
                case FilterValueType.CompletionState:
                    throw new NotImplementedException();
                case FilterValueType.CraftInfo:
                    return GetGroupedCompletions<CraftInfo>(jobID);
                case FilterValueType.ObjectType:
                    return GetGroupedCompletions<ObjectType>(jobID);
                case FilterValueType.Phase:
                    return GetGroupedCompletions<ProjectPhase>(jobID);
                case FilterValueType.ProjectArea:
                    return GetGroupedCompletions<ProjectArea>(jobID);
                case FilterValueType.ProjectSystem:
                    return GetGroupedCompletions<ProjectSystem>(jobID);
                case FilterValueType.Task:
                    return GetGroupedCompletions<Task>(jobID);
                case FilterValueType.TrackingID:
                    return GetGroupedCompletions<TrackedItem>(jobID);
                case FilterValueType.UserDefinedProperty1:
                    return GetGroupedUDPCompletions(jobID, 1);
                case FilterValueType.UserDefinedProperty2:
                    return GetGroupedUDPCompletions(jobID, 2);
                case FilterValueType.UserDefinedProperty3:
                    return GetGroupedUDPCompletions(jobID, 3);
                case FilterValueType.None:
                    return GetGroupedCompletions<JobInfo>(jobID);
                case FilterValueType.WorkCrew:
                    throw new NotImplementedException();
                default:
                    throw new NotImplementedException();
            }
        }

        //internal IEnumerable<NamedPercentage> GetGroupedCompletions<DObj>(long jobID)
        //    where DObj:DomainObject,new()
        //{
        //    var parms = new FilterParameters();
        //    var tpAll = Root.FilterTaskProgress(jobID, parms);
        //    var grouped = GetGroupedValues<DObj>(tpAll);
        //    var rawResult = grouped.Select(tg =>
        //                        new 
        //                            {
        //                                DomainObject = (DObj)tg.Key,
        //                                PercentComplete = tg.Average(tp => tp.PercentComplete)
        //                            }).ToArray();
        //    var namedResult = new List<NamedPercentage>();
        //    foreach(var ao in rawResult)
        //    {
        //        var newItem = new NamedPercentage();
        //        newItem.NameOrTitle = (ao.DomainObject == null ? "None" : ao.DomainObject.DisplayName);
        //        newItem.PercentComplete = ao.PercentComplete;
        //        namedResult.Add(newItem);
        //    }
        //    return namedResult.OrderBy(np => np.NameOrTitle);
        //}

        internal IEnumerable<GroupedCompletionChartDataPoint> GetGroupedCompletions<DObj>(long jobID)
            where DObj : DomainObject, new()
        {
            var result = new List<GroupedCompletionChartDataPoint>();
            var parms = new FilterParameters();
            var tpAll = Root.FilterTaskProgress(jobID, parms);
            var job = Root.JobActions.Find(jobID);

            if (typeof(DObj) == typeof(JobInfo))
            {
                var jobPct = Root.CalculateTotalPercentCompleteForFilter(jobID, parms);
                var mhAll = tpAll.Sum(x => x.ManHoursPerItem);
                var mhComp = tpAll.Sum(x => x.ManHoursPerItem * x.PercentComplete);
                var jobItems = tpAll.Count();

                result.Add(new GroupedCompletionChartDataPoint
                {
                    GroupKey = string.Empty,
                    GroupTitle = "All",
                    ItemCount = jobItems,
                    PercentComplete = jobPct,
                    TotalManHours = mhAll,
                    CompletedManHours = mhComp
                });
            }
            else
            {
                var grouped = GetGroupedValues<DObj>(tpAll);
                var nameResult = grouped.Select(tg =>
                    new
                    {
                        TargetObject = (DObj) tg.Key,
                        ItemCount = tg.Count(),
                        PercentComplete = tg.Average(tp => tp.PercentComplete),
                        MHRequired = tg.Sum(tp => tp.ManHoursPerItem),
                        MHComplete = tg.Sum(tp => tp.ManHoursPerItem * tp.PercentComplete)
                    }).ToList();
                foreach (var anObj in nameResult)
                {
                    var grpDPnt = new GroupedCompletionChartDataPoint
                    {
                        ItemCount = anObj.ItemCount,
                        PercentComplete = (!job.WeightedCalculations || anObj.MHRequired == 0
                            ? anObj.PercentComplete
                            : (anObj.MHComplete / anObj.MHRequired)),
                        TotalManHours = anObj.MHRequired,
                        CompletedManHours = anObj.MHComplete
                    };
                    if (anObj.TargetObject != null)
                    {
                        grpDPnt.GroupKey = anObj.TargetObject.ID.ToString();
                        grpDPnt.GroupTitle = anObj.TargetObject.DisplayName;
                    }
                    else if (anObj.ItemCount > 0)
                    {
                        grpDPnt.GroupKey = string.Empty;
                        grpDPnt.GroupTitle = @"N/A";
                    }

                    result.Add(grpDPnt);
                }
            }
            return result.OrderBy(x => x.GroupTitle).ToList();
        }

        private static IQueryable<IGrouping<DObj, TaskProgress>> GetGroupedValues<DObj>(
            IQueryable<TaskProgress> filtered)
            where DObj : DomainObject, new()
        {
            IQueryable<IGrouping<DObj, TaskProgress>> result = null;

            var ttype = typeof(DObj);

            switch (ttype.Name)
            {
                case "ProjectSystem":
                    result = (IQueryable<IGrouping<DObj, TaskProgress>>) from tp in filtered
                        group tp by tp.TrackedItem.ProjectSystem
                        into tgroup
                        select tgroup;
                    break;
                case "ProjectArea":
                    result = (IQueryable<IGrouping<DObj, TaskProgress>>) from tp in filtered
                        group tp by tp.TrackedItem.ProjectArea
                        into tgroup
                        select tgroup;
                    break;
                case "ProjectPhase":
                    result = (IQueryable<IGrouping<DObj, TaskProgress>>) from tp in filtered
                        group tp by tp.Task.ProjectPhase
                        into tgroup
                        select tgroup;
                    break;
                case "Task":
                    result = (IQueryable<IGrouping<DObj, TaskProgress>>) from tp in filtered
                        group tp by tp.Task
                        into tgroup
                        select tgroup;
                    break;
                case "CraftInfo":
                    result = (IQueryable<IGrouping<DObj, TaskProgress>>) from tp in filtered
                        group tp by tp.TrackedItem.CraftInfo
                        into tgroup
                        select tgroup;
                    break;
                case "Commodity":
                    result = (IQueryable<IGrouping<DObj, TaskProgress>>) from tp in filtered
                        group tp by tp.Commodity
                        into tgroup
                        select tgroup;
                    break;
                case "ObjectType":
                    result = (IQueryable<IGrouping<DObj, TaskProgress>>) from tp in filtered
                        group tp by tp.TrackedItem.ObjectType
                        into tgroup
                        select tgroup;
                    break;
                case "TrackedItem":
                    result = (IQueryable<IGrouping<DObj, TaskProgress>>) from tp in filtered
                        group tp by tp.TrackedItem
                        into tgroup
                        select tgroup;
                    break;
                default:
                    result = null;
                    break;
            }

            return result;
        }

        //internal IEnumerable<NamedPercentage> GetGroupedUDPCompletions(long jobID, int index)
        //{
        //    var parms = new FilterParameters();
        //    var tpAll = Root.FilterTaskProgress(jobID, parms);
        //    IQueryable<IGrouping<string, TaskProgress>> grouped;
        //    switch (index)
        //    {
        //        case 1:
        //            grouped = tpAll.GroupBy(tp => tp.TrackedItem.UDPValue1);
        //            break;
        //        case 2:
        //            grouped = tpAll.GroupBy(tp => tp.TrackedItem.UDPValue2);
        //            break;
        //        case 3:
        //            grouped = tpAll.GroupBy(tp => tp.TrackedItem.UDPValue3);
        //            break;
        //        default:
        //            throw new NotImplementedException();
        //    }

        //    var rawResult = grouped.Select(tg =>
        //                        new NamedPercentage
        //                        {
        //                            NameOrTitle = tg.Key,
        //                            PercentComplete = tg.Average(tp => tp.PercentComplete)
        //                        });
        //    return rawResult.OrderBy(np => np.NameOrTitle).ToList();
        //}

        internal IEnumerable<GroupedCompletionChartDataPoint> GetGroupedUDPCompletions(long jobID, int index)
        {
            var result = new List<GroupedCompletionChartDataPoint>();
            var parms = new FilterParameters();
            var job = Root.JobActions.Find(jobID);
            var tpAll = Root.FilterTaskProgress(jobID, parms);
            IQueryable<IGrouping<string, TaskProgress>> grouped;
            switch (index)
            {
                case 1:
                    grouped = tpAll.GroupBy(tp => tp.TrackedItem.UDPValue1);
                    break;
                case 2:
                    grouped = tpAll.GroupBy(tp => tp.TrackedItem.UDPValue2);
                    break;
                case 3:
                    grouped = tpAll.GroupBy(tp => tp.TrackedItem.UDPValue3);
                    break;
                default:
                    throw new NotImplementedException();
            }

            var rawResult = grouped.Select(tg =>
                new
                {
                    GroupKey = tg.Key,
                    ItemCount = tg.Count(),
                    PercentComplete = tg.Average(tp => tp.PercentComplete),
                    MHRequired = tg.Sum(tp => tp.ManHoursPerItem),
                    MHComplete = tg.Sum(tp => tp.ManHoursPerItem * tp.PercentComplete)
                });
            foreach (var item in rawResult.OrderBy(gccdp => gccdp.GroupKey))
            {
                result.Add(new GroupedCompletionChartDataPoint
                {
                    GroupKey = item.GroupKey,
                    GroupTitle = item.GroupKey,
                    ItemCount = item.ItemCount,
                    PercentComplete =
                        (!job.WeightedCalculations || item.MHRequired == 0
                            ? item.PercentComplete
                            : (item.MHComplete / item.MHRequired)),
                    TotalManHours = item.MHRequired,
                    CompletedManHours = item.MHComplete
                });
            }

            return result;
        }

        internal IEnumerable<PhaseSummary> GetPhaseSummaries(long jobID)
        {
            var parms = new FilterParameters();

            var job = Root.JobActions.Find(jobID);

            var tpAll = Root.FilterTaskProgress(jobID, parms);
            var tpGroups = GetGroupedValues<ProjectPhase>(tpAll);
            var rawResult = tpGroups.Select(phGroup => new PhaseSummary
            {
                PhaseID = phGroup.Key.ID,
                PercentComplete =
                    (!job.WeightedCalculations || phGroup.Sum(tpr => tpr.ManHoursPerItem) == 0
                        ? phGroup.Average(tp => tp.PercentComplete)
                        : (phGroup.Sum(tp => tp.ManHoursPerItem * tp.PercentComplete) /
                           (phGroup.Sum(tp1 => tp1.ManHoursPerItem)))),
                DisplayOrder = phGroup.Key.DisplayOrder,

                IncompleteTaskCount = phGroup.Where(
                                tp1 => !tp1.IsInDeletedScope 
                                    && tp1.PercentComplete < (decimal)1.00)
                                    .Select(tp2 => tp2.TaskID).Distinct().Count(),
                
                TaskCount = phGroup.Where(tp1 => !tp1.IsInDeletedScope).Select(tp2 => tp2.TaskID).Distinct().Count(),
                TotalManHours = phGroup.Sum(tp => tp.ManHoursPerItem),
                CompletedManHours = phGroup.Sum(tp => tp.ManHoursPerItem * tp.PercentComplete),
                Title = phGroup.Key.Title
            });

            return rawResult.OrderBy(ps => ps.DisplayOrder).ToList();
        }

        /// <summary>
        /// Retrieves a list of the key values (regardless of data type) for a specified grouping, job, and FlterParameters.
        /// </summary>
        /// <param name="jobID">ID of the job to check</param>
        /// <param name="parms">The Parameters to use to filter.</param>
        /// <param name="grouping">A value indicating the data item to group on.</param>
        /// <returns></returns>
        public IEnumerable<object> GetGroupKeyListByFilterValueType(long jobID, FilterParameters parms,
            FilterValueType grouping)
        {
            IEnumerable<object> result = new List<object>();
            var fParams = (parms == null ? new FilterParameters() : parms);
            switch (grouping)
            {
                case FilterValueType.Commodity:
                    result = Root.GetCommoditiesForFilter(jobID, fParams).Select(x => (object) x.ID);
                    break;
                case FilterValueType.CraftInfo:
                    result = Root.GetCraftInfoValuesForFilter(jobID, fParams).Select(x => (object) x.ID);
                    break;
                case FilterValueType.ObjectType:
                    result = Root.GetObjectTypesForFilter(jobID, fParams).Select(x => (object) x.ID);
                    break;
                case FilterValueType.Phase:
                    result = Root.PhaseActions.AllForJob(jobID).Select(x => (object) x.ID);
                    break;
                case FilterValueType.ProjectArea:
                    result = Root.GetProjectAreasForFilter(jobID, fParams).Select(x => (object) x.ID);
                    break;
                case FilterValueType.ProjectSystem:
                    result = Root.GetProjectSystemsForFilter(jobID, fParams).Select(x => (object) x.ID);
                    break;
                case FilterValueType.Task:
                    result = Root.GetTasksForFilter(jobID, fParams).Select(x => (object) x.ID);
                    break;
                case FilterValueType.TrackingID:
                    result = Root.FilterTaskProgress(jobID, fParams)
                        .Select(x => x.TrackedItem.TrackingID)
                        .Distinct()
                        .Cast<object>().ToList();
                    break;
                case FilterValueType.UserDefinedProperty1:
                    result = Root.GetUserDefinedVals_1_ForFilter(jobID, fParams).Cast<object>();
                    break;
                case FilterValueType.UserDefinedProperty2:
                    result = Root.GetUserDefinedVals_2_ForFilter(jobID, fParams).Cast<object>();
                    break;
                case FilterValueType.UserDefinedProperty3:
                    result = Root.GetUserDefinedVals_3_ForFilter(jobID, fParams).Cast<object>();
                    break;
                case FilterValueType.WorkCrew:
                    result = Root.GetWorkCrewsForFilter(jobID, fParams).Select(x => (object) x.ID);
                    break;
                default:
                    break;
            }

            return result;
        }
    }
}