﻿using System.Collections.Generic;
using System.Linq;
using Envoc.Core.Shared.Extensions;
using Lib.TOPControls.Charting;
using Lib.TOPControls.Exceptions;
using MMRCommon.Data;

namespace Lib.TOPControls.Services
{
    /// <summary>
    /// Provides operations specific to Project Systems
    /// </summary>
    public class ProjectSystemService
    {
        private readonly TOPControlLibrary service;

        /// <summary>
        /// Creates an instance of ProjectSystemService with the supplied resources.
        /// </summary>
        /// <param name="service">The CRUD service</param>
        public ProjectSystemService(TOPControlLibrary service)
        {
            this.service = service;
        }

        public IEnumerable<ProjectSystem> SelectAll(long jobId)
        {
            return service.SystemActions.AllForJob(jobId);
        }

        /// <summary>
        /// Changes the parent and/or sort order of a given Project System relative to its siblings
        /// </summary>
        /// <param name="item">The Project System instance to change</param>
        /// <param name="parentId">The new parent ID for the node</param>
        /// <param name="newPosition">The new position to move the Project System to relative to its siblings</param>
        /// <param name="currentUserId">The unique identifier of the user making the request</param>
        public ValidationResult ChangePositionInTree(ProjectSystem item, long? parentId, int newPosition,
            long currentUserId)
        {
            //validate the potential new siblings before making the change
            var items = GetSiblings(item.JobID, parentId);

            var dups =
                items.Count(
                    ps =>
                        ps.ID != item.ID && ps.JobID == item.JobID && ps.ParentId == parentId &&
                        ps.SystemNumber == item.SystemNumber);
            if (dups > 0)
            {
                var result = new ValidationResult();
                result.AddViolation(new RuleViolation("There is already a System with that system number.",
                    "SystemNumber"));
                return result;
            }

            ReassignParent(item, parentId, currentUserId);

            items = GetSiblings(item.JobID, parentId);

            items.ChangeSortOrder(item, newPosition);

            foreach (var updated in items)
            {
                service.SystemActions.Update(updated, currentUserId);
            }

            return new ValidationResult();
        }

        /// <summary>
        /// Changes the parent of a given Project System
        /// </summary>
        /// <param name="item">The Project System instance to change</param>
        /// <param name="parentId">The new parent ID for the node</param>
        /// <param name="currentUserId">The unique identifier of the user making the request</param>
        private void ReassignParent(ProjectSystem item, long? parentId, long currentUserId)
        {
            if (ParentHasChanged(item, parentId))
            {
                var siblings = GetSiblings(item.JobID, parentId);
                siblings.Remove(item);
                item.ParentId = parentId;
                service.SystemActions.Update(item, currentUserId);

                RenumberSortOrder(siblings, currentUserId);
            }
        }

        /// <summary>
        /// Inserts a new Project System into the data store
        /// </summary>
        /// <param name="item">The Project System instance to add</param>
        /// <param name="currentUserId">The unique identifier of the user making the request</param>
        public void Add(ProjectSystem item, long currentUserId)
        {
            item.SortOrder = -1;
            try
            {
                service.SystemActions.Add(item, currentUserId);
            }
            catch (ValidationFailureException ex)
            {
                throw;
            }

            UpdateSiblings(item, currentUserId);
        }

        /// <summary>
        /// Removes a Project System from the data store
        /// </summary>
        /// <param name="item">The Project System instance to remove</param>
        /// <param name="currentUserId">The unique identifier of the user making the request</param>
        public ValidationResult Delete(ProjectSystem item, long currentUserId)
        {
            var validationResult = service.SystemActions.ValidateDelete(item.ID);

            if (!validationResult.IsValid)
            {
                return validationResult;
            }

            service.SystemActions.Delete(item.ID, currentUserId);
            UpdateSiblings(item, currentUserId);

            return new ValidationResult();
        }

        /// <summary>
        /// Recursively merges the tracked items of one system into another.
        /// </summary>
        /// <param name="jobId">The job identifier.</param>
        /// <param name="sourceId">The identifier of the source system.</param>
        /// <param name="destinationId">The identifier of the destination system.</param>
        /// <param name="currentUserId">The unique identifier of the user making the request</param>
        public void Merge(long jobId, long sourceId, long destinationId, long currentUserId)
        {
            ResetParentId(jobId, sourceId, destinationId, currentUserId);
            MergeChildren(jobId, sourceId, destinationId, currentUserId);
        }

        private void ResetParentId(long jobId, long sourceId, long destinationId, long currentUserId)
        {
            var system = service.SystemActions.GetSystemBranch(jobId, sourceId);

            foreach (var child in system.Children.ToList())
            {
                child.ParentId = destinationId;
                service.SystemActions.Update(child, currentUserId);
            }
        }

        private void MergeChildren(long jobId, long sourceId, long destinationId, long currentUserId)
        {
            var system = service.SystemActions.GetSystemBranch(jobId, sourceId);
            var trackedItems = service.TrackedItemActions.FindBySystemID(jobId, system.ID).ToList();

            foreach (var trackedItem in trackedItems)
            {
                trackedItem.SystemID = destinationId;
                service.TrackedItemActions.Update(trackedItem, currentUserId);
            }

            service.SystemActions.Delete(system.ID, currentUserId);
        }

        /// <summary>
        /// Builds the Project System Progress tree.
        /// </summary>
        /// <param name="jobId">The job identifier.</param>
        /// <param name="phaseId">The phase identifier.</param>
        /// <returns></returns>
        public IEnumerable<ProjectSystemProgress> BuildProjectSystemProgressTree(long jobId, long phaseId)
        {
            var taskProgressItems = GetTaskProgressBySystemAndPhase(jobId, phaseId);

            //get the built out systems tree and convert to ProjectSystemProgress
            var projectSystemsTree = service.SystemActions.GetTreeByJobId(jobId);
            var projectSystemsProgressTree =
                projectSystemsTree.Select(item => ConvertProjectSystem(item)).ToList();

            //build out the project systems tree with the filtered data task progress items
            PopulateProjectSystemProgressTree(projectSystemsProgressTree, taskProgressItems);

            return projectSystemsProgressTree;
        }

        private IEnumerable<TaskProgress> GetTaskProgressBySystemAndPhase(long jobId, long phaseId)
        {
            var projectSystemIds = service.SystemActions.AllForJob(jobId).Select(x => x.ID);

            //get all of the task progress items filtered by the system ids
            var taskProgressItems =
                service.GetQueryable<TaskProgress>()
                    .Where(
                        x =>
                            projectSystemIds.Contains(x.TrackedItem.SystemID.Value) &&
                            x.Task.PhaseID == phaseId)
                    .OrderBy(x => x.Task.DisplayOrder)
                    .ThenByDescending(x => x.PercentComplete)
                    .ToList();
            return taskProgressItems;
        }

        private ProjectSystemProgress ConvertProjectSystem(ProjectSystem projectSystem)
        {
            var projectSystemProgress = new ProjectSystemProgress
            {
                ID = projectSystem.ID,
                Name = projectSystem.SystemNumber,
                IsSystem = true
            };

            projectSystemProgress.AddChildren(projectSystem.Children.Select(c => ConvertProjectSystem(c)));

            return projectSystemProgress;
        }

        private void PopulateProjectSystemProgressTree(IEnumerable<ProjectSystemProgress> projectSystems,
            IEnumerable<TaskProgress> taskProgressItems)
        {
            foreach (var projectSystemProgress in projectSystems)
            {
                var children =
                    taskProgressItems.Where(x => x.TrackedItem.SystemID == projectSystemProgress.ID && !x.IsDeleted)
                        .Select(c => ConvertTaskProgress(c, projectSystemProgress));

                projectSystemProgress.AddChildren(children);

                //we only want to find the project system children to add tracked item progress to
                PopulateProjectSystemProgressTree(projectSystemProgress.Children.Where(x => x.IsSystem),
                    taskProgressItems);
            }
        }

        public ProjectSystemProgress ConvertTaskProgress(TaskProgress taskProgress, ProjectSystemProgress parent)
        {
            var projectSystemProgress = new ProjectSystemProgress
            {
                ID = taskProgress.TrackedItemID,
                Name = taskProgress.DisplayName,
                PercentComplete = taskProgress.PercentComplete,
                CompletedManhours = (taskProgress.ManHoursPerItem * taskProgress.PercentComplete),
                TotalManhours = taskProgress.ManHoursPerItem,
                IsSystem = false,
                Parent = parent,
                Comments = taskProgress.TrackedItem.Comments
            };

            return projectSystemProgress;
        }

        private bool ParentHasChanged(ProjectSystem item, long? parentId)
        {
            if (!parentId.HasValue && !item.ParentId.HasValue)
            {
                return false;
            }

            if (parentId.HasValue && !item.ParentId.HasValue)
            {
                return true;
            }

            if (!parentId.HasValue && item.ParentId.HasValue)
            {
                return true;
            }

            if (item.ParentId != parentId)
            {
                return true;
            }

            return false;
        }

        private IList<ProjectSystem> GetSiblings(long jobId, long? parentId)
        {
            var siblings =
                service.SystemActions.FindByParentId(jobId, parentId);

            return siblings.OrderBy(x => x.SortOrder).ToList();
        }

        private void UpdateSiblings(ProjectSystem item, long currentUserId)
        {
            var siblings = GetSiblings(item.JobID, item.ParentId);
            RenumberSortOrder(siblings.OrderBy(x => x.SortOrder), currentUserId);
        }

        private void RenumberSortOrder(IEnumerable<ProjectSystem> items, long currentUserId)
        {
            var sortOrder = 0;

            foreach (var item in items)
            {
                item.SortOrder = sortOrder;

                service.SystemActions.Update(item, currentUserId);

                sortOrder++;
            }
        }
    }
}