﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMRCommon.Data;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Interfaces.DataAccess;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;
using Lib.TOPControls.Security;

namespace Lib.TOPControls
{
    /// <summary>
    /// Implements functions for managing TOPUser values.
    /// </summary>
    public class TOPUserLibrary : SoftDeleteLibraryBase<TOPUser>, ITOPUserLibrary
    {

        #region Constructors
        /// <summary>
        /// Creates an instance of TOPUserLibrary
        /// </summary>
        public TOPUserLibrary()
            : base()
        { }

        /// <summary>
        /// Creates an instance of TOPUserLibrary using the root library passed.
        /// </summary>
        /// <param name="rootLibrary">A fully instantiated ITOPControlLibrary.</param>
        public TOPUserLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }

        /// <summary>
        /// Creates an instance of TOPUserLibrary with the supplied resources.
        /// </summary>
        /// <param name="useTOPUserRepository">The Repository to use.</param>
        /// <param name="permissionManager">The Permission manager to use.</param>
        public TOPUserLibrary(IRepository<TOPUser> useTOPUserRepository = null, IPermissionManager permissionManager = null)
            : base(useTOPUserRepository, permissionManager)
        { }


        

        #endregion

        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public override ValidationResult Validate(TOPUser item)
        {
            var result = base.Validate(item);

            if (string.IsNullOrWhiteSpace(item.Login))
            {
                result.AddViolation("The user cannot be added to the system without a login name.", "Login");
            }
            else
            {
                var lViolation = result.ValidateStringLength(item.Login, 1, 50, "Login", "{0} cannot be longer than {2} characters.");
                if (lViolation == null)
                {
                    var dupLogins = Repository.All.Count(tu => tu.ID != item.ID && tu.Login == item.Login && !tu.IsDeleted);
                    if (dupLogins > 0) result.AddViolation("There is already a user with that username in the database.", "Login");
                }
            }

            result.ValidateStringLength(item.FirstName, 1, 50, "FirstName");
            result.ValidateStringLength(item.LastName, 1, 50, "LastName");

            if (item.DefaultJobID.HasValue)
            {
                if (item.UserJobs != null && item.UserJobs.Count > 0)
                {
                    if (item.UserJobs.Count(uj => uj.JobID == item.DefaultJobID.Value) == 0)
                    {
                        result.AddViolation("The default job being set on the user is not one of the jobs they have access to.", "DefaultJobID");
                    }
                }
            }

            return result;
        }





        #region ITOPUserLibrary Members
        /// <summary>
        /// Returns a TOPUser with a Login that is a match for the username passed. Returns a null if a 
        /// matching value does not exist or if the matching item is marked as deleted.  
        /// </summary>
        /// <param name="username">A string value to match to the Login value of each record.</param>
        /// <returns></returns>
        public TOPUser FindByUsername(string username)
        {
            return Repository.All.SingleOrDefault(tu => tu.Login == username && !tu.IsDeleted);
        }

        /// <summary>
        /// Returns a set of TOPUser objects that match the searchTerm passed.  If the searchTerm 
        /// is an empty string, returns all TOPUser items. Does not return TOPUser items that are 
        /// marked as deleted.
        /// </summary>
        /// <param name="searchTerm">A string value to search for.</param>
        /// <returns></returns>
        public IEnumerable<TOPUser> Search(string searchTerm)
        {
            return SearchAsQueryable(searchTerm).Where(tu => !tu.IsDeleted).ToList();                                          
        }

        /// <summary>
        /// Performs that same operation as Search(), but includes TOPUser items that are marked as deleted.  
        /// </summary>
        /// <param name="searchTerm">A string value to search for.</param>
        /// <param name="userID">The ID of the TOPUser performing this search.  If the user referenced by 
        /// userID does not have rights to view deleted TOPUsers, the operation will fail.</param>
        /// <returns></returns>
        public IEnumerable<TOPUser> SearchAny(string searchTerm, long userID)
        {
            VerifyUserPermission(UserActionType.ViewDeleted, new TOPUser(), userID);
            return SearchAsQueryable(searchTerm).ToList();
        }

        /// <summary>
        /// Updates a user preference value with the preferencesText passed. The TOPUser to be updated 
        /// must have an ID that matches the userID passed.
        /// </summary>
        /// <param name="userID">The ID of the TOPUser record to change.</param>
        /// <param name="preferencesText">An XML string that can replace or update the user's prefrences
        /// value.</param>
        public void SetUserPreferences(long userID, string preferencesText)
        {
            var item = Find(userID);
            item.Preferences = preferencesText;
            Repository.Save();
        }


        /// <summary>
        /// Updates the role set on the current user to the role matching the ID passed.  Replaces an 
        /// existing role if it exists, and otherwise, sets the user to have an associated role.
        /// </summary>
        /// <param name="targetUserID">The ID of the TOPUser record to which the role should be associated.</param>
        /// <param name="roleID">The ID of the Role the user should be associated with.</param>
        /// <param name="currentUserID">The ID of the user performing this operation</param>
        public void SetUserRole(long targetUserID, long roleID, long currentUserID)
        {
            var repUR = Root.GetRepository<UserRole>();
            var repRole = Root.GetRepository<Role>();

            var newUR = new UserRole
            {
                UserID = targetUserID,
                RoleID = roleID,
                LastChanged = DateTime.Now
            };

            if (!VerifyUserRolePermission(UserActionType.Update, newUR, currentUserID))
            {
                throw new ApplicationException("User level is too low to set that role.");
            }

            var role = repRole.Find(roleID);
            var urList = repUR.All.Where(ur => ur.UserID == targetUserID);

            var isAlreadyUserRole = false;
            if (role != null)
            {
                foreach (var ur in urList.ToArray())
                {
                    if (ur.RoleID == roleID)
                    {
                        isAlreadyUserRole = true;
                    }
                    else
                    {
                        repUR.Delete(ur.ID);
                    }
                }

                if (!isAlreadyUserRole)
                {
                    repUR.Insert(newUR);
                }
                // Commit these changes.
                repUR.Save();
            }
            else
            {
                throw new ArgumentException("The ID passed for the Role to assign to the user does not exist.", "roleID");
            }
            
        }


        /// <summary>
        /// Creates a new user with the information passed.
        /// </summary>
        /// <param name="username">The username within the system, which will serve as the login name used.</param>
        /// <param name="firstName">The first name of the user.</param>
        /// <param name="lastName">The last name of the user.</param>
        /// <param name="password">The password of the user, not encrypted.</param>
        /// <param name="passwordQuestion">A question to ask the user for a forgotten password process.</param>
        /// <param name="passwordAnswer">The expected answer to the password question.</param>
        /// <param name="currentUserID">The ID of the user performing this operation.</param>
        /// <param name="isApproved">Optional.  A boolean indicating if the user is approved to begin using the system.</param>
        /// <returns></returns>
        public TOPUser AddWithCredentials(string username, string firstName, string lastName, string password, string passwordQuestion, string passwordAnswer, long currentUserID, bool isApproved = true)
        {
            TOPUser result = null;
            
            var credRepository = Root.GetRepository<Credential>();

            var delUser = Repository.All.Where(tuser => tuser.Login == username && tuser.IsDeleted).ToArray();
            foreach (var du in delUser)
            {
                var delCreds = credRepository.All.Where(cr => cr.UserID == du.ID).ToArray();
                foreach (var dc in delCreds)
                {
                    credRepository.Delete(dc.ID);
                }

                var delUJList = Root.GetRepository<UserJob>().All.Where(uj => uj.UserID == du.ID).ToArray();
                foreach (var uj in delUJList)
                {
                    Root.UserJobActions.Delete(uj.ID, currentUserID);
                }

                Repository.Delete(du.ID);
            }
            Repository.Save();
            credRepository.Save();

            var dupCred = credRepository.All.Where(c => c.Login == username);
            var dupUser = Repository.All.Where(tuser => tuser.Login == username && !tuser.IsDeleted);

            

            if (dupCred.Count() > 0 && dupUser.Count() == 0)
            {
                foreach (var cred in dupCred.ToArray())
                {
                    credRepository.Delete(cred.ID);
                }
                credRepository.Save();
            }
            if (dupCred.Count() > 0 && dupUser.Count() > 0)
            {
                throw new ArgumentException("There is already a user with that username/login.", "username");
            }
            

            result = new TOPUser
            {
                FirstName = firstName,
                LastName = lastName,
                Login = username,
                LastChanged = DateTime.Now,
                Preferences = null
            };

            Add(result, currentUserID);

            var newCred = new Credential
            {
                UserID = result.ID,
                UseADAuthentication = false,
                CreationDate = DateTime.Now,
                LastChanged = DateTime.Now,
                IsApproved = isApproved,
                IsLockedOut = false,
                LastLogin = DateTime.Now,
                Password = SecurityHelper.EncryptString(password),
                Login = username,
                PasswordQuestion = passwordQuestion,
                PasswordAnswer = passwordAnswer
            };

            credRepository.Insert(newCred);
            credRepository.Save();


            return result;
        }


        /// <summary>
        /// Changes the password in the user's credentials to the unencrypted password passed to the method.
        /// </summary>
        /// <param name="targetUserID">ID of the user whose password is being changed.</param>
        /// <param name="password">The new password value (unencrypted) </param>
        /// <param name="currentUserID">ID of the user performing this operation.</param>
        public void SetUserPassword(long targetUserID, string password, long currentUserID)
        {
            var item = Find(targetUserID);
            if (item == null) throw new ArgumentException(string.Format("The User ID passed, {0}, did not return a valid user.", targetUserID), "targetUserID");

            VerifyUserPermission(UserActionType.Update, item, currentUserID);

            var repCred = Root.GetRepository<Credential>();

            var cred = repCred.All.FirstOrDefault(c => c.UserID == targetUserID);

            if (cred != null)
            {
                cred.Password = SecurityHelper.EncryptString(password);
            }

            cred.LastChanged = DateTime.Now;

            repCred.Save();
        }

        #endregion

        /// <summary>
        /// Returns a search result, not filtered by deletion state, in the form of an IQueryable.
        /// </summary>
        /// <param name="searchTerm">Search term to use.</param>
        /// <returns></returns>
        protected IQueryable<TOPUser> SearchAsQueryable(string searchTerm)
        {
            return Repository.All.Where(tu => tu.Login.Contains(searchTerm)
                                            || tu.FirstName.Contains(searchTerm)
                                            || tu.LastName.Contains(searchTerm));

        }

        /// <summary>
        /// Copies TOPUser valeus from one instance to another.
        /// </summary>
        /// <param name="from">Object to copy from.</param>
        /// <param name="to">Object to copy to.</param>
        protected override void copyClassValues(TOPUser from, TOPUser to)
        {
            to.Login = from.Login;
            to.FirstName = from.FirstName;
            to.LastName = from.LastName;
            to.Preferences = from.Preferences;
            to.DefaultJobID = from.DefaultJobID;
        }

        public override bool VerifyUserPermission(UserActionType action, DomainObject target, long userID, bool throwError = true)
        {
            return base.VerifyUserPermission(action, target, userID, throwError);
        }

        public bool VerifyUserRolePermission(UserActionType action, UserRole target, long userID, bool throwError = true)
        {
            bool result = base.VerifyUserPermission(action, (DomainObject)target, userID, throwError);

            var roleData = Root.GetRepository<Role>();
            var userRoleData = Root.GetRepository<UserRole>();

            if (result)
            {
                var newRole = roleData.Find(target.RoleID);
                var actorRoles = userRoleData.All.Where(ur => ur.UserID == userID && (ur.Role.RoleName == "TOPAdmin" || ur.Role.RoleName == "TOPManager")).ToList();

                if (actorRoles.Count == 0)
                {
                    result = false;
                }
                else if (newRole == null)
                {
                    throw new ArgumentException("Verifying User Role Permission failed.  Role not found.");
                }

                if (result)
                {
                    // At this point, the user can be assumed to be either a manager or admin. Either one could create a new manager.

                    if (newRole.RoleName == "TOPAdmin" && !actorRoles.Any(ur => ur.Role.RoleName == "TOPAdmin"))
                    {
                        result = false;
                    }

                    if (newRole.RoleName != "TOPAdmin" && userRoleData.All.Any(tr => tr.UserID == target.UserID && tr.Role.RoleName == "TOPAdmin")
                        && !actorRoles.Any(ur => ur.Role.RoleName == "TOPAdmin"))
                    {
                        result = false;
                    }
                }

            }
            return result;
        }


        public bool IsUserInRole(long targetUserID, string roleName)
        {
            var qryUserRoles = Root.GetQueryable<UserRole>();
            return qryUserRoles.Any(x => x.Role.RoleName == roleName && x.UserID == targetUserID);
        }
    }
}
