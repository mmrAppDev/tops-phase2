﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;


namespace Lib.TOPControls
{



    /// <summary>
    /// Implements management functions for UserJob objects.
    /// </summary>
    public class UserJobLibrary : ObjectLibraryBase<UserJob>, IUserJobLibrary
    {
        #region Constructors

        /// <summary>
        /// Creates a new UserJobLibrary with default values.
        /// </summary>
        public UserJobLibrary()
            : base()
        { }
        /// <summary>
        /// Creates a new UserJobLibrary with the Root library passed.
        /// </summary>
        /// <param name="rootLibrary">An object that implements ITOPControlLibrary</param>
        public UserJobLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }
        /// <summary>
        /// Creates a new UserJobLibrary with the resources passed.
        /// </summary>
        /// <param name="useRepository">The Repository to use</param>
        /// <param name="permissionManager">The IPermissionManager to use.</param>
        public UserJobLibrary(IRepository<UserJob> useRepository = null, IPermissionManager permissionManager = null)
            : base(useRepository, permissionManager)
        { }

        #endregion

        
        
        /// <summary>
        /// Retrieves all UserJob items.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserJob> GetAll()
        {
            return FilterDeletedItems(Repository.All).ToList();

        }

        /// <summary>
        /// Returns all UserJob values that belong to a specific job.
        /// </summary>
        /// <param name="jobID">The ID of the job.</param>
        /// <returns></returns>
        public IEnumerable<UserJob> AllForJob(long jobID)
        {
            var undel = FilterDeletedItems(Repository.All);
            return undel.Where(uj => uj.JobID == jobID);
        }


        /// <summary>
        /// Returns all UserJob values that belong to a specific TOPUser.
        /// </summary>
        /// <param name="userID">The ID of the TOPUser.</param>
        /// <returns></returns>
        public IEnumerable<UserJob> AllForUser(long userID)
        {
            var undel = FilterDeletedItems(Repository.All);
            return undel.Where(uj => uj.UserID == userID);
        }

        /// <summary>
        /// Copies UserJob values from one object to another.
        /// </summary>
        /// <param name="from">Object to copy from.</param>
        /// <param name="to">Object to copy to.</param>
        protected override void copyClassValues(UserJob from, UserJob to)
        {
            to.UserID = from.UserID;
            to.JobID = from.JobID;
        }

        /// <summary>
        /// Returns the IQueryable passed with an added filter to eliminate deleted items.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        protected static IQueryable<UserJob> FilterDeletedItems(IQueryable<UserJob> source)
        {
            return source.Where(uj => uj.JobInfo != null && !uj.JobInfo.IsDeleted
                                    && uj.TOPUser != null && !uj.TOPUser.IsDeleted);
        }
    }
}
