﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls
{
    /// <summary>
    /// Implements operations and object validation for instances of the Commodity class.
    /// </summary>
    public class CommodityLibrary : JobObjectLibraryBase<Commodity>, ICommodityLibrary
    {
        #region Constructors

        /// <summary>
        /// Creates a new instance of the library with default settings.
        /// </summary>
        public CommodityLibrary()
            : base()
        { }

        /// <summary>
        /// Creates a new instances of the library with a reference to an ITOPControlLibrary instance,
        /// and by default using its resources.
        /// </summary>
        /// <param name="rootLibrary">An object that implements the ITOPControlLibrary interface.</param>
        public CommodityLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }

        /// <summary>
        /// Creates a new instance of the CommodityLibrary using the repository and permission manager passed.
        /// </summary>
        /// <param name="useCommodityRepository">The repository to access the commodities.</param>
        /// <param name="permissionManager">An object implementing the IPermissionManager interface.</param>
        public CommodityLibrary(IRepository<Commodity> useCommodityRepository = null, IPermissionManager permissionManager = null)
            : base(useCommodityRepository, permissionManager)
        { }

        #endregion Constructors

        /// <summary>
        /// Returns a ValidationResult that indicates if the commodity is valid to store in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public override ValidationResult Validate(Commodity item)
        {
            var result = base.Validate(item);

            if (item != null)
            {
                result.ValidateStringLength(item.CommodityCode, 1, 200, "CommodityCode");
                var dups = Repository.All.Count(c => c.ID != item.ID && c.JobID == item.JobID && c.CommodityCode == item.CommodityCode);

                result.ValidateStringLength(item.Description, 0, 200, "Description");

                if (item.ManHoursEstimated < 0)
                {
                    result.AddViolation("Man-hours estimated must be a positive number.", "ManHoursEstimated");
                }
            }

            return result;
        }

        /// <summary>
        /// Returns an instance of the target type if there is a valid item with a primary descriptive string matching
        /// the name passed, unless the item is marked deleted.
        /// </summary>
        /// <param name="jobID">The ID of the job being searched.</param>
        /// <param name="name">The full name string to search for.</param>
        /// <returns></returns>
        public override Commodity FindByName(long jobID, string name)
        {
            return Repository.All.FirstOrDefault(c => c.JobID == jobID && c.CommodityCode == name);
        }

        #region ICommodityLibrary Members

        /// <summary>
        /// Returns a set of Commodity objects within the Job indicated by jobID that match the
        /// searchTerm passed. Does not return Commodities from deleted jobs.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="comCode">A string value to search for.  If the searchTerm is an
        /// empty string, returns all Commodity records within the job. </param>
        /// <returns></returns>
        public Commodity FindByCommodityCode(long jobID, string comCode)
        {
            return Repository.All.SingleOrDefault(c => c.JobID == jobID
                   && c.CommodityCode == comCode);
        }

        /// <summary>
        /// Returns Commodity object within the with a CommodityCode value that matches the string
        /// value passed.
        /// </summary>
        /// <param name="jobID">The ID of the Job the Commodity Code belongs to.</param>
        /// <param name="searchTerm">A string value representing the Commodity Code.</param>
        /// <returns></returns>
        public IEnumerable<Commodity> Search(long jobID, string searchTerm)
        {
            return Repository.All.Where(c => c.JobID == jobID
                    && (searchTerm == ""
                        || c.CommodityCode == searchTerm
                        || c.Description.Contains(searchTerm)));
        }

        /// <summary>
        /// Returns a Nullable decimal value indicating the number of ManHours of estimated work
        /// each TaskProgress item with the passed Commodity ID represents.  If there is no estimated value for the
        /// commodity, or if there are no items in the system for that commodity, returns a null.
        /// </summary>
        /// <param name="commodityID">The ID of the commodity that is being checked.  Call will fail if this ID is
        /// zero or lower, or if no commodity exists with that ID.</param>
        /// <returns></returns>
        public decimal? GetManHoursPerItem(long commodityID)
        {
            decimal? result = null;

            var item = Find(commodityID);

            throwOnItemNotFound(item);

            var itemcount = Root.TaskProgressActions.CountItemsByCommodity(commodityID);

            if (item.ManHoursEstimated > 0 && itemcount > 0)
            {
                result = item.ManHoursEstimated / Convert.ToDecimal(itemcount);
            }

            return result;
        }

        #endregion ICommodityLibrary Members

        /// <summary>
        /// Adds a condition to the IQueryable passed that filters items down to records that have Commodity ID values
        /// that can be found in the list of values passed.
        /// </summary>
        /// <param name="filteredList">A reference to an IQueryable of TaskProgress to be filtered.</param>
        /// <param name="values">A set of Commodity ID values to use to filter records.</param>
        public static void AttachTaskProgressFilter(ref IQueryable<TaskProgress> filteredList, IEnumerable<long> values)
        {
            filteredList = filteredList.Where(tp => tp.CommodityID.HasValue && values.Contains(tp.CommodityID.Value));
        }

        /// <summary>
        /// Validates the delete...blah...blah...blah
        /// </summary>
        /// <param name="id">ID of the item to delete.</param>
        /// <returns></returns>
        public override ValidationResult ValidateDelete(long id)
        {
            var result = base.ValidateDelete(id);
            var item = Find(id);
            if (item != null)
            {
                // Who has this one?
                var matches = Root.GetQueryable<TaskProgress>().Where(x => x.CommodityID.HasValue && x.CommodityID.Value == id);
                var matchCount = matches.Count();
                if (matchCount > 0)
                {
                    result.AddViolation(string.Format("Commodity cannot be deleted. There are {0} tracked items that reference that Commodity.", matchCount));
                }
            }
            return result;
        }

        /// <summary>
        /// Copy values from one instance of the Commodity Class to another.
        /// </summary>
        /// <param name="from">The instance to copy values from.</param>
        /// <param name="to">The instance to copy values to.</param>
        protected override void copyClassValues(Commodity from, Commodity to)
        {
            to.JobID = from.JobID; 
            to.CommodityCode = from.CommodityCode;
            to.Description = from.Description;
            to.ManHoursEstimated = from.ManHoursEstimated;
        }
    }
}