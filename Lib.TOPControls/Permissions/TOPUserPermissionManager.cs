﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Permissions
{
    public class TOPUserPermissionManager:IPermissionManager
    {
        protected ITOPControlLibrary Root { get; set; }
        protected IPermissionManager RootPermissionManager { get; set; }

        public TOPUserPermissionManager(ITOPControlLibrary root)
        {
            if (root == null) 
            {
                throw new ArgumentException("Cannot create a TOPUserPermissionManager without a root library instance.", "root");
            }
            Root = root;

            RootPermissionManager = Root.PermissionManager;
            if (RootPermissionManager == null)
            {
                RootPermissionManager = new PermissionManager(root);
            }
        }


        public bool IsValidUserAction(UserActionType action, DomainObject target, long userID)
        {
            bool result = RootPermissionManager.IsValidUserAction(action, target, userID);
            


            return result;
        }
    }
}
