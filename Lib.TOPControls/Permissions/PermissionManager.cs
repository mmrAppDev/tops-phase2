﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Permissions
{
    /// <summary>
    /// Implements the functionality of the IPermissionManager for the TOPControlLibrary
    /// </summary>
    public class PermissionManager:IPermissionManager
    {
        /// <summary>
        /// Gets the ITOPControlLibrary this instance provides functionaltity for.
        /// </summary>
        public ITOPControlLibrary Root { get; set; }

        /// <summary>
        /// Create an instance of the PermissionManager class for the root library passed.
        /// </summary>
        /// <param name="rootLibrary">An object that implements the ITOPControlLibrary interface.</param>
        public PermissionManager(ITOPControlLibrary rootLibrary)
        {
            Root = rootLibrary;
        }

        // TODO: Build the concrete permission manager


        #region IPermissionManager Members

        /// <summary>
        /// Identifies whether a user action is valid according to their permissions.
        /// </summary>
        /// <param name="action">A UserActionType value to validate.</param>
        /// <param name="target">A target to validate. For many operations, an unaltered new instance of the 
        /// target type is sufficient.</param>
        /// <param name="userID">The ID of the user performing the operation.</param>
        /// <returns></returns>
        public bool IsValidUserAction(UserActionType action, DomainObject target, long userID)
        {
            // This is currently stubbed out to keep the dependency chain from breaking down.
            Debug.WriteLine("This Permission manager is a stub!");
            return true;
        }

        #endregion
    }
}
