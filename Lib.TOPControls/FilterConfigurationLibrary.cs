﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls
{
    /// <summary>
    /// A library class that performs operations specific to the FilterConfiguration class and its instances.
    /// </summary>
    public class FilterConfigurationLibrary : JobObjectLibraryBase<FilterConfiguration>, IFilterConfigurationLibrary
    {
        #region Constructors

        /// <summary>
        /// Creates an instance of CraftInfoLibrary with default values.
        /// </summary>
        public FilterConfigurationLibrary()
            : base()
        { }

        /// <summary>
        /// Creates an instance of CraftInfoLibrary, setting the Root library and pulling all other resources from it.
        /// </summary>
        /// <param name="rootLibrary">An object that implements ITOPControlLibrary.</param>
        public FilterConfigurationLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }

        /// <summary>
        /// Creates an instance of CraftInfoLibrary with the resource values passed.
        /// </summary>
        /// <param name="filterConfigRepository">The IRepository to use.</param>
        /// <param name="permissionManager">The IPermissionManager to use.</param>
        public FilterConfigurationLibrary(IRepository<FilterConfiguration> filterConfigRepository = null, IPermissionManager permissionManager = null)
            : base(filterConfigRepository, permissionManager)
        { }

        #endregion Constructors

        /// <summary>
        /// Returnes all items of this type belonging to the JobID passed.
        /// </summary>
        /// <param name="jobID">ID of the job to use.</param>
        /// <returns></returns>
        public override IEnumerable<FilterConfiguration> AllForJob(long jobID)
        {
            return base.AllForJob(jobID).OrderBy(fc => fc.DisplayOrder);
        }

        /// <summary>
        /// Copies values specific to the FilterConfiguration class.
        /// </summary>
        /// <param name="from">Object to copy from.</param>
        /// <param name="to">Object to copy to.</param>
        protected override void copyClassValues(FilterConfiguration from, FilterConfiguration to)
        {
            to.JobID = from.JobID;
            to.FilterValueType = from.FilterValueType;
            to.DisplayOrder = from.DisplayOrder;
            to.Active = from.Active;
            to.IsHighVolume = from.IsHighVolume;
        }

        #region IFilterConfigurationLibrary Members

        /// <summary>
        /// Returns a set of FilterConfiguration objects. Modifies the DisplayOrder property of the project
        /// item identified so that it is one item closer to the end of the list, also modifying the
        /// DisplayOrder property of the item that occupies its new position.  No values are changed
        /// if the item is already at the bottom of the list.
        /// </summary>
        /// <param name="itemID">The ID value of the item being moved down.</param>
        /// <param name="userID">The ID of the TOPUser performing this operation. This is used to both
        /// log the action and validate the user’s right to perform the operation.</param>
        /// <returns></returns>
        public IEnumerable<FilterConfiguration> MoveDownInDisplayOrder(long itemID, long userID)
        {
            var target = Find(itemID);
            if (target == null) throw new ArgumentException("Could not find a FilterConfiguration matching the ID passed.");
            if (VerifyUserPermission(UserActionType.Update, target, userID))
            {
                var fcList = AllForJob(target.JobID);
                var nextItem = fcList.Where(fc => fc.DisplayOrder > target.DisplayOrder).FirstOrDefault();
                if (nextItem != null)
                {
                    var ord = nextItem.DisplayOrder;
                    nextItem.DisplayOrder = target.DisplayOrder;
                    target.DisplayOrder = ord;
                    base.SetDomainAutomaticValues(nextItem);
                    base.SetDomainAutomaticValues(target);

                    var validNext = Validate(nextItem);
                    var validTarg = Validate(target);

                    if (validNext.IsValid && validTarg.IsValid)
                    {
                        Repository.Save();
                    }
                }
            }
            return AllForJob(target.JobID);
        }

        /// <summary>
        /// Returns a set of FilterConfiguration objects. Modifies the DisplayOrder property of the project
        /// item identified so that it is one item closer to the start of the list, also modifying
        /// the DisplayOrder property of the item that occupies its new position.  No values are changed
        /// if the item is already at the top of the list.
        /// </summary>
        /// <param name="itemID">The ID value of the item being moved up.</param>
        /// <param name="userID">The ID of the TOPUser performing this operation. This is used to both
        /// log the action and validate the user’s right to perform the operation.</param>
        /// <returns></returns>
        public IEnumerable<FilterConfiguration> MoveUpInDisplayOrder(long itemID, long userID)
        {
            var target = Find(itemID);
            if (target == null) throw new ArgumentException("Could not find a Filter Configuration matching the ID passed.");
            if (VerifyUserPermission(UserActionType.Update, target, userID))
            {
                var fcList = AllForJob(target.JobID).OrderBy(fc => fc.DisplayOrder);
                var nextItem = fcList.Where(fc => fc.DisplayOrder < target.DisplayOrder).LastOrDefault();
                if (nextItem != null)
                {
                    var ord = nextItem.DisplayOrder;
                    nextItem.DisplayOrder = target.DisplayOrder;
                    target.DisplayOrder = ord;
                    base.SetDomainAutomaticValues(nextItem);
                    base.SetDomainAutomaticValues(target);

                    var validNext = Validate(nextItem);
                    var validTarg = Validate(target);

                    if (validNext.IsValid && validTarg.IsValid)
                    {
                        Repository.Save();
                    }
                }
            }
            return AllForJob(target.JobID);
        }


        #endregion IFilterConfigurationLibrary Members

        #region OverridesOfCoreFunctions

        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public override ValidationResult Validate(FilterConfiguration item)
        {
            var result = base.Validate(item);

            if (item.DisplayOrder < 1)
            {
                result.AddViolation("Cannot assign a display order value that is negative or zero.", "DisplayOrder");
            }
            if (item.JobID < 1)
            {
                result.AddViolation("The JobID value is invalid.", "JobID");
            }

            return result;
        }

        /// <summary>
        /// Sets the values that are determined automatically by the domain, as well as handling the relative values of DisplayOrder values.
        /// </summary>
        /// <param name="item"></param>
        protected override void SetDomainAutomaticValues(FilterConfiguration item)
        {
            base.SetDomainAutomaticValues(item);
            if (item.ID == 0)
            {
                var fullList = RenumberItems(item.JobID);
                item.DisplayOrder = fullList.Count() + 1;
            }
            else
            {
                var discrim = GetSiblings(item).Count(fc => fc.DisplayOrder == item.DisplayOrder);
                if (discrim > 0)
                {

                    var fullList = GetSiblings(item).Where(fc => fc.DisplayOrder >= item.DisplayOrder).ToList();

                    foreach (var config in fullList)
                    {
                        config.DisplayOrder = config.DisplayOrder + 1;
                        config.LastChanged = DateTime.Now;
                    }

                }
                RenumberItems(item.JobID);
            }
            
        }

        /// <summary>
        /// Returns all items of this type that are in the same list of IDisplayOrdered as the item passed.
        /// </summary>
        /// <param name="item">The source item for which the siblings are being returned.</param>
        /// <param name="excludeOriginal">A boolean that if true causes the items to be returned without the 
        /// instance of the item originally passed.</param>
        /// <returns></returns>
        protected IQueryable<FilterConfiguration> GetSiblings(FilterConfiguration item, bool excludeOriginal = true)
        {
            return Repository.All.Where(fc => fc.JobID == item.JobID && (!excludeOriginal || fc.ID != item.ID));
        }

        #endregion OverridesOfCoreFunctions

        /// <summary>
        /// Modifies the DisplayOrder property of all items of this type within the Job indicated by JobID
        /// </summary>
        /// <param name="jobID">An ID representing the Job to which this item belongs.</param>
        /// <param name="commitValues">A boolean that when passed as true will trigger a Save operation to commit the altered 
        /// DisplayOrder values to the permanent data.  Otherwise, the changes are left to be included in the Unit of Work that 
        /// commits next.</param>
        /// <returns></returns>
        public virtual IEnumerable<FilterConfiguration> RenumberItems(long jobID, bool commitValues = false)
        {
            var allConfigs = Repository.All.Where(fc => fc.JobID == jobID).ToList().OrderBy(x => x.DisplayOrder).ThenBy(y => y.ID);
            
            for (int cnt = 0; cnt < allConfigs.Count(); cnt++)
            {
                if (allConfigs.ElementAt(cnt).DisplayOrder != cnt + 1)
                {
                    var thisitem = allConfigs.ElementAt(cnt);
                    thisitem.DisplayOrder = cnt + 1;
                    thisitem.LastChanged = DateTime.Now;
                }
            }

            if (commitValues)
            {
                Repository.Save();
            }

            return allConfigs;
        }

        /// <summary>
        /// Returns an instance of the target type if there is a valid item with a primary descriptive string matching
        /// the name passed, unless the item is marked deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job the object should belong to.</param>
        /// <param name="name">The full name string to search for.</param>
        /// <returns></returns>
        public override FilterConfiguration FindByName(long jobID, string name)
        {
            FilterValueType matched = FilterValueType.None;
            if (Enum.TryParse<FilterValueType>(name, out matched))
            {
                return Repository.All.FirstOrDefault(fc => fc.JobID == jobID && fc.FilterType == (int)matched);
            }
            else
            {
                return null;
            }
        }

    }
}