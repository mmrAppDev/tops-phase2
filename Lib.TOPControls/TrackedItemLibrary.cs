﻿using System.Collections.Generic;
using System.Linq;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls
{
    /// <summary>
    /// Implements operations to manage TrackedItem objects
    /// </summary>
    public class TrackedItemLibrary : JobSoftDeleteLibraryBase<TrackedItem>, ITrackedItemLibrary
    {
        #region Constructors

        /// <summary>
        /// Creates a TrackedItemLibrary with defaults.
        /// </summary>
        public TrackedItemLibrary()
            : base()
        {
        }

        /// <summary>
        /// Creates a TrackedItemLibrary with the root library supplied.
        /// </summary>
        /// <param name="rootLibrary">An instance of the root TOPControl library</param>
        public TrackedItemLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        {
        }

        /// <summary>
        /// Creates a TrackedItemLibrary with the resources passed.
        /// </summary>
        /// <param name="useTrackedItemRepository">Repostory to use</param>
        /// <param name="permissionManager">IPermissionManager to use.</param>
        public TrackedItemLibrary(IRepository<TrackedItem> useTrackedItemRepository = null,
            IPermissionManager permissionManager = null)
            : base(useTrackedItemRepository, permissionManager)
        {
        }

        #endregion Constructors

        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public override ValidationResult Validate(TrackedItem item)
        {
            var result = base.Validate(item);

            if (item != null)
            {
                result.ValidateStringLength(item.TrackingID, 3, 200, "TrackingID");
                var dupTID =
                    Repository.All.Count(
                        ti => ti.ID != item.ID && ti.JobID == item.JobID && ti.TrackingID == item.TrackingID);
                if (dupTID > 0)
                {
                    result.AddViolation(
                        "There is already a Tracked Item with a Tracking ID of \"" + item.TrackingID +
                        "\".  TrackingID must be unique.", "TrackingID");
                }

                if (item.ObjectTypeID.HasValue && Root != null &&
                    Root.GetRepository<ObjectType>().Find(item.ObjectTypeID.Value) == null)
                {
                    result.AddViolation("Could not find the Object Type for the ID passed.", "ObjectTypeID");
                }
                if (item.SystemID.HasValue && Root != null &&
                    Root.GetRepository<ProjectSystem>().Find(item.SystemID.Value) == null)
                {
                    result.AddViolation("Could not find the System for the System ID.", "SystemID");
                }
                if (item.AreaID.HasValue && Root != null &&
                    Root.GetRepository<ProjectArea>().Find(item.AreaID.Value) == null)
                {
                    result.AddViolation("Could not find the Area for the Area ID.", "AreaID");
                }
                if (item.CraftID.HasValue && Root != null &&
                    Root.GetRepository<CraftInfo>().Find(item.CraftID.Value) == null)
                {
                    result.AddViolation("Could not find the Craft matching the Craft ID", "CraftID");
                }

                // TODO: Need to determine how to validate the UDP Value so that UDP Values without names cannot be changed to a non-blank value, but UDP Values without names that
                // already exist in the database are not erased.

                if (Root != null)
                {
                    if (item.JobInfo == null) item.JobInfo = Root.JobActions.Find(item.JobID);

                    if (!string.IsNullOrWhiteSpace(item.JobInfo.UDPName1))
                    {
                        result.ValidateStringLength(item.UDPValue1, 0, 200, "UDPValue1",
                            item.JobInfo.UDPName1 + " must be {2} characters or less.");
                    }
                    if (!string.IsNullOrWhiteSpace(item.JobInfo.UDPName2))
                    {
                        result.ValidateStringLength(item.UDPValue2, 0, 200, "UDPValue2",
                            item.JobInfo.UDPName2 + " must be {2} characters or less.");
                    }
                    if (!string.IsNullOrWhiteSpace(item.JobInfo.UDPName3))
                    {
                        result.ValidateStringLength(item.UDPValue3, 0, 200, "UDPValue3",
                            item.JobInfo.UDPName3 + " must be {2} characters or less.");
                    }
                    if (!string.IsNullOrWhiteSpace(item.Comments))
                    {
                        result.ValidateStringLength(item.Comments, 0, 500, "Comments",
                            item.Comments + " must be {2} characters or less.");
                    }
                }
            }

            return result;
        }

        #region ITrackedItemLibrary Members

        /// <summary>
        /// Returns a TrackedItem with a TrackingID that is a match for the trackingID passed. Returns a null if a matching value does not exist.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="trackingID">A string value to compare to the TrackingID of all Tracked Items within the job.</param>
        /// <returns></returns>
        public TrackedItem FindByTrackingID(long jobID, string trackingID)
        {
            return Repository.All.SingleOrDefault(ti => ti.JobID == jobID
                                                        && ti.TrackingID == trackingID
                                                        && ti.IsDeleted == false);
        }

        /// <summary>
        /// Returns a set of TrackedItem objects within the Job indicated by jobID that match the systemID
        /// </summary>
        /// <param name="jobID">The job identifier.</param>
        /// <param name="systemID">The system identifier.</param>
        /// <returns></returns>
        public IEnumerable<TrackedItem> FindBySystemID(long jobID, long systemID)
        {
            return Repository.All.Where(ti => ti.JobID == jobID
                                              && ti.SystemID == systemID
                                              && ti.IsDeleted == false);
        }

        /// <summary>
        /// Returns a set of TrackedItem objects within the Job indicated by jobID that match the
        /// searchTerm passed.  Does not return TrackedItems for deleted jobs, or any TrackedItems that
        /// are marked as deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">A string value to search for.  If the searchTerm is an empty string, returns all TrackedItems in the job.</param>
        /// <returns></returns>
        public IEnumerable<TrackedItem> Search(long jobID, string searchTerm)
        {
            return SearchAnyQueryable(jobID, searchTerm).Where(ti => ti.IsDeleted == false).ToList();
        }

        /// <summary>
        /// Returns a set of TrackedItem objects within the Job indicated by jobID that match the
        /// searchTerm passed.  Returns all items matching items, even if the Job is marked as deleted
        /// or the TrackedItem itself is marked as deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">A string value to search for.  If the searchTerm is an empty string, returns all TrackedItems in the job.</param>
        /// <param name="userID">The ID of the TOPUser performing the operation.</param>
        /// <returns></returns>
        public IEnumerable<TrackedItem> SearchAny(long jobID, string searchTerm, long userID)
        {
            VerifyUserPermission(UserActionType.ViewDeleted, new TrackedItem() {JobID = jobID}, userID);
            return SearchAnyQueryable(jobID, searchTerm).ToList();
        }

        #endregion ITrackedItemLibrary Members

        /// <summary>
        /// Returns a search result set not filtered for deleted items as an IQueryable.
        /// </summary>
        /// <param name="jobID">ID of the job to search.</param>
        /// <param name="searchTerm">The string to use in this search operation.</param>
        /// <returns></returns>
        protected IQueryable<TrackedItem> SearchAnyQueryable(long jobID, string searchTerm)
        {
            return Repository.All.Where(ti => ti.JobID == jobID
                                              && (searchTerm == "" || ti.TrackingID.Contains(searchTerm)));
        }

        /// <summary>
        /// Appends a filter to a Queryable of TaskProgress items that returns only items associated with TrackedItem having a TrackingID
        /// matching a value in the set of values passed.
        /// </summary>
        /// <param name="filteredList">The TaskProgress queryable to filter, passed as a reference.</param>
        /// <param name="values">The list of values to select for.</param>
        public static void AttachTaskProgressTrackingIDFilter(ref IQueryable<TaskProgress> filteredList,
            IEnumerable<string> values)
        {
            if (values != null && values.Count() > 0)
            {
                filteredList = filteredList.Where(tp => tp.TrackedItem != null
                                                        && tp.TrackedItem.TrackingID != ""
                                                        && values.Contains(tp.TrackedItem.TrackingID));
            }
        }

        /// <summary>
        /// Appends a filter to a Queryable of TaskProgress items that returns only items associated with TrackedItem having a User Defined Property
        /// value in the slot requested that has a matching a value in the set of values passed.
        /// </summary>
        /// <param name="filteredList">The TaskProgress queryable to filter, passed as a reference.</param>
        /// <param name="values">The list of values to filter for.</param>
        /// <param name="propertyNumber">The position (1, 2, or 3) of the User Defined Property to filter.</param>
        public static void AttachTaskProgressUDPFilter(ref IQueryable<TaskProgress> filteredList,
            IEnumerable<string> values, int propertyNumber)
        {
            if (values != null && values.Count() > 0)
            {
                switch (propertyNumber)
                {
                    case 1:
                        filteredList =
                            filteredList.Where(
                                tp => tp.JobInfo != null && tp.JobInfo.UDPName1 != null && tp.JobInfo.UDPName1 != ""
                                      && tp.TrackedItem != null && tp.TrackedItem.UDPValue1 != null &&
                                      values.Contains(tp.TrackedItem.UDPValue1));
                        break;

                    case 2:
                        filteredList =
                            filteredList.Where(
                                tp => tp.JobInfo != null && tp.JobInfo.UDPName2 != null && tp.JobInfo.UDPName2 != ""
                                      && tp.TrackedItem != null && tp.TrackedItem.UDPValue2 != null &&
                                      values.Contains(tp.TrackedItem.UDPValue2));
                        break;

                    case 3:
                        filteredList =
                            filteredList.Where(
                                tp => tp.JobInfo != null && tp.JobInfo.UDPName3 != null && tp.JobInfo.UDPName3 != ""
                                      && tp.TrackedItem != null && tp.TrackedItem.UDPValue3 != null &&
                                      values.Contains(tp.TrackedItem.UDPValue3));
                        break;

                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Copies values from one item to another.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        protected override void copyClassValues(TrackedItem from, TrackedItem to)
        {
            to.JobID = from.JobID;
            to.TrackingID = from.TrackingID;
            to.ObjectTypeID = from.ObjectTypeID;
            to.SystemID = from.SystemID;
            to.AreaID = from.AreaID;
            to.CraftID = from.CraftID;
            to.UDPValue1 = from.UDPValue1;
            to.UDPValue2 = from.UDPValue2;
            to.UDPValue3 = from.UDPValue3;
        }

        /// <summary>
        /// Returns an instance of the target type if there is a valid item with a primary descriptive string matching
        /// the name passed, unless the item is marked deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job the object should belong to.</param>
        /// <param name="name">The full name string to search for.  In the case o</param>
        /// <returns></returns>
        public override TrackedItem FindByName(long jobID, string name)
        {
            return FindByTrackingID(jobID, name);
        }
    }
}