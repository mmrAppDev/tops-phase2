﻿using MMRCommon.Data;

namespace Lib.TOPControls
{
    /// <summary>
    /// Currently empty extensions class.
    /// </summary>
    public static class DomainExtensions
    {
        // Validation Result Extentions

        /// <summary>
        /// Validate string length.
        /// </summary>
        /// <param name="result">The ValidationResult</param>
        /// <param name="target">The string being validated, with any irrelevant whitespace already removed.</param>
        /// <param name="minLength">The minimum length of the string.</param>
        /// <param name="maxLength">The maximum length of the string.</param>
        /// <param name="propertyName">The name of the property being validated.</param>
        /// <param name="errorMessage">A string, or a format argument for arguments in this order:  propertyName, minLength, maxLength</param>
        /// <returns></returns>
        public static RuleViolation ValidateStringLength(this ValidationResult result, string target, int minLength, int maxLength, string propertyName = "value", string errorMessage = "{0} must be between {1} and {2} characters long.")
        {
            RuleViolation rv = null;
            if (target == null)
            {
                rv = result.AddViolation(string.Format("{0} is NULL.", propertyName), propertyName);
            }
            else
            {
                if (target.Length < minLength || target.Length > maxLength)
                {
                    rv = result.AddViolation(string.Format(errorMessage, propertyName, minLength, maxLength), propertyName);
                }
            }
            return rv;
        }
    }
}