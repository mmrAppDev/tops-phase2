﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Lib.TOPControls.Charting;
using Lib.TOPControls.Helpers;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Interfaces.DataAccess;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides operations for cross-object actions and a centralized access point for
    /// object-specific libraries.
    /// </summary>
    public class TOPControlLibrary : ITOPControlLibrary
    {
        /// <summary>
        /// Data Access Layer
        /// </summary>
        public ITOPControlDAL DAL { get; set; }

        /// <summary>
        /// Creates a TOPControlLibrary using the Data Access Layer passed.
        /// </summary>
        /// <param name="dal">Data Access Layer</param>
        public TOPControlLibrary(ITOPControlDAL dal)
        {
            DAL = dal;
            PermissionManager = new PermissionManager(this);
        }

        /// <summary>
        /// Gets or sets an object that implements the IPermissionManager interface.
        /// </summary>
        public IPermissionManager PermissionManager { get; set; }

        #region ITOPControlLibrary Members

        /// <summary>
        /// Private field.
        /// </summary>
        protected IJobInfoLibrary _jobActions;

        /// <summary>
        /// Library property
        /// </summary>
        public IJobInfoLibrary JobActions
        {
            get
            {
                if (_jobActions == null) _jobActions = new JobInfoLibrary(this);
                return _jobActions;
            }
            set { _jobActions = value; }
        }

        /// <summary>
        /// Private field.
        /// </summary>
        protected IProjectPhaseLibrary _phaseActions;

        /// <summary>
        /// Library property
        /// </summary>
        public IProjectPhaseLibrary PhaseActions
        {
            get
            {
                if (_phaseActions == null) _phaseActions = new ProjectPhaseLibrary(this);
                return _phaseActions;
            }
            set { _phaseActions = value; }
        }

        /// <summary>
        /// Private field.
        /// </summary>
        protected IObjectTypeLibrary _objectTypeActions;

        /// <summary>
        /// Library property
        /// </summary>
        public IObjectTypeLibrary ObjectTypeActions
        {
            get
            {
                if (_objectTypeActions == null) _objectTypeActions = new ObjectTypeLibrary(this);
                return _objectTypeActions;
            }
            set { _objectTypeActions = value; }
        }

        /// <summary>
        /// Private field.
        /// </summary>
        protected IProjectSystemLibrary _systemActions;

        /// <summary>
        /// Library property
        /// </summary>
        public IProjectSystemLibrary SystemActions
        {
            get
            {
                if (_systemActions == null) _systemActions = new ProjectSystemLibrary(this);
                return _systemActions;
            }
            set { _systemActions = value; }
        }

        /// <summary>
        /// Private field.
        /// </summary>
        protected IProjectAreaLibrary _areaActions;

        /// <summary>
        /// Library property
        /// </summary>
        public IProjectAreaLibrary AreaActions
        {
            get
            {
                if (_areaActions == null) _areaActions = new ProjectAreaLibrary(this);
                return _areaActions;
            }
            set { _areaActions = value; }
        }

        /// <summary>
        /// Private field.
        /// </summary>
        protected ICraftInfoLibrary _craftActions;

        /// <summary>
        /// Library property
        /// </summary>
        public ICraftInfoLibrary CraftActions
        {
            get
            {
                if (_craftActions == null) _craftActions = new CraftInfoLibrary(this);
                return _craftActions;
            }
            set { _craftActions = value; }
        }

        /// <summary>
        /// Private field.
        /// </summary>
        protected ICommodityLibrary _commodityActions;

        /// <summary>
        /// Library property
        /// </summary>
        public ICommodityLibrary CommodityActions
        {
            get
            {
                if (_commodityActions == null) _commodityActions = new CommodityLibrary(this);
                return _commodityActions;
            }
            set { _commodityActions = value; }
        }

        /// <summary>
        /// Private field.
        /// </summary>
        protected ITrackedItemLibrary _trackedItemActions;

        /// <summary>
        /// Library property
        /// </summary>
        public ITrackedItemLibrary TrackedItemActions
        {
            get
            {
                if (_trackedItemActions == null) _trackedItemActions = new TrackedItemLibrary(this);
                return _trackedItemActions;
            }
            set { _trackedItemActions = value; }
        }

        /// <summary>
        /// Private field.
        /// </summary>
        protected ITaskLibrary _taskActions;

        /// <summary>
        /// Library property
        /// </summary>
        public ITaskLibrary TaskActions
        {
            get
            {
                if (_taskActions == null) _taskActions = new TaskLibrary(this);
                return _taskActions;
            }
            set { _taskActions = value; }
        }

        /// <summary>
        /// Private field.
        /// </summary>
        protected ITaskProgressLibrary _taskProgressActions;

        /// <summary>
        /// Library property
        /// </summary>
        public ITaskProgressLibrary TaskProgressActions
        {
            get
            {
                if (_taskProgressActions == null) _taskProgressActions = new TaskProgressLibrary(this);
                return _taskProgressActions;
            }
            set { _taskProgressActions = value; }
        }

        /// <summary>
        /// Private field.
        /// </summary>
        protected ITOPUserLibrary _userActions;

        /// <summary>
        /// Library property
        /// </summary>
        public ITOPUserLibrary UserActions
        {
            get
            {
                if (_userActions == null) _userActions = new TOPUserLibrary(this);
                return _userActions;
            }
            set { _userActions = value; }
        }

        /// <summary>
        /// Private field
        /// </summary>
        protected IUserJobLibrary _userJobActions;

        /// <summary>
        /// Library property
        /// </summary>
        public IUserJobLibrary UserJobActions
        {
            get
            {
                if (_userJobActions == null) _userJobActions = new UserJobLibrary(this);
                return _userJobActions;
            }
            set { _userJobActions = value; }
        }

        /// <summary>
        /// Private field.
        /// </summary>
        protected IWorkCrewLibrary _workCrewActions;

        /// <summary>
        /// Library property
        /// </summary>
        public IWorkCrewLibrary WorkCrewActions
        {
            get
            {
                if (_workCrewActions == null) _workCrewActions = new WorkCrewLibrary(this);
                return _workCrewActions;
            }
            set { _workCrewActions = value; }
        }

        /// <summary>
        /// Private field.
        /// </summary>
        protected IFilterConfigurationLibrary _filterConfigActions;

        /// <summary>
        /// Library property
        /// </summary>
        public IFilterConfigurationLibrary FilterConfigActions
        {
            get
            {
                if (_filterConfigActions == null) _filterConfigActions = new FilterConfigurationLibrary(this);
                return _filterConfigActions;
            }
            set { _filterConfigActions = value; }
        }

        /// <summary>
        /// Private field.
        /// </summary>
        protected IProgressLogEntryLibrary _progressLogEntryActions;

        /// <summary>
        /// Library property
        /// </summary>
        public IProgressLogEntryLibrary ProgressLogEntryActions
        {
            get
            {
                if (_progressLogEntryActions == null) _progressLogEntryActions = new ProgressLogEntryLibrary(this);
                return _progressLogEntryActions;
            }
            set { _progressLogEntryActions = value; }
        }

        /// <summary>
        /// Private field
        /// </summary>
        protected IFileImportLibrary _fileImportActions;

        /// <summary>
        /// FileImport Library access.
        /// </summary>
        public IFileImportLibrary FileImportActions
        {
            get
            {
                if (_fileImportActions == null) _fileImportActions = new FileImportLibrary(this);
                return _fileImportActions;
            }
            set { _fileImportActions = value; }
        }

        /// <summary>
        /// Private fields
        /// </summary>
        protected IChartDefinitionLibrary _chartDefinitionActions;

        /// <summary>
        /// ChartDefinition library
        /// </summary>
        public IChartDefinitionLibrary ChartDefinitionActions
        {
            get
            {
                if (_chartDefinitionActions == null) _chartDefinitionActions = new ChartDefinitionLibrary(this);
                return _chartDefinitionActions;
            }
            set { _chartDefinitionActions = value; }
        }

        /// <summary>
        /// Private item.  
        /// </summary>
        protected IHistoryChartDataLibrary _historyChartDataActions;

        /// <summary>
        /// HistoryChartData Library
        /// </summary>
        public IHistoryChartDataLibrary HistoryChartDataActions
        {
            get
            {
                if (_historyChartDataActions == null) _historyChartDataActions = new HistoryChartDataLibrary(this);
                return _historyChartDataActions;
            }
            set { _historyChartDataActions = value; }
        }

        /// <summary>
        /// Private member
        /// </summary>
        protected IGroupedCompletionChartLibrary _groupedCompletionChartActions;

        /// <summary>
        /// Library for GroupedCompletionChart.
        /// </summary>
        public IGroupedCompletionChartLibrary GroupedCompletionChartActions
        {
            get
            {
                if (_groupedCompletionChartActions == null)
                    _groupedCompletionChartActions = new GroupedCompletionChartLibrary(this);
                return _groupedCompletionChartActions;
            }
            set { _groupedCompletionChartActions = value; }
        }

        /// <summary>
        /// Returns a queryable set of TaskProgress values filtered by the values defined by the filterParameters.
        /// </summary>
        /// <param name="jobID">The ID of the Job being viewed.</param>
        /// <param name="filterParams">A dictionary of items that will restrict the list of values returned.</param>
        /// <returns></returns>
        public IQueryable<TaskProgress> FilterTaskProgress(long jobID, FilterParameters filterParams)
        {
            return FilterTaskProgressQueryable(jobID, filterParams);
        }

        /// <summary>
        /// Creates an IQueryable based on the application of the FilterParameters passed against the
        /// TaskProgress and related tables.
        /// </summary>
        /// <param name="jobID">The ID of the job to query for results.</param>
        /// <param name="filterParams">The set of criteria to use for the response.</param>
        /// <returns></returns>
        internal IQueryable<TaskProgress> FilterTaskProgressQueryable(long jobID, FilterParameters filterParams)
        {
            if (jobID < 1) throw new ArgumentException("JobID is not within a valid range.", "jobID");

            var filteredList = GetRepository<TaskProgress>().All.Where(tp => tp.JobID == jobID);

            //  Loop through each dictionary item
            foreach (var item in filterParams)
            {
                switch (item.Key)
                {
                    case FilterValueType.Commodity:
                        var values1 = item.Value as IEnumerable<long>;
                        if (values1 != null)
                        {
                            CommodityLibrary.AttachTaskProgressFilter(ref filteredList, values1);
                        }
                        break;

                    case FilterValueType.CompletionState:
                        var values2 = item.Value as IEnumerable<CompletionState>;
                        if (values2 != null)
                        {
                            TaskProgressLibrary.AttachCompletionStateFilter(ref filteredList, values2);
                        }
                        break;

                    case FilterValueType.CraftInfo:
                        var values3 = item.Value as IEnumerable<long>;
                        CraftInfoLibrary.AttachTaskProgressFilter(ref filteredList, values3);
                        break;

                    case FilterValueType.None:
                        break;

                    case FilterValueType.ObjectType:
                        var values4 = item.Value as IEnumerable<long>;
                        ObjectTypeLibrary.AttachTaskProgressFilter(ref filteredList, values4);
                        break;

                    case FilterValueType.Phase:
                        var values5 = item.Value as IEnumerable<long>;
                        ProjectPhaseLibrary.AttachTaskProgressFilter(ref filteredList, values5);
                        break;

                    case FilterValueType.ProjectArea:
                        var values6 = item.Value as IEnumerable<long>;
                        ProjectAreaLibrary.AttachTaskProgressFilter(ref filteredList, values6);
                        break;

                    case FilterValueType.ProjectSystem:
                        var values7 = item.Value as IEnumerable<long>;
                        ProjectSystemLibrary.AttachTaskProgressFilter(ref filteredList, values7);
                        break;

                    case FilterValueType.Search:

                        // implement the search as a cross-object search
                        var values8 = item.Value as IEnumerable<string>;
                        AttachTaskProgressSearchFilter(ref filteredList, values8);

                        // Wishlist: Add the ability to search by multiple terms.
                        break;

                    case FilterValueType.Task:
                        var values9 = item.Value as IEnumerable<long>;
                        TaskLibrary.AttachTaskProgressFilter(ref filteredList, values9);
                        break;

                    case FilterValueType.TrackingID:
                        var values10 = item.Value as IEnumerable<string>;
                        TrackedItemLibrary.AttachTaskProgressTrackingIDFilter(ref filteredList, values10);
                        break;

                    case FilterValueType.UserDefinedProperty1:
                        var values11 = item.Value as IEnumerable<string>;
                        TrackedItemLibrary.AttachTaskProgressUDPFilter(ref filteredList, values11, 1);
                        break;

                    case FilterValueType.UserDefinedProperty2:
                        var values12 = item.Value as IEnumerable<string>;
                        TrackedItemLibrary.AttachTaskProgressUDPFilter(ref filteredList, values12, 2);
                        break;

                    case FilterValueType.UserDefinedProperty3:
                        var values13 = item.Value as IEnumerable<string>;
                        TrackedItemLibrary.AttachTaskProgressUDPFilter(ref filteredList, values13, 3);
                        break;

                    case FilterValueType.WorkCrew:
                        var values14 = item.Value as IEnumerable<long>;
                        ProgressLogEntryLibrary.AttachTaskProgressWorkCrewFilter(ref filteredList, values14,
                            GetRepository<ProgressLogEntry>());
                        break;

                    default:
                        break;
                }
            }
            filteredList = FilterDeletedFromTaskProgress(filteredList);
            return filteredList.OrderBy(tp => tp.TrackedItem.TrackingID);
        }

        /// <summary>
        /// Modifies the selection criteria of the IQueryable passed to eliminate deleted items.
        /// </summary>
        /// <param name="src">An IQueryable object for TaskProgress items.</param>
        /// <returns>An IQueryable of the same type, filtering deleted items.</returns>
        internal static IQueryable<TaskProgress> FilterDeletedFromTaskProgress(IQueryable<TaskProgress> src)
        {
            return (from tp in src
                where tp.JobInfo != null && !tp.JobInfo.IsDeleted // Job is not deleted
                      && tp.TrackedItem != null && !tp.TrackedItem.IsDeleted // TrackedItem not deleted
                      && tp.Task != null && !tp.Task.IsDeleted // Task not deleted
                      && !tp.IsDeleted
                // TaskProgress not deleted
                select tp);
        }

        /// <summary>
        /// Returns the set of ProjectSystem selections based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        public IEnumerable<ProjectSystem> GetProjectSystemsForFilter(long jobID, FilterParameters filterParams)
        {
            var filtered = FilterTaskProgressWithExclusion(jobID, filterParams, FilterValueType.ProjectSystem);
            return (from tp in filtered
                where tp.TrackedItem.ProjectSystem != null
                select tp.TrackedItem.ProjectSystem).Distinct().OrderBy(sys => sys.SystemNumber).ToList();
        }

        /// <summary>
        /// Returns the set of ProjectArea selections based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        public IEnumerable<ProjectArea> GetProjectAreasForFilter(long jobID, FilterParameters filterParams)
        {
            var filtered = FilterTaskProgressWithExclusion(jobID, filterParams, FilterValueType.ProjectArea);
            return (from tp in filtered
                where tp.TrackedItem.ProjectArea != null
                select tp.TrackedItem.ProjectArea).Distinct().OrderBy(area => area.AreaNumber).ToList();
        }

        /// <summary>
        /// Returns the set of ObjectType selections based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        public IEnumerable<ObjectType> GetObjectTypesForFilter(long jobID, FilterParameters filterParams)
        {
            var typeToExclude = FilterValueType.ObjectType;
            var filtered = FilterTaskProgressWithExclusion(jobID, filterParams, typeToExclude);
            return (from tp in filtered
                where tp.TrackedItem.ObjectType != null
                select tp.TrackedItem.ObjectType).Distinct().OrderBy(ot => ot.Name).ToList();
        }

        /// <summary>
        /// Returns the set of CraftInfo selections based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        public IEnumerable<CraftInfo> GetCraftInfoValuesForFilter(long jobID, FilterParameters filterParams)
        {
            var typeToExclude = FilterValueType.CraftInfo;
            var filtered = FilterTaskProgressWithExclusion(jobID, filterParams, typeToExclude);
            return (from tp in filtered
                where tp.TrackedItem.CraftInfo != null
                select tp.TrackedItem.CraftInfo).Distinct().OrderBy(ci => ci.CraftCode).ToList();
        }

        public IEnumerable<ProjectSystem> GetProjectSystemTree(long jobId)
        {
            return DAL.GetProjectSystemTree(jobId);
        }

        /// <summary>
        /// Returns the set of values for User Defined Property 1 based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        public IEnumerable<string> GetUserDefinedVals_1_ForFilter(long jobID, FilterParameters filterParams)
        {
            var typeToExclude = FilterValueType.UserDefinedProperty1;
            var filtered = FilterTaskProgressWithExclusion(jobID, filterParams, typeToExclude);
            return (from tp in filtered
                where tp.JobInfo != null && tp.JobInfo.UDPName1 != null && tp.JobInfo.UDPName1 != ""
                      && tp.TrackedItem.UDPValue1 != null
                select tp.TrackedItem.UDPValue1).Distinct().OrderBy(udp => udp).ToList();
        }

        /// <summary>
        /// Returns the set of values for User Defined Property 2 based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        public IEnumerable<string> GetUserDefinedVals_2_ForFilter(long jobID, FilterParameters filterParams)
        {
            var typeToExclude = FilterValueType.UserDefinedProperty2;
            var filtered = FilterTaskProgressWithExclusion(jobID, filterParams, typeToExclude);
            return (from tp in filtered
                where tp.JobInfo != null && tp.JobInfo.UDPName2 != null && tp.JobInfo.UDPName2 != ""
                      && tp.TrackedItem.UDPValue2 != null
                select tp.TrackedItem.UDPValue2).Distinct().OrderBy(udp => udp).ToList();
        }

        /// <summary>
        /// Returns the set of values for User Defined Property 3 based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        public IEnumerable<string> GetUserDefinedVals_3_ForFilter(long jobID, FilterParameters filterParams)
        {
            var typeToExclude = FilterValueType.UserDefinedProperty3;
            var filtered = FilterTaskProgressWithExclusion(jobID, filterParams, typeToExclude);
            return (from tp in filtered
                where tp.JobInfo != null && tp.JobInfo.UDPName3 != null && tp.JobInfo.UDPName3 != ""
                      && tp.TrackedItem.UDPValue3 != null
                select tp.TrackedItem.UDPValue1).Distinct().OrderBy(udp => udp).ToList();
        }

        /// <summary>
        /// Returns the set of Commodity selections based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        public IEnumerable<Commodity> GetCommoditiesForFilter(long jobID, FilterParameters filterParams)
        {
            var typeToExclude = FilterValueType.Commodity;
            var filtered = FilterTaskProgressWithExclusion(jobID, filterParams, typeToExclude);
            return (from tp in filtered
                where tp.Commodity != null
                select tp.Commodity).Distinct().OrderBy(c => c.CommodityCode).ToList();
        }

        /// <summary>
        /// Returns the set of Task selections based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        public IEnumerable<Task> GetTasksForFilter(long jobID, FilterParameters filterParams)
        {
            var typeToExclude = FilterValueType.Task;
            var filtered = FilterTaskProgressWithExclusion(jobID, filterParams, typeToExclude);

            return (from tp in filtered
                where tp.Task != null
                select tp.Task).Distinct().OrderBy(t => t.DisplayOrder).ToList();
        }

        /// <summary>
        /// Returns the set of WorkCrew selections based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        public IEnumerable<WorkCrew> GetWorkCrewsForFilter(long jobID, FilterParameters filterParams)
        {
            var typeToExclude = FilterValueType.WorkCrew;
            var filtered = FilterTaskProgressWithExclusion(jobID, filterParams, typeToExclude);
            var tpIdSet = from tp in filtered
                select tp.ID;
            var progressLog = GetRepository<ProgressLogEntry>().All.Where(ple => ple.JobID == jobID
                                                                                 && ple.TaskProgressID.HasValue
                                                                                 &&
                                                                                 tpIdSet.Contains(
                                                                                     ple.TaskProgressID.Value));
            var resultSet = from ple in progressLog
                where ple.WorkCrewID.HasValue && ple.WorkCrew != null
                select ple.WorkCrew;
            return resultSet;
        }

        /// <summary>
        /// Returns an IQueryable of TaskProgress items by applying items in the FilterParameters passed except for the
        /// FilterValueType specified.  Provides a starter IQueryable for retrieving all possible values for the specified
        /// FilterValueType.
        /// </summary>
        /// <param name="jobID">The ID of the job to use for filtering.</param>
        /// <param name="filterParams">The starting parameters for the filtered list.</param>
        /// <param name="typeToExclude">The FilterValueType to ignore when querying the data.</param>
        /// <returns></returns>
        private IQueryable<TaskProgress> FilterTaskProgressWithExclusion(long jobID, FilterParameters filterParams,
            FilterValueType typeToExclude)
        {
            var prm = filterParams.Clone();
            prm.Remove(typeToExclude);
            var filtered = FilterTaskProgressQueryable(jobID, prm);
            filtered = FilterDeletedFromTaskProgress(filtered);
            return filtered;
        }

        /// <summary>
        /// Modifies the filters on the IQueryable passed to specifically select items that match a string value.
        /// </summary>
        /// <param name="filteredList">The IQueryable to modify, passed as a reference.</param>
        /// <param name="values">An IEnumerable to use to search.  Only the last item in the IEnumerable will be used.</param>
        public static void AttachTaskProgressSearchFilter(ref IQueryable<TaskProgress> filteredList,
            IEnumerable<string> values)
        {
            if (values != null && values.Count() > 0)
            {
                var srchval = values.Last();
                filteredList =
                    filteredList.Where(tp => (tp.TrackedItem != null && (tp.TrackedItem.TrackingID.Contains(srchval)
                                                                         ||
                                                                         (tp.TrackedItem.UDPValue1 != null &&
                                                                          tp.TrackedItem.UDPValue1.Contains(srchval))
                                                                         ||
                                                                         (tp.TrackedItem.UDPValue2 != null &&
                                                                          tp.TrackedItem.UDPValue2.Contains(srchval))
                                                                         ||
                                                                         (tp.TrackedItem.UDPValue3 != null &&
                                                                          tp.TrackedItem.UDPValue3.Contains(srchval))
                                                                         ||
                                                                         (tp.TrackedItem.CraftID.HasValue &&
                                                                          tp.TrackedItem.CraftInfo != null &&
                                                                          tp.TrackedItem.CraftInfo.CraftCode.Contains(
                                                                              srchval))
                                                                         ||
                                                                         (tp.TrackedItem.SystemID.HasValue &&
                                                                          tp.TrackedItem.ProjectSystem.SystemNumber
                                                                              .Contains(srchval))
                                                                         ||
                                                                         (tp.TrackedItem.AreaID.HasValue &&
                                                                          tp.TrackedItem.ProjectArea.AreaNumber.Contains
                                                                              (srchval))))
                                             || tp.Task.Title.Contains(srchval) ||
                                             tp.Task.ProjectPhase.Title.Contains(srchval)
                                             ||
                                             (tp.CommodityID.HasValue && tp.Commodity != null &&
                                              tp.Commodity.CommodityCode.Contains(srchval))
                        );
            }
        }

        /// <summary>
        /// Provides the IObjectLibrary instance for the specified type.
        /// </summary>
        /// <typeparam name="DOType">A subclass of DomainObject with a parameterless constructor.</typeparam>
        /// <returns></returns>
        public IObjectLibrary<DOType> GetLibrary<DOType>() where DOType : DomainObject, new()
        {
            // In theory, with the addition of a generic-driven concrete library class, this method
            // could be altered to automatically handle initialization of new class libraries without
            // custom functionality, to minimize the amount of code needed to rapidly roll out new data access.

            IObjectLibrary<DOType> result = null;
            var targetType = typeof(DOType);
            var targetName = targetType.Name;

            switch (targetName)
            {
                case "JobInfo":
                    result = (IObjectLibrary<DOType>) JobActions;
                    break;

                case "ProjectPhase":
                    result = (IObjectLibrary<DOType>) PhaseActions;
                    break;

                case "ObjectType":
                    result = (IObjectLibrary<DOType>) ObjectTypeActions;
                    break;

                case "ProjectSystem":
                    result = (IObjectLibrary<DOType>) SystemActions;
                    break;

                case "ProjectArea":
                    result = (IObjectLibrary<DOType>) AreaActions;
                    break;

                case "CraftInfo":
                    result = (IObjectLibrary<DOType>) CraftActions;
                    break;

                case "Commodity":
                    result = (IObjectLibrary<DOType>) CommodityActions;
                    break;

                case "TrackedItem":
                    result = (IObjectLibrary<DOType>) TrackedItemActions;
                    break;

                case "TaskProgress":
                    result = (IObjectLibrary<DOType>) TaskProgressActions;
                    break;

                case "TOPUser":
                    result = (IObjectLibrary<DOType>) UserActions;
                    break;

                case "WorkCrew":
                    result = (IObjectLibrary<DOType>) WorkCrewActions;
                    break;

                case "FilterConfiguration":
                    result = (IObjectLibrary<DOType>) FilterConfigActions;
                    break;

                case "ProgressLogEntry":
                    result = (IObjectLibrary<DOType>) ProgressLogEntryActions;
                    break;

                case "FileImport":
                    result = (IObjectLibrary<DOType>) FileImportActions;
                    break;
                default:
                    break;
            }

            return result;
        }

        /// <summary>
        /// Returns the IRepository instance being provided by the Data Access Layer for the Domain Object type specified.
        /// </summary>
        /// <typeparam name="DOType">A subclass of DomainObject with a parameterless constructor.</typeparam>
        /// <returns></returns>
        public IRepository<DOType> GetRepository<DOType>() where DOType : DomainObject, new()
        {
            return DAL.GetRepository<DOType>();
        }

        /// <summary>
        /// Updates the percent complete of a TaskProgress item, and logs the progress the Progress Log, returning the ProgressLogEntry
        /// instance that is generated.
        /// </summary>
        /// <param name="jobID">The ID of the Job to which this progress log entry is being written.  Must match the job of
        /// the TaskProgress item being updated.</param>
        /// <param name="taskProgressID">A long integer identifying the TaskProgess item that will be updated.</param>
        /// <param name="newPercentComplete">A decimal value representing the PercentComplete value being set.</param>
        /// <param name="workDate">The date that the work causing the progress was actually performed.</param>
        /// <param name="workCrewID">The ID value of the WorkCrew that performed the work.</param>
        /// <param name="userID">The ID of the user performing the logging operation.</param>
        /// <returns></returns>
        public ProgressLogEntry UpdateProgress(long jobID, long taskProgressID, decimal newPercentComplete,
            DateTime workDate, long workCrewID, long userID)
        {
            return UpdateProgressCommitOptional(jobID, taskProgressID, newPercentComplete, workDate, workCrewID, userID,
                true);
        }

        /// <summary>
        /// Updates the percent complete of a TaskProgress item, and logs the progress the Progress Log, returning the ProgressLogEntry
        /// instance that is generated.
        /// </summary>
        /// <param name="jobID">The ID of the Job to which this progress log entry is being written.  Must match the job of
        /// the TaskProgress item being updated.</param>
        /// <param name="taskProgressID">A long integer identifying the TaskProgess item that will be updated.</param>
        /// <param name="newPercentComplete">A decimal value representing the PercentComplete value being set.</param>
        /// <param name="workDate">The date that the work causing the progress was actually performed.</param>
        /// <param name="workCrewID">The ID value of the WorkCrew that performed the work.</param>
        /// <param name="userID">The ID of the user performing the logging operation.</param>
        /// <param name="useCommit">If false, defers the database commit to push the new values to the databsae.</param>
        /// <returns></returns>
        public ProgressLogEntry UpdateProgressCommitOptional(long jobID, long taskProgressID, decimal newPercentComplete,
            DateTime workDate, long workCrewID, long userID, bool useCommit = true)
        {
            // Validations
            BaseClasses.ObjectLibraryBase<TaskProgress>.throwOnInvalidUserIDRange(userID);

            //BaseClasses.ObjectLibraryBase<TaskProgress>.throwOnItemNotFound

            if (workDate != DateTime.MinValue)
            {
                workDate = workDate.Date;
            }
            // Build the Progress Log Entry record.
            ProgressLogEntry entry = null;

            decimal oldPC = 0;

            var tp = TaskProgressActions.Find(taskProgressID);

            var allPLE =
                GetRepository<ProgressLogEntry>()
                    .All.Where(ple => ple.TaskProgressID == taskProgressID && ple.WorkDate.HasValue);

            var prevPLE =
                allPLE.Where(p => p.WorkDate <= workDate).OrderBy(ple1 => ple1.WorkDate).ThenBy(ple2 => ple2.EntryDate);

            if (prevPLE.Count() > 0)
            {
                var prev = prevPLE.ToList().Last();
                if (prev.NewPercentComplete.HasValue)
                {
                    oldPC = prev.NewPercentComplete.Value;
                }
            }

            ((TaskProgressLibrary) TaskProgressActions).VerifyUserPermission(UserActionType.Update, tp, userID);

            if (tp != null)
            {
                if (tp.JobID != jobID)
                    throw new ArgumentException(
                        "The Job ID passed does not match the job of the Task Progress item indicated.", "jobID");

                entry = new ProgressLogEntry();

                entry.EntryDate = DateTime.Now;
                if (workDate == DateTime.MinValue)
                {
                    entry.WorkDate = null;
                }
                else
                {
                    entry.WorkDate = workDate;
                }

                var job = (tp.JobInfo ?? JobActions.Find(jobID));
                if (job == null)
                    throw new ArgumentException(
                        "Attempted to log progress on a non-existant job.  ID: " + jobID.ToString(), "jobID");

                entry.JobID = jobID;
                entry.JobName = job.Title;

                // User information
                var entryUser = UserActions.Find(userID);
                if (entryUser == null)
                    throw new ArgumentException(
                        "Attempted to log progress with non-existant user id. ID: " + userID.ToString(), "userID");

                entry.UserID = userID;
                entry.EntryUsername = entryUser.Login;
                entry.EntryUserFirstName = entryUser.FirstName;
                entry.EntryUserLastName = entryUser.LastName;

                // Commodity info
                entry.CommodityID = tp.CommodityID;

                if (entry.CommodityID.HasValue)
                {
                    // set entry values that can be derived from the Commodity
                    var commod = (tp.Commodity ?? CommodityActions.Find(entry.CommodityID.Value));
                    if (commod != null)
                    {
                        entry.CommodityCode = commod.CommodityCode;
                        entry.CommodityDescription = commod.Description;
                        entry.TaskManHours = CommodityActions.GetManHoursPerItem(entry.CommodityID.Value);
                    }
                }

                // Task info
                entry.TaskID = tp.TaskID;
                var taskInfo = (tp.Task ?? TaskActions.Find(entry.TaskID.Value));
                if (taskInfo != null)
                {
                    entry.TaskTitle = taskInfo.Title;
                }

                // TaskProgress
                entry.TaskProgressID = tp.ID;

                // ProjectPhase
                if (taskInfo != null)
                {
                    entry.PhaseID = (taskInfo.PhaseID == 0 ? (long?) null : taskInfo.PhaseID);
                    if (entry.PhaseID.HasValue)
                    {
                        var phaseInfo = (taskInfo.ProjectPhase ?? PhaseActions.Find(entry.PhaseID.Value));
                        if (phaseInfo != null)
                        {
                            entry.PhaseTitle = phaseInfo.Title;
                        }
                    }
                }

                // TrackedItem
                entry.TrackedItemID = (tp.TrackedItemID == 0 ? (long?) null : tp.TrackedItemID);
                var tracked = tp.TrackedItem;
                if (tracked != null)
                {
                    entry.TrackingID = tracked.TrackingID;
                    entry.UDP1_Value = tracked.UDPValue1;
                    entry.UDP2_Value = tracked.UDPValue2;
                    entry.UDP3_Value = tracked.UDPValue3;

                    // ObjectType
                    entry.ObjectTypeID = tracked.ObjectTypeID;
                    if (entry.ObjectTypeID.HasValue)
                    {
                        var ot = (tracked.ObjectType ?? ObjectTypeActions.Find(entry.ObjectTypeID.Value));
                        if (ot != null) entry.ObjectTypeName = ot.Name;
                    }

                    // ProjectSystem
                    entry.ProjectSystemID = tracked.SystemID;
                    if (tracked.ProjectSystem != null)
                    {
                        entry.ProjectSystemNumber = tracked.ProjectSystem.SystemNumber;
                    }

                    // ProjectArea
                    entry.ProjectAreaID = tracked.AreaID;
                    if (tracked.ProjectArea != null)
                    {
                        entry.ProjectAreaNumber = tracked.ProjectArea.AreaNumber;
                    }

                    // CraftInfo
                    entry.CraftInfoID = tracked.CraftID;
                    if (tracked.CraftInfo != null)
                    {
                        entry.CraftCode = tracked.CraftInfo.CraftCode;
                    }
                }

                // Work Crew
                entry.WorkCrewID = (workCrewID == 0 ? (long?) null : workCrewID);
                if (entry.WorkCrewID.HasValue)
                {
                    var wc = WorkCrewActions.Find(workCrewID);
                    if (wc == null)
                        throw new ArgumentException("Attempted to log progress to an invalid work crew ID.",
                            "workCrewID");
                    entry.WorkCrewNumber = wc.CrewNumber;
                    entry.WorkCrewDescription = wc.Description;
                }

                entry.OldPercentComplete = oldPC;
                entry.NewPercentComplete = newPercentComplete;

                var dbTProg = GetRepository<TaskProgress>();
                var dbLog = GetRepository<ProgressLogEntry>();

                // If this is the latest entry in the list, set the
                // percent complete of the current item.  Unless the work date is null
                var isLatest = false;
                if (entry.WorkDate.HasValue)
                {
                    var laterPLE =
                        allPLE.Where(p => p.WorkDate > workDate).OrderBy(p1 => p1.WorkDate).ThenBy(p1 => p1.EntryDate);

                    isLatest = (laterPLE.Count() == 0);

                    if (isLatest)
                    {
                        tp.SetPercentComplete(newPercentComplete);
                        tp.LastChanged = DateTime.Now;
                    }
                    else
                    {
                        // Adjust the OldPercentComplete on the first entry for the next day
                        var nextPLE = laterPLE.First();
                        nextPLE.OldPercentComplete = newPercentComplete;
                        nextPLE.LastChanged = DateTime.Now;
                    }
                }
                dbLog.Insert(entry);

                if (useCommit)
                {
                    dbLog.Save();
                }

                TaskProgress._ResetInitialPercentComplete(tp);

                return entry;
            }
            else
            {
                throw new ArgumentException("Attempted to log progress against an invalid Task Progress ID.",
                    "taskProgressID");
            }
        }

        /// <summary>
        /// Retrieves a set of Job ID values and work dates that have progress that has not been
        /// updated in the ProgressHistory.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UnrecognizedProgress> GetUnrecognizedProgress()
        {
            return DAL.GetUnrecognizedProgress();
        }

        /// <summary>
        /// Updates the ProgressHistory values for the job and date specified.
        /// </summary>
        /// <param name="jobID">ID of the job entries to update.</param>
        /// <param name="workDate">The work date to check for progress.</param>
        public void UpdateProgressHistory(long jobID, DateTime workDate)
        {
            DAL.UpdateProgressHistory(jobID, workDate);
        }

        /// <summary>
        /// Updates the ProgressHistory data for all jobs.
        /// </summary>
        public void UpdateAllProgressHistory()
        {
            var updates = GetUnrecognizedProgress();
            foreach (var up in updates)
            {
                UpdateProgressHistory(up.JobID, up.WorkDate);
            }
        }

        #endregion ITOPControlLibrary Members

        #region ITOPControlLibrary Members

        /// <summary>
        /// Returns a decimal value indicating the percent complete over the entirety of the Filtered Values.
        /// </summary>
        /// <param name="jobID">The Job ID for which to calculate.</param>
        /// <param name="filterParams">A dictionary of items that will restrict the list of values returned.</param>
        /// <returns></returns>
        public decimal CalculateTotalPercentCompleteForFilter(long jobID, FilterParameters filterParams)
        {
            decimal result = 1;
            var job = JobActions.Find(jobID);
            if (job == null) throw new ArgumentException("Cannot find a job with the ID " + jobID.ToString(), "jobID");

            var filtered = FilterTaskProgress(jobID, filterParams);
            filtered = FilterDeletedFromTaskProgress(filtered);
            if (filtered.Count() > 0)
            {
                if (job.WeightedCalculations)
                {
                    var compHrs = filtered.Sum(tp => tp.PercentComplete * tp.ManHoursPerItem);
                    var totHrs = filtered.Sum(tp => tp.ManHoursPerItem);
                    if (totHrs == 0)
                    {
                        result = 0;
                    }
                    else
                    {
                        result = compHrs / totHrs;
                    }
                }
                else
                {
                    result = filtered.Average(tp => tp.PercentComplete);
                }
            }
            return result;
        }

        /// <summary>
        /// Gets a queryable pointing to the full dataset of the defined type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IQueryable<T> GetQueryable<T>()
            where T : class
        {
            return DAL.GetQueryable<T>();
        }

        #endregion

        #region ITOPControlLibrary Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobID"></param>
        /// <param name="filterParams"></param>
        /// <param name="grouping"></param>
        /// <param name="groupValue"></param>
        /// <returns></returns>
        public HistoryChartData GetHistoryChartData(long jobID, FilterParameters filterParams, FilterValueType grouping,
            object groupValue)
        {
            var helper = new ChartingHelper(this);

            return helper.GetHistoryChartData(jobID, filterParams, grouping, groupValue);
        }

        #endregion

        #region ITOPControlLibrary Members

        /// <summary>
        /// Returns a set of percentages along with the Display Name of the group they belong to.
        /// </summary>
        /// <param name="jobID">ID of the job to be used.</param>
        /// <param name="grouping">A FilterValueType indicating the kind of value to group by.</param>
        /// <returns></returns>
        public IEnumerable<GroupedCompletionChartDataPoint> GetGroupedCompletions(long jobID, FilterValueType grouping)
        {
            var helper = new ChartingHelper(this);

            return helper.GetGroupedCompletions(jobID, grouping);
        }

        /// <summary>
        /// Returns a set of PhaseSummary instances representing the state of each phase.
        /// </summary>
        /// <param name="jobID">ID of the job for which the phases should be retrieved.</param>
        /// <returns></returns>
        public IEnumerable<PhaseSummary> GetPhaseSummaries(long jobID)
        {
            var helper = new ChartingHelper(this);

            return helper.GetPhaseSummaries(jobID);
        }

        /// <summary>
        /// Updates all historical and reporting data.  May take several moments.
        /// </summary>
        public void UpdateReportingData()
        {
            var ch = new ChartingHelper(this);
            ch.UpdateAllCharts();
        }

        /// <summary>
        /// Removes the current chart data for the Job indicated or All jobs if a Zero value is passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to purge, or 0 if all jobs should have their current chart data wiped.</param>
        public void PurgeReportingData(long jobID = 0)
        {
            DAL.PurgeReportingData(jobID);
        }

        #endregion

        /// <summary>
        /// Retrieves currently persisted value of the item for the property specified.
        /// </summary>
        /// <typeparam name="DOType">The DomainObject Type of the item.</typeparam>
        /// <typeparam name="TProperty">The datatype of the Property being retrieved</typeparam>
        /// <param name="item">The item for which the committed property value should be retrieved.</param>
        /// <param name="property">An expression identifying the property</param>
        /// <returns></returns>
        public TProperty GetCommittedPropertyValue<DOType, TProperty>(DOType item,
            Expression<Func<DOType, TProperty>> property) where DOType : DomainObject
        {
            return DAL.GetCommittedPropertyValue(item, property);
        }

        /// <summary>
        /// Sets the Commodity for all TaskProgress items within the identified tasks, with certain specific tolerances identified by arguments.
        /// </summary>
        /// <param name="taskID">Required, the TaskID to use when selecting items to change.</param>
        /// <param name="commodityID">The ID of the new Commodity to set on these task items. Will clear the commodity if null.</param>
        /// <param name="matchCommodity">If set, the operation will only change the commodity of items that have a commodity ID matching this value.  If null, will always change the current Commodities set on TaskProgress items.</param>
        /// <param name="setNulls">If true, this operation will set values that currently have no commodity set, as long as they are part of the task.</param>
        /// <returns></returns>
        public int PropogateCommodityByTask(long taskID, long? commodityID, long? matchCommodity = null,
            bool setNulls = true)
        {
            return DAL.PropogateCommodityByTask(taskID, commodityID, matchCommodity, setNulls);
        }

        /// <summary>
        /// Using one of the optional arguments passed, calculates the Historical Chart Values based on a stored procedure.
        /// </summary>
        /// <param name="jobID">ID of the job to use to filter history results.</param>
        /// <param name="phaseID">ID of phase by which to filter.</param>
        /// <param name="taskID">ID of the task by which to filter.</param>
        /// <param name="trackedItemID">ID of the task by which to filter.</param>
        /// <param name="systemID">ID of the system by which to filter.</param>
        /// <param name="commodityID">ID of the system by which to filter.</param>
        /// <param name="areaID">ID of the area by which to filter.</param>
        /// <param name="objectTypeID">ID of the object type by which to filter.</param>
        /// <param name="craftID">ID of the craft by which to filter.</param>
        /// <param name="udpValue1">The first UDP Value to use as a filter.</param>
        /// <param name="udpValue2">The second UDP Value to use as a filter.</param>
        /// <param name="udpValue3">The third UDP Value to use as a filter.</param>
        /// <returns></returns>
        public List<RawHistoryDataPoint> SelectNewHistoryChartData(long? jobID = null, long? phaseID = null,
            long? taskID = null, long? trackedItemID = null,
            long? systemID = null, long? commodityID = null, long? areaID = null, long? objectTypeID = null,
            long? craftID = null, string udpValue1 = null, string udpValue2 = null, string udpValue3 = null,
            DateTime? workDate = null)
        {
            var result = new List<RawHistoryDataPoint>();

            result = DAL.SelectNewHistoryChartData(jobID, phaseID, taskID, trackedItemID, systemID, commodityID, areaID,
                objectTypeID, craftID, udpValue1, udpValue2, udpValue3, workDate);

            return result;
        }
    }
}