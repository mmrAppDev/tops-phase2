﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// Provides Business Process operations for GroupedCompletionChart.
    /// </summary>
    public class GroupedCompletionChartLibrary:JobObjectLibraryBase<GroupedCompletionChart>, IGroupedCompletionChartLibrary
    {

        private Lazy<IRepository<GroupedCompletionChartDataPoint>> _lazyRepDataPoint;

        /// <summary>
        /// Repository for chart data points.
        /// </summary>
        public IRepository<GroupedCompletionChartDataPoint> RepDataPoint
        {
            get
            {
                return _lazyRepDataPoint.Value;
            }
        }

        #region Constructors

        /// <summary>
        /// Creates a new GroupedCompletionChartLibrary with default values.
        /// </summary>
        public GroupedCompletionChartLibrary()
            : base()
        {
            setLazy();
        }
        /// <summary>
        /// Creates a new GroupedCompletionChartLibrary with the Root library passed.
        /// </summary>
        /// <param name="rootLibrary">An object that implements ITOPControlLibrary</param>
        public GroupedCompletionChartLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { setLazy();  }
        /// <summary>
        /// Creates a new GroupedCompletionChartLibrary with the resources passed.
        /// </summary>
        /// <param name="useRepository">The Repository to use</param>
        /// <param name="permissionManager">The IPermissionManager to use.</param>
        public GroupedCompletionChartLibrary(IRepository<GroupedCompletionChart> useRepository = null, IPermissionManager permissionManager = null)
            : base(useRepository, permissionManager)
        { setLazy(); }

        #endregion

        private void setLazy()
        {
            _lazyRepDataPoint = new Lazy<IRepository<GroupedCompletionChartDataPoint>>(() => Root.GetRepository<GroupedCompletionChartDataPoint>());
        }

        /// <summary>
        /// Returns the item with a DisplayName matching the string passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to check for the value.</param>
        /// <param name="name">The string value to look for.</param>
        /// <returns></returns>
        public override GroupedCompletionChart FindByName(long jobID, string name)
        {
            return Repository.All.Where(x => x.JobID == jobID && x.DisplayName == name).FirstOrDefault();
        }

        /// <summary>
        /// Copies values that are specific to the GroupedCompletionChart from one object to another.
        /// </summary>
        /// <param name="from">Instance to copy from.</param>
        /// <param name="to">Instance to copy to.</param>
        protected override void copyClassValues(GroupedCompletionChart from, GroupedCompletionChart to)
        {
            to.ChartDefinitionID = from.ChartDefinitionID;
            to.RunDate = from.RunDate;
        }

        #region IGroupedCompletionChartLibrary Members

        /// <summary>
        /// Returns the GroupedCompletionChart instance generated for the specified job and FilterValueType.
        /// </summary>
        /// <param name="jobID">ID of the job to use.</param>
        /// <param name="grouping">The FilterValueType that describes the way the chart is grouped.</param>
        /// <returns></returns>
        public GroupedCompletionChart GetByDefinition(long jobID, FilterValueType grouping)
        {
            return Repository.All.Where(x => x.JobID == jobID && x.ChartDefinition.FilterValueTypeValue == (int)grouping).FirstOrDefault();
        }

        /// <summary>
        /// Returns all datapoints for the GroupedCompletionChart indicated.
        /// </summary>
        /// <param name="groupedCompletionChartID">The ID of the Grouped Completion Chart to use.</param>
        /// <returns></returns>
        public IEnumerable<GroupedCompletionChartDataPoint> GetDataPoints(long groupedCompletionChartID)
        {
            return RepDataPoint.All.Where(dp => dp.GroupedCompletionChartID == groupedCompletionChartID).ToList();
        }

        /// <summary>
        /// Stores a single data point for a Grouped Completion chart.
        /// </summary>
        /// <param name="item">A GroupedCompletionChartDataPoint instance that already has data and a connection to
        /// a GroupedCompletionChart.</param>
        public void SaveDataPoint(GroupedCompletionChartDataPoint item)
        {
            var dbItem = RepDataPoint.All.SingleOrDefault(x => x.GroupedCompletionChartID == item.GroupedCompletionChartID
                && x.GroupKey == item.GroupKey);
                
            if (dbItem == null)
            {
                dbItem = new GroupedCompletionChartDataPoint
                {
                     GroupedCompletionChartID = item.GroupedCompletionChartID,
                     GroupKey = item.GroupKey, 
                };
            }
            dbItem.ItemCount = item.ItemCount;
            dbItem.GroupTitle = item.GroupTitle;
            dbItem.PercentComplete = item.PercentComplete;
            dbItem.LastChanged = DateTime.Now;
            if (dbItem.ID == 0) RepDataPoint.Insert(dbItem);
            RepDataPoint.Save();
        }

        /// <summary>
        /// Returns a ValidationResult indicating if the item passed can be added to the data storage.
        /// </summary>
        /// <param name="item">A data point instance that has had all of its values set.</param>
        /// <returns></returns>
        public ValidationResult ValidateDataPoint(GroupedCompletionChartDataPoint item)
        {
            var result = new ValidationResult();

            // HistoryChartDataID should be valid
            var chart = Repository.Find(item.GroupedCompletionChartID);
            if (chart == null) result.AddViolation("The Data Point references a grouped chart that cannot be found.", "GroupedCompletionChartID");
            if (item.PercentComplete < 0.0m) result.AddViolation("The Percent Complete cannot be negative.", "PercentComplete");
            if (item.PercentComplete > 1.0m) result.AddViolation("The Percent Complete cannot be more than 100%", "PercentComplete");
            var dups = RepDataPoint.All.Where(x => x.GroupedCompletionChartID == item.GroupedCompletionChartID
                    && x.GroupKey == item.GroupKey && x.ID != item.ID);
            if (dups.Count() > 0) result.AddViolation("There is already a Data Point in this chart for that grouping.", "GroupKey");

            return result;
        }

        #endregion
    }
}
