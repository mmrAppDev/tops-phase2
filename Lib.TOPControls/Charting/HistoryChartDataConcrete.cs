﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// Provides the implementation for the HistoryChartData class.
    /// </summary>
    public class HistoryChartDataConcrete:HistoryChartData 
    {

        /// <summary>
        /// The target object on which the current focused grouping is based.
        /// </summary>
        internal DomainObject GroupingObject { get; set; }

        /// <summary>
        /// The value that provides the current focus for the grouping.
        /// </summary>
        internal string GroupingValue { get; set; }

        /// <summary>
        /// Casts the grouping object as the specified type.
        /// </summary>
        /// <typeparam name="DOType">The DomainObject type to return.  If object cannot be cast
        /// as that type, will return a null value.</typeparam>
        /// <returns></returns>
        internal DOType CastGroupingObject<DOType>()
            where DOType:DomainObject
        {
            return GroupingObject as DOType;
        }


        internal HistoryChartData ToModel()
        {
            var result = new HistoryChartData
            {
                ChartDefinitionID = this.ChartDefinitionID,
                ChartValues = this.ChartValues,
                CompleteCount = this.CompleteCount,
                ContextTitle = this.ContextTitle,
                GroupKey = this.GroupKey,
                GroupTitle = this.GroupTitle,
                ID = this.ID,
                ItemCount = this.ItemCount,
                JobID = this.JobID,
                LastChanged = this.LastChanged,
                PercentComplete = this.PercentComplete,
                CompletedManHours = this.CompletedManHours,
                TotalManHours = this.TotalManHours,
                RunDate = this.RunDate
            };

            result.HistoryChartDataPoints = new System.Collections.ObjectModel.Collection<HistoryChartDataPoint>();
            foreach (var hcdp in this.HistoryChartDataPoints)
            {
                result.HistoryChartDataPoints.Add(
                    new HistoryChartDataPoint 
                    { 
                        HistoryChartData = result, 
                        CompletedManHours = hcdp.CompletedManHours,
                        TotalManHours = hcdp.TotalManHours,
                        ID = hcdp.ID, 
                        LastChanged = hcdp.LastChanged, 
                        PercentComplete = hcdp.PercentComplete, 
                        WorkDate = hcdp.WorkDate 
                    });
            }

            return result;
        }
        
    }
}
