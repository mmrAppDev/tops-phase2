﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// Provides business logic for ChartDefinition objects.
    /// </summary>
    public class ChartDefinitionLibrary:ObjectLibraryBase<ChartDefinition>,IChartDefinitionLibrary
    {

        #region Constructors

        /// <summary>
        /// Creates a new ChartDefinitionLibrary with default values.
        /// </summary>
        public ChartDefinitionLibrary()
            : base()
        { }
        /// <summary>
        /// Creates a new ChartDefinitionLibrary with the Root library passed.
        /// </summary>
        /// <param name="rootLibrary">An object that implements ITOPControlLibrary</param>
        public ChartDefinitionLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }
        /// <summary>
        /// Creates a new ChartDefinitionLibrary with the resources passed.
        /// </summary>
        /// <param name="useRepository">The Repository to use</param>
        /// <param name="permissionManager">The IPermissionManager to use.</param>
        public ChartDefinitionLibrary(IRepository<ChartDefinition> useRepository = null, IPermissionManager permissionManager = null)
            : base(useRepository, permissionManager)
        { }

        #endregion

        /// <summary>
        /// Copies values specific to the ChartDefinitionLibrary from one intance to another.
        /// </summary>
        /// <param name="from">An instance</param>
        /// <param name="to">An instance</param>
        protected override void copyClassValues(ChartDefinition from, ChartDefinition to)
        {
            to.ChartType = from.ChartType;
            to.FilterValueType = from.FilterValueType;
            to.IsGlobal = from.IsGlobal;
        }

        //public override MMRCommon.Data.ValidationResult Validate(ChartDefinition item)
        //{
        //    var result = base.Validate(item);
        //    return result;
        //}

        #region IChartDefinitionLibrary Members

        /// <summary>
        /// Returns all ChartDefinition values.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ChartDefinition> GetAll()
        {
            return Repository.All.ToList();
        }

        #endregion
    }
}
