﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// Provides business layer operations for HistoryChartData items.
    /// </summary>
    public class HistoryChartDataLibrary : JobObjectLibraryBase<HistoryChartData>, IHistoryChartDataLibrary
    {
        private Lazy<IRepository<HistoryChartDataPoint>> _lazyRepDataPoints;

        /// <summary>
        /// The HistoryChartDataPoint Repository
        /// </summary>
        public IRepository<HistoryChartDataPoint> RepDataPoints
        {
            get { return _lazyRepDataPoints.Value; }
        }

        #region Constructors

        /// <summary>
        /// Creates a new HistoryChartDataLibrary with default values.
        /// </summary>
        public HistoryChartDataLibrary()
            : base()
        {
            setLazy();
        }

        /// <summary>
        /// Creates a new HistoryChartDataLibrary with the Root library passed.
        /// </summary>
        /// <param name="rootLibrary">An object that implements ITOPControlLibrary</param>
        public HistoryChartDataLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        {
            setLazy();
        }

        /// <summary>
        /// Creates a new HistoryChartDataLibrary with the resources passed.
        /// </summary>
        /// <param name="useRepository">The Repository to use</param>
        /// <param name="permissionManager">The IPermissionManager to use.</param>
        public HistoryChartDataLibrary(IRepository<HistoryChartData> useRepository = null,
            IPermissionManager permissionManager = null)
            : base(useRepository, permissionManager)
        {
            setLazy();
        }

        #endregion

        private void setLazy()
        {
            _lazyRepDataPoints =
                new Lazy<IRepository<HistoryChartDataPoint>>(() => Root.GetRepository<HistoryChartDataPoint>(), false);
        }

        /// <summary>
        /// Returns the item for a specific job that has a specified DisplayName.
        /// </summary>
        /// <param name="jobID">ID of the job to check.</param>
        /// <param name="name">The Display name to look for.</param>
        /// <returns></returns>
        public override HistoryChartData FindByName(long jobID, string name)
        {
            return Repository.All.Where(x => x.JobID == jobID && x.DisplayName == name).FirstOrDefault();
        }

        /// <summary>
        /// Copies class-specific values from one instance to another.
        /// </summary>
        /// <param name="from">The source data.</param>
        /// <param name="to">The item to copy vaules to.</param>
        protected override void copyClassValues(HistoryChartData from, HistoryChartData to)
        {
            to.ChartDefinitionID = from.ChartDefinitionID;
            to.CompleteCount = from.CompleteCount;
            to.ContextTitle = from.ContextTitle;
            to.GroupKey = from.GroupKey;
            to.GroupTitle = from.GroupTitle;
            to.ItemCount = from.ItemCount;
            to.PercentComplete = from.PercentComplete;
            to.CompletedManHours = from.CompletedManHours;
            to.TotalManHours = from.TotalManHours;
            to.RunDate = from.RunDate;

            // expand this logic

            to.ChartValues = new List<KeyValuePair<DateTime, decimal>>(from.ChartValues);
        }

        #region IHistoryChartDataLibrary Members

        /// <summary>
        /// Retrieves an item by the same values that would be used to generated the item from the database.
        /// </summary>
        /// <param name="jobID">ID of the job.</param>
        /// <param name="parms">A set of FilterParamters to use.</param>
        /// <param name="grouping">A value from the FilterValueType enum indicating how the data is grouped.</param>
        /// <param name="groupValue">A value of the correct type that can be used to identify the exact data item.</param>
        /// <returns></returns>
        public HistoryChartData GetByDefinition(long jobID, FilterParameters parms, FilterValueType grouping,
            object groupValue)
        {
            string findGroupKey = (groupValue == null ? string.Empty : groupValue.ToString());
            var result = Repository.All.FirstOrDefault(x => x.JobID == jobID
                // if we do get to the point of including Filters in the Charts, add params here.
                                                            && x.ChartDefinition.FilterValueTypeValue == (int) grouping
                                                            &&
                                                            x.ChartDefinition.ChartTypeValue ==
                                                            (int) ChartType.Historical
                                                            && x.GroupKey == findGroupKey);
            return result;
        }

        /// <summary>
        /// Returns all data points for a specific HistoryChartData item.
        /// </summary>
        /// <param name="historyChartDataID">The ID of the HistoryChartData item to use.</param>
        /// <returns></returns>
        public IEnumerable<HistoryChartDataPoint> GetDataPoints(long historyChartDataID)
        {
            return RepDataPoints.All.Where(dp => dp.HistoryChartDataID == historyChartDataID).ToList();
        }

        /// <summary>
        /// Stores a data point for a History Chart.
        /// </summary>
        /// <param name="item">The instance containing the data to store.</param>
        public void SaveDataPoint(HistoryChartDataPoint item)
        {
            var dbItem = RepDataPoints.All.SingleOrDefault(dp => dp.HistoryChartDataID == item.HistoryChartDataID
                                                                 && dp.WorkDate == item.WorkDate);
            if (dbItem == null)
            {
                dbItem = new HistoryChartDataPoint
                {
                    HistoryChartDataID = item.HistoryChartDataID,
                    WorkDate = item.WorkDate
                };
            }
            dbItem.PercentComplete = item.PercentComplete;
            dbItem.CompletedManHours = item.CompletedManHours;
            dbItem.TotalManHours = item.TotalManHours;
            dbItem.LastChanged = DateTime.Now;
            if (dbItem.ID == 0) RepDataPoints.Insert(dbItem);
            RepDataPoints.Save();
        }

        /// <summary>
        /// Performs a validation on a potential new or changed HistoryChartData data point.
        /// </summary>
        /// <param name="item">The data point to validate</param>
        /// <returns></returns>
        public ValidationResult ValidateDataPoint(HistoryChartDataPoint item)
        {
            var result = new ValidationResult();

            // HistoryChartDataID should be valid
            var chart = Repository.Find(item.HistoryChartDataID);
            if (chart == null)
                result.AddViolation("The Data Point references a history chart that cannot be found.",
                    "HistoryChartDataID");
            if (item.PercentComplete < 0.0m)
                result.AddViolation("The Percent Complete cannot be negative.", "PercentComplete");
            if (item.PercentComplete > 1.0m)
                result.AddViolation("The Percent Complete cannot be more than 100%", "PercentComplete");
            if (item.TotalManHours < 0)
                result.AddViolation("The Budgeted Man-Hours cannot be negative.", "TotalManHours");
            if (item.CompletedManHours < 0)
                result.AddViolation("The Earned Man-Hours cannot be negative.", "CompletedManHours");

            if (item.WorkDate.Date > DateTime.Now)
                result.AddViolation("The WorkDate cannot be a date in the future.", "WorkDate");

            return result;
        }



        #endregion
    }
}