﻿using System.Collections.Generic;
using System.Linq;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls
{
    /// <summary>
    /// A library class that performs operations specific to the ObjectType class and its instances.
    /// </summary>
    public class ObjectTypeLibrary : JobObjectLibraryBase<ObjectType>, IObjectTypeLibrary
    {
        #region Constructors

        /// <summary>
        /// Creates an instance of ObjectTypeLibrary with default values.
        /// </summary>
        public ObjectTypeLibrary()
            : base()
        { }

        /// <summary>
        /// Creates an instance of ObjecttypeLibrary, setting the Root library and pulling all other resources from it.
        /// </summary>
        /// <param name="rootLibrary">An object that implements ITOPControlLibrary.</param>
        public ObjectTypeLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }

        /// <summary>
        /// Creates an instance of ObjectTypeLibrary with the resource values passed.
        /// </summary>
        /// <param name="useRepository">The IRepository to use.</param>
        /// <param name="permManager">The IPermissionManager to use.</param>
        public ObjectTypeLibrary(IRepository<ObjectType> useRepository = null, IPermissionManager permManager = null)
            : base(useRepository, permManager)
        { }

        #endregion Constructors

        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public override ValidationResult Validate(ObjectType item)
        {
            var result = base.Validate(item);

            if (item != null)
            {
                result.ValidateStringLength(item.Name, 4, 200, "Name");

                var dups = Repository.All.Where(ot => ot.ID != item.ID && ot.JobID == item.JobID && ot.Name == item.Name);
                if (dups.Count() > 0)
                {
                    result.AddViolation("There is already an object type with that name in this job.", "Name");
                }

                if (item.Description != null)
                {
                    result.ValidateStringLength(item.Description, 0, 200, "Description");
                }
            }

            return result;
        }

        /// <summary>
        /// Adds a condition to the IQueryable passed that filters items down to records that have ObjectType ID values
        /// matching the list of values passed.
        /// </summary>
        /// <param name="filteredList">A reference to an IQueryable of TaskProgress to be filtered.</param>
        /// <param name="values">A set of ObjectType ID values to use to filter records.</param>
        public static void AttachTaskProgressFilter(ref IQueryable<TaskProgress> filteredList, IEnumerable<long> values)
        {
            if (values != null && values.Count() > 0)
            {
                filteredList = filteredList.Where(tp => tp.TrackedItem != null
                    && tp.TrackedItem.ObjectTypeID.HasValue
                    && values.Contains(tp.TrackedItem.ObjectTypeID.Value));
            }
        }

        /// <summary>
        /// Copies values specific to ObjectType class.
        /// </summary>
        /// <param name="from">Object to copy from.</param>
        /// <param name="to">Object to copy to.</param>
        protected override void copyClassValues(ObjectType from, ObjectType to)
        {
            to.JobID = from.JobID;
            to.Name = from.Name;
            to.Description = from.Description;
        }


        /// <summary>
        /// Returns an instance of the target type if there is a valid item with a primary descriptive string matching
        /// the name passed, unless the item is marked deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job the object should belong to.</param>
        /// <param name="name">The full name string to search for.</param>
        /// <returns></returns>
        public override ObjectType FindByName(long jobID, string name)
        {
            return Repository.All.FirstOrDefault(ot => ot.Name == name);
        }


        /// <summary>
        /// Validates deletion of an ObjectType
        /// </summary>
        /// <param name="id">ID of the item to be deleted</param>
        /// <returns></returns>
        public override ValidationResult ValidateDelete(long id)
        {
            var result = base.ValidateDelete(id);
            var matches = Root.GetQueryable<TrackedItem>().Count(x => x.ObjectTypeID.HasValue && x.ObjectTypeID.Value == id);
            if (matches > 0)
                result.AddViolation(string.Format("There are {0} Tracked Items that are using this ObjectType.", matches));
            return result;
        }
    }
}