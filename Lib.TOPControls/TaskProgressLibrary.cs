﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides functions for managing TaskProgress items.
    /// </summary>
    public class TaskProgressLibrary : JobSoftDeleteLibraryBase<TaskProgress>, ITaskProgressLibrary
    {
        #region Constructors

        /// <summary>
        /// Creates an instance of TaskProgressLibrary with default settings.
        /// </summary>
        public TaskProgressLibrary()
            : base()
        {
        }

        /// <summary>
        /// Creates an instance of TaskProgressLibrary based on settings and resources of an ITOPControlLibrary
        /// </summary>
        /// <param name="rootLibrary">Object that implements ITOPControlLibrary</param>
        public TaskProgressLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        {
        }

        /// <summary>
        /// Creates and instance of TaskProgressLibrary with the values passed.
        /// </summary>
        /// <param name="useTaskProgressRepository">The repository to use.</param>
        /// <param name="permissionManager">The IPermissionManager to use.</param>
        public TaskProgressLibrary(IRepository<TaskProgress> useTaskProgressRepository = null,
            IPermissionManager permissionManager = null)
            : base(useTaskProgressRepository, permissionManager)
        {
        }

        #endregion Constructors

        /// <summary>
        /// Returns an instance of the target type. Creates a new record for an item of the specified type, if that item
        /// passes validation and the user indicated by the User ID has the right to create an item of this type.
        /// </summary>
        /// <param name="newItem">An instance of the type to be added.</param>
        /// <param name="userID">The ID of the TOPUser performing this operation, to verify permissions and perform activity
        /// logging.  Must match a valid user record.</param>
        /// <returns></returns>
        public override TaskProgress Add(TaskProgress newItem, long userID)
        {
            TaskProgress result = null;
            if (newItem != null && newItem.IsProgressChanged())
            {
                TaskProgress._ResetInitialPercentComplete(newItem);
            }

            // If there is already an item that matches the same TrackedItem/Task combination,
            // but marked deleted, simply restore that item with the new values.
            var delItem = Repository.All.SingleOrDefault(tp => tp.TaskID == newItem.TaskID
                                                               && tp.TrackedItemID == newItem.TrackedItemID
                                                               && tp.IsDeleted);
            if (delItem != null)
            {
                if (delItem.JobID != newItem.JobID)
                    throw new ArgumentException("Job ID on the new item does not match older records.");
                var tempID = delItem.ID;
                copyValues(newItem, delItem);
                delItem.ID = tempID;
                Repository.Save();
                result = delItem;
            }
            else
            {
                result = base.Add(newItem, userID);
            }
            if (result != null)
            {
                UpdateProgress(result.JobID, result.ID, result.PercentComplete, DateTime.MinValue, 0, userID);
            }
            return result;
        }

        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Items with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public override ValidationResult Validate(TaskProgress item)
        {
            var result = base.Validate(item);

            if (item.TaskID < 1)
            {
                result.AddViolation("The TaskID value is outside of the valid range.", "TaskID");
            }
            else if (Root != null)
            {
                var tsk = Root.GetRepository<Task>().Find(item.TaskID);
                if (tsk == null)
                {
                    result.AddViolation("Cannot find the Task for the TaskID passed.", "TaskID");
                }
                else if (tsk.JobID != item.JobID)
                {
                    result.AddViolation("The Task ID does not belong to the same job.", "TaskID");
                }
            }
            if (item.CommodityID.HasValue)
            {
                var com = Root.GetRepository<Commodity>().Find(item.CommodityID.Value);
                if (com == null)
                {
                    result.AddViolation("Cannot find the Commodity for the Commodity ID passed.", "CommodityID");
                }
                else if (com.JobID != item.JobID)
                {
                    result.AddViolation("The Commodity ID does not belong to the same job.", "CommodityID");
                }
            }
            if (item.TrackedItemID < 1)
            {
                result.AddViolation("The Tracked Item ID value is invalid.", "TrackedItemID");
            }
            else
            {
                if (Root != null)
                {
                    var ti = Root.GetRepository<TrackedItem>().Find(item.TrackedItemID);
                    if (ti == null)
                    {
                        result.AddViolation("Cannot find the Tracked Item for the Tracked Item ID.", "TrackedItemID");
                    }
                    else if (ti.JobID != item.JobID)
                    {
                        result.AddViolation("The Tracked Item ID does not belong to the same job.", "TrackedItemID");
                    }
                }
            }

            if (item.PercentComplete < 0.0m || item.PercentComplete > 1.0m)
            {
                result.AddViolation(
                    string.Format("The percent complete value {0:p} is outside of the valid range from 0 % to 100 %.",
                        "PercentComplete"));
            }

            if (item.ID != 0 && item.IsProgressChanged())
            {
                result.AddViolation(
                    "Changes to the Percent Complete value must be made through the RecordProgressEntry function on the library.",
                    "PercentComplete");
            }

            var dupsTaskTI =
                Repository.All.Count(
                    tp => tp.ID != item.ID && tp.TaskID == item.TaskID && tp.TrackedItemID == item.TrackedItemID);
            if (dupsTaskTI > 0)
            {
                result.AddViolation("There is already another TaskProgress item for the same TrackedItem and Task.",
                    "TrackedItem");
            }

            return result;
        }

        #region ITaskProgressLibrary Members

        /// <summary>
        /// Returns an integer value indicating the number of items related to the Commodity specified
        /// by commodityID.  The count returned does not include TaskProgress items for deleted
        /// TrackedItem records.
        /// </summary>
        /// <param name="commodityID">The ID of the Commodity to count.  If commodity ID is passed as
        /// a null, returns a count of all TaskProgress items without an associated Commodity.</param>
        /// <returns></returns>
        public int CountItemsByCommodity(long? commodityID)
        {
            int result = 0;

            if (commodityID.HasValue)
            {
                var filteredList = Repository.All.Where(tp => tp.CommodityID == commodityID.Value);
                filteredList = TOPControlLibrary.FilterDeletedFromTaskProgress(filteredList);

                result = filteredList.Count();
            }
            else
            {
                result =
                    TOPControlLibrary.FilterDeletedFromTaskProgress(Repository.All.Where(tp => tp.CommodityID == null))
                        .Count();
            }

            return result;
        }

        /// <summary>
        /// Returns a set of TaskProgress objects within the Job indicated by jobID that match the
        /// searchTerm passed.  If the searchTerm is an empty string, returns all TaskProgress items
        /// in the job. Does not return TaskProgress for deleted jobs, or for any deleted TrackedItems.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">A string value to search for.</param>
        /// <returns></returns>
        public IEnumerable<TaskProgress> Search(long jobID, string searchTerm)
        {
            return TOPControlLibrary.FilterDeletedFromTaskProgress(SearchAnyQueryable(jobID, searchTerm)).ToList();
        }

        /// <summary>
        /// Performs that same operation as Search(), but includes TrackedItems that are marked as
        /// deleted, or are members of jobs that are marked as deleted.  If the user referenced by
        /// userID does not have rights to view deleted TrackedItems, the operation will fail.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">A string value to search for.</param>
        /// <param name="userID">The ID of the TOPUser performing the operation.</param>
        /// <returns></returns>
        public IEnumerable<TaskProgress> SearchAny(long jobID, string searchTerm, long userID)
        {
            VerifyUserPermission(UserActionType.ViewDeleted, new TaskProgress(), userID);
            return SearchAnyQueryable(jobID, searchTerm).ToList();
        }

        /// <summary>
        /// Returns an IQueryable of TaskProgress objects within the Job indicated by jobID that match the
        /// searchTerm passed.  If the searchTerm is an empty string, returns all TaskProgress items
        /// in the job. Ignores deleted status.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">A string value to search for.</param>
        /// <returns></returns>
        protected IQueryable<TaskProgress> SearchAnyQueryable(long jobID, string searchTerm)
        {
            var srchList = new List<string>();
            srchList.Add(searchTerm);
            var result = Repository.All.Where(tp => tp.JobID == jobID);
            TOPControlLibrary.AttachTaskProgressSearchFilter(ref result, srchList);
            return result;
        }

        /// <summary>
        /// Returns a set of all TaskProgress items for the job that are related to the TrackedItem with the TrackingID passed.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="trackingID">The exact TrackingID of the TrackedItem to use as a filter.</param>
        /// <returns></returns>
        public IEnumerable<TaskProgress> FindByTrackingID(long jobID, string trackingID)
        {
            var srchList = new List<string>();
            srchList.Add(trackingID);
            var matches = Repository.All.Where(tp => tp.JobID == jobID);
            TrackedItemLibrary.AttachTaskProgressTrackingIDFilter(ref matches, srchList);
            matches = TOPControlLibrary.FilterDeletedFromTaskProgress(matches);
            return matches.ToList();
        }

        /// <summary>
        /// Updates the percent complete of a TaskProgress item, and logs the progress the Progress Log, returning the ProgressLogEntry
        /// instance that is generated.
        /// </summary>
        /// <param name="jobID">The ID of the Job to which this progress log entry is being written.  Must match the job of
        /// the TaskProgress item being updated.</param>
        /// <param name="taskProgressID">A long integer identifying the TaskProgess item that will be updated.</param>
        /// <param name="newPercentComplete">A decimal value representing the PercentComplete value being set.</param>
        /// <param name="workDate">The date that the work causing the progress was actually performed.</param>
        /// <param name="workCrewID">The ID value of the WorkCrew that performed the work.</param>
        /// <param name="userID">The ID of the user performing the logging operation.</param>
        /// <returns></returns>
        public ProgressLogEntry UpdateProgress(long jobID, long taskProgressID, decimal newPercentComplete,
            DateTime workDate, long workCrewID, long userID)
        {
            ProgressLogEntry result = null;
            if (Root != null)
            {
                result = Root.UpdateProgress(jobID, taskProgressID, newPercentComplete, workDate, workCrewID, userID);
            }
            return result;
        }

        #endregion ITaskProgressLibrary Members

        /// <summary>
        /// Copies all TaskProgress specific values from one object to another.
        /// </summary>
        /// <param name="from">Object to copy from</param>
        /// <param name="to">Object to copy to</param>
        protected override void copyClassValues(TaskProgress from, TaskProgress to)
        {
            to.JobID = from.JobID;
            to.CommodityID = from.CommodityID;
            to.TaskID = from.TaskID;
            to.TrackedItemID = from.TrackedItemID;
            to.SetPercentComplete(from.PercentComplete);
        }

        /// <summary>
        /// Adds a condition to the IQueryable passed that filters items down to records that have Completion values
        /// that can be found in the list of values passed.
        /// </summary>
        /// <param name="filteredList">A reference to an IQueryable of TaskProgress to be filtered.</param>
        /// <param name="values">A set of CompletionState values to use to filter records.</param>
        public static void AttachCompletionStateFilter(ref IQueryable<TaskProgress> filteredList,
            IEnumerable<CompletionState> values)
        {
            var cnt = values.Count();
            if (cnt == 1)
            {
                var compstate = values.First();
                switch (compstate)
                {
                    case CompletionState.Complete:
                        filteredList = filteredList.Where(tp => tp.PercentComplete == 1);
                        break;

                    case CompletionState.NotStarted:
                        filteredList = filteredList.Where(tp => tp.PercentComplete == 0);
                        break;

                    case CompletionState.PartiallyComplete:
                        filteredList = filteredList.Where(tp => tp.PercentComplete > 0 && tp.PercentComplete < 1);
                        break;

                    default:
                        break;
                }
            }
            else if (cnt == 2)
            {
                if (!values.Contains(CompletionState.NotStarted))
                {
                    filteredList = filteredList.Where(tp => tp.PercentComplete > 0 && tp.PercentComplete <= 1);
                }
                else if (!values.Contains(CompletionState.PartiallyComplete))
                {
                    filteredList = filteredList.Where(tp => tp.PercentComplete == 0 || tp.PercentComplete == 1);
                }
                else if (!values.Contains(CompletionState.Complete))
                {
                    filteredList = filteredList.Where(tp => tp.PercentComplete >= 0 && tp.PercentComplete < 1);
                }
            }
        }

        /// <summary>
        /// Returns an instance of the target type if there is a valid item with a primary descriptive string matching
        /// the name passed, unless the item is marked deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job the object should belong to.</param>
        /// <param name="name">The full name string to search for.  In the case o</param>
        /// <returns></returns>
        public override TaskProgress FindByName(long jobID, string name)
        {
            if (name.Contains("|"))
            {
                var taskName = name.Split(new string[] {"|"}, StringSplitOptions.None).GetValue(0).ToString();
                var trackingID = name.Split(new string[] {"|"}, StringSplitOptions.None).GetValue(1).ToString();
                var taskItem = Root.TaskActions.FindByName(jobID, taskName);
                if (taskItem != null)
                {
                    return FindByTaskTrackingID(taskItem.ID, trackingID);
                }
            }
            return null;
        }

        /// <summary>
        /// Retreives a TaskProgress item by its Task ID and the Tracking ID of its related tracked item.
        /// </summary>
        /// <param name="taskID"></param>
        /// <param name="trackingID"></param>
        /// <returns></returns>
        public TaskProgress FindByTaskTrackingID(long taskID, string trackingID)
        {
            return Repository.All.FirstOrDefault(tp => !tp.IsDeleted
                                                       && tp.TaskID == taskID
                                                       && !tp.Task.IsDeleted
                                                       && tp.TrackedItem.TrackingID == trackingID
                                                       && !tp.TrackedItem.IsDeleted);
        }

        /// <summary>
        /// Sets the Commodity for all TaskProgress items within the identified tasks, with certain specific tolerances identified by arguments.
        /// </summary>
        /// <param name="taskID">Required, the TaskID to use when selecting items to change.</param>
        /// <param name="commodityID">The ID of the new Commodity to set on these task items. Will clear the commodity if null.</param>
        /// <param name="matchCommodity">If set, the operation will only change the commodity of items that have a commodity ID matching this value.  If null, will always change the current Commodities set on TaskProgress items.</param>
        /// <param name="setNulls">If true, this operation will set values that currently have no commodity set, as long as they are part of the task.</param>
        /// <returns></returns>
        public int PropogateCommodityByTask(long taskID, long? commodityID, long? matchCommodity = null,
            bool setNulls = true)
        {
            return Root.PropogateCommodityByTask(taskID, commodityID, matchCommodity, setNulls);
        }

        /// <summary>
        /// Update the progress of a task based on its ProgressHistory.
        /// </summary>
        /// <param name="taskProgressId">The is of the TaskProgress to update.</param>
        /// <param name="userId">The is of the user performing the action</param>
        public void SetProgressFromHistory(long taskProgressId, long userId)
        {
            var taskProgress = Find(taskProgressId);
            var newProgress = taskProgress.ProgressLogEntries
                .OrderByDescending(p => p.WorkDate)
                .ThenByDescending(p => p.EntryDate).FirstOrDefault();

            if (newProgress != null)
            {
                taskProgress.SetPercentComplete(newProgress.NewPercentComplete ?? 0m);
            }
            
            TaskProgress._ResetInitialPercentComplete(taskProgress);
            Update(taskProgress, userId);
        }
    }
}