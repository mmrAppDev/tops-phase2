﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMRCommon.Data;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Interfaces.DataAccess;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;

namespace Lib.TOPControls
{
    /// <summary>
    /// Implements ProjectArea manangement functions.
    /// </summary>
    public class ProjectAreaLibrary : JobObjectLibraryBase<ProjectArea>, IProjectAreaLibrary
    {

        #region Constructors
        /// <summary>
        /// Creates a ProjectAreaLibrary
        /// </summary>
        public ProjectAreaLibrary()
            : base()
        { }
        /// <summary>
        /// Creates a ProjectAreaLibrary based on the Root Library passed.
        /// </summary>
        /// <param name="rootLibrary">An object that implements ITOPControlLibrary</param>
        public ProjectAreaLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }
        /// <summary>
        /// Creates a ProjectAreaLibrary with the resources passed.
        /// </summary>
        /// <param name="ProjectAreaRepository">Repository to use.</param>
        /// <param name="permissionManager">IPermissionManager to use.</param>
        public ProjectAreaLibrary(IRepository<ProjectArea> ProjectAreaRepository = null, IPermissionManager permissionManager = null)
            : base(ProjectAreaRepository, permissionManager)
        { }

        #endregion

        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public override ValidationResult Validate(ProjectArea item)
        {
            var result = base.Validate(item);

            if (item != null)
            {
                result.ValidateStringLength(item.AreaNumber, 2, 200, "AreaNumber");

                var dups = Repository.All.Count(pa => pa.ID != item.ID && pa.JobID == item.JobID && pa.AreaNumber == item.AreaNumber);
                if (dups > 0)
                {
                    result.AddViolation("There is already a Area with that area number.", "AreaNumber");
                }

                result.ValidateStringLength(item.Description, 0, 200, "Description");
            }
           

            return result;
        }




        #region IProjectAreaLibrary Members

        /// <summary>
        /// Returns a set of ProjectArea objects within the job that match the search term passed.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search. If the matching JobInfo is marked as
        /// deleted, an empty result set will be returned.</param>
        /// <param name="searchTerm">The search term to use.  If an empty string is passed, all 
        /// items of this type within the job will be returned.</param>
        /// <returns></returns>
        public IEnumerable<ProjectArea> Search(long jobID, string searchTerm)
        {
            return Repository.All.Where(pa => pa.JobID == jobID
                            && (searchTerm == "" || pa.AreaNumber == searchTerm)).ToList();
                                
        }

        #endregion


        /// <summary>
        /// Adds a condition to the IQueryable passed that filters items down to records that have ProjectArea ID values
        /// matching the list of values passed.
        /// </summary>
        /// <param name="filteredList">A reference to an IQueryable of TaskProgress to be filtered.</param>
        /// <param name="values">A set of ProjectArea ID values to use to filter records.</param>
        public static void AttachTaskProgressFilter(ref IQueryable<TaskProgress> filteredList, IEnumerable<long> values)
        {
            if (values != null && values.Count() > 0)
            {
                filteredList = filteredList.Where(tp => tp.TrackedItem != null
                    && tp.TrackedItem.AreaID.HasValue
                    && values.Contains(tp.TrackedItem.AreaID.Value));
            }
        }

        /// <summary>
        /// Copies class values
        /// </summary>
        /// <param name="from">From this item</param>
        /// <param name="to">To this item</param>
        protected override void copyClassValues(ProjectArea from, ProjectArea to)
        {
            to.JobID = from.JobID;
            to.AreaNumber = from.AreaNumber;
            to.Description = from.Description;
        }


        /// <summary>
        /// Returns an instance of the target type if there is a valid item with a primary descriptive string matching
        /// the name passed, unless the item is marked deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job the object should belong to.</param>
        /// <param name="name">The full name string to search for.</param>
        /// <returns></returns>
        public override ProjectArea FindByName(long jobID, string name)
        {
            return Repository.All.FirstOrDefault(pa => pa.JobID == jobID && pa.AreaNumber == name);
        }

        /// <summary>
        /// Validates deletion of a ProjectArea
        /// </summary>
        /// <param name="id">ID of the item to be deleted</param>
        /// <returns></returns>
        public override ValidationResult ValidateDelete(long id)
        {
            var result = base.ValidateDelete(id);
            var matches = Root.GetQueryable<TrackedItem>().Count(x => x.AreaID.HasValue && x.AreaID.Value == id);
            if (matches > 0)
                result.AddViolation(string.Format("There are {0} Tracked Items that are using this Area.",matches));
            return result;
        }
    }
}
