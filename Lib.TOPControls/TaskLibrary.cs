﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Interfaces.DataAccess;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides methods to manage Task items.
    /// </summary>
    public class TaskLibrary : JobSoftDeleteLibraryBase<Task>, ITaskLibrary
    {
        #region Constructors

        /// <summary>
        /// Creates a TaskLibrary
        /// </summary>
        public TaskLibrary()
            : base()
        { }

        /// <summary>
        /// Creates a TaskLibrary based on the RootLibrary passed.
        /// </summary>
        /// <param name="rootLibrary">Object implementing ITOPControlLibrary</param>
        public TaskLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }

        /// <summary>
        /// Creates a TaskLibrary with the resources passed.
        /// </summary>
        /// <param name="useRepository">Repository to use.</param>
        /// <param name="permissionManager">IPermissionManager to use.</param>
        public TaskLibrary(IRepository<Task> useRepository = null, IPermissionManager permissionManager = null)
            : base(useRepository, permissionManager)
        { }

        #endregion Constructors


        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public override ValidationResult Validate(Task item)
        {
            var result = base.Validate(item);

            if (item != null)
            {
                if (item.PhaseID < 1)
                {
                    result.AddViolation("The PhaseID outside of the valid range of numbers.", "PhaseID");
                }
                else
                {
                    if (Root != null)
                    {
                        var tempPhase = Root.GetRepository<ProjectPhase>().All.SingleOrDefault(pp => pp.ID == item.PhaseID);
                        if (tempPhase == null)
                        {
                            result.AddViolation("The Phase referenced cannot be found.", "PhaseID");
                        }
                        else if (tempPhase.JobID != item.JobID)
                        {
                            result.AddViolation("The Phase referenced belongs to a different job.", "PhaseID");
                        }
                    }
                }

                if (item.DefaultCommodityID.HasValue)
                {
                    if (Root != null)
                    {
                        if (!Root.GetRepository<Commodity>().All.Any(c => c.ID == item.DefaultCommodityID.Value && c.JobID == item.JobID))
                        {
                            result.AddViolation("The Default Commodity cannot be found.", "DefaultCommodityID");
                        }
                    }
                }

                result.ValidateStringLength(item.Title, 1, 50, "Title");
                var dupTitle = Repository.All.Count(t => t.ID != item.ID && !t.IsDeleted && t.PhaseID == item.PhaseID && t.Title == item.Title);
                if (dupTitle > 0)
                {
                    result.AddViolation(string.Format("There is already a Task named \"{0}\" within the same Phase.", item.Title), "Title");
                }

                if (item.ID != 0 && item.DisplayOrder < 1)
                {
                    result.AddViolation("Cannot assign a display order value that is negative or zero.", "DisplayOrder");
                }
            }

            return result;
        }

        /// <summary>
        /// Copies values specific to Task class.
        /// </summary>
        /// <param name="from">Object to copy from.</param>
        /// <param name="to">Object to copy to.</param>
        protected override void copyClassValues(Task from, Task to)
        {
            to.DisplayOrder = from.DisplayOrder;
            to.JobID = from.JobID;
            to.PhaseID = from.PhaseID;
            to.Title = from.Title;
            to.DefaultCommodityID = from.DefaultCommodityID;
        }

        #region ITaskLibrary Members

        /// <summary>
        /// Returns a set of Task objects within the Job that match the search term passed.  Does not return deleted items or items for a deleted job.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">The search term to use to search.  Passing no search term or a blank one, will
        /// return all items within the job.</param>
        /// <returns></returns>
        public IEnumerable<Task> Search(long jobID, string searchTerm)
        {
            return AllForJob(jobID).Where(t => t.Title.Contains(searchTerm));
        }

        /// <summary>
        /// Returns a set of Task objects within the Job that match the search term passed.  Will return deleted items and items for a deleted job.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">The search term to use to search.  Passing no search term or a blank one, will
        /// return all items within the job.</param>
        /// <param name="userID">The ID of the user performing the action.</param>
        /// <returns></returns>
        public IEnumerable<Task> SearchAny(long jobID, string searchTerm, long userID)
        {
            return AllForJobAny(jobID, userID).Where(t => t.Title.Contains(searchTerm));
        }

        #endregion ITaskLibrary Members

        /// <summary>
        /// Adds a condition to the IQueryable passed that filters items down to records that have Task ID values
        /// that can be found in the list of values passed.
        /// </summary>
        /// <param name="filteredList">A reference to an IQueryable of TaskProgress to be filtered.</param>
        /// <param name="values">A set of Task ID values to use to filter records.</param>
        public static void AttachTaskProgressFilter(ref IQueryable<TaskProgress> filteredList, IEnumerable<long> values)
        {
            if (values != null && values.Count() > 0)
            {
                filteredList = filteredList.Where(tp => values.Contains(tp.TaskID));
            }
        }

        /// <summary>
        /// Sets the values that are determined automatically by the domain, as well as handling the relative values of DisplayOrder values.
        /// </summary>
        /// <param name="item"></param>
        protected override void SetDomainAutomaticValues(Task item)
        {
            base.SetDomainAutomaticValues(item);
            if (item.ID == 0)
            {
                var fullList = RenumberItems(item.PhaseID,true);
                item.DisplayOrder = fullList.Count() + 1;
            }
            else
            {
                var discrim = GetSiblings(item).Count(ob => ob.DisplayOrder == item.DisplayOrder);
                if (discrim > 0)
                {

                    var fullList = GetSiblings(item).Where(ob => ob.DisplayOrder >= item.DisplayOrder).ToList();

                    foreach (var sib in fullList)
                    {
                        sib.DisplayOrder = sib.DisplayOrder + 1;
                        sib.LastChanged = DateTime.Now;
                    }

                }
                Repository.Save();
                RenumberItems(item.PhaseID, true);
            }

        }

        /// <summary>
        /// Returns all items of this type that are in the same list of IDisplayOrdered as the item passed.
        /// </summary>
        /// <param name="item">The source item for which the siblings are being returned.</param>
        /// <param name="excludeOriginal">A boolean that if true causes the items to be returned without the 
        /// instance of the item originally passed.</param>
        /// <returns></returns>
        protected IQueryable<Task> GetSiblings(Task item, bool excludeOriginal = true)
        {
            return Repository.All.Where(t => t.JobID == item.JobID && t.PhaseID == item.PhaseID && (!excludeOriginal || t.ID != item.ID));
        }

        /// <summary>
        /// Modifies the DisplayOrder property of all items of this type within the Job indicated by JobID
        /// </summary>
        /// <param name="phaseID">An ID representing the ProjectPhase to which this item belongs.</param>
        /// <param name="commitValues">A boolean that when passed as true will trigger a Save operation to commit the altered 
        /// DisplayOrder values to the permanent data.  Otherwise, the changes are left to be included in the Unit of Work that 
        /// commits next.</param>
        /// <returns></returns>
        public virtual IEnumerable<Task> RenumberItems(long phaseID, bool commitValues = false)
        {
            var allSibs = Repository.All.Where(t => t.PhaseID == phaseID).ToList().OrderBy(x => x.DisplayOrder).ThenBy(y => y.ID);

            for (int cnt = 0; cnt < allSibs.Count(); cnt++)
            {
                if (allSibs.ElementAt(cnt).DisplayOrder != cnt + 1)
                {
                    var thisitem = allSibs.ElementAt(cnt);
                    thisitem.DisplayOrder = cnt + 1;
                    thisitem.LastChanged = DateTime.Now;
                }
            }

            if (commitValues)
            {
                Repository.Save();
            }

            return allSibs;
        }

        /// <summary>
        /// Returns an instance of the target type if there is a valid item with a primary descriptive string matching
        /// the name passed, unless the item is marked deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job the object should belong to.</param>
        /// <param name="name">The full name string to search for.</param>
        /// <returns></returns>
        public override Task FindByName(long jobID, string name)
        {
            return Repository.All.FirstOrDefault(t => t.JobID == jobID && !t.IsDeleted && t.Title == name);
        }





    }
}