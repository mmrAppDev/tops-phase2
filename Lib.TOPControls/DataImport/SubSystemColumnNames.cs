﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.DataImport
{
    /// <summary>
    /// Provides an as-needed list of column aliases for an n-sized list of subsystems.
    /// </summary>
    public class SubsystemColumnNames:SequenceColumnNames
    {

        /// <summary>
        /// Creates a new instance of the SubsystemColumnNames class, using the set of strings
        /// passed as aliases for the 0 or top-level system.
        /// </summary>
        /// <param name="systemAliases">Strings that can be used for the top-level or Parent System level.</param>
        public SubsystemColumnNames(IEnumerable<string> systemAliases)
            : base("Subsystem")
        {
            columnSet.Add(0, systemAliases);
        }

        /// <summary>
        /// Creates a new instance, using a default alias for the top level system.
        /// </summary>
        public SubsystemColumnNames()
            : this(new string[] {"System"})
        {
            
        }

        
    }
}
