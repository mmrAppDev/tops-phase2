﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.DataImport
{
    /// <summary>
    /// Provides an as-needed list of column aliases related to a n-sized sequence of values.
    /// </summary>
    public class SequenceColumnNames:SchemaColumnNames<int>
    {
        /// <summary>
        /// Gets/Sets the base column name for all depths. Unless pre-assigned, this string
        /// be used to build the expected column name for each specific depth.
        /// </summary>
        public string BaseName { get; set; }


        // Another constructor could be made that allows for a specific sequence of 
        // strings could be passed to represent a finite, definite set of string values
        // to apply to a sequence.

        /// <summary>
        /// Creates a sequence column names instance using the expected column base name.
        /// </summary>
        /// <param name="baseName">A string used to built default expected column names.</param>
        public SequenceColumnNames(string baseName)
        {
            BaseName = baseName;    
        }

        /// <summary>
        /// Generates an array of names that can be checked for the specified sequence depth.
        /// </summary>
        /// <param name="idx">The depth for which to generate this sequence name.</param>
        /// <returns></returns>
        public virtual string[] GetDefaultAliases(int idx)
        {
            string def = GetDefaultName(idx);
            return new string[] { def };
        }

        /// <summary>
        /// Generates the default column name for a specific depth of this sequence.
        /// </summary>
        /// <param name="idx">The depth to use when generating this sequence name.</param>
        /// <returns></returns>
        public virtual string GetDefaultName(int idx)
        {
            if (idx > 0)
            {
                return string.Format("{0} {1}", BaseName, idx);
            }
            return null;
        }

        /// <summary>
        /// Retrieves the list of column names to check for this depth of import.
        /// </summary>
        /// <param name="dataID">Used to identify the depth within the sequence to check.</param>
        /// <returns></returns>
        public override IEnumerable<string> GetAliases(int dataID)
        {
            if (!columnSet.ContainsKey(dataID))
            {
                columnSet.Add(dataID, GetDefaultAliases(dataID));
            }
            return base.GetAliases(dataID);

        }


    }
}
