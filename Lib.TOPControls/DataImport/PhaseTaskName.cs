﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.DataImport
{
    /// <summary>
    /// A class to use when identifying unique Phase and Task name combinations.
    /// </summary>
    public class PhaseTaskName:IComparable
    {

        /// <summary>
        /// The phase name.
        /// </summary>
        public string PhaseName { get; set; }

        /// <summary>
        /// The Task name.
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// Creates an instance with no values.
        /// </summary>
        public PhaseTaskName()
        { }

        /// <summary>
        /// Creates an instance with the values passed.
        /// </summary>
        /// <param name="Phase">The name of the phase</param>
        /// <param name="Task">The name of the task</param>
        public PhaseTaskName(string Phase, string Task)
        {
            PhaseName = Phase;
            TaskName = Task;
        }

        #region IComparable Members
        /// <summary>
        /// Compares this instance to the object passed.
        /// </summary>
        /// <param name="obj">An object that will work best if it is also of the same type.</param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            var ptn = obj as PhaseTaskName;
            if (ptn == null)
            {
                return -1;
            }
            else
            {
                var result = this.PhaseName.CompareTo(ptn.PhaseName);
                if (result == 0)
                    result = this.TaskName.CompareTo(ptn.TaskName);
                return result;
            }
        }

        #endregion
    }
}
