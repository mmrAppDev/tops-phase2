﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Lib.TOPControls.Exceptions;
using Lib.TOPControls.Helpers;

namespace Lib.TOPControls.DataImport
{
    /// <summary>
    /// Processes the TaskImport.
    /// </summary>
    public class TaskImporter
    {

        /// <summary>
        /// The Root TOPControlLibrary.
        /// </summary>
        public ITOPControlLibrary Root { get; set; }

        /// <summary>
        /// Gets/Sets the FileImport this importer is based on.
        /// </summary>
        public FileImport FileImport { get; set; }

        /// <summary>
        /// Gets/Sets the DataImportHelper based on the FileImport.
        /// </summary>
        public DataImportHelper Helper { get; set; }

        /// <summary>
        /// Lists the Craft Code of any CraftInfo items that had to be added to perform the import.
        /// </summary>
        public List<string> CraftsAdded { get; set; }

        /// <summary>
        /// Lists the Commodity Code of any Commodities created during the import.
        /// </summary>
        public List<string> CommoditiesAdded { get; set; }

        /// <summary>
        /// Gets/sets the names of the ObjectType items that were added for this import.
        /// </summary>
        public List<string> ObjectTypesAdded { get; set; }

        /// <summary>
        /// Gets/sets the Area Numbers added to the system that didn't exist before the import.
        /// </summary>
        public List<string> AreasAdded { get; set; }


        /// <summary>
        /// Gets/sets the System Numbers that were added to the application for this import.
        /// </summary>
        public List<string> SystemsAdded { get; set; }

        /// <summary>
        /// Gets/sets the TrackingIDs of tracked items added to the system that didn't exist before this import.
        /// </summary>
        public List<string> TrackedItemsAdded { get; set; }

        /// <summary>
        /// Gets/sets the TrackingIDs of tracked items removed from the Task by this import.
        /// </summary>
        public List<string> TrackedItemsRemoved { get; set; }

        /// <summary>
        /// Gets/sets the Title of the Project Phase imported.  
        /// </summary>
        public string PhaseImported { get; set; }
        /// <summary>
        /// Gets/sets the Title of the Task imported.
        /// </summary>
        public string TaskImported { get; set; }

        private Dictionary<string, long> systemPathLookup = new Dictionary<string, long>();

        /// <summary>
        /// Gets/sets the current line number being imported.
        /// </summary>
        public int LineNum { get; set; }

        /// <summary>
        /// Gets the ID of the Job associated with this FileImport.
        /// </summary>
        public long TargetJobID
        {
            get
            {
                if (FileImport == null)
                {
                    return 0;
                }
                else
                {
                    return FileImport.JobID;
                }
            }
        }

        /// <summary>
        /// An instance of the Job to which information is being imported.
        /// </summary>
        public JobInfo TargetJobInfo
        {
            get
            {
                if (FileImport != null && FileImport.JobInfo != null)
                {
                    return FileImport.JobInfo;
                }
                else if (Root != null && TargetJobID > 0)
                {
                    return Root.JobActions.Find(TargetJobID);
                }
                return null;
            }
        }

        /// <summary>
        /// Creates an instance of the TaskImporter
        /// </summary>
        /// <param name="root">The TOPControlLibrary being used.</param>
        /// <param name="fileImportID">The ID of the FileImport being processed.</param>
        public TaskImporter(ITOPControlLibrary root, long fileImportID)
        {
            ResetItemLists();
            Root = root;
            FileImport = Root.FileImportActions.Find(fileImportID);
            Helper = new DataImportHelper(FileImport);
        }

        /// <summary>
        /// Resets the lists of items that were added and removed during the import.
        /// </summary>
        public void ResetItemLists()
        {
            CraftsAdded = new List<string>();
            CommoditiesAdded = new List<string>();
            AreasAdded = new List<string>();
            ObjectTypesAdded = new List<string>();
            SystemsAdded = new List<string>();
            TrackedItemsAdded = new List<string>();
            TrackedItemsRemoved = new List<string>();
        }

        /// <summary>
        /// Performs the import on the recordset according to the TaskImport functionality.
        /// </summary>
        /// <param name="importParameters"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public IEnumerable<TaskProgress> GetNewItems(Dictionary<string, string> importParameters, long userID)
        {
            var result = new List<TaskProgress>();

            var map = Helper.GetColumnIndices();

            var adminID = Root.UserActions.FindByUsername("sysadmin").ID;
            Task importTask = null;

            ProjectPhase importPhase = null;

            var repTask = Root.GetRepository<Task>();

            if (map.ContainsKey(FilterValueType.TrackingID)
                && map.ContainsKey(FilterValueType.Task)
                && map.ContainsKey(FilterValueType.Phase))
            {
                var taskList = Helper.GetTaskList();
                var phaseList = Helper.GetPhaseList();

                // Determine the Task and Phase
                if (taskList.Count() == 1 && phaseList.Count() == 1)
                {
                    var phaseName = phaseList.First().Trim();
                    var taskName = taskList.First().Trim();

                    importPhase = Root.PhaseActions.FindByName(TargetJobID, phaseName);
                    if (importPhase != null)
                    {
                        importTask = repTask.All.SingleOrDefault(t => !t.IsDeleted && t.JobID == TargetJobID && t.PhaseID == importPhase.ID && t.Title == taskName);
                    }

                    if (importPhase == null || importTask == null)
                    {
                        string errorMessage = string.Format("The task ({0}) or phase ({1}) named in the import file cannot be found in the database.", phaseList.ElementAt(0), taskList.ElementAt(0));
                        throw new FileImportException(errorMessage);
                    }
                }
                else
                {
                    var errMsg = new StringBuilder();
                    if (taskList.Count() > 1)
                    {
                        errMsg.AppendFormat("More than one Task was found. ({0})  ", string.Join(",", taskList));
                    }
                    if (phaseList.Count() > 1)
                    {
                        errMsg.AppendFormat("More than one Phase was found. ({0})  ", string.Join(",", phaseList));
                    }
                    if (errMsg.Length == 0)
                    {
                        errMsg.Append("The import data is missing a value for either the Phase or Task to import.");
                    }
                    throw new FileImportException(errMsg.ToString());
                }

            }
            else
            {
                bool missingValues = false;
                if (!map.ContainsKey(FilterValueType.TrackingID))
                {
                    // TrackingID cannot be supplied through import parameters.
                    missingValues = true;
                }
                if (!map.ContainsKey(FilterValueType.Phase))
                {
                    if (importParameters.ContainsKey("Phase"))
                    {
                        var phaseName = importParameters["Phase"];
                        importPhase = Root.PhaseActions.FindByName(TargetJobID, phaseName);
                    }
                    else
                    {
                        missingValues = true;
                    }
                }
                if (!map.ContainsKey(FilterValueType.Task))
                {
                    if (importParameters.ContainsKey("Task") && importPhase != null)
                    {
                        var taskName = importParameters["Task"];
                        importTask = repTask.All.SingleOrDefault(t => !t.IsDeleted && t.JobID == TargetJobID && t.PhaseID == importPhase.ID && t.Title == taskName);
                    }
                    else
                    {
                        missingValues = true;
                    }
                }
                if (missingValues)
                {
                    throw new FileImportException("Cannot import a sheet that does not have a Phase, Task, and Tracking ID column.");
                }
            }

            if (importPhase == null) throw new FileImportException("Cannot import without a defined phase.");
            if (importTask == null) throw new FileImportException("Cannot import without a defined task.");

            PhaseImported = importPhase.Title;
            TaskImported = importTask.Title;

            // Check for tasks that appear more than once.
            var dupTids = Helper.GetTrackingIDDuplicates();

            if (dupTids.Count() > 0)
            {
                throw new FileImportException("The following Tracking ID values appear more than once in the import: " + string.Join(", ", dupTids.ToArray()));
            }

            var repTP = Root.GetRepository<TaskProgress>();
            var unmentioned = repTP.All.Where(tp => tp.TaskID == importTask.ID).Select(tp1=>tp1.ID).ToList();
            LineNum = 0;
            foreach (DataRow dr in Helper.RawTable.Rows)
            {
                LineNum++;
                var trackingID = Helper.GetImportedRowValue(dr, FilterValueType.TrackingID);
                if (string.IsNullOrWhiteSpace(trackingID))
                {
                    Console.WriteLine("Skipping row {0} due to blank Tracking ID.", LineNum);
                }
                else
                {
                    var trackedItem = Root.TrackedItemActions.FindByName(TargetJobID, trackingID);
                    if (trackedItem == null)
                    {
                        // Determine if the item is in the Deleted State
                        var repTI = Root.GetRepository<TrackedItem>();
                        trackedItem = repTI.All.SingleOrDefault(ti => ti.JobID == TargetJobID
                                                            && ti.TrackingID == trackingID
                                                            && ti.IsDeleted);
                        if (trackedItem != null)
                        {
                            // Restore it.
                            trackedItem = Root.TrackedItemActions.Undelete(trackedItem.ID, adminID);
                            if (trackedItem != null)
                            {
                                TrackedItemsAdded.Add(trackedItem.TrackingID);
                            }
                        }
                        else
                        {

                            // We will need to create a new item.
                            trackedItem = new TrackedItem
                            {
                                JobID = TargetJobID,
                                TrackingID = trackingID,
                                LastChanged = DateTime.Now,

                            };
                            TrackedItemsAdded.Add(trackedItem.TrackingID);
                        }
                    }

                    string craftCode = string.Empty;
                    trackedItem.CraftID = null;
                    if (map.ContainsKey(FilterValueType.CraftInfo))
                    {
                        craftCode = Helper.GetImportedRowValue(dr, FilterValueType.CraftInfo);
                        if (!string.IsNullOrWhiteSpace(craftCode))
                        {
                            var craft = Root.CraftActions.FindByName(TargetJobID, craftCode);
                            if (craft == null)
                            {
                                craft = new CraftInfo
                                {
                                    JobID = TargetJobID,
                                    CraftCode = craftCode,
                                    Description = ""
                                };
                                Root.CraftActions.Add(craft, userID);
                                CraftsAdded.Add(craft.CraftCode);
                            }
                            trackedItem.CraftID = craft.ID;
                        }

                    }

                    string objectTypeName = string.Empty;
                    trackedItem.ObjectTypeID = null;
                    if (map.ContainsKey(FilterValueType.ObjectType))
                    {
                        objectTypeName = Helper.GetImportedRowValue(dr, FilterValueType.ObjectType);
                        if (!string.IsNullOrWhiteSpace(objectTypeName))
                        {
                            var ot = Root.ObjectTypeActions.FindByName(TargetJobID, objectTypeName);
                            if (ot == null)
                            {
                                ot = new ObjectType()
                                {
                                    JobID = TargetJobID,
                                    Name = objectTypeName,
                                    Description = "" 
                                };
                                Root.ObjectTypeActions.Add(ot, userID);
                                ObjectTypesAdded.Add(ot.Name);
                            }

                            trackedItem.ObjectTypeID = ot.ID;
                        }
                    }

                    var areaNumber = string.Empty;
                    trackedItem.AreaID = null;
                    if (map.ContainsKey(FilterValueType.ProjectArea))
                    {
                        areaNumber = Helper.GetImportedRowValue(dr, FilterValueType.ProjectArea);
                        if (!string.IsNullOrWhiteSpace(areaNumber))
                        {
                            var area = Root.AreaActions.FindByName(TargetJobID, areaNumber);
                            if (area == null)
                            {
                                area = new ProjectArea
                                {
                                    JobID = TargetJobID,
                                    AreaNumber = areaNumber,
                                    Description = ""
                                };
                                Root.AreaActions.Add(area, userID);
                                AreasAdded.Add(area.AreaNumber);
                            }
                            trackedItem.AreaID = area.ID;
                        }

                    }

                    List<string> systemPath = new List<string>();
                    string sysKey = string.Empty;
                    string systemNumber = string.Empty;
                    long? lastID = null;
                    trackedItem.SystemID = null;        // set the SystemID to null as default.

                    ProjectSystem ps = null;
                    // create a system path for both lookup and modification 
                    foreach (int idx in Helper.SubsystemMap)
                    {
                        // find the current column value.
                        systemNumber = Helper.GetImportedRowValueByIdx(dr, idx);
                        // if it is not empty, add it to the system path
                        if (!string.IsNullOrWhiteSpace(systemNumber))
                        {
                            systemPath.Add(systemNumber);
                            sysKey = "[" + string.Join("][", systemPath.ToArray()) + "]";
                            if (systemPathLookup.ContainsKey(sysKey))
                            {
                                lastID = systemPathLookup[sysKey];
                            }
                            else
                            {
                                // Try to find the matching system, if we can.
                                ps = Root.SystemActions.FindByParentAndName(TargetJobID, lastID, systemNumber);
                                if (ps == null)
                                {
                                    // the path doesn't exist.  We need to add the system.
                                    ps = new ProjectSystem
                                    {
                                        JobID = TargetJobID,
                                        SystemNumber = systemNumber,
                                        ParentId = lastID,
                                        Description = ""
                                    };
                                    Root.SystemActions.Add(ps, userID);
                                    SystemsAdded.Add(sysKey);
                                }
                                systemPathLookup.Add(sysKey, ps.ID);
                                lastID = ps.ID;
                            } // end else (systemPath not yet present)
                        }
                        else    // if it is empty, exit the loop
                        {
                            break;
                        }
                    }
                    
                    trackedItem.SystemID = lastID;
                    
                    trackedItem.UDPValue1 = string.Empty;
                    if (map.ContainsKey(FilterValueType.UserDefinedProperty1))
                    {
                        trackedItem.UDPValue1 = Helper.GetImportedRowValue(dr, FilterValueType.UserDefinedProperty1);
                    }

                    trackedItem.UDPValue2 = string.Empty;
                    if (map.ContainsKey(FilterValueType.UserDefinedProperty2))
                    {
                        trackedItem.UDPValue2 = Helper.GetImportedRowValue(dr, FilterValueType.UserDefinedProperty2);
                    }

                    trackedItem.UDPValue3 = string.Empty;
                    if (map.ContainsKey(FilterValueType.UserDefinedProperty3))
                    {
                        trackedItem.UDPValue3 = Helper.GetImportedRowValue(dr, FilterValueType.UserDefinedProperty3);
                    }

                    // Commit this item to the database.
                    if (trackedItem.ID == default(long))
                    {
                        Root.TrackedItemActions.Add(trackedItem, userID);
                    }
                    else
                    {
                        Root.TrackedItemActions.Update(trackedItem, userID);
                    }

                    // Since its presence in this import indicates that there is a matching TaskProgress,
                    // find or create it.
                    var taskProgress = Root.TaskProgressActions.FindByTaskTrackingID(importTask.ID, trackingID);
                    if (taskProgress == null)
                    {
                        taskProgress = Root.GetQueryable<TaskProgress>().FirstOrDefault(x => x.TaskID == importTask.ID
                            && x.TrackedItem.TrackingID == trackingID
                            && x.IsDeleted);
                        if (taskProgress != null)
                        {
                            Root.TaskProgressActions.Undelete(taskProgress.ID, adminID);
                        }
                        else
                        {
                            taskProgress = new TaskProgress
                            {
                                JobID = TargetJobID,
                                TaskID = importTask.ID,
                                TrackedItemID = trackedItem.ID
                            };
                        }
                    }
                    

                    string commodityCode = string.Empty;
                    taskProgress.CommodityID = null;
                    if (map.ContainsKey(FilterValueType.Commodity))
                    {
                        commodityCode = Helper.GetImportedRowValue(dr, FilterValueType.Commodity);
                        if (!string.IsNullOrWhiteSpace(commodityCode))
                        {
                            var commodity = Root.CommodityActions.FindByName(TargetJobID, commodityCode);
                            if (commodity == null)
                            {
                                commodity = new Commodity
                                {
                                    JobID = TargetJobID,
                                    CommodityCode = commodityCode,
                                    Description = ""
                                };
                                commodity = Root.CommodityActions.Add(commodity, userID);
                            }
                            taskProgress.CommodityID = commodity.ID;
                        }
                    }

                    if (taskProgress.ID == default(long))
                    {
                        Root.TaskProgressActions.Add(taskProgress, userID);
                    }
                    else
	                {
                        Root.TaskProgressActions.Update(taskProgress, userID);
	                }

                    if (unmentioned.Contains(taskProgress.ID))
                    {
                        unmentioned.Remove(taskProgress.ID);
                    }

                    result.Add(taskProgress);
                    
                }

            }
            
            // Remove all unmentioned TaskProgress items from the active list.
            foreach (var tpID in unmentioned)
            {
                var item = Root.TaskProgressActions.Find(tpID);
                if (item != null) TrackedItemsRemoved.Add(item.TrackedItem.TrackingID);
                Root.TaskProgressActions.Delete(tpID, userID);

            }

            return result;

        }


    }
}
