﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.DataImport
{
    /// <summary>
    /// Returns a list of column names for each value in a Task import.
    /// </summary>
    public class TaskImportColumnNames:SchemaColumnNames<FilterValueType>
    {
        /// <summary>
        /// Creates a new instance using the default values.
        /// </summary>
        public TaskImportColumnNames()
            : base()
        {
            columnSet.Add(FilterValueType.Commodity, new string[] {"Commodity"});
            columnSet.Add(FilterValueType.CraftInfo, new string[] {"Craft"});
            columnSet.Add(FilterValueType.ObjectType, new string[] {"ObjectType", "Object Type"});
            columnSet.Add(FilterValueType.Phase, new string[] {"Phase"});
            columnSet.Add(FilterValueType.ProjectArea, new string[] {"Area"});
            columnSet.Add(FilterValueType.ProjectSystem, new string[] { "System" });
            columnSet.Add(FilterValueType.Task, new string[] { "Task" });
            columnSet.Add(FilterValueType.TrackingID, new string[] { "TrackingID", "Tracking ID", "Tag" });
        }

        /// <summary>
        /// Modifies this instance of column names by adding aliases for the User Defined Column names.
        /// </summary>
        /// <param name="job">An instance of a JobInfo object tha tis connected to this TaskImportColumnNames instance.</param>
        /// <returns></returns>
        public TaskImportColumnNames TransformForJob(JobInfo job)
        {
            if (!string.IsNullOrWhiteSpace(job.UDPName1))
                columnSet.Add(FilterValueType.UserDefinedProperty1, new string[] { job.UDPName1, "UDPValue1" });
            if (!string.IsNullOrWhiteSpace(job.UDPName2))
                columnSet.Add(FilterValueType.UserDefinedProperty2, new string[] { job.UDPName2, "UDPValue2" });
            if (!string.IsNullOrWhiteSpace(job.UDPName3))
                columnSet.Add(FilterValueType.UserDefinedProperty3, new string[] { job.UDPName3, "UDPValue3" });
            return this;
        }

        
    }
}
