﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.DataImport
{
    /// <summary>
    /// Holds a list of column names for a schema.
    /// </summary>
    /// <typeparam name="IDType">Determines the datatype used to identify each set of aliases.</typeparam>
    public class SchemaColumnNames<IDType>
    {
        /// <summary>
        /// Creates a new instance.
        /// </summary>
        public SchemaColumnNames()
        {
            columnSet = new Dictionary<IDType, IEnumerable<string>>();
        }

        /// <summary>
        /// Gets/Sets the internal dictionary of names.
        /// </summary>
        public Dictionary<IDType, IEnumerable<string>> columnSet { get; set; }

        /// <summary>
        /// Retrieves a list of aliases matching the key value passed.
        /// </summary>
        /// <param name="dataID">The ID or Key of the list desired.</param>
        /// <returns></returns>
        public virtual IEnumerable<string> GetAliases(IDType dataID)
        {
            if (columnSet.ContainsKey(dataID))
            {
                return columnSet[dataID];
            }
            else
            {
                return new List<string>();
            }
        }

    }
}
