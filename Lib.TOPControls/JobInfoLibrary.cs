﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Interfaces.DataAccess;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls
{
    /// <summary>
    /// A library class that performs operations specific to the JobInfo class and its instances.
    /// </summary>
    public class JobInfoLibrary : SoftDeleteLibraryBase<JobInfo>, IJobInfoLibrary
    {
        #region Constructors

        /// <summary>
        /// Creates an instance of JobInfoLibrary with default values.
        /// </summary>
        public JobInfoLibrary()
            : base()
        { }

        /// <summary>
        /// Creates an instance of JobInfoLibrary, setting the Root library and pulling all other resources from it.
        /// </summary>
        /// <param name="rootLibrary">An object that implements ITOPControlLibrary.</param>
        public JobInfoLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }

        /// <summary>
        /// Creates an instance of JobInfoLibrary with the resource values passed.
        /// </summary>
        /// <param name="jobInfoRepository">The IRepository to use.</param>
        /// <param name="permissionManager">The IPermissionManager to use.</param>
        public JobInfoLibrary(IRepository<JobInfo> jobInfoRepository = null, IPermissionManager permissionManager = null)
            : base(jobInfoRepository, permissionManager)
        { }

        #endregion Constructors

        #region IObjectLibrary<JobInfo> Members

        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public override ValidationResult Validate(JobInfo item)
        {
            var result = base.Validate(item);

            if (string.IsNullOrWhiteSpace(item.Title)
                || item.Title.TrimEnd().Length <= 3
                || item.Title.Trim().Length > 100)
            {
                result.AddViolation("Title must be between 4 and 100 characters long.", "Title");
            }

            if (item.UDPName1 != null && item.UDPName1.Length > 50)
            {
                result.AddViolation("The User Defined Property names cannot be longer than fifty characters.", "UDPName1");
            }
            if (item.UDPName2 != null && item.UDPName2.Length > 50)
            {
                result.AddViolation("The User Defined Property names cannot be longer than fifty characters.", "UDPName2");
            }
            if (item.UDPName3 != null && item.UDPName3.Length > 50)
            {
                result.AddViolation("The User Defined Property names cannot be longer than fifty characters.", "UDPName3");
            }

            if (!string.IsNullOrWhiteSpace(item.UDPName1) && string.Compare(item.UDPName1, item.UDPName2) == 0)
            {
                if (string.Compare(item.UDPName1, item.UDPName3) == 0)
                {
                    result.AddViolation("The user define properties all have the same non-blank name.", "UDPName2");
                }
                else
                {
                    result.AddViolation("The first and second user define properties both have the same name.", "UDPName2");
                }
            }
            else if (!string.IsNullOrWhiteSpace(item.UDPName2) && string.Compare(item.UDPName2, item.UDPName3) == 0)
            {
                result.AddViolation("The second and third user defined properties both have the same name.", "UDPName3");
            }
            else if (!string.IsNullOrWhiteSpace(item.UDPName1) && string.Compare(item.UDPName1, item.UDPName3) == 0)
            {
                result.AddViolation("The first and third user defined properties both have the same name.", "UDPName3");
            }




            if (result.IsValid && Repository != null)
            {
                // Test for duplicate values in the database.
                if (item.ID == default(long))
                {
                    var newDupList = Repository.All.Where(job => job.Title == item.Title);
                    if (newDupList.Count() > 0)
                    {
                        result.AddViolation(string.Format("A job with the Title \"{0}\" already exists. The Title value must be unqiue.", item.Title), "Title");
                    }
                }
                else
                {
                    var existingDupList = Repository.All.Where(job => job.ID != item.ID && job.Title == item.Title);
                    if (existingDupList.Count() > 0)
                    {
                        result.AddViolation(string.Format("The Title being saved for this job, \"{0}\" already belongs to another job.  The Title value must be unique.", item.Title), "Title");
                    }
                }

                // For Weighted Calculations, Verify that the job has Commodities in All TaskProgress items, and that all of those Commodities have hours associated.
                // Now go.
                if (result.IsValid && Root != null && item.ID != default(long))
                {
                    // Check All Task Progress Items in a valid scope.
                    if (item.WeightedCalculations && Root.GetQueryable<TaskProgress>().Any(tp => !tp.IsInDeletedScope && tp.CommodityID == null))
                    {
                        result.AddViolation("Cannot set the Calculation method to Weighted when some of the Task-Item combinations do not have an associated commodity.", "WeightedCalculations");
                    }

                    // Check that all items have hours associate with them.  *NOT SURE WE WILL BE DOING THIS*
                }
            }

            return result;
        }

        /// <summary>
        /// Adds a job to the Application data
        /// </summary>
        /// <param name="newItem">The JobInfo instance to add.</param>
        /// <param name="userID">The ID of the user performing the add.</param>
        /// <returns></returns>
        public override JobInfo Add(JobInfo newItem, long userID)
        {
            var result = base.Add(newItem, userID);

            if (result.ID > 0)
            {
                if (Root != null && Root.UserJobActions != null)
                {
                    var uj = new UserJob { JobID = newItem.ID, UserID = userID };
                    Root.UserJobActions.Add(uj, userID);
                }
                // Add default values to Job.
                AddDefaultProjectPhasesToJob(result, userID);
                AddDefaultCraftsToJob(result, userID);
                AddDefaultWorkCrewToJob(result, userID);
                AddDefaultObjectTypesToJob(result, userID);
                AddDefaultFilterConfigurationsToJOb(result, userID);

            }

            return result;
        }

        public override JobInfo Update(JobInfo item, long userID)
        {
            JobInfo result = null;
            bool clearHistory = false;

            if (Root != null)
            {
                var oldHist = Root.GetCommittedPropertyValue(item, x => x.HistoricalScopeCurve);
                var oldCalc = Root.GetCommittedPropertyValue(item, x => x.WeightedCalculations);

                clearHistory = (item.HistoricalScopeCurve != oldHist || item.WeightedCalculations != oldCalc);
            }

            result = base.Update(item, userID);
            if (clearHistory) 
            {
                Root.PurgeReportingData(item.ID);
                var rhlpr = new Helpers.ChartingHelper(Root);
            }

            return result;
        }
       

        /// <summary>
        /// Adds the default ObjectTypes needed by the new job.
        /// </summary>
        /// <param name="newJob">The JobInfo being populated.</param>
        /// <param name="userID">The ID of the user performing this operation.</param>
        private void AddDefaultObjectTypesToJob(JobInfo newJob, long userID)
        {
            if (Root != null)
            {
                var instElec = new ObjectType
                {
                    JobID = newJob.ID,
                    Name = "Instrument/Electrical"
                };
                var loopCheck = new ObjectType
                {
                    JobID = newJob.ID,
                    Name = "Loop Check"
                };
                var causeEffect = new ObjectType
                {
                    JobID = newJob.ID,
                    Name = "Cause and Effect"
                };

                Root.ObjectTypeActions.Add(instElec, userID);
                Root.ObjectTypeActions.Add(loopCheck, userID);
                Root.ObjectTypeActions.Add(causeEffect, userID);
            }
        }
        /// <summary>
        /// Adds the default WorkCrew items for a new job.
        /// </summary>
        /// <param name="newJob">The JobInfo being populated.</param>
        /// <param name="userID">The ID of the user performing this operation.</param>
        private void AddDefaultWorkCrewToJob(JobInfo newJob, long userID)
        {
            if (Root != null)
            {
                var crew1 = new WorkCrew
                {
                    JobID = newJob.ID,
                    CrewNumber = 1,
                    Description = "Default Crew"
                };

                Root.WorkCrewActions.Add(crew1, userID);
            }
        }
        /// <summary>
        /// Adds the default Craft items for a new job.
        /// </summary>
        /// <param name="newJob">The JobInfo being populated.</param>
        /// <param name="userID">The ID of the user performing this operation.</param>
        private void AddDefaultCraftsToJob(JobInfo newJob, long userID)
        {
            if (Root != null)
            {
                var elec = new CraftInfo { JobID =newJob.ID, CraftCode = "Electrical", Description = ""};
                var inst = new CraftInfo { JobID= newJob.ID, CraftCode = "Instrumentation", Description = "" };
                var mech = new CraftInfo { JobID = newJob.ID, CraftCode = "Mechanical", Description = "" };
                var pipe = new CraftInfo { JobID = newJob.ID, CraftCode = "Piping", Description = "" };

                Root.CraftActions.Add(elec, userID);
                Root.CraftActions.Add(inst, userID);
                Root.CraftActions.Add(mech, userID);
                Root.CraftActions.Add(pipe, userID);
            }
        }
        /// <summary>
        /// Adds the default ProjectPhase items for a new job.
        /// </summary>
        /// <param name="newJob">The JobInfo being populated.</param>
        /// <param name="userID">The ID of the user performing this operation.</param>
        private void AddDefaultProjectPhasesToJob(JobInfo newJob, long userID)
        {
            if (Root != null)
            {
                var preCom = new ProjectPhase { JobID = newJob.ID, Title = "Pre-Commissioning", DisplayOrder = 1 };
                Root.PhaseActions.Add(preCom, userID);
                var commish = new ProjectPhase { JobID = newJob.ID, Title = "Commissioning", DisplayOrder = 2 };
                Root.PhaseActions.Add(commish, userID);

            }
        }


        /// <summary>
        /// Adds the default ProjectPhase items for a new job.
        /// </summary>
        /// <param name="newJob">The JobInfo being populated.</param>
        /// <param name="userID">The ID of the user performing this operation.</param>
        private void AddDefaultFilterConfigurationsToJOb(JobInfo newJob, long userID)
        {
            if (Root != null && Root.FilterConfigActions != null)
            {
                Root.FilterConfigActions.Add(buildDefaultFilterConfiguration(newJob.ID, FilterValueType.Search), userID);
                Root.FilterConfigActions.Add(buildDefaultFilterConfiguration(newJob.ID, FilterValueType.CompletionState), userID);
                Root.FilterConfigActions.Add(buildDefaultFilterConfiguration(newJob.ID, FilterValueType.ProjectSystem), userID);
                Root.FilterConfigActions.Add(buildDefaultFilterConfiguration(newJob.ID, FilterValueType.CraftInfo), userID);
                Root.FilterConfigActions.Add(buildDefaultFilterConfiguration(newJob.ID, FilterValueType.Commodity), userID);
                Root.FilterConfigActions.Add(buildDefaultFilterConfiguration(newJob.ID, FilterValueType.ProjectArea), userID);
                Root.FilterConfigActions.Add(buildDefaultFilterConfiguration(newJob.ID, FilterValueType.UserDefinedProperty1), userID);
                Root.FilterConfigActions.Add(buildDefaultFilterConfiguration(newJob.ID, FilterValueType.UserDefinedProperty2), userID);
                Root.FilterConfigActions.Add(buildDefaultFilterConfiguration(newJob.ID, FilterValueType.UserDefinedProperty3), userID);
                Root.FilterConfigActions.Add(buildDefaultFilterConfiguration(newJob.ID, FilterValueType.Task), userID);
                Root.FilterConfigActions.Add(buildDefaultFilterConfiguration(newJob.ID, FilterValueType.Phase), userID);
            }
        }

        private static FilterConfiguration buildDefaultFilterConfiguration(long jobID, FilterValueType fvt)
        {
            return new FilterConfiguration
            {
                JobID = jobID,
                FilterValueType = fvt,
                DisplayOrder = 50,
                Active = (fvt != FilterValueType.TrackingID 
                            && fvt != FilterValueType.Phase 
                            && fvt != FilterValueType.Task 
                            && fvt != FilterValueType.None),
                IsHighVolume = (fvt == FilterValueType.TrackingID || fvt == FilterValueType.ProjectSystem)
            };
        }

        #endregion IObjectLibrary<JobInfo> Members

        #region IJobInfoLibrary Members

        /// <summary>
        /// Returns a set of JobInfo objects that match the searchTerm passed.  If the term is empty, retuns all JobInfo
        /// records.  Will always exclude JobInfo items marked as deleted.
        /// </summary>
        /// <param name="searchTerm">A string representing the text to search for.</param>
        /// <returns></returns>
        public IEnumerable<JobInfo> Search(string searchTerm)
        {
            return SearchAnyQueryable(searchTerm).Where(j => j.IsDeleted == false).ToList();
        }

        /// <summary>
        /// Returns a set of JobInfo objects that match the searchTerm passed, including items marked as deleted.
        /// </summary>
        /// <param name="searchTerm">A string representing the text to search for.  Passing an empty string will
        /// cause all JobInfo items to be returned.</param>
        /// <param name="userID">The ID of a valid TOPUser.  If the indicated user does not have the rights to
        /// view deleted jobs, the action will fail.</param>
        /// <returns></returns>
        public IEnumerable<JobInfo> SearchAny(string searchTerm, long userID)
        {
            VerifyUserPermission(UserActionType.ViewDeleted, new JobInfo(), userID);
            return SearchAnyQueryable(searchTerm).ToList();
        }

        /// <summary>
        /// Returns an IQueryable with results of the search operation.  Does not filter out items marked as deleted.
        /// </summary>
        /// <param name="searchTerm">A string representing the text to search for.</param>
        /// <returns></returns>
        protected IQueryable<JobInfo> SearchAnyQueryable(string searchTerm)
        {
            if (string.IsNullOrWhiteSpace(searchTerm))
            {
                return Repository.All;
            }
            else
            {
                var result = Repository.All.Where(job => job.Title.Contains(searchTerm));
                return result;
            }
        }

        /// <summary>
        /// Returns a dictionary of the User-Defined Properties for the Job.  This dictionary key is the numeral
        /// index of the property (from 1 to 3) and the value is a string representing the name of the User Defined Property.
        /// </summary>
        /// <param name="jobID">The ID of the JobInfo item.</param>
        /// <returns></returns>
        public Dictionary<int, string> GetUDPList(long jobID)
        {
            var result = new Dictionary<int, string>();

            var item = Find(jobID);
            if (item != null)
            {
                if (!string.IsNullOrWhiteSpace(item.UDPName1))
                {
                    result.Add(1, item.UDPName1);
                }
                if (!string.IsNullOrWhiteSpace(item.UDPName2))
                {
                    result.Add(2, item.UDPName2);
                }
                if (!string.IsNullOrWhiteSpace(item.UDPName3))
                {
                    result.Add(3, item.UDPName3);
                }
            }
            return result;
        }

        #endregion IJobInfoLibrary Members

        /// <summary>
        /// Copies values specific to the JobInfo class from one object to another.
        /// </summary>
        /// <param name="from">The instance to copy from.</param>
        /// <param name="to">The instance to copy to.</param>
        protected override void copyClassValues(JobInfo from, JobInfo to)
        {
            to.Title = from.Title;
            to.UDPName1 = from.UDPName1;
            to.UDPName2 = from.UDPName2;
            to.UDPName3 = from.UDPName3;
            to.ShowEarnedToClient = from.ShowEarnedToClient;
            to.WeightedCalculations = from.WeightedCalculations;
            to.HistoricalScopeCurve = from.HistoricalScopeCurve;
        }
    }
}