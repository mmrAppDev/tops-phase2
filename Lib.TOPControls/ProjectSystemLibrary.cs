﻿using System.Collections.Generic;
using System.Linq;
using Envoc.Core.Shared.Extensions;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides managmenet functions for ProjectSystem items.
    /// </summary>
    public class ProjectSystemLibrary : JobObjectLibraryBase<ProjectSystem>, IProjectSystemLibrary
    {
        #region Constructors

        /// <summary>
        /// Creates a ProjectSystemLibrary
        /// </summary>
        public ProjectSystemLibrary()
            : base()
        {
        }

        /// <summary>
        /// Creates a ProjectSystemLibrary based on the Root Library passed
        /// </summary>
        /// <param name="rootLibrary">Implements ITOPControlLibrary</param>
        public ProjectSystemLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        {
        }

        /// <summary>
        /// Creates a ProjectSystemLibrary based on the resources passed.
        /// </summary>
        /// <param name="useProjectSystemRepository">Repository to use.</param>
        /// <param name="permissionManager">IPermissionManager to use.</param>
        public ProjectSystemLibrary(IRepository<ProjectSystem> useProjectSystemRepository = null,
            IPermissionManager permissionManager = null)
            : base(useProjectSystemRepository, permissionManager)
        {
        }

        #endregion Constructors

        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public override ValidationResult Validate(ProjectSystem item)
        {
            var result = base.Validate(item);

            if (item != null)
            {
                result.ValidateStringLength(item.SystemNumber, 2, 200, "SystemNumber");

                var dups = Repository.All;

                if (item.ParentId.HasValue)
                {
                    dups = dups.Where(
                        ps =>
                            ps.ID != item.ID && ps.JobID == item.JobID && ps.ParentId == item.ParentId &&
                            ps.SystemNumber == item.SystemNumber);
                }
                else
                {
                    dups = dups.Where(
                        ps =>
                            ps.ID != item.ID && ps.JobID == item.JobID && ps.ParentId == null &&
                            ps.SystemNumber == item.SystemNumber);
                }

                if (dups.Any())
                {
                    result.AddViolation("There is already a System with that system number.", "SystemNumber");
                }

                result.ValidateStringLength(item.Description, 0, 200, "Description");
            }
            return result;
        }

        #region IProjectSystemLibrary Members

        /// <summary>
        /// Returns a set of ProjectSystem items within the Job indicated that match the search term.
        /// </summary>
        /// <param name="jobID">The ID of the job by which to filter the results.</param>
        /// <param name="searchTerm">The search term to use for filtering results.  If an empty string
        /// is passed for this argument, returns all items within the job.</param>
        /// <returns></returns>
        public IEnumerable<ProjectSystem> Search(long jobID, string searchTerm)
        {
            var job = Root.JobActions.Find(jobID);
            if (job == null) return new List<ProjectSystem>();

            return Repository.All.Where(ps => ps.JobID == jobID
                                              && (
                                                  searchTerm == ""
                                                  || ps.SystemNumber.Contains(searchTerm))).ToList();
        }

        /// <summary>
        /// Returns a set of ProjectSystem items withing the Job indicated that have the matching parent ID
        /// </summary>
        /// <param name="jobID">The ID of the job by which to filter the results.</param>
        /// <param name="parentId">The ID of the parent to user for filtering results. If the value is null, return all root level systems</param>
        /// <returns></returns>
        public IEnumerable<ProjectSystem> FindByParentId(long jobID, long? parentId)
        {
            var job = Root.JobActions.Find(jobID);
            if (job == null) return new List<ProjectSystem>();

            if (parentId.HasValue)
            {
                return
                    Repository.All.Where(
                        ps => ps.JobID == jobID && ps.ParentId == parentId.Value);
            }

            return
                Repository.All.Where(
                    ps => ps.JobID == jobID && ps.ParentId == (long?) null);
        }

        /// <summary>
        /// Returns a ProjectSystem, if one exists, belonging to the Parent passed (if passed) and having a the name value passed.
        /// </summary>
        /// <param name="jobID">The ID of the job by which to filter the results.</param>
        /// <param name="parentId">The ID of the expected parent item. If this is a top-level item, pass a null value.</param>
        /// <param name="name">The name (or System Number) of the System to find.</param>
        /// <returns></returns>
        public ProjectSystem FindByParentAndName(long jobID, long? parentId, string name)
        {
            return Repository.All.FirstOrDefault(ps =>
                ps.JobID == jobID
                && ((parentId.HasValue && ps.ParentId.HasValue && ps.ParentId.Value == parentId.Value)
                    || (!parentId.HasValue && !ps.ParentId.HasValue))
                && ps.SystemNumber == name);
        }

        /// <summary>
        /// Returns a set of ProjectSystem items withing the Job indicated that have the matching parent ID
        /// </summary>
        /// <param name="jobID">The ID of the job by which to filter the results.</param>
        /// <returns></returns>
        public IEnumerable<ProjectSystem> GetTreeByJobId(long jobID)
        {
            var job = Root.JobActions.Find(jobID);
            if (job == null) return new List<ProjectSystem>();

            var result = this.Root.GetProjectSystemTree(jobID);

            return result;
        }

        /// <summary>
        /// Returns a branch of ProjectSystem items within the indicated System
        /// </summary>
        /// <param name="jobID">The ID of the job by which to filter the results.</param>
        /// <param name="systemID">The ID of the system by which to filter the results.</param>
        /// <returns></returns>
        public ProjectSystem GetSystemBranch(long jobId, long systemID)
        {
            var tree = GetTreeByJobId(jobId);

            foreach (var root in tree)
            {
                if (root.ID == systemID)
                {
                    return root;
                }

                var foundNode = root.GetFlattenedBranch()
                    .FirstOrDefault(node => node.ID == systemID);

                if (foundNode.IsNotNull())
                {
                    return foundNode;
                }
            }

            return null;
        }

        #endregion IProjectSystemLibrary Members

        /// <summary>
        /// Adds a condition to the IQueryable passed that filters items down to records that have System ID values
        /// matching the list of values passed.
        /// </summary>
        /// <param name="filteredList">A reference to an IQueryable of TaskProgress to be filtered.</param>
        /// <param name="values">A set of System ID values to use to filter records.</param>
        public static void AttachTaskProgressFilter(ref IQueryable<TaskProgress> filteredList, IEnumerable<long> values)
        {
            if (values != null && values.Count() > 0)
            {
                filteredList = filteredList.Where(tp => tp.TrackedItem != null
                                                        && tp.TrackedItem.SystemID.HasValue
                                                        && values.Contains(tp.TrackedItem.SystemID.Value));
            }
        }

        /// <summary>
        /// Copies ProjectSystem values between objects.
        /// </summary>
        /// <param name="from">Object to copy from.</param>
        /// <param name="to">Copy to.</param>
        protected override void copyClassValues(ProjectSystem from, ProjectSystem to)
        {
            to.JobID = from.JobID;
            to.SystemNumber = from.SystemNumber;
            to.Description = from.Description;
            to.SortOrder = from.SortOrder;
            to.ParentId = from.ParentId;
        }

        /// <summary>
        /// Returns an instance of the target type if there is a valid item with a primary descriptive string matching
        /// the name passed, unless the item is marked deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job the object should belong to.</param>
        /// <param name="name">The full name string to search for.</param>
        /// <returns></returns>
        public override ProjectSystem FindByName(long jobID, string name)
        {
            return Repository.All.FirstOrDefault(ps => ps.JobID == jobID && ps.SystemNumber == name);
        }

        /// <summary>
        /// Validates deletion of a ProjectSystem
        /// </summary>
        /// <param name="id">ID of the item to be deleted</param>
        /// <returns></returns>
        public override ValidationResult ValidateDelete(long id)
        {
            var result = base.ValidateDelete(id);
            var item = Repository.All.FirstOrDefault(ps => ps.ID == id);
            var matches = Root.GetQueryable<TrackedItem>().Count(x => x.SystemID.HasValue && x.SystemID.Value == id);

            if (item.HasChildren())
            {
                result.AddViolation(string.Format("This System cannot be deleted because it has child Systems.", item));
            }

            if (matches > 0)
                result.AddViolation(string.Format("There are {0} Tracked Items that are using this System.", matches));
            return result;
        }
    }
}