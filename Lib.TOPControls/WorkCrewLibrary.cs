﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMRCommon.Data;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Interfaces.DataAccess;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Permissions;

namespace Lib.TOPControls
{
    /// <summary>
    /// Implements operations for managing WorkCrew objects.
    /// </summary>
    public class WorkCrewLibrary : JobObjectLibraryBase<WorkCrew>, IWorkCrewLibrary
    {

        #region Constructors
        /// <summary>
        /// Creates a default WorkCrewLibrary
        /// </summary>
        public WorkCrewLibrary()
            : base()
        { }
        /// <summary>
        /// Creates a WorkCrewLibrary based on the Root Library passed.
        /// </summary>
        /// <param name="rootLibrary">The root TOPControlLibrary</param>
        public WorkCrewLibrary(ITOPControlLibrary rootLibrary)
            : base(rootLibrary)
        { }
        /// <summary>
        /// Creates a WorkCrewLibrary using the resources passed.
        /// </summary>
        /// <param name="useWorkCrewRepository">The Repository to use.</param>
        /// <param name="permissionManager">The IPermissionManager to use.</param>
        public WorkCrewLibrary(IRepository<WorkCrew> useWorkCrewRepository = null, IPermissionManager permissionManager = null)
            : base(useWorkCrewRepository, permissionManager)
        { }

        #endregion


        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        public override ValidationResult Validate(WorkCrew item)
        {
            var result = base.Validate(item);

            if (item != null)
            {
                if (item.CrewNumber < 1)
                {
                    result.AddViolation("Work Crew Number must be greater than zero.", "CrewNumber");
                }
                else
                {
                    var dups = Repository.All.Count(wc => wc.ID != item.ID && wc.JobID == item.JobID && wc.CrewNumber == item.CrewNumber);
                    if (dups > 0)
                    {
                        result.AddViolation("This job already has a work crew # " + item.CrewNumber.ToString(), "CrewNumber");
                    }
                }
                result.ValidateStringLength(item.Description, 0, 200, "Description");
            }

            return result;
        }


        #region IWorkCrewLibrary Members
        /// <summary>
        /// Returns all WorkCrew items belonging to this job.
        /// </summary>
        /// <param name="jobID">The ID of the job from which to pull.</param>
        /// <returns></returns>
        public IEnumerable<WorkCrew> AllByJob(long jobID)
        {
            var result = new List<WorkCrew>();
            var job = Root.JobActions.Find(jobID);
            if (job != null)
            {
                result = Repository.All.Where(wc => wc.JobID == jobID).ToList();
            }
            return result;
        }

        #endregion



        /// <summary>
        /// Copies values from one WorkCrew item to another.
        /// </summary>
        /// <param name="from">Object to copy from.</param>
        /// <param name="to">Object to copy to.</param>
        protected override void copyClassValues(WorkCrew from, WorkCrew to)
        {
            to.JobID = from.JobID;
            to.CrewNumber = from.CrewNumber;
            to.Description = from.Description;
        }


        /// <summary>
        /// Returns an instance of the target type if there is a valid item with a primary descriptive string matching
        /// the name passed, unless the item is marked deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job the object should belong to.</param>
        /// <param name="name">The full name string to search for.  In the case o</param>
        /// <returns></returns>
        public override WorkCrew FindByName(long jobID, string name)
        {
            return Repository.All.FirstOrDefault(wc => wc.JobID == jobID && wc.Description == name);
        }


    }
}
