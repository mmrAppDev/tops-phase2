﻿using System.Data.Entity.ModelConfiguration;
using Lib.TOPControls.Security;

namespace Lib.TOPControls.DataAccess.Mappings
{
    internal class UserRoleConfiguration : EntityTypeConfiguration<UserRole>
    {
        public UserRoleConfiguration()
        {
            HasRequired(a => a.TOPUser).WithMany().HasForeignKey(c => c.UserID);
            HasRequired(a => a.Role).WithMany(b => b.UserRoles).HasForeignKey(c => c.RoleID);
        }
    }
}