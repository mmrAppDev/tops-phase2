﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Lib.TOPControls.Charting;

namespace Lib.TOPControls.DataAccess.Mappings
{
    internal class ChartDefinitionConfigurationArrangementSituationInitializationCreation:EntityTypeConfiguration<ChartDefinition>
    {
        public ChartDefinitionConfigurationArrangementSituationInitializationCreation()
        {
            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            
        }
    }
}
