﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Lib.TOPControls.Charting;

namespace Lib.TOPControls.DataAccess.Mappings
{
    internal class HistoryChartDataPointConfiguration : EntityTypeConfiguration<HistoryChartDataPoint>
    {
        public HistoryChartDataPointConfiguration()
        {
            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.PercentComplete).HasPrecision(9, 4);

            HasRequired(x => x.HistoryChartData).WithMany(c => c.HistoryChartDataPoints).HasForeignKey(hcdp => hcdp.HistoryChartDataID);



        }
    }
}
