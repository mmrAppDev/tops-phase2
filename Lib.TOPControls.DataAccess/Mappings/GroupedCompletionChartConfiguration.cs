﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Lib.TOPControls.Charting;

namespace Lib.TOPControls.DataAccess.Mappings
{
    internal class GroupedCompletionChartConfiguration:EntityTypeConfiguration<GroupedCompletionChart>
    {
        public GroupedCompletionChartConfiguration()
        {
            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(x => x.JobInfo).WithMany().HasForeignKey(z => z.JobID);
            HasRequired(x => x.ChartDefinition).WithMany().HasForeignKey(hcd => hcd.ChartDefinitionID);


        }
            

    }
}
