﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Lib.TOPControls.DataAccess.Mappings
{
    // ************************************************************************
    // POCO Configuration


    // TaskProgress
    internal class TaskProgressConfiguration : EntityTypeConfiguration<TaskProgress>
    {
        public TaskProgressConfiguration()
        {
            ToTable("dbo.vwTaskProgressReporting");
            HasKey(x => x.ID);

            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CommodityID).HasColumnName("CommodityID").IsOptional();
            Property(tp => tp.TaskID)
                .IsRequired();
            Property(x => x.TrackedItemID).HasColumnName("TrackedItemID").IsRequired();
            Property(x => x.PercentComplete).HasColumnName("PercentComplete").IsRequired().HasPrecision(9, 2);
            Property(x => x.LastChanged).HasColumnName("LastChanged").IsRequired();

            Property(x => x.ManHoursPerItem).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(x => x.IsInDeletedScope).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            // Foreign keys

            HasOptional(a => a.Commodity).WithMany().HasForeignKey(c => c.CommodityID); // FK_TaskProgress_Commodity
            HasRequired(a => a.TrackedItem).WithMany(b => b.Tasks).HasForeignKey(c => c.TrackedItemID); // FK_TaskProgress_TrackedItem
            HasRequired(a => a.JobInfo).WithMany().HasForeignKey(c => c.JobID);
        }
    }
}
