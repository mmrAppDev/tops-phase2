﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Lib.TOPControls.Security;

namespace Lib.TOPControls.DataAccess.Mappings
{
    internal class CredentialConfiguration : EntityTypeConfiguration<Credential>
    {
        public CredentialConfiguration()
        {
            HasKey(x => x.ID);

            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.LastChanged).HasColumnName("LastChanged").IsRequired();

            HasRequired(a => a.TOPUser).WithMany().HasForeignKey(c => c.UserID);
        }
    }
}