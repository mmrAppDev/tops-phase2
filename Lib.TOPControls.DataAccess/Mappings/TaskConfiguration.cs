﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Lib.TOPControls.DataAccess.Mappings
{
    // ************************************************************************
    // POCO Configuration
    // Task
    internal class TaskConfiguration : EntityTypeConfiguration<Task>
    {
        public TaskConfiguration()
        {
            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(t => t.JobInfo).WithMany().HasForeignKey(t => t.JobID);
            HasRequired(t => t.ProjectPhase).WithMany(pp => pp.Tasks).HasForeignKey(t => t.PhaseID);
            HasOptional(t => t.DefaultCommodity).WithMany().HasForeignKey(t => t.DefaultCommodityID);
        }
    }
}