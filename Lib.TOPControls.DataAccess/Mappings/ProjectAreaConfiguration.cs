﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Lib.TOPControls.DataAccess.Mappings
{
    // ************************************************************************
    // POCO Configuration

    // ProjectArea
    internal class ProjectAreaConfiguration : EntityTypeConfiguration<ProjectArea>
    {
        public ProjectAreaConfiguration()
        {
            ToTable("dbo.ProjectArea");
            HasKey(x => x.ID);

            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.JobID).HasColumnName("JobID").IsRequired();
            Property(x => x.AreaNumber).HasColumnName("AreaNumber").IsRequired();
            Property(x => x.LastChanged).HasColumnName("LastChanged").IsRequired();

            // Foreign keys
            HasRequired(a => a.JobInfo).WithMany(b => b.ProjectAreas).HasForeignKey(c => c.JobID); // FK_ProjectArea_JobInfo
        }
    }

}
