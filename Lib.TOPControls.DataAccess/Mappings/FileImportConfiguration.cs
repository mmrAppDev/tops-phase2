﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Lib.TOPControls.DataImport;
using Lib.TOPControls.Security;

namespace Lib.TOPControls.DataAccess.Mappings
{
    /// <summary>
    /// The Mapping for the File Import Class
    /// </summary>
    public class FileImportConfiguration:EntityTypeConfiguration<FileImport>
    {
        /// <summary>
        /// Creates the FileImport mapper
        /// </summary>
        public FileImportConfiguration()
        {
            ToTable("dbo.FileImports");

            HasRequired(fi => fi.JobInfo).WithMany().HasForeignKey(x => x.JobID);
            HasRequired(fi => fi.TOPUser).WithMany().HasForeignKey(x => x.UserID);

            Ignore(fi => fi.RecordsetXmlDocument);

        }
    }
}
