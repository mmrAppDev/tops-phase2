﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Lib.TOPControls.DataAccess.Mappings
{
    // ************************************************************************
    // POCO Configuration

    // ObjectType
    internal class ObjectTypeConfiguration : EntityTypeConfiguration<ObjectType>
    {
        public ObjectTypeConfiguration()
        {
            ToTable("dbo.ObjectType");
            HasKey(x => x.ID);

            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.JobID).HasColumnName("JobID").IsRequired();
            Property(x => x.Name).HasColumnName("Name").IsRequired();
            Property(x => x.Description).HasColumnName("Description").IsRequired();
            Property(x => x.LastChanged).HasColumnName("LastChanged").IsRequired();

            // Foreign keys
            HasRequired(a => a.JobInfo).WithMany(b => b.ObjectTypes).HasForeignKey(c => c.JobID); // FK_ObjectType_JobInfo
        }
    }

}
