﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Lib.TOPControls.DataAccess.Mappings
{
    // ************************************************************************
    // POCO Configuration

    // ProjectSystem
    internal class ProjectSystemConfiguration : EntityTypeConfiguration<ProjectSystem>
    {
        public ProjectSystemConfiguration()
        {
            ToTable("dbo.ProjectSystem");
            HasKey(x => x.ID);

            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.JobID).HasColumnName("JobID").IsRequired();
            Property(x => x.SystemNumber).HasColumnName("SystemNumber").IsRequired();
            Property(x => x.LastChanged).HasColumnName("LastChanged").IsRequired();

            // Foreign keys
            HasRequired(a => a.JobInfo).WithMany(b => b.ProjectSystems).HasForeignKey(c => c.JobID); // FK_ProjectSystem_JobInfo
        }
    }
}
