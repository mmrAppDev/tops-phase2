﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Lib.TOPControls.DataAccess.Mappings
{
    // ************************************************************************
    // POCO Configuration

    // ProjectPhase
    internal class ProjectPhaseConfiguration : EntityTypeConfiguration<ProjectPhase>
    {
        public ProjectPhaseConfiguration()
        {
            ToTable("dbo.ProjectPhase");
            HasKey(x => x.ID);

            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.JobID).HasColumnName("JobID").IsRequired();
            Property(x => x.Title).HasColumnName("Title").IsRequired();
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.LastChanged).HasColumnName("LastChanged").IsRequired();

            // Foreign keys
            HasRequired(a => a.JobInfo).WithMany(b => b.ProjectPhases).HasForeignKey(c => c.JobID); // FK_ProjectPhase_JobInfo
        }
    }

}
