﻿using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using Lib.TOPControls;


namespace Lib.TOPControls.DataAccess.Mappings
{
    /// <summary>
    /// The mapper for the FilterConfiguration
    /// </summary>
    internal class FilterConfigurationConfiguration:EntityTypeConfiguration<FilterConfiguration>
    {
        /// <summary>
        /// Creates an instance of the  FitlerConfiguration mapper
        /// </summary>
        public FilterConfigurationConfiguration()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Table & Column Mappings
            this.ToTable("FilterConfigurations");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.JobID).HasColumnName("JobID");
            this.Property(t => t.FilterType).HasColumnName("FilterType");
            this.Property(t => t.DisplayOrder).HasColumnName("DisplayOrder");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.IsHighVolume).HasColumnName("IsHighVolume");
            this.Property(t => t.LastChanged).HasColumnName("LastChanged");

            // Relationships
            this.HasRequired(t => t.JobInfo)
                .WithMany(t => t.FilterConfigurations)
                .HasForeignKey(d => d.JobID);

        }
    }
}
