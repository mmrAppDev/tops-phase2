﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;


namespace Lib.TOPControls.DataAccess.Mappings
{
    internal class UserJobConfiguration:EntityTypeConfiguration<UserJob>
    {

        public UserJobConfiguration()
        {
            ToTable("dbo.UserJob");

            HasKey(uj => uj.ID);

            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(uj => uj.TOPUser).WithMany(tu => tu.UserJobs).HasForeignKey(uj => uj.UserID);
            HasRequired(uj => uj.JobInfo).WithMany(ji => ji.UserJobs).HasForeignKey(uj => uj.JobID);
        }


    }
}
