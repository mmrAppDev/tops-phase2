﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.DataAccess.Mappings
{
    // Commodity
    internal class CommodityConfiguration : EntityTypeConfiguration<Commodity>
    {
        public CommodityConfiguration()
        {
            ToTable("dbo.Commodity");
            HasKey(x => x.ID);

            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.JobID).HasColumnName("JobID").IsRequired();
            Property(x => x.CommodityCode).HasColumnName("CommodityCode").IsRequired();
            Property(x => x.Description).HasColumnName("Description").IsRequired();
            Property(x => x.ManHoursEstimated).HasColumnName("ManHoursEstimated").IsRequired().HasPrecision(16, 2);
            Property(x => x.LastChanged).HasColumnName("LastChanged").IsRequired();

            // Foreign keys
            HasRequired(a => a.JobInfo).WithMany(b => b.Commodities).HasForeignKey(c => c.JobID); // FK_Commodity_JobInfo
        }
    }
}
