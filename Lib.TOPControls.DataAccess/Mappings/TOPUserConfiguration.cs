﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Lib.TOPControls.DataAccess.Mappings
{
    // ************************************************************************
    // POCO Configuration

    // TOPUser
    internal class TopUserConfiguration : EntityTypeConfiguration<TOPUser>
    {
        public TopUserConfiguration()
        {
            ToTable("dbo.TOPUser");
            HasKey(x => x.ID);

            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Login).HasColumnName("Login").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsRequired();
            Property(x => x.LastName).HasColumnName("LastName").IsRequired();
            Property(x => x.Preferences).HasColumnName("Preferences").IsOptional();

            //Property(x => x.LastAccessDate).HasColumnName("LastAccessDate").IsOptional();
            Property(x => x.LastChanged).HasColumnName("LastChanged").IsRequired();
        }
    }
}
