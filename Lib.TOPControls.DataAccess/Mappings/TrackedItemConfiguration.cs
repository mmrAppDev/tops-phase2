﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.DataAccess.Mappings
{
        // TrackedItem
    internal class TrackedItemConfiguration : EntityTypeConfiguration<TrackedItem>
    {
        public TrackedItemConfiguration()
        {
            ToTable("dbo.TrackedItem");
            HasKey(x => x.ID);

            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.JobID).HasColumnName("JobID").IsRequired();
            Property(x => x.TrackingID).HasColumnName("TrackingID").IsRequired();
            Property(x => x.ObjectTypeID).HasColumnName("ObjectTypeID").IsOptional();
            Property(x => x.SystemID).HasColumnName("SystemID").IsOptional();
            Property(x => x.AreaID).HasColumnName("AreaID").IsOptional();
            Property(x => x.CraftID).HasColumnName("CraftID").IsOptional();
            Property(x => x.UDPValue1).HasColumnName("UDPValue1").IsOptional();
            Property(x => x.UDPValue2).HasColumnName("UDPValue2").IsOptional();
            Property(x => x.UDPValue3).HasColumnName("UDPValue3").IsOptional();
            Property(x => x.LastChanged).HasColumnName("LastChanged").IsRequired();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();

            // Foreign keys
            HasRequired(a => a.JobInfo).WithMany().HasForeignKey(c => c.JobID); // FK_TrackedItem_JobInfo
            HasOptional(a => a.ObjectType).WithMany().HasForeignKey(c => c.ObjectTypeID); // FK_TrackedItem_ObjectType
            HasOptional(a => a.ProjectSystem).WithMany().HasForeignKey(c => c.SystemID); // FK_TrackedItem_ProjectSystem
            HasOptional(a => a.ProjectArea).WithMany().HasForeignKey(c => c.AreaID); // FK_TrackedItem_ProjectArea
            HasOptional(a => a.CraftInfo).WithMany().HasForeignKey(c => c.CraftID); // FK_TrackedItem_CraftInfo
        }
    }


}
