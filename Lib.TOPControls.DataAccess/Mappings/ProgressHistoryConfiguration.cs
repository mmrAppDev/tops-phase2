﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Lib.TOPControls.Charting;

namespace Lib.TOPControls.DataAccess.Mappings
{
    /// <summary>
    /// Mapper for ProgressHistory items pulled from the database
    /// </summary>
    public class ProgressHistoryConfiguration : EntityTypeConfiguration<ProgressHistory>
    {
        /// <summary>
        /// Creates an instance of the ProgressHistory mapper
        /// </summary>
        public ProgressHistoryConfiguration()
        {
            ToTable("dbo.ProgressHistories");

            HasKey(ph => ph.ID);

            

            // Describe a link to the TaskProgress for this item.
            HasRequired(ph => ph.TaskProgress).WithMany().HasForeignKey(fk => fk.TaskProgressID);

        }
    }
}
