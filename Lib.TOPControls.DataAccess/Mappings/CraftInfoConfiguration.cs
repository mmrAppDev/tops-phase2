﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Lib.TOPControls.DataAccess.Mappings
{
    // ************************************************************************
    // POCO Configuration

    // CraftInfo
    internal class CraftInfoConfiguration : EntityTypeConfiguration<CraftInfo>
    {
        public CraftInfoConfiguration()
        {
            ToTable("dbo.CraftInfo");
            HasKey(x => x.ID);

            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.JobID).HasColumnName("JobID").IsRequired();
            Property(x => x.CraftCode).HasColumnName("CraftCode").IsRequired();
            Property(x => x.LastChanged).HasColumnName("LastChanged").IsRequired();

            // Foreign keys
            HasRequired(a => a.JobInfo).WithMany(b => b.Crafts).HasForeignKey(c => c.JobID); // FK_CraftInfo_JobInfo
        }
    }

}
