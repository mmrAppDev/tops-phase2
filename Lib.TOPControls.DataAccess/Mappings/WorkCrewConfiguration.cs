﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Lib.TOPControls.DataAccess.Mappings
{
    // ************************************************************************
    // POCO Configuration

    // WorkCrew
    internal class WorkCrewConfiguration : EntityTypeConfiguration<WorkCrew>
    {
        public WorkCrewConfiguration()
        {
            ToTable("dbo.WorkCrew");
            HasKey(x => x.ID);

            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.JobID).HasColumnName("JobID").IsRequired();
            Property(x => x.CrewNumber).HasColumnName("CrewNumber").IsRequired();
            Property(x => x.Description).HasColumnName("Description").IsRequired();
            Property(x => x.LastChanged).HasColumnName("LastChanged").IsRequired();

            // Foreign keys
            HasRequired(a => a.JobInfo).WithMany(b => b.WorkCrews).HasForeignKey(c => c.JobID); // FK_WorkCrew_JobInfo
        }
    }
}
