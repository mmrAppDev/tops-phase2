﻿using System.Data.Entity.ModelConfiguration;

namespace Lib.TOPControls.DataAccess.Mappings
{
    internal class ProgressLogEntryConfiguration : EntityTypeConfiguration<ProgressLogEntry>
    {
        public ProgressLogEntryConfiguration()
        {
            // Table & Column Mappings
            this.ToTable("ProgressLogEntry");

            // Relationships
            this.HasRequired(ple => ple.TOPUser).WithMany(tu => tu.ProgressLogEntries).HasForeignKey(fk => fk.UserID);
            this.HasOptional(ple => ple.TaskProgress).WithMany(tp => tp.ProgressLogEntries).HasForeignKey(fk => fk.TaskProgressID);
            this.HasOptional(ple => ple.WorkCrew).WithMany(wc => wc.ProgressLogEntries).HasForeignKey(fk => fk.WorkCrewID);
        }
    }
}