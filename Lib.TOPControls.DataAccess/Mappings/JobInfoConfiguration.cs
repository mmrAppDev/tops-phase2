﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Lib.TOPControls.DataAccess.Mappings
{
    // ************************************************************************
    // POCO Configuration

    // JobInfo
    internal class JobInfoConfiguration : EntityTypeConfiguration<JobInfo>
    {
        public JobInfoConfiguration()
        {
            ToTable("dbo.JobInfo");
            HasKey(x => x.ID);

            Property(x => x.ID).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Title).HasColumnName("Title").IsRequired();
            Property(x => x.UDPName1).HasColumnName("UDPName1").IsOptional();
            Property(x => x.UDPName2).HasColumnName("UDPName2").IsOptional();
            Property(x => x.UDPName3).HasColumnName("UDPName3").IsOptional();
            Property(x => x.LastChanged).HasColumnName("LastChanged").IsRequired();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
        }
    }
}