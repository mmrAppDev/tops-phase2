﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Lib.TOPControls.Charting;
using Lib.TOPControls.DataImport;
using Lib.TOPControls.Interfaces.DataAccess;
using Lib.TOPControls.Security;
using MMRCommon.Data;
using MMRCommon.Entity;

namespace Lib.TOPControls.DataAccess
{
    /// <summary>
    /// Implements the TOPControl Data access layer interface for SQL SErver.
    /// </summary>
    public class TOPControlSQLDAL : ITOPControlDAL
    {
        /// <summary>
        /// The DataContext of the TOPControl Application.
        /// </summary>
        public TOPControlDataContext context;

        /// <summary>
        /// A dictionary that allows for a single instance of a repository during a single execution.
        /// </summary>
        protected virtual Dictionary<Type, object> allRepositories { get; set; }

        #region Constructors

        /// <summary>
        /// Default constructor, instantiates its own data context.
        /// </summary>
        public TOPControlSQLDAL()
            : this(new TOPControlDataContext())
        {
        }

        /// <summary>
        /// Creates an instance using the connection object passed.
        /// </summary>
        /// <param name="conn">A connection to the database.</param>
        public TOPControlSQLDAL(DbConnection conn)
            : this(new TOPControlDataContext(conn))
        {
        }

        /// <summary>
        /// Creates an instance using the connection string passed.
        /// </summary>
        /// <param name="connectionString">A standard connection string.</param>
        public TOPControlSQLDAL(string connectionString)
            : this(new TOPControlDataContext(new SqlConnection(connectionString)))
        {
        }

        /// <summary>
        /// Creates an instance based on the fully instantiated TOPControlDataContext
        /// </summary>
        /// <param name="dataContext">The TOPControlDataContext to use.</param>
        public TOPControlSQLDAL(TOPControlDataContext dataContext)
        {
            context = dataContext;
            allRepositories = new Dictionary<Type, object>();
            setupRepositoryLazyLoad();
        }

        /// <summary>
        /// Sets the default Lazy IRepository items for all of the repository backing fields.
        /// </summary>
        protected virtual void setupRepositoryLazyLoad()
        {
            _commodityRep = GetLazyRep<Commodity>();
            _craftInfoRep = GetLazyRep<CraftInfo>();
            _jobInfoRep = GetLazyRep<JobInfo>();
            _objectTypeRep = GetLazyRep<ObjectType>();
            _projectAreaRep = GetLazyRep<ProjectArea>();
            _projectPhaseRep = GetLazyRep<ProjectPhase>();
            _projectSystemRep = GetLazyRep<ProjectSystem>();
            _taskProgressRep = GetLazyRep<TaskProgress>();
            _taskRep = GetLazyRep<Task>();
            _TOPUserRep = GetLazyRep<TOPUser>();
            _trackedItemRep = GetLazyRep<TrackedItem>();
            _workCrewRep = GetLazyRep<WorkCrew>();
            _filterConfigurationRep = GetLazyRep<FilterConfiguration>();
            _progressLogEntryRep = GetLazyRep<ProgressLogEntry>();
            _userJobRep = GetLazyRep<UserJob>();
            _credentialRep = GetLazyRep<Credential>();
            _roleRep = GetLazyRep<Role>();
            _userRoleRep = GetLazyRep<UserRole>();
            _fileImportRep = GetLazyRep<FileImport>();
            _chartDefinitionRep = GetLazyRep<Charting.ChartDefinition>();
            _historyChartDataRep = GetLazyRep<Charting.HistoryChartData>();
            _historyChartDataPointRep = GetLazyRep<Charting.HistoryChartDataPoint>();
            _groupedCompletionChartRep = GetLazyRep<Charting.GroupedCompletionChart>();
            _groupedCompletionChartDataPointRep = GetLazyRep<Charting.GroupedCompletionChartDataPoint>();
        }

        #endregion

        #region ITOPControlDAL Members

        private Lazy<IRepository<Commodity>> _commodityRep;

        /// <summary>
        /// Gets the repository for Commodity objects.
        /// </summary>
        public IRepository<Commodity> CommodityRep
        {
            get { return _commodityRep.Value; }
        }

        private Lazy<IRepository<CraftInfo>> _craftInfoRep;

        /// <summary>
        /// Gets the repository for CraftInfo objects.
        /// </summary>
        public IRepository<CraftInfo> CraftInfoRep
        {
            get { return _craftInfoRep.Value; }
        }

        private Lazy<IRepository<JobInfo>> _jobInfoRep;

        /// <summary>
        /// Gets the repository for JobInfo objects.
        /// </summary>
        public IRepository<JobInfo> JobInfoRep
        {
            get { return _jobInfoRep.Value; }
        }

        private Lazy<IRepository<ObjectType>> _objectTypeRep;

        /// <summary>
        /// Gets the repository for ObjectType objects.
        /// </summary>
        public IRepository<ObjectType> ObjectTypeRep
        {
            get { return _objectTypeRep.Value; }
        }

        private Lazy<IRepository<ProjectArea>> _projectAreaRep;

        /// <summary>
        /// Get a tree representation of ProjectSystem objects
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectSystem> GetProjectSystemTree(long jobID)
        {
            var sql = string.Format("SELECT * FROM [dbo].[GetProjectSystemTree] ({0})", jobID);

            var result =
                context.Database.SqlQuery<ProjectSystem>(sql).OrderBy(x => x.ParentId).ThenBy(x => x.SortOrder).Select(x => x.Clone()).ToList();

            var tree = result.Where(x => !x.ParentId.HasValue).ToList();

            PopulateProjectSystemTree(tree, result);

            return tree;
        }

        private void PopulateProjectSystemTree(IEnumerable<ProjectSystem> tree, IEnumerable<ProjectSystem> result)
        {
            foreach (var projectSystem in tree)
            {
                var children = result.Where(x => x.ParentId == projectSystem.ID);

                foreach (var child in children)
                {
                    projectSystem.AddChild(child);
                }

                PopulateProjectSystemTree(projectSystem.Children, result);
            }
        }

        /// <summary>
        /// Gets the repository for ProjectArea objects.
        /// </summary>
        public IRepository<ProjectArea> ProjectAreaRep
        {
            get { return _projectAreaRep.Value; }
        }

        private Lazy<IRepository<ProjectPhase>> _projectPhaseRep;

        /// <summary>
        /// Gets the repository for ProjectPhase objects.
        /// </summary>
        public IRepository<ProjectPhase> ProjectPhaseRep
        {
            get { return _projectPhaseRep.Value; }
        }

        private Lazy<IRepository<ProjectSystem>> _projectSystemRep;

        /// <summary>
        /// Gets the repository for ProjectSystem objects.
        /// </summary>
        public IRepository<ProjectSystem> ProjectSystemRep
        {
            get { return _projectSystemRep.Value; }
        }

        private Lazy<IRepository<TOPUser>> _TOPUserRep;

        /// <summary>
        /// Gets the repository for TOPUser objects.
        /// </summary>
        public IRepository<TOPUser> TOPUserRep
        {
            get { return _TOPUserRep.Value; }
        }

        private Lazy<IRepository<UserJob>> _userJobRep;

        /// <summary>
        /// Gets the repository for UserJob objects.
        /// </summary>
        public IRepository<UserJob> UserJobRep
        {
            get { return _userJobRep.Value; }
        }

        private Lazy<IRepository<TaskProgress>> _taskProgressRep;

        /// <summary>
        /// Gets the repository for TaskProgress objects.
        /// </summary>
        public IRepository<TaskProgress> TaskProgressRep
        {
            get { return _taskProgressRep.Value; }
        }

        private Lazy<IRepository<Task>> _taskRep;

        /// <summary>
        /// Gets the repository for Task objects.
        /// </summary>
        public IRepository<Task> TaskRep
        {
            get { return _taskRep.Value; }
        }

        private Lazy<IRepository<TrackedItem>> _trackedItemRep;

        /// <summary>
        /// Gets the repository for TrackedItem objects.
        /// </summary>
        public IRepository<TrackedItem> TrackedItemRep
        {
            get { return _trackedItemRep.Value; }
        }

        private Lazy<IRepository<WorkCrew>> _workCrewRep;

        /// <summary>
        /// Gets the repository for WorkCrew objects.
        /// </summary>
        public IRepository<WorkCrew> WorkCrewRep
        {
            get { return _workCrewRep.Value; }
        }

        private Lazy<IRepository<FilterConfiguration>> _filterConfigurationRep;

        /// <summary>
        /// Gets the repository for FilterConfiguraton objects.
        /// </summary>
        public IRepository<FilterConfiguration> FilterConfigurationRep
        {
            get { return _filterConfigurationRep.Value; }
        }

        private Lazy<IRepository<ProgressLogEntry>> _progressLogEntryRep;

        /// <summary>
        /// Repository for ProgressLogEntry objects
        /// </summary>
        public IRepository<ProgressLogEntry> ProgressLogEntryRep
        {
            get { return _progressLogEntryRep.Value; }
        }

        private Lazy<IRepository<Credential>> _credentialRep;

        /// <summary>
        /// Gets the repository for Security.Credential objects
        /// </summary>
        public IRepository<Credential> CredentialRep
        {
            get { return _credentialRep.Value; }
        }

        private Lazy<IRepository<Role>> _roleRep;

        /// <summary>
        /// Gets the repository for Security.Role objects
        /// </summary>
        public IRepository<Role> RoleRep
        {
            get { return _roleRep.Value; }
        }

        private Lazy<IRepository<UserRole>> _userRoleRep;

        /// <summary>
        /// Gets the repository for Security.UserRole objects.
        /// </summary>
        public IRepository<UserRole> UserRoleRep
        {
            get { return _userRoleRep.Value; }
        }

        private Lazy<IRepository<FileImport>> _fileImportRep;

        /// <summary>
        /// Repository for FileImport instances.
        /// </summary>
        public IRepository<FileImport> FileImportRep
        {
            get { return _fileImportRep.Value; }
        }

        /// <summary>
        /// Retrieves an IRepository for the type indicated in the call.
        /// </summary>
        /// <typeparam name="DOType">A System.Type representing a domain object.  Must have a parameterless constructor.</typeparam>
        /// <returns></returns>
        public IRepository<DOType> GetRepository<DOType>() where DOType : DomainObject, new()
        {
            IRepository<DOType> result = null;
            var targetType = typeof(DOType);

            if (allRepositories.ContainsKey(targetType))
            {
                result = allRepositories[targetType] as IRepository<DOType>;
            }
            if (result == null)
            {
                allRepositories.Remove(targetType);
                result = new EFDomainRepository<DOType>(context);
                allRepositories.Add(targetType, result);
            }

            return result;
        }

        /// <summary>
        /// Retreives a raw IQueryable for an item that exists in the data access layer and core, but has no 
        /// accessible library or repository.
        /// </summary>
        /// <typeparam name="T">The Type of object to retrieve.</typeparam>
        /// <returns></returns>
        public IQueryable<T> GetQueryable<T>()
            where T : class
        {
            return context.Set<T>().AsQueryable<T>();
        }

        #endregion

        /// <summary>
        /// Retrieves a Lazy of the repository for the type of object indicated.
        /// </summary>
        /// <typeparam name="DOT">A System.Type that is a subclass of DomainObject and has a parameterless constructor.</typeparam>
        /// <returns></returns>
        protected virtual Lazy<IRepository<DOT>> GetLazyRep<DOT>()
            where DOT : DomainObject, new()
        {
            return new Lazy<IRepository<DOT>>(() => GetRepository<DOT>());
        }

        #region ITOPControlDAL Members

        private Lazy<IRepository<Charting.ChartDefinition>> _chartDefinitionRep;

        /// <summary>
        /// Repository for ChartDefinitions
        /// </summary>
        public IRepository<Charting.ChartDefinition> ChartDefinitionRep
        {
            get { return _chartDefinitionRep.Value; }
        }

        private Lazy<IRepository<Charting.GroupedCompletionChartDataPoint>> _groupedCompletionChartDataPointRep;

        /// <summary>
        /// Repository for GroupedCompletionChartDataPoint
        /// </summary>
        public IRepository<Charting.GroupedCompletionChartDataPoint> GroupedCompletionChartDataPointRep
        {
            get { return _groupedCompletionChartDataPointRep.Value; }
        }

        private Lazy<IRepository<Charting.GroupedCompletionChart>> _groupedCompletionChartRep;

        /// <summary>
        /// Repository for Grouped Completion Chart
        /// </summary>
        public IRepository<Charting.GroupedCompletionChart> GroupedCompletionChartRep
        {
            get { return _groupedCompletionChartRep.Value; }
        }

        private Lazy<IRepository<Charting.HistoryChartDataPoint>> _historyChartDataPointRep;

        /// <summary>
        /// Repository for HistoryChartDataPoint.
        /// </summary>
        public IRepository<Charting.HistoryChartDataPoint> HistoryChartDataPointRep
        {
            get { return _historyChartDataPointRep.Value; }
        }

        private Lazy<IRepository<Charting.HistoryChartData>> _historyChartDataRep;

        /// <summary>
        /// Repository for HistoryChartData
        /// </summary>
        public IRepository<Charting.HistoryChartData> HistoryChartDataRep
        {
            get { return _historyChartDataRep.Value; }
        }

        #endregion

        /// <summary>
        /// Retrieves a set of Job ID values and work dates that have progress that has not been
        /// updated in the ProgressHistory.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UnrecognizedProgress> GetUnrecognizedProgress()
        {
            return
                context.Database.SqlQuery<UnrecognizedProgress>("exec [dbo].[spNeededProgressHistoryUpdates]").ToList();
        }

        /// <summary>
        /// Updates the ProgressHistory values for the job and date specified.
        /// </summary>
        /// <param name="jobID">ID of the job entries to update.</param>
        /// <param name="workDate">The work date to check for progress.</param>
        public void UpdateProgressHistory(long jobID, DateTime workDate)
        {
            context.Database.ExecuteSqlCommand("exec [dbo].[spUpdateProgressHistories] @JobID, @WorkDate",
                new SqlParameter("@JobID", jobID),
                new SqlParameter("@WorkDate", workDate));
            context.Database.Connection.Close();
        }

        /// <summary>
        /// Sets the Commodity for all TaskProgress items within the identified tasks, with certain specific tolerances identified by arguments.
        /// </summary>
        /// <param name="taskID">Required, the TaskID to use when selecting items to change.</param>
        /// <param name="commodityID">The ID of the new Commodity to set on these task items. Will clear the commodity if null.</param>
        /// <param name="matchCommodity">If set, the operation will only change the commodity of items that have a commodity ID matching this value.  If null, will always change the current Commodities set on TaskProgress items.</param>
        /// <param name="setNulls">If true, this operation will set values that currently have no commodity set, as long as they are part of the task.</param>
        /// <returns></returns>
        public int PropogateCommodityByTask(long taskID, long? commodityID, long? matchCommodity = null,
            bool setNulls = true)
        {
            int result = 0;

            // Get items not deleted and in the identified task.
            var targets = TaskProgressRep.All.Where(x => !x.IsDeleted && x.TaskID == taskID
                // Include Items with a value, matching the target commodity, if passed
                                                         &&
                                                         ((x.CommodityID.HasValue &&
                                                           (!matchCommodity.HasValue ||
                                                            matchCommodity.Value == x.CommodityID.Value))
                                                             // Include Items with null values, if set nulls is true.
                                                          || (!x.CommodityID.HasValue && setNulls))).ToList();

            // store the matching context items
            var octx = ((IObjectContextAdapter) context).ObjectContext;
            octx.Refresh(RefreshMode.ClientWins, targets);

            var tidParam = new SqlParameter("@TaskID", SqlDbType.BigInt);
            var ncidParam = new SqlParameter("@NewCommodityID", SqlDbType.BigInt);
            var mcidParam = new SqlParameter("@MatchCommodityID", SqlDbType.BigInt);
            var snullParam = new SqlParameter("@SetNulls", SqlDbType.Bit);

            tidParam.Value = taskID;
            ncidParam.Value = (commodityID.HasValue ? (object) commodityID.Value : DBNull.Value);
            mcidParam.Value = (matchCommodity.HasValue ? (object) matchCommodity.Value : DBNull.Value);
            snullParam.Value = setNulls;

            result =
                context.Database.ExecuteSqlCommand(
                    "Update TaskProgress Set CommodityID = @NewCommodityID Where IsDeleted = 0 And TaskID = @TaskID And ((CommodityID IS NULL And @SetNulls = 1) Or (NOT CommodityID IS NULL And (@MatchCommodityID IS NULL Or CommodityID = @MatchCommodityID)))",
                    tidParam, ncidParam, mcidParam, snullParam);

            // refresh the matching context items
            octx.Refresh(RefreshMode.StoreWins, targets);

            return result;
        }

        internal DbEntityEntry<DOType> GetEntityEntry<DOType>(DOType item) where DOType : DomainObject
        {
            var result = context.Entry<DOType>(item);
            return result;
        }

        /// <summary>
        /// Retrieves currently persisted value of the item for the property specified.
        /// </summary>
        /// <typeparam name="DOType">The DomainObject Type of the item.</typeparam>
        /// <typeparam name="TProperty">The datatype of the Property being retrieved</typeparam>
        /// <param name="item">The item for which the committed property value should be retrieved.</param>
        /// <param name="property">An expression identifying the property</param>
        /// <returns></returns>
        public TProperty GetCommittedPropertyValue<DOType, TProperty>(DOType item,
            Expression<Func<DOType, TProperty>> property)
            where DOType : DomainObject
        {
            return (TProperty) GetEntityEntry(item).Property(property).OriginalValue;
        }

        /// <summary>
        /// Removes the current chart data for the Job indicated or All jobs if a Zero value is passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to purge, or 0 if all jobs should have their current chart data wiped.</param>
        public void PurgeReportingData(long jobID = 0)
        {
            context.Database.ExecuteSqlCommand("exec [dbo].[spPurgeReportingData] @JobID",
                new SqlParameter("@JobID", jobID));
            context.Database.Connection.Close();
        }

        /// <summary>
        /// Using one of the optional arguments passed, calculates the Historical Chart Values based on a stored procedure.
        /// </summary>
        /// <param name="jobID">ID of the job to use to filter history results.</param>
        /// <param name="phaseID">ID of phase by which to filter.</param>
        /// <param name="taskID">ID of the task by which to filter.</param>
        /// <param name="trackedItemID">ID of the task by which to filter.</param>
        /// <param name="systemID">ID of the system by which to filter.</param>
        /// <param name="commodityID">ID of the system by which to filter.</param>
        /// <param name="areaID">ID of the area by which to filter.</param>
        /// <param name="objectTypeID">ID of the object type by which to filter.</param>
        /// <param name="craftID">ID of the craft by which to filter.</param>
        /// <param name="udpValue1">The first UDP Value to use as a filter.</param>
        /// <param name="udpValue2">The second UDP Value to use as a filter.</param>
        /// <param name="udpValue3">The third UDP Value to use as a filter.</param>
        /// <returns></returns>
        public List<RawHistoryDataPoint> SelectNewHistoryChartData(long? jobID = null, long? phaseID = null,
            long? taskID = null, long? trackedItemID = null, long? systemID = null, long? commodityID = null,
            long? areaID = null, long? objectTypeID = null, long? craftID = null, string udpValue1 = null,
            string udpValue2 = null, string udpValue3 = null, DateTime? workDate = null)
        {
            var result = new List<RawHistoryDataPoint>();

            var parmList = new List<SqlParameter>();

            if (jobID.HasValue) parmList.Add(new SqlParameter("@JobID", jobID.Value));
            if (phaseID.HasValue) parmList.Add(new SqlParameter("@PhaseID", phaseID.Value));
            if (taskID.HasValue) parmList.Add(new SqlParameter("@TaskID", taskID.Value));
            if (trackedItemID.HasValue) parmList.Add(new SqlParameter("@TrackedItemID", trackedItemID.Value));
            if (systemID.HasValue) parmList.Add(new SqlParameter("@SystemID", systemID.Value));
            if (commodityID.HasValue) parmList.Add(new SqlParameter("@CommodityID", commodityID.Value));
            if (areaID.HasValue) parmList.Add(new SqlParameter("@AreaID", areaID.Value));
            if (objectTypeID.HasValue) parmList.Add(new SqlParameter("@ObjectTypeID", objectTypeID.Value));
            if (craftID.HasValue) parmList.Add(new SqlParameter("@CraftID", craftID.Value));
            if (udpValue1 != null) parmList.Add(new SqlParameter("@UDPValue1", udpValue1));
            if (udpValue2 != null) parmList.Add(new SqlParameter("@UDPValue2", udpValue2));
            if (udpValue3 != null) parmList.Add(new SqlParameter("@UDPValue3", udpValue3));
            if (workDate.HasValue) parmList.Add(new SqlParameter("@WorkDate", workDate.Value));

            if (parmList.Count == 0) throw new ArgumentException("At least ONE argument must be set.");

            var querystring = new StringBuilder();
            foreach (var parm in parmList)
            {
                if (querystring.Length == 0)
                {
                    querystring.Append("exec [dbo].[spSelectNewHistoryChartData] ");
                }
                else
                {
                    querystring.Append(", ");
                }
                querystring.AppendFormat("{0}={0}", parm.ParameterName);
            }

            result = context.Database.SqlQuery<RawHistoryDataPoint>(querystring.ToString(), parmList.ToArray()).ToList();

            return result;
        }
    }
}