﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Lib.TOPControls;

namespace MMRCommon.Entity
{
    /// <summary>
    /// A base class for repositories accessing 
    /// </summary>
    /// <typeparam name="DOType">A subclass of DomainObject to use to provide access to a matching database table.</typeparam>
    public class EFDomainRepository<DOType>:EFRepository<DOType> where DOType:DomainObject, new()
    {
        /// <summary>
        /// Creates a repository for using Entity Framwork to add and change data that uses the 
        /// domain object class.
        /// </summary>
        /// <param name="dbContext"></param>
        public EFDomainRepository(DbContext dbContext)
            : base(dbContext)
        { }


        /// <summary>
        /// Returns a record from the data matching the id passed.
        /// </summary>
        /// <param name="id">A long integer to compare to the ID of each record.</param>
        /// <returns></returns>
        public override DOType Find(long id)
        {
            return dbSet.SingleOrDefault(dot => dot.ID == id);
        }

        

    }
}
