﻿using System.Data.Common;
using System.Data.Entity;
using Lib.TOPControls.Charting;
using Lib.TOPControls.DataAccess.Mappings;
using Lib.TOPControls.DataImport;
using Lib.TOPControls.Security;

namespace Lib.TOPControls.DataAccess
{
    /// <summary>
    /// The database context for the Data Access Layer
    /// </summary>
    public class TOPControlDataContext : DbContext
    {
        /// <summary>
        /// Commodity items
        /// </summary>
        public IDbSet<Commodity> Commodity { get; set; } // Commodity
        /// <summary>
        /// CraftInfo items.
        /// </summary>
        public IDbSet<CraftInfo> CraftInfo { get; set; } // CraftInfo
        /// <summary>
        /// JobInfo items
        /// </summary>
        public IDbSet<JobInfo> JobInfo { get; set; } // JobInfo
        /// <summary>
        /// ObjectType items.
        /// </summary>
        public IDbSet<ObjectType> ObjectType { get; set; } // ObjectType
        /// <summary>
        /// ProjectArea items.
        /// </summary>
        public IDbSet<ProjectArea> ProjectArea { get; set; } // ProjectArea
        /// <summary>
        /// ProjectPhase items
        /// </summary>
        public IDbSet<ProjectPhase> ProjectPhase { get; set; } // ProjectPhase
        /// <summary>
        /// ProjectSystem items
        /// </summary>
        public IDbSet<ProjectSystem> ProjectSystem { get; set; } // ProjectSystem
        /// <summary>
        /// Task items
        /// </summary>
        public IDbSet<Task> Task { get; set; } // Task
        /// <summary>
        /// TaskProgress items
        /// </summary>
        public IDbSet<TaskProgress> TaskProgress { get; set; } // TaskProgress
        /// <summary>
        /// TOPUser items
        /// </summary>
        public IDbSet<TOPUser> TOPUser { get; set; } // TOPUser
        /// <summary>
        /// UserJob items
        /// </summary>
        public IDbSet<UserJob> UserJob { get; set; }    // UserJob
        /// <summary>
        /// TrackedItem items
        /// </summary>
        public IDbSet<TrackedItem> TrackedItem { get; set; } // TrackedItem
        /// <summary>
        /// FilterConfiguration items
        /// </summary>
        public IDbSet<FilterConfiguration> FilterConfiguration { get; set; } // FilterConfiguration
        /// <summary>
        /// ProgressLogEntry items
        /// </summary>
        public IDbSet<ProgressLogEntry> ProgressLogEntry { get; set; } // ProgressLogEntry
        /// <summary>
        /// WorkCrew items
        /// </summary>
        public IDbSet<WorkCrew> WorkCrew { get; set; } // WorkCrew

        /// <summary>
        /// Credential items
        /// </summary>
        public IDbSet<Credential> Credential { get; set; }   // Security.Credential

        /// <summary>
        /// Role items
        /// </summary>
        public IDbSet<Role> Role { get; set; }   // Security.Role

        /// <summary>
        /// User Role items.
        /// </summary>
        public IDbSet<UserRole> UserRole { get; set; }   // Security.UserRole


        /// <summary>
        /// ProgressHistory items.  Included primarily to allow Linq aggregate syntax.
        /// </summary>
        public IDbSet<ProgressHistory> ProgressHistory { get; set; }

        /// <summary>
        /// FileImport items. 
        /// </summary>
        public IDbSet<FileImport> FileImports { get; set; }

        /// <summary>
        /// Definitions de la Chartes
        /// </summary>
        public IDbSet<ChartDefinition> ChartDefinitions { get; set; }

        /// <summary>
        /// History Chart Data access
        /// </summary>
        public IDbSet<HistoryChartData> HistoryChartData { get; set; }

        /// <summary>
        /// History Chart Data Points access
        /// </summary>
        public IDbSet<HistoryChartDataPoint> HistoryChartDataPoints { get; set; }

        /// <summary>
        /// Access for GroupedCompletionCharts.
        /// </summary>
        public IDbSet<GroupedCompletionChart> GroupedCompletionCharts { get; set; }

        /// <summary>
        /// Access for GroupedCompletionChartData.... whatever.
        /// </summary>
        public IDbSet<GroupedCompletionChartDataPoint> GroupedCompletionChartDataPoints { get; set; }


        /// <summary>
        /// Prevents context from creating objects to match the reflected class definitions.
        /// </summary>
        static TOPControlDataContext()
        {
            Database.SetInitializer<TOPControlDataContext>(null);
        }
        /// <summary>
        /// Creates a default TOPControlDataContext
        /// </summary>
        public TOPControlDataContext()
            : base()
        { }
        /// <summary>
        /// Creates a TOPControlDataContext based on a connection string.
        /// </summary>
        /// <param name="connectionString">A pretty standard connection string.</param>
        public TOPControlDataContext(string connectionString)
            : base(connectionString)
        { }
        /// <summary>
        /// Creates a TOPControlDataContext based on a connection.
        /// </summary>
        /// <param name="conn">A DbConnection</param>
        public TOPControlDataContext(DbConnection conn)
            : base(conn, false)
        { }


        /// <summary>
        /// Adds configurations to the modelBuilder
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FilterConfiguration>().Ignore(fc => fc.FilterValueType);
            modelBuilder.Entity<ChartDefinition>().Ignore(cd => cd.FilterValueType);
            modelBuilder.Entity<ChartDefinition>().Ignore(cd => cd.ChartType);


            base.OnModelCreating( modelBuilder);

            modelBuilder.Configurations.Add(new CommodityConfiguration());
            modelBuilder.Configurations.Add(new CraftInfoConfiguration());
            modelBuilder.Configurations.Add(new JobInfoConfiguration());
            modelBuilder.Configurations.Add(new ObjectTypeConfiguration());
            modelBuilder.Configurations.Add(new ProjectAreaConfiguration());
            modelBuilder.Configurations.Add(new ProjectPhaseConfiguration());
            modelBuilder.Configurations.Add(new ProjectSystemConfiguration());
            modelBuilder.Configurations.Add(new TaskProgressConfiguration());
            modelBuilder.Configurations.Add(new TopUserConfiguration());
            modelBuilder.Configurations.Add(new TrackedItemConfiguration());
            modelBuilder.Configurations.Add(new TaskConfiguration());
            modelBuilder.Configurations.Add(new WorkCrewConfiguration());
            modelBuilder.Configurations.Add(new FilterConfigurationConfiguration());
            modelBuilder.Configurations.Add(new ProgressLogEntryConfiguration());
            modelBuilder.Configurations.Add(new UserJobConfiguration());
            modelBuilder.Configurations.Add(new CredentialConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Configurations.Add(new UserRoleConfiguration());
            modelBuilder.Configurations.Add(new ProgressHistoryConfiguration());
            modelBuilder.Configurations.Add(new FileImportConfiguration());
            modelBuilder.Configurations.Add(new ChartDefinitionConfigurationArrangementSituationInitializationCreation());
            modelBuilder.Configurations.Add(new HistoryChartDataConfiguration());
            modelBuilder.Configurations.Add(new HistoryChartDataPointConfiguration());
            modelBuilder.Configurations.Add(new GroupedCompletionChartConfiguration());
            modelBuilder.Configurations.Add(new GroupedCompletionChartDataPointConfiguration());
        }
    }
}