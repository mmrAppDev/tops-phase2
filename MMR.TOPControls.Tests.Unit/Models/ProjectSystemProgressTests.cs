﻿using System;
using System.Collections.Generic;
using System.Linq;
using Envoc.Core.Shared.Data;
using Envoc.Core.UnitTests.Extensions;
using Lib.TOPControls.Charting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace MMR.TOPControls.Tests.Unit.Models
{
    [TestClass]
    public class ProjectSystemProgressTests
    {
        private IRepository<ProjectSystemProgress> projectSystemProgressRepository;
        private ProjectSystemProgress target;

        [TestInitialize]
        public void Init()
        {
            projectSystemProgressRepository = Substitute.For<IRepository<ProjectSystemProgress>>();
            projectSystemProgressRepository.Select().Returns(GetSystems());
            target = new ProjectSystemProgress();
        }

        private IQueryable<ProjectSystemProgress> GetSystems()
        {
            var systemWithChildren = new ProjectSystemProgress {ID = 2, Name = "Parent System 2"};
            systemWithChildren.AddChild(new ProjectSystemProgress
            {
                ID = 12,
                Name = "Child 1 of System 2",
                CompletedManhours = 30,
                TotalManhours = 100,
                PercentComplete = 0.33M
            });
            systemWithChildren.AddChild(new ProjectSystemProgress
            {
                ID = 22,
                Name = "Child 2 of System 2",
                CompletedManhours = 60,
                TotalManhours = 100,
                PercentComplete = 0.66M
            });
            systemWithChildren.AddChild(new ProjectSystemProgress
            {
                ID = 32,
                Name = "Child 3 of System 2",
                CompletedManhours = 100,
                TotalManhours = 100,
                PercentComplete = 1M
            });

            var systemWithGrandChildren = new ProjectSystemProgress {ID = 3, Name = "Parent System 3"};
            var child = new ProjectSystemProgress
            {
                ID = 13,
                Name = "Child 1 of System 3",
                CompletedManhours = 30,
                TotalManhours = 100,
                PercentComplete = 0.33M
            };
            child.AddChild(new ProjectSystemProgress
            {
                ID = 113,
                Name = "Grandchild 1 of Child 1 of System 3",
                CompletedManhours = 30,
                TotalManhours = 100,
                PercentComplete = 0.33M
            });
            systemWithGrandChildren.AddChild(child);

            var systems = new List<ProjectSystemProgress>
            {
                new ProjectSystemProgress
                {
                    ID = 1,
                    Name = "Parent System 1",
                    CompletedManhours = 100,
                    PercentComplete = 0.25M,
                    TotalManhours = 400
                },
                new ProjectSystemProgress {ID = 4, Name = "Parent System 4"},
                systemWithChildren,
                systemWithGrandChildren
            };

            return systems.AsQueryable();
        }

        [TestClass]
        public class TheHasChildrenMethod : ProjectSystemProgressTests
        {
            [TestMethod]
            public void ReturnsFalseByDefault()
            {
                //act
                bool result = target.HasChildren();

                //assert
                result.ShouldBe(false);
            }

            [TestMethod]
            public void ReturnsFalse_WhenChildrenIsNull()
            {
                //act
                bool result = target.HasChildren();

                //assert
                result.ShouldBe(false);
            }

            [TestMethod]
            public void ReturnsTrue_WhenChildrenHasAtLeastOneItem()
            {
                //arrange
                target.AddChild(new ProjectSystemProgress());

                //act
                bool result = target.HasChildren();

                //assert
                result.ShouldBe(true);
            }
        }

        [TestClass]
        public class TheCompletedManhoursProperty : ProjectSystemProgressTests
        {
            [TestMethod]
            public void ReturnsPropertyValue_WhenHasChildrenIsFalse()
            {
                //arrange
                var system = projectSystemProgressRepository.Select().First(x => !x.HasChildren());

                //act
                decimal result = system.CompletedManhours;

                //assert
                result.ShouldBe(100);
            }

            [TestMethod]
            public void ReturnsSumOfChildrenRollupValue_WhenHasChildrenIsTrue()
            {
                //arrange
                var system = projectSystemProgressRepository.Select().First(x => x.HasChildren());
                system.CompletedManhours = 20;

                //act
                decimal result = system.CompletedManhours;

                //assert
                result.ShouldBe(190);
            }
        }

        [TestClass]
        public class TheTotalManhoursProperty : ProjectSystemProgressTests
        {
            [TestMethod]
            public void ReturnsPropertyValue_WhenHasChildrenIsFalse()
            {
                //arrange
                var system = projectSystemProgressRepository.Select().First(x => !x.HasChildren());

                //act
                decimal result = system.TotalManhours;

                //assert
                result.ShouldBe(400);
            }

            [TestMethod]
            public void ReturnsSumOfChildrenRollupValue_WhenHasChildrenIsTrue()
            {
                //arrange
                var system = projectSystemProgressRepository.Select().First(x => x.HasChildren());
                system.TotalManhours = 20;

                //act
                decimal result = system.TotalManhours;

                //assert
                result.ShouldBe(300);
            }
        }

        [TestClass]
        public class ThePercentCompleteProperty : ProjectSystemProgressTests
        {
            [TestMethod]
            public void ReturnsPropertyValue_WhenHasChildrenIsFalse()
            {
                //arrange
                var system = projectSystemProgressRepository.Select().First(x => !x.HasChildren());

                //act
                decimal result = system.PercentComplete;

                //assert
                result.ShouldBe(0.25M);
            }

            [TestMethod]
            public void ReturnsAverageOfChildrenRollupValue_WhenHasChildrenIsTrue()
            {
                //arrange
                var system = projectSystemProgressRepository.Select().First(x => x.HasChildren());
                system.PercentComplete = 20;

                //act
                decimal result = system.PercentComplete;

                //assert
                Math.Round(result, 6).ShouldBe(0.663333M);
            }
        }

        [TestClass]
        public class ThePathProperty : ProjectSystemProgressTests
        {
            [TestMethod]
            public void ReturnsSystemId_WhenParentIsNull()
            {
                //arrange
                var system = projectSystemProgressRepository.Select().First(x => !x.HasChildren());

                //assert
                system.Path.ShouldBe(system.ID.ToString());
            }

            [TestMethod]
            public void ReturnsParentIDUnderscoreThenID_WhenParentIsNotNullAndItemIsAChild()
            {
                //arrange
                var system = projectSystemProgressRepository.Select().First(x => x.HasChildren());
                var child = system.Children.First();

                //assert
                child.Path.ShouldBe("2_12");
            }

            [TestMethod]
            public void
                ReturnsGrandparentParentIDUnderscoreThenParentParentIDUnderscoreThenID_WhenGrandParentAndParentAreNotNullAndItemIsAChild
                ()
            {
                //arrange
                var system = projectSystemProgressRepository.Select().First(x => x.ID == 3);
                var child = system.Children.First();
                var grandChild = child.Children.First();

                //assert
                grandChild.Path.ShouldBe("3_13_113");
            }
        }
    }
}