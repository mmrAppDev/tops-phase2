﻿using System.Collections.Generic;
using System.Linq;
using Envoc.Core.Shared.Data;
using Envoc.Core.Shared.Extensions;
using Envoc.Core.UnitTests.Extensions;
using Lib.TOPControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace MMR.TOPControls.Tests.Unit.Models
{
    [TestClass]
    public class ProjectSystemTests
    {
        private IRepository<ProjectSystem> projectSystemRepository;
        private ProjectSystem target;

        [TestInitialize]
        public void Init()
        {
            projectSystemRepository = Substitute.For<IRepository<ProjectSystem>>();
            projectSystemRepository.Select().Returns(GetSystems());
            target = new ProjectSystem();
        }

        private IQueryable<ProjectSystem> GetSystems()
        {
            var systems = new List<ProjectSystem>
            {
                new ProjectSystem {ID = 1, SystemNumber = "System Child 1", SortOrder = 0},
                new ProjectSystem {ID = 2, SystemNumber = "System Child 2", SortOrder = 1},
                new ProjectSystem {ID = 3, SystemNumber = "System Child 3", SortOrder = 2},
                new ProjectSystem {ID = 4, SystemNumber = "System Child 4", SortOrder = 3}
            };

            return systems.AsQueryable();
        }

        [TestClass]
        public class TheChangeSortOrderMethod : ProjectSystemTests
        {
            [TestMethod]
            public void AcceptsAProjectSystemAndANewPositionInteger()
            {
                //arrange
                var result = projectSystemRepository.Select();

                //act
                result.ToList().ChangeSortOrder(new ProjectSystem {ID = 1}, 1);
            }

            [TestMethod]
            public void AProjectSystemWithTheSortOrderOf0ChangedTo1IsMovedToTheSecondPosition()
            {
                //arrange
                IList<ProjectSystem> originalOrder = GetSystems();

                //act
                originalOrder.ChangeSortOrder(originalOrder.First(), 1);
                var sortedOrder = originalOrder.OrderBy(x => x.SortOrder);

                //assert
                sortedOrder.ToList()[0].SortOrder.ShouldBe(0);
                sortedOrder.ToList()[0].ID.ShouldBe(2);
                sortedOrder.ToList()[1].SortOrder.ShouldBe(1);
                sortedOrder.ToList()[1].ID.ShouldBe(1);
                sortedOrder.ToList()[2].SortOrder.ShouldBe(2);
                sortedOrder.ToList()[2].ID.ShouldBe(3);
                sortedOrder.ToList()[3].SortOrder.ShouldBe(3);
                sortedOrder.ToList()[3].ID.ShouldBe(4);
            }

            [TestMethod]
            public void AFactWithTheSortOrderOf2ChangeTo0IsMovedToTheFirstPosition()
            {
                //arrange
                IList<ProjectSystem> originalOrder = GetSystems();

                //act
                originalOrder.ChangeSortOrder(originalOrder.First(x => x.ID == 3), 0);
                var sortedOrder = originalOrder.OrderBy(x => x.SortOrder);

                //assert
                sortedOrder.ToList()[0].SortOrder.ShouldBe(0);
                sortedOrder.ToList()[0].ID.ShouldBe(3);
                sortedOrder.ToList()[1].SortOrder.ShouldBe(1);
                sortedOrder.ToList()[1].ID.ShouldBe(1);
                sortedOrder.ToList()[2].SortOrder.ShouldBe(2);
                sortedOrder.ToList()[2].ID.ShouldBe(2);
                sortedOrder.ToList()[3].SortOrder.ShouldBe(3);
                sortedOrder.ToList()[3].ID.ShouldBe(4);
            }

            [TestMethod]
            public void AFactWithTheSortOrderOf3ChangeTo2IsMovedToTheThirdPosition()
            {
                //arrange
                IList<ProjectSystem> originalOrder = GetSystems();

                //act
                originalOrder.ChangeSortOrder(originalOrder.First(x => x.ID == 4), 2);
                var sortedOrder = originalOrder.OrderBy(x => x.SortOrder);

                //assert
                sortedOrder.ToList()[0].SortOrder.ShouldBe(0);
                sortedOrder.ToList()[0].ID.ShouldBe(1);
                sortedOrder.ToList()[1].SortOrder.ShouldBe(1);
                sortedOrder.ToList()[1].ID.ShouldBe(2);
                sortedOrder.ToList()[2].SortOrder.ShouldBe(2);
                sortedOrder.ToList()[2].ID.ShouldBe(4);
                sortedOrder.ToList()[3].SortOrder.ShouldBe(3);
                sortedOrder.ToList()[3].ID.ShouldBe(3);
            }

            [TestMethod]
            public void AFactWithTheSortOrderOf0ChangeTo3IsMovedToTheFourthPosition()
            {
                //arrange
                IList<ProjectSystem> originalOrder = GetSystems();

                //act
                originalOrder.ChangeSortOrder(originalOrder.First(x => x.ID == 1), 3);
                var sortedOrder = originalOrder.OrderBy(x => x.SortOrder);

                //assert
                sortedOrder.ToList()[0].SortOrder.ShouldBe(0);
                sortedOrder.ToList()[0].ID.ShouldBe(2);
                sortedOrder.ToList()[1].SortOrder.ShouldBe(1);
                sortedOrder.ToList()[1].ID.ShouldBe(3);
                sortedOrder.ToList()[2].SortOrder.ShouldBe(2);
                sortedOrder.ToList()[2].ID.ShouldBe(4);
                sortedOrder.ToList()[3].SortOrder.ShouldBe(3);
                sortedOrder.ToList()[3].ID.ShouldBe(1);
            }

            [TestMethod]
            public void AFactWithTheSortOrderOf3ChangeTo0IsMovedToTheFirstPosition()
            {
                //arrange
                IList<ProjectSystem> originalOrder = GetSystems();

                //act
                originalOrder.ChangeSortOrder(originalOrder.First(x => x.ID == 4), 0);
                var sortedOrder = originalOrder.OrderBy(x => x.SortOrder);

                //assert
                sortedOrder.ToList()[0].SortOrder.ShouldBe(0);
                sortedOrder.ToList()[0].ID.ShouldBe(4);
                sortedOrder.ToList()[1].SortOrder.ShouldBe(1);
                sortedOrder.ToList()[1].ID.ShouldBe(1);
                sortedOrder.ToList()[2].SortOrder.ShouldBe(2);
                sortedOrder.ToList()[2].ID.ShouldBe(2);
                sortedOrder.ToList()[3].SortOrder.ShouldBe(3);
                sortedOrder.ToList()[3].ID.ShouldBe(3);
            }

            private List<ProjectSystem> GetSystems()
            {
                return projectSystemRepository.Select().ToList();
            }
        }

        [TestClass]
        public class TheHasChildrenMethod : ProjectSystemTests
        {
            [TestMethod]
            public void ReturnsFalseByDefault()
            {
                //act
                bool result = target.HasChildren();

                //assert
                result.ShouldBe(false);
            }

            [TestMethod]
            public void ReturnsFalse_WhenChildrenIsNull()
            {
                //act
                bool result = target.HasChildren();

                //assert
                result.ShouldBe(false);
            }

            [TestMethod]
            public void ReturnsTrue_WhenChildrenHasAtLeastOneItem()
            {
                //arrange
                target.AddChild(new ProjectSystem());

                //act
                bool result = target.HasChildren();

                //assert
                result.ShouldBe(true);
            }
        }
    }
}