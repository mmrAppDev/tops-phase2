using FluentValidation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MMR.TOPControls.Tests.Unit
{
    [TestClass]
    public class AssemblySetup
    {
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            ValidatorOptions.ResourceProviderType = typeof(Envoc.Core.Shared.Model.ValidationMessageTemplates);
        }
    }
}
