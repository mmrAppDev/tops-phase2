﻿
namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// Identifies the kind of Chart a set of values are intended to populate.
    /// </summary>
    public enum ChartType
    {
        /// <summary>
        /// The Chart Type is not Defined.
        /// </summary>
        None = 0,

        /// <summary>
        /// A Historical Chart, displaying the completion percentages at various points in time.
        /// </summary>
        Historical = 1,

        /// <summary>
        /// A Grouped Completion Chart, displaying the percent complete for each group of a specified type.
        /// </summary>
        GroupedCompletion = 2
    }


}