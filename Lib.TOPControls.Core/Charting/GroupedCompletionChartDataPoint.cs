﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// A value representing the completeness of a single group of item.
    /// </summary>
    public class GroupedCompletionChartDataPoint:DomainObject
    {
        /// <summary>
        /// The ID of the GroupedCompetionChart that owns this Data Point.
        /// </summary>
        public virtual long GroupedCompletionChartID { get; set; }

        /// <summary>
        /// The string value that uniquely identifies this group in the database.
        /// </summary>
        public string GroupKey { get; set; }

        /// <summary>
        /// The Name to apply to this group.
        /// </summary>
        public string GroupTitle { get; set; }

        /// <summary>
        /// A decimal value representing how complete the items in this group actually are.
        /// </summary>
        public decimal PercentComplete { get; set; }

        /// <summary>
        /// Man-Hours shown by the completion of Task Progress items in this group.
        /// </summary>
        public decimal CompletedManHours { get; set; }

        /// <summary>
        /// Man-Hours on all Task Progress items in this group.
        /// </summary>
        public decimal TotalManHours { get; set; }
        
        /// <summary>
        /// An integer representing the number of items in this group.
        /// </summary>
        public int ItemCount { get; set; }
        
        /// <summary>
        /// The GroupedCompetionChart that owns this Data Point.
        /// </summary>
        public virtual GroupedCompletionChart GroupedCompletionChart { get; set; }
        
        /// <summary>
        /// The name to display for this item.
        /// </summary>
        public override string DisplayName
        {
            get { return GroupTitle; }
        }
    }
}
