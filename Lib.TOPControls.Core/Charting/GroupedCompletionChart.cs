﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// The data showing the current completion state for each of a specified grouping of items.
    /// </summary>
    public class GroupedCompletionChart:DomainJobObject
    {
        /// <summary>
        /// The ID of the ChartDefinition that owns this instance.
        /// </summary>
        public virtual long ChartDefinitionID { get; set; }

        /// <summary>
        /// The Date this data was last updated.
        /// </summary>
        public DateTime RunDate { get; set; }

        /// <summary>
        /// The ChartDefinition that owns this instance.
        /// </summary>
        public virtual ChartDefinition ChartDefinition { get; set; }

        /// <summary>
        /// A Collection of data points for this chart.
        /// </summary>
        public virtual ICollection<GroupedCompletionChartDataPoint> GroupedCompletionChartDataPoints { get; set; }

        /// <summary>
        /// The name to display for this item.
        /// </summary>
        public override string DisplayName
        {
            get { return (ChartDefinition != null? ChartDefinition.DisplayName : RunDate.ToString()); }
        }
    }
}
