using System.Collections.Generic;
using System.Linq;
using Envoc.Core.Shared.Extensions;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// A node used to display information about the progress of a ProjectSystem
    /// </summary>
    public class ProjectSystemProgress
    {
        private decimal percentComplete;
        private decimal completedManhours;
        private decimal totalManhours;
        private List<ProjectSystemProgress> children;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectSystemProgress"/> class.
        /// </summary>
        public ProjectSystemProgress()
        {
            children = new List<ProjectSystemProgress>();
        }

        public long ID { get; set; }

        public string Name { get; set; }

        public ProjectSystemProgress Parent { get; set; }

        public decimal PercentComplete
        {
            get
            {
                if (HasChildren())
                {
                    return Children.Average(x => x.PercentComplete);
                }

                return this.percentComplete;
            }
            set { this.percentComplete = value; }
        }

        public decimal CompletedManhours
        {
            get
            {
                if (HasChildren())
                {
                    return Children.Sum(x => x.CompletedManhours);
                }

                return this.completedManhours;
            }
            set { this.completedManhours = value; }
        }

        public decimal TotalManhours
        {
            get
            {
                if (HasChildren())
                {
                    return Children.Sum(x => x.TotalManhours);
                }

                return this.totalManhours;
            }
            set { this.totalManhours = value; }
        }

        public bool HasChildren()
        {
            return Children.IsNotNull() && Children.Any();
        }

        public IEnumerable<ProjectSystemProgress> Children
        {
            get { return children; }
            set { children = value.ToList(); }
        }

        public string Path
        {
            get
            {
                if (Parent.IsNull())
                {
                    return ID.ToString();
                }
                return Parent.Path + "_" + ID;
            }
        }

        public void AddChild(ProjectSystemProgress child)
        {
            child.Parent = this;
            children.Add(child);
        }

        public void AddChildren(IEnumerable<ProjectSystemProgress> childrenToAdd)
        {
            foreach (var child in childrenToAdd)
            {
                AddChild(child);
            }
        }

        public bool IsSystem { get; set; }

        public string Comments { get; set; }
    }
}