﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Charting
{
    public class RawHistoryDataPoint
    {

        /// <summary>
        /// A date representing the point of time being reported.
        /// </summary>
        public DateTime WorkDate { get; set; }

        /// <summary>
        /// A decimal value indicating a completion state on the Work Date.
        /// </summary>
        public decimal PercentComplete { get; set; }

        /// <summary>
        /// Man-Hours shown by the completion of Task Progress items on the Work Date in this group.
        /// </summary>
        public decimal CompletedManHours { get; set; }

        /// <summary>
        /// Man-Hours on all Task Progress items on the Work Date in this group.
        /// </summary>
        public decimal TotalManHours { get; set; }


    }
}
