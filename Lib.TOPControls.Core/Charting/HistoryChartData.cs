﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// Represents the returned data required to render the History Chart for a specific value within the
    /// list of Grouping Values.
    /// </summary>
    public class HistoryChartData:DomainJobObject
    {
        /// <summary>
        /// The ID of the ChartDefinition that this HistoryChartData belongs to.
        /// </summary>
        public virtual long ChartDefinitionID { get; set; }

        /// <summary>
        /// The actual ID value of the group represented by this History Chart Data.  Represented as string because
        /// some key values are numeric and some are string values.
        /// </summary>
        public virtual string GroupKey { get; set; }

        /// <summary>
        /// Title of the grouped value being displayed in the chart.
        /// </summary>
        public virtual string GroupTitle { get; set; }

        /// <summary>
        /// Title of any parent group the current grouped value belongs to.
        /// </summary>
        public virtual string ContextTitle { get; set; }

        /// <summary>
        /// Gets/Sets the date and time that the Chart is being run.
        /// </summary>
        public virtual DateTime RunDate { get; set; }

        /// <summary>
        /// Gets/Sets the number of items within the targeted group.
        /// </summary>
        public virtual int ItemCount { get; set; }

        /// <summary>
        /// Gets/Sets the number of completed items within the targeted group.
        /// </summary>
        public virtual int CompleteCount { get; set; }

        /// <summary>
        /// Gets/Sets the decimal value that represents the percentage of overall 
        /// completion across all items in the list.
        /// </summary>
        public virtual decimal PercentComplete { get; set; }

        /// <summary>
        /// Man-Hours shown by the completion of Task Progress items in this group.
        /// </summary>
        public virtual decimal CompletedManHours { get; set; }

        /// <summary>
        /// Man-Hours on all Task Progress items in this group.
        /// </summary>
        public virtual decimal TotalManHours { get; set; }

        /// <summary>
        /// A list of percentage values representing the status of the current grouping
        /// over the course of the project.
        /// </summary>
        public virtual List<KeyValuePair<DateTime, decimal>> ChartValues { get; set; }

        /// <summary>
        /// The ChartDefinition referenced by ChartDefinitionID.
        /// </summary>
        public virtual ChartDefinition ChartDefinition { get; set; }

        /// <summary>
        /// All Data Points making up this HistoryChartData.
        /// </summary>
        public virtual ICollection<HistoryChartDataPoint> HistoryChartDataPoints { get; set; }

        /// <summary>
        /// The Name to Display for this Instance.
        /// </summary>
        public override string DisplayName
        {
            get { return GroupTitle; }
        }
    }
}
