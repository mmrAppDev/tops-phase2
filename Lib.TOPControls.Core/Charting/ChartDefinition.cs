﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// Defines the essential nature of a chart offered by the application.
    /// </summary>
    public class ChartDefinition:DomainObject
    {
        /// <summary>
        /// An Integer value associated with the ChartType enum value for this instance.
        /// </summary>
        public int ChartTypeValue { get; set; }

        /// <summary>
        /// The ChartType for this ChartDefinition.
        /// </summary>
        public ChartType ChartType
        {
            get
            {
                return (ChartType)ChartTypeValue;
            }
            set
            {
                ChartTypeValue = (int)value;
            }
        }

        /// <summary>
        /// If true, this chart is made available to all jobs in the system.
        /// </summary>
        public bool IsGlobal { get; set; }

        /// <summary>
        /// An Integer value associated with the FilterValueType enum value for this instance.
        /// </summary>
        public int FilterValueTypeValue { get; set; }

        /// <summary>
        /// The FilterValueType that provides the primary grouping for this chart.
        /// </summary>
        public FilterValueType FilterValueType
        {
            get { return (FilterValueType)FilterValueTypeValue; }
            set { FilterValueTypeValue = (int)value; }
        }


        /// <summary>
        /// The Display Name for this item.
        /// </summary>
        public override string DisplayName
        {
            get
            {
                return string.Format("{0} on {1} ({2})", ChartType.ToString(),
                    FilterValueType.ToString(),
                    (IsGlobal ? "global" : "job specific"));
            }
        }
    }
}
