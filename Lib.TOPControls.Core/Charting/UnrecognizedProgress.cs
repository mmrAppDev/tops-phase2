﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// An atomic value represeting a Job and WorkDate that need a ProgressHistory update.
    /// </summary>
    public class UnrecognizedProgress
    {
        /// <summary>
        /// The ID of the Job
        /// </summary>
        public long JobID { get; set; }

        /// <summary>
        /// The Date of the work that was entered.
        /// </summary>
        public DateTime WorkDate { get; set; }

        /// <summary>
        /// The Number of rows of data that currently show as having been entered later than the most recent
        /// ProgressHistory for this job and work date.
        /// </summary>
        public int RowsNeeded { get; set; }
    }
}
