﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// Data representing the progress state of the grouped data on a particular date. 
    /// </summary>
    public class HistoryChartDataPoint:DomainObject
    {
        /// <summary>
        /// The ID of the HistoryChartData item that owns this Data Point.
        /// </summary>
        public virtual long HistoryChartDataID { get; set; }

        /// <summary>
        /// A date representing the point of time being reported.
        /// </summary>
        public DateTime WorkDate { get; set; }

        /// <summary>
        /// A decimal value indicating a completion state on the Work Date.
        /// </summary>
        public decimal PercentComplete { get; set; }

        /// <summary>
        /// Man-Hours shown by the completion of Task Progress items on the Work Date in this group.
        /// </summary>
        public decimal CompletedManHours { get; set; }

        /// <summary>
        /// Man-Hours on all Task Progress items on the Work Date in this group.
        /// </summary>
        public decimal TotalManHours { get; set; }

        /// <summary>
        /// The HistoryChartData item that owns this data point.
        /// </summary>
        public virtual HistoryChartData HistoryChartData { get; set; }
        
        /// <summary>
        /// The Display name of this instance.
        /// </summary>
        public override string DisplayName
        {
            get { return WorkDate.ToString(); }
        }
    }
}
