﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// Represents the state of a TaskProgress on a specific date.
    /// </summary>
    public class ProgressHistory:DomainObject
    {

        /// <summary>
        /// Gets the ID of the JobInfo this Progress History item is associated with.
        /// </summary>
        public long JobID { get; set; }

        /// <summary>
        /// Gets the ID of the TaskProgress item this completion value represents.
        /// </summary>
        public virtual long TaskProgressID { get; set; }
        
        /// <summary>
        /// Gets the Date this completion value represents.
        /// </summary>
        public DateTime WorkDate { get; set; }
        
        /// <summary>
        /// Gets the Percent Complete To Date for this item.
        /// </summary>
        public decimal PCTD { get; set; }

        /// <summary>
        /// Gets the ManHours associated with this item on this date.
        /// </summary>
        public decimal ManHours { get; set; }

        /// <summary>
        /// A string that can represent this item to users.
        /// </summary>
        public override string DisplayName
        {
            get { return TaskProgressID.ToString() + " on " + WorkDate.ToString() + " = " + PCTD.ToString(); }
        }


        /// <summary>
        /// Gets the TaskProgess Instance this item references.
        /// </summary>
        public virtual TaskProgress TaskProgress { get; set; }
    }
}
