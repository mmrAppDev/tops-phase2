﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides the contracts for all objects that will use SoftDelete functionality.
    /// </summary>
    public interface ISoftDeleted
    {

        /// <summary>
        /// A boolean value that indicates whether the underlying database record has been marked as deleted.
        /// </summary>
        bool IsDeleted { get; }
        
        /// <summary>
        /// A method that changes the deletion state of the item.
        /// </summary>
        void SetDeletedState(bool isDeleted);
    }
}
