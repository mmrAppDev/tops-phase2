﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls
{
    /// <summary>
    /// Defines the display information for one particular type of filter value.
    /// </summary>
    public class FilterConfiguration:DomainJobObject, IDisplayOrdered
    {

        /// <summary>
        /// The integer value for the FilterValueType that is persisted to the database.
        /// </summary>
        public int FilterType { get; set; }

        /// <summary>
        /// A value from the FilterValueType enumeration that identifies the column/type being configured.
        /// </summary>
        public FilterValueType FilterValueType
        {
            get { return (FilterValueType)FilterType; }
            set { FilterType = (int)value; }
        }


        /// <summary>
        /// An integer that indicates the order in which to display this value.
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// A boolean indicating whether this filter type should appear in the filter tool.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// A value that indicates whether this filter generally represents a large amount of data, 
        /// so that alternate UI choices can be made when possible.
        /// </summary>
        public bool IsHighVolume { get; set; }


        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return FilterValueType.ToString(); }
        }
    }
}
