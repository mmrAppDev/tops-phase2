﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lib.TOPControls
{
    /// <summary>
    /// Connects a Task to a TrackedItem and represents the current completion state of that relationship.
    /// </summary>
    public class TaskProgress : DomainJobObject, ISoftDeleted
    {
        /// <summary>
        /// The ID of the ProjectPhase this item belongs to.
        /// </summary>
        public virtual long TaskID { get; set; }

        /// <summary>
        /// If present, the ID of the Commodity this item belongs to.
        /// </summary>
        public virtual long? CommodityID { get; set; }

        /// <summary>
        /// The ID of the TrackedItem this item belongs to.
        /// </summary>
        public virtual long TrackedItemID { get; set; }

        /// <summary>
        /// This stores the current percent complete value.
        /// </summary>
        protected decimal? _percentComplete = null;

        /// <summary>
        /// A decimal value indicating the percentage completion state of this item.
        /// </summary>
        public virtual decimal PercentComplete
        {
            get
            {
                if (!_percentComplete.HasValue) SetPercentComplete(0m);

                return _percentComplete.Value;
            }

            internal set { SetPercentComplete(value); }
        }

        /// <summary>
        /// The ProjectPhase this item belongs to.
        /// </summary>
        public virtual Task Task { get; set; }

        /// <summary>
        /// The Commodity this item belongs to.
        /// </summary>
        public virtual Commodity Commodity { get; set; }

        /// <summary>
        /// The TrackedItem this item belongs to.
        /// </summary>
        public virtual TrackedItem TrackedItem { get; set; }

        /// <summary>
        /// The number Man-Hours to apply to this item, according to the associated Commodity.
        /// </summary>
        public decimal ManHoursPerItem { get; set; }

        /// <summary>
        /// A boolean value that is true if this item, it's associated Tracked Item, or the Task it belongs to is marked as Deleted.
        /// </summary>
        public bool IsInDeletedScope { get; set; }

        /// <summary>
        /// The Progress Log Entries for this item.
        /// </summary>
        public virtual ICollection<ProgressLogEntry> ProgressLogEntries { get; set; }

        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return string.Format("{0} ::: {1}", TrackedItem.TrackingID, Task.Title); }
        }

        /// <summary>
        /// Sets the PercentComplete value.  If that value has not yet been set, also sets the InitialPercentComplete to 
        /// the same value.
        /// </summary>
        /// <param name="value">A decimal value to assign.</param>
        public void SetPercentComplete(decimal value)
        {
            if (!_percentComplete.HasValue)
            {
                _initialPercentComplete = value;
            }
            _percentComplete = value;
        }

        private decimal _initialPercentComplete = 0;

        /// <summary>
        /// Gets/Sets the first PercentComplete value set.
        /// </summary>
        internal decimal InitialPercentComplete
        {
            get { return _initialPercentComplete; }
            set { _initialPercentComplete = value; }
        }

        /// <summary>
        /// Returns the first percent complete value set on this item, or the default value of zero.
        /// </summary>
        /// <returns></returns>
        public decimal GetInitialCompletionPercentage()
        {
            return _initialPercentComplete;
        }

        /// <summary>
        /// Retrieves the percent complete as of the target date.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public decimal PercentCompleteAsOf(DateTime target)
        {
            var result = PercentComplete;
            if (ProgressLogEntries != null)
            {
                var prev = from ple in ProgressLogEntries
                    where ple.WorkDate <= target && ple.NewPercentComplete.HasValue
                    orderby ple.WorkDate descending, ple.EntryDate descending
                    select ple;
                var last = prev.FirstOrDefault();
                if (last != null)
                {
                    result = last.NewPercentComplete.Value;
                }
                else
                {
                    var futurePLE = from ple in ProgressLogEntries
                        where ple.WorkDate > target && ple.OldPercentComplete.HasValue
                        orderby ple.WorkDate, ple.EntryDate
                        select ple;
                    var first = futurePLE.FirstOrDefault();
                    if (first != null)
                    {
                        result = first.OldPercentComplete.Value;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Returns a bool that is true if the current percent complete is different than the last known
        /// datavalue for the percent complete.
        /// </summary>
        /// <returns></returns>
        public bool IsProgressChanged()
        {
            return (InitialPercentComplete != PercentComplete);
        }

        /// <summary>
        /// Modifies the initial percent complete on the instance passed so that it matches the current percent complete.
        /// </summary>
        /// <param name="item">A TaskProgress item that has been loaded from the database.</param>
        public static void _ResetInitialPercentComplete(TaskProgress item)
        {
            item.InitialPercentComplete = item.PercentComplete;
        }

        /// <summary>
        /// Returns a CompletionState value indicating the current state of this TaskProgress item.
        /// </summary>
        /// <returns></returns>
        public virtual CompletionState GetCompletionState()
        {
            if (PercentComplete == 0.00m)
            {
                return CompletionState.NotStarted;
            }
            else if (PercentComplete >= 1.00m)
            {
                return CompletionState.Complete;
            }
            return CompletionState.PartiallyComplete;
        }

        #region ISoftDeleted Members

        /// <summary>
        /// A boolean value that indicates whether the underlying database record has been marked as deleted.
        /// </summary>
        public bool IsDeleted { get; private set; }

        /// <summary>
        /// Sets the status of the IsDeleted property of this instance.
        /// </summary>
        /// <param name="isDeleted">The true or false value to set on the IsDeleted property.</param>
        public void SetDeletedState(bool isDeleted)
        {
            IsDeleted = isDeleted;
        }

        #endregion
    }
}