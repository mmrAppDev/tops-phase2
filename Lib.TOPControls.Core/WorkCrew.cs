﻿using System.Collections.Generic;

namespace Lib.TOPControls
{
    /// <summary>
    /// A class representing information stored to the database for a specific work crew.
    /// </summary>
    public class WorkCrew : DomainJobObject
    {
        /// <summary>
        /// A whole-number value that can be used to identify this work crew within the job.
        /// </summary>
        public int CrewNumber { get; set; }

        /// <summary>
        /// A short text description that can help identify the work crew.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return string.Format("{0} - {1}", CrewNumber, Description); }
        }

        /// <summary>
        /// The Progres Log Entries entered for this work crew.
        /// </summary>
        public virtual ICollection<ProgressLogEntry> ProgressLogEntries { get; set; }
    }
}