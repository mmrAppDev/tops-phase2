﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Lib.TOPControls
{
    /// <summary>
    /// Represents information about a user in the application.
    /// </summary>
    public class TOPUser:DomainObject, ISoftDeleted
    {
        


        /// <summary>
        /// The login name used by this user.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// The First Name used by this user.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// The Last Name used by this user.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// An xml value representing a set of preferences for this user when interacting with
        /// the User Interface.
        /// </summary>
        public string Preferences { get; set; }

        /// <summary>
        /// The ID of the job the user should be placed in upon logging in.
        /// </summary>
        public long? DefaultJobID { get; set; }


        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return string.Format("{0} {1}", FirstName, LastName); }
        }

        #region ISoftDeleted Members

        /// <summary>
        /// A boolean value that indicates whether the underlying database record has been marked as deleted.
        /// </summary>
        public bool IsDeleted { get; private set; }

        /// <summary>
        /// Sets the status of the IsDeleted property of this instance.
        /// </summary>
        /// <param name="isDeleted">The true or false value to set on the IsDeleted property.</param>
        public void SetDeletedState(bool isDeleted)
        {
            IsDeleted = isDeleted;
        }

        #endregion

        /// <summary>
        /// The Progress Log Entries by this user.
        /// </summary>
        public virtual ICollection<ProgressLogEntry> ProgressLogEntries { get; set; }

        /// <summary>
        /// UserJob records for this user.
        /// </summary>
        public virtual ICollection<UserJob> UserJobs { get; set; }





    }



}
