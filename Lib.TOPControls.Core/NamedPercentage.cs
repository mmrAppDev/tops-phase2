﻿namespace Lib.TOPControls
{
    /// <summary>
    /// Connects a name or title with a percent complete.
    /// </summary>
    public class NamedPercentage
    {
        /// <summary>
        /// The displayed name or title
        /// </summary>
        public string NameOrTitle { get; set; }

        /// <summary>
        /// The completion percentage as a raw decimal
        /// </summary>
        public decimal PercentComplete { get; set; }

        /// <summary>
        /// Man-Hours shown by the completion of Task Progress items in this group.
        /// </summary>
        public decimal CompletedManHours { get; set; }

        /// <summary>
        /// Man-Hours on all Task Progress items in this group.
        /// </summary>
        public decimal TotalManHours { get; set; }
    }
}