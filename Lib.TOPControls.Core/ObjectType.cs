﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls
{
    /// <summary>
    /// Information indentifying a category of Tracked Item
    /// </summary>
    public class ObjectType:DomainJobObject
    {

        /// <summary>
        /// The name of this ObjectType.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A brief description of this ObjectType.
        /// </summary>
        public string Description { get; set; }


        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return Name; }
        }


        #region Constructors

        /// <summary>
        /// Creates an ObjectType object.  The irony of it all.
        /// </summary>
        public ObjectType()
        {
            Name = string.Empty;
            Description = string.Empty;
        }
        #endregion


    }
}
