﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls
{
    /// <summary>
    /// Represents an activity that needs to be completed within the Phase.
    /// </summary>
    public class Task:DomainJobObject, ISoftDeleted, IDisplayOrdered
    {
        /// <summary>
        /// The Title to use for this Task.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The ID of the Project Phase to which this Task belongs.
        /// </summary>
        public virtual long PhaseID { get; set; }

        /// <summary>
        /// Represents the relative position of this Task within the tasks for this phase. Must be an integer that is greater than zero.
        /// </summary>
        public int DisplayOrder { get; set; }
        
        /// <summary>
        /// If present, the ID of the Commodity to be applied by default to all TaskProgress items belonging to this task.
        /// </summary>
        public virtual long? DefaultCommodityID { get; set; }
        
        /// <summary>
        /// A boolean value that indicates whether the underlying database record has been marked as deleted.
        /// </summary>
        public bool IsDeleted { get; private set; }
        
        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return Title; }
        }

        /// <summary>
        /// The Project Phase to which this task belongs.
        /// </summary>
        public virtual ProjectPhase ProjectPhase { get; set; }

        /// <summary>
        /// The Commodity to applied by default to all TaskProgress Items.
        /// </summary>
        public virtual Commodity DefaultCommodity { get; set; }

        /// <summary>
        /// Sets the status of the IsDeleted property of this instance.
        /// </summary>
        /// <param name="isDeleted">The true or false value to set on the IsDeleted property.</param>
        public void SetDeletedState(bool isDeleted)
        {
            IsDeleted = isDeleted;
        }
    }
}
