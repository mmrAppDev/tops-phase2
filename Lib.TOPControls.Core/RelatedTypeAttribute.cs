﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Lib.TOPControls
{
    /// <summary>
    /// Indicates the Type this item is connected to.
    /// </summary>
    public class RelatedTypeAttribute:Attribute 
    {
        
        private Type _targetType;

        /// <summary>
        /// The System.Type of the values that this item will use.
        /// </summary>
        public Type TargetType { get { return _targetType; } private set { _targetType = value; } }

        /// <summary>
        /// Defines the System.Type connected to this item.
        /// </summary>
        /// <param name="targetType">A System.Type that will identify the values that this item will affect.</param>
        public RelatedTypeAttribute(Type targetType)
            : base()
        {
            TargetType = targetType;
        }
    }
}
