﻿using System.Collections.Generic;

namespace Lib.TOPControls
{
    /// <summary>
    /// Information related to a specific job in the TOPControls application.
    /// </summary>
    public class JobInfo : DomainObject, ISoftDeleted
    {
        /// <summary>
        /// The Title to use for this job.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The name of User Defined Property 1 for this job.  This should be a user-friendly name that can be
        /// displayed in the user interface.
        /// </summary>
        public string UDPName1 { get; set; }

        /// <summary>
        /// The name of User Defined Property 2 for this job.  This should be a user-friendly name that can be
        /// displayed in the user interface.
        /// </summary>
        public string UDPName2 { get; set; }

        /// <summary>
        /// The name of User Defined Property 3 for this job.  This should be a user-friendly name that can be
        /// displayed in the user interface.
        /// </summary>
        public string UDPName3 { get; set; }

        /// <summary>
        /// True if the Client is allowed to view Earned Man-Hours for this job.
        /// </summary>
        public bool ShowEarnedToClient { get; set; }

        /// <summary>
        /// True if this job should calculate progress percentages based on the Man-Hours provided by the associated Commodities.
        /// </summary>
        public bool WeightedCalculations { get; set; }

        /// <summary>
        /// True if this job should calculate historical completions based on the number of items or man-hours in the 
        /// project on that work day.  Otherwise, historical completions will be calculated against the current overall scope for the grouping.
        /// </summary>
        public bool HistoricalScopeCurve { get; set; }

        /// <summary>
        /// A boolean value that indicates whether the underlying database record has been marked as deleted.
        /// </summary>
        public bool IsDeleted { get; private set; }

        /// <summary>
        /// Creates a new instance of the JobInfo class.
        /// </summary>
        public JobInfo()
        {
            setDefaultValues();
        }

        /// <summary>
        /// Sets the default values for a JobInfo instance.
        /// </summary>
        protected virtual void setDefaultValues()
        {
            UDPName1 = null;
            UDPName2 = null;
            UDPName3 = null;
            ShowEarnedToClient = false;
            WeightedCalculations = false;
            HistoricalScopeCurve = true;
        }

        /// <summary>
        /// Modifies this job instance to be deleted.  NOTE: To save an item that is deleted it must be
        /// </summary>
        /// <param name="isDeleted"></param>
        public void SetDeletedState(bool isDeleted)
        {
            IsDeleted = isDeleted;
        }


        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return Title; }
        }

        /// <summary>
        /// The set of all ProjectPhase items related to this job.
        /// </summary>
        public virtual ICollection<ProjectPhase> ProjectPhases { get; set; }

        /// <summary>
        /// The set of all WorkCrew items related to this job.
        /// </summary>
        public virtual ICollection<WorkCrew> WorkCrews { get; set; }

        /// <summary>
        /// The set of all ProjectSystem items related to this job.
        /// </summary>
        public virtual ICollection<ProjectSystem> ProjectSystems { get; set; }

        /// <summary>
        /// The set of all ProjectArea items related to this job.
        /// </summary>
        public virtual ICollection<ProjectArea> ProjectAreas { get; set; }

        /// <summary>
        /// A collection of CraftInfo items belonging to this job.
        /// </summary>
        public virtual ICollection<CraftInfo> Crafts { get; set; }

        /// <summary>
        /// A collection of Commodity items belonging to this job.
        /// </summary>
        public virtual ICollection<Commodity> Commodities { get; set; }

        /// <summary>
        /// A collection of all ObjectTypes belonging to this job.
        /// </summary>
        public virtual ICollection<ObjectType> ObjectTypes { get; set; }

        /// <summary>
        /// A collection of all FilterConfiguration items belonging to this job.
        /// </summary>
        public virtual ICollection<FilterConfiguration> FilterConfigurations { get; set; }


        /// <summary>
        /// A collection of objects linking this job to its related users.
        /// </summary>
        public virtual ICollection<UserJob> UserJobs { get; set; }
    }
}