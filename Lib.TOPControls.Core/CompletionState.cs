﻿

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides a way of selecting a simple completion state range for filtering items 
    /// in the User interface.
    /// </summary>
    public enum CompletionState
    {
        /// <summary>
        /// There has been no progress logged for this item.
        /// </summary>
        NotStarted,
        /// <summary>
        /// Some progress has been logged, but it is not yet complete.
        /// </summary>
        PartiallyComplete,
        /// <summary>
        /// The Task is complete for this TrackedItem.
        /// </summary>
        Complete
    }
}