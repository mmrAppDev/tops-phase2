﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Interfaces
{
    /// <summary>
    /// Provides the contracts for extending the operations of IObjectLibrary to allow management of items
    /// of the DomainJobObject type.
    /// </summary>
    /// <typeparam name="JObType">A System.Type that is a subclass of DomainJobObject and has a parameterless constructor.</typeparam>
    public interface IJobObjectLibrary<JObType>:IObjectLibrary<JObType>
        where JObType:DomainJobObject, new()
    {
        /// <summary>
        /// Gets all instances of this object type for the jobID specified.
        /// </summary>
        /// <returns></returns>
        IEnumerable<JObType> AllForJob(long jobID);


        /// <summary>
        /// Returns an instance of the target type if there is a valid item with a primary descriptive string matching
        /// the name passed, unless the item is marked deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job the object should belong to.</param>
        /// <param name="name">The full name string to search for.</param>
        /// <returns></returns>
        JObType FindByName(long jobID, string name);
    }
}
