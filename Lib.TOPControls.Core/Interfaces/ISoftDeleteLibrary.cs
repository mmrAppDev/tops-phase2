﻿namespace Lib.TOPControls.Interfaces
{
    /// <summary>
    /// Provides the contracts for extending the operations of IObjectLibrary to allow management of items
    /// that have been removed by a Soft Delete.
    /// </summary>
    /// <typeparam name="T">The object type to be managed.</typeparam>
    public interface ISoftDeleteLibrary<T> : IObjectLibrary<T> where T : DomainObject, ISoftDeleted
    {
        /// <summary>
        /// Returns any item in the database that matches the ID requested, even if that item is marked as deleted.
        /// </summary>
        /// <param name="id">The ID value to find.</param>
        /// <param name="userID">The ID of a valid TOPUser item. If the user does not have the right to view deleted items
        /// of this type, returns a NULL.</param>
        /// <returns></returns>
        T FindAny(long id, long userID);

        /// <summary>
        /// Restores an item that has been marked as deleted, if it has not been destroyed.
        /// </summary>
        /// <param name="id">The ID value of the item to Undelete.</param>
        /// <param name="userID">The ID of a valid TOPUser item. If the user does not have the right to restore deleted items
        /// of this type, the operation will fail.</param>
        /// <returns>The undeleted instance of this item.</returns>
        T Undelete(long id, long userID);

        /// <summary>
        /// Completely removes the database record of an item that is marked as deleted.
        /// </summary>
        /// <param name="id">The ID of the item to remove.  The item must aready be marked as deleted.  To destroy active
        /// items, the Delete operation must first by performed.</param>
        /// <param name="userID">The ID of a valid TOPUser item. If the user does not have the right to destroy deleted items
        /// of this type, the operation will fail.</param>
        void Destroy(long id, long userID);
    }
}