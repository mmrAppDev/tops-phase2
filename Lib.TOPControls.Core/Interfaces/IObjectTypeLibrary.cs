﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides operations and object validation for instances of the ObjectType class.
    /// </summary>
    public interface IObjectTypeLibrary:IJobObjectLibrary<ObjectType>
    {
        
    }
}
