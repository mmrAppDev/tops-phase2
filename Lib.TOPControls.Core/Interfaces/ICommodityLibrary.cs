﻿using System.Collections.Generic;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides operations and object validation for instances of the Commodity class.
    /// </summary>
    public interface ICommodityLibrary : IJobObjectLibrary<Commodity>
    {
        /// <summary>
        /// Returns a set of Commodity objects within the Job indicated by jobID that match the
        /// searchTerm passed. Does not return Commodities from deleted jobs.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">A string value to search for.  If the searchTerm is an
        /// empty string, returns all Commodity records within the job. </param>
        /// <returns></returns>
        IEnumerable<Commodity> Search(long jobID, string searchTerm);

        /// <summary>
        /// Returns Commodity object within the with a CommodityCode value that matches the string
        /// value passed.
        /// </summary>
        /// <param name="jobID">The ID of the Job the Commodity Code belongs to.</param>
        /// <param name="comCode">A string value representing the Commodity Code.</param>
        /// <returns></returns>
        Commodity FindByCommodityCode(long jobID, string comCode);

        /// <summary>
        /// Returns a Nullable decimal value indicating the number of ManHours of estimated work
        /// each TaskProgress item with the passed Commodity ID represents.  If there is no estimated value for the
        /// commodity, or if there are no items in the system for that commodity, returns a null.
        /// </summary>
        /// <param name="commodityID">The ID of the commodity that is being checked.  Call will fail if this ID is
        /// zero or lower, or if no commodity exists with that ID.</param>
        /// <returns></returns>
        decimal? GetManHoursPerItem(long commodityID);
    }
}