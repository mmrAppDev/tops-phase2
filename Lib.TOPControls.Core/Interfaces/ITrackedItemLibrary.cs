﻿using System.Collections.Generic;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides most operations and object validation for instances of the TrackedItem class.
    /// </summary>
    public interface ITrackedItemLibrary : ISoftDeleteLibrary<TrackedItem>, IJobObjectLibrary<TrackedItem>
    {
        /// <summary>
        /// Returns a set of TrackedItem objects within the Job indicated by jobID that match the 
        /// searchTerm passed.  Does not return TrackedItems for deleted jobs, or any TrackedItems that 
        /// are marked as deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">A string value to search for.  If the searchTerm is an empty string, returns all TrackedItems in the job.</param>
        /// <returns></returns>
        IEnumerable<TrackedItem> Search(long jobID, string searchTerm);

        /// <summary>
        /// Returns a set of TrackedItem objects within the Job indicated by jobID that match the 
        /// searchTerm passed.  Returns all items matching items, even if the Job is marked as deleted 
        /// or the TrackedItem itself is marked as deleted.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">A string value to search for.  If the searchTerm is an empty string, returns all TrackedItems in the job.</param>
        /// <param name="userID">The ID of the TOPUser performing the operation.</param>
        /// <returns></returns>
        IEnumerable<TrackedItem> SearchAny(long jobID, string searchTerm, long userID);

        /// <summary>
        /// Returns a TrackedItem with a TrackingID that is a match for the trackingID passed. Returns a null if a matching value does not exist.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="trackingID">A string value to compare to the TrackingID of all Tracked Items within the job.</param>
        /// <returns></returns>
        TrackedItem FindByTrackingID(long jobID, string trackingID);

        /// <summary>
        /// Returns a set of TrackedItem objects within the Job indicated by jobID that match the 
        /// systemId passed.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="systemID">The ID of the system the tracked item belongs to.</param>
        /// <returns></returns>
        IEnumerable<TrackedItem> FindBySystemID(long jobID, long systemID);
    }
}