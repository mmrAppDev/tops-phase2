﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.Interfaces;
using MMRCommon.Data;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// Provides operations for GroupedCompletionCharts
    /// </summary>
    public interface IGroupedCompletionChartLibrary:IJobObjectLibrary<GroupedCompletionChart>
    {
        /// <summary>
        /// Retrieves a set of stored grouped values by the same means that would be used to generate the same set of data.
        /// </summary>
        /// <param name="jobID">ID of the Job to use.</param>
        /// <param name="grouping">An enumeration member identifying the value to group on.</param>
        /// <returns></returns>
        GroupedCompletionChart GetByDefinition(long jobID, FilterValueType grouping);

        /// <summary>
        /// Retrieves all GroupedCompletionChartDataPoint items related to the Grouped Completion Chart indicated by the ID.
        /// </summary>
        /// <param name="groupedCompletionChartID">The ID of the Grouped Completion Chart to target.</param>
        /// <returns></returns>
        IEnumerable<GroupedCompletionChartDataPoint> GetDataPoints(long groupedCompletionChartID);

        /// <summary>
        /// Stores the object passed.
        /// </summary>
        /// <param name="item">A GroupedCompletionChartDataPoint instance.</param>
        void SaveDataPoint(GroupedCompletionChartDataPoint item);


        /// <summary>
        /// Validates the item passed.
        /// </summary>
        /// <param name="item">The GroupedCompletionChartDataPoint to validate</param>
        /// <returns></returns>
        ValidationResult ValidateDataPoint(GroupedCompletionChartDataPoint item);
        
        
    }
}
