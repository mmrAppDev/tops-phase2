﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides operations and object validation for instances of the JobInfo class.
    /// </summary>
    public interface IJobInfoLibrary : ISoftDeleteLibrary<JobInfo>
    {

        /// <summary>
        /// Returns a set of JobInfo objects that match the searchTerm passed.  If the term is empty, retuns all JobInfo
        /// records.  Will always exclude JobInfo items marked as deleted.
        /// </summary>
        /// <param name="searchTerm">A string representing the text to search for.</param>
        /// <returns></returns>
        IEnumerable<JobInfo> Search(string searchTerm);

        /// <summary>
        /// Returns a set of JobInfo objects that match the searchTerm passed, including items marked as deleted.
        /// </summary>
        /// <param name="searchTerm">A string representing the text to search for.  Passing an empty string will
        /// cause all JobInfo items to be returned.</param>
        /// <param name="userID">The ID of a valid TOPUser.  If the indicated user does not have the rights to 
        /// view deleted jobs, the action will fail.</param>
        /// <returns></returns>
        IEnumerable<JobInfo> SearchAny(string searchTerm, long userID);

        /// <summary>
        /// Returns a dictionary of the User-Defined Properties for the Job.  This dictionary key is the numeral
        /// index of the property (from 1 to 3) and the value is a string representing the name of the User Defined Property.
        /// </summary>
        /// <param name="jobID">The ID of the JobInfo item.</param>
        /// <returns></returns>
        Dictionary<int, string> GetUDPList(long jobID);
    }
}
