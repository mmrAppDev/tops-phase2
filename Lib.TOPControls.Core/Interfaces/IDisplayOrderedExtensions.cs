﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Interfaces
{
    /// <summary>
    /// The interface to expect on libraries that provide handling for IDisplayOrdered items.
    /// </summary>
    /// <typeparam name="DispOrdType"></typeparam>
    public interface IDisplayOrderedExtensions<DispOrdType>  where DispOrdType:IDisplayOrdered
    {



        /// <summary>
        /// Returns a set of items of this type that are all children of the item with the parentID passed.
        /// </summary>
        /// <param name="parentID">The ID of the item that the targeted IDisplayOrdered items belong to.</param>
        /// <returns></returns>
        IQueryable<DispOrdType> GetSiblings(long parentID);

        /// <summary>
        /// Returns all items of this type that are in the same list of IDisplayOrdered as the item passed.
        /// </summary>
        /// <param name="item">The source item for which the siblings are being returned.</param>
        /// <param name="excludeOriginal">A boolean that if true causes the items to be returned without the 
        /// instance of the item originally passed.</param>
        /// <returns></returns>
        IQueryable<DispOrdType> GetSiblings(DispOrdType item, bool excludeOriginal = true);

        /// <summary>
        /// Modifies the DisplayOrder property of all items of this type within the list indicated by parentID
        /// </summary>
        /// <param name="parentID">An ID representing the relevent parent to which this item belongs.</param>
        /// <param name="commitValues">A boolean that when passed as true will trigger a Save operation to commit the altered 
        /// DisplayOrder values to the permanent data.  Otherwise, the changes are left to be included in the Unit of Work that 
        /// commits next.</param>
        /// <returns></returns>
        IEnumerable<DispOrdType> RenumberItems(long parentID, bool commitValues = true);

    }
}
