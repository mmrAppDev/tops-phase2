﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides most operations and object validation for instances of the TOPUser class.
    /// </summary>
    public interface ITOPUserLibrary:ISoftDeleteLibrary<TOPUser>
    {

        /// <summary>
        /// Returns a set of TOPUser objects that match the searchTerm passed.  If the searchTerm 
        /// is an empty string, returns all TOPUser items. Does not return TOPUser items that are 
        /// marked as deleted.
        /// </summary>
        /// <param name="searchTerm">A string value to search for.</param>
        /// <returns></returns>
        IEnumerable<TOPUser> Search(string searchTerm);

        /// <summary>
        /// Performs that same operation as Search(), but includes TOPUser items that are marked as deleted.  
        /// </summary>
        /// <param name="searchTerm">A string value to search for.</param>
        /// <param name="userID">The ID of the TOPUser performing this search.  If the user referenced by 
        /// userID does not have rights to view deleted TOPUsers, the operation will fail.</param>
        /// <returns></returns>
        IEnumerable<TOPUser> SearchAny(string searchTerm, long userID);


        /// <summary>
        /// Returns a TOPUser with a Login that is a match for the username passed. Returns a null if a 
        /// matching value does not exist or if the matching item is marked as deleted.  
        /// </summary>
        /// <param name="username">A string value to match to the Login value of each record.</param>
        /// <returns></returns>
        TOPUser FindByUsername(string username);


        /// <summary>
        /// Updates a user preference value with the preferencesText passed. The TOPUser to be updated 
        /// must have an ID that matches the userID passed.
        /// </summary>
        /// <param name="userID">The ID of the TOPUser record to change.</param>
        /// <param name="preferencesText">An XML string that can replace or update the user's prefrences
        /// value.</param>
        void SetUserPreferences(long userID, string preferencesText);

        
        /// <summary>
        /// Updates the role set on the current user to the role matching the ID passed.  Replaces an 
        /// existing role if it exists, and otherwise, sets the user to have an associated role.
        /// </summary>
        /// <param name="targetUserID">The ID of the TOPUser record to which the role should be associated.</param>
        /// <param name="roleID">The ID of the Role the user should be associated with.</param>
        /// <param name="currentUserID">The ID of the user performing this operation</param>
        void SetUserRole(long targetUserID, long roleID, long currentUserID);

        /// <summary>
        /// Creates a new user with the information passed.
        /// </summary>
        /// <param name="username">The username within the system, which will serve as the login name used.</param>
        /// <param name="firstName">The first name of the user.</param>
        /// <param name="lastName">The last name of the user.</param>
        /// <param name="password">The password of the user, not encrypted.</param>
        /// <param name="passwordQuestion">A question to ask the user for a forgotten password process.</param>
        /// <param name="passwordAnswer">The expected answer to the password question.</param>
        /// <param name="currentUserID">The ID of the user performing this operation.</param>
        /// <param name="isApproved">Optional.  A boolean indicating if the user is approved to begin using the system.</param>
        /// <returns></returns>
        TOPUser AddWithCredentials(string username, string firstName, string lastName, string password, string passwordQuestion, string passwordAnswer, long currentUserID, bool isApproved = true);

        /// <summary>
        /// Changes the password in the user's credentials to the unencrypted password passed to the method.
        /// </summary>
        /// <param name="targetUserID">ID of the user whose password is being changed.</param>
        /// <param name="password">The new password value (unencrypted) </param>
        /// <param name="currentUserID">ID of the user performing this operation.</param>
        void SetUserPassword(long targetUserID, string password, long currentUserID);

        /// <summary>
        /// Checks whether the user being passed has the Role Name being checked.
        /// </summary>
        /// <param name="targetUserID">The ID of the TOPUser item to check</param>
        /// <param name="roleName">A string matching the name of the role being queried.</param>
        /// <returns></returns>
        bool IsUserInRole(long targetUserID, string roleName);


    }
}
