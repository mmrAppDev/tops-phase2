﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Lib.TOPControls.Charting;
using Lib.TOPControls.DataImport;
using Lib.TOPControls.Security;
using MMRCommon.Data;

namespace Lib.TOPControls.Interfaces.DataAccess
{
    /// <summary>
    /// Provides the set of properties and operations required of the TOPControl Data Access Layer
    /// </summary>
    public interface ITOPControlDAL
    {
        /// <summary>
        /// Gets the repository for JobInfo objects.
        /// </summary>
        IRepository<JobInfo> JobInfoRep { get; }

        /// <summary>
        /// Gets the repository for TOPUser objects.
        /// </summary>
        IRepository<TOPUser> TOPUserRep { get; }

        /// <summary>
        /// Gets the repository for ProjectPhase objects.
        /// </summary>
        IRepository<ProjectPhase> ProjectPhaseRep { get; }

        /// <summary>
        /// Gets the repository for ObjectType objects.
        /// </summary>
        IRepository<ObjectType> ObjectTypeRep { get; }

        /// <summary>
        /// Gets the repository for ProjectSystem objects.
        /// </summary>
        IRepository<ProjectSystem> ProjectSystemRep { get; }

        /// <summary>
        /// Get a tree representation of ProjectSystem objects
        /// </summary>
        /// <returns></returns>
        IEnumerable<ProjectSystem> GetProjectSystemTree(long jobId);

        /// <summary>
        /// Gets the repository for ProjectArea objects.
        /// </summary>
        IRepository<ProjectArea> ProjectAreaRep { get; }

        /// <summary>
        /// Gets the repository for CraftInfo objects.
        /// </summary>
        IRepository<CraftInfo> CraftInfoRep { get; }

        /// <summary>
        /// Gets the repository for Commodity objects.
        /// </summary>
        IRepository<Commodity> CommodityRep { get; }

        /// <summary>
        /// Gets the repository for Task objects.
        /// </summary>
        IRepository<Task> TaskRep { get; }

        /// <summary>
        /// Gets the repository for TrackedItem objects.
        /// </summary>
        IRepository<TrackedItem> TrackedItemRep { get; }

        /// <summary>
        /// Gets the repository for TaskProgress objects.
        /// </summary>
        IRepository<TaskProgress> TaskProgressRep { get; }

        /// <summary>
        /// Gets the repository for FilterConfiguraton objects.
        /// </summary>
        IRepository<FilterConfiguration> FilterConfigurationRep { get; }

        /// <summary>
        /// Gets the repository for UserJob objects.
        /// </summary>
        IRepository<UserJob> UserJobRep { get; }

        /// <summary>
        /// Gets the repository for Security.Credential objects
        /// </summary>
        IRepository<Credential> CredentialRep { get; }

        /// <summary>
        /// Gets the repository for Security.Role objects
        /// </summary>
        IRepository<Role> RoleRep { get; }

        /// <summary>
        /// Gets the repository for Security.UserRole objects.
        /// </summary>
        IRepository<UserRole> UserRoleRep { get; }

        /// <summary>
        /// Repository for ProgressLogEntry objects
        /// </summary>
        IRepository<ProgressLogEntry> ProgressLogEntryRep { get; }

        /// <summary>
        /// Gets the repository for WorkCrew objects.
        /// </summary>
        IRepository<WorkCrew> WorkCrewRep { get; }

        /// <summary>
        /// Gets the repository for FileImport objects.
        /// </summary>
        IRepository<FileImport> FileImportRep { get; }

        /// <summary>
        /// Get the ChartDefinition repository.  Do it.
        /// </summary>
        IRepository<ChartDefinition> ChartDefinitionRep { get; }

        /// <summary>
        /// Gets the HistoryChartData repository.
        /// </summary>
        IRepository<HistoryChartData> HistoryChartDataRep { get; }

        /// <summary>
        /// Gets the HistoryChartDataPoint repository.  It's not nice to point.
        /// </summary>
        IRepository<HistoryChartDataPoint> HistoryChartDataPointRep { get; }

        /// <summary>
        /// Gets the GroupedCompletionChart repository.  Says, "You're welcome."
        /// </summary>
        IRepository<GroupedCompletionChart> GroupedCompletionChartRep { get; }

        /// <summary>
        /// Gets the GroupedCompletionChartDataPoint repository.  That's a long name.
        /// </summary>
        IRepository<GroupedCompletionChartDataPoint> GroupedCompletionChartDataPointRep { get; }

        /// <summary>
        /// Returns a repository for a specific type of item, creating it if necessary.
        /// </summary>
        /// <typeparam name="DOType">The System.Type of targeted objects.  Must be a DomainObject class
        /// with a parameterless constructor.</typeparam>
        /// <returns></returns>
        IRepository<DOType> GetRepository<DOType>()
            where DOType : DomainObject, new();

        /// <summary>
        /// Retreives a raw IQueryable for an item that exists in the data access layer and core, but has no 
        /// accessible library or repository.
        /// </summary>
        /// <typeparam name="T">The Type of object to retrieve.</typeparam>
        /// <returns></returns>
        IQueryable<T> GetQueryable<T>()
            where T : class;

        /// <summary>
        /// Retrieves a set of Job ID values and work dates that have progress that has not been
        /// updated in the ProgressHistory.
        /// </summary>
        /// <returns></returns>
        IEnumerable<UnrecognizedProgress> GetUnrecognizedProgress();

        /// <summary>
        /// Updates the ProgressHistory values for the job and date specified.
        /// </summary>
        /// <param name="jobID">ID of the job entries to update.</param>
        /// <param name="workDate">The work date to check for progress.</param>
        void UpdateProgressHistory(long jobID, DateTime workDate);

        /// <summary>
        /// Removes the current chart data for the Job indicated or All jobs if a Zero value is passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to purge, or 0 if all jobs should have their current chart data wiped.</param>
        void PurgeReportingData(long jobID = 0);

        /// <summary>
        /// Retrieves currently persisted value of the item for the property specified.
        /// </summary>
        /// <typeparam name="DOType">The DomainObject Type of the item.</typeparam>
        /// <typeparam name="TProperty">The datatype of the Property being retrieved</typeparam>
        /// <param name="item">The item for which the committed property value should be retrieved.</param>
        /// <param name="property">An expression identifying the property</param>
        /// <returns></returns>
        TProperty GetCommittedPropertyValue<DOType, TProperty>(DOType item, Expression<Func<DOType, TProperty>> property)
            where DOType : DomainObject;

        /// <summary>
        /// Sets the Commodity for all TaskProgress items within the identified tasks, with certain specific tolerances identified by arguments.
        /// </summary>
        /// <param name="taskID">Required, the TaskID to use when selecting items to change.</param>
        /// <param name="commodityID">The ID of the new Commodity to set on these task items. Will clear the commodity if null.</param>
        /// <param name="matchCommodity">If set, the operation will only change the commodity of items that have a commodity ID matching this value.  If null, will always change the current Commodities set on TaskProgress items.</param>
        /// <param name="setNulls">If true, this operation will set values that currently have no commodity set, as long as they are part of the task.</param>
        /// <returns></returns>
        int PropogateCommodityByTask(long taskID, long? commodityID, long? matchCommodity = null, bool setNulls = true);

        /// <summary>
        /// Using one of the optional arguments passed, calculates the Historical Chart Values based on a stored procedure.
        /// </summary>
        /// <param name="jobID">ID of the job to use to filter history results.</param>
        /// <param name="phaseID">ID of phase by which to filter.</param>
        /// <param name="taskID">ID of the task by which to filter.</param>
        /// <param name="trackedItemID">ID of the task by which to filter.</param>
        /// <param name="systemID">ID of the system by which to filter.</param>
        /// <param name="commodityID">ID of the system by which to filter.</param>
        /// <param name="areaID">ID of the area by which to filter.</param>
        /// <param name="objectTypeID">ID of the object type by which to filter.</param>
        /// <param name="craftID">ID of the craft by which to filter.</param>
        /// <param name="udpValue1">The first UDP Value to use as a filter.</param>
        /// <param name="udpValue2">The second UDP Value to use as a filter.</param>
        /// <param name="udpValue3">The third UDP Value to use as a filter.</param>
        /// <returns></returns>
        List<RawHistoryDataPoint> SelectNewHistoryChartData(long? jobID = null, long? phaseID = null,
            long? taskID = null, long? trackedItemID = null, long? systemID = null, long? commodityID = null,
            long? areaID = null, long? objectTypeID = null, long? craftID = null, string udpValue1 = null,
            string udpValue2 = null, string udpValue3 = null, DateTime? workDate = null);
    }
}