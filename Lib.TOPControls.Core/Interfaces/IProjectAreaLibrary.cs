﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides operations and object validation for instances of the ProjectArea class.
    /// </summary>
    public interface IProjectAreaLibrary:IJobObjectLibrary<ProjectArea>
    {

        /// <summary>
        /// Returns a set of ProjectArea objects within the job that match the search term passed.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search. If the matching JobInfo is marked as
        /// deleted, an empty result set will be returned.</param>
        /// <param name="searchTerm">The search term to use.  If an empty string is passed, all 
        /// items of this type within the job will be returned.</param>
        /// <returns></returns>
        IEnumerable<ProjectArea> Search(long jobID, string searchTerm);

    }
}
