﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides operation contracts for managing UserJobs
    /// </summary>
    public interface IUserJobLibrary:IObjectLibrary<UserJob>
    {
        
        /// <summary>
        /// Returns all UserJob values.
        /// </summary>
        /// <returns></returns>
        IEnumerable<UserJob> GetAll();

        /// <summary>
        /// Returns all UserJob values that belong to a specific job.
        /// </summary>
        /// <param name="jobID">The ID of the job.</param>
        /// <returns></returns>
        IEnumerable<UserJob> AllForJob(long jobID);


        /// <summary>
        /// Returns all UserJob values that belong to a specific TOPUser.
        /// </summary>
        /// <param name="userID">The ID of the TOPUser.</param>
        /// <returns></returns>
        IEnumerable<UserJob> AllForUser(long userID);

    }
}
