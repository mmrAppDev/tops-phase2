﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.DataImport;

namespace Lib.TOPControls.Interfaces
{
    /// <summary>
    /// Provides the basic management operations for the files imported by users.
    /// </summary>
    public interface IFileImportLibrary:IJobObjectLibrary<FileImport>
    {

    }
}
