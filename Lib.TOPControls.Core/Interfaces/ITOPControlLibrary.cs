﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Lib.TOPControls.Charting;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Permissions;
using MMRCommon.Data;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides operations for cross-object actions and a centralized access point for
    /// object-specific libraries.
    /// </summary>
    public interface ITOPControlLibrary
    {
        /// <summary>
        /// Provides access to actions related to objects of type JobInfo
        /// </summary>
        IJobInfoLibrary JobActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type ProjectPhase
        /// </summary>
        IProjectPhaseLibrary PhaseActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type ObjedctType
        /// </summary>
        IObjectTypeLibrary ObjectTypeActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type ProjectSystem
        /// </summary>
        IProjectSystemLibrary SystemActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type ProjectArea
        /// </summary>
        IProjectAreaLibrary AreaActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type CraftInfo
        /// </summary>
        ICraftInfoLibrary CraftActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type Commodity
        /// </summary>
        ICommodityLibrary CommodityActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type TrackedItem
        /// </summary>
        ITrackedItemLibrary TrackedItemActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type Task
        /// </summary>
        ITaskLibrary TaskActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type TaskProgress
        /// </summary>
        ITaskProgressLibrary TaskProgressActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type TOPUser
        /// </summary>
        ITOPUserLibrary UserActions { get; set; }

        /// <summary>
        /// Provides access to actions related to object of type UserJob
        /// </summary>
        IUserJobLibrary UserJobActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type WorkCrew
        /// </summary>
        IWorkCrewLibrary WorkCrewActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type ProgressLogEntry
        /// </summary>
        IProgressLogEntryLibrary ProgressLogEntryActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type FilterConfiguration
        /// </summary>
        IFilterConfigurationLibrary FilterConfigActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type FileImport
        /// </summary>
        IFileImportLibrary FileImportActions { get; set; }

        /// <summary>
        /// Provides access to actions related to objects of type ChartDefinition
        /// </summary>
        IChartDefinitionLibrary ChartDefinitionActions { get; set; }

        /// <summary>
        /// Do things with HistoryChartData.  Amaze your friends.
        /// </summary>
        IHistoryChartDataLibrary HistoryChartDataActions { get; set; }

        /// <summary>
        /// Perform simple operations with GroupedCompletionChart objects.  
        /// </summary>
        IGroupedCompletionChartLibrary GroupedCompletionChartActions { get; set; }

        /// <summary>
        /// Gets/Sets the Permission Manager to be used
        /// </summary>
        IPermissionManager PermissionManager { get; set; }

        /// <summary>
        /// Retreives a raw IQueryable for an item that exists in the data access layer and core, but has no 
        /// accessible library or repository.
        /// </summary>
        /// <typeparam name="T">The Type of object to retrieve.</typeparam>
        /// <returns></returns>
        IQueryable<T> GetQueryable<T>()
            where T : class;

        /// <summary>
        /// Returns the IRepository instance being provided by the Data Access Layer for the Domain Object type specified.
        /// </summary>
        /// <typeparam name="DOType">A subclass of DomainObject with a parameterless constructor.</typeparam>
        /// <returns></returns>
        IRepository<DOType> GetRepository<DOType>()
            where DOType : DomainObject, new();

        /// <summary>
        /// Provides the IObjectLibrary instance for the specified type.
        /// </summary>
        /// <typeparam name="DOType">A subclass of DomainObject with a parameterless constructor.</typeparam>
        /// <returns></returns>
        IObjectLibrary<DOType> GetLibrary<DOType>()
            where DOType : DomainObject, new();

        /// <summary>
        /// Returns a set of TaskProgress values filtered by the values defined by the filterParameters.
        /// </summary>
        /// <param name="jobID">The ID of the Job being viewed.</param>
        /// <param name="filterParams">A dictionary of items that will restrict the list of values returned.</param>
        /// <returns></returns>
        IQueryable<TaskProgress> FilterTaskProgress(long jobID, FilterParameters filterParams);

        /// <summary>
        /// Returns the set of ProjectSystem selections based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        IEnumerable<ProjectSystem> GetProjectSystemsForFilter(long jobID, FilterParameters filterParams);

        /// <summary>
        /// Returns the set of ProjectArea selections based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        IEnumerable<ProjectArea> GetProjectAreasForFilter(long jobID, FilterParameters filterParams);

        /// <summary>
        /// Returns the set of ObjectType selections based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        IEnumerable<ObjectType> GetObjectTypesForFilter(long jobID, FilterParameters filterParams);

        /// <summary>
        /// Returns the set of CraftInfo selections based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        IEnumerable<CraftInfo> GetCraftInfoValuesForFilter(long jobID, FilterParameters filterParams);

        /// <summary>
        /// Get a tree representation of ProjectSystem objects
        /// </summary>
        /// <returns></returns>
        IEnumerable<ProjectSystem> GetProjectSystemTree(long jobId);

        /// <summary>
        /// Returns the set of values for User Defined Property 1 based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        IEnumerable<string> GetUserDefinedVals_1_ForFilter(long jobID, FilterParameters filterParams);

        /// <summary>
        /// Returns the set of values for User Defined Property 2 based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        IEnumerable<string> GetUserDefinedVals_2_ForFilter(long jobID, FilterParameters filterParams);

        /// <summary>
        /// Returns the set of values for User Defined Property 3 based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        IEnumerable<string> GetUserDefinedVals_3_ForFilter(long jobID, FilterParameters filterParams);

        /// <summary>
        /// Returns the set of Commodity selections based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        IEnumerable<Commodity> GetCommoditiesForFilter(long jobID, FilterParameters filterParams);

        /// <summary>
        /// Returns the set of Task selections based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        IEnumerable<Task> GetTasksForFilter(long jobID, FilterParameters filterParams);

        /// <summary>
        /// Returns the set of WorkCrew selections based on the fitler parameters passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to use.</param>
        /// <param name="filterParams">A FilterParameters instance that will filter all items but the type returned.</param>
        /// <returns></returns>
        IEnumerable<WorkCrew> GetWorkCrewsForFilter(long jobID, FilterParameters filterParams);

        /// <summary>
        /// Updates the percent complete of a TaskProgress item, and logs the progress the Progress Log, returning the ProgressLogEntry
        /// instance that is generated.
        /// </summary>
        /// <param name="jobID">The ID of the Job to which this progress log entry is being written.  Must match the job of
        /// the TaskProgress item being updated.</param>
        /// <param name="taskProgressID">A long integer identifying the TaskProgess item that will be updated.</param>
        /// <param name="newPercentComplete">A decimal value representing the PercentComplete value being set.</param>
        /// <param name="workDate">The date that the work causing the progress was actually performed.</param>
        /// <param name="workCrewID">The ID value of the WorkCrew that performed the work.</param>
        /// <param name="userID">The ID of the user performing the logging operation.</param>
        /// <returns></returns>
        ProgressLogEntry UpdateProgress(long jobID, long taskProgressID, decimal newPercentComplete, DateTime workDate,
            long workCrewID, long userID);

        /// <summary>
        /// Returns a decimal value indicating the percent complete over the entirety of the Filtered Values.
        /// </summary>
        /// <param name="jobID">The Job ID for which to calculate.</param>
        /// <param name="filterParams">A dictionary of items that will restrict the list of values returned.</param>
        /// <returns></returns>
        Decimal CalculateTotalPercentCompleteForFilter(long jobID, FilterParameters filterParams);

        /// <summary>
        /// Returns a HistoryChartData instance that provides data for a HistoryChart for the parameters 
        /// and grouping information passed.
        /// </summary>
        /// <param name="jobID">The ID of the related Job.</param>
        /// <param name="filterParams">A FilterParameters item providing the basic environment of the chart.  If passed as null,
        /// returns data restricted only by the grouping selected.</param>
        /// <param name="grouping">A value from the FilterValueType enumeration, indicating the type of value to group
        /// the returned values by.  Pass None to display a single chart for the entire job.</param>
        /// <param name="groupValue">The identifying value for the grouped item data to display. If grouping by system, will be the
        /// ID of the target system (long).  If grouping by UserDefinedProperty, will be the string value to group by.  Ignored if 
        /// grouping is passed as "None".</param>
        /// <returns></returns>
        HistoryChartData GetHistoryChartData(long jobID, FilterParameters filterParams, FilterValueType grouping,
            object groupValue);

        /// <summary>
        /// Returns a set of PhaseSummary items for the job requested.
        /// </summary>
        /// <param name="jobID">ID of the job to use.</param>
        /// <returns></returns>
        IEnumerable<PhaseSummary> GetPhaseSummaries(long jobID);

        /// <summary>
        /// Returns a set of NamedPercentage values to represent the amount of work completed to date for a particular grouping, such as system.
        /// </summary>
        /// <param name="jobID">The ID of the job for which this is being performed.</param>
        /// <param name="grouping">A FilterValueType indicating the kind of values to be grouped.</param>
        /// <returns></returns>
        IEnumerable<GroupedCompletionChartDataPoint> GetGroupedCompletions(long jobID, FilterValueType grouping);

        /// <summary>
        /// Retrieves a set of Job ID values and work dates that have progress that has not been
        /// updated in the ProgressHistory.
        /// </summary>
        /// <returns></returns>
        IEnumerable<UnrecognizedProgress> GetUnrecognizedProgress();

        /// <summary>
        /// Updates the ProgressHistory values for the job and date specified.
        /// </summary>
        /// <param name="jobID">ID of the job entries to update.</param>
        /// <param name="workDate">The work date to check for progress.</param>
        void UpdateProgressHistory(long jobID, DateTime workDate);

        /// <summary>
        /// Updates progress histories and pre-generated chart and reporting data.  In some cases this operation
        /// can take quite a while.
        /// </summary>
        void UpdateReportingData();

        /// <summary>
        /// Retrieves currently persisted value of the item for the property specified.
        /// </summary>
        /// <typeparam name="DOType">The DomainObject Type of the item.</typeparam>
        /// <typeparam name="TProperty">The datatype of the Property being retrieved</typeparam>
        /// <param name="item">The item for which the committed property value should be retrieved.</param>
        /// <param name="property">An expression identifying the property</param>
        /// <returns></returns>
        TProperty GetCommittedPropertyValue<DOType, TProperty>(DOType item, Expression<Func<DOType, TProperty>> property)
            where DOType : DomainObject;

        /// <summary>
        /// Sets the Commodity for all TaskProgress items within the identified tasks, with certain specific tolerances identified by arguments.
        /// </summary>
        /// <param name="taskID">Required, the TaskID to use when selecting items to change.</param>
        /// <param name="commodityID">The ID of the new Commodity to set on these task items. Will clear the commodity if null.</param>
        /// <param name="matchCommodity">If set, the operation will only change the commodity of items that have a commodity ID matching this value.  If null, will always change the current Commodities set on TaskProgress items.</param>
        /// <param name="setNulls">If true, this operation will set values that currently have no commodity set, as long as they are part of the task.</param>
        /// <returns></returns>
        int PropogateCommodityByTask(long taskID, long? commodityID, long? matchCommodity = null, bool setNulls = true);

        /// <summary>
        /// Removes the current chart data for the Job indicated or All jobs if a Zero value is passed.
        /// </summary>
        /// <param name="jobID">The ID of the job to purge, or 0 if all jobs should have their current chart data wiped.</param>
        void PurgeReportingData(long jobID = 0);

        /// <summary>
        /// Using one of the optional arguments passed, calculates the Historical Chart Values based on a stored procedure.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        List<RawHistoryDataPoint> SelectNewHistoryChartData(long? jobID = null, long? phaseID = null,
            long? taskID = null, long? trackedItemID = null,
            long? systemID = null, long? commodityID = null, long? areaID = null, long? objectTypeID = null,
            long? craftID = null, string udpValue1 = null, string udpValue2 = null, string udpValue3 = null,
            DateTime? workDate = null);
    }
}