﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.Interfaces;
using MMRCommon.Data;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// Provides operations for HistoryChartData
    /// </summary>
    public interface IHistoryChartDataLibrary:IJobObjectLibrary<HistoryChartData>
    {
        /// <summary>
        /// Retrieves a set of stored history values by the same means that would be used to generate the same set of data.
        /// </summary>
        /// <param name="jobID">ID of the Job to use.</param>
        /// <param name="parms">A FilterParameters instance, or NULL.</param>
        /// <param name="grouping">An enumeration member identifying the value to group on.</param>
        /// <param name="groupValue">The identifier for the specific group to return.</param>
        /// <returns></returns>
        HistoryChartData GetByDefinition(long jobID, FilterParameters parms, FilterValueType grouping, object groupValue);

        /// <summary>
        /// Retrieves all HistoryChartDataPoint items related to the History Chart Data indicated by the ID.
        /// </summary>
        /// <param name="historyChartDataID">The ID of the History Chart Data to target.</param>
        /// <returns></returns>
        IEnumerable<HistoryChartDataPoint> GetDataPoints(long historyChartDataID);

        /// <summary>
        /// Stores the object passed.
        /// </summary>
        /// <param name="item">A HistoryChartDataPoint instance.</param>
        void SaveDataPoint(HistoryChartDataPoint item);

        /// <summary>
        /// Validates the item passed.
        /// </summary>
        /// <param name="item">The HistoryChartDataPoint to validate</param>
        /// <returns></returns>
        ValidationResult ValidateDataPoint(HistoryChartDataPoint item);
        
    }
}
