﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides operations and object validation for instances of the ProjectPhase class.
    /// </summary>
    public interface IProjectPhaseLibrary:IJobObjectLibrary<ProjectPhase>
    {

        /// <summary>
        /// Returns a set of ProjectPhase objects within the Job indicated by jobID that match 
        /// the searchTerm passed.    Does not return Phases from deleted jobs.
        /// </summary>
        /// <param name="jobID">The ID of the job to search</param>
        /// <param name="searchTerm">The string value to search for.  If the searchTerm is an 
        /// empty string, returns all ProjectPhase records.</param>
        /// <returns></returns>
        IEnumerable<ProjectPhase> Search(long jobID, string searchTerm);

        /// <summary>
        /// Returns a set of ProjectPhase objects, within the Job indicated by jobID (even if 
        /// the job is deleted) that match the searchTerm passed.  
        /// </summary>
        /// <param name="jobID">The ID of the job to search</param>
        /// <param name="searchTerm">The string value to search for.  If the searchTerm is an 
        /// empty string, returns all ProjectPhase records.</param>
        /// <param name="userID">The ID of the TOPUser performing the search. Will fail if the 
        /// referenced user does not have rights to view deleted job info.</param>
        /// <returns></returns>
        IEnumerable<ProjectPhase> SearchAny(long jobID, string searchTerm, long userID);


        /// <summary>
        /// Returns a set of ProjectPhase objects. Modifies the DisplayOrder property of the project 
        /// phase identified so that it is one item closer to the start of the list, also modifying 
        /// the DisplayOrder property of the phase that occupies its new position.  No values are changed 
        /// if the phase is already at the top of the list.
        /// </summary>
        /// <param name="projectPhaseID">The ID value of the ProjectPhase item being moved up.</param>
        /// <param name="userID">The ID of the TOPUser performing this operation. This is used to both 
        /// log the action and validate the user’s right to perform the operation.</param>
        /// <returns></returns>
        IEnumerable<ProjectPhase> MoveUpInDisplayOrder(long projectPhaseID, long userID);

        /// <summary>
        /// Returns a set of ProjectPhase objects. Modifies the DisplayOrder property of the project 
        /// phase identified so that it is one item closer to the end of the list, also modifying the 
        /// DisplayOrder property of the phase that occupies its new position.  No values are changed 
        /// if the phase is already at the bottom of the list. 
        /// </summary>
        /// <param name="projectPhaseID">The ID value of the ProjectPhase item being moved down.</param>
        /// <param name="userID">The ID of the TOPUser performing this operation. This is used to both 
        /// log the action and validate the user’s right to perform the operation.</param>
        /// <returns></returns>
        IEnumerable<ProjectPhase> MoveDownInDisplayOrder(long projectPhaseID, long userID);




    }
}
