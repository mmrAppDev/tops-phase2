﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides operations and object validation for instances of the CraftInfo class.
    /// </summary>
    public interface ICraftInfoLibrary:IJobObjectLibrary<CraftInfo>
    {

        /// <summary>
        /// Returns a set of CraftInfo objects within the Job indicated by jobID that match the 
        /// searchTerm passed.  Does not return Crafts from deleted jobs.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">A string to search for.  If the searchTerm is an empty string, 
        /// returns all CraftInfo records within the job.</param>
        /// <returns></returns>
        IEnumerable<CraftInfo> Search(long jobID, string searchTerm);

    }
}
