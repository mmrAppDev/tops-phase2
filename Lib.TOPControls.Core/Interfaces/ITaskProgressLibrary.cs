﻿using System;
using System.Collections.Generic;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides most operations and object validation for instances of the TrackedItem class.
    /// </summary>
    public interface ITaskProgressLibrary : ISoftDeleteLibrary<TaskProgress>, IJobObjectLibrary<TaskProgress>
    {
        /// <summary>
        /// Returns a set of TaskProgress objects within the Job indicated by jobID that match the
        /// searchTerm passed.  If the searchTerm is an empty string, returns all TaskProgress items
        /// in the job. Does not return TaskProgress for deleted jobs, or for any deleted TrackedItems.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">A string value to search for.</param>
        /// <returns></returns>
        IEnumerable<TaskProgress> Search(long jobID, string searchTerm);

        /// <summary>
        /// Performs that same operation as Search(), but includes TrackedItems that are marked as
        /// deleted, or are members of jobs that are marked as deleted.  If the user referenced by
        /// userID does not have rights to view deleted TrackedItems, the operation will fail.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">A string value to search for.</param>
        /// <param name="userID">The ID of the TOPUser performing the operation.</param>
        /// <returns></returns>
        IEnumerable<TaskProgress> SearchAny(long jobID, string searchTerm, long userID);

        /// <summary>
        /// Returns an integer value indicating the number of items related to the Commodity specified
        /// by commodityID.  The count returned does not include TaskProgress items for deleted
        /// TrackedItem records.
        /// </summary>
        /// <param name="commodityID">The ID of the Commodity to count.  If commodity ID is passed as
        /// a null, returns a count of all TaskProgress items without an associated Commodity.</param>
        /// <returns></returns>
        int CountItemsByCommodity(long? commodityID);

        /// <summary>
        /// Returns a set of all TaskProgress items for the job that are related to the TrackedItem with the TrackingID passed.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="trackingID">The exact TrackingID of the TrackedItem to use as a filter.</param>
        /// <returns></returns>
        IEnumerable<TaskProgress> FindByTrackingID(long jobID, string trackingID);

        /// <summary>
        /// Updates the percent complete of a TaskProgress item, and logs the progress the ProgressLog, returning the ProgressLogEntry
        /// instance that is generated.
        /// </summary>
        /// <param name="jobID">The ID of the Job to which this progress log entry is being written.  Must match the job of
        /// the TaskProgress item being updated.</param>
        /// <param name="taskProgressID">A long integer identifying the TaskProgess item that will be updated.</param>
        /// <param name="newPercentComplete">A decimal value representing the PercentComplete value being set.</param>
        /// <param name="workDate">The date that the work causing the progress was actually performed.</param>
        /// <param name="workCrewID">The ID value of the WorkCrew that performed the work.</param>
        /// <param name="userID">The ID of the user performing the logging operation.</param>
        /// <returns></returns>
        ProgressLogEntry UpdateProgress(long jobID, long taskProgressID, decimal newPercentComplete, DateTime workDate,
            long workCrewID, long userID);

        /// <summary>
        /// Returns a TaskProgress item that matches the Task ID and Tracking ID string passed.
        /// </summary>
        /// <param name="taskID">A long integer repesenting the ID of the associated Task.</param>
        /// <param name="trackingID">The string TrackingID of the associted Tracked Item.</param>
        /// <returns></returns>
        TaskProgress FindByTaskTrackingID(long taskID, string trackingID);

        /// <summary>
        /// Sets the Commodity for all TaskProgress items within the identified tasks, with certain specific tolerances identified by arguments.
        /// </summary>
        /// <param name="taskID">Required, the TaskID to use when selecting items to change.</param>
        /// <param name="commodityID">The ID of the new Commodity to set on these task items. Will clear the commodity if null.</param>
        /// <param name="matchCommodity">If set, the operation will only change the commodity of items that have a commodity ID matching this value.  If null, will always change the current Commodities set on TaskProgress items.</param>
        /// <param name="setNulls">If true, this operation will set values that currently have no commodity set, as long as they are part of the task.</param>
        /// <returns></returns>
        int PropogateCommodityByTask(long taskID, long? commodityID, long? matchCommodity = null, bool setNulls = true);

        /// <summary>
        /// Update the progress of a task based on its ProgressHistory.
        /// </summary>
        /// <param name="taskProgressId">The is of the TaskProgress to update.</param>
        /// <param name="userId">The is of the user performing the action</param>
        void SetProgressFromHistory(long taskProgressId, long userId);
    }
}