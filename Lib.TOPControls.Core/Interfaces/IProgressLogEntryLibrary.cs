﻿using System;
using System.Collections.Generic;
using Lib.TOPControls.Charting;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides the contracts for working with items of the ProgressLogEntry type.
    /// </summary>
    public interface IProgressLogEntryLibrary : IObjectLibrary<ProgressLogEntry>
    {



    }
}