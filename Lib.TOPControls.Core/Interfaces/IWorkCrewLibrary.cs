﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides most operations and object validation for instances of the WorkCrew class.
    /// </summary>
    public interface IWorkCrewLibrary:IJobObjectLibrary<WorkCrew>
    {
        /// <summary>
        /// Returns a set of all WorkCrew items within the Job.
        /// </summary>
        /// <param name="jobID">The ID of the job by which to filter the results.</param>
        /// <returns></returns>
        IEnumerable<WorkCrew> AllByJob(long jobID);
    }
}
