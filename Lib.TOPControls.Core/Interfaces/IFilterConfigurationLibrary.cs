﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides the operations for instances of the FilterConfiguration class.
    /// </summary>
    public interface IFilterConfigurationLibrary:IJobObjectLibrary<FilterConfiguration>
    {
        /// <summary>
        /// Returns a set of FilterConfiguration objects. Modifies the DisplayOrder property of the project 
        /// item identified so that it is one item closer to the start of the list, also modifying 
        /// the DisplayOrder property of the item that occupies its new position.  No values are changed 
        /// if the item is already at the top of the list.
        /// </summary>
        /// <param name="itemID">The ID value of the item being moved up.</param>
        /// <param name="userID">The ID of the TOPUser performing this operation. This is used to both 
        /// log the action and validate the user’s right to perform the operation.</param>
        /// <returns></returns>
        IEnumerable<FilterConfiguration> MoveUpInDisplayOrder(long itemID, long userID);

        /// <summary>
        /// Returns a set of FilterConfiguration objects. Modifies the DisplayOrder property of the project 
        /// item identified so that it is one item closer to the end of the list, also modifying the 
        /// DisplayOrder property of the item that occupies its new position.  No values are changed 
        /// if the item is already at the bottom of the list. 
        /// </summary>
        /// <param name="itemID">The ID value of the item being moved down.</param>
        /// <param name="userID">The ID of the TOPUser performing this operation. This is used to both 
        /// log the action and validate the user’s right to perform the operation.</param>
        /// <returns></returns>
        IEnumerable<FilterConfiguration> MoveDownInDisplayOrder(long itemID, long userID);

    }
}
