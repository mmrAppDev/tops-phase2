﻿using System.Collections.Generic;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides the contracts for working with items of the ProjectSystem type.
    /// </summary>
    public interface IProjectSystemLibrary : IJobObjectLibrary<ProjectSystem>
    {
        /// <summary>
        /// Returns a set of ProjectSystem items within the Job indicated that match the search term.
        /// </summary>
        /// <param name="jobID">The ID of the job by which to filter the results.</param>
        /// <param name="searchTerm">The search term to use for filtering results. If an empty string 
        /// is passed for this argument, returns all items within the job.</param>
        /// <returns></returns>
        IEnumerable<ProjectSystem> Search(long jobID, string searchTerm);

        /// <summary>
        /// Returns a set of ProjectSystem items within the Job indicated
        /// </summary>
        /// <param name="jobID">The ID of the job by which to filter the results.</param>
        /// <param name="parentId">The ID of the parent system</param>
        /// <returns></returns>
        IEnumerable<ProjectSystem> FindByParentId(long jobID, long? parentId);

        /// <summary>
        /// Returns a ProjectSystem, if one exists, belonging to the Parent passed (if passed) and having the name value passed.
        /// </summary>
        /// <param name="jobID">The ID of the job by which to filter the results.</param>
        /// <param name="parentId">The ID of the expected parent item.</param>
        /// <param name="name">The name (or System Number) of the System to find.</param>
        /// <returns></returns>
        ProjectSystem FindByParentAndName(long jobID, long? parentId, string name);

        /// <summary>
        /// Returns a tree of ProjectSystem items within the indicated Job
        /// </summary>
        /// <param name="jobID">The ID of the job by which to filter the results.</param>
        /// <returns></returns>
        IEnumerable<ProjectSystem> GetTreeByJobId(long jobID);

        /// <summary>
        /// Returns a branch of ProjectSystem items within the indicated System
        /// </summary>
        /// <param name="jobID">The ID of the job by which to filter the results.</param>
        /// <param name="systemID">The ID of the system by which to filter the results.</param>
        /// <returns></returns>
        ProjectSystem GetSystemBranch(long jobId, long systemID);
    }
}