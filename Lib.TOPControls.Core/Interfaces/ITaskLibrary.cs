﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls
{
    /// <summary>
    /// Provides most operations and object validation for instances of the Task class.
    /// </summary>
    public interface ITaskLibrary:ISoftDeleteLibrary<Task>, IJobObjectLibrary<Task>
    {

        /// <summary>
        /// Returns a set of Task objects within the Job that match the search term passed.  Does not return deleted items or items for a deleted job.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">The search term to use to search.  Passing no search term or a blank one, will 
        /// return all items within the job.</param>
        /// <returns></returns>
        IEnumerable<Task> Search(long jobID, string searchTerm);

        /// <summary>
        /// Returns a set of Task objects within the Job that match the search term passed.  Will return deleted items and items for a deleted job.
        /// </summary>
        /// <param name="jobID">The ID of the Job to search.</param>
        /// <param name="searchTerm">The search term to use to search.  Passing no search term or a blank one, will 
        /// return all items within the job.</param>
        /// <param name="userID">The ID of the user performing the action.</param>
        /// <returns></returns>
        IEnumerable<Task> SearchAny(long jobID, string searchTerm, long userID);
    }
}
