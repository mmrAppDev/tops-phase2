﻿using MMRCommon.Data;

namespace Lib.TOPControls.Interfaces
{
    /// <summary>
    /// Provides the contracts for basic business class library operations on objects in the DomainObject classes.
    /// </summary>
    /// <typeparam name="T">A System.Type that this library will manage.  Must be subtype of DomainObject.</typeparam>
    public interface IObjectLibrary<T> where T : DomainObject
    {
        /// <summary>
        /// Returns an instance of target type if there is a valid item with an ID value matching the value passed.  If
        /// the matching item is marked as deleted, or if there is no item with a matching ID, retuns NULL.
        /// </summary>
        /// <param name="id">The ID value to find.</param>
        /// <returns></returns>
        T Find(long id);

        /// <summary>
        /// Adds a new instance to the stored data.
        /// </summary>
        /// <param name="newItem">An instance to store.  Must have an ID value of 0.</param>
        /// <param name="userID">The ID of the TOPUser performing this action.</param>
        /// <returns></returns>
        T Add(T newItem, long userID);

        /// <summary>
        /// Updates the record of this type that matches the item's ID with the rest of the item properties.
        /// </summary>
        /// <param name="item">An item to update in the database. This must not be NULL.</param>
        /// <param name="userID">The ID of the TOPUser performing this operation, for both logging and security purposes.
        /// Must match a valid user record.</param>
        /// <returns></returns>
        T Update(T item, long userID);

        /// <summary>
        /// Returns a ValidationResult that indicates if the item passed validation to be recorded in the database.  If the
        /// validation fails, the returned value can be checked for the RuleViolations that caused it.
        /// </summary>
        /// <param name="item">The item to validate.  Item's with an ID of zero will be validated as new items.  All others
        /// will be validated for an update operation.</param>
        /// <returns></returns>
        ValidationResult Validate(T item);

        /// <summary>
        /// Returns a ValidationResult that will be valid if the item matching the ID passed can be deleted 
        /// without causing inconsistent data and without violating business rules.
        /// </summary>
        /// <param name="id">The ID of the item of this library's type that  is to be deleted.</param>
        /// <returns></returns>
        ValidationResult ValidateDelete(long id);


        /// <summary>
        /// Performs a delete operation on the item indicated.
        /// </summary>
        /// <param name="id">The ID of the item of this type to remove from the database.</param>
        /// <param name="userID">The ID of the TOPUser performing this deletion, to verify permissions and perform activity
        /// logging.  Must match a valid user record.</param>
        void Delete(long id, long userID);


    }
}