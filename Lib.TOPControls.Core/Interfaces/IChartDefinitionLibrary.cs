﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.TOPControls.Interfaces;

namespace Lib.TOPControls.Charting
{
    /// <summary>
    /// Defines actions that can be taken for ChartDefinitions.
    /// </summary>
    public interface IChartDefinitionLibrary:IObjectLibrary<ChartDefinition>
    {
        /// <summary>
        /// Returns all ChartDefinition values.
        /// </summary>
        /// <returns></returns>
        IEnumerable<ChartDefinition> GetAll();
    }
}
