﻿namespace Lib.TOPControls
{
    /// <summary>
    /// Summary information for a specific ProjectPhase.
    /// </summary>
    public class PhaseSummary
    {
        /// <summary>
        /// ID of the phase 
        /// </summary>
        public long PhaseID { get; set; }

        /// <summary>
        /// Title to display for thae phase.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The position of this phase relative to other phases in the job.
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// A count of the number of Tasks in this phase that have not been completed.
        /// </summary>
        public int IncompleteTaskCount { get; set; }

        /// <summary>
        /// A count of the number of Tasks belonging to this phase.
        /// </summary>
        public int TaskCount { get; set; }

        /// <summary>
        /// A decimal value representing the completion percentage of this phase.
        /// </summary>
        public decimal PercentComplete { get; set; }

        /// <summary>
        /// Man-Hours shown by the completion of Task Progress items in this phase.
        /// </summary>
        public decimal CompletedManHours { get; set; }

        /// <summary>
        /// Man-Hours on all Task Progress items in this phase.
        /// </summary>
        public decimal TotalManHours { get; set; }
    }
}