﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls
{
    /// <summary>
    /// A base class for all data items within the domain of this application.
    /// </summary>
    public abstract class DomainObject
    {
        /// <summary>
        /// The unique ID number of the domain object class in the database.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// The last change date recorded in the database when this item was retrieved.
        /// </summary>
        public DateTime LastChanged { get; set; }

        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public abstract string DisplayName { get; }

        /// <summary>
        /// Sets the shared values for a domain object being created.
        /// </summary>
        public DomainObject()
        {
            LastChanged = DateTime.Now;
        }

        /// <summary>
        /// Sets the shared values for a domain object being created.
        /// </summary>
        /// <param name="id">The ID value to use for the new domain object.</param>
        /// <param name="lastChange">The LastChanged value to assign for the domain object this creates.</param>
        public DomainObject(long id, DateTime lastChange)
        {
            ID = id;
            LastChanged = lastChange;
        }

    }
}
