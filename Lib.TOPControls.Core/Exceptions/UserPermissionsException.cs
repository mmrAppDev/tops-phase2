﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Exceptions
{
    /// <summary>
    /// The exception that is thrown when a user permissions problem interrupts a low-level operation.
    /// </summary>
    public class UserPermissionsException:ApplicationException
    {
        /// <summary>
        /// The ID of the user performing the violating action.
        /// </summary>
        public long UserID { get; private set; }

        /// <summary>
        /// Creates an instance of the UserPermissionException with a specific error message and user ID.
        /// </summary>
        /// <param name="message">A message indicating the nature of this exception.</param>
        /// <param name="userID">The ID of the user performing the excepted action.</param>
        public UserPermissionsException(string message, long userID)
            : base(message)
        {
            UserID = userID;
        }

        /// <summary>
        /// Creates an instance of the UserPermissionException with a specific error message, user ID, and an inner
        /// exception.
        /// </summary>
        /// <param name="message">A message indicating the nature of this exception.</param>
        /// <param name="userID">The ID of the user performing the excepted action.</param>
        /// <param name="innerException">The exception that caused the current exception.  If the inner exception is not null, the current exception is raised in a catch block that handles the inner exception.</param>
        public UserPermissionsException(string message, long userID, Exception innerException)
            : base(message, innerException)
        {
            UserID = userID;
        }



            
    }
}
