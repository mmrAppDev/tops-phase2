﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMRCommon.Data;

namespace Lib.TOPControls.Exceptions
{
    /// <summary>
    /// An exception, including the failed ValidationResult, that is thrown when a TOPControl library operation attempts validation that fails.
    /// </summary>
    public class ValidationFailureException:ApplicationException
    {
        /// <summary>
        /// The ValidationResult that failed and caused the current exception.
        /// </summary>
        public ValidationResult FailedValidationResult { get; private set; }

        /// <summary>
        /// Creates the Validation failure exception with the exception message and the failed ValidationResult only.
        /// </summary>
        /// <param name="message">The message that should be attached to this exception.</param>
        /// <param name="validationResult">The failed ValidatonResult, complete with RuleViolations.</param>
        public ValidationFailureException(string message, ValidationResult validationResult)
            : base(message)
        {
            FailedValidationResult = validationResult;
        }

        /// <summary>
        /// Creates the Validation failure exception with the exception message and the failed ValidationResult only.
        /// </summary>
        /// <param name="message">The message that should be attached to this exception.</param>
        /// <param name="validationResult">The failed ValidatonResult, complete with RuleViolations.</param>
        /// <param name="innerException">The exception that caused the current exception.  If the inner exception is not null, the current exception is raised in a catch block that handles the inner exception.</param>
        public ValidationFailureException(string message, ValidationResult validationResult, Exception innerException)
            : base(message, innerException)
        {
            FailedValidationResult = validationResult;
        }

    }
}
