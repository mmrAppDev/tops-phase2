﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMRCommon.Data;

namespace Lib.TOPControls.Exceptions
{
    /// <summary>
    /// The Exception type thrown while attempting to import data from a file.
    /// </summary>
    public class FileImportException:ApplicationException
    {

        
        /// <summary>
        /// Creates a new FileImportException
        /// </summary>
        /// <param name="message">The Error message for this exception.</param>
        /// <param name="ex">The inner exception, if present.</param>
        public FileImportException(string message, Exception ex = null)
            : base(message, ex)
        {
            
        }

        /// <summary>
        /// Creates a new FilImportException including info from the Validation Result 
        /// </summary>
        /// <param name="message">The Error message for this exception.</param>
        /// <param name="vr">The validation result, which will be appended to the message.</param>
        /// <param name="ex">The inner exception, if present.</param>
        public FileImportException(string message, ValidationResult vr, Exception ex = null)
            : this(buildValidationResultMessage(message, vr), ex)
        {

        }

        private static string buildValidationResultMessage(string message, ValidationResult vr)
        {
            var result = new StringBuilder(message);
            if (vr != null && !vr.IsValid)
            {
                result.AppendLine();
                result.AppendFormat("Validation Errors: {0}", vr.RuleViolations.Count()).AppendLine();
                foreach (var rv in vr.RuleViolations)
                {
                    result.AppendLine(rv.ErrorMessage);
                }
            }
            return result.ToString();
        }
    }
}
