﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls
{
    /// <summary>
    /// The set of values describing an Area within the job.
    /// </summary>
    public class ProjectArea:DomainJobObject
    {

        /// <summary>
        /// A string value identifying this area within the job.
        /// </summary>
        public string AreaNumber { get; set; }

        /// <summary>
        /// A string value providing an extra layer of description, if desired, for the project area.
        /// </summary>
        public string Description { get; set; }


        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return AreaNumber; }
        }

        /// <summary>
        /// Creates an instance of the ProjectArea class with default values.
        /// </summary>
        public ProjectArea()
            : base()
        {
            Description = string.Empty;
        }


    }
}
