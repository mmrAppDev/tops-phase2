﻿using System.ComponentModel;

namespace Lib.TOPControls
{
    /// <summary>
    /// A value that identifies a value that can be used to filter a list of results when displaying
    /// Task Progress items.
    /// </summary>
    public enum FilterValueType
    {
        /// <summary>
        /// No filter to be applied.
        /// </summary>
        [Description("All Items")]
        [RelatedType(typeof(object))]
        None = 0,

        /// <summary>
        /// TrackingID of the associated Tracked Items.  Related type: string
        /// </summary>
        [Description("Tracking ID")]
        [RelatedType(typeof(string))]
        TrackingID = 1,

        /// <summary>
        /// ObjectType ID of the associated Tracked Items. Related type: long
        /// </summary>
        [Description("Tracking Item Type")]
        [RelatedType(typeof(long))]
        ObjectType = 2,

        /// <summary>
        /// ProjectSystem ID of the associated Tracked Items.  Related type: long
        /// </summary>
        [Description("System")]
        [RelatedType(typeof(long))]
        ProjectSystem = 3,

        /// <summary>
        /// ProjectArea ID of the associated Tracked Items. Related type: long
        /// </summary>
        [Description("Area")]
        [RelatedType(typeof(long))]
        ProjectArea = 4,

        /// <summary>
        /// CraftInfo ID of the associated Tracked Items. Related type: long
        /// </summary>
        [Description("Craft")]
        [RelatedType(typeof(long))]
        CraftInfo = 5,

        /// <summary>
        /// User Property 1 value of the related Tracked Items.  Related type: string
        /// </summary>
        [Description("User Property 1")]
        [RelatedType(typeof(string))]
        UserDefinedProperty1 = 6,

        /// <summary>
        /// User Property 2 value of the related Tracked Items.  Related type: string
        /// </summary>
        [Description("User Property 2")]
        [RelatedType(typeof(string))]
        UserDefinedProperty2 = 7,

        /// <summary>
        /// User Property 3 value of the related Tracked Items.  Related type: string
        /// </summary>
        [Description("User Property 3")]
        [RelatedType(typeof(string))]
        UserDefinedProperty3 = 8,

        /// <summary>
        /// Commodity ID value of the Task Progress Items.  Related type: long
        /// </summary>
        [Description("Commodity Code")]
        [RelatedType(typeof(long))]
        Commodity = 9,

        /// <summary>
        /// WorkCrew ID value of the related ProgressLogEntry Items.  Related type: long
        /// </summary>
        [Description("Work Crew")]
        [RelatedType(typeof(long))]
        WorkCrew = 10,

        /// <summary>
        /// The CompletionState of the Task Progress items. Related type: CompletionState
        /// </summary>
        [Description("Completion State")]
        [RelatedType(typeof(CompletionState))]
        CompletionState = 11,

        /// <summary>
        /// The ProjectPhase ID of the related Task items. Related type: long
        /// </summary>
        [Description("Phase")]
        [RelatedType(typeof(long))]
        Phase = 12,

        /// <summary>
        /// The Task ID of the Task Progress items. Related type: long
        /// </summary>
        [Description("Task")]
        [RelatedType(typeof(long))]
        Task = 13,

        /// <summary>
        /// The search text to use in selecting results.  Related type: string
        /// </summary>
        [Description("Search")]
        [RelatedType(typeof(string))]
        Search = 14
    }
}