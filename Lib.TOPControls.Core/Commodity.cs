﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls
{
    /// <summary>
    /// Represents the values associated with a commodity that is tracked.
    /// </summary>
    public class Commodity:DomainJobObject
    {

        /// <summary>
        /// A string value that identifies this item within the job.
        /// </summary>
        public string CommodityCode { get; set; }

        /// <summary>
        /// A brief description of the Commodity represented by this item.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Represents the total number of manhours estimated for this Commodity in this project.
        /// </summary>
        public decimal ManHoursEstimated { get; set; }

        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return CommodityCode; }
        }

    }
}
