﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls
{

    /// <summary>
    /// Represents a connection from a TOPUser to a JobInfo item.
    /// </summary>
    public class UserJob : DomainObject
    {
        /// <summary>
        /// The ID of the related TOPUser.
        /// </summary>
        public virtual long UserID { get; set; }
        /// <summary>
        /// The ID of the related JobInfo.
        /// </summary>
        public virtual long JobID { get; set; }


        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return string.Format("{0}-{1}", UserID, JobID); }
        }
        /// <summary>
        /// The related TOPUser.
        /// </summary>
        public virtual TOPUser TOPUser { get; set; }
        /// <summary>
        /// The related JobInfo.
        /// </summary>
        public virtual JobInfo JobInfo { get; set; }

    }
}
