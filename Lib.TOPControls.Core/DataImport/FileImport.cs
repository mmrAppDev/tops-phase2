﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;

namespace Lib.TOPControls.DataImport
{
    /// <summary>
    /// Provides data concerning the information imported from a file.
    /// </summary>
    public class FileImport : DomainJobObject
    {
        /// <summary>
        /// The date and time this file was submitted to the system.
        /// </summary>
        public DateTime DateSubmitted { get; set; }

        /// <summary>
        /// The ID of the user that submitted this file.
        /// </summary>
        public virtual long UserID { get; set; }

        /// <summary>
        /// The full path to the file that was committed.
        /// </summary>
        public string Filepath { get; set; }

        /// <summary>
        /// The filename used by the file system
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// The type of file that provided this data.  One of Excel, CSV, or Unknown
        /// </summary>
        public string FileType { get; set; }

        /// <summary>
        /// A name that was provided for this recordset.
        /// </summary>
        public string RecordsetName { get; set; }

        /// <summary>
        /// The xml text that represents the dataset created by this import operation.
        /// </summary>
        public string RecordsetXml { get; set; }

        /// <summary>
        /// The RecordsetXml as an XmlDocument
        /// </summary>
        public XmlDocument RecordsetXmlDocument
        {
            get
            {
                XmlDocument xdoc = null;
                if (!string.IsNullOrWhiteSpace(RecordsetXml))
                {
                    var rdr = new System.IO.StringReader(RecordsetXml);
                    try
                    {
                        xdoc = new XmlDocument();
                        xdoc.LoadXml(RecordsetXml);
                    }
                    catch
                    {
                        xdoc = null;
                    }
                }
                return xdoc;
            }
            set
            {
                RecordsetXml = value.OuterXml;
            }
        }

        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return string.Format("{0} at {1}", RecordsetName, DateSubmitted); }
        }

        /// <summary>
        /// The RecordsetXml as a DataSet
        /// </summary>
        /// <returns></returns>
        public DataSet GetAsDataSet()
        {
            DataSet result = null;
            if (!string.IsNullOrWhiteSpace(RecordsetXml))
            {
                try
                {
                    result = new DataSet();
                    var rdr = new System.IO.StringReader(RecordsetXml);
                    result.ReadXml(rdr);
                }
                catch
                {
                    result = null;
                }
            }
            return result;
        }

        /// <summary>
        /// Sets the RecordsetXml value to an Xml string representation of the dataset
        /// passed.
        /// </summary>
        /// <param name="ds">A dataset containing data imported from the file.</param>
        public void StoreAsXml(DataSet ds)
        {
            var sb = new StringBuilder();
            var sw = new StringWriter(sb);
            ds.WriteXml(sw, XmlWriteMode.WriteSchema);
            RecordsetXml = sb.ToString();
        }

        /// <summary>
        /// An instance of the TOPUser class matching the UserID.
        /// </summary>
        public virtual TOPUser TOPUser { get; set; }
    }
}