﻿using System.Collections.Generic;
using System.Linq;
using Envoc.Core.Shared.Extensions;
using Envoc.Core.Shared.Model;

namespace Lib.TOPControls
{
    /// <summary>
    /// The set of values describing an System within the job.
    /// </summary>
    public class ProjectSystem : DomainJobObject, ISortable
    {
        private List<ProjectSystem> children = new List<ProjectSystem>();

        /// <summary>
        /// Creates an instance fo the ProjectSystem class with default values.
        /// </summary>
        public ProjectSystem()
            : base()
        {
            Description = string.Empty;
        }

        /// <summary>
        /// As string value identifying this system within the job.
        /// </summary>
        public string SystemNumber { get; set; }

        /// <summary>
        /// An optional string value that provides a description of the system
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return SystemNumber; }
        }

        /// <summary>
        /// The unique identifier of a system's parent node. A null value represents a root node
        /// </summary>
        public long? ParentId { get; set; }

        /// <summary>
        /// The order in which the system is displayed relative to its siblings
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        /// A collection of systems that are children of the current system
        /// </summary>
        public IEnumerable<ProjectSystem> Children
        {
            get { return children; }
        }

        /// <summary>
        /// Add an existing ProjectSystem to the Children collection
        /// </summary>
        /// <param name="child"></param>
        public void AddChild(ProjectSystem child)
        {
            children.Add(child);
        }

        /// <summary>
        /// Returns true when the Chilren collection contains one or more item
        /// </summary>
        /// <returns></returns>
        public bool HasChildren()
        {
            return Children.IsNotNull() && Children.Any();
        }

        /// <summary>
        /// Returns a flattened list of then current ProjectSystem object and its children.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectSystem> GetFlattenedBranch()
        {
            return new[] {this}
                .Concat(Children.SelectMany(child => child.GetFlattenedBranch()));
        }

        public ProjectSystem Clone()
        {
            var item = new ProjectSystem()
            {
                ID = this.ID,
                JobID = this.JobID,
                SystemNumber = this.SystemNumber,
                Description = this.Description,
                ParentId = this.ParentId,
                SortOrder = this.SortOrder
            };
            return item;
        }
    }
}