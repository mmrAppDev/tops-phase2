﻿using System;

namespace Lib.TOPControls.Security
{
    /// <summary>
    /// User settings for authentication
    /// </summary>
    public class Credential : DomainObject
    {
        /// <summary>
        /// The ID of the related TOPUser
        /// </summary>
        public virtual long UserID { get; set; }

        /// <summary>
        /// The string the user will use to authenticate
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// The password the user will use to authenticate.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets/sets a value indicating if the user will use Active Directory to authenticate.
        /// </summary>
        public bool UseADAuthentication { get; set; }

        /// <summary>
        /// Indicates whether a user is approved.
        /// </summary>
        public bool IsApproved { get; set; }

        /// <summary>
        /// Indicates whether a user has been locked out of the system.
        /// </summary>
        public bool IsLockedOut { get; set; }

        /// <summary>
        /// Date credential info was created.
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Last date the user was recorded as logging in.
        /// </summary>
        public DateTime LastLogin { get; set; }

        /// <summary>
        /// A question to use when dealing with users that cannot remember a password they use all the time, in order to help
        /// them remember a string they almost never use.
        /// </summary>
        public string PasswordQuestion { get; set; }

        /// <summary>
        /// A string that is meant to be the second line of security if a user refuses to provide the correct password.  Meant to be
        /// a response to the PasswordQuestion.
        /// </summary>
        public string PasswordAnswer { get; set; }

        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return Login; }
        }

        /// <summary>
        /// An instance of the related TOPUser
        /// </summary>
        public virtual TOPUser TOPUser { get; set; }
    }
}