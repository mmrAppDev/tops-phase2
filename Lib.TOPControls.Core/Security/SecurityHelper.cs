﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Security
{
    /// <summary>
    /// Provides some elemental operations for managing security.
    /// </summary>
    public class SecurityHelper
    {
        /// <summary>
        /// Obfuscates text
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public static string EncryptString(string src)
        {
            return src;   
        }

        /// <summary>
        /// De-obfuscates text
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public static string DecryptString(string src)
        {
            return src;
        }

    }
}
