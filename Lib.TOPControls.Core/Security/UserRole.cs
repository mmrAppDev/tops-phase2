﻿namespace Lib.TOPControls.Security
{
    /// <summary>
    /// Represents a connection between a single TOPUser and a single Role.
    /// </summary>
    public class UserRole : DomainObject
    {
        /// <summary>
        /// Gets/Sets the ID of TOPUser
        /// </summary>
        public virtual long UserID { get; set; }

        /// <summary>
        /// Gets/Sets the ID of the Role
        /// </summary>
        public virtual long RoleID { get; set; }

        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return string.Format("{0} - {1}", TOPUser.Login, Role.RoleName); }
        }

        /// <summary>
        /// The related TOPUser.
        /// </summary>
        public virtual TOPUser TOPUser { get; set; }

        /// <summary>
        /// The related Role.
        /// </summary>
        public virtual Role Role { get; set; }
    }
}