﻿using System.Collections.Generic;

namespace Lib.TOPControls.Security
{
    /// <summary>
    /// Represents a Role to use within the application.
    /// </summary>
    public class Role : DomainObject
    {
        /// <summary>
        /// Name to use for this role.
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// Description of what this role is.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return RoleName; }
        }

        /// <summary>
        /// Connections to the TOPUser objects that have this role.
        /// </summary>
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}