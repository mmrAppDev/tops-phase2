﻿namespace Lib.TOPControls
{
    /// <summary>
    /// A base class for DomainObjects that belong to a specific JobInfo item.
    /// </summary>
    public abstract class DomainJobObject : DomainObject
    {
        /// <summary>
        /// The database ID of the JobInfo record that this item belongs to.
        /// </summary>
        public virtual long JobID { get; set; }

        /// <summary>
        /// The JobInfo record to which this item belongs.
        /// </summary>
        public virtual JobInfo JobInfo { get; set; }
    }
}