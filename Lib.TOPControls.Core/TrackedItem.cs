﻿using System.Collections.Generic;

namespace Lib.TOPControls
{
    /// <summary>
    /// Stores the values used on each individually tracked item in the system.
    /// </summary>
    public class TrackedItem : DomainJobObject, ISoftDeleted
    {
        /// <summary>
        /// A string that identifies this item within the job.  Cannot be null.
        /// </summary>
        public string TrackingID { get; set; }

        /// <summary>
        /// If present, returns the ID of the ObjectType of this item.
        /// </summary>
        public virtual long? ObjectTypeID { get; set; }

        /// <summary>
        /// If present, returns the ID of the ProjectSystem of this item.
        /// </summary>
        public virtual long? SystemID { get; set; }

        /// <summary>
        /// If present, returns the ID of the ProjectArea of this item.
        /// </summary>
        public virtual long? AreaID { get; set; }

        /// <summary>
        /// If present, returns the ID of the CraftInfo for this item.
        /// </summary>
        public virtual long? CraftID { get; set; }

        /// <summary>
        /// The value of User Defined Property 1.
        /// </summary>
        public string UDPValue1 { get; set; }

        /// <summary>
        /// The value of User Defined Property 2.
        /// </summary>
        public string UDPValue2 { get; set; }

        /// <summary>
        /// The value of User Defined Property 3.
        /// </summary>
        public string UDPValue3 { get; set; }

        /// <summary>
        /// A boolean value that indicates whether the underlying database record has been marked as deleted.
        /// </summary>
        public bool IsDeleted { get; private set; }

        /// <summary>
        /// The ObjectType associated with this item.
        /// </summary>
        public virtual ObjectType ObjectType { get; set; }

        /// <summary>
        /// The ProjectSystem associated with this item.
        /// </summary>
        public virtual ProjectSystem ProjectSystem { get; set; }

        /// <summary>
        /// The ProjectArea associated with this item.
        /// </summary>
        public virtual ProjectArea ProjectArea { get; set; }

        /// <summary>
        /// The CraftInfo associated with this item.
        /// </summary>
        public virtual CraftInfo CraftInfo { get; set; }

        /// <summary>
        /// A set of Task Progress items for this item.
        /// </summary>
        public virtual ICollection<TaskProgress> Tasks { get; set; }

        /// <summary>
        /// Sets the status of the IsDeleted property of this instance.
        /// </summary>
        /// <param name="isDeleted">The true or false value to set on the IsDeleted property.</param>
        public void SetDeletedState(bool isDeleted)
        {
            IsDeleted = isDeleted;
        }

        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return TrackingID; }
        }

        /// <summary>
        /// The comments associated with this item.
        /// </summary>
        public string Comments { get; set; }
    }
}