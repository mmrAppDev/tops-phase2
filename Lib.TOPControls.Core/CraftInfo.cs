﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls
{
    /// <summary>
    /// Contains the values related to a specific Craft for this job.
    /// </summary>
    public class CraftInfo:DomainJobObject
    {

        /// <summary>
        /// A string value that identifies this Craft within the job.
        /// </summary>
        public string CraftCode { get; set; }

        /// <summary>
        /// A short description of the Craft represented by this item.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return CraftCode; }
        }

    }
}
