﻿using System;

namespace Lib.TOPControls
{
    /// <summary>
    /// The set of values defining a particular change in TaskProgress percent complete, and the 
    /// values of related items in the application when the change occured.
    /// </summary>
    public class ProgressLogEntry : DomainObject
    {
        /// <summary>
        /// The Date/Time this entry was originally added to the system.
        /// </summary>
        public DateTime EntryDate { get; set; }

        /// <summary>
        /// The date this work was performed.
        /// </summary>
        public DateTime? WorkDate { get; set; }

        /// <summary>
        /// The ID of the Job this entry belongs to.
        /// </summary>
        public virtual long JobID { get; set; }

        /// <summary>
        /// The Title of the Job
        /// </summary>
        public string JobName { get; set; }

        /// <summary>
        /// The ID of the TOPUser that created this entry.
        /// </summary>
        public virtual long UserID { get; set; }

        /// <summary>
        /// The login username of the TOPuser that created this entry.
        /// </summary>
        public string EntryUsername { get; set; }

        /// <summary>
        /// The First Name of the TOPUser that created this entry, if available.
        /// </summary>
        public string EntryUserFirstName { get; set; }

        /// <summary>
        /// The First Name of the TOPUser that created this entry, if available.
        /// </summary>
        public string EntryUserLastName { get; set; }

        /// <summary>
        /// Gets the calculated full name of the TOPUser that created this entry.
        /// </summary>
        public string EntryUserFullName
        {
            get
            {
                return (EntryUserFirstName + ' ' + EntryUserLastName).Trim();
            }
        }

        /// <summary>
        /// The ID of the Commodity associated with the Task Progress.
        /// </summary>
        public virtual long? CommodityID { get; set; }

        /// <summary>
        /// The Code for the logged commodity.
        /// </summary>
        public string CommodityCode { get; set; }

        /// <summary>
        /// A description of the commodity being tracked.
        /// </summary>
        public string CommodityDescription { get; set; }

        /// <summary>
        /// The ID of the Task that is associated with this progress entry.
        /// </summary>
        public virtual long? TaskID { get; set; }

        /// <summary>
        /// The Title of the Task that is associated with this progress entry.
        /// </summary>
        public string TaskTitle { get; set; }

        /// <summary>
        /// The ID of the TaskProgress item that is updated.
        /// </summary>
        public virtual long? TaskProgressID { get; set; }

        /// <summary>
        /// The manhours currently estimated for the item being progressed.
        /// </summary>
        public decimal? TaskManHours { get; set; }

        /// <summary>
        /// The ID of the Phase the TaskProgress item is associated with.
        /// </summary>
        public virtual long? PhaseID { get; set; }

        /// <summary>
        /// The title of the associated ProjectPhase at the time of this progress entry.
        /// </summary>
        public string PhaseTitle { get; set; }

        /// <summary>
        /// The ID of the TrackedItem that the TaskProgress item belongs to.
        /// </summary>
        public virtual long? TrackedItemID { get; set; }

        /// <summary>
        /// The user-facing TrackingID of the TrackedItem this entry relates to.
        /// </summary>
        public string TrackingID { get; set; }

        /// <summary>
        /// The ID of the TrackedItem ObjectType this entry relates to.
        /// </summary>
        public virtual long? ObjectTypeID { get; set; }

        /// <summary>
        /// The Name of the Object Type of the TrackedItem for this entry.
        /// </summary>
        public string ObjectTypeName { get; set; }

        /// <summary>
        /// The ID of the ProjectSystem associated with the TrackedItem for this entry.
        /// </summary>
        public virtual long? ProjectSystemID { get; set; }

        /// <summary>
        /// The Number of the ProjectSystem associated with the TrackedItem for this entry.
        /// </summary>
        public string ProjectSystemNumber { get; set; }

        /// <summary>
        /// The ID of the ProjectArea associated with the TrackedItem for this entry.
        /// </summary>
        public virtual long? ProjectAreaID { get; set; }

        /// <summary>
        /// The Number of the ProjectArea associated with the TrackedItem for this entry.
        /// </summary>
        public string ProjectAreaNumber { get; set; }

        /// <summary>
        /// The ID of the CraftInfo associated with this entry's TrackedItem.
        /// </summary>
        public virtual long? CraftInfoID { get; set; }

        /// <summary>
        /// The Code of the CraftInfo associated with this entry's TrackedItem.
        /// </summary>
        public string CraftCode { get; set; }

        /// <summary>
        /// The Name of the First User Defined Property.
        /// </summary>
        public string UDP1_Name { get; set; }

        /// <summary>
        /// The Value of the First User Defined Property for this entry.
        /// </summary>
        public string UDP1_Value { get; set; }

        /// <summary>
        /// The Name of the Second User Defined Property.
        /// </summary>
        public string UDP2_Name { get; set; }

        /// <summary>
        /// The Value of the Second User Defined Property for this entry.
        /// </summary>
        public string UDP2_Value { get; set; }

        /// <summary>
        /// The Name of the Third User Defined Property.
        /// </summary>
        public string UDP3_Name { get; set; }

        /// <summary>
        /// The Value of the Third User Defined Property for this entry.
        /// </summary>
        public string UDP3_Value { get; set; }

        /// <summary>
        /// The ID of the WorkCrew responsible for this entry.
        /// </summary>
        public virtual long? WorkCrewID { get; set; }

        /// <summary>
        /// The Number of the WorkCrew responsible for this entry.
        /// </summary>
        public int? WorkCrewNumber { get; set; }

        /// <summary>
        /// The Work Crew's name or description.
        /// </summary>
        public string WorkCrewDescription { get; set; }

        /// <summary>
        /// The PercentComplete value of the TaskProgress item before the change logged in this entry.
        /// </summary>
        public decimal? OldPercentComplete { get; set; }

        /// <summary>
        /// The PercentComplete value that is set on the Task Progress item.
        /// </summary>
        public decimal? NewPercentComplete { get; set; }

        /// <summary>
        /// If present, represents the TaskProgress item that this entry relates to.
        /// </summary>
        public virtual TaskProgress TaskProgress { get; set; }

        /// <summary>
        /// If present, gets the user that created this entry.
        /// </summary>
        public virtual TOPUser TOPUser { get; set; }

        /// <summary>
        /// If present, gets the WorkCrew that triggered this progress.
        /// </summary>
        public virtual WorkCrew WorkCrew { get; set; }


        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return ID.ToString(); }
        }
    }
}