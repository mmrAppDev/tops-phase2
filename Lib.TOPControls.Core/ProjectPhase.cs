﻿using System.Collections.Generic;

namespace Lib.TOPControls
{
    /// <summary>
    /// The set of values associated with a specific phase within a job and all of its tasks.
    /// </summary>
    public class ProjectPhase : DomainJobObject, IDisplayOrdered
    {
        /// <summary>
        /// The Title to use for this phase.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Represents the relative position of this Project Phase within the phases for this job. Must be an integer that is greater than zero.
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets the name used to describe this item to the user.
        /// </summary>
        public override string DisplayName
        {
            get { return Title; }
        }

        /// <summary>
        /// The set of Task items related to this ProjectPhase item.
        /// </summary>
        public virtual ICollection<Task> Tasks { get; set; }
    }
}