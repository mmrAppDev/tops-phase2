﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Lib.TOPControls
{
    /// <summary>
    /// Contains pairs of values that define filters to be placed on the set of TaskProgress items returned to the
    /// presentation layer.
    /// </summary>
    public class FilterParameters : Dictionary<FilterValueType, object>
    {
        /// <summary>
        /// Creates a new instance of FilterParameters with no filters applied.
        /// </summary>
        public FilterParameters()
            : base()
        {
        }

        /// <summary>
        /// Creates a new instance of FilterParameters using the Dictionary passed to define the starting set
        /// of filters.
        /// </summary>
        /// <param name="source"></param>
        public FilterParameters(Dictionary<FilterValueType, object> source)
            : base(source)
        {
        }

        /// <summary>
        /// Sets a single value of the identified type as a filter for a FilterValueType.
        /// </summary>
        /// <typeparam name="T">The type of the filter value to use.  Must match the related value of the
        /// filterType passed.</typeparam>
        /// <param name="filterType">A FilterValueType value identifying the type of values to compare to the value
        /// passed.</param>
        /// <param name="value">A single instance of the type, that will be used to select specific sets of values from
        /// the database.</param>
        /// <returns></returns>
        public IEnumerable<T> SetFilterValue<T>(FilterValueType filterType, T value)
        {
            var tempList = new List<T>();
            tempList.Add(value);
            if (this.ContainsKey(filterType))
            {
                this[filterType] = tempList;
            }
            else
            {
                Add(filterType, tempList);
            }

            return (IEnumerable<T>)this[filterType];
        }

        /// <summary>
        /// Sets an IEnumerable of type T as a filter for a FilterValueType.
        /// </summary>
        /// <typeparam name="T">The type of the filter value to use.  Must match the related value of the
        /// filterType passed.</typeparam>
        /// <param name="filterType">A FilterValueType value identifying the type of data to compare to the values
        /// passed.</param>
        /// <param name="valueSet">An IEnumerable of values to be used as filters against the filterType.</param>
        /// <returns></returns>
        public IEnumerable<T> SetFilterValues<T>(FilterValueType filterType, IEnumerable<T> valueSet)
        {
            if (this.ContainsKey(filterType))
            {
                this[filterType] = valueSet;
            }
            else
            {
                Add(filterType, valueSet);
            }
            return (IEnumerable<T>)this[filterType];
        }

        /// <summary>
        /// Adds a single value of type T to the set of values for a FilterValueType.
        /// </summary>
        /// <typeparam name="T">The type of the filter value to use.  Must match the related value of the
        /// filterType passed.</typeparam>
        /// <param name="filterType">A FilterValueType value identifying the type of data to compare to the values
        /// passed.</param>
        /// <param name="value">A single value that will be appended to the existing set of values for this filterType.</param>
        /// <returns></returns>
        public IEnumerable<T> AddFilterValue<T>(FilterValueType filterType, T value)
        {
            if (!IsValidValueType(filterType, value.GetType())) throw new ArgumentException(string.Format("Filter value type {0} requires a {1} value but a {2} was passed.", filterType.ToString(), GetRelatedType(filterType).Name, value.GetType().Name));

            if (this.ContainsKey(filterType))
            {
                var current = this[filterType];
                if (current == null)
                {
                    var tempList = new List<T>();
                    tempList.Add(value);
                    this[filterType] = tempList;
                }
                if (current.GetType().Equals(typeof(T)))
                {
                    this[filterType] = new List<T>(new T[] { (T)current, value });
                }
                else
                {
                    var currentList = current as IEnumerable<T>;
                    List<T> tempList;
                    if (currentList != null)
                    {
                        tempList = new List<T>(currentList);
                    }
                    else
                    {
                        tempList = new List<T>();
                    }
                    tempList.Add(value);
                    this[filterType] = tempList;
                }
                return (IEnumerable<T>)this[filterType];
            }
            else
            {
                return SetFilterValue<T>(filterType, value);
            }
        }

        /// <summary>
        /// Removes a single value of type T from the set of values for a FilterValueType.
        /// </summary>
        /// <typeparam name="T">The type of the filter value to use.  Must match the related value of the
        /// filterType passed.</typeparam>
        /// <param name="filterType">A FilterValueType value identifying the type of data to compare to the values
        /// passed.</param>
        /// <param name="value">A single value that will be appended to the existing set of values for this filterType.</param>
        /// <returns></returns>
        public IEnumerable<T> RemoveFilterValue<T>(FilterValueType filterType, T value)
        {
            IEnumerable<T> result = null;

            if (!IsValidValueType(filterType, value.GetType())) throw new ArgumentException(string.Format("Filter value type {0} requires a {1} value but a {2} was passed.", filterType.ToString(), GetRelatedType(filterType).Name, value.GetType().Name));

            if (this.ContainsKey(filterType))
            {
                var valueList = this[filterType] as IEnumerable<T>;
                if (valueList != null)
                {
                    if (valueList.Contains(value))
                    {
                        var valTypeList = valueList as List<T>;
                        if (valTypeList == null) valTypeList = new List<T>(valueList);
                        valTypeList.Remove(value);
                        if (valTypeList.Count > 0)
                        {
                            this[filterType] = valTypeList;
                            result = valTypeList;
                        }
                        else
                        {
                            Remove(filterType);
                        }
                    }
                    else
                    {
                        result = this[filterType] as IEnumerable<T>;
                    }
                }
                else
                {
                    Remove(filterType);
                }
            }
            return result;
        }

        /// <summary>
        /// Adds an ID to the ObjectType ID values selected. Returns a list of all current values.
        /// </summary>
        /// <param name="objectTypeID">ID of the ObjectType.</param>
        /// <returns></returns>
        public IEnumerable<long> AddObjectTypeParameter(long objectTypeID)
        {
            return AddFilterValue<long>(FilterValueType.ObjectType, objectTypeID);
        }

        /// <summary>
        /// Adds an ID to the ProjectSystem ID values selected. Returns a list of all current values.
        /// </summary>
        /// <param name="systemID">ID of a ProjectSystem</param>
        /// <returns></returns>
        public IEnumerable<long> AddProjectSystemParameter(long systemID)
        {
            return AddFilterValue<long>(FilterValueType.ProjectSystem, systemID);
        }

        /// <summary>
        /// Adds an ID to the ProjectArea ID values selected. Returns a list of all current values.
        /// </summary>
        /// <param name="areaID">ID of a ProjectArea</param>
        /// <returns></returns>
        public IEnumerable<long> AddProjectAreaParameter(long areaID)
        {
            return AddFilterValue<long>(FilterValueType.ProjectArea, areaID);
        }

        /// <summary>
        /// Adds an ID to the CraftInfo ID values selected. Returns a list of all current values.
        /// </summary>
        /// <param name="craftID"></param>
        /// <returns></returns>
        public IEnumerable<long> AddCraftInfoParameter(long craftID)
        {
            return AddFilterValue<long>(FilterValueType.CraftInfo, craftID);
        }

        /// <summary>
        /// Adds an ID to the WorkCrew ID values selected. Returns a list of all current values.
        /// </summary>
        /// <param name="workCrewID">ID of a WorkCrew</param>
        /// <returns></returns>
        public IEnumerable<long> AddWorkCrewParameter(long workCrewID)
        {
            return AddFilterValue<long>(FilterValueType.WorkCrew, workCrewID);
        }

        /// <summary>
        /// Adds an ID to the Commodity values selected. Returns a list of all current values.
        /// </summary>
        /// <param name="commodityID">ID of the commodity.</param>
        /// <returns></returns>
        public IEnumerable<long> AddCommodityParameter(long commodityID)
        {
            return AddFilterValue<long>(FilterValueType.Commodity, commodityID);
        }

        /// <summary>
        /// Adds an ID to the ProjectPhase values selected. Returns a list of all current values.
        /// </summary>
        /// <param name="phaseID">ID of the phase.</param>
        /// <returns></returns>
        public IEnumerable<long> AddPhaseParameter(long phaseID)
        {
            return AddFilterValue<long>(FilterValueType.Phase, phaseID);
        }

        /// <summary>
        /// Adds an ID the Task values selected. Returns a list of all current values.
        /// </summary>
        /// <param name="taskID">ID of the task</param>
        /// <returns></returns>
        public IEnumerable<long> AddTaskParameter(long taskID)
        {
            return AddFilterValue<long>(FilterValueType.Task, taskID);
        }

        /// <summary>
        /// Adds a Tracking ID to the Tracking IDs selected. Returns a list of all current values.
        /// </summary>
        /// <param name="trackingID">The Tracking ID value of the TrackedItem.</param>
        /// <returns></returns>
        public IEnumerable<string> AddTrackingIDParameter(string trackingID)
        {
            return AddFilterValue<string>(FilterValueType.TrackingID, trackingID);
        }

        /// <summary>
        /// Adds a string value to the list values matched in User Defined Property 1. Returns a list of all current values.
        /// </summary>
        /// <param name="udpValue">The string value to match.</param>
        /// <returns></returns>
        public IEnumerable<string> AddUDP_1_Parameter(string udpValue)
        {
            return AddFilterValue<string>(FilterValueType.UserDefinedProperty1, udpValue);
        }

        /// <summary>
        /// Adds a string value to the list values matched in User Defined Property 2. Returns a list of all current values.
        /// </summary>
        /// <param name="udpValue">The string value to match.</param>
        /// <returns></returns>
        public IEnumerable<string> AddUDP_2_Parameter(string udpValue)
        {
            return AddFilterValue<string>(FilterValueType.UserDefinedProperty2, udpValue);
        }

        /// <summary>
        /// Adds a string value to the list values matched in User Defined Property 3. Returns a list of all current values.
        /// </summary>
        /// <param name="udpValue">The string value to match.</param>
        /// <returns></returns>
        public IEnumerable<string> AddUDP_3_Parameter(string udpValue)
        {
            return AddFilterValue<string>(FilterValueType.UserDefinedProperty3, udpValue);
        }

        /// <summary>
        /// Sets the search term to use to select values to return. Returns a list of all current values.
        /// </summary>
        /// <param name="searchTerm">The string value to match. Will perform a partial match.</param>
        /// <returns></returns>
        public IEnumerable<string> AddSearchParameter(string searchTerm)
        {
            return SetFilterValue<string>(FilterValueType.Search, searchTerm);
        }

        /// <summary>
        /// Adds a CompletionState value to the values selected. Returns a list of all current values.
        /// </summary>
        /// <param name="state">A value of the CompletionState enumeration.</param>
        /// <returns></returns>
        public IEnumerable<CompletionState> AddCompletionStateParameter(CompletionState state)
        {
            return AddFilterValue<CompletionState>(FilterValueType.CompletionState, state);
        }

        /// <summary>
        /// Removes a exact string from the list of Tracking ID values selected. Returns a list of all remaining values.
        /// </summary>
        /// <param name="trackingID">The exact string to remove.</param>
        /// <returns></returns>
        public IEnumerable<string> RemoveTrackingIDParameter(string trackingID)
        {
            return RemoveFilterValue<string>(FilterValueType.TrackingID, trackingID);
        }

        /// <summary>
        /// Removes an ID from the list of ObjectType ID values selected. Returns a list of all remaining values.
        /// </summary>
        /// <param name="objectTypeID">ID of the ObjectType</param>
        /// <returns></returns>
        public IEnumerable<long> RemoveObjectTypeParameter(long objectTypeID)
        {
            return RemoveFilterValue<long>(FilterValueType.ObjectType, objectTypeID);
        }

        /// <summary>
        /// Removes an ID from the list of ProjectSystem ID values selected. Returns a list of all remaining values.
        /// </summary>
        /// <param name="systemID">ID of the ProjectSystem.</param>
        /// <returns></returns>
        public IEnumerable<long> RemoveProjectSystemParameter(long systemID)
        {
            return RemoveFilterValue<long>(FilterValueType.ProjectSystem, systemID);
        }

        /// <summary>
        /// Removes an ID from the list of ProjectArea ID values selected.  Returns a list of all remaining values.
        /// </summary>
        /// <param name="areaID">ID of the ProjectArea</param>
        /// <returns></returns>
        public IEnumerable<long> RemoveProjectAreaParameter(long areaID)
        {
            return RemoveFilterValue<long>(FilterValueType.ProjectArea, areaID);
        }

        /// <summary>
        /// Removes an ID from the list of CraftInfo ID values selected.  Returns a list of all remaining values.
        /// </summary>
        /// <param name="craftID">ID of the CraftInfo</param>
        /// <returns></returns>
        public IEnumerable<long> RemoveCraftInfoParameter(long craftID)
        {
            return RemoveFilterValue<long>(FilterValueType.CraftInfo, craftID);
        }

        /// <summary>
        /// Removes an exact string from the list of User Defined Property 1 values selected.  Returns a list of all remaining values.
        /// </summary>
        /// <param name="udpValue">The string to remove from the list.</param>
        /// <returns></returns>
        public IEnumerable<string> RemoveUDP_1_Parameter(string udpValue)
        {
            return RemoveFilterValue<string>(FilterValueType.UserDefinedProperty1, udpValue);
        }

        /// <summary>
        /// Removes an exact string from the list of User Defined Property 2 values selected.  Returns a list of all remaining values.
        /// </summary>
        /// <param name="udpValue">The string to remove from the list.</param>
        /// <returns></returns>
        public IEnumerable<string> RemoveUDP_2_Parameter(string udpValue)
        {
            return RemoveFilterValue<string>(FilterValueType.UserDefinedProperty2, udpValue);
        }

        /// <summary>
        /// Removes an exact string from the list of User Defined Property 3 values selected.  Returns a list of all remaining values.
        /// </summary>
        /// <param name="udpValue">The string to remove from the list.</param>
        /// <returns></returns>
        public IEnumerable<string> RemoveUDP_3_Parameter(string udpValue)
        {
            return RemoveFilterValue<string>(FilterValueType.UserDefinedProperty3, udpValue);
        }

        /// <summary>
        /// Removes an ID from the list of Commodity ID values selected.  Returns a list of all remaining values.
        /// </summary>
        /// <param name="commodityID">ID of the Commodity.</param>
        /// <returns></returns>
        public IEnumerable<long> RemoveCommodityParameter(long commodityID)
        {
            return RemoveFilterValue<long>(FilterValueType.Commodity, commodityID);
        }

        /// <summary>
        /// Removes an ID from the list of WorkCrew ID values selected.  Returns a list of all remaining values.
        /// </summary>
        /// <param name="workCrewID">ID of the WorkCrew.</param>
        /// <returns></returns>
        public IEnumerable<long> RemoveWorkCrewParameter(long workCrewID)
        {
            return RemoveFilterValue<long>(FilterValueType.WorkCrew, workCrewID);
        }

        /// <summary>
        /// Removes a CompletionState value from the list of CompletionStates selected.  Returns a list of all remaining values.
        /// </summary>
        /// <param name="state">The CompletionState to remove.</param>
        /// <returns></returns>
        public IEnumerable<CompletionState> RemoveCompletionStateParameter(CompletionState state)
        {
            return RemoveFilterValue<CompletionState>(FilterValueType.CompletionState, state);
        }

        /// <summary>
        /// Removes an ID from the list of ProjectPhase ID values selected.   Returns a list of all remaining values.
        /// </summary>
        /// <param name="phaseID">ID of the ProjectPhase.</param>
        /// <returns></returns>
        public IEnumerable<long> RemovePhaseParameter(long phaseID)
        {
            return RemoveFilterValue<long>(FilterValueType.Phase, phaseID);
        }

        /// <summary>
        /// Removes an ID from the list of Task ID values selected.  Returns a list of all remaining values.
        /// </summary>
        /// <param name="taskID">ID of the Task</param>
        /// <returns></returns>
        public IEnumerable<long> RemoveTaskParameter(long taskID)
        {
            return RemoveFilterValue<long>(FilterValueType.Task, taskID);
        }

        /// <summary>
        /// Removes a specific search term from the FilterParameters.  Returns a list of all remaining values.
        /// </summary>
        /// <param name="searchTerm">Search term to remove.</param>
        /// <returns></returns>
        public IEnumerable<string> RemoveSearchParameter(string searchTerm)
        {
            return RemoveFilterValue<string>(FilterValueType.Search, searchTerm);
        }

        /// <summary>
        /// Returns a true if the datattype passed is the related type for the FilterValueType passed.
        /// </summary>
        /// <param name="filterType">FilterValueType to use.</param>
        /// <param name="dataType">System.Type to check</param>
        /// <returns></returns>
        public static bool IsValidValueType(FilterValueType filterType, Type dataType)
        {
            return (dataType.Equals(GetRelatedType(filterType)));
        }

        /// <summary>
        /// Returns the System.Type that is related to the FilterValueType value passed.
        /// </summary>
        /// <param name="filterType">An item from the FilterValueType enumeration.</param>
        /// <returns></returns>
        public static Type GetRelatedType(FilterValueType filterType)
        {
            FieldInfo field = filterType.GetType().GetField(filterType.ToString());
            RelatedTypeAttribute[] attributes = (RelatedTypeAttribute[])field.GetCustomAttributes(typeof(RelatedTypeAttribute), false);
            return attributes[0].TargetType;
        }

        /// <summary>
        /// Returns a Type identifying the Domain Type associated with the filter type passed.
        /// </summary>
        /// <param name="filterType">A valuing from the FilterValueType enumeration.</param>
        /// <returns></returns>
        public static Type GetRelatedDomainType(FilterValueType filterType)
        {
            switch (filterType)
            {
                case FilterValueType.None:
                    return typeof(object);
                case FilterValueType.TrackingID:
                    return typeof(TrackedItem);
                case FilterValueType.ObjectType:
                    return typeof(ObjectType);
                case FilterValueType.ProjectSystem:
                    return typeof(ProjectSystem);
                case FilterValueType.ProjectArea:
                    return typeof(ProjectArea);
                case FilterValueType.CraftInfo:
                    return typeof(CraftInfo);
                case FilterValueType.UserDefinedProperty1:
                    return typeof(object);
                case FilterValueType.UserDefinedProperty2:
                    return typeof(object);
                case FilterValueType.UserDefinedProperty3:
                    return typeof(object);
                case FilterValueType.Commodity:
                    return typeof(Commodity);
                case FilterValueType.WorkCrew:
                    return typeof(WorkCrew);
                case FilterValueType.CompletionState:
                    return typeof(CompletionState);
                case FilterValueType.Phase:
                    return typeof(ProjectPhase);
                case FilterValueType.Task:
                    return typeof(Task);
                case FilterValueType.Search:
                    return typeof(object);
                default:
                    return typeof(object);
            }



        }

        /// <summary>
        /// Creates a new FilterParameters instance with a members that are identical to this instance.
        /// </summary>
        /// <returns></returns>
        public FilterParameters Clone()
        {
            return new FilterParameters(this);
        }
    }
}