﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls.Permissions
{
    /// <summary>
    /// Sets the contract for permission managers used to validate user rights.
    /// </summary>
    public interface IPermissionManager
    {
        /// <summary>
        /// Identifies whether a user action is valid according to their permissions.
        /// </summary>
        /// <param name="action">A UserActionType value to validate.</param>
        /// <param name="target">A target to validate. For many operations, an unaltered new instance of the 
        /// target type is sufficient.</param>
        /// <param name="userID">The ID of the user performing the operation.</param>
        /// <returns></returns>
        bool IsValidUserAction(UserActionType action, DomainObject target, long userID);
    }
}
