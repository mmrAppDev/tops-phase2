﻿namespace Lib.TOPControls.Permissions
{
    /// <summary>
    /// A value that identifies a value that can be used to filter a list of results when displaying
    /// Task Progress items.
    /// </summary>
    public enum UserActionType
    {
        /// <summary>
        /// Viewing an item or displaying the item in a report.
        /// </summary>
        View,

        /// <summary>
        /// Adding an instance of the type to the database.
        /// </summary>
        Insert,

        /// <summary>
        /// Changing the property values of an item in the database.
        /// </summary>
        Update,

        /// <summary>
        /// Deleting an item in the database.
        /// </summary>
        Delete,

        /// <summary>
        /// Viewing database items that are marked as deleted, but not destroyed.
        /// </summary>
        ViewDeleted,

        /// <summary>
        /// Restoring to a non-deleted state an item that is marked as deleted.
        /// </summary>
        Undelete,

        /// <summary>
        /// Permanently removing an item from the database that is marked as deleted.
        /// </summary>
        Destroy,

        /// <summary>
        /// A specific user action that cannot be represented by other User Action Types.
        /// </summary>
        CustomAction
    }
}