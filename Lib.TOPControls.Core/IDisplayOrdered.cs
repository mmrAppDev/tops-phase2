﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.TOPControls
{
    /// <summary>
    /// The basic interface for domain object items with the DiplayOrder property.
    /// </summary>
    public interface IDisplayOrdered
    {
        /// <summary>
        /// The ID of the object.
        /// </summary>
        long ID { get; set; }

        /// <summary>
        /// Gets/Sets an integer value indicating this item's position within a display list of siblings.
        /// </summary>
        int DisplayOrder { get; set; }
    }
}
