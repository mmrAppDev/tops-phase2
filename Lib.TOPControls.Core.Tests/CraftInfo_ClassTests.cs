﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.Tests
{
    [TestClass]
    public class CraftInfo_ClassTests : DomainJobObject_BaseTests<CraftInfo>
    {
        [TestMethod]
        public void CraftInfoConstructor_SetsDefaultValues()
        {
            string expectedCraftCode = null;
            string expectedDescription = null;

            var actual = createDefaultInstance();

            Assert.AreEqual(expectedCraftCode, actual.CraftCode);
            Assert.AreEqual(expectedDescription, actual.Description);
        }
    }
}