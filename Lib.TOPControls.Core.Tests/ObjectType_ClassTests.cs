﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.Tests
{
    [TestClass]
    public class ObjectType_ClassTests : DomainJobObject_BaseTests<ObjectType>
    {
        [TestMethod]
        public void ObjectTypeConstructor_SetsDefaultValues()
        {
            string expectedName = string.Empty;
            string expectedDescription = string.Empty;

            var actual = createDefaultInstance();

            Assert.AreEqual(expectedName, actual.Name);
            Assert.AreEqual(expectedDescription, actual.Description);
        }
    }
}