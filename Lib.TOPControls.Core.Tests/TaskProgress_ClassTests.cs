﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.Tests
{
    [TestClass]
    public class TaskProgress_ClassTests
    {
        protected virtual TaskProgress createDefaultInstance()
        {
            return new TaskProgress();
        }

        protected virtual TaskProgress createDefaultInstance(decimal percentComplete)
        {
            var result = new TaskProgress();
            result.SetPercentComplete(percentComplete);
            return result;
        }

        [TestMethod]
        public void TaskProgressConstructor_CreatesInstance()
        {
            var actual = createDefaultInstance();
            Assert.IsInstanceOfType(actual, typeof(TaskProgress));
        }

        [TestMethod]
        public void TaskProgressConstructor_SetsDefaultValues()
        {
            var expectedID = 0;
            var expectedTaskID = 0;
            long? expectedCommodityID = null;
            var expectedTrackedItemID = 0;
            var expectedPercentComplete = 0;
            var expectedLastChangedMinimum = DateTime.Now;

            var actual = createDefaultInstance();

            Assert.AreEqual(expectedID, actual.ID);
            Assert.AreEqual(expectedTaskID, actual.TaskID);
            Assert.AreEqual(expectedCommodityID, actual.CommodityID);
            Assert.AreEqual(expectedTrackedItemID, actual.TrackedItemID);
            Assert.AreEqual(expectedPercentComplete, actual.PercentComplete);
            Assert.IsTrue(expectedLastChangedMinimum <= actual.LastChanged);

            Assert.IsNull(actual.Task);
            Assert.IsNull(actual.Commodity);
            Assert.IsNull(actual.TrackedItem);
        }

        [TestMethod]
        public void TaskProgress_GetCompletionState_CompleteOnPercentComplete1()
        {
            // Test to be sure that the CompletionState is Complete when PercentComplete is 100%
            var expectedCompletionState = CompletionState.Complete;
            var expectedPercentComplete = 1m;
            AssertCompletionResult(expectedCompletionState, expectedPercentComplete);
        }

        // Test to be sure that the CompletionSTate is NotStarted when the percent complete is zero.
        [TestMethod]
        public void TaskProgress_GetCompletionState_NotStartedOnPercentComplete0()
        {
            AssertCompletionResult(CompletionState.NotStarted, 0m);
        }

        [TestMethod]
        public void TaskProgress_GetCompletionState_PartiallyCompleteBetween0And1()
        {
            decimal percVal = 0.000001m;
            var expCS = CompletionState.PartiallyComplete;

            AssertCompletionResult(expCS, percVal);

            for (decimal percStep = 0.01m; percStep < 1m; percStep += 0.01m)
            {
                AssertCompletionResult(expCS, percStep);
            }
        }

        // Test the PercentCompleteAsOf(Date) method with an artificially generated set of data.

        [TestMethod]
        public void TaskProgress_PercentCompleteAsOf_ReturnsCurrentState_NoPLE()
        {
            var target = createDefaultInstance();
            target.ProgressLogEntries = new List<ProgressLogEntry>();

            var expectedPC = target.PercentComplete;
            var actualPC = target.PercentCompleteAsOf(DateTime.Parse("5/30/2013"));

            Assert.AreEqual(expectedPC, actualPC);


            target.SetPercentComplete(0.33m);

            expectedPC = target.PercentComplete;
            actualPC = target.PercentCompleteAsOf(DateTime.Parse("5/30/2013"));

            Assert.AreEqual(expectedPC, actualPC);

            target.SetPercentComplete(0.66m);

            expectedPC = target.PercentComplete;
            actualPC = target.PercentCompleteAsOf(DateTime.Parse("5/30/2013"));

            Assert.AreEqual(expectedPC, actualPC);

            target.SetPercentComplete(1m);

            expectedPC = target.PercentComplete;
            actualPC = target.PercentCompleteAsOf(DateTime.Parse("5/30/2013"));

            Assert.AreEqual(expectedPC, actualPC);
        }


        [TestMethod]
        public void TaskProgress_PercentCompleteAsOf_ReturnsCurrentState_BeforeFirstDateReturnsFirstOldPercent()
        {
            var target = createDefaultInstance(0.66m);
            target.ProgressLogEntries = buildProgressHistory(target);

            var expectedPC = target.ProgressLogEntries.Single(ple => ple.WorkDate == DateTime.Parse("5/1/2013")).OldPercentComplete.Value;
            var actualPC = target.PercentCompleteAsOf(DateTime.Parse("4/15/2013"));

            Assert.AreEqual(expectedPC, actualPC);

            // change the first oldPC value and try it again.

            var ple1 = target.ProgressLogEntries.Single(ple => ple.WorkDate == DateTime.Parse("5/1/2013"));
            ple1.OldPercentComplete = 0.22m;

            expectedPC = target.ProgressLogEntries.Single(ple => ple.WorkDate == DateTime.Parse("5/1/2013")).OldPercentComplete.Value;
            actualPC = target.PercentCompleteAsOf(DateTime.Parse("4/15/2013"));

            Assert.AreEqual(expectedPC, actualPC);

        }

        [TestMethod]
        public void TaskProgress_PercentCompleteAsOf_ReturnsCurrentState_OnOrAfterFirstDateReturnsFirstNewPercent()
        {
            var target = createDefaultInstance(0.66m);
            target.ProgressLogEntries = buildProgressHistory(target);

            var expectedPC = target.ProgressLogEntries.Single(ple => ple.WorkDate == DateTime.Parse("5/1/2013")).NewPercentComplete.Value;
            var actualPC = target.PercentCompleteAsOf(DateTime.Parse("5/1/2013"));

            Assert.AreEqual(expectedPC, actualPC);

            // change the first oldPC value and try it again.

            expectedPC = target.ProgressLogEntries.Single(ple => ple.WorkDate == DateTime.Parse("5/1/2013")).NewPercentComplete.Value;
            actualPC = target.PercentCompleteAsOf(DateTime.Parse("5/2/2013"));

            Assert.AreEqual(expectedPC, actualPC);
        }

        [TestMethod]
        public void TaskProgress_PercentCompleteAsOf_ReturnsCurrentState_OnOrAfterSecondDateReturnsSecondNewPercent()
        {
            var target = createDefaultInstance(0.66m);
            target.ProgressLogEntries = buildProgressHistory(target);

            var expectedPC = target.ProgressLogEntries.Single(ple => ple.WorkDate == DateTime.Parse("5/15/2013")).NewPercentComplete.Value;
            var actualPC = target.PercentCompleteAsOf(DateTime.Parse("5/15/2013"));

            Assert.AreEqual(expectedPC, actualPC);

            // change the first oldPC value and try it again.

            expectedPC = target.ProgressLogEntries.Single(ple => ple.WorkDate == DateTime.Parse("5/15/2013")).NewPercentComplete.Value;
            actualPC = target.PercentCompleteAsOf(DateTime.Parse("5/17/2013"));

            Assert.AreEqual(expectedPC, actualPC);
        }
        
        
        private ICollection<ProgressLogEntry> buildProgressHistory(TaskProgress target)
        {
            var result = new List<ProgressLogEntry>();
            decimal increment = 0.33m;
            // if the percent complete is > 0, use a varying number of steps to get there.
            if (target.PercentComplete > 0m)
            {
                // use two dates:  5/1/2013, 5/15/2013
                var ple1 = new ProgressLogEntry
                {
                    ID = 44,
                    TaskProgressID = target.ID,
                    OldPercentComplete = 0m,
                    NewPercentComplete = target.PercentComplete / 2m,
                    WorkDate = DateTime.Parse("5/1/2013")
                };
                result.Add(ple1);
                var pc1 = ple1.NewPercentComplete;
                var ple2 = new ProgressLogEntry
                {
                    ID = 583,
                    TaskProgressID = target.ID,
                    OldPercentComplete = pc1,
                    NewPercentComplete = target.PercentComplete,
                    WorkDate = DateTime.Parse("5/15/2013")
                };
                result.Add(ple2);
            }
            else
            {
                // use two dates:  5/1/2013, 5/15/2013
                var ple1 = new ProgressLogEntry
                {
                    ID = 44,
                    TaskProgressID = target.ID,
                    OldPercentComplete = 0m,
                    NewPercentComplete = increment,
                    WorkDate = DateTime.Parse("5/1/2013")
                };
                result.Add(ple1);
                var pc1 = ple1.NewPercentComplete;
                var ple2 = new ProgressLogEntry
                {
                    ID = 583,
                    TaskProgressID = target.ID,
                    OldPercentComplete = pc1,
                    NewPercentComplete = target.PercentComplete,
                    WorkDate = DateTime.Parse("5/15/2013")
                };
                result.Add(ple2);
            }

            return result;
        }

        #region AbstractedAssertProcess

        private void AssertCompletionResult(CompletionState expectedCompletionState, decimal testedPercentComplete)
        {
            var target = createDefaultInstance();
            
            target.SetPercentComplete(testedPercentComplete);

            var actualCompletionState = target.GetCompletionState();

            Assert.AreEqual(expectedCompletionState, actualCompletionState);
        }

        #endregion AbstractedAssertProcess
    }
}