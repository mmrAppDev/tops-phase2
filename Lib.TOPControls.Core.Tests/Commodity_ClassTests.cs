﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.Tests
{
    [TestClass]
    public class Commodity_ClassTests : DomainJobObject_BaseTests<Commodity>
    {
        [TestMethod]
        public void CommodityConstructor_SetsDefaultValues()
        {
            string expectedCommodityCode = null;
            string expectedDescription = null;
            decimal expectedManHoursEstmated = 0;

            var actual = createDefaultInstance();

            Assert.AreEqual(expectedCommodityCode, actual.CommodityCode);
            Assert.AreEqual(expectedDescription, actual.Description);
            Assert.AreEqual(expectedManHoursEstmated, actual.ManHoursEstimated);
        }
    }
}