﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.Tests
{
    [TestClass]
    public class ProjectPhase_ClassTests : DomainJobObject_BaseTests<ProjectPhase>
    {
        [TestMethod]
        public void ProjectPhaseConstructor_SetsDefaultValues()
        {
            string expectedTitle = null;
            var expectedDisplayOrder = 0;

            var actual = createDefaultInstance();

            Assert.AreEqual(expectedTitle, actual.Title);
            Assert.AreEqual(expectedDisplayOrder, actual.DisplayOrder);
        }
    }
}