﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.Tests
{
    [TestClass]
    public class Task_ClassTests : DomainJobObject_BaseTests<Task>
    {
        [TestMethod]
        public void TaskContructor_SetsDefaultValues()
        {
            string expectedTitle = null;
            var expectedDisplayOrder = 0;

            var actual = createDefaultInstance();

            Assert.AreEqual(expectedTitle, actual.Title);
            Assert.AreEqual(expectedDisplayOrder, actual.DisplayOrder);
        }


        [TestMethod()]
        public void Task_DefaultCommodityID_PropertyUnit1()
        {
            var target = new Task();

            // Default value is null
            Assert.IsNull(target.DefaultCommodityID);

            long? expectedValue = 1;
            long? actualValue;

            // Test simple assignment
            target.DefaultCommodityID = expectedValue;
            actualValue = target.DefaultCommodityID;
            Assert.AreEqual(expectedValue, actualValue, "Simple assignment");

            // Test Edge case assignment
            expectedValue = long.MinValue;
            target.DefaultCommodityID = expectedValue;
            actualValue = target.DefaultCommodityID;
            Assert.AreEqual(expectedValue, actualValue, "Min value assignment");

            expectedValue = long.MaxValue;
            target.DefaultCommodityID = expectedValue;
            actualValue = target.DefaultCommodityID;
            Assert.AreEqual(expectedValue, actualValue, "Max value assignment");

            expectedValue = null;
            target.DefaultCommodityID = expectedValue;
            actualValue = target.DefaultCommodityID;
            Assert.AreEqual(expectedValue, actualValue, "Null value assignment");

            expectedValue = 0;
            target.DefaultCommodityID = expectedValue;
            actualValue = target.DefaultCommodityID;
            Assert.AreEqual(expectedValue, actualValue, "Zero value assignment");
        }

    }
}