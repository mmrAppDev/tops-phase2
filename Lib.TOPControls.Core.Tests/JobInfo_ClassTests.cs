﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.Tests
{
    [TestClass]
    public class JobInfo_ClassTests
    {
        [TestMethod]
        public void JobInfoConstructor_CreatesInstance()
        {
            var actual = new JobInfo();
            Assert.IsInstanceOfType(actual, typeof(JobInfo));
        }

        [TestMethod]
        public void JobInfoConstructor_SetsDefaultValues()
        {
            var expectedID = 0;
            string expectedTitle = null;
            string expectedUDPName1 = null;
            string expectedUDPName2 = null;
            string expectedUDPName3 = null;
            var expectedLastChangedMinimum = DateTime.Now;
            var expectedIsDeleted = false;

            var actual = new JobInfo();

            Assert.AreEqual(expectedID, actual.ID);
            Assert.AreEqual(expectedTitle, actual.Title);
            Assert.AreEqual(expectedUDPName1, actual.UDPName1);
            Assert.AreEqual(expectedUDPName2, actual.UDPName2);
            Assert.AreEqual(expectedUDPName3, actual.UDPName3);
            Assert.IsTrue(expectedLastChangedMinimum <= actual.LastChanged);
            Assert.AreEqual(expectedIsDeleted, actual.IsDeleted);
        }
    }
}