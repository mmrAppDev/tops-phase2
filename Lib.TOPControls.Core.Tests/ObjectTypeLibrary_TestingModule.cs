﻿using Lib.TOPControls.Permissions;
using Lib.TOPControls.Tests.LibraryTests.Mock;
using MMRCommon.Data;
using Ninject.Modules;

namespace Lib.TOPControls.Tests
{
    public class ObjectTypeLibrary_TestingModule<ObType> : NinjectModule where ObType : DomainObject, new()
    {
        public override void Load()
        {
            Bind<IRepository<ObType>>().To<RepositoryMocker<ObType>>();
            Bind<IPermissionManager>().To<PermissionManagerMocker>();
        }
    }
}