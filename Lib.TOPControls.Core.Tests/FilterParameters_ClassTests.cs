﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.Tests
{
    [TestClass]
    public class FilterParameters_ClassTests
    {
        [TestMethod]
        public void FilterParameters_GetRelatedType()
        {
            var expected = typeof(long);
            var actual = FilterParameters.GetRelatedType(FilterValueType.Commodity);

            Assert.AreEqual(expected, actual);

            expected = typeof(CompletionState);
            actual = FilterParameters.GetRelatedType(FilterValueType.CompletionState);

            Assert.AreEqual(expected, actual);

            expected = typeof(long);
            actual = FilterParameters.GetRelatedType(FilterValueType.CraftInfo);

            Assert.AreEqual(expected, actual);

            expected = typeof(long);
            actual = FilterParameters.GetRelatedType(FilterValueType.ObjectType);

            Assert.AreEqual(expected, actual);

            expected = typeof(long);
            actual = FilterParameters.GetRelatedType(FilterValueType.Phase);

            Assert.AreEqual(expected, actual);

            expected = typeof(long);
            actual = FilterParameters.GetRelatedType(FilterValueType.ProjectArea);

            Assert.AreEqual(expected, actual);

            expected = typeof(long);
            actual = FilterParameters.GetRelatedType(FilterValueType.ProjectSystem);

            Assert.AreEqual(expected, actual);

            expected = typeof(string);
            actual = FilterParameters.GetRelatedType(FilterValueType.Search);

            Assert.AreEqual(expected, actual);

            expected = typeof(long);
            actual = FilterParameters.GetRelatedType(FilterValueType.Task);

            Assert.AreEqual(expected, actual);

            expected = typeof(string);
            actual = FilterParameters.GetRelatedType(FilterValueType.TrackingID);

            Assert.AreEqual(expected, actual);

            expected = typeof(string);
            actual = FilterParameters.GetRelatedType(FilterValueType.UserDefinedProperty1);

            Assert.AreEqual(expected, actual);

            expected = typeof(string);
            actual = FilterParameters.GetRelatedType(FilterValueType.UserDefinedProperty2);

            Assert.AreEqual(expected, actual);

            expected = typeof(string);
            actual = FilterParameters.GetRelatedType(FilterValueType.UserDefinedProperty3);

            Assert.AreEqual(expected, actual);

            expected = typeof(long);
            actual = FilterParameters.GetRelatedType(FilterValueType.WorkCrew);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void FilterParameters_AddObjParam_Adds1ToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddObjectTypeParameter(expectedID1);

            var result = target[FilterValueType.ObjectType];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());
        }

        [TestMethod]
        public void FilterParameters_AddObjParam_AddsSecondItemToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddObjectTypeParameter(expectedID1);

            var result = target[FilterValueType.ObjectType];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());

            var expectedID2 = (long)482391;

            target.AddObjectTypeParameter(expectedID2);

            expectedCount = 2;

            result = target[FilterValueType.ObjectType];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            actualList = result as IEnumerable<long>;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));
            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveObjParam_RemovesFirstItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddObjectTypeParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddObjectTypeParameter(expectedID2);

            target.RemoveObjectTypeParameter(expectedID1);

            var result = target[FilterValueType.ObjectType];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsFalse(actualList.Contains(expectedID1));

            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveObjParam_RemovesLastItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddObjectTypeParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddObjectTypeParameter(expectedID2);

            target.RemoveObjectTypeParameter(expectedID2);

            var result = target[FilterValueType.ObjectType];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));

            Assert.IsFalse(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_AddSysParam_Adds1ToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddProjectSystemParameter(expectedID1);

            var result = target[FilterValueType.ProjectSystem];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());
        }

        [TestMethod]
        public void FilterParameters_AddSysParam_AddsSecondItemToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddProjectSystemParameter(expectedID1);

            var result = target[FilterValueType.ProjectSystem];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());

            var expectedID2 = (long)482391;

            target.AddProjectSystemParameter(expectedID2);

            expectedCount = 2;

            result = target[FilterValueType.ProjectSystem];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            actualList = result as IEnumerable<long>;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));
            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveSysParam_RemovesFirstItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddProjectSystemParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddProjectSystemParameter(expectedID2);

            target.RemoveProjectSystemParameter(expectedID1);

            var result = target[FilterValueType.ProjectSystem];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsFalse(actualList.Contains(expectedID1));

            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveSysParam_RemovesLastItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddProjectSystemParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddProjectSystemParameter(expectedID2);

            target.RemoveProjectSystemParameter(expectedID2);

            var result = target[FilterValueType.ProjectSystem];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));

            Assert.IsFalse(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_AddAreaParam_Adds1ToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddProjectAreaParameter(expectedID1);

            var result = target[FilterValueType.ProjectArea];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());
        }

        [TestMethod]
        public void FilterParameters_AddAreaParam_AddsSecondItemToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddProjectAreaParameter(expectedID1);

            var result = target[FilterValueType.ProjectArea];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());

            var expectedID2 = (long)482391;

            target.AddProjectAreaParameter(expectedID2);

            expectedCount = 2;

            result = target[FilterValueType.ProjectArea];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            actualList = result as IEnumerable<long>;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));
            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveAreaParam_RemovesFirstItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddProjectAreaParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddProjectAreaParameter(expectedID2);

            target.RemoveProjectAreaParameter(expectedID1);

            var result = target[FilterValueType.ProjectArea];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsFalse(actualList.Contains(expectedID1));

            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveAreaParam_RemovesLastItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddProjectAreaParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddProjectAreaParameter(expectedID2);

            target.RemoveProjectAreaParameter(expectedID2);

            var result = target[FilterValueType.ProjectArea];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));

            Assert.IsFalse(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_AddCraftParam_Adds1ToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddCraftInfoParameter(expectedID1);

            var result = target[FilterValueType.CraftInfo];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());
        }

        [TestMethod]
        public void FilterParameters_AddCraftParam_AddsSecondItemToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddCraftInfoParameter(expectedID1);

            var result = target[FilterValueType.CraftInfo];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());

            var expectedID2 = (long)482391;

            target.AddCraftInfoParameter(expectedID2);

            expectedCount = 2;

            result = target[FilterValueType.CraftInfo];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            actualList = result as IEnumerable<long>;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));
            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveCraftParam_RemovesFirstItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddCraftInfoParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddCraftInfoParameter(expectedID2);

            target.RemoveCraftInfoParameter(expectedID1);

            var result = target[FilterValueType.CraftInfo];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsFalse(actualList.Contains(expectedID1));

            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveCraftParam_RemovesLastItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddCraftInfoParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddCraftInfoParameter(expectedID2);

            target.RemoveCraftInfoParameter(expectedID2);

            var result = target[FilterValueType.CraftInfo];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));

            Assert.IsFalse(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_AddCrewParam_Adds1ToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddWorkCrewParameter(expectedID1);

            var result = target[FilterValueType.WorkCrew];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());
        }

        [TestMethod]
        public void FilterParameters_AddCrewParam_AddsSecondItemToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddWorkCrewParameter(expectedID1);

            var result = target[FilterValueType.WorkCrew];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());

            var expectedID2 = (long)482391;

            target.AddWorkCrewParameter(expectedID2);

            expectedCount = 2;

            result = target[FilterValueType.WorkCrew];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            actualList = result as IEnumerable<long>;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));
            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveCrewParam_RemovesFirstItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddWorkCrewParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddWorkCrewParameter(expectedID2);

            target.RemoveWorkCrewParameter(expectedID1);

            var result = target[FilterValueType.WorkCrew];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsFalse(actualList.Contains(expectedID1));

            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveCrewParam_RemovesLastItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddWorkCrewParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddWorkCrewParameter(expectedID2);

            target.RemoveWorkCrewParameter(expectedID2);

            var result = target[FilterValueType.WorkCrew];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));

            Assert.IsFalse(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_AddCommodityParam_Adds1ToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddCommodityParameter(expectedID1);

            var result = target[FilterValueType.Commodity];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());
        }

        [TestMethod]
        public void FilterParameters_AddCommodityParam_AddsSecondItemToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddCommodityParameter(expectedID1);

            var result = target[FilterValueType.Commodity];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());

            var expectedID2 = (long)482391;

            target.AddCommodityParameter(expectedID2);

            expectedCount = 2;

            result = target[FilterValueType.Commodity];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            actualList = result as IEnumerable<long>;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));
            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveCommodityParam_RemovesFirstItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddCommodityParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddCommodityParameter(expectedID2);

            target.RemoveCommodityParameter(expectedID1);

            var result = target[FilterValueType.Commodity];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsFalse(actualList.Contains(expectedID1));

            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveCommodityParam_RemovesLastItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddCommodityParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddCommodityParameter(expectedID2);

            target.RemoveCommodityParameter(expectedID2);

            var result = target[FilterValueType.Commodity];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));

            Assert.IsFalse(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_AddPhaseParam_Adds1ToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddPhaseParameter(expectedID1);

            var result = target[FilterValueType.Phase];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());
        }

        [TestMethod]
        public void FilterParameters_AddPhaseParam_AddsSecondItemToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddPhaseParameter(expectedID1);

            var result = target[FilterValueType.Phase];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());

            var expectedID2 = (long)482391;

            target.AddPhaseParameter(expectedID2);

            expectedCount = 2;

            result = target[FilterValueType.Phase];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            actualList = result as IEnumerable<long>;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));
            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemovePhaseParam_RemovesFirstItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddPhaseParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddPhaseParameter(expectedID2);

            target.RemovePhaseParameter(expectedID1);

            var result = target[FilterValueType.Phase];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsFalse(actualList.Contains(expectedID1));

            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemovePhaseParam_RemovesLastItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddPhaseParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddPhaseParameter(expectedID2);

            target.RemovePhaseParameter(expectedID2);

            var result = target[FilterValueType.Phase];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));

            Assert.IsFalse(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_AddTaskParam_Adds1ToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddTaskParameter(expectedID1);

            var result = target[FilterValueType.Task];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());
        }

        [TestMethod]
        public void FilterParameters_AddTaskParam_AddsSecondItemToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddTaskParameter(expectedID1);

            var result = target[FilterValueType.Task];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            var actualList = result as IEnumerable<long>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());

            var expectedID2 = (long)482391;

            target.AddTaskParameter(expectedID2);

            expectedCount = 2;

            result = target[FilterValueType.Task];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));

            actualList = result as IEnumerable<long>;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));
            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveTaskParam_RemovesFirstItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddTaskParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddTaskParameter(expectedID2);

            target.RemoveTaskParameter(expectedID1);

            var result = target[FilterValueType.Task];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsFalse(actualList.Contains(expectedID1));

            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveTaskParam_RemovesLastItem()
        {
            var target = new FilterParameters();

            var expectedID1 = (long)143;

            target.AddTaskParameter(expectedID1);
            var expectedID2 = (long)482391;

            target.AddTaskParameter(expectedID2);

            target.RemoveTaskParameter(expectedID2);

            var result = target[FilterValueType.Task];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<long>));
            var actualList = result as IEnumerable<long>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));

            Assert.IsFalse(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_AddTrackingIDParam_Adds1ToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = "XXX-ID1";

            target.AddTrackingIDParameter(expectedID1);

            var result = target[FilterValueType.TrackingID];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));

            var actualList = result as IEnumerable<string>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());
        }

        [TestMethod]
        public void FilterParameters_AddTrackingIDParam_AddsSecondItemToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = "GGG-3881";

            target.AddTrackingIDParameter(expectedID1);

            var result = target[FilterValueType.TrackingID];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));

            var actualList = result as IEnumerable<string>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());

            var expectedID2 = "GRAX-00-3818";

            target.AddTrackingIDParameter(expectedID2);

            expectedCount = 2;

            result = target[FilterValueType.TrackingID];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));

            actualList = result as IEnumerable<string>;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));
            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveTrackingIDParam_RemovesFirstItem()
        {
            var target = new FilterParameters();

            var expectedID1 = "BOZO-RESPAWN";

            target.AddTrackingIDParameter(expectedID1);
            var expectedID2 = "NONE";

            target.AddTrackingIDParameter(expectedID2);

            target.RemoveTrackingIDParameter(expectedID1);

            var result = target[FilterValueType.TrackingID];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));
            var actualList = result as IEnumerable<string>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsFalse(actualList.Contains(expectedID1));

            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveTrackingIDParam_RemovesLastItem()
        {
            var target = new FilterParameters();

            var expectedID1 = "143";

            target.AddTrackingIDParameter(expectedID1);
            var expectedID2 = "482391";

            target.AddTrackingIDParameter(expectedID2);

            target.RemoveTrackingIDParameter(expectedID2);

            var result = target[FilterValueType.TrackingID];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));
            var actualList = result as IEnumerable<string>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));

            Assert.IsFalse(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_AddUDP_1_Param_Adds1ToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = "XXX-ID1";

            target.AddUDP_1_Parameter(expectedID1);

            var result = target[FilterValueType.UserDefinedProperty1];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));

            var actualList = result as IEnumerable<string>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());
        }

        [TestMethod]
        public void FilterParameters_AddUDP_1_Param_AddsSecondItemToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = "GGG-3881";

            target.AddUDP_1_Parameter(expectedID1);

            var result = target[FilterValueType.UserDefinedProperty1];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));

            var actualList = result as IEnumerable<string>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());

            var expectedID2 = "GRAX-00-3818";

            target.AddUDP_1_Parameter(expectedID2);

            expectedCount = 2;

            result = target[FilterValueType.UserDefinedProperty1];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));

            actualList = result as IEnumerable<string>;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));
            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveUDP_1_Param_RemovesFirstItem()
        {
            var target = new FilterParameters();

            var expectedID1 = "BOZO-RESPAWN";

            target.AddUDP_1_Parameter(expectedID1);
            var expectedID2 = "NONE";

            target.AddUDP_1_Parameter(expectedID2);

            target.RemoveUDP_1_Parameter(expectedID1);

            var result = target[FilterValueType.UserDefinedProperty1];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));
            var actualList = result as IEnumerable<string>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsFalse(actualList.Contains(expectedID1));

            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveUDP_1_Param_RemovesLastItem()
        {
            var target = new FilterParameters();

            var expectedID1 = "143";

            target.AddUDP_1_Parameter(expectedID1);
            var expectedID2 = "482391";

            target.AddUDP_1_Parameter(expectedID2);

            target.RemoveUDP_1_Parameter(expectedID2);

            var result = target[FilterValueType.UserDefinedProperty1];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));
            var actualList = result as IEnumerable<string>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));

            Assert.IsFalse(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_AddUDP_2_Param_Adds1ToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = "XXX-ID1";

            target.AddUDP_2_Parameter(expectedID1);

            var result = target[FilterValueType.UserDefinedProperty2];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));

            var actualList = result as IEnumerable<string>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());
        }

        [TestMethod]
        public void FilterParameters_AddUDP_2_Param_AddsSecondItemToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = "GGG-3881";

            target.AddUDP_2_Parameter(expectedID1);

            var result = target[FilterValueType.UserDefinedProperty2];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));

            var actualList = result as IEnumerable<string>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());

            var expectedID2 = "GRAX-00-3818";

            target.AddUDP_2_Parameter(expectedID2);

            expectedCount = 2;

            result = target[FilterValueType.UserDefinedProperty2];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));

            actualList = result as IEnumerable<string>;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));
            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveUDP_2_Param_RemovesFirstItem()
        {
            var target = new FilterParameters();

            var expectedID1 = "BOZO-RESPAWN";

            target.AddUDP_2_Parameter(expectedID1);
            var expectedID2 = "NONE";

            target.AddUDP_2_Parameter(expectedID2);

            target.RemoveUDP_2_Parameter(expectedID1);

            var result = target[FilterValueType.UserDefinedProperty2];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));
            var actualList = result as IEnumerable<string>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsFalse(actualList.Contains(expectedID1));

            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveUDP_2_Param_RemovesLastItem()
        {
            var target = new FilterParameters();

            var expectedID1 = "143";

            target.AddUDP_2_Parameter(expectedID1);
            var expectedID2 = "482391";

            target.AddUDP_2_Parameter(expectedID2);

            target.RemoveUDP_2_Parameter(expectedID2);

            var result = target[FilterValueType.UserDefinedProperty2];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));
            var actualList = result as IEnumerable<string>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));

            Assert.IsFalse(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_AddUDP_3_Param_Adds1ToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = "XXX-ID1";

            target.AddUDP_3_Parameter(expectedID1);

            var result = target[FilterValueType.UserDefinedProperty3];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));

            var actualList = result as IEnumerable<string>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());
        }

        [TestMethod]
        public void FilterParameters_AddUDP_3_Param_AddsSecondItemToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = "GGG-3881";

            target.AddUDP_3_Parameter(expectedID1);

            var result = target[FilterValueType.UserDefinedProperty3];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));

            var actualList = result as IEnumerable<string>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());

            var expectedID2 = "GRAX-00-3818";

            target.AddUDP_3_Parameter(expectedID2);

            expectedCount = 2;

            result = target[FilterValueType.UserDefinedProperty3];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));

            actualList = result as IEnumerable<string>;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));
            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveUDP_3_Param_RemovesFirstItem()
        {
            var target = new FilterParameters();

            var expectedID1 = "BOZO-RESPAWN";

            target.AddUDP_3_Parameter(expectedID1);
            var expectedID2 = "NONE";

            target.AddUDP_3_Parameter(expectedID2);

            target.RemoveUDP_3_Parameter(expectedID1);

            var result = target[FilterValueType.UserDefinedProperty3];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));
            var actualList = result as IEnumerable<string>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsFalse(actualList.Contains(expectedID1));

            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveUDP_3_Param_RemovesLastItem()
        {
            var target = new FilterParameters();

            var expectedID1 = "143";

            target.AddUDP_3_Parameter(expectedID1);
            var expectedID2 = "482391";

            target.AddUDP_3_Parameter(expectedID2);

            target.RemoveUDP_3_Parameter(expectedID2);

            var result = target[FilterValueType.UserDefinedProperty3];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));
            var actualList = result as IEnumerable<string>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));

            Assert.IsFalse(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_AddSearchParam_Adds1ToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = "XXX-ID1";

            target.AddSearchParameter(expectedID1);

            var result = target[FilterValueType.Search];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));

            var actualList = result as IEnumerable<string>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());
        }

        [TestMethod]
        public void FilterParameters_RemoveSearchParam_RemovesFirstItem()
        {
            var target = new FilterParameters();

            var expectedID1 = "BOZO-RESPAWN";

            target.AddSearchParameter(expectedID1);
            var expectedID2 = "NONE";

            target.AddSearchParameter(expectedID2);

            target.RemoveSearchParameter(expectedID1);

            var result = target[FilterValueType.Search];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<string>));
            var actualList = result as IEnumerable<string>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsFalse(actualList.Contains(expectedID1));
        }

        [TestMethod]
        public void FilterParameters_AddCompletionStateParam_Adds1ToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = CompletionState.Complete;

            target.AddCompletionStateParameter(expectedID1);

            var result = target[FilterValueType.CompletionState];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<CompletionState>));

            var actualList = result as IEnumerable<CompletionState>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());
        }

        [TestMethod]
        public void FilterParameters_AddCompletionStateParam_AddsSecondItemToFilterType()
        {
            var target = new FilterParameters();

            var expectedID1 = CompletionState.NotStarted;

            target.AddCompletionStateParameter(expectedID1);

            var result = target[FilterValueType.CompletionState];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<CompletionState>));

            var actualList = result as IEnumerable<CompletionState>;

            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.AreEqual(expectedID1, actualList.First());

            var expectedID2 = CompletionState.PartiallyComplete;

            target.AddCompletionStateParameter(expectedID2);

            expectedCount = 2;

            result = target[FilterValueType.CompletionState];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<CompletionState>));

            actualList = result as IEnumerable<CompletionState>;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));
            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveCompletionStateParam_RemovesFirstItem()
        {
            var target = new FilterParameters();

            var expectedID1 = CompletionState.Complete;

            target.AddCompletionStateParameter(expectedID1);
            var expectedID2 = CompletionState.NotStarted;

            target.AddCompletionStateParameter(expectedID2);

            target.RemoveCompletionStateParameter(expectedID1);

            var result = target[FilterValueType.CompletionState];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<CompletionState>));
            var actualList = result as IEnumerable<CompletionState>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsFalse(actualList.Contains(expectedID1));

            Assert.IsTrue(actualList.Contains(expectedID2));
        }

        [TestMethod]
        public void FilterParameters_RemoveCompletionStateParam_RemovesLastItem()
        {
            var target = new FilterParameters();

            var expectedID1 = CompletionState.PartiallyComplete;

            target.AddCompletionStateParameter(expectedID1);
            var expectedID2 = CompletionState.Complete;

            target.AddCompletionStateParameter(expectedID2);

            target.RemoveCompletionStateParameter(expectedID2);

            var result = target[FilterValueType.CompletionState];

            Assert.IsInstanceOfType(result, typeof(IEnumerable<CompletionState>));
            var actualList = result as IEnumerable<CompletionState>;
            var expectedCount = 1;

            Assert.AreEqual(expectedCount, actualList.Count());

            Assert.IsTrue(actualList.Contains(expectedID1));

            Assert.IsFalse(actualList.Contains(expectedID2));
        }
    }
}