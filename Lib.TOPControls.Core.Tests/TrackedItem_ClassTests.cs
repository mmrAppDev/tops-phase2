﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.Tests
{
    [TestClass]
    public class TrackedItem_ClassTests : DomainJobObject_BaseTests<TrackedItem>
    {
        [TestMethod]
        public void TrackedItemConstructor_SetsDefaultValues()
        {
            string expectedTagNumber = null;
            long? expectedObjectTypeID = null;
            long? expectedSystemID = null;
            long? expectedAreaID = null;
            long? expectedCraftID = null;
            string expectedUDPValue1 = null;
            string expectedUDPValue2 = null;
            string expectedUDPValue3 = null;
            bool expectedIsDeleted = false;

            var actual = createDefaultInstance();

            Assert.AreEqual(expectedTagNumber, actual.TrackingID);
            Assert.AreEqual(expectedObjectTypeID, actual.ObjectTypeID);
            Assert.AreEqual(expectedSystemID, actual.SystemID);
            Assert.AreEqual(expectedAreaID, actual.AreaID);
            Assert.AreEqual(expectedCraftID, actual.CraftID);
            Assert.AreEqual(expectedUDPValue1, actual.UDPValue1);
            Assert.AreEqual(expectedUDPValue2, actual.UDPValue2);
            Assert.AreEqual(expectedUDPValue3, actual.UDPValue3);
            Assert.AreEqual(expectedIsDeleted, actual.IsDeleted);

            Assert.IsNull(actual.ObjectType);
            Assert.IsNull(actual.ProjectSystem);
            Assert.IsNull(actual.ProjectArea);
            Assert.IsNull(actual.CraftInfo);
        }
    }
}