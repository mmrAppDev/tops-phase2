﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.Tests
{
    [TestClass]
    public class ProjectSystem_ClassTests : DomainJobObject_BaseTests<ProjectSystem>
    {
        [TestMethod]
        public void ProjectSystemConstructor_SetsDefaultValues()
        {
            string expectedSystemNumber = null;

            var actual = createDefaultInstance();

            Assert.AreEqual(expectedSystemNumber, actual.SystemNumber);
        }
    }
}