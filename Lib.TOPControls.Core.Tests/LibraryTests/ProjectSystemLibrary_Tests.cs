﻿using System;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMRCommon.Data;
using Ninject;

namespace Lib.TOPControls.Tests.LibraryTests
{
    [TestClass]
    public class ProjectSystemLibrary_Tests : JobObjectLibrary_BaseTests<ProjectSystem>
    {
        #region OverridesAndBaseClassOverloads

        protected override IObjectLibrary<ProjectSystem> GetTargetLibrary()
        {
            var library = kernel.Get<ProjectSystemLibrary>();
            return library;
        }

        protected override IObjectLibrary<ProjectSystem> GetTargetLibrary(IRepository<ProjectSystem> useRepository)
        {
            return GetTargetAsJObLibrary(useRepository);
        }

        public override IJobObjectLibrary<ProjectSystem> GetTargetAsJObLibrary(IRepository<ProjectSystem> useRepository = null)
        {
            var library = kernel.Get<ProjectSystemLibrary>();
            library.Repository = useRepository;
            return library;
        }

        protected override ProjectSystem GetInvalidObjectInstance(bool asNewItem)
        {
            var result = new ProjectSystem
            {
                ID = (asNewItem ? 0 : 1)
            };
            return result;
        }

        protected override ProjectSystem GetValidObjectInstance(bool asNewItem)
        {
            var result = new ProjectSystem
            {
                ID = (asNewItem ? 0 : 1),
                JobID = 1,
                SystemNumber = "888-888-88888",
                LastChanged = DateTime.Now
            };
            return result;
        }

        #endregion OverridesAndBaseClassOverloads

        #region Tests

        #endregion Tests
    }
}