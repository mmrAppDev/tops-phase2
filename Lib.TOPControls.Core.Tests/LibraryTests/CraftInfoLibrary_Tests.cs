﻿using System;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMRCommon.Data;
using Ninject;

namespace Lib.TOPControls.Tests.LibraryTests
{
    [TestClass]
    public class CraftInfoLibrary_Tests : JobObjectLibrary_BaseTests<CraftInfo>
    {
        #region OverridesAndBaseClassOverloads

        protected override IObjectLibrary<CraftInfo> GetTargetLibrary()
        {
            var library = kernel.Get<CraftInfoLibrary>();
            return library;
        }

        protected override IObjectLibrary<CraftInfo> GetTargetLibrary(IRepository<CraftInfo> useRepository)
        {
            return GetTargetAsJObLibrary(useRepository);
        }

        public override IJobObjectLibrary<CraftInfo> GetTargetAsJObLibrary(IRepository<CraftInfo> useRepository = null)
        {
            var library = kernel.Get<CraftInfoLibrary>();
            library.Repository = useRepository;
            return library;
        }

        protected override CraftInfo GetInvalidObjectInstance(bool asNewItem)
        {
            var result = new CraftInfo
            {
                ID = (asNewItem ? 0 : 1)
            };
            return result;
        }

        protected override CraftInfo GetValidObjectInstance(bool asNewItem)
        {
            var result = new CraftInfo
            {
                ID = (asNewItem ? 0 : 1),
                JobID = 1,
                CraftCode = "888-888-88888",
                Description = "Sample Craft",
                LastChanged = DateTime.Now
            };
            return result;
        }

        #endregion OverridesAndBaseClassOverloads

        #region Tests

        #endregion Tests
    }
}