﻿using System.Linq;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Common;
using Lib.TOPControls.Tests.LibraryTests.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.Tests.LibraryTests
{
    [TestClass]
    public class JobInfoLibrary_Tests : SoftDeleteLibrary_BaseTests<JobInfo>
    {
        #region OverridesAndBaseClassOverloads

        protected override IObjectLibrary<JobInfo> GetTargetLibrary()
        {
            var result = new JobInfoLibrary();
            SetDefaultPermMananger(result);
            return result;
        }

        protected override IObjectLibrary<JobInfo> GetTargetLibrary(MMRCommon.Data.IRepository<JobInfo> useRepository)
        {
            return GetTargetAsSDLibrary(useRepository);
        }

        public override ISoftDeleteLibrary<JobInfo> GetTargetAsSDLibrary(MMRCommon.Data.IRepository<JobInfo> useRepository = null)
        {
            var result = new JobInfoLibrary(useRepository);
            SetDefaultPermMananger(result);
            return result;
        }

        protected override JobInfo GetInvalidObjectInstance(bool asNewItem)
        {
            var result = new JobInfo();

            result.ID = (asNewItem ? 0 : 23);
            result.Title = null;
            return result;
        }

        protected override JobInfo GetValidObjectInstance(bool asNewItem)
        {
            return GetValidObjectInstance(asNewItem);
        }

        protected JobInfo GetValidObjectInstance(bool asNewItem, long useID = 1)
        {
            var result = new JobInfo();

            result.ID = (asNewItem ? 0 : useID);
            result.Title = string.Format("Test Job Title {0}", useID);
            return result;
        }

        #endregion OverridesAndBaseClassOverloads

        #region Tests

        #region ValidateTests

        [TestMethod]
        public void JobInfoLibrary_Validate_TitleTooShort_NewItem()
        {
            var target = GetTargetLibrary();
            var expectedIsValid = false;
            var expectedInvlalidProperty = "Title";

            var subject = new JobInfo();

            subject.ID = 0;
            subject.Title = "T&M";
            var actual = target.Validate(subject);

            Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should not have passed.");

            var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            Assert.IsTrue(errorCountTitle > 0, "No Title violations were returned.");
        }

        [TestMethod]
        public void JobInfoLibrary_Validate_TitleTooLong_NewItem()
        {
            var target = GetTargetLibrary();
            var expectedIsValid = false;
            var expectedInvlalidProperty = "Title";

            var subject = new JobInfo();

            subject.ID = 0;
            subject.Title = "vlZm59iqMPovSq2tuUT2eegHcbi602pmEhcz76lbsuic29nn08l9OhHtEaZc9LV2wBpxXp6NNBQ5wzSWymVAqv10xGkwCEDF25Uyq";

            var actual = target.Validate(subject);

            Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should not have passed.");

            var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            Assert.IsTrue(errorCountTitle > 0, "No Title violations were returned.");
        }

        [TestMethod]
        public void JobInfoLibrary_Validate_TitleTooShort_ExistingItem()
        {
            var subject = GetValidObjectInstance(false);

            var mockData = new Mock.RepositoryMocker<JobInfo>();
            mockData.Insert(subject);

            var target = GetTargetLibrary();
            var expectedIsValid = false;
            var expectedInvlalidProperty = "Title";

            subject.Title = "D7D";

            var actual = target.Validate(subject);

            Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should not have passed.");

            var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            Assert.IsTrue(errorCountTitle > 0, "No Title violations were returned.");
        }

        [TestMethod]
        public void JobInfoLibrary_Validate_TitleTooLong_ExistingItem()
        {
            var subject = GetValidObjectInstance(false);

            var mockData = new Mock.RepositoryMocker<JobInfo>();
            mockData.Insert(subject);

            var target = GetTargetLibrary();
            var expectedIsValid = false;
            var expectedInvlalidProperty = "Title";

            subject.Title = "s5k4iyoVu8rAM1xASKiojbE5e99xKaFRQzywlTb8MLC5fOAKZiIukLoR5kBWZGpvB2LFF4WWFgrpNX6Ttg7zP2nAv6sYKcdv9MDX5";

            var actual = target.Validate(subject);

            Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should not have passed.");

            var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            Assert.IsTrue(errorCountTitle > 0, "No Title violations were returned.");
        }

        [TestMethod]
        public void JobInfoLibrary_Validate_UDPName_TooLong_1()
        {
            var subject = GetValidObjectInstance(false);

            var mockData = new Mock.RepositoryMocker<JobInfo>();
            mockData.Insert(subject);

            var target = GetTargetLibrary();
            var expectedIsValid = false;
            var expectedInvlalidProperty = "UDPName1";

            subject.UDPName1 = "pCgJ5N7GCk8pbklJ1wLZ5wbrjadjLeAOSM6CGaos3TYkO6Fk2gK";

            var actual = target.Validate(subject);

            Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should not have passed.");

            var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            Assert.IsTrue(errorCountTitle > 0, "No {0} violations were returned.", expectedInvlalidProperty);
        }

        [TestMethod]
        public void JobInfoLibrary_Validate_UDPName_TooLong_2()
        {
            var subject = GetValidObjectInstance(false);

            var mockData = new Mock.RepositoryMocker<JobInfo>();
            mockData.Insert(subject);

            var target = GetTargetLibrary();
            var expectedIsValid = false;
            var expectedInvlalidProperty = "UDPName2";

            subject.UDPName2 = "hlbHvKKk9lVMIjcGfdEHt3y6UamOb2ONSm6EDl3WITFBtpBrHju";

            var actual = target.Validate(subject);

            Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should not have passed.");

            var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            Assert.IsTrue(errorCountTitle > 0, "No {0} violations were returned.", expectedInvlalidProperty);
        }

        [TestMethod]
        public void JobInfoLibrary_Validate_UDPName_TooLong_3()
        {
            var subject = GetValidObjectInstance(false);

            var mockData = new Mock.RepositoryMocker<JobInfo>();
            mockData.Insert(subject);

            var target = GetTargetLibrary();
            var expectedIsValid = false;
            var expectedInvlalidProperty = "UDPName3";

            subject.UDPName3 = "4EnBi5Mf2WHsC9jkecDZaj2g5HQ124UeWKy3j05Kx9BCFsfdqd5";

            var actual = target.Validate(subject);

            Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should not have passed.");

            var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            Assert.IsTrue(errorCountTitle > 0, "No {0} violations were returned.", expectedInvlalidProperty);
        }

        [TestMethod]
        public void JobInfoLibrary_Validate_UDPNameSame_1_2()
        {
            var subject = GetValidObjectInstance(true);

            var target = GetTargetLibrary();
            var expectedIsValid = false;
            var expectedInvlalidProperty = "UDPName2";

            subject.UDPName1 = "s5k4iyoVu8rAM1xASKiojbE5e99xKaFRQzywlTb8MLC5fOA";
            subject.UDPName2 = subject.UDPName1;
            subject.UDPName3 = "12345678901234567890123456789012345678901234567";

            var actual = target.Validate(subject);

            Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should not have passed.");

            var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            Assert.IsTrue(errorCountTitle > 0, "No User Defined Property Name violations were returned.");
        }

        [TestMethod]
        public void JobInfoLibrary_Validate_UDPNameSame_2_3()
        {
            var subject = GetValidObjectInstance(true);

            var target = GetTargetLibrary();
            var expectedIsValid = false;
            var expectedInvlalidProperty = "UDPName3";

            subject.UDPName1 = "12345678901234567890123456789012345678901234567";
            subject.UDPName2 = "s5k4iyoVu8rAM1xASKiojbE5e99xKaFRQzywlTb8MLC5fOA";
            subject.UDPName3 = subject.UDPName2;

            var actual = target.Validate(subject);

            Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should not have passed.");

            var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            Assert.IsTrue(errorCountTitle > 0, "No User Defined Property Name violations were returned.");
        }

        [TestMethod]
        public void JobInfoLibrary_Validate_UDPNameSame_1_3()
        {
            var subject = GetValidObjectInstance(true);

            var target = GetTargetLibrary();
            var expectedIsValid = false;
            var expectedInvlalidProperty = "UDPName3";

            subject.UDPName1 = "s5k4iyoVu8rAM1xASKiojbE5e99xKaFRQzywlTb8MLC5fOA";
            subject.UDPName2 = "12345678901234567890123456789012345678901234567";
            subject.UDPName3 = subject.UDPName1;

            var actual = target.Validate(subject);

            Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should not have passed.");

            var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            Assert.IsTrue(errorCountTitle > 0, "No User Defined Property Name violations were returned.");
        }

        [TestMethod]
        public void JobInfoLibrary_Validate_UDPNameSame_All()
        {
            var subject = GetValidObjectInstance(true);

            var target = GetTargetLibrary();
            var expectedIsValid = false;
            var expectedInvlalidProperty = "UDPName2";

            subject.UDPName1 = "8l9OhHtEaZc9LV2wBpxXp6NNBQ5wzSWymVAqv10";
            subject.UDPName2 = subject.UDPName1;
            subject.UDPName3 = subject.UDPName2;

            var actual = target.Validate(subject);

            Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should not have passed.");

            var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            Assert.IsTrue(errorCountTitle > 0, "No User Defined Property Name violations were returned.");
        }

        [TestMethod]
        public void JobInfoLibrary_Validate_NewJobWithDuplicateName()
        {
            var existing = GetValidObjectInstance(false);
            existing.Title = "Existing Job Title";
            var subject = GetValidObjectInstance(true);
            subject.Title = existing.Title;
            var jobData = new RepositoryMocker<JobInfo>();

            existing.ID = jobData.GetNextID();

            jobData.items.Add(existing);

            var target = GetTargetLibrary(jobData);
            var expectedIsValid = false;
            var expectedInvlalidProperty = "Title";

            var actual = target.Validate(subject);

            Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should have failed for title existing in repository.");

            var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            Assert.IsTrue(errorCountTitle > 0, "No violations were returned for {0}.", expectedInvlalidProperty);
        }

        [TestMethod]
        public void JobInfoLibrary_Validate_ExistingJobWithDuplicateName()
        {
            var TitleToDuplicate = "FirstItemTitle";
            var first = GetValidObjectInstance(false);
            first.Title = TitleToDuplicate;
            var second = GetValidObjectInstance(false);
            second.Title = "Second Item Title";
            var jobData = new RepositoryMocker<JobInfo>();

            first.ID = jobData.GetNextID();
            var firstID = first.ID;
            jobData.items.Add(first);
            second.ID = jobData.GetNextID();
            var secondID = second.ID;
            jobData.items.Add(second);

            var target = GetTargetLibrary(jobData);
            var expectedIsValid = false;
            var expectedInvlalidProperty = "Title";

            var subject = target.Find(secondID);
            subject.Title = TitleToDuplicate;

            var actual = target.Validate(subject);

            Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should have failed for title existing in repository.");

            var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            Assert.IsTrue(errorCountTitle > 0, "No violations were returned for {0}.", expectedInvlalidProperty);
        }

        [TestMethod]
        public void JobInfoLibrary_Validate_JobIDNotFound()
        {
            var first = GetValidObjectInstance(false);
            first.Title = "FirstItemTitle";
            var second = GetValidObjectInstance(false);
            second.Title = "Second Item Title";
            var jobData = new RepositoryMocker<JobInfo>();

            first.ID = jobData.GetNextID();
            var firstID = first.ID;
            jobData.items.Add(first);
            second.ID = jobData.GetNextID();
            var secondID = second.ID;

            // second item will not be present in the repository.

            var target = GetTargetLibrary(jobData);
            var expectedIsValid = false;
            var expectedInvlalidProperty = "ID";

            var actual = target.Validate(second);

            Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should have failed for job ID not in repository.");

            var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            Assert.IsTrue(errorCountTitle > 0, "No violations were returned for {0}.", expectedInvlalidProperty);
        }

        #endregion ValidateTests

        #region FindTests

        // Find automatically retrieves child items
        [TestMethod]
        public void JobInfoLibrary_Find_RetrievesChildPhases()
        {
            //var jobData = new RepositoryMocker<JobInfo>();

            //var first = GetValidObjectInstance(false);
            //first.Title = "Job Title";

            //first.ID = jobData.GetNextID();
            //var firstID = first.ID;
            //jobData.items.Add(first);
            //second.ID = jobData.GetNextID();
            //var secondID = second.ID;
            //jobData.items.Add(second);

            //var target = GetTargetLibrary(jobData);
            //var expectedIsValid = false;
            //var expectedInvlalidProperty = "Title";

            //var subject = target.Find(secondID);
            //subject.Title = TitleToDuplicate;

            //var actual = target.Validate(subject);

            //Assert.AreEqual(expectedIsValid, actual.IsValid, "Validation should have failed for title existing in repository.");

            //var errorCountTitle = actual.RuleViolations.Count(rv => rv.PropertyName == expectedInvlalidProperty);

            //Assert.IsTrue(errorCountTitle > 0, "No violations were returned for {0}.", expectedInvlalidProperty);
        }

        #endregion FindTests

        #region FindAnyTests

        // A found non-deleted job is returned with all relevent ProjectPhase items in ProjectPhases property.

        // A found deleted job is returned with all relevent ProjectPhase items in ProjectPhases property.

        #endregion FindAnyTests

        #region SearchTests

        // TODO: Add test cases for Search(string searchTerm)  - Waiting for ProjectPhase Repository.
        // Blank, null, or whitespace search term returns all items in Repository.
        // Returns all items with matching Title.
        // Does not return deleted jobs with matching title
        // Returns all items with matching Phase Names
        // Does not return deleted jobs with matching phases
        // Returns an empty set for no Repository results.
        // Returns an empty set when there are no matching job/phase values.
        // Returns an empty set when all matching job/phase items are marked deleted.

        #endregion SearchTests

        #region SearchAnyTests

        // TODO: Add test cases for SearchAny()

        #endregion SearchAnyTests

        #region GetUDPListTests

        // TODO: Add test cases for GetUDPList(long)

        #endregion GetUDPListTests

        #endregion Tests
    }
}