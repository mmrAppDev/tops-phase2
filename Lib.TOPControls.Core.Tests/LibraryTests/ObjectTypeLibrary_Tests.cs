﻿using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMRCommon.Data;
using Ninject;

namespace Lib.TOPControls.Tests.LibraryTests
{
    [TestClass]
    public class ObjectTypeLibrary_Tests : JobObjectLibrary_BaseTests<ObjectType>
    {
        #region OverridesAndBaseClassOverloads

        protected override IObjectLibrary<ObjectType> GetTargetLibrary()
        {
            var library = kernel.Get<ObjectTypeLibrary>();
            return library;
        }

        protected override IObjectLibrary<ObjectType> GetTargetLibrary(IRepository<ObjectType> useRepository)
        {
            return GetTargetAsJObLibrary(useRepository);
        }

        public override IJobObjectLibrary<ObjectType> GetTargetAsJObLibrary(IRepository<ObjectType> useRepository = null)
        {
            var library = kernel.Get<ObjectTypeLibrary>();
            library.Repository = useRepository;
            return library;
        }

        protected override ObjectType GetInvalidObjectInstance(bool asNewItem)
        {
            var result = new ObjectType
            {
                ID = (asNewItem ? 0 : 1),
                JobID = -1,
                Name = "",
                Description = "NoType"
            };
            return result;
        }

        protected override ObjectType GetValidObjectInstance(bool asNewItem)
        {
            var result = new ObjectType
            {
                ID = (asNewItem ? 0 : 1),
                JobID = 1,
                Name = "Instrument/Electrical",
                Description = "Identifies items at the smallest testable scale."
            };
            return result;
        }

        #endregion OverridesAndBaseClassOverloads

        #region Tests

        #endregion Tests
    }
}