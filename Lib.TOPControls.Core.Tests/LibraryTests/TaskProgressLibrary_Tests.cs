﻿using System;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Common;
using Lib.TOPControls.Tests.LibraryTests.Mock;
using Lib.TOPControls.BaseClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMRCommon.Data;
using Ninject;

namespace Lib.TOPControls.Tests.LibraryTests
{
    [TestClass]
    public class TaskProgressLibrary_Tests : JobSoftDeleteLibrary_BaseTests<TaskProgress>
    {
        #region OverridesAndBaseClassOverloads

        protected override IObjectLibrary<TaskProgress> GetTargetLibrary()
        {
            var library = kernel.Get<TaskProgressLibrary>();
            return library;
        }

        protected override IObjectLibrary<TaskProgress> GetTargetLibrary(IRepository<TaskProgress> useRepository)
        {
            return GetTargetAsSDLibrary(useRepository);
        }

        
        public override JobSoftDeleteLibraryBase<TaskProgress> GetTargetAsJObLibrary(IRepository<TaskProgress> useRepository = null)
        {
            return GetTargetAsSDLibrary(useRepository) as JobSoftDeleteLibraryBase<TaskProgress>;
        }

        public override ISoftDeleteLibrary<TaskProgress> GetTargetAsSDLibrary(IRepository<TaskProgress> useRepository = null)
        {
            var library = kernel.Get<TaskProgressLibrary>();
            library.Repository = useRepository;
            return library;
        }

        protected override TaskProgress GetInvalidObjectInstance(bool asNewItem)
        {
            var result = new TaskProgress
            {
                ID = (asNewItem ? 0 : 1)
            };
            return result;
        }

        protected override TaskProgress GetValidObjectInstance(bool asNewItem)
        {
            var result = new TaskProgress
            {
                ID = (asNewItem ? 0 : 1),
                TaskID = 1,
                TrackedItemID = 1,
                LastChanged = DateTime.Now
            };
            return result;
        }

        #endregion OverridesAndBaseClassOverloads

        #region Tests

        public void Extracted_UT_TaskProgressLibrary_ValidateForAlteredPercentComplete(bool newItem)
        {
            var target = GetTargetLibrary();
            var item = GetValidObjectInstance(newItem);
            var expectedValidity = newItem;
            // Set the percent compelete twice to be sure that the Initial Value doesn't match the 
            // current.

            item.SetPercentComplete(0.33m);
            item.SetPercentComplete(0.66m);

            var result = target.Validate(item);

            
            Assert.AreEqual(expectedValidity, result.IsValid, "Validation should fail with initial percent complete doesn't match current value.");
            if (!result.IsValid)
            {
                RuleViolation pcError = null;
                foreach (var rv in result.RuleViolations)
                {
                    if (rv.PropertyName == "PercentComplete")
                    {
                        pcError = rv;
                        break;
                    }
                }
                Assert.IsNotNull(pcError, "No error found for PercentComplete property.");
            }
        }


        [TestMethod]
        public void UT_TaskProgressLibrary_ValidatePassesForAlteredPercentComplete_NewItem()
        {
            Extracted_UT_TaskProgressLibrary_ValidateForAlteredPercentComplete(true);
        }


        [TestMethod]
        public void UT_TaskProgressLibrary_ValidateFailsForAlteredPercentComplete_UpdateItem()
        {
            Extracted_UT_TaskProgressLibrary_ValidateForAlteredPercentComplete(false);
        }



        [TestMethod]
        public void UT_TaskProgressLibrary_AddSucceedsForAlteredPercentComplete()
        {
            var dataMock = new RepositoryMocker<TaskProgress>();
            var target = GetTargetLibrary(dataMock);
            var item = GetValidObjectInstance(true);



            var userID = 1;
            // Set the percent compelete twice to be sure that the Initial Value doesn't match the 
            // current.

            item.SetPercentComplete(0.33m);
            item.SetPercentComplete(0.66m);

            var result = target.Add(item, userID);

        }

        

        #endregion Tests


    }
}