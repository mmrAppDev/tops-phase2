﻿using System;
using System.Collections.Generic;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Common;
using Lib.TOPControls.Tests.LibraryTests.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using MMRCommon.Data;
using Ninject;

namespace Lib.TOPControls.Tests.LibraryTests
{
    [TestClass]
    public class FilterConfigurationLibrary_Tests : JobObjectLibrary_BaseTests<FilterConfiguration>
    {
        public override IJobObjectLibrary<FilterConfiguration> GetTargetAsJObLibrary(IRepository<FilterConfiguration> useRepository = null)
        {
            var library = kernel.Get<FilterConfigurationLibrary>();
            library.Repository = useRepository;
            return library;
        }

        protected override Interfaces.IObjectLibrary<FilterConfiguration> GetTargetLibrary()
        {
            var library = kernel.Get<FilterConfigurationLibrary>();
            return library;
        }

        protected override IObjectLibrary<FilterConfiguration> GetTargetLibrary(IRepository<FilterConfiguration> useRepository)
        {
            return GetTargetAsJObLibrary(useRepository);
        }

        protected override FilterConfiguration GetInvalidObjectInstance(bool asNewItem)
        {
            var result = new FilterConfiguration
            {
                ID = (asNewItem ? 0 : 1),
                JobID = -1,
                DisplayOrder = -1
            };
            return result;
        }

        protected override FilterConfiguration GetValidObjectInstance(bool asNewItem)
        {
            var result = new FilterConfiguration
            {
                ID = (asNewItem ? 0 : 1),
                JobID = 1,
                FilterValueType = TOPControls.FilterValueType.Phase,
                LastChanged = DateTime.Now,
                DisplayOrder = 1
            };
            return result;
        }

        protected IRepository<FilterConfiguration> GetEmptyRepository()
        {
            var result = new RepositoryMocker<FilterConfiguration>();
            return result;
        }

        

        #region Tests

        [TestMethod]
        public void FilterConfigurationLibrary_Renumber_SetsASingleItemToDisplayOrder1()
        {
            long jobValue = 7364;
            var fcitems = new List<FilterConfiguration>();

            fcitems.Add(new FilterConfiguration { ID = 301, 
                JobID = jobValue, 
                Active=true, 
                DisplayOrder = 12, 
                IsHighVolume=false, 
                FilterValueType = FilterValueType.CraftInfo });

            var rep = new RepositoryMocker<FilterConfiguration>(fcitems);

            var target = GetTargetAsJObLibrary(rep) as FilterConfigurationLibrary;

            target.RenumberItems(jobValue);


            var expectedDisplayOrder = 1;
            var actualDisplayOrder = rep.items[0].DisplayOrder;

            Assert.AreEqual(expectedDisplayOrder, actualDisplayOrder);

        }

        [TestMethod]
        public void FilterConfigurationLibrary_Renumber_Sets_3_4_to_1_2()
        {

            long jobValue = 7364;
            var fcitems = new List<FilterConfiguration>();

            fcitems.Add(new FilterConfiguration
            {
                ID = 301,
                JobID = jobValue,
                Active = true,
                DisplayOrder = 3,
                IsHighVolume = false,
                FilterValueType = FilterValueType.Commodity
            });

            fcitems.Add(new FilterConfiguration
            {
                ID = 300,
                JobID = jobValue,
                Active = true,
                DisplayOrder = 4,
                IsHighVolume = false,
                FilterValueType = FilterValueType.Search
            });

            var rep = new RepositoryMocker<FilterConfiguration>(fcitems);

            var target = GetTargetAsJObLibrary(rep) as FilterConfigurationLibrary;

            target.RenumberItems(jobValue);

            var sorted = rep.items.OrderBy(fc => fc.DisplayOrder);

            for (int idx = 0; idx < fcitems.Count; idx++)
            {
                var expectedDisplayOrder = idx + 1;
                var actualDisplayOrder = sorted.ElementAt(idx).DisplayOrder;
                Assert.AreEqual(expectedDisplayOrder, actualDisplayOrder);
            }


        }


        [TestMethod]
        public void FilterConfigurationLibrary_Renumber_Sets_97_425831_to_1_2()
        {

            long jobValue = 7364;
            var fcitems = new List<FilterConfiguration>();

            fcitems.Add(new FilterConfiguration
            {
                ID = 301,
                JobID = jobValue,
                Active = true,
                DisplayOrder = 425831,
                IsHighVolume = false,
                FilterValueType = FilterValueType.Commodity
            });

            fcitems.Add(new FilterConfiguration
            {
                ID = 300,
                JobID = jobValue,
                Active = true,
                DisplayOrder = 97,
                IsHighVolume = false,
                FilterValueType = FilterValueType.Search
            });

            var rep = new RepositoryMocker<FilterConfiguration>(fcitems);

            var target = GetTargetAsJObLibrary(rep) as FilterConfigurationLibrary;

            target.RenumberItems(jobValue);

            var sorted = rep.items.OrderBy(fc => fc.DisplayOrder);

            for (int idx = 0; idx < fcitems.Count; idx++)
            {
                var expectedDisplayOrder = idx + 1;
                var actualDisplayOrder = sorted.ElementAt(idx).DisplayOrder;
                Assert.AreEqual(expectedDisplayOrder, actualDisplayOrder);
            }
        }


        [TestMethod]
        public void FilterConfigurationLibrary_Renumber_NoErrorOnEmptySet()
        {
            long jobValue = 7364;
            var fcitems = new List<FilterConfiguration>();


            var rep = new RepositoryMocker<FilterConfiguration>(fcitems);

            var target = GetTargetAsJObLibrary(rep) as FilterConfigurationLibrary;

            target.RenumberItems(jobValue);

            var expectedCount = 0;
            var actualCount = rep.items.Count;

            Assert.AreEqual(expectedCount, actualCount);

        }


        #endregion

    }
}