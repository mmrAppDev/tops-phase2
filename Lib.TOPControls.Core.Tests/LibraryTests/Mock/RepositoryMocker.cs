﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMRCommon.Data;

namespace Lib.TOPControls.Tests.LibraryTests.Mock
{
    public class RepositoryMocker<ObType> : IRepository<ObType> where ObType : DomainObject, new()
    {
        public bool? IsItemFound { get; set; }

        public List<ObType> items { get; set; }

        public long LastIDFound { get; set; }

        public long LastIDDeleted { get; set; }

        public ObType LastItemInserted { get; set; }

        public bool ChangesSaved { get; set; }

        #region Constructors

        public RepositoryMocker()
        {
            LastIDFound = 0;
            LastIDDeleted = 0;
            LastItemInserted = null;
            ChangesSaved = false;
            items = new List<ObType>();
        }

        public RepositoryMocker(IEnumerable<ObType> useItems)
            : this()
        {
            items.AddRange(useItems);
        }

        #endregion Constructors

        #region IRepository<ObType> Members

        public IQueryable<ObType> All
        {
            get { return items.AsQueryable(); }
        }

        public void Delete(long id)
        {
            LastIDDeleted = id;
            ChangesSaved = false;
            var deleteTarget = items.SingleOrDefault(obj => obj.ID == id);
            if (deleteTarget != null)
            {
                items.Remove(deleteTarget);
            }
        }

        public ObType Find(long id)
        {
            LastIDFound = id;
            ObType result = null;
            ChangesSaved = false;
            if (IsItemFound.HasValue && IsItemFound.Value)        // result remains null if IsItemFound is null
            {
                var listItem = items.SingleOrDefault(itemVal => itemVal.ID == id);
                if (listItem == null)
                {
                    listItem = new ObType() { ID = id, LastChanged = DateTime.Now };
                }
                result = listItem;
            }
            else if (!IsItemFound.HasValue)
            {
                result = items.SingleOrDefault(itemVal => itemVal.ID == id);
            }
            return result;
        }

        public void Insert(ObType item)
        {
            LastItemInserted = item;
            ChangesSaved = false;
            item.ID = GetNextID();
            items.Add(item);
        }

        public void Save()
        {
            ChangesSaved = true;
        }

        #endregion IRepository<ObType> Members

        public long GetNextID()
        {
            if (items.Count > 0)
            {
                var maxvalue = items.Max(obj => obj.ID);
                return maxvalue + 1;
            }
            return 1;
        }
    }
}