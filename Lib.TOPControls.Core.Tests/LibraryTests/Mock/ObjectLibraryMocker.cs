﻿using Lib.TOPControls.Interfaces;
using MMRCommon.Data;

namespace Lib.TOPControls.Tests.LibraryTests.Mock
{
    public class ObjectLibraryMocker<ObType> : IObjectLibrary<ObType> where ObType : DomainObject, new()
    {
        public IRepository<ObType> Repository { get; set; }

        public ObjectLibraryMocker()
        {
            var tempRep = new RepositoryMocker<ObType>();
            tempRep.IsItemFound = true;
            Repository = tempRep;
        }

        #region IObjectLibrary<ObType> Members

        public ObType Find(long id)
        {
            return Repository.Find(id);
        }

        public ObType Add(ObType newItem, long userID)
        {
            Repository.Insert(newItem);
            Repository.Save();
            return newItem;
        }

        public ObType Update(ObType item, long userID)
        {
            Repository.Save();
            return item;
        }

        public MMRCommon.Data.ValidationResult Validate(ObType item)
        {
            return new ValidationResult();
        }

        public void Delete(long id, long userID)
        {
            Repository.Delete(id);
            Repository.Save();
        }

        #endregion IObjectLibrary<ObType> Members

        #region IObjectLibrary<ObType> Members


        public ValidationResult ValidateDelete(long id)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}