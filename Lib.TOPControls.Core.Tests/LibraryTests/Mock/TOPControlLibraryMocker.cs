﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lib.TOPControls.Charting;
using Lib.TOPControls.Interfaces;
using MMRCommon.Data;

namespace Lib.TOPControls.Tests.LibraryTests.Mock
{
    public class TOPControlLibraryMocker : ITOPControlLibrary
    {
        #region ITOPControlLibrary Members

        public IJobInfoLibrary JobActions { get; set; }

        public IProjectPhaseLibrary PhaseActions { get; set; }

        public IObjectTypeLibrary ObjectTypeActions { get; set; }

        public IProjectSystemLibrary SystemActions { get; set; }

        public IProjectAreaLibrary AreaActions { get; set; }

        public ICraftInfoLibrary CraftActions { get; set; }

        public ICommodityLibrary CommodityActions { get; set; }

        public ITrackedItemLibrary TrackedItemActions { get; set; }

        public ITaskLibrary TaskActions { get; set; }

        public ITaskProgressLibrary TaskProgressActions { get; set; }

        public ITOPUserLibrary UserActions { get; set; }

        public IUserJobLibrary UserJobActions { get; set; }

        public IWorkCrewLibrary WorkCrewActions { get; set; }

        public Permissions.IPermissionManager PermissionManager { get; set; }

        public IFilterConfigurationLibrary FilterConfigActions { get; set; }

        public IProgressLogEntryLibrary ProgressLogEntryActions { get; set; }

        public MMRCommon.Data.IRepository<DOType> GetRepository<DOType>()
            where DOType : DomainObject, new()
        {
            IRepository<DOType> result = null;
            var srcLib = GetLibrary<DOType>() as ObjectLibraryMocker<DOType>;
            if (srcLib == null || srcLib.Repository == null)
            {
                result = new RepositoryMocker<DOType>();
                if (srcLib != null)
                {
                    srcLib.Repository = result;
                }
            }
            else
            {
                result = srcLib.Repository;
            }

            return result;
        }

        public Interfaces.IObjectLibrary<DOType> GetLibrary<DOType>()
            where DOType : DomainObject, new()
        {
            IObjectLibrary<DOType> result = null;
            switch (typeof(DOType).Name)
            {
                case "JobInfo":

                    //if (JobActions == null) JobActions = CreateNewLibrary<JobInfo>();
                    result = ((IObjectLibrary<DOType>) JobActions ?? CreateNewLibrary<DOType>());
                    break;

                case "ProjectPhase":

                    //if (PhaseActions == null) PhaseActions = CreateNewLibrary<ProjectPhase>();
                    result = (IObjectLibrary<DOType>) PhaseActions ?? CreateNewLibrary<DOType>();
                    break;

                case "ObjectType":

                    //if (ObjectTypeActions == null) ObjectTypeActions = CreateNewLibrary<ObjectType>();
                    result = (IObjectLibrary<DOType>) ObjectTypeActions ?? CreateNewLibrary<DOType>();
                    break;

                case "ProjectSystem":

                    //if (SystemActions == null) SystemActions = CreateNewLibrary<DOType>();
                    result = (IObjectLibrary<DOType>) SystemActions ?? CreateNewLibrary<DOType>();
                    break;

                case "ProjectArea":

                    //if (AreaActions == null) AreaActions = CreateNewLibrary<DOType>();
                    result = (IObjectLibrary<DOType>) AreaActions ?? CreateNewLibrary<DOType>();
                    break;

                case "CraftInfo":

                    //if (CraftActions == null) CraftActions = CreateNewLibrary<DOType>();
                    result = (IObjectLibrary<DOType>) CraftActions ?? CreateNewLibrary<DOType>();
                    break;

                case "Commodity":

                    //if (CommodityActions == null) CommodityActions = CreateNewLibrary<DOType>();
                    result = (IObjectLibrary<DOType>) CommodityActions ?? CreateNewLibrary<DOType>();
                    break;

                case "TrackedItem":

                    //if (TrackedItemActions == null) TrackedItemActions = CreateNewLibrary<DOType>();
                    result = (IObjectLibrary<DOType>) TrackedItemActions ?? CreateNewLibrary<DOType>();
                    break;

                case "Task":
                    result = (IObjectLibrary<DOType>) TaskActions ?? CreateNewLibrary<DOType>();
                    break;

                case "TaskProgress":

                    //if (TaskActions == null) TaskActions = CreateNewLibrary<DOType>();
                    result = (IObjectLibrary<DOType>) TaskProgressActions ?? CreateNewLibrary<DOType>();
                    break;

                case "TOPUser":

                    //if (UserActions == null) UserActions = CreateNewLibrary<DOType>();
                    result = (IObjectLibrary<DOType>) UserActions ?? CreateNewLibrary<DOType>();
                    break;

                case "WorkCrew":

                    //if (WorkCrewActions == null) WorkCrewActions = CreateNewLibrary<DOType>();
                    result = (IObjectLibrary<DOType>) WorkCrewActions ?? CreateNewLibrary<DOType>();
                    break;

                case "FilterConfiguration":
                    result = (IObjectLibrary<DOType>) FilterConfigActions ?? CreateNewLibrary<DOType>();
                    break;

                case "ProgressLogEntry":
                    result = (IObjectLibrary<DOType>) ProgressLogEntryActions ?? CreateNewLibrary<DOType>();
                    break;

                default:
                    result = CreateNewLibrary<DOType>();
                    break;
            }

            return result;
        }

        protected IObjectLibrary<DOType> CreateNewLibrary<DOType>()
            where DOType : DomainObject, new()
        {
            var result = new ObjectLibraryMocker<DOType>();
            return result;
        }

        public IQueryable<TaskProgress> FilterTaskProgress(long jobID, FilterParameters filterParams)
        {
            throw new NotImplementedException();
        }

        #endregion ITOPControlLibrary Members

        #region ITOPControlLibrary Members

        #endregion ITOPControlLibrary Members

        #region ITOPControlLibrary Members

        public IEnumerable<ProjectSystem> GetProjectSystemsForFilter(long jobID, FilterParameters filterParams)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProjectArea> GetProjectAreasForFilter(long jobID, FilterParameters filterParams)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ObjectType> GetObjectTypesForFilter(long jobID, FilterParameters filterParams)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CraftInfo> GetCraftInfoValuesForFilter(long jobID, FilterParameters filterParams)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProjectSystem> GetProjectSystemTree(long jobId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetUserDefinedVals_1_ForFilter(long jobID, FilterParameters filterParams)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetUserDefinedVals_2_ForFilter(long jobID, FilterParameters filterParams)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetUserDefinedVals_3_ForFilter(long jobID, FilterParameters filterParams)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Commodity> GetCommoditiesForFilter(long jobID, FilterParameters filterParams)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Task> GetTasksForFilter(long jobID, FilterParameters filterParams)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<WorkCrew> GetWorkCrewsForFilter(long jobID, FilterParameters filterParams)
        {
            throw new NotImplementedException();
        }

        public ProgressLogEntry UpdateProgress(long jobID, long taskProgressID, decimal newPercentComplete,
            DateTime workDate, long workCrewID, long userID)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ITOPControlLibrary Members

        public decimal CalculateTotalPercentCompleteForFilter(long jobID, FilterParameters filterParams)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> GetQueryable<T>() where T : class
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ITOPControlLibrary Members

        public HistoryChartData GetHistoryChartData(long jobID, FilterParameters filterParams, FilterValueType grouping,
            object groupValue)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ITOPControlLibrary Members

        public IFileImportLibrary FileImportActions
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        #endregion

        #region ITOPControlLibrary Members

        public IEnumerable<PhaseSummary> GetPhaseSummaries(long jobID)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<GroupedCompletionChartDataPoint> GetGroupedCompletions(long jobID, FilterValueType grouping)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ITOPControlLibrary Members

        public IChartDefinitionLibrary ChartDefinitionActions
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public IHistoryChartDataLibrary HistoryChartDataActions
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public IGroupedCompletionChartLibrary GroupedCompletionChartActions
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        #endregion

        #region ITOPControlLibrary Members

        public IEnumerable<UnrecognizedProgress> GetUnrecognizedProgress()
        {
            throw new NotImplementedException();
        }

        public void UpdateProgressHistory(long jobID, DateTime workDate)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ITOPControlLibrary Members

        public void UpdateReportingData()
        {
            throw new NotImplementedException();
        }

        #endregion

        public TProperty GetCommittedPropertyValue<DOType, TProperty>(DOType item,
            System.Linq.Expressions.Expression<Func<DOType, TProperty>> property) where DOType : DomainObject
        {
            throw new NotImplementedException();
        }

        public int PropogateCommodityByTask(long taskID, long? commodityID, long? matchCommodity = null,
            bool setNulls = true)
        {
            throw new NotImplementedException();
        }

        public void PurgeReportingData(long jobID = 0)
        {
            throw new NotImplementedException();
        }

        public List<RawHistoryDataPoint> SelectNewHistoryChartData(long? jobID = null, long? phaseID = null,
            long? taskID = null, long? trackedItemID = null,
            long? systemID = null, long? commodityID = null, long? areaID = null, long? objectTypeID = null,
            long? craftID = null, string udpValue1 = null, string udpValue2 = null, string udpValue3 = null,
            DateTime? workDate = null)
        {
            throw new NotImplementedException();
        }
    }
}