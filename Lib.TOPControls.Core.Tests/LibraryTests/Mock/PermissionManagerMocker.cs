﻿using Lib.TOPControls.Permissions;

namespace Lib.TOPControls.Tests.LibraryTests.Mock
{
    public class PermissionManagerMocker : IPermissionManager
    {
        public long LastCheckedUserID { get; set; }

        public DomainObject LastCheckedTarget { get; set; }

        public UserActionType LastCheckedAction { get; set; }

        public bool ThrowNotFoundUserException { get; set; }

        public bool ApproveActions { get; set; }

        #region Constructors

        public PermissionManagerMocker()
        {
            LastCheckedUserID = 0;
            LastCheckedTarget = null;
            LastCheckedAction = UserActionType.View;
            ThrowNotFoundUserException = false;
            ApproveActions = true;
        }

        #endregion Constructors

        #region IPermissionManager Members

        public bool IsValidUserAction(UserActionType action, DomainObject target, long userID)
        {
            LastCheckedAction = action;
            LastCheckedTarget = target;
            LastCheckedUserID = userID;
            if (ThrowNotFoundUserException)
                throw new Exceptions.UserPermissionsException(string.Format("No user account could be found for user ID {0}.", userID), userID);
            return ApproveActions;
        }

        #endregion IPermissionManager Members
    }
}