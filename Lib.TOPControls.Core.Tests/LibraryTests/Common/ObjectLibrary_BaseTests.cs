﻿using System;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Exceptions;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMRCommon.Data;
using Ninject;

namespace Lib.TOPControls.Tests.LibraryTests.Common
{
    /// <summary>
    /// Provides a base collection of common test methods for classes implementing the IDomainObjectLibrary interface.
    /// </summary>
    [TestClass]
    public abstract class ObjectLibrary_BaseTests<ObType>
        where ObType : DomainObject, new()
    {
        public ObjectLibrary_BaseTests()
        {
            kernel = new StandardKernel(new ObjectTypeLibrary_TestingModule<ObType>());
        }

        public IKernel kernel { get; set; }

        /// <summary>
        /// Returns the target library being tested by the concerete class.
        /// </summary>
        /// <returns></returns>
        protected abstract IObjectLibrary<ObType> GetTargetLibrary();

        /// <summary>
        /// Returns the target library being tested by the concerete class, with an IRepository than can be used to test
        /// the classes interactions with the mock Repository.
        /// </summary>
        /// <returns></returns>
        protected abstract IObjectLibrary<ObType> GetTargetLibrary(IRepository<ObType> useRepository);

        /// <summary>
        /// Returns an instance of the targeted object class that will fail a validation attempt.
        /// </summary>
        /// <returns></returns>
        protected abstract ObType GetInvalidObjectInstance(bool asNewItem);

        /// <summary>
        /// Returns an instance of the targeted object class that will not fail a validation attempt.
        /// </summary>
        /// <returns></returns>
        protected abstract ObType GetValidObjectInstance(bool asNewItem);

        /// <summary>
        /// When overridden in a derived class, injects the permissons manager passed into the
        /// IObjectLibrary, if the injection method is recognized.
        /// </summary>
        /// <param name="target">The ObjectLibrary to decorate.</param>
        /// <param name="permMan">The permissions manager to include.</param>
        protected virtual void SetLibraryPermMananger(IObjectLibrary<ObType> target, Permissions.IPermissionManager permMan)
        {
            var objLib = target as ObjectLibraryBase<ObType>;
            if (objLib != null)
            {
                objLib.PermManager = permMan;
            }
        }

        protected virtual void SetDefaultPermMananger(IObjectLibrary<ObType> target)
        {
            var pm = new PermissionManagerMocker();
            pm.ApproveActions = true;
            SetLibraryPermMananger(target, pm);
        }

        [TestMethod]
        public void ObjectLibrary_Const_GetsInstance()
        {
            var target = GetTargetLibrary();
            Assert.IsInstanceOfType(target, typeof(IObjectLibrary<ObType>));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Find_FailsOnNegativeID()
        {
            var target = GetTargetLibrary();
            var objectID = -1;

            target.Find(objectID);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Find_FailsOnZeroID()
        {
            var target = GetTargetLibrary();
            var objectID = 0;

            target.Find(objectID);
        }

        [TestMethod]
        public void ObjectLibrary_Find_ReturnsNullIfNotFound()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = false;   // guarantee the Repository will not return a value.

            var objectID = 23;
            var target = GetTargetLibrary(dataMock);

            var result = target.Find(objectID);

            Assert.IsNull(result);
        }

        [TestMethod]
        public void ObjectLibrary_Find_ReturnsInstanceIfFound()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = true;   // guarantee the Repository will find the value.

            var objectID = 23;
            var target = GetTargetLibrary(dataMock);

            var result = target.Find(objectID);

            Assert.IsInstanceOfType(result, typeof(ObType), "No result or incorrect type.");
        }

        [TestMethod]
        public void ObjectLibrary_Find_UsesRepositoryFind()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = true;   // guarantee the Repository will find the value.

            var expectedID = 23;
            var target = GetTargetLibrary(dataMock);

            var result = target.Find(expectedID);

            var foundID = dataMock.LastIDFound;

            Assert.AreEqual(expectedID, foundID, "Repository not used for find operation.");

            Assert.IsNotNull(result);
        }

        #region Add_MethodTests

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ObjectLibrary_Add_FailsOnNullItem()
        {
            var target = GetTargetLibrary();

            ObType expectedItem = null;
            long expectedUserID = 1;

            target.Add(expectedItem, expectedUserID);
        }

        // Add fails when validation fails.
        [TestMethod]
        [ExpectedException(typeof(ValidationFailureException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Add_FailOnValidationFailure()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = false;

            var target = GetTargetLibrary(dataMock);

            ObType expectedItem = GetInvalidObjectInstance(true);
            long expectedUserID = 1;

            target.Add(expectedItem, expectedUserID);
        }

        // Add validation failure exception includes ValidationResult.
        [TestMethod]
        public void ObjectLibrary_Add_ValidationFailureException_ContainsValidationResult()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = false;

            var target = GetTargetLibrary(dataMock);

            ObType expectedItem = GetInvalidObjectInstance(true);
            long expectedUserID = 1;

            try
            {
                target.Add(expectedItem, expectedUserID);
            }
            catch (ValidationFailureException vfex)
            {
                Assert.IsInstanceOfType(vfex.FailedValidationResult, typeof(ValidationResult), "Operation's validation failure did not return ValidationResult.");
                Assert.AreEqual(false, vfex.FailedValidationResult.IsValid);
            }
            catch
            {
                throw;
            }
        }

        // Add operation returns item with new ID value.
        [TestMethod]
        public void ObjectLibrary_Add_ReturnsResultItem()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = false;

            var target = GetTargetLibrary(dataMock);

            ObType expectedItem = GetValidObjectInstance(true);
            long expectedUserID = 1;
            long unexpectedObjectID = expectedItem.ID;

            var opResult = target.Add(expectedItem, expectedUserID);

            Assert.IsNotNull(opResult);
            Assert.AreNotEqual(unexpectedObjectID, opResult.ID);
        }

        // Add operation causes Repository Save.
        [TestMethod]
        public void ObjectLibrary_Add_UsesRepositorySave()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = false;

            var target = GetTargetLibrary(dataMock);

            ObType expectedItem = GetValidObjectInstance(true);
            long expectedUserID = 1;

            target.Add(expectedItem, expectedUserID);

            Assert.IsTrue(dataMock.ChangesSaved, "Add operation did not commit database changes.");
        }

        // Add operation alters LastChanged date.
        [TestMethod]
        public void ObjectLibrary_Add_UpdatesLastChangedDate()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = false;

            var target = GetTargetLibrary(dataMock);

            ObType expectedItem = GetValidObjectInstance(true);
            expectedItem.LastChanged = DateTime.Now.AddSeconds(-30);

            long expectedUserID = 1;
            DateTime notExpectedLastChangedDate = expectedItem.LastChanged;

            var opResult = target.Add(expectedItem, expectedUserID);

            Assert.IsNotNull(opResult);
            Assert.AreNotEqual(notExpectedLastChangedDate, opResult.LastChanged);
        }

        // TODO: Test Cases for Add Operation:
        // Add fails when userID not valid
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Add_FailsOnNegativeUserID()
        {
            var target = GetTargetLibrary();
            var testItem = GetValidObjectInstance(true);

            long expectedUserID = -1;

            target.Add(testItem, expectedUserID);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Add_FailsOnZeroUserID()
        {
            var target = GetTargetLibrary();
            var testItem = GetValidObjectInstance(true);

            long expectedUserID = 0;

            target.Add(testItem, expectedUserID);
        }

        // Add fails when user not found
        [TestMethod]
        [ExpectedException(typeof(UserPermissionsException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Add_FailsOnUserIDNotFound()
        {
            var target = GetTargetLibrary();
            var permMan = new PermissionManagerMocker();
            permMan.ThrowNotFoundUserException = true;

            SetLibraryPermMananger(target, permMan);

            var testItem = GetValidObjectInstance(true);

            long expectedUserID = 1;

            target.Add(testItem, expectedUserID);
        }

        // Add fails when user does not have rights.
        [TestMethod]
        [ExpectedException(typeof(UserPermissionsException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Add_FailWhenUserActionNotPermitted()
        {
            var target = GetTargetLibrary();
            var permMan = new PermissionManagerMocker();
            permMan.ApproveActions = false;

            SetLibraryPermMananger(target, permMan);

            var testItem = GetValidObjectInstance(true);

            long expectedUserID = 1;

            target.Add(testItem, expectedUserID);
        }

        #endregion Add_MethodTests

        #region Update_MethodTests

        // Update fails on null item.
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ObjectLibrary_Update_FailsOnNullItem()
        {
            var target = GetTargetLibrary();

            ObType expectedItem = null;
            long expectedUserID = 1;

            target.Update(expectedItem, expectedUserID);
        }

        // Update fails on missing item.
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Update_FailsOnMissingItem()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = false;

            var target = GetTargetLibrary(dataMock);

            ObType expectedItem = GetValidObjectInstance(false);
            expectedItem.ID = dataMock.GetNextID();
            expectedItem.LastChanged = DateTime.Now;

            long expectedUserID = 1;

            var opResult = target.Update(expectedItem, expectedUserID);
        }

        // Update fails on validation failure.
        [TestMethod]
        [ExpectedException(typeof(ValidationFailureException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Update_FailsOnValidationFailure()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = true;

            var target = GetTargetLibrary(dataMock);

            ObType expectedItem = GetInvalidObjectInstance(false);
            expectedItem.ID = dataMock.GetNextID();
            expectedItem.LastChanged = DateTime.Now;
            dataMock.items.Add(expectedItem);

            long expectedUserID = 1;

            var opResult = target.Update(expectedItem, expectedUserID);
        }

        // Update validation failure exception contains ValidationResult
        [TestMethod]
        public void ObjectLibrary_Update_ValidationFailureException_ContainsValidationResult()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = true;

            var target = GetTargetLibrary(dataMock);

            ObType expectedItem = GetInvalidObjectInstance(false);
            expectedItem.ID = dataMock.GetNextID();
            expectedItem.LastChanged = DateTime.Now;
            dataMock.items.Add(expectedItem);

            long expectedUserID = 1;

            try
            {
                var opResult = target.Update(expectedItem, expectedUserID);
            }
            catch (ValidationFailureException vfex)
            {
                Assert.IsInstanceOfType(vfex.FailedValidationResult, typeof(ValidationResult), "Operation's validation failure did not return ValidationResult.");
                Assert.AreEqual(false, vfex.FailedValidationResult.IsValid);
            }
            catch
            {
                throw;
            }
        }

        // Update returns updated item.
        [TestMethod]
        public void ObjectLibrary_Update_ReturnsResultItem()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = true;

            var target = GetTargetLibrary(dataMock);

            ObType expectedItem = GetValidObjectInstance(false);
            expectedItem.ID = dataMock.GetNextID();
            expectedItem.LastChanged = DateTime.Now;
            dataMock.items.Add(expectedItem);

            long expectedUserID = 1;
            var opResult = target.Update(expectedItem, expectedUserID);

            Assert.IsInstanceOfType(opResult, typeof(ObType), "Failed to return an item on update.");
            Assert.AreEqual(expectedItem.ID, opResult.ID);
        }

        // Update uses Repository save operation.
        [TestMethod]
        public void ObjectLibrary_Update_UsesRepositorySave()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = true;

            var target = GetTargetLibrary(dataMock);

            ObType expectedItem = GetValidObjectInstance(false);
            expectedItem.ID = dataMock.GetNextID();
            expectedItem.LastChanged = DateTime.Now;
            dataMock.items.Add(expectedItem);

            long expectedUserID = 1;
            var opResult = target.Update(expectedItem, expectedUserID);

            Assert.IsTrue(dataMock.ChangesSaved, "Save operation not performed.");
        }

        // Update changes last changed value.
        [TestMethod]
        public void ObjectLibrary_Update_SetsLastChangedValue()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = true;

            var target = GetTargetLibrary(dataMock);

            ObType expectedItem = GetValidObjectInstance(false);
            expectedItem.ID = dataMock.GetNextID();
            expectedItem.LastChanged = DateTime.Now.AddSeconds(-30);
            dataMock.items.Add(expectedItem);

            var notExpectedLastChanged = expectedItem.LastChanged;

            long expectedUserID = 1;
            var opResult = target.Update(expectedItem, expectedUserID);

            Assert.AreNotEqual(notExpectedLastChanged, opResult.LastChanged, "Update did not change LastChanged value.");
        }

        // Update fails when userID not valid
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Update_FailsOnNegativeUserID()
        {
            var mockData = new RepositoryMocker<ObType>();
            var testItem = GetValidObjectInstance(false);
            testItem.ID = mockData.GetNextID();
            mockData.items.Add(testItem);

            var target = GetTargetLibrary(mockData);

            long expectedUserID = -1;

            target.Update(testItem, expectedUserID);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Update_FailsOnZeroUserID()
        {
            var mockData = new RepositoryMocker<ObType>();
            var testItem = GetValidObjectInstance(false);
            testItem.ID = mockData.GetNextID();
            mockData.items.Add(testItem);

            var target = GetTargetLibrary(mockData);

            long expectedUserID = 0;

            target.Update(testItem, expectedUserID);
        }

        // Update fails when user not found
        [TestMethod]
        [ExpectedException(typeof(UserPermissionsException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Update_FailsOnUserIDNotFound()
        {
            var mockData = new RepositoryMocker<ObType>();
            var testItem = GetValidObjectInstance(false);
            testItem.ID = mockData.GetNextID();
            mockData.items.Add(testItem);
            var target = GetTargetLibrary(mockData);
            var permMan = new PermissionManagerMocker();
            permMan.ThrowNotFoundUserException = true;

            SetLibraryPermMananger(target, permMan);

            long expectedUserID = 1;

            target.Update(testItem, expectedUserID);
        }

        // Update fails when user does not have rights.
        [TestMethod]
        [ExpectedException(typeof(UserPermissionsException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Update_FailWhenUserActionNotPermitted()
        {
            var mockData = new RepositoryMocker<ObType>();
            var testItem = GetValidObjectInstance(false);
            testItem.ID = mockData.GetNextID();
            mockData.items.Add(testItem);
            var target = GetTargetLibrary(mockData);
            var permMan = new PermissionManagerMocker();
            permMan.ApproveActions = false;

            SetLibraryPermMananger(target, permMan);

            long expectedUserID = 1;

            target.Update(testItem, expectedUserID);
        }

        #endregion Update_MethodTests

        // Delete fails when id outside of logical range.
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Delete_FailsOnNegativeID()
        {
            var target = GetTargetLibrary();
            long objectID = -1;
            long expectedUserID = 1;

            target.Delete(objectID, expectedUserID);
        }

        // Delete fails when id outside of logical range.
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Delete_FailsOnZeroID()
        {
            var target = GetTargetLibrary();
            long objectID = 0;
            long expectedUserID = 1;

            target.Delete(objectID, expectedUserID);
        }

        // Delete fails when no item can be found for ID
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Delete_FailsOnMissingObject()
        {
            var mockData = new RepositoryMocker<ObType>();
            mockData.IsItemFound = false;

            var target = GetTargetLibrary(mockData);

            long objectID = 13;
            long expectedUserID = 1;

            target.Delete(objectID, expectedUserID);
        }

        // Delete calls Repository delete
        [TestMethod]
        public virtual void ObjectLibrary_Delete_CallsRepositoryDelete()
        {
            var mockData = new RepositoryMocker<ObType>();
            mockData.IsItemFound = true;

            var subject = GetValidObjectInstance(false);
            subject.ID = mockData.GetNextID();

            mockData.items.Add(subject);

            var target = GetTargetLibrary(mockData);

            long objectID = subject.ID;
            long expectedUserID = 1;

            target.Delete(objectID, expectedUserID);

            Assert.AreEqual(objectID, mockData.LastIDDeleted, "Last deleted ID doesn't seem to match the ID deleted.");
        }

        // Delete calls Repository Save operation
        [TestMethod]
        public void ObjectLibrary_Delete_CallsRepositorySave()
        {
            var mockData = new RepositoryMocker<ObType>();
            mockData.IsItemFound = true;

            var subject = GetValidObjectInstance(false);
            subject.ID = mockData.GetNextID();

            mockData.items.Add(subject);

            var target = GetTargetLibrary(mockData);

            long objectID = subject.ID;
            long expectedUserID = 1;

            target.Delete(objectID, expectedUserID);

            Assert.IsTrue(mockData.ChangesSaved, "Delete did not call the Repository Save operation.");
        }

        // TODO:  Test Cases for Delete Operation
        // Delete fails when userID not valid.
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Delete_FailsOnNegativeUserID()
        {
            var mockData = new RepositoryMocker<ObType>();
            var testItem = GetValidObjectInstance(false);
            testItem.ID = mockData.GetNextID();
            mockData.items.Add(testItem);

            var objectID = testItem.ID;

            var target = GetTargetLibrary(mockData);

            long expectedUserID = -1;

            target.Delete(objectID, expectedUserID);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Delete_FailsOnZeroUserID()
        {
            var mockData = new RepositoryMocker<ObType>();
            var testItem = GetValidObjectInstance(false);
            testItem.ID = mockData.GetNextID();
            mockData.items.Add(testItem);

            var objectID = testItem.ID;

            var target = GetTargetLibrary(mockData);

            long expectedUserID = 0;

            target.Delete(objectID, expectedUserID);
        }

        // Delete fails when user not found
        [TestMethod]
        [ExpectedException(typeof(UserPermissionsException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Delete_FailsOnUserIDNotFound()
        {
            var mockData = new RepositoryMocker<ObType>();
            var testItem = GetValidObjectInstance(false);
            testItem.ID = mockData.GetNextID();
            mockData.items.Add(testItem);

            var objectID = testItem.ID;

            var target = GetTargetLibrary(mockData);
            var permMan = new PermissionManagerMocker();
            permMan.ThrowNotFoundUserException = true;

            SetLibraryPermMananger(target, permMan);

            long expectedUserID = 1;

            target.Delete(objectID, expectedUserID);
        }

        // Delete fails when user does not have rights.
        [TestMethod]
        [ExpectedException(typeof(UserPermissionsException), AllowDerivedTypes = true)]
        public void ObjectLibrary_Delete_FailWhenUserActionNotPermitted()
        {
            var mockData = new RepositoryMocker<ObType>();
            var testItem = GetValidObjectInstance(false);
            testItem.ID = mockData.GetNextID();
            mockData.items.Add(testItem);

            var objectID = testItem.ID;

            var target = GetTargetLibrary(mockData);
            var permMan = new PermissionManagerMocker();
            permMan.ApproveActions = false;

            SetLibraryPermMananger(target, permMan);

            long expectedUserID = 1;

            target.Delete(objectID, expectedUserID);
        }

        // Validate fails on item Null
        [TestMethod]
        public void ObjectLibrary_Validate_InvalidItemReturnsInvalidResult()
        {
            var dataMock = new RepositoryMocker<ObType>();
            dataMock.IsItemFound = false;

            var target = GetTargetLibrary(dataMock);

            var expectedItem = GetInvalidObjectInstance(true);
            var expectedIsValid = false;

            var actual = target.Validate(expectedItem);

            Assert.AreEqual(expectedIsValid, actual.IsValid);
        }
    }
}