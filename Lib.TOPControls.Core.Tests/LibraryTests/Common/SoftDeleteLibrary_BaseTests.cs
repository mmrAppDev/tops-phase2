﻿using System;
using Lib.TOPControls.Exceptions;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMRCommon.Data;

namespace Lib.TOPControls.Tests.LibraryTests.Common
{
    [TestClass]
    public abstract class SoftDeleteLibrary_BaseTests<SDType> : ObjectLibrary_BaseTests<SDType>
        where SDType : DomainObject, ISoftDeleted, new()
    {
        public abstract ISoftDeleteLibrary<SDType> GetTargetAsSDLibrary(
            IRepository<SDType> useRepository = null);

        [TestMethod]
        public void SDLibrary_Find_ReturnsNullForDeleted()
        {
            var mockData = new RepositoryMocker<SDType>();
            mockData.IsItemFound = true;

            var subject = GetValidObjectInstance(false);
            subject.ID = mockData.GetNextID();
            subject.SetDeletedState(true);
            mockData.items.Add(subject);

            var target = GetTargetLibrary(mockData);
            var objectID = subject.ID;

            var result = target.Find(objectID);

            Assert.IsNull(result, "Find operation should not return an item that has been marked deleted.");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void SDLibrary_Add_FailsOnDeletedItem()
        {
            var mockData = new RepositoryMocker<SDType>();
            var subject = GetValidObjectInstance(true);
            subject.SetDeletedState(true);
            var expectedUserID = 1;

            var target = GetTargetLibrary(mockData);

            var result = target.Add(subject, expectedUserID);
        }

        // Update cannot change an item marked as deleted.
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void SDLibrary_Update_CannotChangeDeletedState_ToDeleted()
        {
            var mockData = new RepositoryMocker<SDType>();
            var subject = GetValidObjectInstance(false);
            subject.ID = mockData.GetNextID();
            mockData.items.Add(subject);
            var objectID = subject.ID;

            var expectedUserID = 1;

            var target = GetTargetLibrary(mockData);

            var item = target.Find(objectID);

            item.SetDeletedState(true);

            var result = target.Update(subject, expectedUserID);
        }

        // TODO:  Test Cases for Delete

        // Disable CallsRepositoryDelete Test

        public override void ObjectLibrary_Delete_CallsRepositoryDelete()
        {
            Assert.IsInstanceOfType(GetTargetLibrary(), typeof(ISoftDeleteLibrary<SDType>));
        }

        // Does not remove item from Repository
        [TestMethod]
        public void SDLibrary_Delete_DoesNotCallRepositoryDelete()
        {
            var mockData = new RepositoryMocker<SDType>();
            var subject = GetValidObjectInstance(false);

            subject.ID = mockData.GetNextID();
            mockData.items.Add(subject);

            var expectedLastIDDeleted = 0;
            mockData.LastIDDeleted = expectedLastIDDeleted;     // initialize

            var objectID = subject.ID;

            var expectedUserID = 1;

            var target = GetTargetLibrary(mockData);

            target.Delete(objectID, expectedUserID);

            Assert.AreEqual(expectedLastIDDeleted, mockData.LastIDDeleted, "Soft delete called Repository delete when it shouldn't have.");
        }

        // Marks item as deleted
        [TestMethod]
        public void SDLibrary_Delete_MarksItemAsDeleted()
        {
            var mockData = new RepositoryMocker<SDType>();
            var subject = GetValidObjectInstance(false);

            subject.ID = mockData.GetNextID();
            mockData.items.Add(subject);

            var objectID = subject.ID;

            var expectedUserID = 1;

            var target = GetTargetLibrary(mockData);

            target.Delete(objectID, expectedUserID);

            Assert.IsTrue(mockData.ChangesSaved, "Repository save not called for soft delete.");

            var delItem = mockData.Find(objectID);

            Assert.IsNotNull(delItem, "Soft Delete removed item from test Repository.");
            Assert.IsTrue(delItem.IsDeleted, "Soft Delete failed to mark item as deleted.");
        }

        // Does nothing to items already marked as deleted.

        // TODO:  Test Cases for FindAny
        // Fails on negative id
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void SDLibrary_FindAny_FailsOnNegativeID()
        {
            var target = GetTargetAsSDLibrary();

            var objectID = -1;
            long expectedUserID = 1;

            target.FindAny(objectID, expectedUserID);
        }

        // Fails on zero id
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void SDLibrary_FindAny_FailsOnZeroID()
        {
            var target = GetTargetAsSDLibrary();
            var objectID = 0;
            long expectedUserID = 1;

            target.FindAny(objectID, expectedUserID);
        }

        // Fails on negative user id
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void SDLibrary_FindAny_FailsOnNegativeUserID()
        {
            var target = GetTargetAsSDLibrary();
            var expectedUserID = -1;
            var objectID = 1;

            target.FindAny(objectID, expectedUserID);
        }

        // Fails on zero user id
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void SDLibrary_FindAny_FailsOnZeroUserID()
        {
            var target = GetTargetAsSDLibrary();
            var objectID = 1;
            long expectedUserID = 0;

            target.FindAny(objectID, expectedUserID);
        }

        // Returns null if item not found.
        [TestMethod]
        public void SDLibrary_FindAny_ReturnsNullIfNoItemFound()
        {
            var dataMock = new RepositoryMocker<SDType>();
            dataMock.IsItemFound = false;   // guarantee the Repository will not return a value.

            var objectID = 23;
            var expectedUserID = 1;
            var target = GetTargetAsSDLibrary(dataMock);

            var result = target.FindAny(objectID, expectedUserID);

            Assert.IsNull(result);
        }

        // Returns instance if deleted item found.
        [TestMethod]
        public void SDLibrary_FindAny_ReturnsInstanceForDeleted()
        {
            var mockData = new RepositoryMocker<SDType>();

            var subject = GetValidObjectInstance(false);
            subject.ID = mockData.GetNextID();
            subject.SetDeletedState(true);
            mockData.items.Add(subject);

            var target = GetTargetAsSDLibrary(mockData);
            var objectID = subject.ID;
            var expectedUserID = 1;

            var result = target.FindAny(objectID, expectedUserID);

            Assert.IsNotNull(result, "Find operation should return an item that has been marked deleted.");
            Assert.IsTrue(result.IsDeleted, "Item is no longer marked as deleted.");
        }

        // Returns instance if not deleted item found.
        [TestMethod]
        public void SDLibrary_FindAny_ReturnsInstanceForNeverDeleted()
        {
            var mockData = new RepositoryMocker<SDType>();

            var subject = GetValidObjectInstance(false);
            subject.ID = mockData.GetNextID();
            mockData.items.Add(subject);

            var target = GetTargetAsSDLibrary(mockData);
            var objectID = subject.ID;
            var expectedUserID = 1;

            var result = target.FindAny(objectID, expectedUserID);

            Assert.IsNotNull(result, "Find operation should return an item that has never been marked deleted.");
            Assert.IsFalse(result.IsDeleted, "Item's deleted state has changed.");
        }

        // Uses Repository Find operation.
        [TestMethod]
        public void SDLibrary_FindAny_UsesRepositoryFind()
        {
            var dataMock = new RepositoryMocker<SDType>();
            dataMock.IsItemFound = true;   // guarantee the Repository will find the value.
            dataMock.LastIDFound = 0;      // reset the last ID found to the baseline

            var expectedID = 23;
            var expectedUserID = 1;
            var target = GetTargetAsSDLibrary(dataMock);

            var result = target.FindAny(expectedID, expectedUserID);

            var foundID = dataMock.LastIDFound;

            Assert.AreEqual(expectedID, foundID, "Repository not used for find operation.");

            Assert.IsNotNull(result);
        }

        // Fails if user id not for real user
        [TestMethod]
        [ExpectedException(typeof(UserPermissionsException), AllowDerivedTypes = true)]
        public void SDLibrary_FindAny_FailsIfUserIDNotForRealUser()
        {
            var dataMock = new RepositoryMocker<SDType>();
            dataMock.IsItemFound = true;   // guarantee the Repository will find the value.

            var permMan = new PermissionManagerMocker();
            permMan.ThrowNotFoundUserException = true;

            var expectedID = 23;
            var expectedUserID = 1;
            var target = GetTargetAsSDLibrary(dataMock);
            SetLibraryPermMananger(target, permMan);

            var result = target.FindAny(expectedID, expectedUserID);
        }

        // Fails if user does not have permissions.
        [TestMethod]
        [ExpectedException(typeof(UserPermissionsException), AllowDerivedTypes = true)]
        public void SDLibrary_FindAny_FailsIfUserActionNotPermitted()
        {
            var dataMock = new RepositoryMocker<SDType>();
            dataMock.IsItemFound = true;   // guarantee the Repository will find the value.

            var permMan = new PermissionManagerMocker();
            permMan.ApproveActions = false;

            var expectedID = 23;
            var expectedUserID = 1;
            var target = GetTargetAsSDLibrary(dataMock);
            SetLibraryPermMananger(target, permMan);

            var result = target.FindAny(expectedID, expectedUserID);
        }

        // TODO:  Test Cases for Undelete
        // Fails on negative id
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SDLibrary_Undelete_FailsOnNegativeID()
        {
            var target = GetTargetAsSDLibrary();

            var objectID = -1;
            long expectedUserID = 1;

            target.Undelete(objectID, expectedUserID);
        }

        // Fails on zero id
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SDLibrary_Undelete_FailsOnZeroID()
        {
            var dataMock = new RepositoryMocker<SDType>();
            dataMock.IsItemFound = true;   // guarantee the Repository will find the value.
            var target = GetTargetAsSDLibrary();

            var objectID = 0;
            long expectedUserID = 1;

            target.Undelete(objectID, expectedUserID);
        }

        // Fails on negative user id
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SDLibrary_Undelete_FailsOnNegativeUserID()
        {
            var dataMock = new RepositoryMocker<SDType>();
            dataMock.IsItemFound = true;   // guarantee the Repository will find the value.
            var target = GetTargetAsSDLibrary(dataMock);

            var objectID = 26;
            long expectedUserID = -1;

            target.Undelete(objectID, expectedUserID);
        }

        // Fails on zero user id
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SDLibrary_Undelete_FailsOnZeroUserID()
        {
            var dataMock = new RepositoryMocker<SDType>();
            dataMock.IsItemFound = true;   // guarantee the Repository will find the value.
            var target = GetTargetAsSDLibrary(dataMock);

            var objectID = 26;
            long expectedUserID = 0;

            target.Undelete(objectID, expectedUserID);
        }

        // Uses Repository to update item.
        [TestMethod]
        public void SDLibrary_Undelete_UsesRepositoryToUpdate()
        {
            var dataMock = new RepositoryMocker<SDType>();
            var targetItem = GetValidObjectInstance(false);
            targetItem.ID = dataMock.GetNextID();
            targetItem.SetDeletedState(true);
            dataMock.items.Add(targetItem);

            var target = GetTargetAsSDLibrary(dataMock);

            var objectID = targetItem.ID;
            long expectedUserID = 13;

            target.Undelete(objectID, expectedUserID);

            Assert.AreEqual(objectID, dataMock.LastIDFound, "Library did not retrieve the data item from the Repository.");
            Assert.IsTrue(dataMock.ChangesSaved, "Did not save changes to Repository.");

            var result = target.FindAny(objectID, expectedUserID);

            Assert.IsNotNull(result, "Undelete seems to have removed item from list.");
            Assert.IsFalse(result.IsDeleted, "Undelete did not change deletion status.");
        }

        // Does nothing on non-deleted items.
        [TestMethod]
        public void SDLibrary_Undelete_DoesNothingIfIsDeletedFalse()
        {
            var dataMock = new RepositoryMocker<SDType>();
            var targetItem = GetValidObjectInstance(false);
            targetItem.ID = dataMock.GetNextID();
            dataMock.items.Add(targetItem);

            var target = GetTargetAsSDLibrary(dataMock);

            var objectID = targetItem.ID;
            long expectedUserID = 13;

            target.Undelete(objectID, expectedUserID);

            Assert.AreEqual(objectID, dataMock.LastIDFound, "Library did not retrieve the data item from the Repository.");
            Assert.IsFalse(dataMock.ChangesSaved, "Detected incorrect Repository save operation.");

            var result = target.FindAny(objectID, expectedUserID);

            Assert.IsNotNull(result, "Undelete seems to have removed item from list.");
            Assert.IsFalse(result.IsDeleted, "Undelete changed deletion status of undeleted item.");
        }

        // Fails if item not found.
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void SDLibrary_Undelete_FailsIfItemNotFound()
        {
            var dataMock = new RepositoryMocker<SDType>();
            dataMock.IsItemFound = false;

            var target = GetTargetAsSDLibrary(dataMock);

            var objectID = 26;
            long expectedUserID = 13;

            target.Undelete(objectID, expectedUserID);
        }

        // Fails if user id not for real user
        [TestMethod]
        [ExpectedException(typeof(UserPermissionsException), AllowDerivedTypes = true)]
        public void SDLibrary_Undelete_FailsIfUserIDNotForRealUser()
        {
            var dataMock = new RepositoryMocker<SDType>();
            dataMock.IsItemFound = true;   // guarantee the Repository will find the value.
            var tempItem = GetValidObjectInstance(false);
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            var permMan = new PermissionManagerMocker();
            permMan.ThrowNotFoundUserException = true;

            var expectedID = tempItem.ID;
            var expectedUserID = 1;
            var target = GetTargetAsSDLibrary(dataMock);
            SetLibraryPermMananger(target, permMan);

            var result = target.Undelete(expectedID, expectedUserID);
        }

        // Fails if user does not have permissions.
        [TestMethod]
        [ExpectedException(typeof(UserPermissionsException), AllowDerivedTypes = true)]
        public void SDLibrary_Undelete_FailsIfUserActionNotPermitted()
        {
            var dataMock = new RepositoryMocker<SDType>();
            dataMock.IsItemFound = true;   // guarantee the Repository will find the value.
            var tempItem = GetValidObjectInstance(false);
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            var permMan = new PermissionManagerMocker();
            permMan.ApproveActions = false;

            var expectedID = tempItem.ID;
            var expectedUserID = 1;
            var target = GetTargetAsSDLibrary(dataMock);
            SetLibraryPermMananger(target, permMan);

            var result = target.Undelete(expectedID, expectedUserID);
        }

        // TODO:  Test Cases for Destroy
        // Fails on negative id
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SDLibrary_Destroy_FailsOnNegativeID()
        {
            var target = GetTargetAsSDLibrary();

            var objectID = -1;
            long expectedUserID = 1;

            target.Destroy(objectID, expectedUserID);
        }

        // Fails on zero id
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SDLibrary_Destroy_FailsOnZeroID()
        {
            var target = GetTargetAsSDLibrary();

            var objectID = 0;
            long expectedUserID = 1;

            target.Destroy(objectID, expectedUserID);
        }

        // Fails on negative user id
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SDLibrary_Destroy_FailsOnNegativeUserID()
        {
            var dataMock = new RepositoryMocker<SDType>();
            var tempItem = GetValidObjectInstance(false);
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            var target = GetTargetAsSDLibrary(dataMock);

            var objectID = tempItem.ID;
            long expectedUserID = -11;

            target.Destroy(objectID, expectedUserID);
        }

        // Fails on zero user id
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SDLibrary_Destroy_FailsOnZeroUserID()
        {
            var dataMock = new RepositoryMocker<SDType>();
            var tempItem = GetValidObjectInstance(false);
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            var target = GetTargetAsSDLibrary(dataMock);

            var objectID = tempItem.ID;
            long expectedUserID = 0;

            target.Destroy(objectID, expectedUserID);
        }

        // Uses Repository to remove item.
        [TestMethod]
        public void SDLibrary_Destroy_UsesRepositoryToRemoveItem()
        {
            var dataMock = new RepositoryMocker<SDType>();
            var targetItem = GetValidObjectInstance(false);
            targetItem.ID = dataMock.GetNextID();
            targetItem.SetDeletedState(true);
            dataMock.items.Add(targetItem);

            var target = GetTargetAsSDLibrary(dataMock);

            var objectID = targetItem.ID;
            long expectedUserID = 13;

            target.Destroy(objectID, expectedUserID);

            Assert.AreEqual(objectID, dataMock.LastIDFound, "Library did not retrieve the data item from the Repository.");
            Assert.AreEqual(objectID, dataMock.LastIDDeleted, "Library did not call Repository delete method.");
            Assert.IsTrue(dataMock.ChangesSaved, "Did not save changes to Repository.");
        }

        // Does nothing on non-deleted items.
        [TestMethod]
        public void SDLibrary_Destroy_DoesNothingOnItemsNeverDeleted()
        {
            var dataMock = new RepositoryMocker<SDType>();
            var targetItem = GetValidObjectInstance(false);
            targetItem.ID = dataMock.GetNextID();
            dataMock.items.Add(targetItem);

            var expectedLastIDDeleted = 0;

            // Set baselines
            dataMock.LastIDDeleted = expectedLastIDDeleted;
            dataMock.ChangesSaved = false;

            var target = GetTargetAsSDLibrary(dataMock);

            var objectID = targetItem.ID;
            long expectedUserID = 13;

            target.Destroy(objectID, expectedUserID);

            Assert.AreEqual(objectID, dataMock.LastIDFound, "Library did not retrieve the data item from the Repository.");
            Assert.AreEqual(expectedLastIDDeleted, dataMock.LastIDDeleted, "Library did not call Repository delete method.");
            Assert.IsFalse(dataMock.ChangesSaved, "Performed Repository save without valid changes.");
        }

        // Fails if item not found.
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void SDLibrary_Destroy_FailsIfItemNotFound()
        {
            var dataMock = new RepositoryMocker<SDType>();
            dataMock.IsItemFound = false;

            var target = GetTargetAsSDLibrary(dataMock);

            var objectID = 1;
            long expectedUserID = 13;

            target.Destroy(objectID, expectedUserID);
        }

        // Fails if user id not found.
        [TestMethod]
        [ExpectedException(typeof(UserPermissionsException), AllowDerivedTypes = true)]
        public void SDLibrary_Destroy_FailsIfUserIDNotFound()
        {
            var dataMock = new RepositoryMocker<SDType>();
            var tempItem = GetValidObjectInstance(false);
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            var permMan = new PermissionManagerMocker();
            permMan.ThrowNotFoundUserException = true;

            var target = GetTargetAsSDLibrary(dataMock);
            SetLibraryPermMananger(target, permMan);

            var objectID = 1;
            long expectedUserID = 13;

            target.Destroy(objectID, expectedUserID);
        }

        // Fails if user does not have permissions.
        [TestMethod]
        [ExpectedException(typeof(UserPermissionsException), AllowDerivedTypes = true)]
        public void SDLibrary_Destroy_FailsIfUserActionNotPermitted()
        {
            var dataMock = new RepositoryMocker<SDType>();
            var tempItem = GetValidObjectInstance(false);
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            var permMan = new PermissionManagerMocker();
            permMan.ApproveActions = false;

            var target = GetTargetAsSDLibrary(dataMock);
            SetLibraryPermMananger(target, permMan);

            var objectID = 1;
            long expectedUserID = 13;

            target.Destroy(objectID, expectedUserID);
        }
    }
}