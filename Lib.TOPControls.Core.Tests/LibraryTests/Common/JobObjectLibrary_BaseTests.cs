﻿using System;
using System.Linq;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMRCommon.Data;

namespace Lib.TOPControls.Tests.LibraryTests.Common
{
    [TestClass]
    public abstract class JobObjectLibrary_BaseTests<DJO> : ObjectLibrary_BaseTests<DJO>
        where DJO : DomainJobObject, new()
    {
        public abstract IJobObjectLibrary<DJO> GetTargetAsJObLibrary(
            IRepository<DJO> useRepository = null);

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public virtual void JOLibrary_AllForJob_NegativeJob()
        {
            var target = GetTargetAsJObLibrary();
            var jobIDArgument = -1;

            target.AllForJob(jobIDArgument);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public virtual void JOLibrary_AllForJob_ZeroJobID()
        {
            var target = GetTargetAsJObLibrary();
            var jobIDArgument = 0;

            target.AllForJob(jobIDArgument);
        }

        [TestMethod]
        public virtual void JOLibrary_AllForJob_EmptyRepReturnsEmptyResult()
        {
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();
            var target = GetTargetAsJObLibrary(dataMock);

            var resultList = target.AllForJob(1);
            var expectedResultCount = 0;

            Assert.AreEqual(expectedResultCount, resultList.Count());
        }

        [TestMethod]
        public virtual void JOLibrary_AllForJob_IgnoresOtherJob()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 1;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 2;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 5;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            var target = GetTargetAsJObLibrary(dataMock);

            var resultList = target.AllForJob(targetJobID);
            var expectedResultCount = 0;

            Assert.AreEqual(expectedResultCount, resultList.Count());
        }

        [TestMethod]
        public virtual void JOLibrary_AllForJob_ReturnsMatchingJobInstances()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            var expectedResultID1 = tempItem.ID;

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            var expectedResultID2 = tempItem.ID;

            var target = GetTargetAsJObLibrary(dataMock);

            var resultList = target.AllForJob(targetJobID);
            var expectedResultCount = 2;

            Assert.AreEqual(expectedResultCount, resultList.Count());

            var item1match = resultList.SingleOrDefault(djo => djo.ID == expectedResultID1);
            Assert.IsNotNull(item1match, "Did not return the expected result value.");

            var item2match = resultList.SingleOrDefault(djo => djo.ID == expectedResultID2);
            Assert.IsNotNull(item2match, "Did not return the expected result value.");
        }

        // Test that no results are returned if the job is deleted.
        [TestMethod]
        public virtual void JOLibrary_AllForJob_ReturnsEmptyForDeletedJob()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            var jobDataMock = new RepositoryMocker<JobInfo>();
            var jobLib = new JobInfoLibrary(jobDataMock, new PermissionManagerMocker());
            var delJob = new JobInfo { ID = targetJobID, Title = "Cancelled job" };
            delJob.SetDeletedState(true);
            jobDataMock.items.Add(delJob);
            var rootLib = new TOPControlLibraryMocker();
            rootLib.JobActions = jobLib;
            rootLib.PermissionManager = jobLib.PermManager;
            jobLib.Root = rootLib;

            var target = GetTargetAsJObLibrary(dataMock) as JobObjectLibraryBase<DJO>;
            target.Root = rootLib;

            var resultList = target.AllForJob(targetJobID);
            var expectedResultCount = 0;

            Assert.AreEqual(expectedResultCount, resultList.Count(), "Results returned for deleted job for type [{0}].", typeof(DJO));
        }
    }
}