﻿using System;
using System.Linq;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Exceptions;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMRCommon.Data;

namespace Lib.TOPControls.Tests.LibraryTests.Common
{
    [TestClass]
    public abstract class JobSoftDeleteLibrary_BaseTests<DJO>
        : SoftDeleteLibrary_BaseTests<DJO>
        where DJO : DomainJobObject, ISoftDeleted, new()
    {
        protected override IObjectLibrary<DJO> GetTargetLibrary()
        {
            return GetTargetAsJObLibrary();
        }

        protected override IObjectLibrary<DJO> GetTargetLibrary(MMRCommon.Data.IRepository<DJO> useRepository)
        {
            return GetTargetAsJObLibrary(useRepository);
        }

        public abstract JobSoftDeleteLibraryBase<DJO> GetTargetAsJObLibrary(
            IRepository<DJO> useRepository = null);

        private const long VALIDUSERID = 1;
        private const long NEGATIVEUSERID = -1;
        private const long ZEROUSERID = 0;

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public virtual void JSDLibrary_AllForJob_NegativeJob()
        {
            var target = GetTargetAsJObLibrary();
            var jobIDArgument = -1;

            target.AllForJob(jobIDArgument);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public virtual void JSDLibrary_AllForJob_ZeroJobID()
        {
            var target = GetTargetAsJObLibrary();
            var jobIDArgument = 0;

            target.AllForJob(jobIDArgument);
        }

        [TestMethod]
        public virtual void JSDLibrary_AllForJob_EmptyRepReturnsEmptyResult()
        {
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();
            var target = GetTargetAsJObLibrary(dataMock);

            var resultList = target.AllForJob(1);
            var expectedResultCount = 0;

            Assert.AreEqual(expectedResultCount, resultList.Count());
        }

        [TestMethod]
        public virtual void JSDLibrary_AllForJob_IgnoresOtherJob()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 1;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 2;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 5;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            var target = GetTargetAsJObLibrary(dataMock);

            var resultList = target.AllForJob(targetJobID);
            var expectedResultCount = 0;

            Assert.AreEqual(expectedResultCount, resultList.Count());
        }

        [TestMethod]
        public virtual void JSDLibrary_AllForJob_ReturnsMatchingJobInstances()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            var expectedResultID1 = tempItem.ID;

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            var expectedResultID2 = tempItem.ID;

            var target = GetTargetAsJObLibrary(dataMock);

            var resultList = target.AllForJob(targetJobID);
            var expectedResultCount = 2;

            Assert.AreEqual(expectedResultCount, resultList.Count());

            var item1match = resultList.SingleOrDefault(djo => djo.ID == expectedResultID1);
            Assert.IsNotNull(item1match, "Did not return the expected result value.");

            var item2match = resultList.SingleOrDefault(djo => djo.ID == expectedResultID2);
            Assert.IsNotNull(item2match, "Did not return the expected result value.");
        }

        // Test that no results are returned if the job is deleted.
        [TestMethod]
        public virtual void JSDLibrary_AllForJob_ReturnsEmptyForDeletedJob()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            var jobDataMock = new RepositoryMocker<JobInfo>();
            var jobLib = new JobInfoLibrary(jobDataMock, new PermissionManagerMocker());
            var delJob = new JobInfo { ID = targetJobID, Title = "Cancelled job" };
            delJob.SetDeletedState(true);
            jobDataMock.items.Add(delJob);
            var rootLib = new TOPControlLibraryMocker();
            rootLib.JobActions = jobLib;
            rootLib.PermissionManager = jobLib.PermManager;
            jobLib.Root = rootLib;

            var target = GetTargetAsJObLibrary(dataMock) as JobSoftDeleteLibraryBase<DJO>;
            target.Root = rootLib;

            var resultList = target.AllForJob(targetJobID);
            var expectedResultCount = 0;

            Assert.AreEqual(expectedResultCount, resultList.Count(), "Results returned for deleted job for type [{0}].", typeof(DJO));
        }

        // Add tests to check for deleted item behavior:

        //      Does not show deleted items.
        [TestMethod]
        public virtual void JSDLibrary_AllForJob_DoeNotReturnItemsMarkedDeleted()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            var unexpectedResultID1 = tempItem.ID;

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(false);
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            var unexpectedResultID2 = tempItem.ID;

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(false);
            dataMock.items.Add(tempItem);

            var target = GetTargetAsJObLibrary(dataMock);

            var resultList = target.AllForJob(targetJobID);
            var expectedResultCount = 2;

            Assert.AreEqual(expectedResultCount, resultList.Count());
            var item1match = resultList.SingleOrDefault(djo => djo.ID == unexpectedResultID1);
            Assert.IsNull(item1match, "Returned unexpected value.");

            var item2match = resultList.SingleOrDefault(djo => djo.ID == unexpectedResultID2);
            Assert.IsNull(item2match, "Returned unexpected value.");
        }

        //      Shows all non-deleted items.
        [TestMethod]
        public virtual void JSDLibrary_AllForJob_ReturnAllItemsNotMarkedDeleted()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(false);
            dataMock.items.Add(tempItem);

            var expectedResultID1 = tempItem.ID;

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(false);
            dataMock.items.Add(tempItem);

            var expectedResultID2 = tempItem.ID;

            var target = GetTargetAsJObLibrary(dataMock);

            var resultList = target.AllForJob(targetJobID);
            var expectedResultCount = 2;

            Assert.AreEqual(expectedResultCount, resultList.Count());

            var item1match = resultList.SingleOrDefault(djo => djo.ID == expectedResultID1);
            Assert.IsNotNull(item1match, "Did not return the expected result value.");

            var item2match = resultList.SingleOrDefault(djo => djo.ID == expectedResultID2);
            Assert.IsNotNull(item2match, "Did not return the expected result value.");
        }

        // Add a similar set of tests for AllForJobAny, including
        //      userID validation and inclusion of deleted records.

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public virtual void JSDLibrary_AllForJobAny_NegativeJob()
        {
            var target = GetTargetAsJObLibrary();
            var jobIDArgument = -1;

            target.AllForJobAny(jobIDArgument, VALIDUSERID);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public virtual void JSDLibrary_AllForJobAny_ZeroJobID()
        {
            var target = GetTargetAsJObLibrary();
            var jobIDArgument = 0;

            target.AllForJobAny(jobIDArgument, VALIDUSERID);
        }

        [TestMethod]
        public virtual void JSDLibrary_AllForJobAny_EmptyRepReturnsEmptyResult()
        {
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();
            var target = GetTargetAsJObLibrary(dataMock);

            var resultList = target.AllForJobAny(1, VALIDUSERID);
            var expectedResultCount = 0;

            Assert.AreEqual(expectedResultCount, resultList.Count());
        }

        [TestMethod]
        public virtual void JSDLibrary_AllForJobAny_IgnoresOtherJob()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 1;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 2;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 5;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            var target = GetTargetAsJObLibrary(dataMock);

            var resultList = target.AllForJobAny(targetJobID, VALIDUSERID);
            var expectedResultCount = 0;

            Assert.AreEqual(expectedResultCount, resultList.Count());
        }

        [TestMethod]
        public virtual void JSDLibrary_AllForJobAny_ReturnsMatchingJobInstances()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            var expectedResultID1 = tempItem.ID;

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            var expectedResultID2 = tempItem.ID;

            var target = GetTargetAsJObLibrary(dataMock);

            var resultList = target.AllForJobAny(targetJobID, VALIDUSERID);
            var expectedResultCount = 2;

            Assert.AreEqual(expectedResultCount, resultList.Count());

            var item1match = resultList.SingleOrDefault(djo => djo.ID == expectedResultID1);
            Assert.IsNotNull(item1match, "Did not return the expected result value.");

            var item2match = resultList.SingleOrDefault(djo => djo.ID == expectedResultID2);
            Assert.IsNotNull(item2match, "Did not return the expected result value.");
        }

        // Test that all results are returned if the job is deleted.
        [TestMethod]
        public virtual void JSDLibrary_AllForJobAny_ReturnsAllForDeletedJob()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 4;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            dataMock.items.Add(tempItem);

            var jobDataMock = new RepositoryMocker<JobInfo>();
            var jobLib = new JobInfoLibrary(jobDataMock, new PermissionManagerMocker());
            var delJob = new JobInfo { ID = targetJobID, Title = "Cancelled job" };
            delJob.SetDeletedState(true);
            jobDataMock.items.Add(delJob);
            var rootLib = new TOPControlLibraryMocker();
            rootLib.JobActions = jobLib;
            rootLib.PermissionManager = jobLib.PermManager;
            jobLib.Root = rootLib;

            var target = GetTargetAsJObLibrary(dataMock) as JobSoftDeleteLibraryBase<DJO>;
            target.Root = rootLib;

            var resultList = target.AllForJobAny(targetJobID, VALIDUSERID);
            var expectedResultCount = 2;

            Assert.AreEqual(expectedResultCount, resultList.Count(), "Results not returned for deleted job for type [{0}].", typeof(DJO));
        }

        // Add tests to check for deleted item behavior:

        //      Does not show deleted items.
        [TestMethod]
        public virtual void JSDLibrary_AllForJobAny_REturnsAllItemsMarkedDeleted()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(false);
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(false);
            dataMock.items.Add(tempItem);

            var target = GetTargetAsJObLibrary(dataMock);

            var resultList = target.AllForJobAny(targetJobID, VALIDUSERID);
            var expectedResultCount = dataMock.items.Count(x => x.JobID == targetJobID);

            Assert.AreEqual(expectedResultCount, resultList.Count());

            foreach (var thisItem in dataMock.items.Where(x => x.IsDeleted == true && x.JobID == targetJobID))
            {
                var itemMatch = resultList.SingleOrDefault(djo => djo.ID == thisItem.ID);
                Assert.IsNotNull(itemMatch, "Failed to return expected deleted item for class {0}.", typeof(DJO));
            }
        }

        //      Shows all non-deleted items.
        [TestMethod]
        public virtual void JSDLibrary_AllForJobAny_ReturnAllItemsNotMarkedDeleted()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(false);
            dataMock.items.Add(tempItem);

            var expectedResultID1 = tempItem.ID;

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(false);
            dataMock.items.Add(tempItem);

            var expectedResultID2 = tempItem.ID;

            var target = GetTargetAsJObLibrary(dataMock);

            var resultList = target.AllForJobAny(targetJobID, VALIDUSERID);
            var expectedResultCount = dataMock.items.Count(x => x.JobID == targetJobID);

            Assert.AreEqual(expectedResultCount, resultList.Count());

            foreach (var thisItem in dataMock.items.Where(x => x.IsDeleted == false && x.JobID == targetJobID))
            {
                var itemMatch = resultList.SingleOrDefault(djo => djo.ID == thisItem.ID);
                Assert.IsNotNull(itemMatch, "Failed to return expected non-deleted item for class {0}.", typeof(DJO));
            }
        }

        // user ID and permission validation

        // Fails on Negative UserID
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public virtual void JSDLibrary_AllForJobAny_FailsOnNegativeUserID()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            var target = GetTargetAsJObLibrary(dataMock);

            target.AllForJobAny(targetJobID, NEGATIVEUSERID);
        }

        // Fails on Zero user ID
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public virtual void JSDLibrary_AllForJobAny_FailsOnZeroUserID()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            var target = GetTargetAsJObLibrary(dataMock);

            target.AllForJobAny(targetJobID, ZEROUSERID);
        }

        // Fails on UserID not found
        [TestMethod]
        [ExpectedException(typeof(UserPermissionsException), AllowDerivedTypes = true)]
        public virtual void JSDLibrary_AllForJobAny_FailsOnUserIDNotFound()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            var permMan = new PermissionManagerMocker();
            permMan.ThrowNotFoundUserException = true;

            var target = GetTargetAsJObLibrary(dataMock);
            SetLibraryPermMananger(target, permMan);

            target.AllForJobAny(targetJobID, VALIDUSERID);
        }

        // Fails on UserAction not Permitted
        [TestMethod]
        [ExpectedException(typeof(UserPermissionsException), AllowDerivedTypes = true)]
        public virtual void JSDLibrary_AllForJobAny_FailsOnUserActionNotPermitted()
        {
            var targetJobID = 3;
            var dataMock = new RepositoryMocker<DJO>();
            dataMock.items = new System.Collections.Generic.List<DJO>();

            var tempItem = GetValidObjectInstance(false);
            tempItem.JobID = 3;
            tempItem.ID = dataMock.GetNextID();
            tempItem.SetDeletedState(true);
            dataMock.items.Add(tempItem);

            var permMan = new PermissionManagerMocker();
            permMan.ApproveActions = false;

            var target = GetTargetAsJObLibrary(dataMock);
            SetLibraryPermMananger(target, permMan);

            target.AllForJobAny(targetJobID, VALIDUSERID);
        }
    }
}