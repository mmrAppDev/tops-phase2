﻿using System;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMRCommon.Data;
using Ninject;

namespace Lib.TOPControls.Tests.LibraryTests
{
    [TestClass]
    public class WorkCrewLibrary_Tests : JobObjectLibrary_BaseTests<WorkCrew>
    {
        #region OverridesAndBaseClassOverloads

        protected override IObjectLibrary<WorkCrew> GetTargetLibrary()
        {
            var library = kernel.Get<WorkCrewLibrary>();
            return library;
        }

        protected override IObjectLibrary<WorkCrew> GetTargetLibrary(IRepository<WorkCrew> useRepository)
        {
            return GetTargetAsJObLibrary(useRepository);
        }

        public override IJobObjectLibrary<WorkCrew> GetTargetAsJObLibrary(IRepository<WorkCrew> useRepository = null)
        {
            var library = kernel.Get<WorkCrewLibrary>();
            library.Repository = useRepository;
            return library;
        }

        protected override WorkCrew GetInvalidObjectInstance(bool asNewItem)
        {
            var result = new WorkCrew
            {
                ID = (asNewItem ? 0 : 1)
            };
            return result;
        }

        protected override WorkCrew GetValidObjectInstance(bool asNewItem)
        {
            var result = new WorkCrew
            {
                ID = (asNewItem ? 0 : 1),
                JobID = 1,
                CrewNumber = 1,
                Description = "First Work Crew",
                LastChanged = DateTime.Now
            };
            return result;
        }

        #endregion OverridesAndBaseClassOverloads

        #region Tests

        #endregion Tests
    }
}