﻿using System;
using System.Linq;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Common;
using Lib.TOPControls.Tests.LibraryTests.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMRCommon.Data;
using Ninject;

namespace Lib.TOPControls.Tests.LibraryTests
{
    [TestClass]
    public class ProjectPhaseLibrary_Tests : JobObjectLibrary_BaseTests<ProjectPhase>
    {
        #region OverridesAndBaseClassOverloads

        protected override IObjectLibrary<ProjectPhase> GetTargetLibrary()
        {
            var library = kernel.Get<ProjectPhaseLibrary>();
            return library;
        }

        protected override IObjectLibrary<ProjectPhase> GetTargetLibrary(IRepository<ProjectPhase> useRepository)
        {
            return GetTargetAsJObLibrary(useRepository);
        }

        public override IJobObjectLibrary<ProjectPhase> GetTargetAsJObLibrary(IRepository<ProjectPhase> useRepository = null)
        {
            var library = kernel.Get<ProjectPhaseLibrary>();
            library.Repository = useRepository;
            return library;
        }

        protected override ProjectPhase GetInvalidObjectInstance(bool asNewItem)
        {
            var result = new ProjectPhase
            {
                ID = (asNewItem ? 0 : 1),
                JobID = -1,
                Title = null
            };
            return result;
        }

        protected override ProjectPhase GetValidObjectInstance(bool asNewItem)
        {
            var result = new ProjectPhase
            {
                ID = (asNewItem ? 0 : 1),
                JobID = 1,
                Title = "Pre-Commissioning",
                LastChanged = DateTime.Now,
                DisplayOrder = 1
            };
            return result;
        }

        #endregion OverridesAndBaseClassOverloads

        #region Tests

        #region FindTests

        // TODO:  Add test cases for Find, when the JobID refers to a deleted job.

        #endregion FindTests

        #region MoveDownInDisplayOrderTests

        [TestMethod]
        public void ProjectPhaseLibrary_MoveDownInDisplayOrder_SwapsItems()
        {
            var targetJobID = 1;
            var libRoot = new TOPControlLibraryMocker();

            var dataMock = getBoringOrderedRepository(targetJobID);

            var target = GetTargetLibrary(dataMock) as ProjectPhaseLibrary;
            target.Root = libRoot;

            var result = target.MoveDownInDisplayOrder(3, 1);

            var expectedThirdID = 4;
            var expectedFourthID = 3;

            Assert.AreEqual(expectedThirdID, result.ElementAt(2).ID);
            Assert.AreEqual(expectedFourthID, result.ElementAt(3).ID);
        }

        [TestMethod]
        public void ProjectPhaseLibrary_MoveDownInDisplayOrder_LastItem()
        {
            var targetJobID = 1;
            var dataMock = getBoringOrderedRepository(targetJobID);

            var libRoot = new TOPControlLibraryMocker();

            var target = GetTargetLibrary(dataMock) as ProjectPhaseLibrary;
            target.Root = libRoot;

            var result = target.MoveDownInDisplayOrder(4, 1);

            var expectedFirstID = 1;
            var expectedSecondID = 2;
            var expectedThirdID = 3;
            var expectedFourthID = 4;

            Assert.AreEqual(expectedFirstID, result.ElementAt(0).ID);
            Assert.AreEqual(expectedSecondID, result.ElementAt(1).ID);
            Assert.AreEqual(expectedThirdID, result.ElementAt(2).ID);
            Assert.AreEqual(expectedFourthID, result.ElementAt(3).ID);
        }

        #endregion MoveDownInDisplayOrderTests

        #region MoveUpInDisplayOrderTests

        // TODO:  Add test cases for MoveUpInDisplayOrder

        [TestMethod]
        public void ProjectPhaseLibrary_MoveUpInDisplayOrder_SwapsItems()
        {
            var targetJobID = 1;
            var dataMock = getBoringOrderedRepository(targetJobID);

            var libRoot = new TOPControlLibraryMocker();

            var target = GetTargetLibrary(dataMock) as ProjectPhaseLibrary;
            target.Root = libRoot;

            var result = target.MoveUpInDisplayOrder(3, 1);

            var expectedFirstID = 1;
            var expectedSecondID = 3;
            var expectedThirdID = 2;
            var expectedFourthID = 4;

            Assert.AreEqual(expectedFirstID, result.ElementAt(0).ID);
            Assert.AreEqual(expectedSecondID, result.ElementAt(1).ID);
            Assert.AreEqual(expectedThirdID, result.ElementAt(2).ID);
            Assert.AreEqual(expectedFourthID, result.ElementAt(3).ID);
        }

        [TestMethod]
        public void ProjectPhaseLibrary_MoveUpInDisplayOrder_FirstItem()
        {
            var targetJobID = 1;
            var dataMock = getBoringOrderedRepository(targetJobID);

            var libRoot = new TOPControlLibraryMocker();

            var target = GetTargetLibrary(dataMock) as ProjectPhaseLibrary;
            target.Root = libRoot;

            var result = target.MoveUpInDisplayOrder(1, 1);

            var expectedFirstID = 1;
            var expectedSecondID = 2;
            var expectedThirdID = 3;
            var expectedFourthID = 4;

            Assert.AreEqual(expectedFirstID, result.ElementAt(0).ID);
            Assert.AreEqual(expectedSecondID, result.ElementAt(1).ID);
            Assert.AreEqual(expectedThirdID, result.ElementAt(2).ID);
            Assert.AreEqual(expectedFourthID, result.ElementAt(3).ID);
        }

        #endregion MoveUpInDisplayOrderTests

        #region SearchTests

        // TODO:  Add test cases for Search

        #endregion SearchTests

        #region SearchAnyTests

        // TODO:  Add test cases for SearchAny

        #endregion SearchAnyTests

        #endregion Tests

        private static RepositoryMocker<ProjectPhase> getBoringOrderedRepository(int targetJobID)
        {
            var dataMock = new RepositoryMocker<ProjectPhase>();

            dataMock.items.Add(new ProjectPhase
            {
                ID = 1,
                JobID = targetJobID,
                Title = "First",
                DisplayOrder = 1
            });
            dataMock.items.Add(new ProjectPhase
            {
                ID = 2,
                JobID = targetJobID,
                Title = "Second",
                DisplayOrder = 2
            });
            dataMock.items.Add(new ProjectPhase
            {
                ID = 3,
                JobID = targetJobID,
                Title = "Third",
                DisplayOrder = 3
            });
            dataMock.items.Add(new ProjectPhase
            {
                ID = 4,
                JobID = targetJobID,
                Title = "Fourth",
                DisplayOrder = 4
            });
            return dataMock;
        }
    }
}