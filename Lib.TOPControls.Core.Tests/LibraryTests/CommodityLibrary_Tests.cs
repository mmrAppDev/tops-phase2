﻿using System;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMRCommon.Data;
using Ninject;

namespace Lib.TOPControls.Tests.LibraryTests
{
    [TestClass]
    public class CommodityLibrary_Tests : JobObjectLibrary_BaseTests<Commodity>
    {
        #region OverridesAndBaseClassOverloads

        protected override IObjectLibrary<Commodity> GetTargetLibrary()
        {
            var library = kernel.Get<CommodityLibrary>();
            return library;
        }

        protected override IObjectLibrary<Commodity> GetTargetLibrary(IRepository<Commodity> useRepository)
        {
            return GetTargetAsJObLibrary(useRepository);
        }

        public override IJobObjectLibrary<Commodity> GetTargetAsJObLibrary(IRepository<Commodity> useRepository = null)
        {
            var library = kernel.Get<CommodityLibrary>();
            library.Repository = useRepository;
            return library;
        }

        protected override Commodity GetInvalidObjectInstance(bool asNewItem)
        {
            var result = new Commodity
            {
                ID = (asNewItem ? 0 : 1)
                
            };
            return result;
        }

        protected override Commodity GetValidObjectInstance(bool asNewItem)
        {
            var result = new Commodity
            {
                ID = (asNewItem ? 0 : 1),
                JobID = 1,
                CommodityCode = "888-888-88888",
                Description = string.Empty, 
                LastChanged = DateTime.Now
            };
            return result;
        }

        #endregion OverridesAndBaseClassOverloads

        #region Tests

        #endregion Tests
    }
}