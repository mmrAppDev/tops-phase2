﻿using System;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMRCommon.Data;
using Ninject;

namespace Lib.TOPControls.Tests.LibraryTests
{
    [TestClass]
    public class TOPUserLibrary_Tests : SoftDeleteLibrary_BaseTests<TOPUser>
    {
        #region OverridesAndBaseClassOverloads

        protected override IObjectLibrary<TOPUser> GetTargetLibrary()
        {
            var library = kernel.Get<TOPUserLibrary>();
            return library;
        }

        protected override IObjectLibrary<TOPUser> GetTargetLibrary(IRepository<TOPUser> useRepository)
        {
            return GetTargetAsSDLibrary(useRepository);
        }

        public override ISoftDeleteLibrary<TOPUser> GetTargetAsSDLibrary(IRepository<TOPUser> useRepository = null)
        {
            var library = kernel.Get<TOPUserLibrary>();
            library.Repository = useRepository;
            return library;
        }

        protected override TOPUser GetInvalidObjectInstance(bool asNewItem)
        {
            var result = new TOPUser
            {
                ID = (asNewItem ? 0 : 1)
            };
            return result;
        }

        protected override TOPUser GetValidObjectInstance(bool asNewItem)
        {
            var result = new TOPUser
            {
                ID = (asNewItem ? 0 : 1),
                Login = "luser",
                FirstName = "Login",
                LastName = "User",
                Preferences = string.Empty,
                LastChanged = DateTime.Now
            };
            return result;
        }

        #endregion OverridesAndBaseClassOverloads

        #region Tests

        #endregion Tests
    }
}