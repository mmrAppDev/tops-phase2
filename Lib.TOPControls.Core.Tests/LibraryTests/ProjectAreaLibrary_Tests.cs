﻿using System;
using System.Linq;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Common;
using Lib.TOPControls.Tests.LibraryTests.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMRCommon.Data;
using Ninject;

namespace Lib.TOPControls.Tests.LibraryTests
{
    [TestClass]
    public class ProjectAreaLibrary_Tests : JobObjectLibrary_BaseTests<ProjectArea>
    {
        #region OverridesAndBaseClassOverloads

        protected override IObjectLibrary<ProjectArea> GetTargetLibrary()
        {
            var library = kernel.Get<ProjectAreaLibrary>();
            return library;
        }

        protected override IObjectLibrary<ProjectArea> GetTargetLibrary(IRepository<ProjectArea> useRepository)
        {
            return GetTargetAsJObLibrary(useRepository);
        }

        public override IJobObjectLibrary<ProjectArea> GetTargetAsJObLibrary(IRepository<ProjectArea> useRepository = null)
        {
            var library = kernel.Get<ProjectAreaLibrary>();
            library.Repository = useRepository;
            return library;
        }

        protected override ProjectArea GetInvalidObjectInstance(bool asNewItem)
        {
            var result = new ProjectArea
            {
                ID = (asNewItem ? 0 : 1)
            };
            return result;
        }

        protected override ProjectArea GetValidObjectInstance(bool asNewItem)
        {
            var result = new ProjectArea
            {
                ID = (asNewItem ? 0 : 1),
                JobID = 1,
                AreaNumber = "888-888-88888",
                LastChanged = DateTime.Now
            };
            return result;
        }

        #endregion OverridesAndBaseClassOverloads

        #region Tests

        [TestMethod]
        public void ProjectAreaLibrary_Search_EmptySearchTerm_ReturnsAllForJob()
        {
            var dataMock = new RepositoryMocker<ProjectArea>();
            dataMock.items.Add(new ProjectArea
                {
                    ID = dataMock.GetNextID(),
                    JobID = 1,
                    AreaNumber = string.Format("10{0}", dataMock.GetNextID())
                });
            dataMock.items.Add(new ProjectArea
            {
                ID = dataMock.GetNextID(),
                JobID = 1,
                AreaNumber = string.Format("Test", dataMock.GetNextID())
            });
            dataMock.items.Add(new ProjectArea
            {
                ID = dataMock.GetNextID(),
                JobID = 2,
                AreaNumber = string.Format("10{0}", dataMock.GetNextID())
            });
            dataMock.items.Add(new ProjectArea
            {
                ID = dataMock.GetNextID(),
                JobID = 1,
                AreaNumber = string.Format("10{0}", dataMock.GetNextID())
            });
            var searchTerm = "";
            var searchJobID = 1;
            var target = GetTargetLibrary(dataMock) as IProjectAreaLibrary;

            var result = target.Search(searchJobID, searchTerm);

            Assert.AreNotEqual(0, result.Count(), "Search returned no results.");
            Assert.AreEqual(3, result.Count(), "Unexpected result count.");

            int expectedCount = 1;
            int actualCount;
            foreach (var expectedItem in dataMock.items.Where(pa => pa.JobID == 1))
            {
                actualCount = result.Where(pa => pa.ID == expectedItem.ID).Count();
                Assert.AreEqual(expectedCount, actualCount, "Could not find item with ID {0}", expectedItem.ID);
            }
        }

        #endregion Tests
    }
}