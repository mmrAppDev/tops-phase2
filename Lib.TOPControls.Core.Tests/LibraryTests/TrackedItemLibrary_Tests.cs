﻿using System;
using Lib.TOPControls.BaseClasses;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.Tests.LibraryTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMRCommon.Data;
using Ninject;

namespace Lib.TOPControls.Tests.LibraryTests
{
    [TestClass]
    public class TrackedItemLibrary_Tests : JobSoftDeleteLibrary_BaseTests<TrackedItem>
    {
        #region OverridesAndBaseClassOverloads

        protected override IObjectLibrary<TrackedItem> GetTargetLibrary()
        {
            var library = kernel.Get<TrackedItemLibrary>();
            return library;
        }

        protected override IObjectLibrary<TrackedItem> GetTargetLibrary(IRepository<TrackedItem> useRepository)
        {
            return GetTargetAsSDLibrary(useRepository);
        }

        public override ISoftDeleteLibrary<TrackedItem> GetTargetAsSDLibrary(IRepository<TrackedItem> useRepository = null)
        {
            return (ISoftDeleteLibrary<TrackedItem>)GetTargetAsJObLibrary(useRepository);
        }

        public override JobSoftDeleteLibraryBase<TrackedItem> GetTargetAsJObLibrary(IRepository<TrackedItem> useRepository = null)
        {
            var library = kernel.Get<TrackedItemLibrary>();
            library.Repository = useRepository;
            return library;
        }

        protected override TrackedItem GetInvalidObjectInstance(bool asNewItem)
        {
            var result = new TrackedItem
            {
                ID = (asNewItem ? 0 : 1)
            };
            return result;
        }

        protected override TrackedItem GetValidObjectInstance(bool asNewItem)
        {
            var result = new TrackedItem
            {
                ID = (asNewItem ? 0 : 1),
                JobID = 1,
                TrackingID = "888-999-888381",
                LastChanged = DateTime.Now
            };
            return result;
        }

        #endregion OverridesAndBaseClassOverloads

        #region Tests

        #endregion Tests
    }
}