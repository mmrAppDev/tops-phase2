﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.Tests
{
    [TestClass]
    public class ProjectArea_ClassTests : DomainJobObject_BaseTests<ProjectArea>
    {
        [TestMethod]
        public void ProjectAreaConstructor_SetsDefaultValues()
        {
            string expectedAreaNumber = null;
            string expectedDescription = "";

            var actual = createDefaultInstance();

            Assert.AreEqual(expectedAreaNumber, actual.AreaNumber);
            Assert.AreEqual(expectedDescription, actual.Description);
        }
    }
}