﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.Tests
{
    [TestClass]
    public abstract class DomainJobObject_BaseTests<T> where T : DomainJobObject, new()
    {
        protected virtual T createDefaultInstance()
        {
            return new T();
        }

        [TestMethod]
        public void DoJOConstructor_CreatesInstance()
        {
            var actual = createDefaultInstance();
            Assert.IsInstanceOfType(actual, typeof(T));
        }

        [TestMethod]
        public void DoJOConstructor_SetsDefaultValues()
        {
            var expectedID = 0;
            var expectedJobID = 0;
            var expectedLastChangedMinimum = DateTime.Now;

            var actual = createDefaultInstance();

            Assert.AreEqual(expectedID, actual.ID);
            Assert.AreEqual(expectedJobID, actual.JobID);
            Assert.IsTrue(expectedLastChangedMinimum <= actual.LastChanged);
        }
    }
}