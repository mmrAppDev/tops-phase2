using System.Web;
using System.Web.Security;

namespace MMR.TOPControls.Web.Security
{
    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        public void Login(string emailAddress)
        {
            FormsAuthentication.SetAuthCookie(emailAddress, true);
        }

        public void Logout()
        {
            HttpContext.Current.Session.Abandon();
            FormsAuthentication.SignOut();
        }

        public string GetRedirectUrl()
        {
            return FormsAuthentication.GetRedirectUrl("", false);
        }
    }
}