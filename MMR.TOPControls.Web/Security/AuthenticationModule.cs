using System;
using System.Web;
using System.Web.Mvc;
using MMR.TOPControls.Web.Security;
using Envoc.Core.Shared.Extensions;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;

[assembly: WebActivator.PreApplicationStartMethod(typeof(AuthenticationModule), "Start")]
namespace MMR.TOPControls.Web.Security
{
    public class AuthenticationModule : IHttpModule
    {
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(AuthenticationModule));
        }

        public void Init(HttpApplication context)
        {
            context.PostAuthenticateRequest += ContextOnPostAuthenticateRequest;
        }

        private void ContextOnPostAuthenticateRequest(object sender, EventArgs e)
        {
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                return;
            }

            var service = DependencyResolver.Current.GetService<UserSessionService>();
            var currentUser = service.CurrentUser;

            if (currentUser.IsNull())
            {
                return;
            }

            var principle = new UserPrinciple(currentUser);
            HttpContext.Current.User = principle;
        }

        public void Dispose()
        {
        }
    }
}