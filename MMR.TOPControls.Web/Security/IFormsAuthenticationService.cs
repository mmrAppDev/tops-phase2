namespace MMR.TOPControls.Web.Security
{
    public interface IFormsAuthenticationService
    {
        void Login(string emailAddress);
        void Logout();
        string GetRedirectUrl();
    }
}