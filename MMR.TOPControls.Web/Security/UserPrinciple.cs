using System.Security.Principal;
using Lib.TOPControls;
using Lib.TOPControls.Provider;

namespace MMR.TOPControls.Web.Security
{
    internal class UserPrinciple : IPrincipal
    {
        private readonly TOPUser user;
        private readonly LocalDataRoleProvider roleService;

        public UserPrinciple(TOPUser user)
        {
            this.user = user;
            Identity = new UserIdentity(user);
            roleService = new LocalDataRoleProvider();
        }

        public IIdentity Identity { get; private set; }

        public bool IsInRole(string role)
        {
            return roleService.IsUserInRole(user.Login, role);
        }
    }

    internal class UserIdentity : IIdentity
    {
        public UserIdentity(TOPUser user)
        {
            IsAuthenticated = true;
            Name = user.Login;
            ID = user.ID;
        }

        protected long ID { get; private set; }

        public string AuthenticationType
        {
            get { return "Forms"; }
        }

        public bool IsAuthenticated { get; private set; }

        public string Name { get; private set; }
    }
}