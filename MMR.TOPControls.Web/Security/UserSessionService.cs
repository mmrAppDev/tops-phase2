using System.Security.Principal;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using Envoc.Core.Shared.Extensions;

namespace MMR.TOPControls.Web.Security
{
    public class UserSessionService
    {
        private readonly IIdentity identity;
        private readonly TOPControlLibrary userService;
        private TOPUser currentUser;

        public UserSessionService(IIdentity identity)
        {
            this.identity = identity;
            userService = new TOPControlLibrary(new TOPControlSQLDAL());
        }

        public TOPUser CurrentUser
        {
            get
            {
                if (!identity.IsAuthenticated)
                {
                    return null;
                }

                if (currentUser.IsNotNull())
                {
                    return currentUser;
                }

                currentUser = userService.UserActions.FindByUsername(identity.Name);
                
                return currentUser;
            }
        }
    }
}