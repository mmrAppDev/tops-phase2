using System;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Routing;
using Envoc.Core.Shared.Extensions;

namespace MMR.TOPControls.Web.Helpers
{
    internal static class AuthorizationHelper
    {
        public static bool HasActionPermission(this UrlHelper htmlHelper, string actionName, string controllerName)
        {
            ControllerBase controllerToLinkTo = GetControllerByName(htmlHelper.RequestContext, controllerName);

            return IsControllerActionAuthorized(htmlHelper.RequestContext, controllerToLinkTo, actionName);
        }

        public static bool HasActionPermission(this HtmlHelper htmlHelper, string actionName, string controllerName)
        {
            ControllerBase controllerToLinkTo = string.IsNullOrEmpty(controllerName)
                                                    ? htmlHelper.ViewContext.Controller
                                                    : GetControllerByName(htmlHelper.ViewContext.RequestContext,
                                                                          controllerName);
            return IsControllerActionAuthorized(htmlHelper.ViewContext.RequestContext, controllerToLinkTo, actionName);
        }

        private static bool IsControllerActionAuthorized(RequestContext requestContext,
                                                         ControllerBase controllerToLinkTo,
                                                         string actionName)
        {
            var controllerContext = new ControllerContext(requestContext,
                                                          controllerToLinkTo);
            var controllerDescriptor =
                new ReflectedControllerDescriptor(controllerToLinkTo.GetType());
            ActionDescriptor actionDescriptor = controllerDescriptor.FindAction(controllerContext, actionName);

            return ActionIsAuthorized(controllerContext, actionDescriptor);
        }

        private static ControllerBase GetControllerByName(RequestContext requestContext, string controllerName)
        {
            IControllerFactory factory = ControllerBuilder.Current.GetControllerFactory();

            IController controller = factory.CreateController(requestContext, controllerName);

            if (controller.IsNull())
            {
                throw new InvalidOperationException(
                    string.Format(
                        CultureInfo.CurrentUICulture,
                        "Controller factory {0} controller {1} returned null",
                        factory.GetType(),
                        controllerName));
            }

            return (ControllerBase) controller;
        }

        private static bool ActionIsAuthorized(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            if (actionDescriptor.IsNull())
                return false;

            var authContext = new AuthorizationContext(controllerContext, actionDescriptor);
            var filters = FilterProviders.Providers.GetFilters(controllerContext, actionDescriptor);
            var filterInfo = new FilterInfo(filters);

            foreach (
                IAuthorizationFilter authFilter in filterInfo.AuthorizationFilters)
            {
                authFilter.OnAuthorization(authContext);

                if (authContext.Result != null)
                    return false;
            }

            return true;
        }
    }
}