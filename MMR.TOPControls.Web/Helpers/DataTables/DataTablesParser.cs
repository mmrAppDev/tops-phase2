using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using MMR.TOPControls.Web.Helpers.DataTables.Attributes;

namespace MMR.TOPControls.Web.Helpers.DataTables
{
    public class DataTableParser<T>
    {
        private const string AscendingSort = "asc";
        private IOrderedQueryable<T> orderedQueryable;
        private readonly IQueryable<T> queriable;
        private readonly Type type;
        private readonly PropertyInfo[] properties;
        private readonly PropertyInfo[] searchableProperties;
        private readonly PropertyInfo[] sortableProperties;

        public DataTableParser(IQueryable<T> queriable)
        {
            this.queriable = queriable;
            type = typeof(T);
            properties = type.GetProperties();
            searchableProperties = GetSearchableProperties(properties);
            sortableProperties = GetSortableProperties(properties);
        }

        public FormatedList Parse(DataTableRequest request)
        {
            var list = new FormatedList();
            list.Import(properties.Select(x => x.Name).ToArray());
            list.sEcho = request.TableEcho;
            list.iTotalRecords = queriable.Count();

            var skip = request.Skip;
            var take = request.Take;

            ApplySort(request);

            var unpagedData = orderedQueryable
                .Where(ApplyGenericSearch(request))
                .Where(IndividualPropertySearch(request));

            list.iTotalDisplayRecords = unpagedData.Count();
            list.aaData = unpagedData
                .Skip(skip)
                .Take(take)
                .ToList()
                .Select(SelectProperties);

            return list;
        }

        private void ApplySort(DataTableRequest request)
        {
            orderedQueryable = null;
            foreach (var dataTableColumn in request.Columns)
            {
                var sortcolumn = dataTableColumn.ColumnIndex;
                if (sortcolumn < 0 || sortcolumn >= properties.Length)
                    continue;

                var property = properties[sortcolumn];
                if (!sortableProperties.Contains(property))
                    continue;

                var sortdir = dataTableColumn.SortDirection;
                if(string.IsNullOrEmpty(sortdir))
                    continue;

                SortOnColumn(sortcolumn, sortdir);
            }
            if (orderedQueryable == null) SortOnColumn(0, AscendingSort);
        }

        private void SortOnColumn(int sortcolumn, string sortDirection)
        {
            var method = typeof(DataTableParser<T>).GetMethod("ApplyTypeSort", BindingFlags.NonPublic | BindingFlags.Instance);
            var generic = method.MakeGenericMethod(properties[sortcolumn].PropertyType);
            generic.Invoke(this, BindingFlags.NonPublic | BindingFlags.Instance, null, new object[] { sortcolumn, sortDirection }, null);
        }

        // ReSharper disable UnusedMember.Local - called via reflection
        private void ApplyTypeSort<TSearch>(int sortcolumn, string sortdir)
        // ReSharper restore UnusedMember.Local
        {
            var paramExpr = Expression.Parameter(typeof(T), "val");
            var property = Expression.Property(paramExpr, properties[sortcolumn]);
            var propertyExpr = Expression.Lambda<Func<T, TSearch>>(property, paramExpr);


            if (string.IsNullOrEmpty(sortdir) || sortdir.Equals(AscendingSort, StringComparison.OrdinalIgnoreCase))
                orderedQueryable = orderedQueryable == null
                                       ? queriable.OrderBy(propertyExpr)
                                       : orderedQueryable.ThenBy(propertyExpr);
            else
                orderedQueryable = orderedQueryable == null
                                       ? queriable.OrderByDescending(propertyExpr)
                                       : orderedQueryable.ThenByDescending(propertyExpr);
        }

        private PropertyInfo[] GetSortableProperties(IEnumerable<PropertyInfo> propertyInfos)
        {
            return propertyInfos.Where(x => x.GetCustomAttributes(typeof(SortableAttribute), false).Any()).ToArray();
        }

        private PropertyInfo[] GetSearchableProperties(IEnumerable<PropertyInfo> propertyInfos)
        {
            return propertyInfos.Where(x => x.GetCustomAttributes(typeof(SearchableAttribute), false).Any()).ToArray();
        }

        private Func<T, IEnumerable<object>> SelectProperties
        {
            get { return value => properties.Select(prop => (prop.GetValue(value, new object[0]))); }
        }

        private Expression<Func<T, bool>> IndividualPropertySearch(DataTableRequest request)
        {
            var paramExpr = Expression.Parameter(typeof(T), "val");
            Expression compoundExpression = Expression.Constant(true);

            foreach (var dataTableColumn in request.Columns)
            {
                var query = dataTableColumn.SearchTerm;

                if(string.IsNullOrEmpty(query))
                    continue;

                var searchColumn = dataTableColumn.ColumnIndex;
                if (searchColumn < 0 || searchColumn >= properties.Length)
                    continue;

                var propertyToSearch = properties[searchColumn];
                if (!searchableProperties.Contains(propertyToSearch))
                    continue;

                var expression = ParseFactory.GetParser<T>(query, propertyToSearch, paramExpr).GetSearchExpression();
                compoundExpression = Expression.And(compoundExpression, expression);
            }
            var result = Expression.Lambda<Func<T, bool>>(compoundExpression, paramExpr);
            return result;
        }

        private Expression<Func<T, bool>> ApplyGenericSearch(DataTableRequest request)
        {
            string search = request.AllSearch;

            if (string.IsNullOrEmpty(search) || properties.Length == 0)
                return x => true;

            var paramExpression = Expression.Parameter(typeof(T), "val");
            var searchValues = GetSearchTerms(search).ToList();

            Expression compoundExpression = Expression.Constant(true);
            foreach (var searchValue in searchValues)
            {
                if (string.IsNullOrEmpty(searchValue)) continue;

                Expression singleValue = Expression.Constant(false);
                var propertyQueries = GetSearchQueries(searchValue, paramExpression);
                foreach (var propertyQuery in propertyQueries)
                {
                    singleValue = Expression.Or(singleValue, propertyQuery);
                }

                compoundExpression = Expression.And(compoundExpression, singleValue);
            }

            var result = Expression.Lambda<Func<T, bool>>(compoundExpression, paramExpression);
            return result;
        }

        private IEnumerable<string> GetSearchTerms(string value)
        {
            var quotedList = new List<string>();

            var regex = new Regex("[^ ]*(\"([^\"]+)\")[^ ]*", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline);
            for (var match = regex.Match(value); match.Success; match = match.NextMatch())
            {
                quotedList.Add(match.Groups[0].Value);
            }

            foreach (var quoted in quotedList)
            {
                var index = value.IndexOf(quoted, StringComparison.OrdinalIgnoreCase);
                value = value.Substring(0, index) +
                        value.Substring(index + quoted.Length, value.Length - (index + quoted.Length));
            }

            regex = new Regex("[^ ]*[^ ]+[^ ]*", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline);
            for (var match = regex.Match(value); match.Success; match = match.NextMatch())
            {
                quotedList.Add(match.Groups[0].Value);
            }

            return quotedList.Select(x => x.Replace("\"", ""));
        }

        private IEnumerable<Expression> GetSearchQueries(string searchExpression, ParameterExpression paramExpression)
        {
            return properties
                .Where(x => searchableProperties.Contains(x))
                .Select(property => ParseFactory.GetParser<T>(searchExpression, property, paramExpression).GetSearchExpression());
        }
    }
}