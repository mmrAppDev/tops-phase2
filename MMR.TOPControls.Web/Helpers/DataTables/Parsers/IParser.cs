using System.Linq.Expressions;

namespace MMR.TOPControls.Web.Helpers.DataTables.Parsers
{
    public interface IParser
    {
        Expression GetSearchExpression();
    }
}