using System.Linq.Expressions;
using System.Reflection;

namespace MMR.TOPControls.Web.Helpers.DataTables.Parsers
{
    public class ComparableParserExpressionFactory
    {
        public static Expression GetExpression(ConstantExpression value, ParameterExpression parameterExpression, PropertyInfo propertyToSearch, ComparisonType comparisonType)
        {
            switch (comparisonType)
            {
                case ComparisonType.GreaterThan:
                    return Expression.GreaterThan(Expression.Property(parameterExpression, propertyToSearch), value);
                case ComparisonType.LessThan:
                    return Expression.LessThan(Expression.Property(parameterExpression, propertyToSearch), value);
                case ComparisonType.GreaterThanOrEquals:
                    return Expression.GreaterThanOrEqual(Expression.Property(parameterExpression, propertyToSearch), value);
                case ComparisonType.LessThanOrEquals:
                    return Expression.LessThanOrEqual(Expression.Property(parameterExpression, propertyToSearch), value);
                case ComparisonType.NotEquals:
                    return Expression.NotEqual(Expression.Property(parameterExpression, propertyToSearch), value);
                default:
                    return Expression.Equal(Expression.Property(parameterExpression, propertyToSearch), value);
            }
        }
    }
}