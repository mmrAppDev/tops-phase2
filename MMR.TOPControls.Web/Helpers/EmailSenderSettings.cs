using System;
using System.Collections.Generic;
using System.Configuration;

namespace MMR.TOPControls.Web.Helpers
{
    internal class EmailSenderSettings
    {
        public EmailSenderSettings()
        {
            Server = ConfigurationManager.AppSettings["SmtpServer"];
            FromAddress = ConfigurationManager.AppSettings["FromAddress"];
            FromName = ConfigurationManager.AppSettings["FromName"];
            Username = ConfigurationManager.AppSettings["SmtpUsername"];
            Password = ConfigurationManager.AppSettings["SmtpPassword"];
            Port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
            TestToAddress = ConfigurationManager.AppSettings["TestToAddress"];
            ToAddress = ConfigurationManager.AppSettings["ToAddress"];
            IsInTestMode = Convert.ToBoolean(ConfigurationManager.AppSettings["IsInTestMode"]);
            AttachmentFileLocation = new List<string>();
        }

        public string Server { get; private set; }

        public int Port { get; private set; }

        public string Username { get; private set; }

        public string Password { get; private set; }

        public string FromAddress { get; private set; }

        public string FromName { get; private set; }

        public bool IsInTestMode { get; private set; }

        public string TestToAddress { get; set; }

        public IEnumerable<string> AttachmentFileLocation { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }

        public string ToAddress { get; set; }

        public string GetToAddress()
        {
            if (IsInTestMode)
            {
                return TestToAddress;
            }

            return ToAddress;
        }
    }
}