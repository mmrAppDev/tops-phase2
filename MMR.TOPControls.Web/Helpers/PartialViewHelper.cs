using System.Linq;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using JetBrains.Annotations;

namespace MMR.TOPControls.Web.Helpers
{
    public static class PartialViewHelper
    {
        public static MvcHtmlString ConditionalPartial(this HtmlHelper helper,
                                                       [AspMvcPartialView] string partialViewName, bool displayPartial)
        {
            if (displayPartial)
            {
                return helper.Partial(partialViewName);
            }

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString ConditionalPartial(this HtmlHelper helper,
                                                       [AspMvcPartialView] string partialViewName, object model,
                                                       bool displayPartial)
        {
            if (displayPartial)
            {
                return helper.Partial(partialViewName, model);
            }

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString ConditionalPartial(this HtmlHelper helper,
                                                       [AspMvcPartialView] string partialViewName, object model,
                                                       ViewDataDictionary viewData,
                                                       bool displayPartial)
        {
            if (displayPartial)
            {
                return helper.Partial(partialViewName, model, viewData);
            }

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString ConditionalPartial(this HtmlHelper helper,
                                                       [AspMvcPartialView] string partialViewName,
                                                       ViewDataDictionary viewData,
                                                       bool displayPartial)
        {
            if (displayPartial)
            {
                return helper.Partial(partialViewName, viewData);
            }

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString AuthorizePartial(this HtmlHelper helper, [AspMvcPartialView] string partialViewName)
        {
            if (helper.ViewContext.HttpContext.Request.IsAuthenticated)
            {
                return helper.Partial(partialViewName);
            }

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString AuthorizePartial(this HtmlHelper helper, [AspMvcPartialView] string partialViewName,
                                                     object model)
        {
            if (helper.ViewContext.HttpContext.Request.IsAuthenticated)
            {
                return helper.Partial(partialViewName, model);
            }

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString AuthorizePartial(this HtmlHelper helper, [AspMvcPartialView] string partialViewName,
                                                     object model, ViewDataDictionary viewData)
        {
            if (helper.ViewContext.HttpContext.Request.IsAuthenticated)
            {
                return helper.Partial(partialViewName, model, viewData);
            }

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString AuthorizePartial(this HtmlHelper helper, [AspMvcPartialView] string partialViewName,
                                                     ViewDataDictionary viewData)
        {
            if (helper.ViewContext.HttpContext.Request.IsAuthenticated)
            {
                return helper.Partial(partialViewName, viewData);
            }

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString AuthorizePartial(this HtmlHelper helper, [AspMvcPartialView] string partialViewName,
                                                     params string[] roles)
        {
            var user = helper.ViewContext.HttpContext.User;

            if (roles.Any(role => user.IsInRole(role)))
            {
                return helper.Partial(partialViewName);
            }

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString AuthorizePartial(this HtmlHelper helper, [AspMvcPartialView] string partialViewName,
                                                     object model,
                                                     params string[] roles)
        {
            var user = helper.ViewContext.HttpContext.User;

            if (roles.Any(role => user.IsInRole(role)))
            {
                return helper.Partial(partialViewName, model);
            }

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString AuthorizePartial(this HtmlHelper helper, [AspMvcPartialView] string partialViewName,
                                                     object model, ViewDataDictionary viewData,
                                                     params string[] roles)
        {
            var user = helper.ViewContext.HttpContext.User;

            if (roles.Any(role => user.IsInRole(role)))
            {
                return helper.Partial(partialViewName, model, viewData);
            }

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString AuthorizePartial(this HtmlHelper helper, [AspMvcPartialView] string partialViewName,
                                                     ViewDataDictionary viewData,
                                                     params string[] roles)
        {
            var user = helper.ViewContext.HttpContext.User;

            if (roles.Any(role => user.IsInRole(role)))
            {
                return helper.Partial(partialViewName, viewData);
            }

            return MvcHtmlString.Empty;
        }
    }
}