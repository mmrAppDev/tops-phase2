﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Web;
using System.Web.Mvc;

namespace MMR.TOPControls.Web.Helpers
{
    public class JsonModelBinder : ActionFilterAttribute
    {
        public JsonModelBinder()
        {
        }

        public Type ActionParameterType { get; set; }

        public string ActionParameterName { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpRequestBase request = filterContext.HttpContext.Request;
            Stream stream = request.InputStream;
            stream.Position = 0;
            filterContext.ActionParameters[ActionParameterName] =
                (new DataContractJsonSerializer(ActionParameterType)).ReadObject(stream);
        }
    }
}