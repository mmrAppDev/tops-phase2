using System;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;

namespace MMR.TOPControls.Web.Helpers
{
    public delegate void EmailSentEventHandler(object sender, EventArgs e);

    public delegate void EmailErrorEventHandler(object sender, Exception e);

    internal class EmailSender
    {
        public event EmailSentEventHandler OnEmailSent = delegate { };
        public event EmailErrorEventHandler OnEmailError = delegate { };

        private readonly EmailSenderSettings settings;

        public EmailSender(EmailSenderSettings settings)
        {
            this.settings = settings;
        }

        public void Send()
        {
            var smtpServer = GetSmtpServer();
            var mailMessage = CreateMailMessage();

            try
            {
                smtpServer.Send(mailMessage);
                OnEmailSent(this, new EventArgs());
            }
            catch (Exception ex)
            {
                OnEmailError(this, ex);
            }
        }

        private SmtpClient GetSmtpServer()
        {
            return new SmtpClient(settings.Server)
                {
                    Port = settings.Port,
                    Credentials = new NetworkCredential(settings.Username, settings.Password)
                };
        }

        private MailMessage CreateMailMessage()
        {
            var mailMessage = new MailMessage();
            mailMessage.To.Add(new MailAddress(settings.ToAddress));
            mailMessage.From = new MailAddress(settings.FromAddress, settings.FromName);
            mailMessage.Subject = settings.Subject;
            mailMessage.Body = settings.Message;

            mailMessage.AlternateViews.Add(CreatePlainTextView());
            mailMessage.AlternateViews.Add(CreateHtmlView());

            foreach (var path in settings.AttachmentFileLocation)
            {
                mailMessage.Attachments.Add(new Attachment(path));
            }

            return mailMessage;
        }

        private AlternateView CreatePlainTextView()
        {
            var textType = new ContentType("text/plain");
            var textView = AlternateView.CreateAlternateViewFromString(PlainTextMessage(), textType);

            return textView;
        }

        private string PlainTextMessage()
        {
            var message = settings.Message.Replace("<break>", "\r\n");

            return message;
        }

        private AlternateView CreateHtmlView()
        {
            var htmlType = new ContentType("text/html");
            var htmlView = AlternateView.CreateAlternateViewFromString(HtmlMessage(), htmlType);

            return htmlView;
        }

        private string HtmlMessage()
        {
            return settings.Message.Replace("<break>", "<br/>");
        }
    }
}