using System.Web.Mvc;
using JetBrains.Annotations;

namespace MMR.TOPControls.Web.Helpers
{
    public static class UrlExtensions
    {
        public static string AuthorizeAction(this UrlHelper helper, [AspMvcAction] string actionName,
                                             [AspMvcController] string controllerName)
        {
            if (helper.HasActionPermission(actionName, controllerName))
            {
                return helper.Action(actionName, controllerName);
            }

            return string.Empty;
        }

        public static string AuthorizeAction(this UrlHelper helper, [AspMvcAction] string actionName,
                                             [AspMvcController] string controllerName, object routeValues)
        {
            if (helper.HasActionPermission(actionName, controllerName))
            {
                return helper.Action(actionName, controllerName, routeValues);
            }

            return string.Empty;
        }
    }
}