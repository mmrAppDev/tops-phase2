using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using JetBrains.Annotations;

namespace MMR.TOPControls.Web.Helpers
{
    public static class MvcHtmlStringHelper
    {
        public static MvcHtmlString AuthorizeActionLink(this HtmlHelper helper, string linkText,
                                                        [AspMvcAction] string actionName,
                                                        [AspMvcController] string controllerName, object routeValues,
                                                        object htmlAttributes)
        {
            if (helper.HasActionPermission(actionName, controllerName))
            {
                return helper.ActionLink(linkText, actionName, controllerName, routeValues, htmlAttributes);
            }

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString AuthorizeActionLink(this HtmlHelper helper, string linkText,
                                                        [AspMvcAction] string actionName,
                                                        [AspMvcController] string controllerName,
                                                        RouteValueDictionary routeValues,
                                                        IDictionary<string, object> htmlAttributes)
        {
            if (helper.HasActionPermission(actionName, controllerName))
            {
                return helper.ActionLink(linkText, actionName, controllerName, routeValues, htmlAttributes);
            }

            return MvcHtmlString.Empty;
        }
    }
}