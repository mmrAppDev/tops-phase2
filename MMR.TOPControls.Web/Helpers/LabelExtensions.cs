using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;

namespace MMR.TOPControls.Web.Helpers
{
    public static class LabelExtensions
    {
        public static MvcHtmlString LabelFor<TModel, TValue>(
            this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression,
            object htmlAttributes
            )
        {
            return LabelHelper(
                html,
                ModelMetadata.FromLambdaExpression(expression, html.ViewData),
                ExpressionHelper.GetExpressionText(expression),
                htmlAttributes, null);
        }

        private static MvcHtmlString LabelHelper(HtmlHelper html, ModelMetadata metadata, string htmlFieldName,
                                                 object htmlAttributes, string additionTags)
        {
            string resolvedLabelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
            if (string.IsNullOrEmpty(resolvedLabelText))
            {
                return MvcHtmlString.Empty;
            }

            TagBuilder tag = new TagBuilder("label");
            tag.Attributes.Add("for",
                               TagBuilder.CreateSanitizedId(
                                   html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(htmlFieldName)));
            tag.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            tag.SetInnerText(resolvedLabelText);

            if (!string.IsNullOrEmpty(additionTags))
            {
                tag.InnerHtml += additionTags;
            }

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString LabelFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                             Expression<Func<TModel, TValue>> expression,
                                                             object htmlAttributes, string additionTags)
        {
            return LabelHelper(
                html,
                ModelMetadata.FromLambdaExpression(expression, html.ViewData),
                ExpressionHelper.GetExpressionText(expression),
                htmlAttributes,
                additionTags);
        }

        public static MvcHtmlString Label(
            this HtmlHelper html, string text,
            object htmlAttributes
            )
        {
            return LabelHelper(
                html, text,
                htmlAttributes, null);
        }

        public static MvcHtmlString Label(
            this HtmlHelper html, string text,
            object htmlAttributes, string additionalTags
            )
        {
            return LabelHelper(
                html, text,
                htmlAttributes, additionalTags);
        }

        private static MvcHtmlString LabelHelper(HtmlHelper html, string text, object htmlAttributes,
                                                 string additionTags)
        {
            var tag = new TagBuilder("label");
            tag.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            tag.SetInnerText(text);

            if (!string.IsNullOrEmpty(additionTags))
            {
                tag.InnerHtml += additionTags;
            }

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }
    }
}