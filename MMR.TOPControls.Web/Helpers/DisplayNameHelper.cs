using System;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace MMR.TOPControls.Web.Helpers
{
    public static class DisplayNameHelper
    {
        public static IHtmlString DisplayName<TModel, TValue>(
            this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression)
        {
            var metadata = ModelMetadata.FromLambdaExpression<TModel, TValue>(expression, html.ViewData);

            if (string.IsNullOrEmpty(metadata.DisplayName))
            {
                return new HtmlString(metadata.PropertyName);
            }

            return new HtmlString(metadata.DisplayName);
        }
    }
}