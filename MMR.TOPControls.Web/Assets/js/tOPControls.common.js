var TopControls = TopControls || {};
TopControls.admin = TopControls.admin || {};

(function() {
    TopControls.common = {
        init: function() {
            TopControls.common.highlightLinks();
            TopControls.common.setupDataTableDefaults();
            TopControls.common.initModalViews();
            TopControls.common.initPlugins();
            TopControls.common.registerEnquire();

            $.ajaxSetup({
                cache: false 
            });
        },

        initModalValidation: function () {
            $('form').removeData('validator');
            $('form').removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse('form');
        },

        initModalViews: function () {
            $(document).on('click', "[data-button-action='create'], [data-button-action='edit'], [data-button-action='delete']", function (e) {
                e.preventDefault();

                if ($("#modal-container").length == 0) {
                    $('body').append('<div id="modal-container" class="modal hide" />');
                }

                $("#modal-container").load($(this).attr('href'), function () {
                    TopControls.common.initModalValidation();
                    $("#modal-container").removeClass("hide").modal("show");
                    $("#modal-container input[type='text']:enabled:first").focus();
                });
            });
            
            $(document).on('click', "#changeJobModalButton", function (e) {
                e.preventDefault();
                var container = $("#changeJobModal");

                container.load($(this).attr('href'), function () {
                    container.removeClass("hide").modal("show");
                    container.find("input[type='text']:enabled:first").focus();
                });
            });
            
            $(document).on('click', "#manageTasksModalButton", function (e) {
                e.preventDefault();
                var container = $("#taskManagementModal");

                container.load($(this).attr('href'), function () {
                    container.removeClass("hide").modal("show");
                    container.find("input[type='text']:enabled:first").focus();
                });
            });

            $(document).off('click', '#btnSubmitPhaseTaskEditSelection');
            $(document).on('click', '#btnSubmitPhaseTaskEditSelection', function (e) {
                e.preventDefault();

                var taskSelector = $('#taskPhaseSelector');
                var phaseId = taskSelector.val();
                var urlBase = taskSelector.data("url");
                
                window.location = urlBase + '?phaseId=' + phaseId;
            });

            $(document).on('submit', '[data-form-action="change-password"]', function (e) {
                e.preventDefault();
                
                if ($('#Password').val() && $('#Password').val() == $('#ConfirmPassword').val()) {
                    $.post($(this).attr('action'), $(this).serialize(), function (result) {
                        if (result.success) {
                            $("#modal-container").modal('hide');
                        } else {
                            $("#modal-container").html(result);
                        }
                    });
                } else {
                    var text = $('#Password').val() ? 'Passwords must match' : 'Password required';
                    $('#passwords-match').text(text);
                }
            });
        },

        highlightLinks: function() {
            var url = location.pathname;
            $('.nav li').children('a').each(function() {
                var ref = $(this).attr('href');
                if (url == ref) {
                    $(this).parent().addClass('active');
                }
            });
        },  
        
        setupDataTableDefaults: function() {
            $.extend($.fn.dataTable.defaults, {                
                "sDom": "<'row'<'span6 pull-left'l><'span6 pull-right'f>r>t<'row'<'span6 pull-left'i><'span6 pull-right'p>>",
                "sPaginationType": "bootstrap",
                "bJQueryUI": false,
                "bProcessing": false,
                "bServerSide": true,
                "fnServerData": TopControls.common.getTableData
            });
        },
        
        getTableData: function (sSource, aoData, fnCallback) {
            var data = new Array();

            var columnCount = _.find(aoData, function(o) { return o.name == 'iColumns'; }).value;
            var echo = _.find(aoData, function (o) { return o.name == 'sEcho'; }).value;
            var skip = _.find(aoData, function (o) { return o.name == 'iDisplayStart'; }).value;
            var take = _.find(aoData, function (o) { return o.name == 'iDisplayLength'; }).value;
            var search = _.find(aoData, function (o) { return o.name == 'sSearch'; }).value;
            var sortCols = _.filter(aoData, function (o) { return o.name.indexOf('iSortCol_') == 0; });
            var sortDirs = _.filter(aoData, function (o) { return o.name.indexOf('sSortDir_') == 0; });
            var searches = _.filter(aoData, function (o) { return o.name.indexOf('sSearch_') == 0; });

            data.push({ "name": "TableEcho", "value": echo });
            data.push({ "name": "Skip", "value": skip });
            data.push({ "name": "Take", "value": take });
            data.push({ "name": "AllSearch", "value": search });

            var actual = 0;

            _.each(sortCols, function (columnSort,sortIndex) {
                var columnIndex = columnSort.value;
                var columnSearch = searches[columnIndex].value;
                var sortDir = sortDirs[sortIndex].value;
                
                data.push({ "name": "Columns[" + actual + "].ColumnIndex", "value": columnIndex });
                data.push({ "name": "Columns[" + actual + "].SortDirection", "value": sortDir });
                
                if (columnSearch != '') {
                    data.push({ "name": "Columns[" + actual + "].SearchTerm", "value": columnSearch });
                }

                actual++;
            });

            for (var i = 0; i < columnCount; i++) {
                var searchTerm = searches[i].value;
                if (searchTerm == '') {
                    continue;
                }
                data.push({ "name": "Columns[" + actual + "].ColumnIndex", "value": i });
                data.push({ "name": "Columns[" + actual + "].SearchTerm", "value": searchTerm });
                actual++;
            }

            $.post(sSource, data)
                .success(fnCallback);
        },
        
        initPlugins: function () {
            $('select[data-select2-default]').each(function () {
                var el = $(this);
                
                el.select2({
                    width: TopControls.common.select2Width
                });

                var ev = el.attr("data-select2-default"); // el.val();

                setTimeout(function () {
                    el.select2("val", ev);
                }, 500);
            });
        },

        registerEnquire: function () {
            if (!$('html').hasClass('ie9') && !$('html').hasClass('lt-ie9')) {
                enquire.register('only all and (max-width: 480px)', {
                    match: function () {
                        // show sidebar
                        $('html').addClass('isSmallScreen');
                        $('select[data-select2-default]').select2("destroy");
                    },
                    unmatch: function () {
                        // hide sidebar
                        $('html').removeClass('isSmallScreen');
                    }

                }, true); // note the `true`!   
            }
        },
        
        select2Width: $('html').attr('class').indexOf('no-borderradius') > -1 ? 200 : 'resolve'
    };
    
    TopControls.common.init();
})();