(function () {

    var objectType = TopControls.objectType = {};

    objectType.init = function () {
        objectType.registerModalEvents();
    };

    objectType.registerModalEvents = function () {
        $("#modal-container").on('submit', "[data-form-action='create'], [data-form-action='edit'], [data-form-action='delete']", function (e) {
            e.preventDefault();
            $.post($(this).attr('action'), $(this).serialize(), function (result) {
                if (result.success) {
                    objectType.objectTypeTable.fnDraw();
                    $("#modal-container").modal('hide');
                } else {
                    $("#modal-container").html(result);
                }
            });
        });

        objectType.initDataTable();

    };

    objectType.initDataTable = function () {
        objectType.objectTypeTable = $("#objecttype-table").dataTable({
            aaSorting: [[0, "asc"]],
            bServerSide: true,
            bDestroy: true,
            sAjaxSource: TopControls.objectType.listUrl,
            fnRowCallback: function (nRow, aData) {
                $('td:eq(2)', nRow).html(TopControls.objectType.editLink.replace("idPlaceholder", aData[4]) + ' ' + TopControls.objectType.deleteLink.replace("idPlaceholder", aData[4]));
            },
            aoColumns:
            [
                { sType: "string", sClass: "dtAlignLeft" }
                , { sType: "string", sClass: "dtAlignLeft" }
                , { "bSortable": false }
            ]
        });
    };
})();

