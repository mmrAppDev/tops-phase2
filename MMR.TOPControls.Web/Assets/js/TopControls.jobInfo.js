(function () {

    var jobInfo = TopControls.jobInfo = {};

    jobInfo.init = function () {
		jobInfo.registerModalEvents();
    };

	jobInfo.registerModalEvents = function() {
		$("#modal-container").on('submit', "[data-form-action='create'], [data-form-action='edit'], [data-form-action='delete']", function (e) {
			e.preventDefault();
			$.post($(this).attr('action'), $(this).serialize(), function (result) {
				if (result.success) {
				    jobInfo.jobInfoTable.fnDraw();
					$("#modal-container").modal('hide');
				} else {
					$("#modal-container").html(result);
				}
			});
		});

		jobInfo.initDataTable();

	};

    jobInfo.initDataTable = function() {
            jobInfo.jobInfoTable = $("#jobinfo-table").dataTable({
                aaSorting: [[0, "asc"]],
                bServerSide: true,
                bDestroy: true,
                sAjaxSource: TopControls.jobInfo.listUrl,
                fnRowCallback: function (nRow, aData) {
                    $('td:eq(4)', nRow).html(TopControls.jobInfo.editLink.replace("idPlaceholder", aData[4]) + ' ' + TopControls.jobInfo.settingsLink.replace("idPlaceholder", aData[4]) + ' ' + TopControls.jobInfo.deleteLink.replace("idPlaceholder", aData[4]));
                },
                aoColumns:
                [
					{ sType: "string", sClass: "dtAlignLeft" }
		            , { sType: "string", sClass: "dtAlignLeft", "bSortable": false }
		            , { sType: "string", sClass: "dtAlignLeft", "bSortable": false }
		            , { sType: "string", sClass: "dtAlignLeft", "bSortable": false }
                    , { "bSortable": false }
				]
            });
		};
})();

