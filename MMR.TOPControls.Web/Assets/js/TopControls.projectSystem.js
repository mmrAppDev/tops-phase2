(function () {

    var projectSystem = TopControls.projectSystem = {};

    projectSystem.init = function () {
		projectSystem.registerModalEvents();
    };

	projectSystem.registerModalEvents = function() {
		$("#modal-container").on('submit', "[data-form-action='create'], [data-form-action='edit'], [data-form-action='delete']", function (e) {
			e.preventDefault();
			$.post($(this).attr('action'), $(this).serialize(), function (result) {
				if (result.success) {
				    $("#modal-container").modal('hide');
				    $(document).trigger('modal::system-updated');
				} else {
					$("#modal-container").html(result);
				}
			});
		});
	};
})();

