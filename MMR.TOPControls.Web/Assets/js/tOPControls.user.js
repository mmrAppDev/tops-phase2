(function () {
    var user = TopControls.user = {};

    user.init = function () {
        user.registerModalEvents();
    };

    user.registerModalEvents = function () {
        $("#modal-container").on('submit', "form", function (e) {
            e.preventDefault();
            $.post($(this).attr('action'), $(this).serialize(), function (result) {
                if (result.success) {
                    $("#modal-container").modal('hide');
                } else {
                    $("#modal-container").html(result);
                    TopControls.common.initModalValidation();
                }
            });
        });
    };
})();
