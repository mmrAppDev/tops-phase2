(function () {

    var workCrew = TopControls.workCrew = {};

    workCrew.init = function () {
		workCrew.registerModalEvents();
    };

	workCrew.registerModalEvents = function() {
		$("#modal-container").on('submit', "[data-form-action='create'], [data-form-action='edit'], [data-form-action='delete']", function (e) {
			e.preventDefault();
			$.post($(this).attr('action'), $(this).serialize(), function (result) {
				if (result.success) {
				    workCrew.workCrewTable.fnDraw();
					$("#modal-container").modal('hide');
				} else {
					$("#modal-container").html(result);
				}
			});
		});

		workCrew.initDataTable();

	};

    workCrew.initDataTable = function() {
            workCrew.workCrewTable = $("#workcrew-table").dataTable({
                aaSorting: [[0, "asc"]],
                bServerSide: true,
                bDestroy: true,
                sAjaxSource: TopControls.workCrew.listUrl,
                fnRowCallback: function (nRow, aData) {
                    $('td:eq(2)', nRow).html(TopControls.workCrew.editLink.replace("idPlaceholder", aData[3]) + ' ' + TopControls.workCrew.deleteLink.replace("idPlaceholder", aData[3]));
                },
                aoColumns:
                [
                    { sType: "numeric", sClass: "dtAlignRight" }
		            , { sType: "string", sClass: "dtAlignLeft" }
                    , { "bSortable": false }
				]
            });
		};
})();

