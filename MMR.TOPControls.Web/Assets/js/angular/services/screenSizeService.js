﻿'use strict';

(function () {
    // Service object
    var screenSizeService = function ($timeout, enquire) {
        var exports = {};

        if (!$('html').hasClass('ie9') && !$('html').hasClass('lt-ie9')) {
            enquire.register('only all and (max-width: 480px)', {
                match: function () {
                    $timeout(function () {
                        exports.isSmallScreen = true;
                    }, 0);
                },
                unmatch: function () {
                    $timeout(function () {
                        exports.isSmallScreen = false;
                    }, 0);
                }

            }, true); // note the `true`!   
        }

        exports.isSmallScreen = false;

        return exports;
    };

    // Injected dependancies
    screenSizeService.$inject = ['$timeout', 'enquire'];

    // Service creation
    angular.module('Services')
        .factory('screenSizeService', screenSizeService);
})();