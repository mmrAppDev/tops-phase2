﻿'use strict';

(function () {
    // Service object
    var tagService = function ($http, $q) {
        var exports = {};
        var baseUrl = window.__BaseUrl;

        var convertFilterToParams = function (filter, pageNum, pageSize) {
            return {
                PhaseId: filter.phase,
                Tasks: filter.task,
                SearchText: filter.searchText,
                Commodities: filter.commodities,
                ObjectTypes: filter.objectTypes,
                Systems: filter.systems,
                Areas: filter.areas,
                CraftInfo: filter.craftInfo,
                Udp1: filter.udp1,
                Udp2: filter.udp2,
                Udp3: filter.udp3,
                PageNum: pageNum,
                PageSize: pageSize
            };
        };
        
        var convertTrackedItemToParams = function (item) {
            return {
                JobID: item.JobID,
                ID: item.ID,
                TrackingID: item.TrackingID,
                ObjectTypeID: item.ObjectTypeID,
                SystemID: item.SystemID,
                AreaID: item.AreaID,
                CraftID: item.CraftID,
                UDPName1: item.UDPName1,
                UDPName2: item.UDPName2,
                UDPName3: item.UDPName3,
                UDPValue1: item.UDPValue1,
                UDPValue2: item.UDPValue2,
                UDPValue3: item.UDPValue3,
                TaskID: item.TaskID,
                Comments: item.Comments
            };
        };

        exports.getFilteredTrackedItems = function (filter, pageNum, pageSize) {
            var url = baseUrl + 'Home/GetFilteredTags?workDate=' + encodeURIComponent(filter.date);
            var params = convertFilterToParams(filter, pageNum, pageSize);

            return $http.post(url, params).then(function(response) {
                return response.data;
            });
        };
        
        exports.getFilteredPercentComplete = function (filter) {
            var url = baseUrl + 'Home/GetFilteredPercentComplete';
            var params = convertFilterToParams(filter, 0, 0);

            return $http.post(url, params).then(function (response) {
                return response.data;
            });
        };


        exports.deleteProgressEntry = function DeleteProgressEntry(entryId, taskProgressID) {
            var url = baseUrl + 'Home/DeleteProgressEntry?';

            var params = [
                'progressEntryId=' + entryId,
                'taskProgressID=' + taskProgressID
            ];

            return $http.post(url + params.join('&')).then(function (response) {
                return response.data;
            });
        }

        exports.getTaskProgress = function GetTaskProgress(item) {
            var url = baseUrl + 'Home/GetTaskProgress?';

            var params = [
                'trackingId=' + encodeURIComponent(item.TrackingID),
                'taskId=' + item.TaskId
            ];
            
            return $http.get(url + params.join('&')).then(function (response) {
                response.data.Entries = response.data.Entries.map(function(e) {
                    e.WorkDate && (e.WorkDate = new XDate(e.WorkDate.split('T')[0]).toDate());
                    return e;
                });
                return response.data;
            });
        }

        exports.updateTrackedItemProgress = function(taskProgressID, percentComplete, filter) {
            var url = baseUrl + 'Home/SaveFilterProgress';
            
            var params = {
                TaskProgressID: taskProgressID,
                NewPecentComplete: percentComplete,
                WorkDate: filter.date,
                WorkCrewID: filter.workCrew
            };
            
            return $http.post(url, params).then(function (response) {
                return response.data;
            });
        };

        exports.getTrackedItemDetails = function(itemId, taskId) {
            var url = baseUrl + 'Home/GetTrackedItem/';

            var params = {
                id: itemId,
                taskId: taskId
            };

            return $http.post(url, params).then(function (response) {
                return response.data;
            }); 
        }

        exports.getCopyTrackedItem = function (trackedItemId, phaseId) {
            var url = baseUrl + 'Home/GetCopyTrackedItem/?trackedItemId=' + trackedItemId + '&phaseId=' + phaseId;
            
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        exports.copyTrackedItem = function (trackedItemId, taskId) {
            var url = baseUrl + 'Home/CopyTrackedItem/';

            var params = {
                trackedItemId: trackedItemId,
                taskId: taskId
            };

            return $http.post(url, params).then(function (response) {
                return response.data;
            });
        }

        exports.getNewItemOptions = function (taskId) {
            var url = baseUrl + 'Home/GetNewTrackedItem/';

            var params = {
                taskId: taskId
            };

            return $http.post(url, params).then(function (response) {
                return response.data;
            });
        }

        exports.updateTrackedItem = function (item) {
            var url = baseUrl + 'Home/UpdateTrackedItem';

            var params = convertTrackedItemToParams(item);

            return $http.post(url, params).then(function (response) {
                return response.data;
            });
        }

        exports.deleteTrackedItem = function (trackedItemId) {
            var url = baseUrl + 'Home/DeleteTrackedItem/';

            return $http.post(url, { trackedItemId: trackedItemId }).then(function (response) {
                return response.data;
            });
        }

        exports.addTrackedItem = function (item) {
            var url = baseUrl + 'Home/AddTrackedItem';

            var params = convertTrackedItemToParams(item);

            return $http.post(url, params).then(function (response) {
                return response.data;
            });
        }

        return exports;
    };

    // Injected dependancies
    tagService.$inject = ['$http', '$q'];

    // Service creation
    angular.module('Services')
        .factory('TagService', tagService);
})();