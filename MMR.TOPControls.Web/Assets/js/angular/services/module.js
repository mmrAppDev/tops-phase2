﻿(function () {
    'use strict';

    angular
        .module('Services', [])
        .value('enquire', window.enquire);
})();