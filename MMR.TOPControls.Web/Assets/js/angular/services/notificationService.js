﻿(function () {
    
    'use strict';
    
    function notificationService($rootScope, $timeout) {
        var self = this;
        
        var topNotification = $('.top-notification');
        var loadingPromise = null;
        
        this.hideMessage = hideMessage;
        this.showMessage = showMessage;
        this.showLoadingMessage = showLoadingMessage;
        
        function cancelPromise() {
            if (loadingPromise) {
                $timeout.cancel(loadingPromise);
                loadingPromise = null;
            }
            topNotification.stop().hide();
        };

        function showMessage(notificationText, lifetime) {
            // Previous message cleanup
            cancelPromise();

            // new message setup
            topNotification.fadeIn();
            topNotification.text(notificationText);

            if (lifetime) {
                $timeout(self.hideMessage, lifetime);
            }
        };

        function hideMessage() {
            topNotification.fadeOut('slow', function () {
                topNotification.text('');
            });

            loadingPromise = null;
        };
        
        function showLoadingMessage(notificationText) {
            cancelPromise();

            loadingPromise = $timeout(function () {
                self.showMessage(notificationText);
                loadingPromise = $timeout(function () {
                    self.hideMessage();
                }, 3000);
            }, 2000);
        };
    };

    // Injected dependancies
    notificationService.$inject = ['$rootScope', '$timeout'];

    // Service creation
    angular.module('Services')
        .service('NotificationService', notificationService);
})();