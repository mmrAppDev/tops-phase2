﻿'use strict';

(function () {
    // Service object
    var chartDataService = function ($http, $q) {
        var exports = {};
        var baseUrl = window.__BaseUrl;
        var apiBaseUrl = window.__ProjectStatusBaseUrl;

        var getApiUrl = function(url, hasParams) {
            var qsSeperator = (hasParams) ? '&' : '?';
            var cacheBuster = qsSeperator + '_cacheBuster=' + new Date().getTime();
            return apiBaseUrl + url + cacheBuster;
        };

        exports.getJobPercentComplete = function () {
            var url = getApiUrl("/GetJobPercentComplete");
            return $http.get(url).then(function (response) {
                return response.data;
            });
        };

        exports.getJobProgressHistory = function () {
            var url = getApiUrl("/GetJobCompleteChartData");
            return $http.get(url).then(function (response) {
                var chartData = _.map(response.data.ChartValues, function (progressObj) {
                    return parseDataPoint(progressObj);
                });

                var result = {
                    title: response.data.ContextTitle,
                    data: _.sortBy(chartData, function(date) { return date; }),
                    totalManhours: getManhourLabel(response.data)
                };
                return result;
            });
        };
        
        exports.getPhaseSummaries = function () {
            var url = getApiUrl("/GetPhaseSummaries");
            return $http.get(url).then(function (response) {
                return response.data;
            });
        };

        exports.getTrackedItemCompletionByPhase = function (trackedItemId) {
            var url = getApiUrl("/GetTrackedItemCompletionByPhase?trackedItemId=" + trackedItemId, true);
            return $http.get(url).then(function (response) {
                return response.data;
            });
        };

        exports.getTaskProgressHistory = function (taskId) {
            var url = getApiUrl("/GetTasksChartData?taskID=" + taskId, true);
            return $http.get(url).then(function (response) {
                var chartData = _.map(response.data.ChartValues, function (progressObj) {
                    return parseDataPoint(progressObj);
                });
                var result = {
                    title: response.data.ContextTitle,
                    data: _.sortBy(chartData, function (date) { return date; }),
                    totalManhours: getManhourLabel(response.data)
                };
                return result;
            });
        };

        exports.getPhaseProgressHistory = function (phaseId) {
            var url = getApiUrl("/GetPhaseChartData?phaseID=" + phaseId, true);
            return $http.get(url).then(function (response) {
                var chartData = _.map(response.data.ChartValues, function (progressObj) {
                    return parseDataPoint(progressObj);
                });
                var result = {
                    title: response.data.ContextTitle,
                    data: _.sortBy(chartData, function (date) { return date; }),
                    totalManhours: getManhourLabel(response.data)
                };
                return result;
            });
        };
        
        exports.getSystemProgressHistory = function (systemId) {
            var url = getApiUrl("/GetSystemHistoryChartData?systemID=" + systemId, true);
            return $http.get(url).then(function (response) {
                var chartData = _.map(response.data.ChartValues, function (progressObj) {
                    return parseDataPoint(progressObj);
                });
                var result = {
                    title: response.data.ContextTitle,
                    data: _.sortBy(chartData, function (date) { return date; }),
                    totalManhours: getManhourLabel(response.data)
                };
                return result;
            });
        };

        function getManhourLabel(data) {
            if (data.ShowEarnedToClient) {
                return 'Earned Man-hours ' + data.CompletedManhours + ' / ' + data.TotalManhours;
            }

            return '';
        }

        function parseDataPoint(progressObj) {

            var d = new XDate(new Date(parseInt(progressObj.WorkDate.substr(6))));

            return [
                Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()),
                parseFloat(((progressObj.PercentComplete) * 100).toFixed(3)),
                parseFloat(((progressObj.TotalManHours)).toFixed(2)),
                parseFloat(((progressObj.CompletedManHours)).toFixed(2))
            ];
        }
        
        exports.getSystemStatusSummary = function (phaseId) {
            var url = getApiUrl("/GetProjectSystemsProgress?phaseID=" + phaseId, true);
            return $http.get(url).then(function (response) {
                return response.data;
            });
        };

        exports.getTasksByPhase = function (phaseId) {
            var url = getApiUrl("/GetTasksByPhase?phaseID=" + phaseId, true);
            return $http.get(url).then(function (response) {
                return response.data.Tasks;
            });
        };
        
        exports.getPhases = function () {
            var url = getApiUrl("/GetPhases");
            return $http.get(url).then(function (response) {
                return response.data.Phases;
            });
        };

        exports.getSystems = function () {
            var url = getApiUrl("/GetSystemList");
            return $http.get(url).then(function (response) {
                return response.data.Systems;
            });
        };

        exports.updateTrackedItemComment = function (id, comments) {
            var url = getApiUrl("/UpdateTrackedItemComment");
            var data = { id: id, comments: comments };
            return $http.post(url, data).then(function (response) {
                return response.data;
            });
        };


        return exports;
    };

    // Injected dependencies
    chartDataService.$inject = ['$http', '$q'];

    // Service creation
    angular.module('Services')
        .factory('chartDataService', chartDataService);
})();