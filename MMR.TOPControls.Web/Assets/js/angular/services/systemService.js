﻿'use strict';

(function() {
    angular.module('Services')
        .service('eventBus', EventBus)
        .service('systemService', SystemService);

    EventBus.$inject = ['$rootScope'];
    function EventBus($rootScope) {

        this.on = on;
        this.emit = emit;

        function on(eventNamespace, handler, optDestroyScope) {
            var off = $rootScope.$on(eventNamespace, handler);

            if (optDestroyScope) {
                optDestroyScope.$on('destroy', off);
            }

            return off;
        }

        function emit() {
            return $rootScope.$emit.apply($rootScope, arguments);
        }
    }

    SystemService.$inject = ['$http', '$timeout', '$q', 'eventBus'];

    function SystemService($http, $timeout, $q, eventBus) {
        var baseUrl = window.__ProjectSystemBaseUrl;

        var svc = this;
        var pathMap = {};
        var sudoMapComplete = _.debounce(handleMapComplete, 100);
        var systemTree = [];

        svc.changePositionInTree = changePositionInTree;
        svc.getFlatSystemTree = getFlatSystemTree;
        svc.getSystems = getSystems;
        svc.merge = merge;

        function changePositionInTree(id, parentId, sortOrder) {
            var url = baseUrl + '/ChangePositionInTree';
            var data = {
                ID: id,
                ParentId: parentId,
                SortOrder: sortOrder
            }

            return $http.post(url, data).then(unwrapResponse);
        }

        function getSystems() {
            var url = baseUrl + '/ListItems';
            return $http.get(url).then(unwrapResponse);
        }

        function getFlatSystemTree() {
            var dfd = $q.defer();

            var deregister = eventBus.on('handleMapComplete', onHandleMapComplete);

            getSystems().then(onGetSystemsSuccess);

            return dfd.promise;

            function onGetSystemsSuccess(systems) {
                systemTree.length = 0;
                systems.data.forEach(function (child) {
                    $timeout(function () {
                        mapSystem(child);
                    });
                });
            }

            function onHandleMapComplete(evt) {
                deregister();
                dfd.resolve(systemTree);
            }
        }

        function getSystemPathParents(idMap) {
            if (!idMap)
                return '';

            var parents = idMap.split('_').map(getById).reverse();
            parents.pop();

            return parents;

            function getById(systemId) {
                return pathMap[systemId];
            }
        }
        
        function handleMapComplete() {
            var sortedTree = _.sortBy(systemTree, function (s) {
                return s.parentPath.join('>');
            });

            systemTree.length = 0;
            angular.copy(sortedTree, systemTree);

            eventBus.emit('handleMapComplete');
        }

        function mapSystem(system, parentPath) {
            var path = system.ID + (parentPath ? '_' + parentPath : '');
            pathMap[system.ID] = system.DisplayName;
            system.path = path;
            system.parentPath = getSystemPathParents(path);
            system.parentDisplay = system.parentPath.join(' > ');
            system.searchText = system.parentDisplay + ' ' + system.DisplayName;

            systemTree.push(system);
            if (system.Children.length) {
                system.Children.forEach(function (child) {
                    $timeout(function () {
                        mapSystem(child, path);
                        sudoMapComplete();
                    });
                });
            }
        }

        function merge(sourceId, destinationId) {
            var url = baseUrl + '/Merge';

            var params = {
                sourceId:sourceId,
                destinationId: destinationId
            }
            return $http.post(url, params).then(unwrapResponse);
        }

        function unwrapResponse(resp) {
            return resp.data;
        }
    }
})();