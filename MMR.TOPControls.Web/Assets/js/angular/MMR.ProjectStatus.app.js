﻿'use strict';

(function () {
    // Create an application
    var app = angular.module('MMR.ProjectStatus', [
        'Controllers',
        'Services',
        'highcharts-ng',
        'yaru22.directives.hovercard',
        'ngSanitize',
        'ui.select',
        'angular.filter',
        'monospaced.elastic'
    ]);

    app.run(function () {
        $(document).off('keypress.finder');
        $(document).on('keypress.finder', function(e) {
            var focuser = $('.selectize-input');
            var hasFocus = focuser.is('.selectize-focus') || focuser.is('.focus');
            if (!hasFocus && e.which === 102) {
                focuser.click();
            }
        });
    });
})();