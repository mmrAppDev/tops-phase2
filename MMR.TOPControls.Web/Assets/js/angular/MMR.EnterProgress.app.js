﻿'use strict';

(function() {
    // Create an application
    var app = angular.module('MMR.EnterProgress', [
        'Controllers',
        'Services',
        'Directives',
        'Filters',
        'ui.bootstrap',
        'ui.bootstrap.pagination',
        'ui.select',
        'monospaced.elastic'
    ]);

    app.config(function () {
        
    });
})();