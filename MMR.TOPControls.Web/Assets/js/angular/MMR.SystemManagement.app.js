﻿'use strict';

(function () {
    // Create an application
    angular.module('MMR.SystemManagement', ['Services', 'Directives', 'Filters', 'ui.tree', 'ui.bootstrap', 'ui.select'])
        .run(bootstrap)
        .controller('SystemManagementController', SystemManagementController)
        .controller('MergeSystemsCtrl', MergeSystemsCtrl);

    bootstrap.$inject = ['$rootScope', 'treeConfig'];
    function bootstrap($rootScope, treeConfig) {
        $rootScope.itemRenderUrl = window.itemRenderUrl;

        treeConfig.dragClass = 'angular-ui-tree-drag tree';
    }

    SystemManagementController.$inject = ['$timeout', '$modal', 'systemService', 'NotificationService'];
    function SystemManagementController($timeout, $modal, projectSystems, NotificationService) {
        var vm = this;
        var hiddenMap = {};
        var allKeysMap = {};

        vm.list = [];
        vm.deleteLink = window.__deleteLink;
        vm.createLink = window.__createLink;
        vm.editLink = window.__editLink;

        vm.addNodeId = addNodeId;
        vm.isHidden = isHidden;
        vm.openMergeDialog = openMergeDialog;
        vm.shouldExpandAll = shouldExpandAll;
        vm.toggleHidden = toggleHidden;
        vm.toggleCollapse = toggleCollapse;

        vm.treeOptions = {
            dropped: onItemDropped,
            accept: accept
        }

        init();
        function init() {
            projectSystems.getSystems().then(updateTree);

            $(document).on('modal::system-updated', function () {
                projectSystems.getSystems().then(updateTree);
            });
        }

        function accept(sourceNodeScope, destNodesScope, destIndex) {
            var srcId = sourceNodeScope.$modelValue.ID;

            var destParent = destNodesScope.$nodeScope;
            var destParentId = destParent ? destParent.$modelValue.ID : 'ROOT';

            var preventInceptionBug = (srcId != destParentId);

            console.log('srcId', srcId, 'destParentId', destParentId);

            return preventInceptionBug && true;
        }

        function addNodeId(id) {
            allKeysMap["_" + id] = true;
        }

        function isHidden(id) {
            return !!hiddenMap["_" + id];
        }

        function onItemDropped(event) {
            var src = event.source;
            var srcParent = src.nodeScope.$parentNodeScope;
            var srcId = srcParent ? srcParent.$modelValue.ID : null;
            var srcParentName = srcParent ? srcParent.$modelValue.DisplayName : "Top Level";

            var model = src.nodeScope.$modelValue;
            var itemId = model.ID;

            var dest = event.dest;
            var destParent = dest.nodesScope.$nodeScope;
            var destId = destParent ? destParent.$modelValue.ID : null;
            var destParentName = destParent ? destParent.$modelValue.DisplayName : "Top Level";
            var newIndex = dest.index == 0 ? -1 : dest.index;

            var invalidIndex = src.index < 0 || dest.index < 0;

            var noPositionChanged = (src.index == dest.index) && (srcId == destId);

            if (invalidIndex || noPositionChanged) {
                return;
            }

            var baseUrl = window.__BaseUrl;
            var modalInstance = $modal.open({
                templateUrl: baseUrl + 'Assets/js/angular/template/confirmSystemMove.html',
                controller: function ($scope, $modalInstance, data) {
                    $scope.data = data;

                    $scope.confirmMove = confirmMove;
                    $scope.cancel = cancel;

                    function confirmMove() {
                        $modalInstance.close(true);
                    }
                    function cancel() {
                        $modalInstance.close(false);
                    }
                },
                resolve: {
                    data: function () {
                        return {
                            model: model,
                            from: srcParentName,
                            to: destParentName
                        };
                    }
                }
            });

            modalInstance.result.then(onClose, onClose);

            function onClose(val) {
                if (val === true) {
                    projectSystems
                        .changePositionInTree(itemId, destId, newIndex)
                        .then(onPositionUpdated, onError);
                } else {
                    projectSystems.getSystems().then(updateTree);
                };
            }

            function onError() {
                NotificationService.showMessage("Error updating item.", 3000);
            }

            function onPositionUpdated(resp) {
                if (!resp.success) {
                    vm.list.length = 0;
                    NotificationService.showMessage(resp.message, 3000);
                }

                $timeout(function () {
                    updateTree(resp);
                });
            }

            //console.log('ITEM ID: ' + itemId);
            //console.log('FROM INDEX: ' + src.index + ' of parent: ' + srcId);
            //console.log('TO INDEX: ' + dest.index + ' of parent: ' + destId, newIndex);
            //console.log('ITEM NAME: ' + model.DisplayName);
            //console.log('FROM: ' + srcParentName);
            //console.log('TO: ' + destParentName);
        }

        function openMergeDialog() {
            var baseUrl = window.__BaseUrl;

            var modalInstance = $modal.open({
                templateUrl: baseUrl + 'Assets/js/angular/template/mergeSystemsDialog.html',
                controller: 'MergeSystemsCtrl',
                resolve: {
                    systems: projectSystems.getFlatSystemTree
                }
            });

            modalInstance.result.then(function () {
                projectSystems.getSystems().then(updateTree);
            });
        }

        function shouldExpandAll() {
            return _.keys(hiddenMap).length > 0;
        }

        function toggleCollapse() {
            if (shouldExpandAll()) {
                hiddenMap = {};
            } else {
                _.extend(hiddenMap, allKeysMap);
            }
        }

        function toggleHidden(id) {
            hiddenMap["_" + id] = !hiddenMap["_" + id];
        }

        function updateTree(resp) {
            vm.list = resp.data;
        }
    }

    MergeSystemsCtrl.$inject = ['$scope', '$modalInstance', 'systemService', 'systems'];
    function MergeSystemsCtrl($scope, $modalInstance, systemService, systems) {
        $scope.systemTree = systems;
        $scope.sourceSystem = {};
        $scope.destinationSystem = {};

        $scope.cancel = cancel;
        $scope.disableMerge = disableMerge;
        $scope.ok = merge;
        $scope.onSelectSource = onSelectSource;
        $scope.onSelectDestination = onSelectDestination;

        function cancel() {
            $modalInstance.dismiss('cancel');
        };

        function disableMerge() {
            return !$scope.sourceSystem.ID ||
                !$scope.destinationSystem.ID ||
                ($scope.sourceSystem.ID == $scope.destinationSystem.ID);
        }

        function merge() {
            $scope.errorMessage = null;
            systemService
                .merge($scope.sourceSystem.ID, $scope.destinationSystem.ID)
                .then(function (response) {
                    if (response.success) {
                        $modalInstance.close();
                    } else {
                        $scope.errorMessage = response.message;
                    }
                });
        };

        function onSelectSource(system) {
            $scope.errorMessage = null;
            $scope.sourceSystem = system;
        }

        function onSelectDestination(system) {
            $scope.errorMessage = null;
            $scope.destinationSystem = system;
        }
    }
})();