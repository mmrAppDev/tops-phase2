﻿angular.module("Directives")
    .directive('datepicker', function factory() {

        var directiveDefinitionObject = {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModelCtrl) {
                $(function() {
                    element.datepicker();
                    
                    element.bind('changeDate', function (e) {
                        element.datepicker('hide');
                        var date = new XDate(e.date).toString('M/d/yy');
                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    });

                    $(document).on('click', '#btnSetDateToday', function (e) {
                        var date = new XDate(new Date()).toString('M/d/yy');
                        $('#selectedDate').val(date);
                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    });
                });
            }
        };

        return directiveDefinitionObject;
    });