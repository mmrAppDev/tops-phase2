﻿'use strict';

(function () {

    systemBreakdownController.$inject = ['$scope', 'chartDataService'];
    function systemBreakdownController($scope, chartDataService) {

        var fuse;
        var MIN_MATCH_SCORE = 0.3;
        var pathMap = window.pathMap = {};
        var onPathBuildComplete = _.debounce(_onPathBuildComplete, 100);

        $scope.BaseUrl = __BaseUrl;
        $scope.expandState = {};
        $scope.expandAllState = {};
        $scope.itemList = [];
        $scope.filteredItems = [];
        $scope.systems = [];
        $scope.state = {
            loadingGroupedData: false
        };

        $scope.addPath = addPath;
        $scope.formatDisplayName = formatDisplayName;
        $scope.getPath = getPath;
        $scope.getStage = getStage;
        $scope.getSystemsByPhase = getSystemsByPhase;
        $scope.isRoot = isRoot;
        $scope.itemDepth = itemDepth;
        $scope.matchSelectedPhaseName = matchSelectedPhaseName;
        $scope.onSearchSelect = onSearchSelect;
        $scope.onTrackedItemHover = onTrackedItemHover;
        $scope.searchItems = searchItems;
        $scope.toggle = toggle;
        
        function addPath(children) {
            children.forEach(mapId);

            function mapId(child) {
                var modifier = child.IsSystem ? 'system_' : 'tracked_item_';

                var pathData = {
                    type: child.IsSystem ? 'System' : 'Tracked Item',
                    id: child.ID,
                    path: child.Path,
                    name: child.Name
                };

                pathMap[modifier + child.ID] = pathData;
                $scope.itemList.push(pathData);

                if (child.Children.length) {
                    setTimeout(function() {
                        addPath(child.Children);
                        onPathBuildComplete();
                    }, 0);
                }
            }
        }

        function formatDisplayName(itemName, omitTaskName) {
            if (!itemName) {
                return "";
            }
            var parts = itemName.split(' ::: ');
            parts[0] = String.format('<span class="split-item-name">{0}</span>', parts[0]);
            parts[1] && (parts[1] = String.format('<span class="split-task-name">{0}</span>', parts[1]));

            if (omitTaskName) {
                parts = [parts[0]];
            }

            return parts.join(' ');
        }

        function getPath(item) {
            if (!item)
                return "";

            var path = item.path.split('_');

            if (item.type != "System") {
                path.pop();
            }

            if (path.length < 2) {
                return "";
            }

            return path.map(function (id) {
                return pathMap['system_' + id].name;
            }).join('\n--------------\n');
        }

        function getStage(completionPercent) {
            switch (true) {
                case completionPercent > 0 && completionPercent <= 0.33:
                    return 'stage1';
                case completionPercent > 0.33 && completionPercent <= 0.66:
                    return 'stage2';
                case completionPercent > 0.66:
                    return 'stage3';
                default:
                    return '';
            }
        }

        function getSystemsByPhase() {
            $scope.state.loadingGroupedData = true;
            chartDataService.getSystemStatusSummary($scope.state.systemPhase).then(function (resp) {
                $scope.systems.length = 0;
                angular.copy(resp.data, $scope.systems);
                $scope.state.loadingGroupedData = false;

                pathMap = window.pathMap = {};
                addPath($scope.systems);
            });
        }

        function matchSelectedPhaseName(name) {
            return _.findWhere($scope.phaseSummaries, { PhaseID: parseInt($scope.state.systemPhase) }).Title == name;
        }

        function isRoot(item) {
            return item.Path.split('_').length == 1;
        }

        function itemDepth(item) {
            return item.Path.split('_').length;
        }

        function onSearchSelect($item, $model) {
            var systems = $item.path.split('_');

            systems.pop();
            systems.forEach(function(id) {
                $scope.expandState[id] = true;
            });

            var selector = $item.type == "System" ? 'system_' : 'tracked_item_';

            setTimeout(function() {
                var el = $('[item-id="' + selector + $item.id + '"]');
                $("html, body").animate({ scrollTop: el.position().top - 150 }, 500, function() {
                    el.highlight({ time: 2000 });
                });
            }, 500);
        }

        function onTrackedItemHover(itemId) {
            return function onTrackedItemHoverProxy() {
                $scope.$broadcast('trackedItem::update::' + itemId);
            }
        }

        function searchItems($search) {
            if ($search.length < 3) {
                $scope.filteredItems = [];
                return;
            }

            $scope.filteredItems = _.chain(fuse.search($search))
                .filter(filterMinScore)
                .sortBy(fixSort)
                .pluck('item')
                .value();

            function filterMinScore(o) {
                return o.score < MIN_MATCH_SCORE;
            };

            function fixSort(o) {
                return o.score + ' ' + o.item.type;
            }
        }

        function toggle(systemId) {
            $scope.expandState[systemId] = !$scope.expandState[systemId];
        }
        
        function _onPathBuildComplete() {
            var sorted = _.sortBy($scope.itemList, function (i) {
                return i.type + i.name;
            });

            fuse = new Fuse(sorted, { keys: ["name"], includeScore: true });
            $scope.expandState = {};
            $scope.expandAllState = {};
            _.forEach(pathMap, function (val, key) {
                if (val.type == "System") {
                    $scope.expandAllState[val.id] = true;
                }
            });
        }
    }

    HoverDetailCtrl.$inject = ['$scope', 'chartDataService'];

    function HoverDetailCtrl($scope, chartDataService) {
        var vm = this;
        var defaultSaveButtonText = 'Save Comment';

        vm.init = init;
        vm.trackedItemData = [];
        vm.updateTrackedItem = updateTrackedItem;
        vm.saveButtonText = defaultSaveButtonText;

        function onHoverIn() {
            vm.loading = true;
            vm.canEdit = window.__CanEditComments;
            chartDataService.getTrackedItemCompletionByPhase(vm.itemId).then(onSuccess);

            function onSuccess(response) {
                vm.loading = false;
                angular.copy(response.data, vm.trackedItemData);
            }
        }

        function init(itemId) {
            vm.itemId = itemId;
            $scope.$on('trackedItem::update::' + itemId, onHoverIn);
        }

        function updateTrackedItem(item) {
            vm.errorText = '';
            vm.saveButtonText = 'Saving...';

            chartDataService.updateTrackedItemComment(item.ID, item.Comments).then(onSuccess, onError);

            function onSuccess(response) {
                vm.saveButtonText = defaultSaveButtonText;
                vm.errorText = response.errors;
            }

            function onError() {
                vm.saveButtonText = 'Error Saving';
            }

        }
    }

    angular.module('Controllers')
        .controller('SystemBreakdownController', systemBreakdownController)
        .controller('HoverDetailCtrl', HoverDetailCtrl);
})();