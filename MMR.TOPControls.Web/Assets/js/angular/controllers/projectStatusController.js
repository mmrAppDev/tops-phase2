﻿'use strict';

(function () {

    projectStatusTabsController.$inject = ['$scope', 'chartDataService', 'screenSizeService'];
    function projectStatusTabsController($scope, chartDataService, screenSize) {
        $scope.screenSize = screenSize;
        $scope.displayType = 'percent';
        $scope.activeTab = 'overview';
        $scope.phaseSummaries = [];

        $scope.setActiveTab = setActiveTab;

        init();
        function init() {
            $scope.$watch('phaseSummaries', calcPhaseHeight);
            $scope.$watch('displayType', function (curr, prev) {
                if (curr != prev) {
                    $scope.$broadcast('displayType::changed');
                    $('#toggle-displayType .btn').removeClass('active');
                    setTimeout(function () { $('#toggle-' + curr).addClass('active'); }, 0);
                }
            });

            chartDataService.getPhaseSummaries().then(function (data) {
                $scope.phaseSummaries = data.Phases;
            });
        }

        function setActiveTab(tab) {
            $scope.activeTab = tab;
        }

        function calcPhaseHeight(newVal, oldVal) {
            if (newVal.length) {
                setTimeout(function () {
                    var tallest = _.max(angular.element('.horizontal-phase .level'), function (el) {
                        return angular.element(el).height();
                    });

                    var newHeight = angular.element(tallest).height();
                    angular.element('.horizontal-phase .level').height(newHeight);
                }, 0);
            }
        };
    }

    projectStatusController.$inject = ['$scope', 'chartDataService', '$timeout'];
    function projectStatusController($scope, chartDataService, $timeout) {
        $scope.prevPhase = prevPhase;
        $scope.nextPhase = nextPhase;
        $scope.prevTask = prevTask;
        $scope.nextTask = nextTask;
        $scope.prevSystem = prevSystem;
        $scope.nextSystem = nextSystem;

        $scope.jobPercentComplete = 0;
        

        $scope.state = {
            taskPhase: null,
            currentTaskIdx: null,
            loadingTaskData: false,
            tasks: [],
            currentPhaseIdx: null,
            loadingPhaseData: false,
            systems: [],
            currentSystemIdx: null,
            loadingSystemData: false,
            loadingJobData: false,
            loadingGroupedData: false,
            phases: []
        };
        
        $scope.jobPercentCompleteChart = {
            options: {
                chart: {
                    type: 'bar',
                    height: 50, backgroundColor: '#CCC', borderRadius: 0,
                    spacingBottom: 0, spacingLeft: 0, spacingRight: 0, spacingTop: 0,
                    renderTo: 'jobPercentComplete'
                },
                title: null,
                xAxis: {
                    maxPadding: 0, title: null, labels: { enabled: false }
                },
                yAxis: {
                    min: 0, max: 100, maxPadding: 0, gridLineWidth: 0,
                    title: null, labels: { enabled: false }
                },
                plotOptions: {
                    bar: { color: '#762A2E', groupPadding: 0, pointPadding: 0 }
                },
                credits: { enabled: false },
                tooltip: { enabled: false },
                legend: { enabled: false },
                exporting: { enabled: false }
            },
            series: [{ data: [$scope.jobPercentComplete] }]
        };

        $scope.jobProgressChart = {
            options: {
                chart: {
                    type: "line",
                    zoomType: 'x', height: 300, borderRadius: 0,
                    spacingBottom: 10, spacingLeft: 0, spacingRight: 0, spacingTop: 10,
                    renderTo: 'jobProgressChart'
                },
                title: null,
                xAxis: {
                    type: 'datetime', labels: { rotation: 60 }
                },
                yAxis: { title: null, min: 0 },
                tooltip: { "valueSuffix": "%" },
                plotOptions: {
                    line: { color: '#FDAC62', lineWidth: 4 },
                    series: { marker: { radius: 4 } }
                },
                legend: { enabled: false },
                credits: { enabled: false }
            },
            series: [{ data: [] }],
            title: null
        };

        $scope.taskProgressChart = {
            options: {
                chart: {
                    type: "area",
                    zoomType: 'x', height: 200, borderRadius: 0,
                    spacingBottom: 10, spacingLeft: 0, spacingRight: 0, spacingTop: 10,
                    renderTo: 'taskProgressChart'
                },
                title: null,
                xAxis: {
                    type: 'datetime',
                    labels: { rotation: 60 }
                },
                yAxis: { title: null, max: 100, min: 0 },
                tooltip: { "valueSuffix": "%" },
                plotOptions: {
                    area: { color: '#762A2E', lineWidth: 4, fillOpacity: 1 },
                    series: { marker: { enabled: false } }
                },
                legend: { enabled: false },
                credits: { enabled: false }
            },
            series: [{ data: [] }],
            title: null
        };

        $scope.phaseProgressChart = {
            options: {
                chart: {
                    type: "area",
                    zoomType: 'x',
                    height: 200, borderRadius: 0,
                    spacingBottom: 10, spacingLeft: 0, spacingRight: 0, spacingTop: 10,
                    renderTo: 'phaseProgressChart'
                },
                title: null,
                xAxis: {
                    type: 'datetime',
                    labels: { rotation: 60 }
                },
                yAxis: { title: null, max: 100, min: 0 },
                tooltip: { "valueSuffix": "%" },
                plotOptions: {
                    area: { color: '#762A2E', lineWidth: 4, fillOpacity: 1 },
                    series: { marker: { enabled: false } }
                },
                legend: { enabled: false },
                credits: { enabled: false }
            },
            series: [{ data: [] }],
            title: null
        };

        $scope.systemProgressChart = {
            options: {
                chart: {
                    type: "area",
                    zoomType: 'x',
                    height: 200, borderRadius: 0,
                    spacingBottom: 10, spacingLeft: 0, spacingRight: 0, spacingTop: 10,
                    renderTo: 'systemProgressChart'
                },
                title: null,
                xAxis: {
                    type: 'datetime',
                    labels: { rotation: 60 }
                },
                yAxis: { title: null, max: 100, min: 0 },
                tooltip: { "valueSuffix": "%" },
                plotOptions: {
                    area: { color: '#762A2E', lineWidth: 4, fillOpacity: 1 },
                    series: { marker: { enabled: false } }
                },
                legend: { enabled: false },
                credits: { enabled: false }
            },
            series: [{ data: [] }],
            title: null
        };

        function updatePhaseData() {
            var phaseId = $scope.state.phases[$scope.state.currentPhaseIdx].id;

            // reset data for 'loading' state
            $scope.state.loadingPhaseData = true;
            $scope.phaseProgressChart.series[0].data = [];

            // get data
            chartDataService.getPhaseProgressHistory(phaseId).then(function (response) {
                $scope.state.loadingPhaseData = false;
                $scope.phaseProgressChart.series[0].data = response.data;
                $scope.phaseProgressChart.totalManhours = getTotalManhoursLabel(response.totalManhours);
            });
        };

        function updateSystemHistData() {
            var systemId = $scope.state.systems[$scope.state.currentSystemIdx].id;

            // reset data for 'loading' state
            $scope.state.loadingSystemData = true;
            $scope.systemProgressChart.series[0].data = [];

            // get data
            chartDataService.getSystemProgressHistory(systemId).then(function (response) {
                $scope.state.loadingSystemData = false;
                $scope.systemProgressChart.series[0].data = response.data;
                $scope.systemProgressChart.totalManhours = getTotalManhoursLabel(response.totalManhours);
            });
        };

        function getTotalManhoursLabel(totalManhours) {
            return ($scope.displayType == 'percent') ? '' : totalManhours;
        }

        function updateJobPercentComplete() {
            chartDataService.getJobPercentComplete().then(function (data) {
                $timeout(function () {

                    var label = '';
                    var value = data.PercentComplete + '%';

                    if ($scope.displayType == 'percent') {
                        label = data.PercentComplete + '%';
                    } else {
                        label = data.PercentComplete + '% (' + data.CompletedManHours + ' Man-Hours Earned of ' + data.TotalManHours + ' Budgeted)';
                    }
                    
                    updateSystemHistData();
                    updatePhaseData();

                    $scope.jobPercentComplete = label;
                    $scope.jobPercentCompleteChart.series[0].data[0] = parseFloat(value);
                }, 0);
            });
        };

        function prevPhase() {
            $scope.state.currentPhaseIdx--;
            updatePhaseData();
        };

        function nextPhase() {
            $scope.state.currentPhaseIdx++;
            updatePhaseData();
        };

        function prevTask() {
            $scope.state.currentTaskIdx--;
        };

        function nextTask() {
            $scope.state.currentTaskIdx++;
        };

        function prevSystem() {
            if ($scope.state.currentSystemIdx > 0) {
                $scope.state.currentSystemIdx--;
                updateSystemHistData();
            }
        };

        function nextSystem() {
            if ($scope.state.currentSystemIdx < $scope.state.systems.length - 1) {
                $scope.state.currentSystemIdx++;
                updateSystemHistData();
            }
        };
        
        function redrawChart(chartId) {
            var idx = $(chartId).data('highchartsChart');
            var hc = Highcharts.charts[idx];
            hc.render();
        }

        function initWatches() {
            $scope.$watch('state.taskPhase', function (currentVal, prevVal, scope) {
                if (angular.isDefined(currentVal) && currentVal != prevVal) {
                    chartDataService.getTasksByPhase($scope.state.taskPhase).then(function (tasks) {
                        $scope.state.tasks = tasks;
                        $scope.state.loadingTaskData = true;
                        $scope.taskProgressChart.series[0].data = [];

                        $scope.state.currentTaskIdx = 0;
                        var taskId = $scope.state.tasks[0].id;


                        chartDataService.getTaskProgressHistory(taskId).then(function (response) {
                            $scope.state.loadingTaskData = false;
                            $scope.taskProgressChart.series[0].data = response.data;
                            $scope.taskProgressChart.totalManhours = getTotalManhoursLabel(response.totalManhours);
                        });
                    });
                }
            });

            $scope.$watch('state.currentTaskIdx', function (currentVal, prevVal, scope) {
                if (angular.isDefined(currentVal) && currentVal != prevVal) {
                    $scope.state.loadingTaskData = true;
                    $scope.taskProgressChart.series[0].data = [];

                    var taskId = $scope.state.tasks[$scope.state.currentTaskIdx].id;


                    chartDataService.getTaskProgressHistory(taskId).then(function (response) {
                        $scope.state.loadingTaskData = false;
                        $scope.taskProgressChart.series[0].data = response.data;
                        $scope.taskProgressChart.totalManhours = getTotalManhoursLabel(response.totalManhours);
                    });
                }
            });

            $scope.$on('displayType::changed', function() {
                updateJobPercentComplete();
                redrawChart('#jobPercentComplete');
            });
        }

        function init() {
            chartDataService.getJobPercentComplete().then(function (data) {
                $timeout(function () {
                    $scope.jobPercentComplete = data.PercentComplete + '%';
                    $scope.jobPercentCompleteChart.series[0].data[0] = parseFloat(data.PercentComplete);
                }, 0);
            });

            

            chartDataService.getJobProgressHistory().then(function (chartData) {
                $scope.jobProgressChart.series[0].data = chartData.data;
                $scope.jobProgressChart.totalManhours = chartData.totalManhours;
            });

            chartDataService.getPhases().then(function (phases) {
                $scope.state.phases = phases;
                $scope.state.currentPhaseIdx = 0;

                updatePhaseData();
            });

            chartDataService.getSystems().then(function (systems) {
                $scope.state.systems = systems;
                $scope.state.currentSystemIdx = 0;

                updateSystemHistData();
            });

            initWatches();

            setTimeout(function () {
                $(window).trigger('resize');
            }, 1000);
        };

        $timeout(init, 100);
    };

    function dataLabelFilter() {
        return function (point, displayType) {
            if (displayType == 'percent') {
                return (point.y).toFixed(2) + '%';
            } else {
                return (point.completedManHours) + ' / ' + (point.totalManHours);
            }
        }
    }


    angular.module('Controllers')
        .controller('ProjectStatusTabsController', projectStatusTabsController)
        .controller('ProjectStatusController', projectStatusController)
        .filter('dataLabelFilter', dataLabelFilter);
})();