﻿'use strict';

(function() {
    EnterProgressController.$inject = ['$scope', '$rootScope', '$q', '$timeout', '$modal', 'systemService', 'TagService', 'NotificationService'];
    function EnterProgressController($scope, $rootScope, $q, $timeout, $modal, SystemService, TagService, NotificationService) {

        var phaseTasks = null;
        var throttledPercentUpdate = _.throttle(percentUpdate, 4000);
        
        $scope.applyFilter = applyFilter;
        $scope.beginEnteringData = beginEnteringData;
        $scope.changePage = changePage;
        $scope.deleteTrackedItemView = deleteTrackedItemView;
        $scope.editMainFilter = editMainFilter;
        $scope.openCopyTrackedItemView = openCopyTrackedItemView;
        $scope.openEditTrackedItemView = openEditTrackedItemView;
        $scope.openNewTrackedItemView = openNewTrackedItemView;
        $scope.setTagStatus = setTagStatus;
        $scope.showTrackedItemHistory = showTrackedItemHistory;
        $scope.toggleDetail = toggleDetail;
        $scope.triggerApplyFilter = triggerApplyFilter;
        $scope.workdateInPast = workdateInPast;
        
        $scope.detailedTrackedItem = false;
        $scope.systemTree = [];

        $scope.config = {
            resultPagerSize: 5,
            resultLinesPerPage: 15
        };

        $scope.state = {
            enteringData: false,
            resultCurrentPage: 1,
            resultNumOfPages: 0,
            resultTotalCount: 0,
            resultSearchProgress: '0.00%'
        };

        $scope.progressValues = {
            date: new XDate(new Date()).toString('M/d/yyyy'),
            searchText: '',
            workCrew: null,
            phase: null,
            task: [],
            commodities: [],
            objectTypes: [],
            systems: [],
            areas: [],
            craftInfo: [],
            udp1: [],
            udp2: [],
            udp3: []
        };
        
        $scope.displayValues = {
            workCrew: '',
            phase: ''
        };
        
        init();
        function init() {
            $scope.$watch('state.resultTotalCount', watchResultTotalCount);
            $scope.$watch('[progressValues.workCrew, progressValues.phase, progressValues.date]', watchProgressValues, true);
            $('#FilterTask').on("select2-selecting", onSelect2Change);
            SystemService.getFlatSystemTree().then(onGetFlatSystemTree);

            $rootScope.$on('taskprogress::entrychanged', _.debounce(applyFilter, 500));
            function onGetFlatSystemTree(systems) {
                $scope.systemTree.length = 0;
                angular.copy(systems, $scope.systemTree);
            }
        }

        function applyFilter() {
            NotificationService.showMessage("Loading");
            
            return TagService
                .getFilteredTrackedItems($scope.progressValues, $scope.state.resultCurrentPage, $scope.config.resultLinesPerPage)
                .then(function (response) {
                    $scope.trackedItems = response.TrackedItems;
                    $scope.state.resultTotalCount = response.TotalRecords;
                    $scope.state.resultSearchProgress = response.TotalSearchProgress;
                    NotificationService.hideMessage();
            });
        };

        function beginEnteringData() {
            if (!$scope.progressValues.date || !$scope.progressValues.phase || !$scope.progressValues.workCrew) {
                NotificationService.showMessage("Please enter all filtering data to proceed.", 5000);
                return;
            } else if (new Date(new XDate(new Date($scope.progressValues.date)).toString('M/d/yy')) > new Date((new XDate(new Date())).toString('M/d/yy'))) {
                NotificationService.showMessage("Entering progress for future dates is not allowed.", 5000);
                return;
            } else if ((new XDate(new Date($scope.progressValues.date))).toString('M/d/yy') == (new XDate(new Date())).toString('M/d/yy')) {
                var proceed = confirm('You have selected to enter progress made on today\'s date. \n If you instead wanted to enter progress for a different date, \n click cancel and select a different date. \n\nProceed?');
                if (!proceed) {
                    return;
                }
            };

            NotificationService.hideMessage();

            $scope.applyFilter();

            $scope.state.enteringData = true;


        };

        function changePage(page) {
            $scope.state.resultCurrentPage = page;
            $scope.applyFilter();
        };

        function deleteTrackedItemView(item) {
            var baseUrl = window.__BaseUrl;
            var modalInstance = $modal.open({
                templateUrl: baseUrl + 'Assets/js/angular/template/deleteTrackedItem.html',
                controller: 'DeleteTrackedItemCtrl',
                resolve: {
                    item: function () {
                        return item;
                    },
                    filter: function () {
                        return $scope.progressValues;
                    }
                }
            });

            modalInstance.result.then(function () {
                // apply filter because you changed things
                $scope.applyFilter();
            });
        }

        function editMainFilter() {
            $scope.state.enteringData = false;
        };

        function openCopyTrackedItemView(item) {
            var baseUrl = window.__BaseUrl;
            var modalInstance = $modal.open({
                templateUrl: baseUrl + 'Assets/js/angular/template/copyTrackedItem.html',
                controller: 'CopyTrackedItemCtrl',
                resolve: {
                    item: function () {
                        return item;
                    },
                    phaseId: function() {
                        return $scope.progressValues.phase;
                    }
                }
            });

            modalInstance.result.then(function () {
                // apply filter because you changed things
                $scope.applyFilter();
            });
        }

        function openEditTrackedItemView(item) {
            var baseUrl = window.__BaseUrl;
            var modalInstance = $modal.open({
                templateUrl: baseUrl + 'Assets/js/angular/template/editTrackedItem.html',
                controller: 'EditTrackedItemCtrl',
                resolve: {
                    item: function() {
                        return item;
                    },
                    filter: function() {
                        return $scope.progressValues;
                    },
                    systemTree: function () {
                        return $scope.systemTree;
                    },
                    phaseTasks: function() {
                        return $scope.phaseTasks;
                    }
                }
            });

            modalInstance.result.then(function () {
                // apply filter because you changed things
                $scope.applyFilter();
            });
        }
        
        function openNewTrackedItemView() {
            var baseUrl = window.__BaseUrl;
            var modalInstance = $modal.open({
                templateUrl: baseUrl + 'Assets/js/angular/template/newTrackedItem.html',
                controller: 'NewTrackedItemCtrl',
                resolve: {
                    filter: function () {
                        return $scope.progressValues;
                    },
                    systemTree: function () {
                        return $scope.systemTree;
                    },
                    phaseTasks: function () {
                        return $scope.phaseTasks;
                    }
                }
            });

            modalInstance.result.then(function () {
                // apply filter because you changed things
                $scope.applyFilter();
            });
        }

        function showTrackedItemHistory(item) {
            var baseUrl = window.__BaseUrl;
            var modalInstance = $modal.open({
                templateUrl: baseUrl + 'Assets/js/angular/template/showTrackedItemHistory.html',
                controller: 'ProgressHistoryController',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });

            modalInstance.result.then(function () {
                // apply filter because you changed things
                $scope.applyFilter();
            });
        }

        function onSelect2Change(e) {
            $scope.$apply(function () {
                $scope.progressValues.task = e.val;
            });
        }

        function percentUpdate() {
            TagService
                .getFilteredPercentComplete($scope.progressValues)
                .then(onSuccess);

            function onSuccess(response) {
                $scope.state.resultSearchProgress = response.TotalSearchProgress;
            }
        }

        function setTagStatus(trackedItem, status, uncheckStatus) {
            var foundItem = _.find($scope.trackedItems, function (item) {
                return item.ID === trackedItem.ID;
            });

            var newStatus = (foundItem.PercentCompleteOnDate == status || (foundItem.PercentCompleteOnDate == 1 && status == 0.99)) ? uncheckStatus : status;
            var message = "Saved";

            if (newStatus < foundItem.PercentCompleteOnDate) {
                var proceed = confirm('You are decreasing the marked progress on this item. \n\nProceed?');
                if (!proceed) {
                    return;
                }
            }
            
            foundItem.saveInProgress = true;
            
            TagService
                .updateTrackedItemProgress(trackedItem.ID, newStatus, $scope.progressValues)
                .then(function () {
                    NotificationService.showLoadingMessage(message);
                    throttledPercentUpdate();
                    return applyFilter();
                }).then(function() {
                    foundItem.saveInProgress = false;
            });
        };

        function toggleDetail() {
            $scope.detailedTrackedItem = !$scope.detailedTrackedItem;
        };

        function triggerApplyFilter() {
            // Why is this magic here?? To fix setting back to 
            // page one after applying new search criteria
            $scope.state.resultCurrentPage = 1;

            $scope.applyFilter().then(function () {
                $scope.state.resultCurrentPage = 1;
            });
        };

        function watchProgressValues(currentVal, prevVal, scope) {
            if (currentVal[0]) {
                var workCrewData = $("#workCrewSelector").select2("data");
                $scope.displayValues.workCrew = workCrewData.text;
            }

            if (currentVal[1]) {
                var projectPhaseData = $("#projectPhaseSelector").select2("data");
                $scope.displayValues.phase = projectPhaseData.text;

                if (currentVal[1] != prevVal[1]) {
                    $.get(__TaskByPhaseUrl, { phaseID: $scope.progressValues.phase }).success(function (response) {
                        $scope.phaseTasks = response.Tasks;
                        $scope.progressValues.task.length = 0;
                    });
                }
            }

            if (currentVal[0] && currentVal[1]) {
                $scope.state.enteringData = true;
                triggerApplyFilter();
            }
        }

        function watchResultTotalCount() {
            var addlPage = ($scope.state.resultTotalCount % $scope.config.resultLinesPerPage == 0) ? 0 : 1;
            var numPages = $scope.state.resultTotalCount / $scope.config.resultLinesPerPage;

            $scope.state.resultNumOfPages = parseInt(numPages + addlPage);
        }

        function workdateInPast() {
            var today = new XDate(new Date().toDateString());
            var workDate = new XDate($scope.progressValues.date);

            return workDate < today;
        }
    };

    ProgressHistoryController.$inject = ['$scope', '$modalInstance', 'TagService', 'item'];
    function ProgressHistoryController($scope, $modalInstance, TagService, item) {

        $scope.item = item;

        $scope.deleteProgressEntry = deleteProgressEntry;
        $scope.getArrow = getArrow;
        $scope.ok = ok;
        init();

        function init() {
            getHistory();
        }

        function getHistory() {
            TagService.getTaskProgress(item).then(function (history) {
                $scope.history = history;
            });
        }

        function deleteProgressEntry(entry) {
            if (confirm('Are you sure')) {
                TagService.deleteProgressEntry(entry.ID, entry.TaskProgressID).then(getHistory);
                $scope.$emit('taskprogress::entrychanged');
            }
        }

        function getArrow(entry, index) {
            if (!$scope.history || !$scope.history.Entries.length || !$scope.history.Entries[index + 1]) {
                return '';
            }

            switch (true) {
                case entry.NewPercentComplete > $scope.history.Entries[index + 1].NewPercentComplete:
                    return 'arrow-up';
                case entry.NewPercentComplete < $scope.history.Entries[index +1].NewPercentComplete:
                    return 'arrow-down';
                default:
                    return '';
            }
        }

        function ok() {
            $modalInstance.dismiss('cancel');
        };
    }
    
    function EditTrackedItemCtrl($scope, $modalInstance, TagService, NotificationService, item, filter, systemTree, phaseTasks) {

        $scope.filter = filter;
        $scope.systemTree = systemTree;
        $scope.phaseTasks = phaseTasks;

        $scope.ok = ok;
        $scope.cancel = cancel;

        TagService.getTrackedItemDetails(item.TrackedItemID, item.TaskId).then(onGetTrackedItemDetails);

        function ok() {
            TagService
                .updateTrackedItem($scope.viewmodel.item)
                .then(function (response) {
                    if (response.success) {
                        $modalInstance.close();
                    } else {
                        $("#errors").html(response.errors);
                        $("#errors").show();
                    }
                });
        };
        
        function cancel() {
            $modalInstance.dismiss('cancel');
        };

        function onGetTrackedItemDetails(viewmodelData) {
            $scope.viewmodel = viewmodelData;
        }
    }

    function DeleteTrackedItemCtrl($scope, $modalInstance, TagService, NotificationService, item, filter) {

        $scope.filter = filter;

        TagService.getTrackedItemDetails(item.TrackedItemID, item.TaskId).then(function (viewmodelData) {
            $scope.viewmodel = viewmodelData;
        });

        $scope['delete'] = function () {
            TagService
                .deleteTrackedItem($scope.viewmodel.item.ID)
                .then(function (response) {
                    if (response.success) {
                        $modalInstance.close();
                    } else {
                        $("#errors").html(response.errors);
                        $("#errors").show();
                    }
                });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    function NewTrackedItemCtrl($scope, $modalInstance, TagService, NotificationService, filter, systemTree, phaseTasks) {

        $scope.filter = filter;
        $scope.ok = ok;
        $scope.cancel = cancel;
        $scope.systemTree = $scope.systemTree = systemTree;
        $scope.phaseTasks = phaseTasks;

        TagService
            .getNewItemOptions()
            .then(onGetNewItemOptions);

        function ok() {
            TagService
                .addTrackedItem($scope.viewmodel.item)
                .then(function (response) {
                    if (response.success) {
                        $modalInstance.close();
                    } else {
                        $("#errors").html(response.errors);
                        $("#errors").show();
                    }
                });
        };

        function cancel() {
            $modalInstance.dismiss('cancel');
        };

        function onGetNewItemOptions(viewmodelData) {
            $scope.viewmodel = viewmodelData;
        }
    }

    function CopyTrackedItemCtrl($scope, $modalInstance, TagService, item, phaseId) {

        $scope.ok = ok;
        $scope.cancel = cancel;
        $scope.phases = window.__phases;
        $scope.phaseId = phaseId;
        $scope.onPhaseChange = onPhaseChange;

        onPhaseChange();

        function onPhaseChange($item, $model) {
            $model && ($scope.phaseId = $model);

            TagService
                .getCopyTrackedItem(item.TrackedItemID, $scope.phaseId)
                .then(onGetNewItemOptions);
        }

        function ok() {
            TagService
                .copyTrackedItem(item.TrackedItemID, $scope.viewmodel.newTaskID)
                .then(function (response) {
                    if (response.success) {
                        $modalInstance.close();
                    } else {
                        $("#errors").html(response.errors);
                        $("#errors").show();
                    }
                });
        };

        function cancel() {
            $modalInstance.dismiss('cancel');
        };

        function onGetNewItemOptions(viewmodelData) {
            $scope.viewmodel = viewmodelData;
        }
    }

    angular.module('Controllers')
        .controller('EnterProgressController', EnterProgressController)
        .controller('EditTrackedItemCtrl', EditTrackedItemCtrl)
        .controller('DeleteTrackedItemCtrl', DeleteTrackedItemCtrl)
        .controller('NewTrackedItemCtrl', NewTrackedItemCtrl)
        .controller('CopyTrackedItemCtrl', CopyTrackedItemCtrl)
        .controller('ProgressHistoryController', ProgressHistoryController);
})();