﻿angular.module('Filters').filter('completionstate', function() {
        return function(input) {
            var css = 'not-started';

            if (input >= 1) {
                css = 'complete';
            } else if (input >= 0.5) {
                css = 'almost-complete';
            } else if (input >= 0) {
                css = 'work-started';
            }

            return css;
        };
    })
    .filter('trust', function($sce) {
        return function(val) {
            return $sce.trustAsHtml(val);
        };
    })
    .filter('makeHighlightVisible', function() {
        return function(val) {
            var selector = '<span class="ui-select-highlight">';
            var highlightIndex = val.indexOf(selector);
            var maxDistance = 25;

            if (highlightIndex == -1 || highlightIndex < maxDistance || val.length < 30)
                return val;

            var formatted = val.substring(0, 10) + '...' + val.substring(highlightIndex - 5);
            return formatted;
        };
    })
    .filter('groupByTask', function groupByTask() {
        var cache = {};
        return function _groupByTask(trackedItemCollection, systemID) {
            if (!cache[systemID]) {
                cache[systemID] = calcGroup(trackedItemCollection);
            }
            return cache[systemID];
        };

        function calcGroup(trackedItemCollection) {
            return _.chain(trackedItemCollection).groupBy(function(t) {
                return t.Name.split(' ::: ')[1];
            }).map(function(val, key) {
                return { name: key, items: val }
            }).value();
        }
    });