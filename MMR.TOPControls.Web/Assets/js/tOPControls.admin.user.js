(function () {
    var user = TopControls.admin.user = {};

    user.init = function () {
        user.initDataTable();
        user.registerModalEvents();
    };

    user.initDataTable = function () {
        user.userTable = $("#user-table").dataTable({
            aaSorting: [[1, "asc"]],
            sAjaxSource: TopControls.admin.user.listUrl,
            fnRowCallback: function (nRow, aData) {
                $('td:eq(0)', nRow).html(
                    TopControls.admin.user.editLink.replace("idPlaceholder", aData[0]) + ' ' +
                    TopControls.admin.user.deleteLink.replace("idPlaceholder", aData[0])
                );
                $('td:eq(2)', nRow).html(aData[2] ? 'Yes' : 'No');
            },
            aoColumnDefs:
            [
                { bSortable: false, aTargets: [0] }
            ]
        });
    };

    user.registerModalEvents = function () {
        $("#modal-container").on('submit', "form", function (e) {
            e.preventDefault();
            $.post($(this).attr('action'), $(this).serialize(), function (result) {
                if (result.success) {
                    user.userTable.fnDraw();
                    $("#modal-container").modal('hide');
                } else {
                    $("#modal-container").html(result);
                    TopControls.common.initModalValidation();
                }
            });
        });
    };
})();
