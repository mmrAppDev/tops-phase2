(function () {

    var topUser = TopControls.topUser = {};

    topUser.showResponse = function (response) {
        var $nPane = $('div.top-notification');

        if ($nPane.length > 0) {


            var responseText = '';
            var descriptionText = '';
            if (typeof response === 'String') {
                responseText = response;
            } else {
                if (response.success) {
                    responseText = "User Job Action Succeeded";
                } else {
                    responseText = "User Job Action Failed";
                    descriptionText = response.message;
                };
            };

            var tempPane = $(document.createElement("div"));
            tempPane.append($("<p><strong>" + responseText + "</strong></p>"));
            tempPane.append(document.createTextNode(descriptionText));

            $nPane.append(tempPane);
            $nPane.show();

            setTimeout(function () {
                $(tempPane).remove();
                if ($nPane.text() == '') {
                    $nPane.hide();
                }
            }, 3000);
            
        }
        





    };

    topUser.init = function () {
		topUser.registerModalEvents();
    };

	topUser.registerModalEvents = function() {
		$("#modal-container").on('submit', "[data-form-action='create'], [data-form-action='edit'], [data-form-action='delete']", function (e) {
			e.preventDefault();
			$.post($(this).attr('action'), $(this).serialize(), function (result) {
				if (result.success) {
				    topUser.topUserTable.fnDraw();
					$("#modal-container").modal('hide');
				} else {
					$("#modal-container").html(result);
				}
			});
		});


		topUser.initDataTable();

	};

	topUser.bindEvents = function () {
	    $(document).on('click', '[data-action="add-job"], [data-action="remove-job"]', function (e) {
	        e.preventDefault();

	        var container = $('#jobListSection');

	        var url = e.target.href;

	        var successUrl = topUser.urlMap.jobRefreshUrl;

	        $.post(url, function (response) {
	            if (response.success) {
	                container.load(successUrl);


	            } else {
	                console.log(url, response);
	            }
	            topUser.showResponse(response);
	        });

	    });

	};


    

	topUser.initUserJobEdit = function (urlMap) {
	    this.urlMap = urlMap;
	    this.bindEvents();
	};

    topUser.initDataTable = function() {
        topUser.topUserTable = $("#topuser-table").dataTable({
            aaSorting: [[0, "asc"]],
            bServerSide: true,
            bDestroy: true,
            sAjaxSource: TopControls.topUser.listUrl,
            fnRowCallback: function (nRow, aData) {
                $('td:eq(4)', nRow).html(TopControls.topUser.editLink.replace("idPlaceholder", aData[5]) + ' ' + TopControls.topUser.jobsLink.replace("idPlaceholder", aData[5]) + ' ' + TopControls.topUser.deleteLink.replace("idPlaceholder", aData[5]));
            },
            aoColumns:
            [
                { sType: "string", sClass: "dtAlignLeft" },
                { sType: "string", sClass: "dtAlignLeft" },
                { sType: "string", sClass: "dtAlignLeft" },
                { sType: "string", sClass: "dtAlignLeft" },
                { "bSortable": false }
            ]
        });
    };

})();

