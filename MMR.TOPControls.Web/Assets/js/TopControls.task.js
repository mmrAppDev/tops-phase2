(function () {

    var task = TopControls.task = {};

    task.init = function () {
		task.registerModalEvents();
    };

	task.registerModalEvents = function() {
		$("#modal-container").on('submit', "[data-form-action='create'], [data-form-action='edit'], [data-form-action='delete']", function (e) {
			e.preventDefault();
			$.post($(this).attr('action'), $(this).serialize(), function (result) {
				if (result.success) {
				    task.taskTable.fnDraw();
					$("#modal-container").modal('hide');
				} else {
					$("#modal-container").html(result);
				}
			});
		});

		task.initDataTable();

	};

    task.initDataTable = function() {
            task.taskTable = $("#task-table").dataTable({
                aaSorting: [[1, "asc"]],
                bServerSide: true,
                bDestroy: true,
                sAjaxSource: TopControls.task.listUrl,
                fnRowCallback: function (nRow, aData) {
                    $('td:eq(3)', nRow).html(TopControls.task.editLink.replace("idPlaceholder", aData[7]) + ' ' + TopControls.task.deleteLink.replace("idPlaceholder", aData[7]));
                },
                aoColumns:
                [
					{ sType: "string", sClass: "dtAlignLeft" },
		            { sType: "string", sClass: "dtAlignLeft" },
		            { bVisible: false },
		            { sType: "string", sClass: "dtAlignLeft" },
                    { "bSortable": false }
				]
            });
		};
})();

