(function () {

    var projectPhase = TopControls.projectPhase = {};

    projectPhase.init = function () {
		projectPhase.registerModalEvents();
    };

	projectPhase.registerModalEvents = function() {
		$("#modal-container").on('submit', "[data-form-action='create'], [data-form-action='edit'], [data-form-action='delete']", function (e) {
			e.preventDefault();
			$.post($(this).attr('action'), $(this).serialize(), function (result) {
				if (result.success) {
				    projectPhase.projectPhaseTable.fnDraw();
					$("#modal-container").modal('hide');
				} else {
					$("#modal-container").html(result);
				}
			});
		});

		projectPhase.initDataTable();

	};

    projectPhase.initDataTable = function() {
            projectPhase.projectPhaseTable = $("#projectphase-table").dataTable({
                aaSorting: [[1, "asc"]],
                bServerSide: true,
                bDestroy: true,
                sAjaxSource: TopControls.projectPhase.listUrl,
                fnRowCallback: function (nRow, aData) {
                    $('td:eq(2)', nRow).html(TopControls.projectPhase.editLink.replace("idPlaceholder", aData[3]) + ' ' + TopControls.projectPhase.deleteLink.replace("idPlaceholder", aData[3]));
                },
                aoColumns:
                [
				      { sType: "string", sClass: "dtAlignLeft" }
		            , { sType: "string", sClass: "dtAlignLeft" }
                    , { "bSortable": false }
				]
            });
		};
})();

