﻿(function () {

    var jobSettings = TopControls.jobSettings = {};




    var bindEvents = function () {
        $(document).on('click', '[data-action="add-user"], [data-action="remove-user"]', function (e) {
            e.preventDefault();
            
            var container = $('#userListSection');
            
            var url = e.target.href;
            var successUrl = jobSettings.urlMap.userRefreshUrl;

            $.post(url, function (response) {
                if (response.success) {
                    container.load(successUrl);
                } else {
                    console.log(url, response);
                }
                jobSettings.showResponse(response);
            });
        });
    };
    
    jobSettings.init = function (urlMap) {
        this.urlMap = urlMap;
        bindEvents();
    };

    jobSettings.showResponse = function (response) {
        var $nPane = $('div.top-notification');

        if ($nPane.length > 0) {


            var responseText = '';
            var descriptionText = '';
            if (typeof response === 'String') {
                responseText = response;
            } else {
                if (response.success) {
                    responseText = "User Job Action Succeeded";
                } else {
                    responseText = "User Job Action Failed";
                    descriptionText = response.message;
                };
            };

            var tempPane = $(document.createElement("div"));
            tempPane.append($("<p><strong>" + responseText + "</strong></p>"));
            tempPane.append(document.createTextNode(descriptionText));

            $nPane.append(tempPane);
            $nPane.show();

            setTimeout(function () {
                $(tempPane).remove();
                if ($nPane.text() == '') {
                    $nPane.hide();
                }
            }, 3000);

        }
    };
})();

