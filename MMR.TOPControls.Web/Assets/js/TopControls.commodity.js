(function () {

    var commodity = TopControls.commodity = {};

    commodity.init = function () {
		commodity.registerModalEvents();
    };

	commodity.registerModalEvents = function() {
		$("#modal-container").on('submit', "[data-form-action='create'], [data-form-action='edit'], [data-form-action='delete']", function (e) {
			e.preventDefault();
			$.post($(this).attr('action'), $(this).serialize(), function (result) {
				if (result.success) {
				    commodity.commodityTable.fnDraw();
					$("#modal-container").modal('hide');
				} else {
					$("#modal-container").html(result);
				}
			});
		});

		commodity.initDataTable();

	};

    commodity.initDataTable = function() {
            commodity.commodityTable = $("#commodity-table").dataTable({
                aaSorting: [[0, "asc"]],
                bServerSide: true,
                bDestroy: true,
                sAjaxSource: TopControls.commodity.listUrl,
                fnRowCallback: function (nRow, aData) {
                    $('td:eq(3)', nRow).html(TopControls.commodity.editLink.replace("idPlaceholder", aData[5]) + ' ' + TopControls.commodity.deleteLink.replace("idPlaceholder", aData[5]));
                },
                aoColumns:
                [
					{ sType: "string", sClass: "dtAlignLeft" }
		        ,   { sType: "string", sClass: "dtAlignLeft" }
		        ,	{ sType: "string", sClass: "dtAlignLeft" }
                ,   { "bSortable": false }
				]
            });
		};
})();

