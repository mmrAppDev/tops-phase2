(function () {

    var craftInfo = TopControls.craftInfo = {};

    craftInfo.init = function () {
        craftInfo.registerModalEvents();
    };

    craftInfo.registerModalEvents = function () {
        $("#modal-container").on('submit', "[data-form-action='create'], [data-form-action='edit'], [data-form-action='delete']", function (e) {
            e.preventDefault();
            $.post($(this).attr('action'), $(this).serialize(), function (result) {
                if (result.success) {
                    craftInfo.craftInfoTable.fnDraw();
                    $("#modal-container").modal('hide');
                } else {
                    $("#modal-container").html(result);
                }
            });
        });

        craftInfo.initDataTable();

    };

    craftInfo.initDataTable = function () {
        craftInfo.craftInfoTable = $("#craftinfo-table").dataTable({
            aaSorting: [[0, "asc"]],
            bServerSide: true,
            bDestroy: true,
            sAjaxSource: TopControls.craftInfo.listUrl,
            fnRowCallback: function (nRow, aData) {
                $('td:eq(2)', nRow).html(TopControls.craftInfo.editLink.replace("idPlaceholder", aData[3]) + ' ' + TopControls.craftInfo.deleteLink.replace("idPlaceholder", aData[3]));
            },
            aoColumns:
            [
                { sType: "string", sClass: "dtAlignLeft" }
                , { sType: "string", sClass: "dtAlignLeft" }
                , { "bSortable": false }
            ]
        });
    };
})();

