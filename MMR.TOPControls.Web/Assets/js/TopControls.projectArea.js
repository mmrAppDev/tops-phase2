(function () {

    var projectArea = TopControls.projectArea = {};

    projectArea.init = function () {
		projectArea.registerModalEvents();
    };

	projectArea.registerModalEvents = function() {
		$("#modal-container").on('submit', "[data-form-action='create'], [data-form-action='edit'], [data-form-action='delete']", function (e) {
			e.preventDefault();
			$.post($(this).attr('action'), $(this).serialize(), function (result) {
				if (result.success) {
				    projectArea.projectAreaTable.fnDraw();
					$("#modal-container").modal('hide');
				} else {
					$("#modal-container").html(result);
				}
			});
		});

		projectArea.initDataTable();

	};

    projectArea.initDataTable = function() {
            projectArea.projectAreaTable = $("#projectarea-table").dataTable({
                aaSorting: [[0, "asc"]],
                bServerSide: true,
                bDestroy: true,
                sAjaxSource: TopControls.projectArea.listUrl,
                fnRowCallback: function (nRow, aData) {
                    $('td:eq(2)', nRow).html(TopControls.projectArea.editLink.replace("idPlaceholder", aData[3]) + ' ' + TopControls.projectArea.deleteLink.replace("idPlaceholder", aData[3]));
                },
                aoColumns:
                [
					{ sType: "string", sClass: "dtAlignLeft" }
		            , { sType: "string", sClass: "dtAlignLeft" }
                    , { "bSortable": false }
				]
            });
		};
})();

