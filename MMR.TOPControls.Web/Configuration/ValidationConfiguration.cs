using System;
using System.Web.Mvc;
using FluentValidation;
using FluentValidation.Mvc;

namespace MMR.TOPControls.Web.Configuration
{
    public static class ValidationConfiguration
    {
        public static void Configure()
        {
            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;

            ValidatorOptions.ResourceProviderType = typeof(Envoc.Core.Shared.Model.ValidationMessageTemplates);

            FluentValidationModelValidatorProvider.Configure(provider =>
            {
                provider.ValidatorFactory = new DependencyResolverValidatorFactory();
            });
        }

        public class DependencyResolverValidatorFactory : ValidatorFactoryBase
        {
            public override IValidator CreateInstance(Type validatorType)
            {
                return (IValidator)DependencyResolver.Current.GetService(validatorType);
            }
        }
    }
}