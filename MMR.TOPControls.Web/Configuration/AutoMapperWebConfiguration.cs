using AutoMapper;
using Lib.TOPControls;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Configuration
{
    public static class AutoMapperWebConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(configuration =>
            {
                configuration.CreateMap<CraftInfo, CraftInfoViewModel>().ReverseMap();
                configuration.CreateMap<Commodity, CommodityViewModel>().ReverseMap();
                configuration.CreateMap<ObjectType, ObjectTypeViewModel>().ReverseMap();

                configuration.CreateMap<ProjectSystem, ProjectSystemViewModel>()
                    .ForMember(src => src.Children, opt => opt.Ignore());

                configuration.CreateMap<ProjectSystemViewModel,ProjectSystem>()
                    .ForMember(src => src.Children, opt => opt.Ignore());

                configuration.CreateMap<ProjectArea, ProjectAreaViewModel>().ReverseMap();
                configuration.CreateMap<JobInfo, JobInfoViewModel>().ReverseMap();
                configuration.CreateMap<ProjectPhase, ProjectPhaseViewModel>().ReverseMap();
                configuration.CreateMap<WorkCrew, WorkCrewViewModel>().ReverseMap();
                configuration.CreateMap<Task, TaskViewModel>().ReverseMap();
                configuration.CreateMap<TrackedItem, TrackedItemViewModel>().ReverseMap();
                configuration.CreateMap<TOPUser, TopUserViewModel>().ReverseMap();
            });
        }
    }
}