using System;
using System.Collections.Generic;
using System.Web.Mvc;
using MMR.TOPControls.Common.Extensions;

namespace MMR.TOPControls.Web.Configuration
{
    public class ModelMetadataConfigurationProvider : DataAnnotationsModelMetadataProvider
    {
        protected override ModelMetadata CreateMetadata(
            IEnumerable<Attribute> attributes, Type containerType,
            Func<object> modelAccessor, Type modelType, string propertyName)
        {
            var metadata = base.CreateMetadata(attributes,
                                                containerType,
                                                modelAccessor,
                                                modelType,
                                                propertyName);

            if (metadata.DisplayName == null)
            {
                metadata.DisplayName = GetDisplayName(metadata.PropertyName);
            }

            return metadata;
        }

        private string GetDisplayName(string propertyName)
        {
            if (propertyName == null)
            {
                return null;
            }
            if (propertyName.EndsWith("Id"))
            {
                propertyName = propertyName.Substring(0, propertyName.Length - 2);
            }
            return propertyName.ToSeparatedWords();
        }
    }
}