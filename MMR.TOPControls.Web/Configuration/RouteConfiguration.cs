using System.Web.Mvc;
using System.Web.Routing;

namespace MMR.TOPControls.Web.Configuration
{
    public static class RouteConfiguration
    {
        public static void Configure()
        {
            RouteTable.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            RouteTable.Routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new[] { "MMR.TOPControls.Web.Controllers" });
        }
    }
}