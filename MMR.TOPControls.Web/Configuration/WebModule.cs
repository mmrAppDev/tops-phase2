using System;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using MMR.TOPControls.Web.Controllers;
using MMR.TOPControls.Web.Security;
using Envoc.Core.Shared.Security.Validators;
using FluentValidation;
using Module = Autofac.Module;

namespace MMR.TOPControls.Web.Configuration
{
    public class WebModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => HttpContext.Current.User.Identity).As<IIdentity>();

            builder.RegisterControllers(Assembly.GetExecutingAssembly()).OnActivated(SetUser());;

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(IValidator<>))
                .AsImplementedInterfaces();

            builder.RegisterType(typeof(UserValidator<>));

            builder.RegisterType<FormsAuthenticationService>()
                .As<IFormsAuthenticationService>();
        }

        private static Action<IActivatedEventArgs<object>> SetUser()
        {
            return x =>
            {
                var secureController = x.Instance as BaseController;
                if (secureController != null)
                {
                    var userSessionService = DependencyResolver.Current.GetService<UserSessionService>();
                    secureController.CurrentUser = userSessionService.CurrentUser;
                }
            };
        }
    }
}