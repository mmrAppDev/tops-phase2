using System.Web.Optimization;

namespace MMR.TOPControls.Web.Configuration
{
    public class BundleConfiguration
    {
        public static void Configure()
        {
            BundleTable.Bundles.IgnoreList.Clear();

            var jsShivBundle = new ScriptBundle("~/Assets/js/compatibility");
            jsShivBundle.Include("~/Assets/js/libs/json2.js");
            jsShivBundle.Include("~/Assets/js/libs/respond.min.js");
            jsShivBundle.Include("~/Assets/js/libs/modernizr-2.6.2.js");
            BundleTable.Bundles.Add(jsShivBundle);

            var jsBundle = new ScriptBundle("~/Assets/js/bundle");
            jsBundle.Include("~/Assets/js/libs/jquery-1.8.2.min.js",
                             "~/Assets/js/libs/jquery-ui-1.9.0.min.js",
                             "~/Assets/js/libs/underscore.js",
                             "~/Assets/js/libs/bootstrap.min.js",
                             "~/Assets/js/libs/bootstrap-modalmanager.js",
                             "~/Assets/js/libs/bootstrap-datepicker.js",
                             "~/Assets/js/libs/bootbox.js",
                             "~/Assets/js/libs/jquery.validate.js",
                             "~/Assets/js/libs/jquery.validate.unobtrusive.min.js",
                             "~/Assets/js/libs/jquery.dataTables.min.js",
                             "~/Assets/js/libs/jquery.dataTables.boostrap.js",
                             "~/Assets/js/libs/select2.js",
                             "~/Assets/js/libs/retina-0.0.2.min.js",
                             "~/Assets/js/libs/enquire.min.js",
                             "~/Assets/js/libs/plugins.js",
                             "~/Assets/js/libs/xdate.js",
                             "~/Assets/js/TopControls.common.js");
            jsBundle.IncludeDirectory("~/Assets/js", "*.js", false);
            BundleTable.Bundles.Add(jsBundle);

            var angularBundle = new ScriptBundle("~/Assets/js/angular-libs")
                .Include(
                    "~/Assets/js/libs/angular/angular.js",
                    "~/Assets/js/libs/angular/angular-sanitize.min.js",
                    "~/Assets/js/angular/directives/select.min.js",
                    "~/Assets/js/angular/directives/angular-filter.min.js",
                    "~/Assets/js/angular/directives/angular-hovercard.js");

            var angularCharts = new ScriptBundle("~/Assets/js/angular-highcharts")
                .Include("~/Assets/js/angular/highcharts/highcharts.js")
                .Include("~/Assets/js/angular/highcharts/highcharts-ng.js");

            angularBundle.Transforms.Clear();
            angularCharts.Transforms.Clear();

            BundleTable.Bundles.Add(angularBundle);
            BundleTable.Bundles.Add(angularCharts);

            BundleTable.Bundles.Add(new ScriptBundle("~/Assets/js/ng-controllers")
                .Include("~/Assets/js/angular/controllers/module.js")
                .IncludeDirectory("~/Assets/js/angular/controllers/", "*.js"));
            
            BundleTable.Bundles.Add(new ScriptBundle("~/Assets/js/ng-services")
                .Include("~/Assets/js/angular/services/module.js")
                .IncludeDirectory("~/Assets/js/angular/services/", "*.js"));

            BundleTable.Bundles.Add(new ScriptBundle("~/Assets/js/ng-filters")
                .Include("~/Assets/js/angular/filters/module.js")
                .IncludeDirectory("~/Assets/js/angular/filters/", "*.js"));

            BundleTable.Bundles.Add(new ScriptBundle("~/Assets/js/ng-directives")
                .Include("~/Assets/js/angular/directives/module.js")
                .IncludeDirectory("~/Assets/js/angular/directives/", "*.js"));

            var cssBundle = new StyleBundle("~/Assets/css/styles");
            cssBundle.Include("~/Assets/css/library.css",
                              "~/Assets/css/bootstrap-modal.css",
                              "~/Assets/css/font-awesome.css",
                              "~/Assets/css/responsive.css",
                              "~/Assets/css/theme.css",
                              "~/Assets/css/select2.css",
                              "~/Assets/css/select.css",
                              "~/Assets/css/selectize.default.css",
                              "~/Assets/css/datepicker.css",
                              "~/Assets/css/styles.css",
                              "~/Assets/css/shame.css",
                              "~/Assets/css/custom.css");
            BundleTable.Bundles.Add(cssBundle);
        }
    }
}