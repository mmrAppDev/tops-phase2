using System.Web.Mvc;
using MMR.TOPControls.Web.Configuration;
using MMR.TOPControls.Web.Helpers;

namespace MMR.TOPControls.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            BundleConfiguration.Configure();
            AreaRegistration.RegisterAllAreas();
            RouteConfiguration.Configure();
            GlobalFilterConfiguration.Configure();
            ValidationConfiguration.Configure();
            AutofacConfiguration.Configure();
            AutoMapperWebConfiguration.Configure();
            ModelMetadataProviders.Current = new ModelMetadataConfigurationProvider();
            //DatabaseUpgrader.PerformUpgrade();

            ModelBinders.Binders.Add(typeof(long), new LongModelBinder());
            ModelBinders.Binders.Add(typeof(long?), new LongModelBinder());
        }
    }
}