﻿using System.Web.Mvc;

namespace MMR.TOPControls.Web.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { action = "List", id = UrlParameter.Optional },
                new[] { "MMR.TOPControls.Web.Areas.Admin.Controllers" }
            );
        }
    }
}