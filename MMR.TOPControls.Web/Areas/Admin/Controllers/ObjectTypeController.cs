using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;
using MMR.TOPControls.Web.Helpers.DataTables;
using Envoc.Core.Shared.Extensions;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.Controllers
{
    public class ObjectTypeController : AdminBaseController
    {
		private readonly TOPControlLibrary service;

        public ObjectTypeController()
        {
            service = new TOPControlLibrary(new TOPControlSQLDAL());
        }

        public ActionResult List()
        {
            return View(GetObjectTypeViewModels());
        }

        public ActionResult ListItems(DataTableRequest request)
        {
            var parser = new DataTableParser<ObjectTypeViewModel>(GetObjectTypeViewModels());
            var formattedList = parser.Parse(request);
            return Json(formattedList);
        }

        public ActionResult Create()
        {
            return PartialView(new ObjectTypeViewModel { JobID = CurrentJob.ID });
        }

        [HttpPost]
        public ActionResult Create(ObjectTypeViewModel viewModel)
        {
            var model = ConvertViewModelToModel(viewModel);

            if (PerformAction(() => service.ObjectTypeActions.Add(model, CurrentUser.ID)))
            {
                return Json(new {success = true});
            }
            ;

            return PartialView(viewModel);
        }

        public ActionResult Edit(int id)
        {
            var model = service.ObjectTypeActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            return PartialView((ConvertModelToViewModel(model)));
        }

        [HttpPost]
        public ActionResult Edit(ObjectTypeViewModel viewModel)
        {
            var model = service.ObjectTypeActions.Find(viewModel.ID);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            Mapper.Map(viewModel, model);

            if (PerformAction(() => service.ObjectTypeActions.Update(model, CurrentUser.ID)))
            {
                return Json(new {success = true});
            }
            ;

            return PartialView(viewModel);
        }

        public ActionResult Delete(int id)
        {
            var model = service.ObjectTypeActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);

            return PartialView(viewModel);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = service.ObjectTypeActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);

            if (viewModel.CanDelete
                && PerformAction(() => service.ObjectTypeActions.Delete(id, CurrentUser.ID)))
            {
                return Json(new { success = true });
            }
            return PartialView(viewModel);
        }

        private IQueryable<ObjectTypeViewModel> GetObjectTypeViewModels()
        {
            var ObjectTypeCollection = service.ObjectTypeActions.AllForJob(CurrentJob.ID).AsQueryable().Project().To<ObjectTypeViewModel>();
            return ObjectTypeCollection.AsQueryable();
        }

        private ObjectType ConvertViewModelToModel(ObjectTypeViewModel viewModel)
        {
            return Mapper.Map<ObjectType>(viewModel);
        }

        private ObjectTypeViewModel ConvertModelToViewModel(ObjectType model)
        {
            return Mapper.Map<ObjectTypeViewModel>(model);
        }

        private DeletionViewModel<ObjectTypeViewModel> BuildDeletionViewModel(ObjectType model)
        {
            return new DeletionViewModel<ObjectTypeViewModel>(
                ConvertModelToViewModel(model),
                service.ObjectTypeActions.ValidateDelete(model.ID));
        }
	}
}