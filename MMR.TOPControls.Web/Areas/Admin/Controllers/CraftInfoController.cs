using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Envoc.Core.Shared.Extensions;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;
using MMR.TOPControls.Web.Helpers.DataTables;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.Controllers
{
    public class CraftInfoController : AdminBaseController
    {
		private readonly TOPControlLibrary service;

		public CraftInfoController()
		{
		    service = new TOPControlLibrary(new TOPControlSQLDAL());
		}

        public ActionResult List()
        {
            return View(GetCraftInfoViewModels());
        }

        public ActionResult ListItems(DataTableRequest request)
        {
            var parser = new DataTableParser<CraftInfoViewModel>(GetCraftInfoViewModels());
            var formattedList = parser.Parse(request);
            return Json(formattedList);
        }

        public ActionResult Create()
        {
            return PartialView(new CraftInfoViewModel { JobID = CurrentJob.ID});
        }

        [HttpPost]
        public ActionResult Create(CraftInfoViewModel viewModel)
        {
			var model = ConvertViewModelToModel(viewModel);

            if (PerformAction(() => service.CraftActions.Add(model, CurrentUser.ID)))
            {
                return Json(new { success = true });
            };

            return PartialView(viewModel);
        }

        public ActionResult Edit(int id)
        {
            var model = service.CraftActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            return PartialView((ConvertModelToViewModel(model)));
        }

        [HttpPost]
        public ActionResult Edit(CraftInfoViewModel viewModel)
        {
            var model = service.CraftActions.Find(viewModel.ID);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            Mapper.Map(viewModel, model);

            if (PerformAction(() => service.CraftActions.Update(model, CurrentUser.ID)))
            {
                return Json(new { success = true });
            };

            return PartialView(viewModel);
        }

        public ActionResult Delete(int id)
        {
            var model = service.CraftActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);

            return PartialView(viewModel);
        }


        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = service.CraftActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);

            if (viewModel.CanDelete && PerformAction(() => service.CraftActions.Delete(id, CurrentUser.ID)))
            {
                return Json(new { success = true });    
            }
            return PartialView(viewModel);            
        }

        private IQueryable<CraftInfoViewModel> GetCraftInfoViewModels()
        {
            var craftInfoCollection = service.CraftActions.Search(CurrentJob.ID, "").AsQueryable().Project().To<CraftInfoViewModel>();
            return craftInfoCollection.AsQueryable();
        }

	    private CraftInfo ConvertViewModelToModel(CraftInfoViewModel viewModel)
        {
            return Mapper.Map<CraftInfo>(viewModel);
        }

		private CraftInfoViewModel ConvertModelToViewModel(CraftInfo model)
        {
            return Mapper.Map<CraftInfoViewModel>(model);
        }

        private DeletionViewModel<CraftInfoViewModel> BuildDeletionViewModel(CraftInfo model)
        {
            var viewModel = ConvertModelToViewModel(model);
            var result = new DeletionViewModel<CraftInfoViewModel>(
                viewModel, 
                service.CraftActions.ValidateDelete(model.ID));
            return result;
        }
	}
}