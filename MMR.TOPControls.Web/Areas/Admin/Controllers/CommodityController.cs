using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;
using MMR.TOPControls.Web.Helpers.DataTables;
using Envoc.Core.Shared.Extensions;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.Controllers
{
    public class CommodityController : AdminBaseController
    {
        private readonly TOPControlLibrary service;

        public CommodityController()
        {
            service = new TOPControlLibrary(new TOPControlSQLDAL());
        }

        public ActionResult List()
        {
            return View(GetCommodityViewModels());
        }

        public ActionResult ListItems(DataTableRequest request)
        {
            var parser = new DataTableParser<CommodityViewModel>(GetCommodityViewModels());
            var formattedList = parser.Parse(request);
            return Json(formattedList);
        }

        public ActionResult Create()
        {
            return PartialView(new CommodityViewModel { JobID = CurrentJob.ID });
        }

        [HttpPost]
        public ActionResult Create(CommodityViewModel viewModel)
        {
            var model = ConvertViewModelToModel(viewModel);

            if (PerformAction(() => service.CommodityActions.Add(model, CurrentUser.ID)))
            {
                return Json(new {success = true});
            }
            ;

            return PartialView(viewModel);
        }

        public ActionResult Edit(int id)
        {
            var model = service.CommodityActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            return PartialView((ConvertModelToViewModel(model)));
        }

        [HttpPost]
        public ActionResult Edit(CommodityViewModel viewModel)
        {
            var model = service.CommodityActions.Find(viewModel.ID);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            Mapper.Map(viewModel, model);

            if (PerformAction(() => service.CommodityActions.Update(model, CurrentUser.ID)))
            {
                return Json(new {success = true});
            }
            ;

            return PartialView(viewModel);
        }


        public ActionResult Delete(int id)
        {
            var model = service.CommodityActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);

            return PartialView(viewModel);
        }

        private DeletionViewModel<CommodityViewModel> BuildDeletionViewModel(Commodity model)
        {
            var viewModel = ConvertModelToViewModel(model);
            return new DeletionViewModel<CommodityViewModel>(viewModel,
                service.CommodityActions.ValidateDelete(model.ID));
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = service.CommodityActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);
            
            if (viewModel.CanDelete
                && PerformAction(() => service.CommodityActions.Delete(id, CurrentUser.ID)))
            {
                return Json(new { success = true });
            }

            return PartialView(viewModel);
        }

                 

        private IQueryable<CommodityViewModel> GetCommodityViewModels()
        {
            var CommodityCollection = service.CommodityActions.Search(CurrentJob.ID, "").Select(Mapper.Map<CommodityViewModel>);
            return CommodityCollection.AsQueryable();
        }

        private Commodity ConvertViewModelToModel(CommodityViewModel viewModel)
        {
            return Mapper.Map<Commodity>(viewModel);
        }

        private CommodityViewModel ConvertModelToViewModel(Commodity model)
        {
            return Mapper.Map<CommodityViewModel>(model);
        }
    }
}