using System.Web.Mvc;
using MMR.TOPControls.Web.Controllers;

namespace MMR.TOPControls.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "TOPAdmin,TOPManager")]
    public class AdminBaseController : BaseController
    {
    }
}