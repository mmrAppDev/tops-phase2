using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Envoc.Core.Shared.Extensions;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using Lib.TOPControls.Provider;
using Lib.TOPControls.Security;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;
using MMR.TOPControls.Web.Helpers.DataTables;
using System.Web.Security;
using System;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.Controllers
{
    public class TopUserController : AdminBaseController
    {
        private readonly TOPControlLibrary service;
        private readonly LocalDataRoleProvider roleProvider = new LocalDataRoleProvider();
        

        public TopUserController()
        {
            service = new TOPControlLibrary(new TOPControlSQLDAL());
        }

        public ActionResult List()
        {
            return View(GetTopUserViewModels());
        }

        public ActionResult ListItems(DataTableRequest request)
        {
            var parser = new DataTableParser<TopUserViewModel>(GetTopUserViewModels());
            var formattedList = parser.Parse(request);
            return Json(formattedList);
        }

        public ActionResult Create()
        {
            return PartialView(new CreateTopUserViewModel
                {
                    JobID = CurrentJob.ID,
                    Roles = GetAllRoles(),
                    JobList = GetJobSelectList(),
                    DefaultJobID = CurrentJob.ID
                });
        }

        private IEnumerable<SelectListItem> GetAllRoles(TOPUser model = null)
        {
            string[] userRoles;
            string modelRole = "";

            if (model != null)
            {
                userRoles = roleProvider.GetRolesForUser(model.Login);
            if (userRoles.Length > 0)
            {
                modelRole = userRoles.First();
            }
            }

            var roles =  roleProvider
                .GetAllRoles()
                .Select(role => new SelectListItem
                    {
                        Text = role, 
                        Value = role,
                        Selected = model != null && modelRole == role
                    }).ToList();

            roles.Insert(0, new SelectListItem());

            return roles;
        }

        [HttpPost]
        public ActionResult Create(CreateTopUserViewModel viewModel)
        {
            var model = ConvertViewModelToModel(viewModel);

            if (model.DefaultJobID.HasValue && model.DefaultJobID.Value == 0)
            {
                model.DefaultJobID = null;
            }

            if (string.IsNullOrWhiteSpace(viewModel.Password))
            {
                ModelState.AddModelError("Password", "The password  must be a non-blank value.");
            }
            else if (viewModel.Password.Trim() != viewModel.ConfirmPassword.Trim())
            {
                ModelState.AddModelError("Password", "The confirmation does not match the password.");
            }
            
            var vr = service.UserActions.Validate(model);
            if (!vr.IsValid)
            {
                foreach (var rulev in vr.RuleViolations)
                {
                    ModelState.AddModelError(rulev.PropertyName, rulev.ErrorMessage);
                }
            }
            if (ModelState.IsValid && PerformAction(() => service.UserActions.AddWithCredentials(viewModel.Login, viewModel.FirstName, viewModel.LastName, viewModel.Password.Trim(), "1-1-2-3-5-8-?", "13", CurrentUser.ID, true)))
            {
                roleProvider.AddUsersToRoles(new[] {model.Login}, new[] {viewModel.RoleName});
                if (viewModel.DefaultJobID != 0 && service.JobActions.Find(viewModel.DefaultJobID) != null)
                {
                    var newUser = service.UserActions.FindByUsername(model.Login);
                    var uj = new UserJob { JobID = viewModel.DefaultJobID, UserID = newUser.ID, LastChanged = DateTime.Now };
                    newUser.DefaultJobID = viewModel.DefaultJobID;
                    service.UserJobActions.Add(uj, CurrentUser.ID);
                }
                return Json(new { success = true });
            };

            viewModel.Roles = GetAllRoles(model);
            viewModel.JobList = GetJobSelectList(model);

            return PartialView(viewModel);
        }

        private IEnumerable<SelectListItem> GetJobSelectList(TOPUser model = null, bool includeBlank = true)
        {
            List<SelectListItem> jobList;
            bool isSubjectAdmin = false;
            bool isActorAdmin = User.IsInRole("TOPAdmin");

            if (model != null && model.ID > 0)
            {

                isSubjectAdmin = service.UserActions.IsUserInRole(model.ID, "TOPAdmin");
                if (isSubjectAdmin)
                {
                    var userJobs = service.UserJobActions.AllForUser(model.ID);

                    jobList = service.JobActions.Search("")
                                .Select(j => new SelectListItem
                                    {
                                        Text = j.Title,
                                        Value = j.ID.ToString(),
                                        Selected = true
                                    }).ToList();

                    foreach(SelectListItem sli in jobList)
                    {
                        
                        if (!userJobs.Any(x => x.JobID.ToString() == sli.Value))
                        {
                            sli.Text += " (From TOPAdmin)";
                        }
                    }
                }
                else
                {
                    jobList =
                        service.UserJobActions.AllForUser(model.ID)
                               .Select(uj => new SelectListItem
                                   {
                                       Text = uj.JobInfo.Title,
                                       Value = uj.JobInfo.ID.ToString(),
                                       Selected = uj.JobInfo.ID.Equals(model.DefaultJobID)
                                   }).ToList();
                }
            }
            else
            {

                jobList = service.JobActions.Search("")
                            .Select(j => new SelectListItem
                                {
                                    Text = j.Title,
                                    Value = j.ID.ToString(),
                                    Selected = true
                                }).ToList();

            }
            if (includeBlank)
            {
                var blankItem = new SelectListItem
                        {
                            Text = "",
                            Value = "0",
                            Selected = (model == null ? false : !model.DefaultJobID.HasValue)
                        };
                jobList.Insert(0, blankItem);
            }
            return jobList;
        }

        public ActionResult Edit(int id)
        {
            var model = service.UserActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = createEditTopUserViewModel(model);

            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(EditTopUserViewModel viewModel)
        {
            var model = service.UserActions.Find(viewModel.ID);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            Mapper.Map(viewModel, model);

            try
            {
                SetSelectedValue(Request.Form, model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("RoleName", ex.Message);
            }

            if (!string.IsNullOrWhiteSpace(viewModel.Password))
            {
                if (viewModel.Password == viewModel.ConfirmPassword)
                {
                    try
                    {
                        service.UserActions.SetUserPassword(viewModel.ID, viewModel.Password.Trim(), CurrentUser.ID);
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("Password", ex);
                    }
                }
                else
                {
                    ViewData.ModelState.AddModelError("Password", "Password and Confirmation do not match.");
                }
            }

            if (ModelState.IsValid)
            {
                if (PerformAction(() => service.UserActions.Update(model, CurrentUser.ID)))
                {
                    return RedirectToAction("List");
                }
            }
            var role = Request.Form["RoleName"];
            viewModel.Roles = GetAllRoles(model);
            viewModel.JobList = GetJobSelectList(model, false);
            viewModel.DefaultJob = GetDefaultJob(model);
            string strDefJobID = Request.Form["DefaultJobID"];
            long defJobID = default(long);
            if (!string.IsNullOrWhiteSpace(strDefJobID) 
                    && long.TryParse(strDefJobID, out defJobID)
                    && defJobID != default(long))
            {
                viewModel.DefaultJobID = defJobID;
            }

            return PartialView(viewModel);
        }

        private EditTopUserViewModel createEditTopUserViewModel(TOPUser model)
        {
            var viewModel = new EditTopUserViewModel
            {
                ID = model.ID,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Login = model.Login,
                JobList = GetJobSelectList(model, false),
                DefaultJob = GetDefaultJob(model),
                DefaultJobID = model.DefaultJobID.HasValue ? model.DefaultJobID.Value : 0,
                Roles = GetAllRoles(model)
            };
            viewModel.AvailableJobs = service.JobActions.Search("").AsQueryable()
                   .Project().To<JobInfoViewModel>().Where(j => !model.UserJobs.Select(uj => uj.JobID).Contains(j.ID)).ToList();

            return viewModel;
        }

        public ActionResult EditJobs(int id)
        {
            var model = service.UserActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = createEditTopUserViewModel(model);

            return PartialView(viewModel);
        }

        public ActionResult EditUserJobs(int id)
        {
            var model = service.UserActions.Find(id);
            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = createEditTopUserViewModel(model);

            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult AddUserJob(long id, long jobid)
        {
            // validate things that should exist
            var job = service.JobActions.Find(jobid);
            if (job == null || job.IsDeleted)
            {
                return Json(new { success = false, message = "Job is missing or deleted." });
            }
            var user = service.UserActions.Find(id);
            if (user == null || user.IsDeleted)
            {
                return Json(new { success = false, message = "Uesr is missing or deleted." });
            }
            // validates things that should not already exist
            if (service.UserJobActions.AllForUser(id).Any(uj => uj.JobID == jobid))
            {
                return Json(new { success = false, message = "User already belongs to job. (This response can be caused by double-clicking the add link.)" });
            }

            var userJob = new UserJob
            {
                JobID = jobid,
                UserID = id
            };

            try
            {
                service.UserJobActions.Add(userJob, CurrentUser.ID);

                return Json(new { success = true });
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message });
            }
        }

        [HttpPost]
        public ActionResult RemoveUserJob(long id, long jobid)
        {
            // validate things that should exist

            var userJob = service.UserJobActions.AllForUser(id).FirstOrDefault(uj => uj.JobID == jobid);
            if (userJob == null)
            {
                return Json(new { success = false, message = "User doesn't belong to job. (This response can be caused by double-clicking the remove link.)" });
            }

            try
            {
                service.UserJobActions.Delete(userJob.ID, CurrentUser.ID);

                return Json(new { success = true });
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message });
            }
        }


        
        
        
        private void SetSelectedValue(NameValueCollection collection, TOPUser model)
        {
            var role = collection["RoleName"];

            if (string.IsNullOrWhiteSpace(role))
            {
                clearUserRoles(model.Login);
            }
            else if (roleProvider.RoleExists(role))
            {
                var roleNew = service.GetRepository<Role>().All.FirstOrDefault(x => x.RoleName == role);

                if (roleNew == null) throw new ApplicationException("Role is valid but cannot be found in the database.");
                if (!roleProvider.IsUserInRole(model.Login, role))
                {
                    service.UserActions.SetUserRole(model.ID, roleNew.ID, CurrentUser.ID);
                }
            }

            string strDefJobID = collection["DefaultJobID"];
            long defJobID = default(long);
            
            if (!string.IsNullOrWhiteSpace(strDefJobID) 
                    && long.TryParse(strDefJobID, out defJobID)
                    && defJobID != default(long))
            {
                model.DefaultJobID = defJobID;
            }
            else
            {
                model.DefaultJobID = null;
            }


            
        }

        private void clearUserRoles(string username)
        {
            var userRoles = roleProvider.GetRolesForUser(username);
            var userlist = new string[] { username };
            roleProvider.RemoveUsersFromRoles(userlist, userRoles);
        }

        public ActionResult Delete(int id)
        {
            var model = service.UserActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }
            var viewModel = BuildDeletionViewModel(model);

            return PartialView(viewModel);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = service.UserActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }
            var viewModel = BuildDeletionViewModel(model);

            if (viewModel.CanDelete
                && PerformAction(() => service.UserActions.Delete(id, CurrentUser.ID)))
            {
                return Json(new { success = true });
            }

            return PartialView(viewModel);
        }



        private IQueryable<TopUserViewModel> GetTopUserViewModels()
        {
            var topUserCollection = service.UserActions.Search("").Select(x => new TopUserViewModel
                {
                    ID = x.ID,
                    Login = x.Login,
                    DefaultJob = GetDefaultJob(x),
                    FirstName = x.FirstName,
                    LastName = x.LastName
                });
            return topUserCollection.AsQueryable();
        }

        private string GetDefaultJob(TOPUser topUser)
        {
            return topUser.DefaultJobID.HasValue ? service.JobActions.Find(topUser.DefaultJobID.Value).Title : "Not Set";
        }

        private TOPUser ConvertViewModelToModel(TopUserViewModel viewModel)
        {
            return Mapper.Map<TOPUser>(viewModel);
        }

        private TopUserViewModel ConvertModelToViewModel(TOPUser model)
        {
            return Mapper.Map<TopUserViewModel>(model);
        }

        private DeletionViewModel<TopUserViewModel> BuildDeletionViewModel(TOPUser model)
        {
            return new DeletionViewModel<TopUserViewModel>(
                ConvertModelToViewModel(model),
                service.UserActions.ValidateDelete(model.ID));
        }
        
    }
}