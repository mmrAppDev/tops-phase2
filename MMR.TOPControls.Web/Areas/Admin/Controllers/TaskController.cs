using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Envoc.Core.Shared.Extensions;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;
using MMR.TOPControls.Web.Helpers.DataTables;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.Controllers
{
    public class TaskController : AdminBaseController
    {
        private readonly TOPControlLibrary service;

        public TaskController()
        {
            service = new TOPControlLibrary(new TOPControlSQLDAL());
        }

        public ActionResult List(long phaseId)
        {
            var model = new TaskListViewModel
            {
                Phase = service.PhaseActions.Find(phaseId),
                PhaseTasks = GetTaskViewModels(phaseId)
            };
            return View(model);
        }

        public ActionResult ListItems(DataTableRequest request, long phaseId)
        {
            var parser = new DataTableParser<TaskViewModel>(GetTaskViewModels(phaseId));
            var formattedList = parser.Parse(request);
            return Json(formattedList);
        }

        public ActionResult Create(long phaseId)
        {
            var viewModel = new CreateTaskViewModel
            {
                PhaseID = phaseId,
                JobID = CurrentJob.ID
            };

            AddCommodityListToViewModel(viewModel);

            return PartialView(viewModel);
        }

        [HttpPost]
        public ActionResult Create(CreateTaskViewModel viewModel)
        {
            var model = ConvertViewModelToModel(viewModel);
            model.PhaseID = viewModel.PhaseID;

            if (PerformAction(() => service.TaskActions.Add(model, CurrentUser.ID)))
            {
                PropogateCommodity(viewModel.PropogateCommodity, model.ID, model.DefaultCommodityID);

                return Json(new {success = true});
            }

            return PartialView(viewModel);
        }

        public ActionResult Edit(int id)
        {
            var model = service.TaskActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            return PartialView((ConvertModelToViewModel(model)));
        }

        [HttpPost]
        public ActionResult Edit(TaskViewModel viewModel)
        {
            var model = service.TaskActions.Find(viewModel.ID);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            Mapper.Map(viewModel, model);

            if (PerformAction(() => service.TaskActions.Update(model, CurrentUser.ID)))
            {
                PropogateCommodity(viewModel.PropogateCommodity, model.ID, model.DefaultCommodityID);
                return Json(new {success = true});
            }

            AddCommodityListToViewModel(viewModel);
            return PartialView(viewModel);
        }

        public ActionResult Delete(int id)
        {
            var model = service.TaskActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);

            return PartialView(viewModel);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = service.TaskActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);

            if (viewModel.CanDelete
                && PerformAction(() => service.TaskActions.Delete(id, CurrentUser.ID)))
            {
                return Json(new {success = true});
            }

            return PartialView(viewModel);
        }

        private IQueryable<TaskViewModel> GetTaskViewModels(long phaseId)
        {
            var taskCollection =
                service.TaskActions.AllForJob(CurrentJob.ID)
                    .AsQueryable()
                    .Where(p => p.PhaseID.Equals(phaseId));

            return
                taskCollection.Select(
                    x =>
                        new TaskViewModel
                        {
                            ID = x.ID,
                            DefaultCommodity =
                                x.DefaultCommodity == null ? string.Empty : x.DefaultCommodity.CommodityCode,
                            DefaultCommodityID = x.DefaultCommodityID,
                            DisplayOrder = x.DisplayOrder,
                            JobID = x.JobID,
                            Title = x.Title
                        });
        }

        private Task ConvertViewModelToModel(TaskViewModel viewModel)
        {
            return Mapper.Map<Task>(viewModel);
        }

        private TaskViewModel ConvertModelToViewModel(Task model)
        {
            var viewModel = Mapper.Map<TaskViewModel>(model);
            AddCommodityListToViewModel(viewModel);
            return viewModel;
        }

        private void AddCommodityListToViewModel(TaskViewModel viewModel)
        {
            var commodities =
                service.CommodityActions.Search(CurrentJob.ID, "")
                    .Select(x => new SelectListItem {Value = x.ID.ToString(), Text = x.DisplayName});
            var selectListSeed = new List<SelectListItem>(commodities);
            selectListSeed.Insert(0, new SelectListItem {Value = null, Text = string.Empty});

            viewModel.Commodities = new SelectList(selectListSeed, "Value", "Text");
        }

        private DeletionViewModel<TaskViewModel> BuildDeletionViewModel(Task model)
        {
            return new DeletionViewModel<TaskViewModel>(
                ConvertModelToViewModel(model),
                service.TaskActions.ValidateDelete(model.ID));
        }

        private void PropogateCommodity(bool propogateCommodity, long taskId, long? defaultCommodityID)
        {
            if (propogateCommodity)
            {
                service.PropogateCommodityByTask(taskId, defaultCommodityID);
            }
        }
    }
}