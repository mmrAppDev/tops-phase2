﻿using System;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.Controllers
{
    public class JobSettingsController : AdminBaseController
    {
        private readonly TOPControlLibrary service;

        public JobSettingsController()
        {
            service = new TOPControlLibrary(new TOPControlSQLDAL());
        }

        public ActionResult Index(long? id)
        {
            if (id.HasValue && id.Value > 0)
            {
                if (User.IsInRole("TOPAdmin") || service.UserJobActions.AllForJob(id.Value).Any(uj => uj.UserID == CurrentUser.ID))
                {
                    // try to switch to the indicated job
                    var job = service.JobActions.Find(id.Value);

                    CurrentJob = Mapper.Map<JobInfoViewModel>(job);
                }
                else
                {
                    return RedirectToAction("Index", "JobInfo", new { area = "Admin" });
                }
            }

            var viewModel = new JobSettingsViewModel
            {
                Id = CurrentJob.ID,
                ShowEarnedToClient = CurrentJob.ShowEarnedToClient,
                WeightedCalculations = CurrentJob.WeightedCalculations,
                HistoricalScopeCurve = CurrentJob.HistoricalScopeCurve
            };

            return View(viewModel);
        }



        [HttpPost]
        public ActionResult Index(JobSettingsViewModel viewModel)
        {
            var jobInfo = service.JobActions.Find(viewModel.Id);

            jobInfo.ShowEarnedToClient = viewModel.ShowEarnedToClient;
            jobInfo.WeightedCalculations = viewModel.WeightedCalculations;
            jobInfo.HistoricalScopeCurve = viewModel.HistoricalScopeCurve;

            if (PerformAction(() => service.JobActions.Update(jobInfo, CurrentUser.ID)))
            {
                return RedirectToAction("Index", "ProjectStatus", new { area = "" });
            }
            CurrentJob = Mapper.Map<JobInfoViewModel>(jobInfo);

            return View(viewModel);
        }

        public ActionResult JobSettingsUsers()
        {
            var jobUsers = service.UserJobActions.AllForJob(CurrentJob.ID).ToList();

            var viewModel = new JobSettingsViewModel
            {
                JobUsers = jobUsers,
                AvailableJobUsers =
                    service.UserActions.Search("").Where(u => !jobUsers.Select(uj => uj.UserID).Contains(u.ID))
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddUser(long id)
        {
            var userJob = new UserJob
            {
                JobID = CurrentJob.ID,
                UserID = id
            };

            try
            {
                service.UserJobActions.Add(userJob, CurrentUser.ID);

                return Json(new {success = true});
            }
            catch (Exception e)
            {
                return Json(new {success = false, message = e.Message});
            }
        }

        [HttpPost]
        public ActionResult RemoveUser(long id)
        {
            try
            {
                service.UserJobActions.Delete(id, CurrentUser.ID);

                return Json(new {success = true});
            }
            catch (Exception e)
            {
                return Json(new {success = false, message = e.Message});
            }
        }
    }
}