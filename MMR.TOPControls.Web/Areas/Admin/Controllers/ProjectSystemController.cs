using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Envoc.Core.Shared.Extensions;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using Lib.TOPControls.Services;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.Controllers
{
    public class ProjectSystemController : AdminBaseController
    {
        private readonly TOPControlLibrary library;
        private readonly ProjectSystemService service;

        public ProjectSystemController()
        {
            library = new TOPControlLibrary(new TOPControlSQLDAL());
            service = new ProjectSystemService(library);
        }

        public ActionResult List()
        {
            return View(new List<ProjectSystemViewModel>());
        }

        public ActionResult ListItems()
        {
            return Json(new
            {
                success = true,
                message = "",
                data = GetProjectSystemViewModels()
            });
        }

        public JsonResult GetAllForSelectList(string query)
        {
            var results = library.SystemActions
                .Search(CurrentJob.ID, query)
                .OrderBy(x => x.SystemNumber)
                .Select(x => new {id = x.ID, text = x.SystemNumber});

            return Json(results);
        }

        public ActionResult Create(long? parentId)
        {
            var parentName = string.Empty;

            if (parentId.HasValue)
            {
                var parent = library.SystemActions.Find(parentId.GetValueOrDefault());
                parentName = parent.SystemNumber;
            }

            return PartialView(new ProjectSystemViewModel
            {
                JobID = CurrentJob.ID,
                ParentId = parentId,
                ParentName = parentName
            });
        }

        [HttpPost]
        public ActionResult Create(ProjectSystemViewModel viewModel)
        {
            var model = ConvertViewModelToModel(viewModel);

            if (PerformAction(() => service.Add(model, CurrentUser.ID)))
            {
                return Json(new {success = true});
            }

            return PartialView(viewModel);
        }

        public ActionResult Edit(int id)
        {
            var model = library.SystemActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            return PartialView((ConvertModelToViewModel(model)));
        }

        public ActionResult ChangePositionInTree(ProjectSystemViewModel viewModel)
        {
            var model = library.SystemActions.Find(viewModel.ID);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var result = service.ChangePositionInTree(model, viewModel.ParentId, viewModel.SortOrder, CurrentUser.ID);

            return
                Json(
                    new
                    {
                        success = result.IsValid,
                        message = string.Join(", ", result.RuleViolations.Select(x => x.ErrorMessage)),
                        data = GetProjectSystemViewModels()
                    });
        }

        [HttpPost]
        public ActionResult Edit(ProjectSystemViewModel viewModel)
        {
            var model = library.SystemActions.Find(viewModel.ID);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            Mapper.Map(viewModel, model);

            if (PerformAction(() => library.SystemActions.Update(model, CurrentUser.ID)))
            {
                return Json(new {success = true});
            }

            return PartialView(viewModel);
        }

        public ActionResult Delete(int id)
        {
            var model = library.SystemActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);

            return PartialView(viewModel);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = library.SystemActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var result = service.Delete(model, CurrentUser.ID);

            return
                Json(
                    new
                    {
                        success = result.IsValid,
                        message = string.Join(", ", result.RuleViolations.Select(x => x.ErrorMessage)),
                        data = GetProjectSystemViewModels()
                    });
        }

        [HttpPost]
        public ActionResult Merge(int sourceId, int destinationId)
        {
            try
            {
                service.Merge(CurrentJob.ID, sourceId, destinationId, CurrentUser.ID);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) ex = ex.InnerException;
                return
                    Json(
                        new
                        {
                            success = false,
                            message = ex.Message,
                            data = string.Empty
                        });
            }

            return
                Json(
                    new
                    {
                        success = true,
                        message = string.Empty,
                        data = string.Empty
                    });
        }

        private IEnumerable<ProjectSystem> GetProjectSystemViewModels()
        {
            var tree = library.SystemActions.GetTreeByJobId(CurrentJob.ID);

            return tree;
        }

        private ProjectSystem ConvertViewModelToModel(ProjectSystemViewModel viewModel)
        {
            return Mapper.Map<ProjectSystem>(viewModel);
        }

        private ProjectSystemViewModel ConvertModelToViewModel(ProjectSystem model)
        {
            return Mapper.Map<ProjectSystemViewModel>(model);
        }

        private DeletionViewModel<ProjectSystemViewModel> BuildDeletionViewModel(ProjectSystem model)
        {
            return new DeletionViewModel<ProjectSystemViewModel>(
                ConvertModelToViewModel(model),
                library.SystemActions.ValidateDelete(model.ID));
        }
    }
}