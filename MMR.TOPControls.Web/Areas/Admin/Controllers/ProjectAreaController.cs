using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;
using MMR.TOPControls.Web.Helpers.DataTables;
using Envoc.Core.Shared.Extensions;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.Controllers
{
    public class ProjectAreaController : AdminBaseController
    {
		private readonly TOPControlLibrary service;

        public ProjectAreaController()
        {
            service = new TOPControlLibrary(new TOPControlSQLDAL());
        }

        public ActionResult List()
        {
            return View(GetProjectAreaViewModels());
        }

        public ActionResult ListItems(DataTableRequest request)
        {
            var parser = new DataTableParser<ProjectAreaViewModel>(GetProjectAreaViewModels());
            var formattedList = parser.Parse(request);
            return Json(formattedList);
        }

        public ActionResult GetAllForSelectList(string query)
        {
            var results = service.AreaActions
                .Search(CurrentJob.ID, "")
                .Where(x=>x.AreaNumber.Contains(query))
                .OrderBy(x=>x.AreaNumber)
                .Select(x => new { id = x.ID, text = x.AreaNumber });

            return Json(results);
        }

        public ActionResult Create()
        {
            return PartialView(new ProjectAreaViewModel {JobID = CurrentJob.ID});
        }

        [HttpPost]
        public ActionResult Create(ProjectAreaViewModel viewModel)
        {
            var model = ConvertViewModelToModel(viewModel);

            if (PerformAction(() => service.AreaActions.Add(model, CurrentUser.ID)))
            {
                return Json(new {success = true});
            }
            ;

            return PartialView(viewModel);
        }

        public ActionResult Edit(int id)
        {
            var model = service.AreaActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            return PartialView((ConvertModelToViewModel(model)));
        }

        [HttpPost]
        public ActionResult Edit(ProjectAreaViewModel viewModel)
        {
            var model = service.AreaActions.Find(viewModel.ID);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            Mapper.Map(viewModel, model);

            if (PerformAction(() => service.AreaActions.Update(model, CurrentUser.ID)))
            {
                return Json(new {success = true});
            }
            ;

            return PartialView(viewModel);
        }

        public ActionResult Delete(int id)
        {
            var model = service.AreaActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);

            return PartialView(viewModel);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = service.AreaActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);

            if (viewModel.CanDelete
                && PerformAction(() => service.AreaActions.Delete(id, CurrentUser.ID)))
            {
                return Json(new { success = true });
            }

            return PartialView(viewModel);
        }

        private IQueryable<ProjectAreaViewModel> GetProjectAreaViewModels()
        {
            var ProjectAreaCollection = service.AreaActions.Search(CurrentJob.ID, "").AsQueryable().Project().To<ProjectAreaViewModel>();
            return ProjectAreaCollection.AsQueryable();
        }

        private ProjectArea ConvertViewModelToModel(ProjectAreaViewModel viewModel)
        {
            return Mapper.Map<ProjectArea>(viewModel);
        }

        private ProjectAreaViewModel ConvertModelToViewModel(ProjectArea model)
        {
            return Mapper.Map<ProjectAreaViewModel>(model);
        }

        private DeletionViewModel<ProjectAreaViewModel> BuildDeletionViewModel(ProjectArea model)
        {
            return new DeletionViewModel<ProjectAreaViewModel>(
                ConvertModelToViewModel(model),
                service.AreaActions.ValidateDelete(model.ID));
        }
    }
}