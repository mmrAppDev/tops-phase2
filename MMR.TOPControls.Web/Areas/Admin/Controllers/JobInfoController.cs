using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;
using MMR.TOPControls.Web.Helpers.DataTables;
using Envoc.Core.Shared.Extensions;
using AutoMapper.QueryableExtensions;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.Controllers
{
    [Authorize(Roles="TOPAdmin")]
    public class JobInfoController : AdminBaseController
    {
		private readonly TOPControlLibrary service;

		public JobInfoController()
		{
            service = new TOPControlLibrary(new TOPControlSQLDAL());
		}

        public ActionResult List()
        {
            return View(GetJobInfoViewModels());
        }

        public ActionResult ListItems(DataTableRequest request)
        {
            var parser = new DataTableParser<JobInfoViewModel>(GetJobInfoViewModels());
            var formattedList = parser.Parse(request);
            return Json(formattedList);
        }

        public ActionResult Create()
        {
            return PartialView(new JobInfoViewModel());
        }

        [HttpPost]
        public ActionResult Create(JobInfoViewModel viewModel)
        {
			var model = ConvertViewModelToModel(viewModel);

            if (PerformAction(() => service.JobActions.Add(model, CurrentUser.ID)))
            {
                CurrentJob = viewModel;
                return Json(new { success = true });
            };

            return PartialView(viewModel);
        }

        public ActionResult Edit(int id)
        {
            var model = service.JobActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            return PartialView((ConvertModelToViewModel(model)));
        }

        [HttpPost]
        public ActionResult Edit(JobInfoViewModel viewModel)
        {
            var model = service.JobActions.Find(viewModel.ID);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            Mapper.Map(viewModel, model);

            if (PerformAction(() => service.JobActions.Update(model, CurrentUser.ID)))
            {
                CurrentJob = viewModel;
                return Json(new { success = true });
            };

            return PartialView(viewModel);
        }

        public ActionResult Delete(int id)
        {
            var model = service.JobActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);

            return PartialView(viewModel);
        }

        private JobInfoViewModel BuildDeletionViewModel(JobInfo model)
        {
            var vr = service.JobActions.ValidateDelete(model.ID);
            var result = ConvertModelToViewModel(model);
            result.CanDelete = vr.IsValid;
            if (!vr.IsValid)
            {
                AddToModelError(vr.RuleViolations);
            }
            return result;
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = service.JobActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }
            
            if (PerformAction(() => service.JobActions.Delete(model.ID, CurrentUser.ID)))
            {
                return Json(new { success = true });
            }

            var viewModel = BuildDeletionViewModel(model);

            return PartialView(viewModel);
        }

        private IQueryable<JobInfoViewModel> GetJobInfoViewModels()
        {

            return service.JobActions.Search("").AsQueryable().Project().To<JobInfoViewModel>();
        }

	    private JobInfo ConvertViewModelToModel(JobInfoViewModel viewModel)
        {
            return Mapper.Map<JobInfo>(viewModel);
        }

		private JobInfoViewModel ConvertModelToViewModel(JobInfo model)
        {
            return Mapper.Map<JobInfoViewModel>(model);
        }
	}
}