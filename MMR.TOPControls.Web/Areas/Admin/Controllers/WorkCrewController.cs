using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Envoc.Core.Shared.Extensions;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;
using MMR.TOPControls.Web.Controllers;
using MMR.TOPControls.Web.Helpers.DataTables;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.Controllers
{ 
    public class WorkCrewController : BaseController
    {
		private readonly TOPControlLibrary service;

        public WorkCrewController()
        {
            service = new TOPControlLibrary(new TOPControlSQLDAL());
        }

        public ActionResult List()
        {
            return View(GetWorkCrewViewModels());
        }

        public ActionResult ListItems(DataTableRequest request)
        {
            var parser = new DataTableParser<WorkCrewViewModel>(GetWorkCrewViewModels());
            var formattedList = parser.Parse(request);
            return Json(formattedList);
        }

        public ActionResult GetAllForSelectList(string query)
        {
            var results = service.WorkCrewActions
                .AllByJob(CurrentJob.ID)
                .Where(x=>x.Description.Contains(query))
                .OrderBy(x=>x.CrewNumber)
                .Select(x => new { id = x.ID, text = x.CrewNumber });

            return Json(results);
        }

        public ActionResult Create()
        {
            return PartialView(new WorkCrewViewModel
                {
                    JobID = CurrentJob.ID
                });
        }

        [HttpPost]
        public ActionResult Create(WorkCrewViewModel viewModel)
        {
            var model = ConvertViewModelToModel(viewModel);

            if (PerformAction(() => service.WorkCrewActions.Add(model, CurrentUser.ID)))
            {
                return Json(new {success = true});
            }
            ;

            return PartialView(viewModel);
        }

        public ActionResult Edit(int id)
        {
            var model = service.WorkCrewActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            return PartialView((ConvertModelToViewModel(model)));
        }

        [HttpPost]
        public ActionResult Edit(WorkCrewViewModel viewModel)
        {
            var model = service.WorkCrewActions.Find(viewModel.ID);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            Mapper.Map(viewModel, model);

            if (PerformAction(() => service.WorkCrewActions.Update(model, CurrentUser.ID)))
            {
                return Json(new {success = true});
            }
            ;

            return PartialView(viewModel);
        }

        public ActionResult Delete(int id)
        {
            var model = service.WorkCrewActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }
            var viewModel = BuildDeletionViewModel(model);
            return PartialView(viewModel);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = service.WorkCrewActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);

            if (viewModel.CanDelete
                && PerformAction(() => service.WorkCrewActions.Delete(id, CurrentUser.ID)))
            {
                return Json(new { success = true });
            }
            return PartialView(viewModel);
        }

        private IQueryable<WorkCrewViewModel> GetWorkCrewViewModels()
        {
            var workCrewCollection = service.WorkCrewActions.AllByJob(CurrentJob.ID).AsQueryable().Project().To<WorkCrewViewModel>();
            return workCrewCollection.AsQueryable();
        }

        private WorkCrew ConvertViewModelToModel(WorkCrewViewModel viewModel)
        {
            return Mapper.Map<WorkCrew>(viewModel);
        }

        private WorkCrewViewModel ConvertModelToViewModel(WorkCrew model)
        {
            return Mapper.Map<WorkCrewViewModel>(model);
        }

        private DeletionViewModel<WorkCrewViewModel> BuildDeletionViewModel(WorkCrew model)
        {
            return new DeletionViewModel<WorkCrewViewModel>(
                ConvertModelToViewModel(model),
                service.WorkCrewActions.ValidateDelete(model.ID));
        }
	}
}