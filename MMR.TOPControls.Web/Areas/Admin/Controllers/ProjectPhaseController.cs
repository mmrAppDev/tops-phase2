using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;
using MMR.TOPControls.Web.Helpers.DataTables;
using Envoc.Core.Shared.Extensions;
using AutoMapper.QueryableExtensions;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.Controllers
{
    public class ProjectPhaseController : AdminBaseController
    {
		private readonly TOPControlLibrary service;

        public ProjectPhaseController()
        {
            service = new TOPControlLibrary(new TOPControlSQLDAL());
        }

        public ActionResult List()
        {
            return View(GetProjectPhaseViewModels());
        }

        public ActionResult ListItems(DataTableRequest request)
        {
            var parser = new DataTableParser<ProjectPhaseViewModel>(GetProjectPhaseViewModels());
            var formattedList = parser.Parse(request);
            return Json(formattedList);
        }

        public ActionResult Create()
        {
            return PartialView(new ProjectPhaseViewModel {JobID = CurrentJob.ID});
        }

        [HttpPost]
        public ActionResult Create(ProjectPhaseViewModel viewModel)
        {
            var model = ConvertViewModelToModel(viewModel);

            if (PerformAction(() => service.PhaseActions.Add(model, CurrentUser.ID)))
            {
                return Json(new {success = true});
            }
            ;

            return PartialView(viewModel);
        }

        public ActionResult Edit(int id)
        {
            var model = service.PhaseActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            return PartialView((ConvertModelToViewModel(model)));
        }

        [HttpPost]
        public ActionResult Edit(ProjectPhaseViewModel viewModel)
        {
            var model = service.PhaseActions.Find(viewModel.ID);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            Mapper.Map(viewModel, model);

            if (PerformAction(() => service.PhaseActions.Update(model, CurrentUser.ID)))
            {
                return Json(new {success = true});
            }
            ;

            return PartialView(viewModel);
        }

        public ActionResult Delete(int id)
        {
            var model = service.PhaseActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);

            return PartialView(viewModel);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = service.PhaseActions.Find(id);

            if (model.IsNull())
            {
                return new HttpNotFoundResult();
            }

            var viewModel = BuildDeletionViewModel(model);

            if (viewModel.CanDelete
                && PerformAction(() => service.PhaseActions.Delete(id, CurrentUser.ID)))
            {
                return Json(new { success = true });
            }

            return PartialView(viewModel);
        }


        private DeletionViewModel<ProjectPhaseViewModel> BuildDeletionViewModel(ProjectPhase model)
        {
            return new DeletionViewModel<ProjectPhaseViewModel>(
                ConvertModelToViewModel(model),
                service.PhaseActions.ValidateDelete(model.ID));


        }

        private IQueryable<ProjectPhaseViewModel> GetProjectPhaseViewModels()
        {
            var ProjectPhaseCollection = service.PhaseActions.AllForJob(CurrentJob.ID).AsQueryable().Project().To<ProjectPhaseViewModel>();
            return ProjectPhaseCollection.AsQueryable();
        }

        private ProjectPhase ConvertViewModelToModel(ProjectPhaseViewModel viewModel)
        {
            return Mapper.Map<ProjectPhase>(viewModel);
        }

        private ProjectPhaseViewModel ConvertModelToViewModel(ProjectPhase model)
        {
            return Mapper.Map<ProjectPhaseViewModel>(model);
        }
	}
}