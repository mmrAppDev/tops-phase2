﻿using System.ComponentModel;
using Envoc.Core.Shared.Extensions;
using MMR.TOPControls.Web.Helpers.DataTables.Attributes;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class ProjectPhaseViewModel : DomainViewModelBase
    {
        private string title;

        [Searchable]
        [Sortable]
        [DisplayName("Title")]
        public string Title
        {
            get { return title; }
            set { title = value.ConvertNullToEmptyString(); }
        }

        [Searchable]
        [Sortable]
        [DisplayName("Display Order")]
        public int DisplayOrder { get; set; }
    }
}