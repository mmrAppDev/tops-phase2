﻿using System.ComponentModel;
using Envoc.Core.Shared.Extensions;
using MMR.TOPControls.Web.Helpers.DataTables.Attributes;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class ProjectAreaViewModel : DomainViewModelBase
    {
        private string areaNumber = string.Empty;
        private string description = string.Empty;

        [Searchable]
        [Sortable]
        [DisplayName("Area Number")]
        public string AreaNumber
        {
            get { return areaNumber; }
            set { areaNumber = value.ConvertNullToEmptyString(); }
        }

        [Searchable]
        [Sortable]
        public string Description
        {
            get { return description; }
            set { description = value.ConvertNullToEmptyString(); }
        }
    }
}