﻿using System.ComponentModel;
using MMR.TOPControls.Web.Helpers.DataTables.Attributes;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class TopUserViewModel:DomainViewModelBase
    {
        [Searchable][Sortable]
        public string Login { get; set; }

        [Searchable][Sortable]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [Searchable][Sortable]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [Searchable][Sortable]
        [DisplayName("Default Job")]
        public string DefaultJob { get; set; }
    }
}