﻿using System.ComponentModel;
using System.Web.Mvc;
using MMR.TOPControls.Web.Helpers.DataTables.Attributes;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class TaskViewModel : DomainViewModelBase
    {
        [Searchable]
        [Sortable]
        public string Title { get; set; }

        [DisplayName("Display Order")]
        [Searchable]
        [Sortable]
        public int DisplayOrder { get; set; }

        public long? DefaultCommodityID { get; set; }

        [DisplayName("Default Commodity")]
        public string DefaultCommodity { get; set; }

        [DisplayName("Default Commodity")]
        public SelectList Commodities { get; set; }

        [DisplayName("Replace Default Commodity on Active Items?")]
        public bool PropogateCommodity { get; set; }
    }
}