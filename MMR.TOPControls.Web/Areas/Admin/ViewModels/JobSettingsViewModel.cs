﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using Lib.TOPControls;

namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class JobSettingsViewModel
    {
        public long Id { get; set; }

        public IEnumerable<UserJob> JobUsers { get; set; }

        public IEnumerable<TOPUser> AvailableJobUsers { get; set; }

        public bool ShowEarnedToClient { get; set; }

        [DisplayName("Calculate Completions")]
        public bool WeightedCalculations { get; set; }

        public SelectList WeightedCalculationsOptions
        {
            get
            {
                var options = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Weighted Average of Man-Hours", Value = "true"},
                    new SelectListItem {Text = "Simple One-to-One Values", Value = "false"}
                };

                return new SelectList(options, "Value", "Text");
            }
        }

        public bool HistoricalScopeCurve { get; set; }

        public SelectList HistoricalScopeCurveOptions
        {
            get
            {
                var options = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Use Current Scope and Man-Hours", Value = "false"},
                    new SelectListItem {Text = "Use Scope and Weight Values at Time of Entry", Value = "true"}
                };

                return new SelectList(options, "Value", "Text");
            }
        }
    }
}