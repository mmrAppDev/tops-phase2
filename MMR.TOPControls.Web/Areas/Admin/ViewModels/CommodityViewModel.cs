﻿using System.ComponentModel;
using MMR.TOPControls.Web.Helpers.DataTables.Attributes;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class CommodityViewModel : DomainViewModelBase
    {
        [Searchable]
        [Sortable]
        [DisplayName("Commodity Code")]
        public string CommodityCode { get; set; }

        [Searchable]
        [Sortable]
        public string Description { get; set; }

        [Searchable]
        [Sortable]
        [DisplayName("Man Hours Estimated")]
        public decimal ManHoursEstimated { get; set; }

        public bool CanDelete { get; set; }
    }
}