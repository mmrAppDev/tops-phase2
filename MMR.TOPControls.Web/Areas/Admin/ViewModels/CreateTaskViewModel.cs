﻿namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class CreateTaskViewModel : TaskViewModel
    {
        public long PhaseID { get; set; }
    }
}