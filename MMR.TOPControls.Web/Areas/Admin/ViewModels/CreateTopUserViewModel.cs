using System.Collections.Generic;
using System.Web.Mvc;

namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class CreateTopUserViewModel : TopUserViewModel
    {
        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
        
        public long DefaultJobID { get; set; }
        public string RoleName { get; set; }

        public IEnumerable<SelectListItem> JobList { get; set; }
        public IEnumerable<SelectListItem> Roles { get; set; }
    }
}