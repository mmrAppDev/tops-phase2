﻿using MMR.TOPControls.Web.Helpers.DataTables.Attributes;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class ObjectTypeViewModel : DomainViewModelBase
    {
        [Searchable]
        [Sortable]
        public string Name { get; set; }

        [Searchable]
        [Sortable]
        public string Description { get; set; }

        public bool CanDelete { get; set; }
    }
}