﻿using System.Collections.Generic;
using Lib.TOPControls;

namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class TaskListViewModel
    {
        public ProjectPhase Phase { get; set; }
        public IEnumerable<TaskViewModel> PhaseTasks { get; set; }
    }
}