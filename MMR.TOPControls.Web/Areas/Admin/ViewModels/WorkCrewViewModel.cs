﻿using Envoc.Core.Shared.Extensions;
using MMR.TOPControls.Web.Helpers.DataTables.Attributes;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class WorkCrewViewModel : DomainViewModelBase
    {
        private string description = string.Empty;

        [Searchable][Sortable]
        public int CrewNumber { get; set; }

        [Searchable][Sortable]
        public string Description
        {
            get { return description; }
            set { description = value.ConvertNullToEmptyString(); }
        }
    }
}