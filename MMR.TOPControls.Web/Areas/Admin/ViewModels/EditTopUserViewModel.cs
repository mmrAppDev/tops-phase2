﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class EditTopUserViewModel : CreateTopUserViewModel
    {
        public IEnumerable<JobInfoViewModel> AvailableJobs { get; set; }
    }
}