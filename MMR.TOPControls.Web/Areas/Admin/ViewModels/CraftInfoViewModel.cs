﻿using System.ComponentModel;
using MMR.TOPControls.Web.Helpers.DataTables.Attributes;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class CraftInfoViewModel : DomainViewModelBase
    {
        [Searchable]
        [Sortable]
        [DisplayName("Craft Code")]
        public string CraftCode { get; set; }

        [Searchable]
        [Sortable]
        public string Description { get; set; }

    }
}