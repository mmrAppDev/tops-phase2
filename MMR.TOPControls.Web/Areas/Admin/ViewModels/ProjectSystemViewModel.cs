﻿using System.Collections.Generic;
using System.ComponentModel;
using Envoc.Core.Shared.Extensions;
using MMR.TOPControls.Web.Helpers.DataTables.Attributes;
using MMR.TOPControls.Web.ViewModels;

namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class ProjectSystemViewModel : DomainViewModelBase
    {
        private string description = string.Empty;
        private string systemNumber = string.Empty;

        [Searchable]
        [Sortable]
        [DisplayName("System Number")]
        public string SystemNumber
        {
            get { return systemNumber; }
            set { systemNumber = value.ConvertNullToEmptyString(); }
        }

        [Searchable]
        [Sortable]
        public string Description
        {
            get { return description; }
            set { description = value.ConvertNullToEmptyString(); }
        }

        public long? ParentId { get; set; }

        public string ParentName { get; set; }

        public int SortOrder { get; set; }

        public IEnumerable<ProjectSystemViewModel> Children { get; set; }
    }
}