﻿using System.ComponentModel;
using MMR.TOPControls.Web.Helpers.DataTables.Attributes;

namespace MMR.TOPControls.Web.Areas.Admin.ViewModels
{
    public class JobInfoViewModel
    {
        [Searchable, Sortable]
        [DisplayName("Title")]
        public string Title { get; set; }

        [DisplayName("UDP Name 1")]
        public string UDPName1 { get; set; }

        [DisplayName("UDP Name 2")]
        public string UDPName2 { get; set; }

        [DisplayName("UDP Name 3")]
        public string UDPName3 { get; set; }

        public long ID { get; set; }

        public bool CanDelete { get; set; }

        public bool ShowEarnedToClient { get; set; }

        public bool WeightedCalculations { get; set; }

        public bool HistoricalScopeCurve { get; set; }

    }
}