//using System;
//using System.Web.Mvc;
//using AutoMapper;
//using Envoc.Core.Shared.Data;
//using Envoc.Core.Shared.Security;
//using Envoc.Core.Shared.Security.Validators;
//using MMR.TOPControls.Web.Helpers;
//using MMR.TOPControls.Web.ViewModels.User;

//namespace MMR.TOPControls.Web.Controllers
//{
//    public class UserController : BaseController
//    {
//        private readonly UserService<User> service;
//        private readonly UserValidator<User> userValidator;
//        private readonly ForgotPasswordService forgotPasswordService;
//        private readonly IRepository<ForgotPasswordToken> forgotPasswordTokenrepository;
//        private readonly IReadOnlyRepository<User> userRepository;

//        public UserController(UserService<User> service, UserValidator<User> userValidator,
//                              ForgotPasswordService forgotPasswordService,
//                              IRepository<ForgotPasswordToken> forgotPasswordTokenrepository,
//                              IReadOnlyRepository<User> userRepository)
//        {
//            this.service = service;
//            this.userValidator = userValidator;
//            this.forgotPasswordService = forgotPasswordService;
//            this.forgotPasswordTokenrepository = forgotPasswordTokenrepository;
//            this.userRepository = userRepository;
//        }

//        [Authorize]
//        public ActionResult ChangePassword()
//        {
//            var viewModel = new ChangePasswordViewModel();
//            return PartialView(viewModel);
//        }

//        [HttpPost]
//        public ActionResult ChangePassword(ChangePasswordViewModel viewModel)
//        {
//            var validator = new ChangePasswordViewModelValidator();
//            var result = validator.Validate(viewModel);

//            if (result.IsValid)
//            {
//                var changePasswordResult =
//                    service.ChangePassword(new UserResetPasswordStrategy<User>(CurrentUser.Email,
//                                                                               viewModel.OldPassword,
//                                                                               viewModel.NewPassword));

//                if (changePasswordResult.Success)
//                {
//                    return Json(new {success = true});
//                }

//                ModelState.AddModelError("OldPassword", "The Old Password given does not match our records.");
//            }

//            foreach (var validationFailure in result.Errors)
//            {
//                ModelState.AddModelError(validationFailure.PropertyName, validationFailure.ErrorMessage);
//            }

//            return PartialView(viewModel);
//        }

//        [Authorize]
//        public ActionResult ChangeExpiredPassword()
//        {
//            var viewModel = new ChangePasswordViewModel();
//            return View(viewModel);
//        }

//        [HttpPost]
//        public ActionResult ChangeExpiredPassword(ChangePasswordViewModel viewModel)
//        {
//            if (service.ChangePassword(CurrentUser.EmailAddress, viewModel.OldPassword, viewModel.NewPassword))
//            {
//                return RedirectToAction("Index", "Home");
//            }

//            ModelState.AddModelError("OldPassword", "The Old Password given does not match our records.");

//            return View(viewModel);
//        }

//        public ActionResult ForgotPassword()
//        {
//            return View(new ForgotPasswordTokenRequestViewModel());
//        }

//        [HttpPost]
//        public ActionResult ForgotPassword(ForgotPasswordTokenRequestViewModel viewModel)
//        {
//            var token = forgotPasswordService.RequestToken(viewModel.EmailAddress);
//            token.SetValidator(new ForgotPasswordTokenValidator());

//            if (token.PasswordResetStatus == PasswordResetStatus.TokenIssued)
//            {
//                if (PerformAction(() => forgotPasswordService.Insert(token)))
//                {
//                    try
//                    {
//                        SendPasswordResetConfirmationEmailToUser(token);
//                        return RedirectToAction("Login", "Authentication");
//                    }
//                    catch (Exception)
//                    {
//                        forgotPasswordService.Delete(token);
//                        viewModel.Message = "There was a problem sending the confirmation email.";
//                        return View(viewModel);
//                    }
//                }
//            }

//            viewModel.Message = GetAuthenticationMessage(token.PasswordResetStatus);

//            return View(viewModel);
//        }

//        public ActionResult PasswordResetConfirmation(long tokenId)
//        {
//            var viewModel = new ChangeForgottenPasswordViewModel {TokenId = tokenId};
//            return View(viewModel);
//        }

//        [HttpPost]
//        public ActionResult PasswordResetConfirmation(ChangeForgottenPasswordViewModel viewModel)
//        {
//            var validator = new ChangeForgottenPasswordViewModelValidator();
//            var result = validator.Validate(viewModel);

//            if (result.IsValid)
//            {
//                var changePasswordResult =
//                    service.ChangePassword(new ForgotPasswordStrategy<User>(forgotPasswordTokenrepository,
//                                                                            userRepository,
//                                                                            viewModel.EmailAddress,
//                                                                            viewModel.TokenId,
//                                                                            viewModel.NewPassword));

//                if (changePasswordResult.Success)
//                {
//                    var user = service.SelectOne(x => x.EmailAddress.Equals(viewModel.EmailAddress));
//                    return RedirectToAction("Login", "Authentication");
//                }

//                viewModel.Message = GetAuthenticationMessage(changePasswordResult.Status);
//            }

//            return View(viewModel);
//        }

//        private string GetAuthenticationMessage(PasswordResetStatus status)
//        {
//            switch (status)
//            {
//                case PasswordResetStatus.TokenHasExpired:
//                    return
//                        "The password reset token has expired. Return to the login page click the Forgot Password link.";
//                case PasswordResetStatus.TokenNotFound:
//                    return "The token Id provided does not match the given account.";
//                case PasswordResetStatus.UsernameNotProvided:
//                    return "You must provide a valid email address.";
//                case PasswordResetStatus.UsernameNotFound:
//                    return "That email address does not match our records.";
//                case PasswordResetStatus.AccountInactive:
//                    return "This account has been deactivated by the administrator.";
//                default:
//                    return string.Empty;
//            }
//        }

//        private void SendPasswordResetConfirmationEmailToUser(ForgotPasswordToken token)
//        {
//            var emailConfig = new EmailSenderSettings
//                {
//                    ToAddress = token.User.EmailAddress,
//                    Subject = "Password Reset Confirmation",
//                    Message =
//                        "Click the link below to complete the password reset process.<break>Note: If you have not made a request to reset your password, ignore this message.<break>"
//                };

//            var url = Url.Action("PasswordResetConfirmation", "User", new {tokenId = token.TokenId},
//                                 Request.Url.Scheme);

//            emailConfig.Message += string.Format("<a href='{0}'>Reset Password<a/>", url);

//            var emailSender = new EmailSender(emailConfig);
//            emailSender.Send();
//        }

//        private User ConvertViewModelToModel(UserViewModel viewModel)
//        {
//            return Mapper.Map<User>(viewModel);
//        }

//        private UserViewModel ConvertModelToViewModel(User model)
//        {
//            return Mapper.Map<UserViewModel>(model);
//        }
//    }
//}