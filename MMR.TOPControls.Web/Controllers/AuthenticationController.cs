using System.Web.Mvc;
using System.Linq;
using AutoMapper;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using Lib.TOPControls.Provider;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;
using MMR.TOPControls.Web.Security;
using MMR.TOPControls.Web.ViewModels;
using Lib.TOPControls.Exceptions;
using System;

namespace MMR.TOPControls.Web.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly LocalDataMembershipProvider service;
        private readonly TOPControlLibrary dataService;
        private readonly IFormsAuthenticationService authenticationService;

        public AuthenticationController(IFormsAuthenticationService authenticationService)
        {
            service = new LocalDataMembershipProvider();
            dataService = new TOPControlLibrary(new TOPControlSQLDAL());
            this.authenticationService = authenticationService;
        }

        public ActionResult Login()
        {
            if (IsNotStarted())
            {
                return RedirectToAction("Startup");
            }
            return View(new LoginViewModel());
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel viewModel)
        {
            var validated = service.ValidateUser(viewModel.EmailAddress, viewModel.Password);

            if (validated)
            {
                var user = dataService.UserActions.FindByUsername(viewModel.EmailAddress);
                if (user != null && user.UserJobs.Count > 0)
                {
                    var jobID = (user.DefaultJobID.HasValue ? user.DefaultJobID.Value : user.UserJobs.First().JobID);
                    var currentJob = dataService.JobActions.Find(jobID);
                    var currentJobViewModel = Mapper.Map<JobInfoViewModel>(currentJob);

                    if (HttpContext.Session != null) HttpContext.Session["CurrentJob"] = currentJobViewModel;

                    authenticationService.Login(viewModel.EmailAddress);
                    return Redirect(authenticationService.GetRedirectUrl());
                }
                
                if (user != null)
                {
                    ModelState.AddModelError("EmailAddress", "User does not have access to a job.  A manager or administrator will need to add job access.");
                }
                else
                {
                    ModelState.AddModelError("EmailAddress", "User credentials validated, but user cannot be found.");
                }
            }

            viewModel.Message = "Error validating user credentials";
            return View(viewModel);
        }

        [Authorize]
        public ActionResult Logout()
        {
            authenticationService.Logout();
            return RedirectToAction("Login");
        }


        public ActionResult Startup()
        {
            if (!IsNotStarted())
            {
                return RedirectToAction("Login");
            }

            return View(new SystemSetupViewModel());
        }


        [HttpPost]
        public ActionResult Startup(SystemSetupViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                if (viewModel.AdminPassword == viewModel.ConfirmPassword)
                {
                    try
                    {
                        var sHelper = new Lib.TOPControls.Helpers.StartupHelper(dataService);
                        sHelper.StartupJob(viewModel.InitialJobTitle, viewModel.AdminPassword);
                        return RedirectToAction("Login");
                    }
                    catch (ValidationFailureException vfex)
                    {
                        var vr = vfex.FailedValidationResult;
                        foreach (var rv in vr.RuleViolations)
                        {
                            if (rv.PropertyName == "InitialJobTitle"
                                || rv.PropertyName == "AdminPassword")
                            {
                                ModelState.AddModelError(rv.PropertyName, rv.ErrorMessage);
                            }
                            else
                            {
                                ModelState.AddModelError("", rv.ErrorMessage);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("", ex.Message);
                    }
                }
                else
                {
                    ModelState.AddModelError("AdminPassword", "The two password values entered do not match.  Please try again.");
                }
            }
            return View(viewModel);
        }

        private bool IsNotStarted()
        {
            var allusers = dataService.UserActions.Search("");
            var alljobs = dataService.JobActions.Search("");

            return !allusers.Any() && !alljobs.Any();
        }

    }
}
