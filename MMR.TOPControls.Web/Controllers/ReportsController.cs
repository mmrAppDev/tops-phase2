﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using MMR.TOPControls.Web.wrefReports;

namespace MMR.TOPControls.Web.Controllers
{
    public class ReportsController : BaseController
    {
        //
        // GET: /Reports/

        public ActionResult Index()
        {
            return View();
        }

        

        public ActionResult Print(string name)
        {
            var svc = new ReportExecutionService();
            string historyID = null;
            var execHeader = new ExecutionHeader();
            svc.Credentials = System.Net.CredentialCache.DefaultCredentials;
            svc.ExecutionHeaderValue = execHeader;

            var execInfo = svc.LoadReport(GetReportPath(name), historyID);
            var parms = execInfo.Parameters;
            var pvals = GetParameterValues(parms);

            var format = "PDF";
            var sessionID = svc.ExecutionHeaderValue.ExecutionID;


            string extension;
            string mimetype;
            string encoding;
            Warning[] warnings;
            string[] streamids;


            byte[] filebytes;
            try
            {
                svc.SetExecutionParameters(pvals.ToArray(), "en-us");
                filebytes = svc.Render(format, null, out extension, out mimetype, out encoding, out warnings, out streamids);

                var result = new FileContentResult(filebytes, mimetype);
                result.FileDownloadName = makeFilenameFriendly(name) + makeFilenameFriendly(CurrentJob.Title) + ".pdf";
                return result;
            }
            catch (Exception ex)
            {
                throw ex; 
            }

            return null;

        }


        protected bool? GetURLValueAsBoolean(string valueName)
        {
            bool? result = null;
            var strVal = Request.QueryString[valueName];
            if (strVal != null)
            {
                bool temp;
                if (bool.TryParse(strVal, out temp))
                {
                    result = temp;
                }
            }
            return result;
        }

        protected string GetReportPath(string name)
        {
            return "/Reports/TOPControls/" + name;
        }

        protected List<ParameterValue> GetParameterValues(ReportParameter[] parms)
        {
            var pvals = new List<ParameterValue>();

            foreach (var x in parms)
            {
                ParameterValue pv = null;
                if (x.Name == "JobID")
                {
                    pv = GetParameterValueInstance(x, CurrentJob.ID);
                }
                else if (x.TypeSpecified && x.Type == ParameterTypeEnum.Boolean)
                {
                    var bvalUrl = GetURLValueAsBoolean(x.Name);
                    if (bvalUrl.HasValue)
                    {
                        pv = GetParameterValueInstance(x, bvalUrl.Value);
                    }
                }
                if (pv != null) pvals.Add(pv);
            }

            return pvals;
        }

        protected ParameterValue GetParameterValueInstance(ReportParameter rp, object value)
        {
            ParameterValue result = null;
            if (rp != null)
            {
                if (value != null || (rp.NullableSpecified && rp.Nullable))
                {
                    result = new ParameterValue { Name = rp.Name, Value = value.ToString() };
                }
                else if (rp.NullableSpecified && rp.Nullable)
                {
                    result = new ParameterValue { Name = rp.Name, Value = null };
                }
            }

            return result;
        }

        private string makeFilenameFriendly(string src)
        {
            var result = new StringBuilder();
            var filler = "_";
            var badChars = System.IO.Path.GetInvalidFileNameChars();

            foreach (var ch in src.ToCharArray())
            {
                if (badChars.Contains(ch))
                {
                    result.Append(filler);
                }
                else
                {
                    result.Append(ch);
                }
            }
            return result.ToString();

        }
    
    }
}
