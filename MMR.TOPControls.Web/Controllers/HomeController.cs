using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Envoc.Core.Shared.Extensions;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using Microsoft.Ajax.Utilities;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;
using MMR.TOPControls.Web.Helpers;
using MMR.TOPControls.Web.ViewModels;
using MMR.TOPControls.Web.ViewModels.ProgressFilter;
using MMRCommon.Data;
using Newtonsoft.Json;

namespace MMR.TOPControls.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly TOPControlLibrary service;

        public HomeController()
        {
            service = new TOPControlLibrary(new TOPControlSQLDAL());
        }

        public ActionResult Index()
        {
            if (User.IsInRole("TOPClient"))
            {
                return RedirectToAction("Index", "ProjectStatus");
            }

            var filter = new EnterProgressViewModel();

            if (CurrentJob.IsNotNull())
            {
                filter = GetDefaultFilter();

                AddUserDefinedValuesToViewModels(filter);
            }
            else
            {
                return RedirectToAction("Login", "Authentication");
            }

            return View(filter);
        }

        private EnterProgressViewModel GetDefaultFilter()
        {
            var filter = new EnterProgressViewModel
            {
                CommodityFilter = service.CommodityActions
                    .Search(CurrentJob.ID, "")
                    .OrderBy(x => x.CommodityCode)
                    .Select(
                        x =>
                            new SelectListItem
                            {
                                Text = string.Format("({0}) {1}", x.CommodityCode, x.Description),
                                Value = x.ID.ToString()
                            })
                    .ToList(),
                ObjectTypeFilter = service.ObjectTypeActions
                    .AllForJob(CurrentJob.ID)
                    .OrderBy(x => x.Name)
                    .Select(x => new SelectListItem {Text = x.Name, Value = x.ID.ToString()})
                    .ToList(),
                CraftInfoFilter = service.CraftActions
                    .Search(CurrentJob.ID, "")
                    .OrderBy(x => x.CraftCode)
                    .Select(x => new SelectListItem {Text = x.CraftCode, Value = x.ID.ToString()})
                    .ToList(),
                AreaFilter = service.AreaActions
                    .AllForJob(CurrentJob.ID)
                    .OrderBy(x => x.AreaNumber)
                    .Select(x => new SelectListItem {Text = x.AreaNumber, Value = x.ID.ToString()})
                    .ToList(),
                SystemFilter = service.SystemActions
                    .AllForJob(CurrentJob.ID)
                    .OrderBy(x => x.SystemNumber)
                    .Select(x => new SelectListItem {Text = x.SystemNumber, Value = x.ID.ToString()})
                    .ToList(),
                PhaseSelector = service.PhaseActions
                    .AllForJob(CurrentJob.ID)
                    .OrderBy(x => x.DisplayOrder)
                    .Select(x => new SelectListItem {Text = x.Title, Value = x.ID.ToString()})
                    .ToList(),
                WorkCrewSelector = service.WorkCrewActions
                    .AllForJob(CurrentJob.ID)
                    .OrderBy(x => x.CrewNumber)
                    .Select(x => new SelectListItem {Text = x.Description, Value = x.ID.ToString()})
                    .ToList()
            };

            return filter;
        }

        private void AddUserDefinedValuesToViewModels(EnterProgressViewModel filter)
        {
            var udpList = service.JobActions.GetUDPList(CurrentJob.ID);

            foreach (var udp in udpList)
            {
                IEnumerable<string> selectList = GetUserDefinedVals(udp.Key, CurrentJob.ID, new FilterParameters());
                filter.UdpList.Add(new UdpFilterItem
                {
                    Key = udp.Key,
                    Value = udp.Value,
                    UdpSelectList = selectList.Select(x => new SelectListItem {Text = x, Value = x}).ToList()
                });
            }
        }

        private IEnumerable<string> GetUserDefinedVals(int key, long jobId, FilterParameters filterParameters)
        {
            IEnumerable<string> selectList = null;
            switch (key)
            {
                case 1:
                    selectList = service.GetUserDefinedVals_1_ForFilter(jobId, filterParameters);
                    break;
                case 2:
                    selectList = service.GetUserDefinedVals_2_ForFilter(jobId, filterParameters);
                    break;
                case 3:
                    selectList = service.GetUserDefinedVals_3_ForFilter(jobId, filterParameters);
                    break;
            }
            return selectList;
        }

        public ActionResult ManageTasks()
        {
            var phases = new TaskPhaseViewModel()
            {
                TaskPhaseFilter = service.PhaseActions.AllForJob(CurrentJob.ID).Select(x => new SelectListItem
                {
                    Text = x.Title,
                    Value = x.ID.ToString()
                }).ToList()
            };

            return View("_TaskPhaseSelector", phases);
        }


        public ActionResult ChangeJob()
        {
            ChangeJobViewModel jobList = null;

            if (User.IsInRole("TOPAdmin"))
            {
                var allJobs = service.JobActions.Search("");

                jobList = new ChangeJobViewModel() 
                {
                    JobInfoFilter = allJobs
                        .OrderBy(job => job.Title)
                        .Select(
                            x => new SelectListItem 
                            {
                                Text = x.Title,
                                Value = x.ID.ToString(),
                                Selected = (x.ID == CurrentJob.ID)
                            })
                            .ToList()
                };
            }
            else
            {
                var userJobs = service.UserJobActions.AllForUser(CurrentUser.ID);

                jobList = new ChangeJobViewModel()
                {
                    JobInfoFilter = userJobs
                        .OrderBy(uj => uj.JobInfo.Title)
                        .Select(
                            x =>
                                new SelectListItem
                                {
                                    Text = x.JobInfo.Title,
                                    Value = x.JobID.ToString(),
                                    Selected = (x.JobID == CurrentJob.ID)
                                })
                        .ToList()
                };
            }

            return View("_ChangeJob", jobList);
        }

        [HttpPost]
        public ActionResult ChangeJob(FormCollection collection)
        {
            var jobId = int.Parse(collection["jobSelector"]);
            var job = service.JobActions.Find(jobId);

            CurrentJob = Mapper.Map<JobInfoViewModel>(job);

            return RedirectToAction("Index");
        }

        public ActionResult CurrentJobDisplay()
        {
            return View("_CurrentJob", CurrentJob);
        }

        [HttpPost]
        public ActionResult GetFilteredTags(FilterParamtersViewModel filter, DateTime workDate)
        {
            var filterParams = AddFilterParameters(filter);

            var query = service
                .FilterTaskProgress(CurrentJob.ID, filterParams);

            var skip = (filter.PageNum * filter.PageSize) - filter.PageSize;
            var take = filter.PageSize;

            var pagedFilterList = query
                .Skip(skip).Take(take)
                .Select(taskProgress => new FilterResultViewModel
                {
                    TrackedItemID = taskProgress.TrackedItemID,
                    PercentComplete = taskProgress.PercentComplete,
                    TrackingID = taskProgress.TrackedItem.TrackingID,
                    ID = taskProgress.ID,
                    Commodity = taskProgress.Commodity.CommodityCode,
                    ObjectType = taskProgress.TrackedItem.ObjectType.Name,
                    System = taskProgress.TrackedItem.ProjectSystem.SystemNumber,
                    Area = taskProgress.TrackedItem.ProjectArea.AreaNumber,
                    Craft = taskProgress.TrackedItem.CraftInfo.CraftCode,
                    Comments = taskProgress.TrackedItem.Comments,
                    TaskName = taskProgress.Task.Title,
                    TaskId = taskProgress.TaskID
                })
                .ToList()
                .Select(vm =>
                {
                    vm.PercentCompleteOnDate = service.TaskProgressActions.Find(vm.ID).PercentCompleteAsOf(workDate);
                    return vm;
                });

            var totalRecords = query.Count();
            var totalSearchProgress = service.CalculateTotalPercentCompleteForFilter(CurrentJob.ID, filterParams);

            var settings = new JsonSerializerSettings {ReferenceLoopHandling = ReferenceLoopHandling.Ignore};

            return new JsonNetResult()
            {
                Data =
                    new
                    {
                        TrackedItems = pagedFilterList,
                        TotalRecords = totalRecords,
                        TotalSearchProgress = totalSearchProgress.ToString("P2")
                    },
                SerializerSettings = settings
            };
        }

        [HttpPost]
        public ActionResult GetFilteredPercentComplete(FilterParamtersViewModel filter)
        {
            var filterParams = AddFilterParameters(filter);

            var totalSearchProgress = service.CalculateTotalPercentCompleteForFilter(CurrentJob.ID, filterParams);

            return new JsonNetResult {Data = new {TotalSearchProgress = totalSearchProgress.ToString("P2")}};
        }

        [HttpPost]
        public ActionResult SaveFilterProgress(UpdateProgressViewModel progressInfo)
        {
            var entry = service.UpdateProgress(CurrentJob.ID, progressInfo.TaskProgressID,
                progressInfo.NewPecentComplete, progressInfo.WorkDate, progressInfo.WorkCrewID, CurrentUser.ID);

            return Json(new {Success = true});
        }

        [HttpGet]
        public ActionResult GetTasksByPhase(long phaseID)
        {
            var filterParams = new FilterParameters();
            filterParams.AddPhaseParameter(phaseID);

            var tasks = service
                .TaskActions.AllForJob(CurrentJob.ID)
                .Where(t => t.PhaseID == phaseID)
                .OrderBy(to => to.DisplayOrder)
                .Select(task => new FilterTaskViewModel
                {
                    id = task.ID,
                    text = task.Title
                });

            return Json(new {Tasks = tasks}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel viewModel)
        {
            service.UserActions.SetUserPassword(CurrentUser.ID, viewModel.Password, CurrentUser.ID);

            return Json(new {success = true});
        }

        private FilterParameters AddFilterParameters(FilterParamtersViewModel filter)
        {
            var filterParams = new FilterParameters();
            filterParams.AddPhaseParameter(filter.PhaseId);

            if (!string.IsNullOrEmpty(filter.SearchText))
            {
                filterParams.AddSearchParameter(filter.SearchText);
            }

            foreach (var taskId in filter.Tasks)
                filterParams.AddTaskParameter(taskId);

            foreach (var commodityId in filter.Commodities)
                filterParams.AddCommodityParameter(commodityId);

            foreach (var objectTypeId in filter.ObjectTypes)
                filterParams.AddObjectTypeParameter(objectTypeId);

            if (filter.Systems.Any())
            {
                var branch = service.SystemActions.GetSystemBranch(CurrentJob.ID, filter.Systems.First());
                var systemIds = new List<long>();
                AddSystemsToFilter(branch, systemIds);

                filter.Systems = systemIds;
            }

            foreach (var systemId in filter.Systems)
                filterParams.AddProjectSystemParameter(systemId);

            foreach (var areaId in filter.Areas)
                filterParams.AddProjectAreaParameter(areaId);

            foreach (var craftInfoId in filter.CraftInfo)
                filterParams.AddCraftInfoParameter(craftInfoId);

            foreach (var udp1Val in filter.Udp1)
                filterParams.AddUDP_1_Parameter(udp1Val);

            foreach (var udp2Val in filter.Udp2)
                filterParams.AddUDP_2_Parameter(udp2Val);

            foreach (var udp3Val in filter.Udp3)
                filterParams.AddUDP_3_Parameter(udp3Val);

            return filterParams;
        }

        private void AddSystemsToFilter(ProjectSystem branch, List<long> systemIds)
        {
            systemIds.Add(branch.ID);

            foreach (var projectSystem in branch.Children)
            {
                AddSystemsToFilter(projectSystem, systemIds);
            }
        }

        [HttpPost]
        public ActionResult GetTrackedItem(int id, int taskId)
        {
            var model = service.GetQueryable<TrackedItem>().First(x => x.ID == id);
            return GetTrackedItemViewModel(model, taskId);
        }

        [HttpPost]
        public ActionResult GetNewTrackedItem()
        {
            var model = new TrackedItem();
            return GetTrackedItemViewModel(model, -1);
        }

        private ActionResult GetTrackedItemViewModel(TrackedItem model, int taskId)
        {
            var viewmodel = Mapper.Map<TrackedItem, TrackedItemViewModel>(model);
            var objectTypes = GetObjectTypeViewModels(CurrentJob.ID);
            var commodities = GetCommodityViewModels(CurrentJob.ID);
            var projectSystems = GetProjectSystemViewModels(CurrentJob.ID);
            var projectAreas = GetProjectAreaViewModels(CurrentJob.ID);
            var crafts = GetCraftInfoViewModels(CurrentJob.ID);
            viewmodel.TaskID = taskId;

            var canDelete = GetCanDeleteTrackedItem(model);

            if (!string.IsNullOrEmpty(CurrentJob.UDPName1))
            {
                viewmodel.UDPName1 = CurrentJob.UDPName1;
            }

            if (!string.IsNullOrEmpty(CurrentJob.UDPName2))
            {
                viewmodel.UDPName2 = CurrentJob.UDPName2;
            }

            if (!string.IsNullOrEmpty(CurrentJob.UDPName3))
            {
                viewmodel.UDPName3 = CurrentJob.UDPName3;
            }

            return
                Json(new {item = viewmodel, objectTypes, commodities, projectSystems, projectAreas, crafts, canDelete});
        }

        [HttpPost]
        [JsonModelBinder(ActionParameterName = "viewModel", ActionParameterType = typeof(TrackedItemViewModel))]
        public ActionResult UpdateTrackedItem(TrackedItemViewModel viewModel)
        {
            var model = service.GetQueryable<TrackedItem>().First(x => x.ID == viewModel.ID);
            model.JobID = CurrentJob.ID;

            Mapper.Map(viewModel, model);

            var result = service.TrackedItemActions.Validate(model);
            if (result.IsValid)
            {
                service.TrackedItemActions.Update(model, CurrentUser.ID);
                var canDelete = GetCanDeleteTrackedItem(model);

                return Json(new {success = true, canDelete});
            }

            return TrackedItemValidationFailure(result);
        }

        [HttpGet]
        public ActionResult GetCopyTrackedItem(long trackedItemId, long phaseId)
        {
            var model = service.GetQueryable<TrackedItem>().First(x => x.ID == trackedItemId);
            var taskIds = model.Tasks.Select(x => x.TaskID);
            var tasks =
                service.GetQueryable<Task>()
                    .Where(x => x.JobID == CurrentJob.ID && x.PhaseID == phaseId && !taskIds.Contains(x.ID)).ToList();

            return
                Json(
                    new
                    {
                        trackedItemId = model.TrackingID,
                        tasks = tasks.Select(x => new {TaskId = x.ID, TaskName = x.Title})
                    });
        }

        [HttpPost]
        public ActionResult CopyTrackedItem(long trackedItemId, long taskId)
        {
            var model = service.GetQueryable<TrackedItem>().First(x => x.ID == trackedItemId);
            var taskProgress = service.GetQueryable<TaskProgress>().First(x => x.TaskID == taskId);

            model.Tasks.Add(taskProgress);
            service.TrackedItemActions.Update(model, CurrentUser.ID);
            var canDelete = GetCanDeleteTrackedItem(model);

            return Json(new {success = true, canDelete});
        }

        [HttpPost]
        [JsonModelBinder(ActionParameterName = "viewModel", ActionParameterType = typeof(TrackedItemViewModel))]
        public ActionResult AddTrackedItem(TrackedItemViewModel viewModel)
        {
            var model = Mapper.Map<TrackedItemViewModel, TrackedItem>(viewModel);
            model.JobID = CurrentJob.ID;

            var result = service.TrackedItemActions.Validate(model);
            if (result.IsValid)
            {
                service.TrackedItemActions.Add(model, CurrentUser.ID);

                var task = service.TaskActions.Find(viewModel.TaskID);
                var taskProgress = new TaskProgress
                {
                    JobID = CurrentJob.ID,
                    TaskID = task.ID,
                    TrackedItemID = model.ID,
                    CommodityID = task.DefaultCommodityID
                };

                service.TaskProgressActions.Add(taskProgress, CurrentUser.ID);

                return Json(new {success = true});
            }

            return TrackedItemValidationFailure(result);
        }

        [HttpPost]
        public ActionResult DeleteTrackedItem(int trackedItemId)
        {
            service.TrackedItemActions.Delete(trackedItemId, CurrentUser.ID);

            return Json(new {success = true});
        }

        [HttpGet]
        public ActionResult GetTaskProgress(string trackingId, decimal taskId)
        {
            var taskProgress = service.TaskProgressActions.FindByTrackingID(CurrentJob.ID, trackingId);
            var entries =
                taskProgress.Where(p => p.TaskID == taskId).SelectMany(r => r.ProgressLogEntries.Select(x => new
                {
                    x.ID,
                    x.PhaseTitle,
                    x.DisplayName,
                    x.EntryDate,
                    x.EntryUserFullName,
                    x.OldPercentComplete,
                    x.NewPercentComplete,
                    x.TaskProgressID,
                    x.WorkDate,
                    x.TaskTitle
                }).OrderByDescending(p => p.WorkDate).ThenByDescending(p=>p.EntryDate).ToList());

            return new JsonNetResult {Data = new {Entries = entries}};
        }

        [HttpPost]
        public ActionResult DeleteProgressEntry(long progressEntryId, long taskProgressID)
        {
            service.ProgressLogEntryActions.Delete(progressEntryId, CurrentUser.ID);
            service.TaskProgressActions.SetProgressFromHistory(taskProgressID, CurrentUser.ID);
            
            return Json(new {success = true});
        }

        private ActionResult TrackedItemValidationFailure(ValidationResult result)
        {
            foreach (var error in result.RuleViolations)
            {
                ModelState.AddModelError(error.PropertyName, error.ErrorMessage);
            }

            return
                Json(
                    new
                    {
                        success = false,
                        errors = string.Join("<br/>", result.RuleViolations.Select(x => x.ErrorMessage))
                    });
        }

        private IQueryable<ObjectTypeViewModel> GetObjectTypeViewModels(long jobId)
        {
            var ObjectTypeCollection =
                service.ObjectTypeActions.AllForJob(jobId).AsQueryable().Project().To<ObjectTypeViewModel>();
            return ObjectTypeCollection.OrderBy(x => x.Name).AsQueryable();
        }

        private IQueryable<CommodityViewModel> GetCommodityViewModels(long jobId)
        {
            var CommodityCollection =
                service.CommodityActions.Search(jobId, "").AsQueryable().Project().To<CommodityViewModel>();
            return CommodityCollection.OrderBy(x => x.Description).AsQueryable();
        }

        private IQueryable<ProjectSystemViewModel> GetProjectSystemViewModels(long jobId)
        {
            var ProjectSystemCollection =
                service.SystemActions.Search(jobId, "").AsQueryable().Project().To<ProjectSystemViewModel>();
            return ProjectSystemCollection.OrderBy(x => x.SystemNumber).AsQueryable();
        }

        private IQueryable<ProjectAreaViewModel> GetProjectAreaViewModels(long jobId)
        {
            var ProjectAreaCollection =
                service.AreaActions.Search(jobId, "").AsQueryable().Project().To<ProjectAreaViewModel>();
            return ProjectAreaCollection.OrderBy(x => x.Description).AsQueryable();
        }

        private IQueryable<CraftInfoViewModel> GetCraftInfoViewModels(long jobId)
        {
            var craftInfoCollection =
                service.CraftActions.Search(jobId, "").AsQueryable().Project().To<CraftInfoViewModel>();
            return craftInfoCollection.OrderBy(x => x.CraftCode).AsQueryable();
        }

        private bool GetCanDeleteTrackedItem(TrackedItem model)
        {
            var filterParams = new FilterParameters();

            if (!model.TrackingID.IsNullOrWhiteSpace())
            {
                filterParams.AddTrackingIDParameter(model.TrackingID);
            }

            var query = service
                .FilterTaskProgress(CurrentJob.ID, filterParams);
            var progress = query.FirstOrDefault();
            var canDelete = progress.IsNotNull() && progress.PercentComplete == 0;

            return canDelete;
        }
    }
}