﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using Lib.TOPControls.DataImport;
using Lib.TOPControls.Interfaces;
using Lib.TOPControls.FileImporter.Version1;
using Lib.TOPControls.Exceptions;
using MMR.TOPControls.Web.ViewModels;
using System.Diagnostics;
using System.Text;

namespace MMR.TOPControls.Web.Controllers
{
    public class ImportController : BaseController
    {

        private readonly TOPControlLibrary service;

        public ImportController()
        {
            service = new TOPControlLibrary(new TOPControlSQLDAL());
        }

        //
        // GET: /Import/

        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Upload()
        {
            var file = Request.Files[0];
            var viewModel = new ImportResultsViewModel
                {
                    ImportSucceeded = false
                };

            if (file != null && file.ContentLength > 0)
            {

                var filename = Path.GetFileName(file.FileName);
                var path = Path.Combine(Path.GetTempPath(), filename);          
                if (System.IO.File.Exists(path)) System.IO.File.Delete(path);

                file.SaveAs(path);
                var fimp = new FileImporterService1(service);

                FileAnalysisResult evalfile = null;
                try
                {
                    evalfile = fimp.ExamineFile(path, "TaskImport");

                }
                catch (FileImportException fex)
                {
                    viewModel = new ImportResultsViewModel
                    {
                        ImportSucceeded = false
                    };
                    ViewBag.UploadFailure  = fex.Message + " \n " + fex.InnerException.ToString();
                    
                }
                if (evalfile != null)
                {
                    if (evalfile.CandidateRecordsets.Count() == 1 && evalfile.CandidateRecordsets.ElementAt(0).SchemaPoints > 2)
                    {
                        var crs = evalfile.CandidateRecordsets.ElementAt(0);
                        var imported = fimp.ReadImportTable(path, "TaskImport", crs.Name, CurrentUser.ID, CurrentJob.ID);
                        var fi = service.FileImportActions.Find(imported.RecordsetID);

                        var timporter = new TaskImporter(service, imported.RecordsetID);
                        try
                        {

                            var newItems = timporter.GetNewItems(new Dictionary<string, string>(), CurrentUser.ID);

                            ViewBag.UploadSuccess = string.Format("Uploaded and processed file {0}, recordset \"{1}\", assigned FileImport ID: {2}. Items successfuly imported: {3}", filename, imported.RecordsetName, imported.RecordsetID, newItems.Count());

                            viewModel = new ImportResultsViewModel
                            {
                                ImportSucceeded = true,
                                PhaseImported = timporter.PhaseImported,
                                TaskImported = timporter.TaskImported,
                                AreasAdded = timporter.AreasAdded,
                                CommoditiesAdded = timporter.CommoditiesAdded,
                                CraftsAdded = timporter.CraftsAdded,
                                ObjectTypesAdded = timporter.ObjectTypesAdded,
                                SystemsAdded = timporter.SystemsAdded,
                                TrackedItemsAdded = timporter.TrackedItemsAdded,
                                TrackedItemsRemoved = timporter.TrackedItemsRemoved
                            };
                        }
                        catch (FileImportException fex)
                        {
                            ViewBag.UploadFailure = String.Format("Problem encountered while importing the data from the file. The import returned the message: " + fex.Message);
                        }
                        catch (ValidationFailureException vfex)
                        {
                            ViewBag.UploadFailure = String.Format("One of the imported items failed validation: {0}", vfex.FailedValidationResult.RuleViolations.ElementAt(0).ErrorMessage);
                        }
                        
                    }
                    else
                    {
                        var bestScore = 0;
                        var bestPosition = -1;
                        foreach (var crs in evalfile.CandidateRecordsets)
                        {
                            if (crs.SchemaPoints > bestScore)
                            {
                                bestScore = crs.SchemaPoints;
                                bestPosition = crs.Position;
                            }
                        }
                        if (bestScore > 2)
                        {
                            var src = evalfile.CandidateRecordsets.FirstOrDefault(rs => rs.Position == bestPosition);
                        
                            var imported = fimp.ReadImportTable(path, "TaskImport", src.Name, CurrentUser.ID, CurrentJob.ID);
                            var fi = service.FileImportActions.Find(imported.RecordsetID);

                            var timporter = new TaskImporter(service, imported.RecordsetID);
                            try
                            {
                                var newItems = timporter.GetNewItems(new Dictionary<string, string>(), CurrentUser.ID);

                                ViewBag.UploadSuccess = string.Format("Uploaded and processed file {0}, recordset \"{1}\", assigned FileImport ID: {2}. Items successfuly imported: {3}", filename, imported.RecordsetName, imported.RecordsetID, newItems.Count());

                                viewModel = new ImportResultsViewModel
                                {
                                    ImportSucceeded = true,
                                    PhaseImported = timporter.PhaseImported,
                                    TaskImported = timporter.TaskImported,
                                    AreasAdded = timporter.AreasAdded,
                                    CommoditiesAdded = timporter.CommoditiesAdded,
                                    CraftsAdded = timporter.CraftsAdded,
                                    ObjectTypesAdded = timporter.ObjectTypesAdded,
                                    SystemsAdded = timporter.SystemsAdded,
                                    TrackedItemsAdded = timporter.TrackedItemsAdded,
                                    TrackedItemsRemoved = timporter.TrackedItemsRemoved
                                };
                            }
                            catch (FileImportException fex)
                            {
                                ViewBag.UploadFailure = String.Format("Problem encountered while importing the data from the file. The import returned the message: " + fex.Message);
                            }
                            catch (ValidationFailureException vfex)
                            {
                                ViewBag.UploadFailure = String.Format("One of the imported items failed validation: {0}", vfex.FailedValidationResult.RuleViolations.ElementAt(0).ErrorMessage);
                            }

                        }
                        else
                        {
                            ViewBag.UploadFailure = "None of the sheets in the uploaded file had enough data to import.";
                        }

                    }

                }
                System.IO.File.Delete(path);
                return View(viewModel);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult ExportAll()
        {
            var source = Server.MapPath(@"~\TaskImport.xlsx");
            var target = Server.MapPath(@"~\Files\Temp\Export.xlsx");
            var dt = DateTime.Now;
            target = target.Replace("Export.xlsx", string.Format("{0}_ExportAll_{1}{2}{3}{4}.xlsx",makeFilenameFriendly(CurrentJob.Title), dt.Year, dt.Month, dt.Day, dt.Millisecond));
            while (System.IO.File.Exists(target))
            {
                target = Server.MapPath(@"~\Files\Temp\Export.xlsx").Replace("Export.xlsx", string.Format("{0}_ExportAll_{1}{2}{3}.xlsx", makeFilenameFriendly(CurrentJob.Title), dt.Year, dt.Month, dt.Day, dt.Millisecond));
            }
            var fimp = new FileImporterService1(service);
            
            fimp.ExportJobData(CurrentJob.ID, target, source);

            return new RedirectResult("~/Files/Temp/" + Path.GetFileName(target));

        }

        private string makeFilenameFriendly(string src)
        {
            var result = new StringBuilder();
            var filler="_";
            var badChars = Path.GetInvalidFileNameChars();
            
            foreach(var ch in src.ToCharArray())
            {
                if (badChars.Contains(ch))
                {
                    result.Append(filler);
                }
                else
                {
                    result.Append(ch);
                }
            }
            return result.ToString();

        }
    }
}
