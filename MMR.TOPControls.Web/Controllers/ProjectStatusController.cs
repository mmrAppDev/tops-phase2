﻿using System;
using System.Linq;
using System.Web.Mvc;
using Envoc.Core.Shared.Extensions;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using Lib.TOPControls.Services;
using MMR.TOPControls.Web.Helpers;
using MMR.TOPControls.Web.ViewModels;
using MMR.TOPControls.Web.ViewModels.ProgressFilter;
using MMRCommon.Data;
using Newtonsoft.Json;

namespace MMR.TOPControls.Web.Controllers
{
    public class ProjectStatusController : BaseController
    {
        private readonly TOPControlLibrary library;
        private readonly ProjectSystemService service;

        public ProjectStatusController()
        {
            library = new TOPControlLibrary(new TOPControlSQLDAL());
            service = new ProjectSystemService(library);
        }

        public ActionResult Index()
        {
            var summaries = library.GetPhaseSummaries(CurrentJob.ID);

            var viewModel = new ProgressStatusViewModel
            {
                ShowEarnedToClient = ShowEarnedToClient,
                PhaseSelector = library.PhaseActions
                    .AllForJob(CurrentJob.ID)
                    .Where(ps => summaries.Select(s => s.PhaseID).Contains(ps.ID))
                    .OrderBy(x => x.DisplayOrder)
                    .Select(x => new SelectListItem {Text = x.Title, Value = x.ID.ToString()})
                    .ToList()
            };

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult GetJobPercentComplete()
        {
            decimal completedManhours = 0;
            decimal totalManhours = 0;
            decimal percentComplete = 0;

            var jobResult = library.GetGroupedCompletions(CurrentJob.ID, FilterValueType.None).FirstOrDefault();
            if (jobResult != null)
            {
                completedManhours = jobResult.CompletedManHours;
                totalManhours = jobResult.TotalManHours;
                percentComplete = jobResult.PercentComplete;
            }

            return
                Json(
                    new
                    {
                        PercentComplete = (percentComplete * 100).ToString("F1"),
                        CompletedManHours = completedManhours.ToString("F1"),
                        TotalManHours = totalManhours.ToString("F1")
                    }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetPhaseSummaries()
        {
            var summaries = library.GetPhaseSummaries(CurrentJob.ID);

            return Json(new {Phases = summaries}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetPhases()
        {
            var summaries = library.GetPhaseSummaries(CurrentJob.ID);

            var phases = library
                .PhaseActions.AllForJob(CurrentJob.ID)
                .Where(ps => summaries.Select(s => s.PhaseID).Contains(ps.ID))
                .Select(phase => new
                {
                    id = phase.ID,
                    title = phase.Title
                });

            return Json(new {Phases = phases}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetSystemList()
        {
            var systems = library.SystemActions.AllForJob(CurrentJob.ID)
                .Select(sys => new
                {
                    id = sys.ID,
                    title = sys.DisplayName
                });
            return Json(new {Systems = systems}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetTasksByPhase(long phaseID)
        {
            var filterParams = new FilterParameters();
            filterParams.AddPhaseParameter(phaseID);

            var tasks = library
                .GetTasksForFilter(CurrentJob.ID, filterParams)
                .Select(task => new FilterTaskViewModel
                {
                    id = task.ID,
                    text = task.Title
                });

            return Json(new {Tasks = tasks}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetJobCompleteChartData()
        {
            var historyChartViewModel =
                new HistoryChartViewModel(library.GetHistoryChartData(CurrentJob.ID, null, FilterValueType.None, null),
                    ShowEarnedToClient);
            return Json(historyChartViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetTasksChartData(long taskID)
        {
            var historyChartViewModel =
                new HistoryChartViewModel(
                    library.GetHistoryChartData(CurrentJob.ID, null, FilterValueType.Task, taskID),
                    ShowEarnedToClient);
            return Json(historyChartViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetPhaseChartData(long phaseID)
        {
            var historyChartViewModel =
                new HistoryChartViewModel(library.GetHistoryChartData(CurrentJob.ID, null, FilterValueType.Phase,
                    phaseID), ShowEarnedToClient);
            return Json(historyChartViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetSystemHistoryChartData(long systemID)
        {
            var historyChartViewModel =
                new HistoryChartViewModel(
                    library.GetHistoryChartData(CurrentJob.ID, null, FilterValueType.ProjectSystem,
                        systemID), ShowEarnedToClient);
            return Json(historyChartViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetGroupedCompletions(string grouping)
        {
            var typedGrouping = FilterValueType.ProjectSystem;
            Enum.TryParse<FilterValueType>(grouping, out typedGrouping);
            var groupedCompletions = library.GetGroupedCompletions(CurrentJob.ID, typedGrouping);
            var resultList = groupedCompletions
                .OrderBy(x => x.GroupTitle)
                .Select(gc => new NamedPercentage()
                {
                    NameOrTitle = gc.GroupTitle,
                    PercentComplete = gc.PercentComplete,
                    CompletedManHours = gc.CompletedManHours,
                    TotalManHours = gc.TotalManHours
                });
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetProjectSystemsProgress(long phaseId)
        {
            var tree = service.BuildProjectSystemProgressTree(CurrentJob.ID, phaseId);
            return new JsonNetResult
            {
                Data = new {success = true, message = string.Empty, data = tree},
                SerializerSettings = new JsonSerializerSettings {ReferenceLoopHandling = ReferenceLoopHandling.Ignore}
            };
        }

        [HttpGet]
        public ActionResult GetTrackedItemCompletionByPhase(long trackedItemId)
        {
            var trackedItem = library.TrackedItemActions.Find(trackedItemId);
            var param = new FilterParameters();
            param.AddTrackingIDParameter(trackedItem.TrackingID);
            var taskProgress = library.FilterTaskProgress(CurrentJob.ID, param);

            var groupedValues =
                taskProgress.GroupBy(g => g.Task.ProjectPhase.Title)
                    .Select(
                        x =>
                            new
                            {
                                Phase = x.Key, 
                                x.FirstOrDefault().Task.ProjectPhase.DisplayOrder,
                                PercentComplete = x.Average(p => p.PercentComplete),
                                Tasks = x.Select(v => new {Task = v.Task.Title, PercentComplete = v.PercentComplete})
                            });

            return new JsonNetResult
            {
                Data = new {success = true, message = string.Empty, data = groupedValues},
                SerializerSettings = new JsonSerializerSettings {ReferenceLoopHandling = ReferenceLoopHandling.Ignore}
            };
        }

        [HttpGet]
        public ActionResult UpdateAllData()
        {
            var ch = new Lib.TOPControls.Helpers.ChartingHelper(library);
            ch.UpdateAllCharts();
            return new EmptyResult();
        }

        private bool ShowEarnedToClient
        {
            get { return CurrentJob.ShowEarnedToClient || !User.IsInRole("TOPClient"); }
        }

        [HttpPost]
        public ActionResult UpdateTrackedItemComment(int id, string comments)
        {
            var model = library.GetQueryable<TrackedItem>().First(x => x.ID == id);
            model.JobID = CurrentJob.ID;
            model.Comments = comments;

            var result = library.TrackedItemActions.Validate(model);
            if (result.IsValid)
            {
                library.TrackedItemActions.Update(model, CurrentUser.ID);
                var filterParams = new FilterParameters();
                filterParams.AddTrackingIDParameter(model.TrackingID);
                var query = library
                    .FilterTaskProgress(CurrentJob.ID, filterParams);
                var progress = query.FirstOrDefault();
                var canDelete = progress.IsNotNull() && progress.PercentComplete == 0;

                return Json(new {success = true, canDelete});
            }

            return TrackedItemValidationFailure(result);
        }

        private ActionResult TrackedItemValidationFailure(ValidationResult result)
        {
            foreach (var error in result.RuleViolations)
            {
                ModelState.AddModelError(error.PropertyName, error.ErrorMessage);
            }

            return
                Json(
                    new
                    {
                        success = false,
                        errors = string.Join("<br/>", result.RuleViolations.Select(x => x.ErrorMessage))
                    });
        }
    }
}