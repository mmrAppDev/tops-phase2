using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using Lib.TOPControls.Exceptions;
using MMR.TOPControls.Web.Areas.Admin.ViewModels;
using MMR.TOPControls.Web.Security;
using MMRCommon.Data;

namespace MMR.TOPControls.Web.Controllers
{

    [Authorize]
    public class BaseController : Controller
    {
        public TOPUser CurrentUser { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            if (requestContext.HttpContext.Session == null || requestContext.HttpContext.Session["CurrentJob"] == null)
            {
                requestContext.HttpContext.Response.Clear();
                requestContext.HttpContext.Response.Redirect(Url.Action("Login", "Authentication", new { area = "" }));
                requestContext.HttpContext.Response.End();
            }
        }

        public JobInfoViewModel CurrentJob
        {
            get
            {
                return (JobInfoViewModel) HttpContext.Session["CurrentJob"];
            }
            set { HttpContext.Session["CurrentJob"] = value; }
        }

        protected bool PerformAction(Action action)
        {
            try
            {
                action();
                return true;
            }
            catch (ValidationException ex)
            {
                AddToModelError(ex.Errors);
            }
            catch (ValidationFailureException vfex)
            {
                AddToModelError(vfex.FailedValidationResult.RuleViolations);
            }
            catch 
            {
                throw;
            }
            
            return false;
        }

        protected PerformActionResult Act(Action action)
        {
            try
            {
                action();
                return new PerformActionResult(true, string.Empty);
            }
            catch (ValidationException ex)
            {
                StringBuilder sb = new StringBuilder();

                foreach (var error in ex.Errors)
                {
                    sb.Append(error.ErrorMessage + "<br/>");
                }

                return new PerformActionResult(false, sb.ToString());
            }
            catch (Exception ex)
            {
                return new PerformActionResult(false, ex.Message);
            }
        }

        protected internal new JsonResult Json(object data)
        {
            return Json(data, null, null, JsonRequestBehavior.AllowGet);
        }

        protected internal new JsonResult Json(object data, string contentType)
        {
            return Json(data, contentType, null, JsonRequestBehavior.AllowGet);
        }

        protected internal new virtual JsonResult Json(object data, string contentType, Encoding contentEncoding)
        {
            return Json(data, contentType, contentEncoding, JsonRequestBehavior.AllowGet);
        }

        protected internal void AddToModelError(IEnumerable<ValidationFailure> items)
        {
            foreach (var rv in items)
            {
                ModelState.AddModelError(rv.PropertyName, rv.ErrorMessage);
            }
        }

        protected internal void AddToModelError(IEnumerable<RuleViolation> items)
        {
            foreach (var rv in items)
            {
                ModelState.AddModelError(rv.PropertyName, rv.ErrorMessage);
            }
        }
    }

    public class PerformActionResult
    {
        public PerformActionResult(bool success, string message)
        {
            this.Success = success;
            this.Message = message;
        }

        public bool Success { get; private set; }
        public string Message { get; set; }
    }
}