﻿namespace MMR.TOPControls.Web.ViewModels
{
    public class ChangePasswordViewModel
    {
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}