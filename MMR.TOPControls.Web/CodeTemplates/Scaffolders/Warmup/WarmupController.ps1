[T4Scaffolding.Scaffolder(Description = "Create a wamup controller")][CmdletBinding()]
param(        
	[parameter(Mandatory = $true, ValueFromPipelineByPropertyName = $true)][string]$ModelName,
	[string]$ControllerName,   
    [string]$Project,
	[string]$CodeLanguage,
	[string]$Area,
	[string]$ViewScaffolder = "View",
	[alias("MasterPage")]$Layout,
	[alias("ContentPlaceholderIDs")][string[]]$SectionNames,
	[alias("PrimaryContentPlaceholderID")][string]$PrimarySectionName,
	[switch]$ReferenceScriptLibraries = $false,
	[switch]$NoChildItems = $true,
	[string[]]$TemplateFolders,
	[switch]$Force = $false
)

$ControllerName = $ModelName + "Controller"

$outputPath = "Controllers\" + $ModelName + "Controller"  # The filename extension will be added based on the template's <#@ Output Extension="..." #> directive

if ($Area) 
{
	$outputPath = "Areas\" + $Area + "\" + $outputPath
}

$namespace = (Get-Project $Project).Properties.Item("DefaultNamespace").Value
$foundModelType = Get-ProjectType $ModelName -Project $Project -ErrorAction SilentlyContinue
$overwriteFilesExceptController = $Force -and ((!$ForceMode) -or ($ForceMode -eq "PreserveController"))

Add-ProjectItemViaTemplate $outputPath -Template WarmupControllerTemplate `
	-Model @{ ModelName = $ModelName; Namespace = $namespace; Project = $Project } `
	-SuccessMessage "Added WarmupController output at {0}" `
	-TemplateFolders $TemplateFolders -Project $Project -CodeLanguage $CodeLanguage -Force:$Force