[T4Scaffolding.Scaffolder(Description = "Add List, Create, Edit modal ready forms")][CmdletBinding()]
param(        
	[parameter(Mandatory = $true, ValueFromPipelineByPropertyName = $true)][string]$ModelName,
    [string]$Project,
	[string]$CodeLanguage,
	[string[]]$TemplateFolders,
	[switch]$Force = $true
)

@("List", "Create", "Edit", "Delete", "_ListItems") | %{
	Scaffold ModalView -ModelName $ModelName -ViewName $_
}

Scaffold JavaScriptView -ModelName $ModelName