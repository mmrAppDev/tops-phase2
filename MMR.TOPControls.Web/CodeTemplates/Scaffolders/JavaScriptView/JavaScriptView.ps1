[T4Scaffolding.Scaffolder(Description = "Enter a description of JavaScriptView here")][CmdletBinding()]
param( 
	[parameter(Mandatory = $true, ValueFromPipelineByPropertyName = $true)][string]$ModelName,
    [string]$Project,
	[string]$CodeLanguage,
	[string[]]$TemplateFolders,
	[switch]$Force = $true
)

$viewModel = $ModelName + "ViewModel"
$ModelName = $ModelName.Substring(0, 1).ToLower() + $ModelName.Substring(1)

$projectName = $Project.Replace(".Web", "")
$projectName = $projectName.SubString($projectName.IndexOf(".") + 1)
$projectName = $projectName.Substring(0, 1).ToLower() + $projectName.Substring(1)

$outputPath = "Assets\js\" + $projectName + "." + $ModelName  # The filename extension will be added based on the template's <#@ Output Extension="..." #> directive

if ($Area) 
{
	$outputPath = "Assets\js\" +  $Area + "\" + $projectName + "." + $ModelName  # The filename extension will be added based on the template's <#@ Output Extension="..." #> directive
}

$namespace = (Get-Project $Project).Properties.Item("DefaultNamespace").Value

# Ensure we have a controller name, plus a model type if specified
if ($ModelName) {
	$foundModelType = Get-ProjectType $viewModel -Project $Project
	if (!$foundModelType) { return }
	$primaryKeyName = [string](Get-PrimaryKey $foundModelType.FullName -Project $Project)
}

if ($foundModelType) { $relatedEntities = [Array](Get-RelatedEntities $foundModelType.FullName -Project $project) }
if (!$relatedEntities) { $relatedEntities = @() }

Add-ProjectItemViaTemplate $outputPath -Template JavaScriptView `
	-Model @{ 
		Namespace = $namespace; 
		ModelName = $ModelName;
		Project = $projectName;
		ViewModelDataType = [MarshalByRefObject]$foundModelType;
		ViewModelDataTypeName = $foundModelType.FullName;
		PrimaryKeyName = $primaryKeyName;
		RelatedEntities = $relatedEntities;
		} `
	-SuccessMessage "Added View output at {0}" `
	-TemplateFolders $TemplateFolders -Project $Project -CodeLanguage $CodeLanguage -Force: $Force