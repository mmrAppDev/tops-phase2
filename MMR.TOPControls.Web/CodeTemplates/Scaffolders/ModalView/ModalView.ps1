[T4Scaffolding.Scaffolder(Description = "Enter a description of ModalView here")][CmdletBinding()]
param(        
	[parameter(Mandatory = $true, ValueFromPipelineByPropertyName = $true)][string]$ModelName,
	[parameter(Mandatory = $true, ValueFromPipelineByPropertyName = $true)][string]$ViewName,
    [string]$Project,
	[string]$CodeLanguage,
	[string[]]$TemplateFolders,
	[switch]$Force = $true
)

$controllerName = $ModelName + "Controller"

$outputPath = "Views\" + $ModelName + "\" + $ViewName # The filename extension will be added based on the template's <#@ Output Extension="..." #> directive

if ($Area) 
{
	$outputPath = "Areas\" + $Area + "\" + $outputPath
}

$namespace = (Get-Project $Project).Properties.Item("DefaultNamespace").Value
$viewModel = $ModelName + "ViewModel"

$projectName = $Project.Replace(".Web", "").SubString($Project.IndexOf(".") + 1)
$javascriptProjectName = $projectName.Substring(0, 1).ToLower() + $projectName.Substring(1)
$projectName = $projectName.ToLower()

# Ensure we have a controller name, plus a model type if specified
if ($ModelName) {
	$foundModelType = Get-ProjectType $viewModel -Project $Project
	if (!$foundModelType) { return }
	$primaryKeyName = [string](Get-PrimaryKey $foundModelType.FullName -Project $Project)
}

if ($foundModelType) { $relatedEntities = [Array](Get-RelatedEntities $foundModelType.FullName -Project $project) }
if (!$relatedEntities) { $relatedEntities = @() }

Add-ProjectItemViaTemplate $outputPath -Template $ViewName `
	-Model @{ 
		Namespace = $namespace; 
		ViewModel = $viewModel; 
		ModelName = $ModelName;
		ViewName = $ViewName; 
		Project = $projectName;
        JavascriptProject = $javascriptProjectName;
		ViewModelDataType = [MarshalByRefObject]$foundModelType;
		ViewModelDataTypeName = $foundModelType.FullName;
		PrimaryKeyName = $primaryKeyName;
		RelatedEntities = $relatedEntities;
		} `
	-SuccessMessage "Added View output at {0}" `
	-TemplateFolders $TemplateFolders -Project $Project -CodeLanguage $CodeLanguage -Force: $Force