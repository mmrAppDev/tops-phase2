﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MMR.TOPControls.Web.ViewModels
{
    public class UdpFilterItem
    {
        public int Key { get; set; }

        public string Value { get; set; }

        public List<SelectListItem> UdpSelectList { get; set; }
    }
}