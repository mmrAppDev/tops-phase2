﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MMR.TOPControls.Web.ViewModels
{
    public class ImportResultsViewModel
    {
      
        public bool ImportSucceeded { get; set; }


        public List<string> CraftsAdded { get; set; }

        public List<string> CommoditiesAdded { get; set; }

        public List<string> ObjectTypesAdded { get; set; }

        public List<string> AreasAdded { get; set; }

        public List<string> SystemsAdded { get; set; }

        public List<string> TrackedItemsAdded { get; set; }

        public List<string> TrackedItemsRemoved { get; set; }

        public string PhaseImported { get; set; }

        public string TaskImported { get; set; }

        public int LinesRead { get; set; }

        public ImportResultsViewModel()
        {
            ImportSucceeded = false;
            CraftsAdded = new List<string>();
            CommoditiesAdded = new List<string>();
            ObjectTypesAdded = new List<string>();
            AreasAdded = new List<string>();
            SystemsAdded = new List<string>();
            TrackedItemsAdded = new List<string>();
            TrackedItemsRemoved = new List<string>();
            PhaseImported = "N/A";
            TaskImported = "N/A";
            LinesRead = 0;
        }
    }
}