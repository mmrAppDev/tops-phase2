using System.Collections.Generic;

namespace MMR.TOPControls.Web.ViewModels.ProgressFilter
{
    public class FilterParamtersViewModel
    {
        public int PageNum { get; set; }

        public int PageSize { get; set; }

        public long PhaseId { get; set; }

        private IEnumerable<long> tasks = new List<long>();
        private IEnumerable<long> commodities = new List<long>();
        private IEnumerable<long> objectTypes = new List<long>();
        private IEnumerable<long> systems = new List<long>();
        private IEnumerable<long> areas = new List<long>();
        private IEnumerable<long> craftInfo = new List<long>();
        private IEnumerable<string> udp1 = new List<string>();
        private IEnumerable<string> udp2 = new List<string>();
        private IEnumerable<string> udp3 = new List<string>();

        public IEnumerable<long> Tasks
        {
            get { return tasks; }
            set { tasks = value; }
        }

        public IEnumerable<long> Commodities
        {
            get { return commodities; }
            set { commodities = value; }
        }

        public IEnumerable<long> ObjectTypes
        {
            get { return objectTypes; }
            set { objectTypes = value; }
        }

        public IEnumerable<long> Systems
        {
            get { return systems; }
            set { systems = value; }
        }

        public IEnumerable<long> Areas
        {
            get { return areas; }
            set { areas = value; }
        }

        public IEnumerable<long> CraftInfo
        {
            get { return craftInfo; }
            set { craftInfo = value; }
        }

        public IEnumerable<string> Udp1
        {
            get { return udp1; }
            set { udp1 = value; }
        }

        public IEnumerable<string> Udp2
        {
            get { return udp2; }
            set { udp2 = value; }
        }

        public IEnumerable<string> Udp3
        {
            get { return udp3; }
            set { udp3 = value; }
        }

        public string SearchText { get; set; }
    }
}