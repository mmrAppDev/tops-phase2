namespace MMR.TOPControls.Web.ViewModels.ProgressFilter
{
    public class FilterTaskViewModel
    {
        public long id { get; set; }
        public string text { get; set; }
    }
}