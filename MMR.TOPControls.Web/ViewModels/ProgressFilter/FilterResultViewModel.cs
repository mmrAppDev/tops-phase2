namespace MMR.TOPControls.Web.ViewModels.ProgressFilter
{
    public class FilterResultViewModel
    {
        private decimal percentComplete;
        private decimal percentCompleteOnDate;

        public long TrackedItemID { get; set; }

        public decimal PercentComplete
        {
            get
            {
                var convertedVal = (percentComplete.Equals(0.99M)) ? 1M : percentComplete;
                return convertedVal;
            }
            set { percentComplete = value; }
        }

        public decimal PercentCompleteOnDate
        {
            get
            {
                var convertedVal = (percentCompleteOnDate.Equals(0.99M)) ? 1M : percentCompleteOnDate;
                return convertedVal;
            }
            set { percentCompleteOnDate = value; }
        }

        public string TrackingID { get; set; }

        public long ID { get; set; }

        public string Commodity { get; set; }

        public string ObjectType { get; set; }

        public string System { get; set; }

        public string Area { get; set; }

        public string Craft { get; set; }

        public string Comments { get; set; }

        public long TaskId { get; set; }

        public string TaskName { get; set; }
    }
}