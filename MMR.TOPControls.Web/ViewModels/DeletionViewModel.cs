﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MMRCommon.Data;

namespace MMR.TOPControls.Web.ViewModels
{
    public class DeletionViewModel<VMType>:DomainViewModelBase 
        where VMType:DomainViewModelBase
    {
        /// <summary>
        /// The ViewModel that this DeletionViewModel represents.
        /// </summary>
        public VMType SourceModel { get; set; }

        public DeletionViewModel(VMType targetViewModel)
        {
            SourceModel = targetViewModel;
            Messages = new List<RuleViolation>();
        }

        public DeletionViewModel(VMType targetViewModel, ValidationResult validation)
            : this(targetViewModel)
        {
            CanDelete = validation.IsValid;
            Messages = validation.RuleViolations;
        }

        public bool CanDelete { get; set; }

        public IEnumerable<RuleViolation> Messages { get; set; }

        public override long ID
        {
            get
            {
                return SourceModel.ID;
            }
            set
            {
                SourceModel.ID = value;
            }
        }

        public override long JobID
        {
            get
            {
                return SourceModel.JobID;
            }
            set
            {
                SourceModel.JobID = value;
            }
        }


        
    }
}