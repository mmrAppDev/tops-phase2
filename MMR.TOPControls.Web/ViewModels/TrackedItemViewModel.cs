﻿namespace MMR.TOPControls.Web.ViewModels
{
    public class TrackedItemViewModel : DomainViewModelBase
    {
        public string TrackingID { get; set; }

        public long? ObjectTypeID { get; set; }

        public long? SystemID { get; set; }

        public long? AreaID { get; set; }

        public long? CraftID { get; set; }

        public string UDPName1 { get; set; }

        public string UDPName2 { get; set; }

        public string UDPName3 { get; set; }

        public string UDPValue1 { get; set; }

        public string UDPValue2 { get; set; }

        public string UDPValue3 { get; set; }

        public bool IsDeleted { get; private set; }

        public long TaskID { get; set; }

        public string Comments { get; set; }
    }
}