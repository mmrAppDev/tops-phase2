﻿using System.ComponentModel;

namespace MMR.TOPControls.Web.ViewModels
{
    public class DomainViewModelBase
    {
        [DisplayName("Job ID")]
        public virtual long JobID { get; set; }
        public virtual long ID { get; set; }
    }
}