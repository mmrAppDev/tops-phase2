using System;

namespace MMR.TOPControls.Web.ViewModels
{
    public class UpdateProgressViewModel
    {
        private decimal newPecentComplete;
        public long TaskProgressID { get; set; }
        public decimal NewPecentComplete
        {
            get { return (newPecentComplete >= 0.99M) ? 1M : newPecentComplete; }
            set { newPecentComplete = value; }
        }

        public DateTime WorkDate { get; set; }
        public long WorkCrewID { get; set; }
    }
}