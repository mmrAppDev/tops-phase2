﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MMR.TOPControls.Web.ViewModels
{
    public class ChangeJobViewModel
    {
        public List<SelectListItem> JobInfoFilter { get; set; }
    }
}