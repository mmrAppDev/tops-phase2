using FluentValidation;

namespace MMR.TOPControls.Web.ViewModels.User
{
    public class ForgotPasswordTokenRequestViewModelValidator : AbstractValidator<ForgotPasswordTokenRequestViewModel>
    {
        public ForgotPasswordTokenRequestViewModelValidator()
        {
            RuleFor(x => x.EmailAddress)
                .NotEmpty()
                .EmailAddress()
                .Length(0, 256);
        }
    }
}