using System.ComponentModel;

namespace MMR.TOPControls.Web.ViewModels.User
{
    public class ForgotPasswordTokenRequestViewModel
    {
        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        public string Message { get; set; }
    }
}