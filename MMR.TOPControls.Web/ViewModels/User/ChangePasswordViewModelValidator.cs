using FluentValidation;

namespace MMR.TOPControls.Web.ViewModels.User
{
    public class ChangePasswordViewModelValidator : AbstractValidator<ChangePasswordViewModel>
    {
        public ChangePasswordViewModelValidator()
        {
            RuleFor(x => x.OldPassword)
                .NotEmpty();

            RuleFor(x => x.NewPassword)
                .NotEmpty()
                .Length(0, 255)
                .Equal(x => x.ConfirmPassword)
                .WithMessage("New Password and Confirm Password must match.");

            RuleFor(x => x.ConfirmPassword)
                .NotEmpty()
                .Length(0, 255)
                .Equal(x => x.NewPassword)
                .WithMessage("New Password and Confirm Password must match.");
        }
    }
}