using System.ComponentModel;

namespace MMR.TOPControls.Web.ViewModels.User
{
    public class ChangeForgottenPasswordViewModel
    {
        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        [DisplayName("Token Id")]
        public long TokenId { get; set; }

        [DisplayName("New Password")]
        public string NewPassword { get; set; }

        [DisplayName("Confirm Password")]
        public string ConfirmPassword { get; set; }

        public string Message { get; set; }
    }
}