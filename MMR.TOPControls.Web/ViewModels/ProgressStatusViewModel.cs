﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MMR.TOPControls.Web.ViewModels
{
    public class ProgressStatusViewModel
    {
        public bool ShowEarnedToClient { get; set; }

        public List<SelectListItem> PhaseSelector { get; set; }
    }
}