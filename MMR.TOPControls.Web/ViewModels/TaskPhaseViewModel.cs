using System.Collections.Generic;
using System.Web.Mvc;

namespace MMR.TOPControls.Web.ViewModels
{
    public class TaskPhaseViewModel
    {
        public List<SelectListItem> TaskPhaseFilter { get; set; }
    }
}