﻿using System;
using System.Collections.Generic;
using Lib.TOPControls.Charting;

namespace MMR.TOPControls.Web.ViewModels
{
    public class HistoryChartViewModel
    {
        public long ChartDefinitionID { get; set; }

        public HistoryChartViewModel(HistoryChartData src, bool showEarnedToClient)
        {
            ChartDefinitionID = src.ChartDefinitionID;
            CompleteCount = src.CompleteCount;
            ContextTitle = src.ContextTitle;
            DisplayName = src.DisplayName;
            GroupKey = src.GroupKey;
            GroupTitle = src.GroupTitle;
            ID = src.ID;
            ItemCount = src.ItemCount;
            JobID = src.JobID;
            PercentComplete = src.PercentComplete;
            RunDate = src.RunDate;
            TotalManhours = Math.Round(src.TotalManHours, 2);
            CompletedManhours = Math.Round(src.CompletedManHours, 2);
            ShowEarnedToClient = showEarnedToClient;

            ChartValues = new List<HistoryChartDataPoint>();
            foreach (var dp in src.HistoryChartDataPoints)
            {
                ChartValues.Add(new HistoryChartDataPoint
                {
                    CompletedManHours = dp.CompletedManHours,
                    PercentComplete = dp.PercentComplete,
                    TotalManHours = dp.TotalManHours,
                    WorkDate = dp.WorkDate
                });
            }
        }

        public int CompleteCount { get; set; }

        public string ContextTitle { get; set; }

        public List<HistoryChartDataPoint> ChartValues { get; set; }

        public string DisplayName { get; set; }

        public string GroupKey { get; set; }

        public string GroupTitle { get; set; }

        public long ID { get; set; }

        public int ItemCount { get; set; }

        public long JobID { get; set; }

        public decimal PercentComplete { get; set; }

        public decimal TotalManhours { get; set; }

        public decimal CompletedManhours { get; set; }

        public bool ShowEarnedToClient { get; set; }

        public DateTime RunDate { get; set; }
    }
}