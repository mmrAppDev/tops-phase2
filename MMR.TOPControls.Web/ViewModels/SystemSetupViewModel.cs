﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MMR.TOPControls.Web.ViewModels
{

    public class SystemSetupViewModel
    {
        [DisplayName("Administrator Password")]
        [Required(AllowEmptyStrings=false, ErrorMessage="An Administrator Password must be entered to continue.")]
        [MinLength(6, ErrorMessage = "The Adminsitrator Password must contain at least six characters.")]
        public string AdminPassword { get; set; }

        [DisplayName("Confirm Password")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "The confirmation password must be entered.")]
        [MinLength(6, ErrorMessage = "The Adminsitrator Password must contain at least six characters.")]
        public string ConfirmPassword { get; set; }        

        [DisplayName("Job Display Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "A Job Name must be entered to continue.")]
        [MaxLength(100, ErrorMessage = "Maximum Job Name length exceeded.")]
        [MinLength(4, ErrorMessage="Job Name must be at least 4 characters long.")]
        public string InitialJobTitle { get; set; }

    }
}