﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MMR.TOPControls.Web.ViewModels
{
    public class EnterProgressViewModel
    {
        private List<UdpFilterItem> udpList = new List<UdpFilterItem>();

        public List<SelectListItem> CommodityFilter { get; set; }
        public List<SelectListItem> ObjectTypeFilter { get; set; }
        public List<SelectListItem> CraftInfoFilter { get; set; }
        public List<SelectListItem> SystemFilter { get; set; }
        public List<SelectListItem> AreaFilter { get; set; }

        public List<SelectListItem> WorkCrewSelector { get; set; }
        public List<SelectListItem> PhaseSelector { get; set; }

        public List<UdpFilterItem> UdpList
        {
            get { return udpList; }
            set { udpList = value; }
        }
    }
}