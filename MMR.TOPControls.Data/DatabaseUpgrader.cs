using System.Data.Entity.Migrations;

namespace MMR.TOPControls.Data
{
    public static class DatabaseUpgrader
    {
        public static void PerformUpgrade()
        {
            var config = new Migrations.Configuration();
            var migrator = new DbMigrator(config);
            migrator.Update();
        }
    }
}
