using System.Data.Entity;
using System.Linq;
using MMR.TOPControls.Models.Extensions;
using Envoc.Core.Shared.Data;
using Envoc.Core.Shared.Model;

namespace MMR.TOPControls.Data
{
    public class SqlRepository<T> : IRepository<T> where T : class, IIdentifiable
    {
        private readonly DataContext dataContext;
        private readonly DbSet<T> dbSet;

        public SqlRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
            dbSet = dataContext.Set<T>();
        }

        public IQueryable<T> Select()
        {
            return dbSet;
        }

        public void Save(T item)
        {
            if (item.IsNew())
            {
                dbSet.Add(item);
            }
            dataContext.SaveChanges();
        }

        public void Delete(T item)
        {
            dbSet.Remove(item);
            dataContext.SaveChanges();
        }
    }
}