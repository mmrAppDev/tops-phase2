using System.Data.Entity.ModelConfiguration;
using Envoc.Core.Shared.Security;

namespace MMR.TOPControls.Data.EntityTypeConfigurations
{
    public class ForgotPasswordTokenEntityTypeConfiguration : EntityTypeConfiguration<ForgotPasswordToken>
    {
        public ForgotPasswordTokenEntityTypeConfiguration()
        {
            Map(x => x.ToTable("ForgotPasswordToken"));

            Property(x => x.Id).HasColumnName("ForgotPasswordTokenId");

            Property(x => x.UserId).IsRequired();

            Property(x => x.ExpirationDate).IsRequired();

            Property(x => x.TokenId).IsRequired();
        }
    }
}