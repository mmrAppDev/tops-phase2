using System.Data.Entity.ModelConfiguration;
using Envoc.Core.Shared.Security;

namespace MMR.TOPControls.Data.EntityTypeConfigurations
{
    public class UserEntityTypeConfiguration : EntityTypeConfiguration<User>
    {
        public UserEntityTypeConfiguration()
        {
            Property(x => x.EmailAddress)
                .IsRequired()
                .HasMaxLength(256);

            Property(x => x.PasswordHash)
                .IsRequired();

            Property(x => x.PasswordSalt)
                .IsRequired();

            HasMany(x => x.Roles)
                .WithMany(x => x.Users)
                .Map(x =>
                    {
                        x.MapLeftKey("UserId");
                        x.MapRightKey("RoleId");
                        x.ToTable("UserRoles");
                    });
        }
    }
}
