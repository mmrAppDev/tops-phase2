using System.Data.Entity.ModelConfiguration;
using Envoc.Core.Shared.Security;

namespace MMR.TOPControls.Data.EntityTypeConfigurations
{
    public class RoleEntityTypeConfiguration : EntityTypeConfiguration<Role>
    {
        public RoleEntityTypeConfiguration()
        {
            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Description)
                .IsRequired();
        }
    }
}
