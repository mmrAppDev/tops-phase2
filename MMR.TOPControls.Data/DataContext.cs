using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using MMR.TOPControls.Common.Extensions;
using MMR.TOPControls.Data.EntityTypeConfigurations;

namespace MMR.TOPControls.Data
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
            IQueryableExtensions.QueryOptions = new DbQueryOptions();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new UserEntityTypeConfiguration())
                        .Add(new RoleEntityTypeConfiguration())
                        .Add(new ForgotPasswordTokenEntityTypeConfiguration());
        }
    }
}