using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using MMR.TOPControls.Common.Extensions;

namespace MMR.TOPControls.Data
{
    internal class DbQueryOptions : IQueryableExtensions.IQueryOptions
    {
        public IQueryable<T> Include<T, TProperty>(IQueryable<T> source, Expression<Func<T, TProperty>> path)
            where T : class
        {
            return DbExtensions.Include(source, path);
        }

        public IQueryable<T> AsNoTracking<T>(IQueryable<T> source) where T : class
        {
            return DbExtensions.AsNoTracking(source);
        }
    }
}
