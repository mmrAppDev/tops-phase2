using System.Data.Entity.Migrations;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace MMR.TOPControls.Data.Migrations
{
    public partial class AddElmah : DbMigration
    {
        public override void Up()
        {
            string script;
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("MMR.TOPControls.Data.Migrations.Elmah.SqlServer.sql"))
            using (var reader = new StreamReader(stream))
            {
                script = reader.ReadToEnd();
            }

            string[] sqlCommands = Regex.Split(script, @"\bGO\b");
            foreach (var sqlCommand in sqlCommands)
            {
                if (!string.IsNullOrWhiteSpace(sqlCommand))
                {
                    Sql(sqlCommand);
                }
            }
        }

        public override void Down()
        {
            DropTable("ELMAH_Error");
            Sql("DROP PROCEDURE ELMAH_GetErrorXml");
            Sql("DROP PROCEDURE ELMAH_GetErrorsXml");
            Sql("DROP PROCEDURE ELMAH_LogError");
        }
    }
}
