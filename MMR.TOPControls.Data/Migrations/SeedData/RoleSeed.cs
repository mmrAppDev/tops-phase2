using System.Data.Entity.Migrations;
using Envoc.Core.Shared.Security;

namespace MMR.TOPControls.Data.Migrations.SeedData
{
    public class RoleSeed
    {
        public void Seed(DataContext context)
        {
            context.Set<Role>().AddOrUpdate(x => x.Name, new Role
            {
                Name = "Admin",
                Description = "Has access to administrative functions of the website."
            });

            context.SaveChanges();
        }
    }
}
