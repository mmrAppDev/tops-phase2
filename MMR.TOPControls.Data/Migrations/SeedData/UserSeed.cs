using System.Linq;
using Envoc.Core.Shared.Security;

namespace MMR.TOPControls.Data.Migrations.SeedData
{
    public class UserSeed
    {
        public void Seed(DataContext context)
        {
            var users = context.Set<User>();
            if (!users.Any())
            {
                var admin = new User
                {
                    EmailAddress = "admin@envoc.com",
                    IsActive = true
                };
                admin.SetPassword("password");
                admin.Roles.Add(context.Set<Role>().FirstOrDefault(x => x.Name.Equals("Admin")));
                users.Add(admin);
            }

            context.SaveChanges();
        }
    }
}