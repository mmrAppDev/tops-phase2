using System.Data.Entity.Migrations;
using MMR.TOPControls.Data.Migrations.SeedData;

namespace MMR.TOPControls.Data.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataContext context)
        {
            new RoleSeed().Seed(context);
            new UserSeed().Seed(context);
        }
    }
}
