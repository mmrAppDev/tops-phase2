using Autofac;
using Envoc.Core.Shared.Data;

namespace MMR.TOPControls.Data.Configuration
{
    public class DataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DataContext>()
                .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(SqlRepository<>))
                .As(typeof(IRepository<>))
                .As(typeof(IReadOnlyRepository<>));
        }
    }
}
