﻿using System;
using System.Linq;
using Lib.TOPControls.DataAccess;
using Lib.TOPControls.Interfaces.DataAccess;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.DataAccess.Tests
{
    [TestClass]
    public class Task_IntegrationTests
    {

        protected ITOPControlDAL getTargetDAL()
        {
            return new TOPControlSQLDAL(@"Data Source=MMR-SQLDEV;Initial Catalog=TOPControlData;Integrated Security=True");
        }

        [TestMethod]
        public void SQLDAL_Task_GetJobItems()
        {
            var target = getTargetDAL();
            var tasks = target.TaskRep.All.Where(t => t.JobID == 1);

            Assert.AreNotEqual(0, tasks.Count());

            foreach (var item in tasks.ToList())
            {
                Assert.IsNotNull(item.JobInfo, "No job instance returned.");
                Assert.IsNotNull(item.ProjectPhase, "No phase instance returned.");
                
            }
        }

        [TestMethod]
        public void TaskData_FindTaskFromTrackedItem()
        {
            var target = getTargetDAL();

            var tpSrc = target.TaskProgressRep.All.FirstOrDefault(x => !x.IsDeleted && !x.Task.IsDeleted && !x.TrackedItem.IsDeleted);
            
            if (tpSrc == null) Assert.Inconclusive("Insufficient data to perform test.");

            var task = target.TaskRep.Find(tpSrc.TaskID);

            Assert.IsNotNull(task);

            var tempJob = target.JobInfoRep.Find(task.JobID);

            Assert.IsNotNull(tempJob, "Couldn't find the job {0}.", tempJob.Title);

            var trackedSet = target.TrackedItemRep.All.Where(ti => ti.JobID == tempJob.ID
                                                        && ti.Tasks.Count(tp => tp.TaskID == task.ID) > 0);

            Assert.AreNotEqual(0, trackedSet.Count(), "Couldn't find any tags that have that task.");

            var trackItem = trackedSet.FirstOrDefault();

            var taskProgressList = trackItem.Tasks;

            TaskProgress tempTaskProgress = null;

            foreach (var tp in taskProgressList)
            {
                if (tp.TaskID == task.ID)
                {
                    tempTaskProgress = tp;
                    break;
                }
            }

            Assert.AreEqual(task.ID, tempTaskProgress.Task.ID, "Could not navigate to the task from a related TrackedItem.");

        }



    }
}
