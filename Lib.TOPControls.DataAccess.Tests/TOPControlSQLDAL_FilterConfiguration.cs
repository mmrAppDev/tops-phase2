﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using Lib.TOPControls.Interfaces.DataAccess;
using MMRCommon.Data;

namespace Lib.TOPControls.DataAccess.Tests
{
    [TestClass]
    public class TOPControlSQLDAL_FilterConfiguration_Tests
    {
        string useConnectionString = @"Data Source=MMR-SQLDEV;Initial Catalog=TOPControlData;Integrated Security=True";

        protected ITOPControlDAL getTargetDAL()
        {
            return new TOPControlSQLDAL(useConnectionString);
        }

        [TestMethod]
        public void TOPC_SQLDAL_FilterConfigurations_CanRetrieveItems()
        {
            var target = getTargetDAL();

            var allFC = target.FilterConfigurationRep.All.Where(fc => fc.JobID == 1);

            foreach (var item in allFC)
            {
                Assert.IsTrue(item.ID > 0);
            }
        }

        [TestMethod]
        public void TOPC_SQLDEAL_FilterConfigurations_JobLoadedWithConfigs()
        {
            var target = getTargetDAL();

            var allFC = target.FilterConfigurationRep.All.Where(fc => fc.JobID == 1);

            var thisJob = target.JobInfoRep.Find(1);

            Assert.AreEqual(allFC.Count(), thisJob.FilterConfigurations.Count);

            foreach (var item in allFC)
            {
                Assert.IsNotNull(thisJob.FilterConfigurations.SingleOrDefault(fc => fc.ID == item.ID));
            }
        }

    }
}
