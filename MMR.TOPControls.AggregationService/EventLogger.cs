﻿using System;
using System.Diagnostics;

namespace MMR.TOPControls.AggregationService
{
    public class EventLogger
    {
        static string sSource;
        static string sLog;
        static bool bSourceSetup;

        static EventLogger()
        {
            sSource = "TOPControlDataAggregationService";
            sLog = "Application";
            bSourceSetup = false;
        }

        public static void EnsureSource()
        {
            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);
        }

        public static void LogEvent(string message, EventLogEntryType entryType = EventLogEntryType.Warning)
        {
            if (!bSourceSetup)
                EnsureSource();

            EventLog.WriteEntry(sSource, message, entryType);
        }

    }
}
