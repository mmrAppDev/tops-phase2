﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;

namespace MMR.TOPControls.AggregationService
{
    public partial class DataAggregationService : ServiceBase
    {
        public const int INT_MS_TO_MIN_RATIO = 60000;

        

        #region ThreadingAccomodations

        /// <summary>
        /// The AutoReset for the continuing data reset operation.
        /// </summary>
        protected static ManualResetEvent exit { get; set; }

        /// <summary>
        /// The WaitHandle for the service itself.
        /// </summary>
        protected static RegisteredWaitHandle h { get; set; }

        #endregion


        #region Settings Retrieval

        /// <summary>
        /// The Configured number of minutes between updates.
        /// </summary>
        protected static int MinutesBetweenUpdates
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["MinutesBetweenUpdates"]);
            }
        }


        protected static string LocalConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["TOPControlDataContext"].ConnectionString;
            }
        }

        #endregion


        public DataAggregationService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Perform the Settings Load operation.
            


            exit = new ManualResetEvent(false);
            setupTimer();
        }

        protected override void OnStop()
        {
            // TODO: Store the settings, as needed.

            exit.Set();
        }




        protected static void setupTimer()
        {
            exit.Reset();
            var msBetween = MinutesBetweenUpdates * INT_MS_TO_MIN_RATIO;
            h = ThreadPool.RegisterWaitForSingleObject(exit, new WaitOrTimerCallback(HistoryUpdateCheckpoint),
                                                                            null, msBetween, true);
        }


        protected static void HistoryUpdateCheckpoint(object state, bool timedOut)
        {
            
            PerformUpdates();
            
            if (timedOut)
            {
                setupTimer();
            }
        }


        protected static void PerformUpdates()
        {
            // fill with code referencing the update structure and performing updates.
            var service = new TOPControlLibrary(new TOPControlSQLDAL(LocalConnectionString));
            try
            {
                service.UpdateReportingData();
            }
            catch (Exception ex)
            {
                // TODO: Log the error.    
                try
                {
                    EventLogger.LogEvent(ex.ToString(), EventLogEntryType.Error);
                }
                catch
                {
                    throw ex;
                }
            }
            
        }


    }
}
