﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Lib.TOPControls.Exceptions;
using Lib.TOPControls.FileImporter.Version1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lib.TOPControls.FileImporter.Tests.Version1
{
    [TestClass]
    public class FIS_ExamineFile_Tests
    {
        private string getValidExcelFilename()
        {
            return @"C:\Ack\TOPTemp\Sample Tasks and IDs Project 5501TM.xlsx";
        }

        private string getValidCSVFilename()
        {
            return @"C:\Ack\TOPTemp\Sample Tasks and IDs Project 5501TM.csv";
        }

        private string getValidTextFilename()
        {
            return @"C:\Ack\TOPTemp\Sample Tasks and IDs Project 5501TM.txt";
        }

        private string getValidXmlFilename()
        {
            return @"C:\Ack\TOPTemp\BChecksheets5501_schema.xml";
        }


        private string getIncompleteExcelFilename()
        {
            return @"C:\Ack\TOPTemp\Sample Tasks and IDs Project 5501TM.xlsx";
        }

        private string taskSchemaName = "TaskImport";
        

        protected IFileImporterService1 GetTargetClass()
        {
            return new FileImporterService1();
        }

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void ExamineFile_ReturnsInstanceOfFileAnalysisResult()
        //{
        //    var target = GetTargetClass();

        //    var response = target.ExamineFile(getValidExcelFilename(), taskSchemaName);

        //    Assert.IsInstanceOfType(response, typeof(FileAnalysisResult));
        //}

        [TestMethod]
        [ExpectedException(typeof(FileImportException))]
        public void ExamineFile_FailsWhenFileNotFound()
        {
            var target = GetTargetClass();

            var badFilename = @"C:\zerba.txt";

            if (File.Exists(badFilename)) badFilename = @"C:\abrezNAHNAH.txt";

            if (File.Exists(badFilename)) Assert.Inconclusive("Test cannot complete until rewritten.");

            var response = target.ExamineFile(badFilename, taskSchemaName);
        }

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void ExamineFile_ReturnSameFileName()
        //{
        //    var target = GetTargetClass();
        //    var expectedFilename = getValidExcelFilename();
        //    var response = target.ExamineFile(expectedFilename, taskSchemaName);

        //    var actualfilename = response.Filename;

        //    Assert.AreEqual(expectedFilename, actualfilename);
        //}

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void ExamineFile_ReturnsADateTimeBetweenCallAndResponse()
        //{
        //    var target = GetTargetClass();
        //    var expectedFilename = getValidExcelFilename();
        //    var beforeExecute = DateTime.Now;

        //    var response = target.ExamineFile(expectedFilename, taskSchemaName);

        //    var afterExecute = DateTime.Now;

        //    Assert.IsTrue(beforeExecute <= response.DateAnalyzed, "DateAnalyzed is earlier than method call.");
        //    Assert.IsTrue(afterExecute >= response.DateAnalyzed, "DateAnalyzed is later than response.");
        //}

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void ExmaineFile_CorrectlyIdentifiesExcelFileTypeByName()
        //{
        //    var target = GetTargetClass();
        //    var expectedFilename = getValidExcelFilename();
        //    var expectedFileFormat = FileFormat.Excel;

        //    var response = target.ExamineFile(expectedFilename, taskSchemaName);

        //    var actualFileFormat = response.Format;

        //    Assert.AreEqual(expectedFileFormat, actualFileFormat);
        //}

        //// TODO: BC  Fix local file refrences in getValidCSVFilename()
        //[TestMethod]
        //public void ExamineFile_CorrectlyIdentifiesCSVFileByName()
        //{
        //    var target = GetTargetClass();
        //    var expectedFilename = getValidCSVFilename();
        //    var expectedFileFormat = FileFormat.CSV;

        //    var response = target.ExamineFile(expectedFilename, taskSchemaName);

        //    var actualFileFormat = response.Format;

        //    Assert.AreEqual(expectedFileFormat, actualFileFormat);
        //}

        //// TODO: BC  Fix local file refrences in getValidTextFilename()
        //[TestMethod]
        //public void ExamineFile_ReturnsUnknownFormatForTextOrXml()
        //{
        //    var target = GetTargetClass();
        //    var expectedFilename = getValidTextFilename();
        //    var expectedFileFormat = FileFormat.Unknown;

        //    var response = target.ExamineFile(expectedFilename, taskSchemaName);

        //    var actualFileFormat = response.Format;

        //    Assert.AreEqual(expectedFileFormat, actualFileFormat);

        //    expectedFilename = getValidXmlFilename();

        //    response = target.ExamineFile(expectedFilename, taskSchemaName);

        //    actualFileFormat = response.Format;

        //    Assert.AreEqual(expectedFileFormat, actualFileFormat);
        //}

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void ExamineFile_ReturnsSameSchemaNameForValidSchema()
        //{
        //    var target = GetTargetClass();
        //    var expectedSchema = taskSchemaName;

        //    var resopnse = target.ExamineFile(getValidExcelFilename(), expectedSchema);

        //    var actualSchema = resopnse.SchemaName;

        //    Assert.AreEqual(expectedSchema, actualSchema);
        //}

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void ExamineFile_ReturnsBlankSchemaNameForInvalidSchema()
        //{
        //    var target = GetTargetClass();
        //    var unexpectedSchema = "VartoBlaques";

        //    var resopnse = target.ExamineFile(getValidExcelFilename(), unexpectedSchema);

        //    var actualSchema = resopnse.SchemaName;

        //    Assert.AreNotEqual(unexpectedSchema, actualSchema);
        //}

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void ExamineFile_Excel_ReturnsCandidateRecordSets()
        //{
        //    var target = GetTargetClass();

        //    var response = target.ExamineFile(getValidExcelFilename(), taskSchemaName);

        //    Assert.IsNotNull(response.CandidateRecordsets);

        //}

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void ExamineFile_Excel_ReturnsCorrectNumberOfCandidateRecordSets()
        //{
        //    var target = GetTargetClass();
        //    var expectedCount = 3;
        //    var response = target.ExamineFile(getValidExcelFilename(), taskSchemaName);

        //    var actualCount = response.CandidateRecordsets.Count();
        //    Assert.AreEqual(expectedCount, actualCount);
        //}


        public void UTIL_AssertRSNamePresent(IEnumerable<RecordsetSummary> candidateRecordsets, string findName)
        {
            var rSummary = candidateRecordsets.SingleOrDefault(rs => rs.Name == findName);
            Assert.IsNotNull(rSummary);
        }

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void ExamineFile_Excel_ReturnsCorrectCandidateRecordSetNames()
        //{
        //    var target = GetTargetClass();
        //    var expectedName1 = "'B Checksheet$'";
        //    var expectedName2 = "'C Checksheet$'";
        //    var expectedName3 = "'Loop Check$'";

        //    var response = target.ExamineFile(getValidExcelFilename(), taskSchemaName);

        //    UTIL_AssertRSNamePresent(response.CandidateRecordsets, expectedName1);
        //    UTIL_AssertRSNamePresent(response.CandidateRecordsets, expectedName2);
        //    UTIL_AssertRSNamePresent(response.CandidateRecordsets, expectedName3);
            
        //}

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void ExamineFile_Excel_EachCandidateRecordsetHasUniquePosition()
        //{
        //    var target = GetTargetClass();
        //    var response = target.ExamineFile(getValidExcelFilename(), taskSchemaName);

        //    var foundPositions = new List<int>();

        //    foreach (var rs in response.CandidateRecordsets)
        //    {
        //        Assert.IsFalse(foundPositions.Contains(rs.Position), "ExamineFile returns two tables at position {0}.", rs.Position);
        //        foundPositions.Add(rs.Position);
        //    }
        //}

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void ExamineFile_Excel_EachCandidateRecordsetHasColumnsList()
        //{
        //    var target = GetTargetClass();
        //    var response = target.ExamineFile(getValidExcelFilename(), taskSchemaName);

        //    var foundPositions = new List<int>();

        //    foreach (var rs in response.CandidateRecordsets)
        //    {
        //        Assert.IsNotNull(rs.Columns, "No Column list for recordset named {0}.", rs.Name);
        //    }
        //}

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void ExamineFile_Excel_EachCandidateRecordHasCorrectRowcount()
        //{
        //    var target = GetTargetClass();
        //    var expectedName1 = "'B Checksheet$'";
        //    var expectedName2 = "'C Checksheet$'";
        //    var expectedName3 = "'Loop Check$'";

        //    var response = target.ExamineFile(getValidExcelFilename(), taskSchemaName);

        //    var expectedRowCount1 = 492;
        //    var expectedRowCount2 = 492;
        //    var expectedRowCount3 = 904;
            
        //    var rSummary = response.CandidateRecordsets.SingleOrDefault(rs => rs.Name == expectedName1);
        //    var actualRowCount = rSummary.RowCount;

        //    Assert.AreEqual(expectedRowCount1, actualRowCount, "Rowcount is off for {0}", rSummary.Name);

        //    rSummary = response.CandidateRecordsets.SingleOrDefault(rs => rs.Name == expectedName2);
        //    actualRowCount = rSummary.RowCount;
        //    Assert.AreEqual(expectedRowCount2, actualRowCount, "Rowcount is off for {0}", rSummary.Name);

        //    rSummary = response.CandidateRecordsets.SingleOrDefault(rs => rs.Name == expectedName3);
        //    actualRowCount = rSummary.RowCount;
        //    Assert.AreEqual(expectedRowCount3, actualRowCount, "Rowcount is off for {0}", rSummary.Name);
            
        //}

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void ExamineFile_Excel_EachCandidateRecordHasMissingRquiredColumns()
        //{
        //    var target = GetTargetClass();

        //    var response = target.ExamineFile(getIncompleteExcelFilename(), taskSchemaName);

        //    foreach (var rs in response.CandidateRecordsets)
        //    {
        //        Assert.IsNotNull(rs.MissingRequiredColumns, "RecordsetSummary does not have missing required column list.");

        //    }

        //}

        //// TODO: BC  Fix local file refrences in getIncompleteExcelFilename()
        //[TestMethod]
        //public void ExamineFile_Excel_EachCandidateRecordListsMissingRquiredColumns()
        //{
        //    var target = GetTargetClass();
        //    var expectedMissingColumn = "Tracking ID";

        //    var response = target.ExamineFile(getIncompleteExcelFilename(), taskSchemaName);

        //    foreach (var rs in response.CandidateRecordsets)
        //    {
        //        Assert.IsTrue(rs.MissingRequiredColumns.Contains(expectedMissingColumn), "Recordset does not show missinge column Tracking ID.");

        //    }

        //}

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void ExamineFile_Excel_EachCandidateRecordsetHas1SchemaPOintForEachMatchedColumname()
        //{
        //    var target = GetTargetClass();
        //    var expectedName1 = "'B Checksheet$'";
        //    var expectedName2 = "'C Checksheet$'";
        //    var expectedName3 = "'Loop Check$'";

        //    var response = target.ExamineFile(getValidExcelFilename(), taskSchemaName);

        //    var expectedPoints1 = 3;
        //    var expectedPoints2 = 3;
        //    var expectedPoints3 = 3;

        //    var rSummary = response.CandidateRecordsets.SingleOrDefault(rs => rs.Name == expectedName1);
        //    var actualPoints = rSummary.SchemaPoints;

        //    Assert.AreEqual(expectedPoints1, actualPoints, "Schema points are wrong for {0}", rSummary.Name);

        //    rSummary = response.CandidateRecordsets.SingleOrDefault(rs => rs.Name == expectedName2);
        //    actualPoints = rSummary.SchemaPoints;
        //    Assert.AreEqual(expectedPoints2, actualPoints, "Schema points are wrong for {0}", rSummary.Name);

        //    rSummary = response.CandidateRecordsets.SingleOrDefault(rs => rs.Name == expectedName3);
        //    actualPoints = rSummary.SchemaPoints;
        //    Assert.AreEqual(expectedPoints3, actualPoints, "Schema points are wrong for {0}", rSummary.Name);
        //}

    }
}
