﻿using System.Data;
using System.Linq;
using Lib.TOPControls.DataAccess;
using Lib.TOPControls.FileImporter.Version1;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;

namespace Lib.TOPControls.FileImporter.Tests.Version1
{
    [TestClass]
    public class FIS_ReadImportTable_Tests
    {
        private string getValidExcelFilename()
        {
            return @"C:\Ack\TOPTemp\Loop Check DeDuped Import 5501TM.xlsx";
        }

        protected ITOPControlLibrary GetLibraryRoot()
        {
            var constr = ConfigurationManager.ConnectionStrings["TOPControlDataContext"].ConnectionString;
            return new TOPControlLibrary(new TOPControlSQLDAL(constr));
        }

        protected IFileImporterService1 GetTargetClass()
        {
            var result = new FileImporterService1(GetLibraryRoot());

            return result;
        }

        private string taskSchemaName = "TaskImport";

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void FIS_ReadImportTable_CreatesNewFileImport()
        //{
        //    var target = GetTargetClass();

        //    var exRes = target.ExamineFile(getValidExcelFilename(), taskSchemaName);
        //    var root = GetLibraryRoot();
        //    var userID = 1;
        //    while (root.UserActions.Find(userID) == null)
        //    {
        //        userID++;
        //    }
        //    var jobID = 1;
        //    while (root.JobActions.Find(jobID) == null)
        //    {
        //        jobID++;
        //    }
        //    if (exRes.CandidateRecordsets.Count() > 0)
        //    {
        //        var idx = 0;
        //        var targetName = string.Empty;
        //        while (targetName == string.Empty && idx < exRes.CandidateRecordsets.Count())
        //        {
        //            var x = exRes.CandidateRecordsets.ElementAt(idx);
        //            if (x.SchemaPoints > 2) targetName = x.Name;
        //            idx++;
        //        }
        //        if (targetName == string.Empty) Assert.Inconclusive("Can't identify a workable recordset.");

        //        var readRes = target.ReadImportTable(exRes.Filename, exRes.SchemaName, targetName, userID, jobID);
        //        Assert.IsInstanceOfType(readRes, typeof(ImportedRecordset));
        //    }
        //}

        //// TODO: BC  Fix local file refrences in getValidExcelFilename()
        //[TestMethod]
        //public void FIS_ReadImportTable_CanRebuildDataset()
        //{
        //    var target = GetTargetClass();

        //    var exRes = target.ExamineFile(getValidExcelFilename(), taskSchemaName);

        //    var root = GetLibraryRoot();
        //    var userID = 1;
        //    while (root.UserActions.Find(userID) == null)
        //    {
        //        userID++;
        //    }
        //    var jobID = 1;
        //    while (root.JobActions.Find(jobID) == null)
        //    {
        //        jobID++;
        //    }

        //    long importID = 0;

        //    if (exRes.CandidateRecordsets.Count() > 0)
        //    {
        //        var idx = 0;
        //        var targetName = string.Empty;
        //        while (targetName == string.Empty && idx < exRes.CandidateRecordsets.Count())
        //        {
        //            var x = exRes.CandidateRecordsets.ElementAt(idx);
        //            if (x.SchemaPoints > 2) targetName = x.Name;
        //            idx++;
        //        }
        //        if (targetName == string.Empty) Assert.Inconclusive("Can't identify a workable recordset.");

        //        var readRes = target.ReadImportTable(exRes.Filename, exRes.SchemaName, targetName, userID, jobID);
        //        Assert.IsInstanceOfType(readRes, typeof(ImportedRecordset));
        //        importID = readRes.RecordsetID;

        //        target = null;

        //        var lib = GetLibraryRoot();

        //        var iFile = lib.FileImportActions.Find(importID);

        //        Assert.IsNotNull(iFile, "Could not retrieve the instance of the imported file.");

        //        var ds = iFile.GetAsDataSet();

        //        Assert.IsInstanceOfType(ds, typeof(DataSet));

        //        var tbl = ds.Tables[0];

        //        // check all columns are present.

        //        foreach (var c in readRes.Columns)
        //        {
        //            Assert.IsTrue(tbl.Columns.Contains(c.Name), "The column \"{0}\" is missing from the rebuilt table.", c.Name);
        //        }

        //        var expectedCount = readRes.RowCount;
        //        var actualCount = tbl.Rows.Count;

        //        Assert.AreEqual(expectedCount, actualCount, "The rowcount is off in the rebuilt data table.");
        //    }
        //}


        //// TODO: BC  Fix local file refrences
        //[TestMethod]
        //public void FIS_Export_AnythingAtAll()
        //{
        //    var target = GetTargetClass() as FileImporterService1;

        //    if (target != null)
        //    {
        //        var targetFilenameTemplate = @"C:\Ack\TestingExport{0}.xlsx";
        //        var idx = 0;
        //        var targetFilename = string.Format(targetFilenameTemplate, "");
        //        while (System.IO.File.Exists(targetFilename))
        //        {
        //            targetFilename = string.Format(targetFilenameTemplate, idx);
        //            idx++;
        //        }

        //        target.ExportJobData(3, targetFilename, @"C:\Ack\TOPTemp\TaskImport.xlsx");
        //    }
        //}

    }
}