﻿CREATE TABLE [dbo].[HistoryChartDataPoints]
(
	[ID] BIGINT NOT NULL IDENTITY PRIMARY KEY,
	[HistoryChartDataID] BIGINT NOT NULL REFERENCES HistoryChartData (ID) ON DELETE CASCADE,
	[WorkDate] DATETIME NOT NULL,
	[PercentComplete] DECIMAL(9,4) NOT NULL DEFAULT ((0)),
	[CompletedManHours] DECIMAL(9,4) NOT NULL DEFAULT ((0)),
	[TotalManHours] DECIMAL(9,4) NOT NULL DEFAULT ((0)), 
	[LastChanged] DATETIME NOT NULL DEFAULT (getdate())
)
