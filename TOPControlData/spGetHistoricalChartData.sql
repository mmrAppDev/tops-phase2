﻿CREATE PROCEDURE [dbo].[spGetHistoricalChartData]
	@JobID bigint
AS

SELECT JobInfo.Title As JobTitle
	, HCD.GroupTitle
	, HCD.JobID
	, HCD.CompleteCount
	, HCD.ItemCount
	, HCD.PercentComplete As FinalPercentComplete
	, PNTS.WorkDate
	, PNTS.PercentComplete
FROM ChartDefinitions CD
	Inner Join HistoryChartData HCD
		On HCD.ChartDefinitionID = CD.ID
	Inner Join JobInfo
		On JobInfo.ID = HCD.JobID
	Inner Join HistoryChartDataPoints PNTS
		On PNTS.HistoryChartDataID = HCD.ID
Where CD.ChartTypeValue = 1
	And CD.FilterValueTypeValue = 0
	And HCD.JobID = @JobID
Order By JobID, WorkDate
