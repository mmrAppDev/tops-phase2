﻿CREATE TABLE [dbo].[ProgressLogEntry]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [EntryDate] DATETIME NOT NULL DEFAULT GetDate(), 
    [WorkDate] DATETIME NULL, 
    [JobID] BIGINT NOT NULL, 
    [JobName] VARCHAR(100) NULL, 
    [UserID] BIGINT NOT NULL, 
    [EntryUsername] VARCHAR(50) NOT NULL DEFAULT '', 
    [EntryUserFirstName] VARCHAR(100) NULL, 
	[EntryUserLastName] VARCHAR(100) NULL,
    [CommodityID] BIGINT NULL, 
    [CommodityCode] VARCHAR(200) NULL, 
    [CommodityDescription] VARCHAR(200) NULL, 
    [TaskID] BIGINT NULL, 
    [TaskTitle] VARCHAR(100) NULL, 
	[TaskProgressID] BIGINT NULL,
    [TaskManHours] DECIMAL(9, 4) NULL DEFAULT 0, 
    [PhaseID] BIGINT NULL, 
    [PhaseTitle] VARCHAR(50) NULL, 
    [TrackedItemID] BIGINT NULL, 
    [TrackingID] VARCHAR(200) NULL, 
    [ObjectTypeID] BIGINT NULL, 
    [ObjectTypeName] VARCHAR(200) NULL, 
    [ProjectSystemID] BIGINT NULL, 
    [ProjectSystemNumber] VARCHAR(200) NULL, 
    [ProjectAreaID] BIGINT NULL, 
    [ProjectAreaNumber] VARCHAR(200) NULL, 
    [CraftInfoID] BIGINT NULL, 
    [CraftCode] VARCHAR(200) NULL, 
    [UDP1_Name] VARCHAR(50) NULL, 
    [UDP1_Value] VARCHAR(200) NULL, 
    [UDP2_Name] VARCHAR(50) NULL, 
    [UDP2_Value] VARCHAR(200) NULL, 
    [UDP3_Name] VARCHAR(50) NULL, 
    [UDP3_Value] VARCHAR(200) NULL, 
    [WorkCrewID] BIGINT NULL, 
    [WorkCrewNumber] INT NULL, 
    [WorkCrewDescription] VARCHAR(200) NULL, 
    [OldPercentComplete] DECIMAL(9, 2) NULL, 
    [NewPercentComplete] DECIMAL(9, 2) NULL, 
    [LastChanged] DATETIME NOT NULL DEFAULT GetDate()
)

GO

CREATE INDEX [IX_ProgressLogEntry_JobIDWorkDate] ON [dbo].[ProgressLogEntry] (JobID, WorkDate)


GO

CREATE INDEX [IX_ProgressLogEntry_TaskProgressID] ON [dbo].[ProgressLogEntry] (TaskProgressID)


GO





CREATE INDEX [IX_ProgressLogEntry_JobID_TaskProgressID_WorkDate] ON [dbo].[ProgressLogEntry] ([JobID], [TaskProgressID], [WorkDate])
