﻿CREATE TABLE [dbo].[GroupedCompletionDataPoints]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY,
	[GroupedCompletionChartID] BIGINT NOT NULL REFERENCES GroupedCompletionCharts (ID) ON DELETE CASCADE,
	[GroupKey] VARCHAR(200) NOT NULL DEFAULT (''),
	[GroupTitle] VARCHAR(200) NOT NULL DEFAULT (''),
	[PercentComplete] DECIMAL(9,4) NOT NULL DEFAULT ((0)),
	[CompletedManHours] DECIMAL(9,4) NOT NULL DEFAULT ((0)),
	[TotalManHours] DECIMAL(9,4) NOT NULL DEFAULT ((0)), 
	[ItemCount] INT NOT NULL DEFAULT ((0)),
	[LastChanged] DATETIME NOT NULL DEFAULT (getdate())
)
