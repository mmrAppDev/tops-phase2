﻿CREATE VIEW [dbo].[vwLastDailyRecordedTime]
	AS 
Select PLE.WorkDate, PLE.TaskProgressID, Max(PLE.EntryDate) AS LastEntryTime
from ProgressLogEntry PLE
Group By PLE.WorkDate, PLE.TaskProgressID

