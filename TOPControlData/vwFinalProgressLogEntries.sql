﻿CREATE VIEW [dbo].[vwFinalProgressLogEntries]
	AS 
	Select PLE.*
	From ProgressLogEntry PLE
		Inner Join vwLastDailyRecordedTime LRT
			On LRT.TaskProgressID = PLE.TaskProgressID 
				And LRT.WorkDate = PLE.WorkDate
				And LRT.LastEntryTime = PLE.EntryDate


