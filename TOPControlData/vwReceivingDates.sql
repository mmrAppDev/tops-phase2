﻿CREATE VIEW [dbo].[vwReceivingDates]
	AS SELECT DISTINCT ProgressLogEntry.JobID,
		ProgressLogEntry.WorkDate
	FROM ProgressLogEntry
