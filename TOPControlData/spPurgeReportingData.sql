﻿CREATE PROCEDURE [dbo].[spPurgeReportingData]
	@JobID bigint = 0

AS
	-- GroupedCompletionCharts:  DataPoints removed by cascading delete.
	Delete from [GroupedCompletionCharts]
	Where (@JobID = 0 OR [GroupedCompletionCharts].[JobID] = @JobID);

	-- HistoryChartData: DataPoints Removed by cascading delete.
	Delete from [HistoryChartData]
	Where (@JobID = 0 OR [HistoryChartData].[JobID] = @JobID);


RETURN 0
