﻿CREATE TABLE [dbo].[CraftInfo]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [JobID] BIGINT NOT NULL , 
    [CraftCode] VARCHAR(200) NOT NULL DEFAULT (''),
	[Description] VARCHAR(200) NOT NULL DEFAULT (''), 
    [LastChanged] DATETIME NOT NULL DEFAULT (getdate()), 
    CONSTRAINT [FK_CraftInfo_JobInfo] FOREIGN KEY ([JobID]) REFERENCES [JobInfo]([ID])
)
