﻿CREATE TABLE [dbo].[ProjectSystem]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [JobID] BIGINT NOT NULL , 
	[SystemNumber] varchar(200) NOT NULL DEFAULT '',
	[Description] VARCHAR(200) NOT NULL DEFAULT '', 
    [LastChanged] DATETIME NOT NULL DEFAULT GetDate(), 
    [ParentId] BIGINT NULL, 
    [SortOrder] INT NULL DEFAULT 0, 
    CONSTRAINT [FK_ProjectSystem_JobInfo] FOREIGN KEY ([JobID]) REFERENCES [JobInfo]([ID])
)
