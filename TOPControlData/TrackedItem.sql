﻿CREATE TABLE [dbo].[TrackedItem]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [JobID] BIGINT NOT NULL, 
    [TrackingID] VARCHAR(200) NOT NULL DEFAULT '', 
    [ObjectTypeID] BIGINT NULL, 
    [SystemID] BIGINT NULL, 
    [AreaID] BIGINT NULL, 
    [CraftID] BIGINT NULL, 
    [UDPValue1] VARCHAR(200) NULL, 
    [UDPValue2] VARCHAR(200) NULL, 
    [UDPValue3] VARCHAR(200) NULL, 
    [LastChanged] DATETIME NOT NULL DEFAULT GetDate(), 
    [IsDeleted] BIT NOT NULL DEFAULT 0, 
    [Comments] VARCHAR(500) NULL, 
    CONSTRAINT [FK_TrackedItem_JobInfo] FOREIGN KEY ([JobID]) REFERENCES [JobInfo]([ID]), 
    CONSTRAINT [FK_TrackedItem_ObjectType] FOREIGN KEY ([ObjectTypeID]) REFERENCES [ObjectType]([ID]) ON DELETE SET NULL, 
    CONSTRAINT [FK_TrackedItem_ProjectSystem] FOREIGN KEY ([SystemID]) REFERENCES [ProjectSystem]([ID]) ON DELETE SET NULL, 
    CONSTRAINT [FK_TrackedItem_ProjectArea] FOREIGN KEY ([AreaID]) REFERENCES [ProjectArea]([ID]) ON DELETE SET NULL, 
    CONSTRAINT [FK_TrackedItem_CraftInfo] FOREIGN KEY ([CraftID]) REFERENCES [CraftInfo]([ID]) ON DELETE SET NULL 
)

GO

CREATE UNIQUE INDEX [IX_TrackedItem_JobID_TrackingID] ON [dbo].[TrackedItem] ([JobID], [TrackingID])
