﻿CREATE TABLE [dbo].[Tasks]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [JobID] BIGINT NOT NULL, 
    [PhaseID] BIGINT NOT NULL, 
    [Title] VARCHAR(50) NOT NULL DEFAULT '', 
    [DisplayOrder] INT NOT NULL DEFAULT 1,
	[DefaultCommodityID] BIGINT NULL, 
    [LastChanged] DATETIME NOT NULL DEFAULT GetDate(), 
    [IsDeleted] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_Task_JobInfo] FOREIGN KEY ([JobID]) REFERENCES [JobInfo]([ID]),
	CONSTRAINT [FK_Task_ProjectPhase] FOREIGN Key ([PhaseID]) REFERENCES [ProjectPhase]([ID]) ON DELETE CASCADE
)

GO

CREATE UNIQUE INDEX [IX_Task_JobID_Title] ON [dbo].[Tasks] ([JobID], [PhaseID], [Title])
