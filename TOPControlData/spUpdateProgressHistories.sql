﻿CREATE PROCEDURE [dbo].[spUpdateProgressHistories]
	@JobID bigint,
	@WorkDate datetime = NULL
AS
	Delete From ProgressHistories 
	Where ProgressHistories.ID In 
		(
			Select PH.ID 
			From ProgressHistories PH
				Left Outer Join vwProgressHistory VPH
				On PH.TaskProgressID = VPH.TaskProgressID
					And PH.WorkDate = VPH.WorkDate
			Where VPH.JobID IS NULL
				And PH.JobID = @JobID
				And (@WorkDate IS NULL Or PH.WorkDate = @WorkDate)
		)

	Update ProgressHistories Set
		PCTD = VPH.PCTD,
		ManHours = VPH.ManHours,
		LastChanged = VPH.LastChanged
	From 
		ProgressHistories PH 
			Inner Join vwProgressHistory VPH
				On PH.TaskProgressID = VPH.TaskProgressID
					And PH.WorkDate = VPH.WorkDate
	Where VPH.JobID = @JobID
		And (@WorkDate IS NULL Or VPH.WorkDate = @WorkDate)
		And (PH.PCTD <> VPH.PCTD
				Or PH.ManHours <> VPH.ManHours
				Or PH.LastChanged <> VPH.LastChanged);

	Insert Into ProgressHistories 
		(JobID, TaskProgressID, WorkDate, PCTD, ManHours, LastChanged)
	Select 
		VPH.JobID, VPH.TaskProgressID, VPH.WorkDate, VPH.PCTD, VPH.ManHours, VPH.LastChanged
	From vwProgressHistory VPH
		Left Outer Join ProgressHistories PH
			On PH.TaskProgressID = VPH.TaskProgressID
				And PH.WorkDate = VPH.WorkDate
	Where PH.JobID is NULL
		And (@WorkDate IS NULL Or VPH.WorkDate = @WorkDate)
		And NOT VPH.WorkDate IS NULL
		And VPH.JobID = @JobID;

RETURN 0
