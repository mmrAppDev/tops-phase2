﻿CREATE TABLE [dbo].[JobInfo]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [Title] VARCHAR(100) NOT NULL DEFAULT (''), 
    [UDPName1] VARCHAR(50) NULL, 
    [UDPName2] VARCHAR(50) NULL, 
    [UDPName3] VARCHAR(50) NULL, 
	[ShowEarnedToClient] BIT NOT NULL DEFAULT ((0)),
	[WeightedCalculations] BIT NOT NULL DEFAULT ((0)),
	[HistoricalScopeCurve] BIT NOT NULL DEFAULT ((0)),
    [LastChanged] DATETIME NOT NULL DEFAULT (getdate()), 
    [IsDeleted] BIT NOT NULL DEFAULT ((0)), 
    CONSTRAINT [CK_JobInfo_Title] CHECK (len([Title]) > (3)) 
)

GO

CREATE UNIQUE INDEX [IX_JobInfo_Title] ON [dbo].[JobInfo] ([Title])
