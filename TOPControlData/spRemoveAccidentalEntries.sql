﻿CREATE PROCEDURE [dbo].[spRemoveAccidentalEntries]
AS

Delete From [dbo].[ProgressLogEntry] 
	Where [ProgressLogEntry].[ID] in 
		( Select FirstID As AccidentalID 
			from vwAccidentialProgressEntries 
		 Union 
			Select SecondID 
			from vwAccidentialProgressEntries);


RETURN 0
