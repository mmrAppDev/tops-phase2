﻿CREATE VIEW [dbo].[vwTaskProgressReporting]
	AS 
	SELECT TP.[ID]
		, TP.[JobID]
		, TP.[CommodityID]
		, TP.[TaskID]
		, TP.[TrackedItemID]
		, TP.[PercentComplete]
		, TP.[LastChanged]
		, TP.[IsDeleted]
		, ISNULL(CMH.[ManHoursPerItem], 0.0) AS [ManHoursPerItem]
		, CONVERT(bit, IIF(TP.[IsDeleted] = 1 OR TI.[IsDeleted] = 1 OR T.[IsDeleted] = 1, 1, 0)) As [IsInDeletedScope]
	FROM [dbo].[TaskProgress] TP
		LEFT JOIN [dbo].[vwCommodityManhours] CMH
			ON CMH.[ID] = TP.[CommodityID]
		INNER JOIN [dbo].[TrackedItem] TI
			ON TI.[ID] = TP.[TrackedItemID]
		INNER JOIN [dbo].[Tasks] T
			ON T.[ID] = TP.[TaskID]

