﻿CREATE TABLE [dbo].[GroupedCompletionCharts]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY,
	[JobID] BIGINT NOT NULL REFERENCES JobInfo (ID),
	[ChartDefinitionID] BIGINT NOT NULL REFERENCES ChartDefinitions (ID),
	[RunDate] DATETIME NOT NULL DEFAULT (getdate()),
	[LastChanged] DATETIME NOT NULL DEFAULT (getdate())
)
