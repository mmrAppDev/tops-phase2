﻿CREATE TABLE [dbo].[HistoryChartData]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY,
	[JobID] BIGINT NOT NULL REFERENCES JobInfo (ID),
	[ChartDefinitionID] BIGINT NOT NULL REFERENCES ChartDefinitions (ID),
	[GroupKey] varchar(200) NOT NULL DEFAULT (''),
	[GroupTitle] varchar(200) NOT NULL DEFAULT (''),
	[ContextTitle] varchar(200) NOT NULL DEFAULT (''),
	[RunDate] datetime NOT NULL DEFAULT (getdate()),
	[ItemCount] int NOT NULL DEFAULT ((0)),
	[CompleteCount] int NOT NULL DEFAULT ((0)),
	[PercentComplete] decimal(9,4) NOT NULL DEFAULT ((0)), 
	[CompletedManHours] DECIMAL(9,4) NOT NULL DEFAULT ((0)),
	[TotalManHours] DECIMAL(9,4) NOT NULL DEFAULT ((0)), 
	[LastChanged] datetime NOT NULL DEFAULT (getdate())
)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'A count of the relevent Tracked Items in the specified group.',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'HistoryChartData',
    @level2type = N'COLUMN',
    @level2name = N'ItemCount'
GO

CREATE INDEX [IX_HistoryChartData_JobChartGroupKeyID] ON [dbo].[HistoryChartData] ([JobID], [ChartDefinitionID], [GroupKey], [ID])
