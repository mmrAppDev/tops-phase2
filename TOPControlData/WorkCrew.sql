﻿CREATE TABLE [dbo].[WorkCrew]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [JobID] BIGINT NOT NULL, 
    [CrewNumber] INT NOT NULL DEFAULT 1, 
    [Description] VARCHAR(200) NOT NULL DEFAULT '', 
    [LastChanged] DATETIME NOT NULL DEFAULT GetDate(), 
    CONSTRAINT [FK_WorkCrew_JobInfo] FOREIGN KEY ([JobID]) REFERENCES [JobInfo]([ID]) ON DELETE CASCADE
)

GO

CREATE UNIQUE INDEX [IX_WorkCrew_JobID_CrewNumber] ON [dbo].[WorkCrew] ([JobID], [CrewNumber])
