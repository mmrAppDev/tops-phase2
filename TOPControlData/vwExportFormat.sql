﻿CREATE VIEW [dbo].[vwExportFormat]
	AS 
	Select [JI].[ID] As [JobID]
	, [JI].[Title] as [JobTitle]
	, [PP].[DisplayOrder] As [PhaseDisplayOrder]
	, [T].[DisplayOrder] As [TaskDisplayOrder]
	, [PP].[Title] as [Phase]
	, [T].[Title] as [Task]
	, [TI].[TrackingID] as [Tracking ID]
	, [TP].[PercentComplete]
	, IsNull(IsNull([PS3].[SystemNumber], [PS2].[SystemNumber]), ISNULL([PS1].[SystemNumber], '')) As [System]
	, IIF([PS3].[SystemNumber] IS NULL, IIF([PS2].[SystemNumber] IS NULL, '', IsNull([PS1].[SystemNumber], '')), IsNull([PS2].[SystemNumber], '')) As [Subsystem1]
	, IIF([PS3].[SystemNumber] IS NULL, '', IsNull([PS1].[SystemNumber], '')) As [Subsystem2]
	, IsNull([PA].[AreaNumber], '') as [Area]
	, IsNull([CI].[CraftCode], '') as [Craft]
	, IsNull([C].[CommodityCode], '') as [Commodity]
	, IsNull([OT].[Name], '') as [Object Type]
	, IsNull([TI].[UDPValue1], '') As [Custom1Value]
	, IsNull([TI].[UDPValue2], '') As [Custom2Value]
	, IsNull([TI].[UDPValue3], '') As [Custom3Value]
From TaskProgress TP
	Inner Join Tasks T
		on T.ID = TP.TaskID
	Inner Join ProjectPhase PP
		on PP.ID = T.PhaseID
	Left Outer Join Commodity C
		on C.ID = TP.CommodityID
	Inner Join TrackedItem TI
		on TI.ID = TP.TrackedItemID
	Inner Join JobInfo JI
		on TI.JobID = JI.ID
	Left Outer Join ProjectSystem PS1
		on TI.SystemID = PS1.ID
	Left Outer Join ProjectSystem PS2
		on PS1.ParentId = PS2.ID
	Left Outer Join ProjectSystem PS3
		on PS2.ParentId = PS3.ID
	Left Outer Join ProjectArea PA
		on TI.AreaID = PA.ID
	Left Outer Join CraftInfo CI
		on TI.CraftID = CI.ID
	Left Outer Join ObjectType OT
		on OT.ID = TI.ObjectTypeID
Where [TP].[IsDeleted] = 0
	And [TI].[IsDeleted] = 0
	And [T].[IsDeleted] = 0
	And [JI].[IsDeleted] = 0



