﻿CREATE PROCEDURE [dbo].[spGetExportData]
	@JobId bigint
AS
	
Select 
	[EF].[Phase]
	, [EF].[Task]
	, [EF].[Tracking ID]
	, [EF].[Area]
	, [EF].[Craft]
	, [EF].[Commodity]
	, [EF].[Object Type]
	, [EF].[System]
	, [EF].[Subsystem1]
	, [EF].[Subsystem2]
	, [EF].[PercentComplete]
	, [EF].[Custom1Value]
	, [EF].[Custom2Value]
	, [EF].[Custom3Value]
From [vwExportFormat] [EF]
Where [EF].[JobID] = @JobId
Order by [JobTitle], [PhaseDisplayOrder], [TaskDisplayOrder], [Tracking ID]
