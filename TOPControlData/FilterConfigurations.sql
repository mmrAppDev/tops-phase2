﻿CREATE TABLE [dbo].[FilterConfigurations]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [JobID] BIGINT NOT NULL, 
    [FilterType] INT NOT NULL DEFAULT ((0)), 
    [DisplayOrder] INT NOT NULL, 
    [Active] BIT NOT NULL DEFAULT ((1)), 
    [IsHighVolume] BIT NOT NULL DEFAULT ((0)), 
    [LastChanged] DATETIME NOT NULL DEFAULT (getdate()), 
    CONSTRAINT [FK_FilterConfigurations_JobInfo] FOREIGN KEY ([JobID]) REFERENCES [JobInfo]([ID]) ON DELETE CASCADE

)
