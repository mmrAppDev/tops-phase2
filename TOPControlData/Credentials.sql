﻿CREATE TABLE [dbo].[Credentials]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [UserID] BIGINT NOT NULL, 
    [Login] VARCHAR(50) NOT NULL DEFAULT (''), 
    [Password] NVARCHAR(MAX) NULL,
    [UseADAuthentication] BIT NOT NULL DEFAULT ((0)), 
	[IsApproved] BIT NOT NULL DEFAULT ((1)),
	[IsLockedOut] BIT NOT NULL DEFAULT ((0)),
	[CreationDate] DATETIME NOT NULL DEFAULT (getdate()),
	[LastLogin] DATETIME NOT NULL DEFAULT (getdate()),
	[PasswordQuestion] varchar(200) NOT NULL DEFAULT ('What is six times seven?'),
	[PasswordAnswer] varchar(50) NOT NULL DEFAULT ('42'),
    [LastChanged] DATETIME NOT NULL DEFAULT (getdate()), 
    CONSTRAINT [FK_Credentials_TOPUser] FOREIGN KEY ([UserID]) REFERENCES [TOPUser]([ID]) ON DELETE CASCADE
)
