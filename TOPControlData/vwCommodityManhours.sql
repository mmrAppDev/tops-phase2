﻿CREATE VIEW [dbo].[vwCommodityManhours]
	AS 
	SELECT C.[ID]
		, C.[JobID]
		, C.[CommodityCode]
		, C.[Description]
		, C.[ManHoursEstimated]
		, COUNT(TP.[ID]) As [CommodityPartCount]
		, IIF(COUNT(TP.[ID]) > 0, (C.[ManHoursEstimated] / COUNT(TP.[ID])), 0) As [ManHoursPerItem]
	FROM [Commodity] C
		INNER JOIN [TaskProgress] TP ON TP.[CommodityID] = C.[ID]
	GROUP BY C.[ID], C.[JobID], C.[CommodityCode], C.[Description], C.[ManHoursEstimated]


