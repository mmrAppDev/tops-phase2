﻿CREATE TABLE [dbo].[TOPUser]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [Login] VARCHAR(50) NOT NULL DEFAULT '', 
    [FirstName] VARCHAR(50) NOT NULL DEFAULT '', 
    [LastName] VARCHAR(50) NOT NULL DEFAULT '', 
	[Preferences] xml, 
	[DefaultJobID] BIGINT, 
    [LastChanged] DATETIME NOT NULL DEFAULT GetDate(), 
	[IsDeleted] BIT NOT NULL DEFAULT 0,
    CONSTRAINT [CK_TOPUser_Login] CHECK (LEN(Login) > 1)
)

GO

CREATE UNIQUE INDEX [IX_TOPUser_Login] ON [dbo].[TOPUser] ([Login])
