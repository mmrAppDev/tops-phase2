﻿
CREATE Procedure spRemoveDeletedJobData
	@JobID bigint
As
BEGIN

	If EXISTS (Select * from JobInfo where ID = @JobID And IsDeleted = 1)
	BEGIN
		BEGIN TRAN
		Delete from UserJob where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' UserJob items';

		Delete from ProgressHistories Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' ProgressHistory items';
		Delete from HistoryChartData Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' HistoryChartData items';

		Delete from TaskProgress Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' TaskProgress items';
		Delete from Tasks Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' Tasks items';
		Delete from ProjectPhase Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' ProjectPhase items';
		Delete from Commodity Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' Commodity items';

		Delete from TrackedItem Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' TrackedItem items';
		Delete from CraftInfo Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' CraftInfo items';
		Delete from ProjectSystem Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' ProjectSystem items';
		Delete from ProjectArea Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' ProjectArea items';
		Delete from ObjectType Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' ObjectType items';
		Delete from WorkCrew Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' WorkCrew items';
		Delete from HistoryChartData Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' HistoryChartData items';
		Delete from GroupedCompletionCharts Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' GroupedCompletionCharts items';
		Delete from ProgressLogEntry Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' ProgressLogEntry items';
		Delete from FilterConfigurations Where JobID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' FilterConfigurations items';

		Delete from JobInfo Where ID = @JobID
		PRINT 'Deleted ' + CONVERT(varchar(10), @@ROWCOUNT) + ' JobInfo items';

		COMMIT TRAN
	END
END
--Delete from jobinfo Where ID = 9

--Update JobInfo Set
--	Title = 'DELETED JOB'
--Where ID = 9