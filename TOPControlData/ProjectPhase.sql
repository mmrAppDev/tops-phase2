﻿CREATE TABLE [dbo].[ProjectPhase]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [JobID] BIGINT NOT NULL, 
    [Title] VARCHAR(50) NOT NULL DEFAULT '', 
    [DisplayOrder] INT NOT NULL DEFAULT 1, 
    [LastChanged] DATETIME NOT NULL DEFAULT GetDate(), 
    CONSTRAINT [FK_ProjectPhase_JobInfo] FOREIGN KEY ([JobID]) REFERENCES [JobInfo]([ID])
)
