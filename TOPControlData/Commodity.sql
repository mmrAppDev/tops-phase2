﻿CREATE TABLE [dbo].[Commodity]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [JobID] BIGINT NOT NULL, 
    [CommodityCode] VARCHAR(200) NOT NULL DEFAULT (''), 
    [Description] VARCHAR(200) NOT NULL DEFAULT (''), 
    [ManHoursEstimated] DECIMAL(16, 2) NOT NULL DEFAULT ((0)), 
    [LastChanged] DATETIME NOT NULL DEFAULT (getdate()), 
    CONSTRAINT [FK_Commodity_JobInfo] FOREIGN KEY ([JobID]) REFERENCES [JobInfo]([ID])
)
