﻿CREATE PROCEDURE [dbo].[spNeededProgressHistoryUpdates]
AS

Exec spRemoveAccidentalEntries;

Select PLE.JobID, PLE.WorkDate, Count(PLE.ID) As RowsNeeded
from ProgressHistories PH
	Right Outer Join ProgressLogEntry PLE
		On PLE.JobID = PH.JobID
			AND PLE.TaskProgressID = PH.TaskProgressID
			AND PLE.WorkDate = PH.WorkDate
	Inner Join TaskProgress TP
		On TP.ID = PLE.TaskProgressID
Where PLE.WorkDate is not NULL
	And TP.IsDeleted = 0
Group by PLE.JobID, PLE.WorkDate
Having Count(PH.ID) < Count(distinct PLE.TaskProgressID)
UNION
select PLE.JobID, PLE.WorkDate, Count(PLE.ID) AS RowsNeeded
from ProgressHistories PH
	Inner Join ProgressLogEntry PLE
		On PLE.JobID = PH.JobID
			AND PLE.TaskProgressID = PH.TaskProgressID
			AND PLE.WorkDate = PH.WorkDate
			AND PLE.LastChanged > PH.LastChanged
	Inner Join TaskProgress TP
		On TP.ID = PLE.TaskProgressID
Where TP.IsDeleted = 0
Group by PLE.JobID, PLE.WorkDate
Having Count(PH.ID) > 0
Order by RowsNeeded DESC