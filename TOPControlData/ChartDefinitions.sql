﻿CREATE TABLE [dbo].[ChartDefinitions]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
	[ChartTypeValue] INT NOT NULL DEFAULT ((0)), 
    [IsGlobal] BIT NOT NULL DEFAULT ((1)),
	[FilterValueTypeValue] INT NOT NULL DEFAULT ((0)), 
    [LastChanged] DATETIME NOT NULL DEFAULT (getdate())
)

GO

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Defines if this chart type is available to all jobs.',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'ChartDefinitions',
    @level2type = N'COLUMN',
    @level2name = N'IsGlobal'
GO
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'An Integer Value representing  a member of the ChartType enumeration.',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'ChartDefinitions',
    @level2type = N'COLUMN',
    @level2name = N'ChartTypeValue'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'An integer value representing a member of the FilterValueType enumeration.',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'ChartDefinitions',
    @level2type = N'COLUMN',
    @level2name = N'FilterValueTypeValue'