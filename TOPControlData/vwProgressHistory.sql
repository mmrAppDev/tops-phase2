﻿CREATE VIEW [dbo].[vwProgressHistory]
	AS 
SELECT TaskProgress.JobID, 
	TaskProgress.ID As TaskProgressID, 
	vwReceivingDates.WorkDate, 
	IsNull(Sum(Case When PLE.WorkDate <= vwReceivingDates.WorkDate 
			Then PLE.NewPercentComplete - PLE.OldPercentComplete 
			Else 0 End)
			, 0) As PCTD,
	ISNULL(Max(PLE.TaskManHours), 0) AS ManHours,
	IsNull(Max(Case When PLE.WorkDate = vwReceivingDates.WorkDate Then PLE.LastChanged Else Null End), Max(TaskProgress.LastChanged)) As LastChanged
from TaskProgress
	Inner Join vwReceivingDates
		On vwReceivingDates.JobID = TaskProgress.JobID
	Left Outer Join ProgressLogEntry PLE
		On PLE.JobID = TaskProgress.JobID 
			And PLE.TaskProgressID = TaskProgress.ID
Group By TaskProgress.JobID, TaskProgress.ID, vwReceivingDates.WorkDate


