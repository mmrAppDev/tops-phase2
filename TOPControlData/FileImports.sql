﻿CREATE TABLE [dbo].[FileImports]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY,
	[DateSubmitted] DATETIME NOT NULL DEFAULT (getdate()),
	[JobID] BIGINT NOT NULL, 
	[UserID] BIGINT NOT NULL DEFAULT ((1)),
    [Filepath] VARCHAR(500) NOT NULL, 
    [Filename] VARCHAR(250) NOT NULL, 
    [FileType] VARCHAR(50) NOT NULL DEFAULT ('Excel'), 
    [RecordsetName] VARCHAR(100) NOT NULL, 
    [RecordsetXml] XML NOT NULL, 
    [LastChanged] DATETIME NOT NULL DEFAULT (getdate()), 
    CONSTRAINT [FK_FileImport_JobInfo] FOREIGN KEY ([JobID]) REFERENCES [JobInfo]([ID]) ON DELETE CASCADE
)
