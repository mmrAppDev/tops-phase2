﻿CREATE FUNCTION [dbo].[GetProjectSystemTree]
(
	@jobID as int
) 

RETURNS TABLE AS 
RETURN 
( 
	WITH ProjectSystemCTE (ID, JobID, SystemNumber, Description, LastChanged, ParentId, SortOrder)
	AS
	(
		-- anchor member definition
		SELECT ID, JobID, SystemNumber, [Description], LastChanged, ParentId, SortOrder
		FROM ProjectSystem
		WHERE ParentId IS NULL AND JobId = @jobID

		UNION ALL

		-- recursive member definition
		SELECT p.ID, p.JobID, p.SystemNumber, p.Description, p.LastChanged, p.ParentId, p.SortOrder
		FROM ProjectSystem AS p
		INNER JOIN ProjectSystemCTE cte ON cte.ID = p.ParentId
		WHERE p.JobId = @jobID
	)
	SELECT * FROM ProjectSystemCTE
)