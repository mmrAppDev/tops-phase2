﻿CREATE VIEW [dbo].[vwAccidentialProgressEntries]
	AS 
SELECT PLEUp.ID As FirstID, PLEDown.ID As SecondID, PLEUp.NewPercentComplete As AmountUp, PLEUp.WorkDate, PLEDown.EntryDate - PLEUp.EntryDate As TimeSpan, PLEUp.EntryUsername As UpUser, PLEDown.EntryUsername As DownName
FROM ProgressLogEntry PLEUp 
	Inner Join ProgressLogEntry PLEDown
		On PLEUp.TrackedItemID = PLEDown.TrackedItemID
		and PLEUp.WorkDate = PLEDown.WorkDate
		and PLEUp.WorkDate Is Not NULL
		and PLEUp.OldPercentComplete = 0 
		and PLEUp.NewPercentComplete > 0
		and PLEDown.OldPercentComplete = PLEUp.NewPercentComplete
		and PLEDown.NewPercentComplete = PLEUp.OldPercentComplete
		and PLEDown.UserID = PLEUp.UserID
		and PLEDown.WorkCrewID = PLEUp.WorkCrewID
		and PLEDown.EntryDate > PLEUp.EntryDate
		and PLEDown.EntryDate < DateAdd(minute, 2, PLEUp.EntryDate)
WHERE
	-- Identify items in which there are no other log entries in between. 
	(select count(PLEBetween.ID) as BetweenItems
		from ProgressLogEntry PLEBetween 
		where PLEBetween.TrackedItemID = PLEUp.TrackedItemID
			and PLEBetween.WorkDate = PLEUp.WorkDate
			and PLEBetween.EntryDate between PLEUp.EntryDate and PLEDown.EntryDate
			and PLEBetween.ID <> PLEUp.ID
			and PLEBetween.ID <> PLEDown.ID
			) = 0

