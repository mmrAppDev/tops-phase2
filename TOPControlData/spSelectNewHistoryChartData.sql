﻿CREATE PROCEDURE [dbo].[spSelectNewHistoryChartData]
	@WorkDate datetime = NULL,
	@JobID bigint = NULL,
	@PhaseID bigint = NULL,
	@TaskID bigint = NULL,
	@TrackedItemID bigint = NULL,
	@SystemID bigint = NULL, 
	@CommodityID bigint = NULL,
	@AreaID bigint = NULL,
	@ObjectTypeID bigint = NULL,
	@CraftID bigint = NULL,
	@UDPValue1 varchar(200) = NULL,
	@UDPValue2 varchar(200) = NULL,
	@UDPValue3 varchar(200) = NULL
AS
	Select PH.WorkDate, IIF(HistoricalScopeCurve = 1, SUM(PH.ManHours) , SUM(TP.ManHoursPerItem)) As TotalManHours, 
	IIF(HistoricalScopeCurve = 1, SUM(PH.PCTD * PH.ManHours), SUM(PH.PCTD * TP.ManHoursPerItem)) As CompletedManHours,
	IIF(JOB.WeightedCalculations = 1 And IIF(HistoricalScopeCurve = 1, SUM(PH.ManHours) , SUM(TP.ManHoursPerItem)) > 0,
			IIF(HistoricalScopeCurve = 1, SUM(PH.PCTD * PH.ManHours) , SUM(PH.PCTD * TP.ManHoursPerItem)) / IIF(HistoricalScopeCurve = 1, SUM(PH.ManHours) , SUM(TP.ManHoursPerItem)), 
			AVG(PCTD)) As [PercentComplete]
from 
	ProgressHistories PH
		Inner Join vwTaskProgressReporting TP
			ON TP.ID = PH.TaskProgressID
		Inner Join Tasks TA
			ON TA.ID = TP.TaskID
		Inner Join ProjectPhase PP
			ON PP.ID = TA.PhaseID
		Inner Join JobInfo JOB
			ON JOB.ID = PP.JobID
		Inner Join TrackedItem TI
			ON TI.ID = TP.TrackedItemID
Where 
	NOT PH.WorkDate IS NULL
	And (@WorkDate IS NULL Or PH.WorkDate = @WorkDate)
	And (@JobID IS NULL Or PH.JobID = @JobID)
	And (@PhaseID IS NULL Or TA.PhaseID = @PhaseID)
	And (@TaskID IS NULL Or TP.TaskID = @TaskID)
	And (@TrackedItemID IS NULL Or TP.TrackedItemID = @TrackedItemID)
	And (@SystemID IS NULL Or TI.SystemID = @SystemID) 
	And (@CommodityID IS NULL Or TP.CommodityID = @CommodityID)
	And (@AreaID IS NULL Or TI.AreaID = @AreaID)
	And (@ObjectTypeID IS NULL Or TI.ObjectTypeID = @ObjectTypeID)
	And (@CraftID IS NULL Or TI.CraftID = @CraftID)
	And (@UDPValue1 IS NULL Or TI.UDPValue1 = @UDPValue1)
	And (@UDPValue2 IS NULL Or TI.UDPValue2 = @UDPValue2)
	And (@UDPValue3 IS NULL Or TI.UDPValue3 = @UDPValue3)
	And TP.IsInDeletedScope = 0
Group By PH.WorkDate, JOB.WeightedCalculations, JOB.HistoricalScopeCurve
Order by PH.WorkDate
