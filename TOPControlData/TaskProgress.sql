﻿CREATE TABLE [dbo].[TaskProgress] (
    [ID]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [JobID]           BIGINT         NOT NULL,
    [CommodityID]     BIGINT         NULL,
    [TaskID]          BIGINT         NOT NULL,
    [TrackedItemID]   BIGINT         NOT NULL,
    [PercentComplete] DECIMAL (9, 2) DEFAULT ((0)) NOT NULL,
    [LastChanged]     DATETIME       DEFAULT (getdate()) NOT NULL,
    [IsDeleted]       BIT            DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_TaskProgress_Commodity] FOREIGN KEY ([CommodityID]) REFERENCES [dbo].[Commodity] ([ID]) ON DELETE SET NULL,
    CONSTRAINT [FK_TaskProgress_JobInfo] FOREIGN KEY ([JobID]) REFERENCES [dbo].[JobInfo] ([ID]),
    CONSTRAINT [FK_TaskProgress_Task] FOREIGN KEY ([TaskID]) REFERENCES [dbo].[Tasks] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_TaskProgress_TrackedItem] FOREIGN KEY ([TrackedItemID]) REFERENCES [dbo].[TrackedItem] ([ID])
);



GO

CREATE UNIQUE INDEX [IX_TaskProgress_JobID_TaskID_TrackedItemID] ON [dbo].[TaskProgress] ([JobID], [TaskID], [TrackedItemID])

GO

CREATE UNIQUE INDEX [IX_TaskProgress_JobID_ID_TrackedItemID] ON [dbo].[TaskProgress] ([JobID], [ID], [TrackedItemID])

GO
CREATE NONCLUSTERED INDEX [IX_TaskProgress_Job_IsDeleted]
    ON [dbo].[TaskProgress]([JobID] ASC, [IsDeleted] ASC)
    INCLUDE([TaskID], [TrackedItemID]);

