﻿CREATE TABLE [dbo].[ProgressHistories] (
    [ID]             BIGINT          IDENTITY (1, 1) NOT NULL,
    [JobID]          BIGINT          NOT NULL,
    [TaskProgressID] BIGINT          NOT NULL,
    [WorkDate]       DATETIME        NOT NULL,
    [PCTD]           DECIMAL (18, 2) DEFAULT ((0)) NOT NULL,
    [ManHours]       DECIMAL (9, 4)  DEFAULT ((0)) NOT NULL,
    [LastChanged]    DATETIME        NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);



GO
CREATE NONCLUSTERED INDEX [IX_ProgressHistories_Job_TaskProgress_WorkDate_LastChanged]
    ON [dbo].[ProgressHistories]([JobID] ASC, [TaskProgressID] ASC, [WorkDate] ASC, [LastChanged] ASC);

