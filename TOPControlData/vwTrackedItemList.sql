﻿CREATE VIEW [dbo].[vwTrackedItemList]
	AS 
Select JI.Title as JobTitle, 
	TI.*, 
	PS.SystemNumber, 
	PA.AreaNumber,
	CI.CraftCode,
	JI.UDPName1 As Custom1Name,
	TI.UDPValue1 As Custom1Value,
	JI.UDPName2 As Custom2Name,
	TI.UDPValue2 As Custom2Value,
	JI.UDPName3 As Custom3Name,
	TI.UDPValue3 As Custom3Value
From TrackedItem TI
	Inner Join JobInfo JI
		on TI.JobID = JI.ID
	Left Outer Join ProjectSystem PS
		on TI.SystemID = PS.ID
	Left Outer Join ProjectArea PA
		on TI.AreaID = PA.ID
	Left Outer Join CraftInfo CI
		on TI.CraftID = CI.ID
