﻿CREATE TABLE [dbo].[ObjectType]
(
	[ID] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [JobID] BIGINT NOT NULL, 
	[Name] VARCHAR(200) NOT NULL DEFAULT (''), 
    [Description] VARCHAR(200) NOT NULL DEFAULT (''), 
    [LastChanged] DATETIME NOT NULL DEFAULT (getdate()), 
    CONSTRAINT [FK_ObjectType_JobInfo] FOREIGN KEY ([JobID]) REFERENCES [JobInfo]([ID]) ON DELETE CASCADE
     
)

GO

CREATE UNIQUE INDEX [IX_ObjectType_JobID_Name] ON [dbo].[ObjectType] ([JobID], [Name])
