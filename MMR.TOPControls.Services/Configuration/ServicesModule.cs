using Autofac;
using Envoc.Core.Shared.Service;

namespace MMR.TOPControls.Services.Configuration
{
    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(ThisAssembly).AsSelf();

            builder.RegisterGeneric(typeof(SimpleService<>));
        }
    }
}
