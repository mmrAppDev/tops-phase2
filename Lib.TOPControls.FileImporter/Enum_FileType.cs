﻿
using System.Runtime.Serialization;
namespace Lib.TOPControls.FileImporter
{
    [DataContract]
    public enum FileFormat
    {
        [EnumMember]
        Unknown,

        [EnumMember]
        Excel,
        
        [EnumMember]
        CSV

    }

}