﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Lib.TOPControls.FileImporter.Version1
{
    [ServiceContract(Namespace="http://apps.mmrgrp.com/TOPControls")]
    public interface IFileImporterService1
    {
        [OperationContract]
        FileAnalysisResult ExamineFile(string filename, string schemaName);

        [OperationContract]
        ImportedRecordset ReadImportTable(string filename, string schemaName, string recordsetName, long userID, long jobID);

        [OperationContract]
        void ExportJobData(long jobID, string filepath, string sourceTemplateFilepath);
    
    }

    
}
