﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Lib.TOPControls.FileImporter.Version1
{
    [DataContract]
    public class RecordsetSummary
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int Position { get; set; }

        [DataMember]
        public IEnumerable<ColumnDef> Columns { get; set; }

        [DataMember]
        public int SchemaPoints { get; set; }

        [DataMember]
        public int RowCount { get; set; }

        [DataMember]
        public IEnumerable<string> MissingRequiredColumns { get; set; }

    }
}
