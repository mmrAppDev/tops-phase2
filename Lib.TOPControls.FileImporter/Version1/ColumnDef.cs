﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Lib.TOPControls.FileImporter.Version1
{
    [DataContract]
    public class ColumnDef
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Datatype { get; set; }
    }
}
