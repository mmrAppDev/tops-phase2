﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Lib.TOPControls;
using Lib.TOPControls.DataAccess;
using Lib.TOPControls.DataImport;
using Lib.TOPControls.Exceptions;

namespace Lib.TOPControls.FileImporter.Version1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FileImporterService1" in both code and config file together.
    public class FileImporterService1 : IFileImporterService1
    {
        private Lazy<ITOPControlLibrary> _root = new Lazy<ITOPControlLibrary>(() => getRoot());

        public FileImporterService1()
        {
        }

        public FileImporterService1(ITOPControlLibrary useLibrary)
        {
            _root = new Lazy<ITOPControlLibrary>(() => useLibrary);
        }

        public ITOPControlLibrary Root
        {
            get { return _root.Value; }
        }

        #region IFileImporterService1 Members

        public FileAnalysisResult ExamineFile(string filename, string schemaName)
        {
            var result = new FileAnalysisResult();
            if (!File.Exists(filename)) throw new FileImportException(string.Format("Could not find a file with the name {0}.", filename));

            result.Filename = filename;
            result.DateAnalyzed = DateTime.Now;
            result.Format = determineFormatByName(filename);
            if (isValidSchemaName(schemaName))
                result.SchemaName = schemaName;
            else
                result.SchemaName = string.Empty;

            if (result.Format == FileFormat.Excel)
            {
                result.CandidateRecordsets = GetExcelCandidateRecordsets(filename, schemaName);
            }

            return result;
        }

        public ImportedRecordset ReadImportTable(string filename, string schemaName, string recordsetName, long userID, long jobID)
        {
            var result = new ImportedRecordset();
            if (!File.Exists(filename)) throw new FileImportException(string.Format("Could not find a file with the name {0}.", filename));
            if (!isValidSchemaName(schemaName)) throw new FileImportException("Invalid schema name.");
            
            
            
            try
            {
                // build a FileImport item to store with the final recordset.
                var subject = new FileImport();
                subject.JobID = jobID;
                subject.UserID = userID;
                subject.DateSubmitted = DateTime.Now;
                subject.Filepath = filename;
                subject.Filename = Path.GetFileName(filename);
                subject.FileType = determineFormatByName(filename).ToString();

                if (subject.FileType == FileFormat.Excel.ToString())
                {
                    var excelDS = getExcelData(filename, recordsetName);

                    if (excelDS != null)
                    {
                        removeBlankRows(excelDS);

                        result.RecordsetName = recordsetName;
                        result.RowCount = excelDS.Tables[0].Rows.Count;
                        result.Columns = convertToColumnList(excelDS.Tables[0].Columns);

                        subject.RecordsetName = recordsetName;
                        subject.StoreAsXml(excelDS);

                        var vr = Root.FileImportActions.Validate(subject);
                        if (vr.IsValid)
                        {
                            Root.FileImportActions.Add(subject, userID);
                        }
                        else
                        {
                            throw new FileImportException("A serious failure occurred while importing the file.", vr);
                        }

                        if (subject.ID != default(long))
                        {
                            result.RecordsetID = subject.ID;
                        }
                    }
                }
                else
                {
                    throw new FileImportException("File type not supported.");
                }
            }
            catch (Exception ex)
            {
                throw new FileImportException("There was a problem importing the selected data.", ex);
            }
            return result;
            
        }

        public void ExportJobData(long jobID, string filepath, string sourceTemplateFilepath)
        {

            var dc = new TOPControlDataContext();
            using (var con = new SqlConnection(dc.Database.Connection.ConnectionString))
            {
                using (var cmd = new SqlCommand("[dbo].[spGetExportData]", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@JobID", SqlDbType.BigInt).Value = jobID;
                    var da = new SqlDataAdapter(cmd);
                    var dt = new DataTable();
                    da.Fill(dt);

                    var dictUDP = Root.JobActions.GetUDPList(jobID);

                    for (int idx = 1; idx < 4; idx++)
                    {
                        correctUDPColumn(idx, dt, dictUDP);
                    }

                    File.Copy(sourceTemplateFilepath, filepath);

                    var sheetnames = getSheetNames(filepath);

                    writeExcelData(filepath, sheetnames[0], dt); 

                }
            }
            



        }

        private void correctUDPColumn(int index, DataTable dt, Dictionary<int, string> dictUDP)
        {
            var col = findUDPColumnByIndex(index, dt.Columns);
            if (dictUDP.ContainsKey(index) && !string.IsNullOrWhiteSpace(dictUDP[index]))
            {
                if (col != null)
                {
                    col.ColumnName = dictUDP[index];
                }
                else
                {
                    col = dt.Columns.Add(dictUDP[index], typeof(string), "");
                }
            }
            else
            {
                if (col != null)
                {
                    dt.Columns.Remove(col);
                }
            }
        }

        private DataColumn findUDPColumnByIndex(int index, DataColumnCollection all)
        {
            var colName = string.Format("Custom{0}Value", index);
            if (all.Contains(colName))
            {
                return all[colName];
            }
            return null;
        }

        private void removeBlankRows(DataSet excelDS)
        {
            var dt = excelDS.Tables[0];

            var idx = 0;
            while (idx < dt.Rows.Count)
            {
                var dr = dt.Rows[idx];
                bool hasValue = false;
                foreach (var item in dr.ItemArray)
                {
                    if (item != null && item != DBNull.Value)
                    {
                        hasValue = true;
                        break;
                    }
                }

                if (!hasValue)
                {
                    dt.Rows.Remove(dr);
                }
                else
                {
                    idx++;
                }
            }
        }

        #endregion

        private FileFormat determineFormatByName(string filename)
        {
            var ext = Path.GetExtension(filename);

            if (ext == ".csv") 
                return FileFormat.CSV;
            else if (ext==".xlsx")
                return FileFormat.Excel;
            return FileFormat.Unknown;
        }

        private bool isValidSchemaName(string schemaName)
        {
            return (schemaName == "TaskImport");
        }

        private List<string> getSheetNames(string filename)
        {
            var result = new List<string>();
            var sheetTable =  getExcelSheets(filename);

            foreach (DataRow row in sheetTable.Rows)
            {
                result.Add(row["TABLE_NAME"].ToString());
            }

            return result;
        }


        private static string getExcelConnectionString(string filename)
        {
            var connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0}; Extended Properties=Excel 8.0;", filename);
            return connectionString;
        }

        private DataTable getExcelSheets(string filename)
        {
            var connectionString = getExcelConnectionString(filename);
            var conn = new OleDbConnection(connectionString);
            DataTable result = null;

            try
            {
                conn.Open();

                result = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                
            }
            catch (Exception ex)
            {
                throw new FileImportException("Error retrieving Excel sheet data.", ex);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        private DataSet getExcelData(string filename, string sheetName = "")
        {
            var connectionString = getExcelConnectionString(filename);
            var conn = new OleDbConnection(connectionString);
            var commandText = string.Format("SELECT * FROM [{0}]", sheetName);            
            var adapter = new OleDbDataAdapter(commandText, connectionString);
            var ds = new DataSet("ImportData");

            adapter.Fill(ds, "Imported");

            return ds;
        }

        private void writeExcelData(string filename, string sheetName, DataTable dt)
        {
            var connectionString = getExcelConnectionString(filename);

            // NOTICE:  Add a line here to change the table name if the default name isn't useful.
            dt.TableName = sheetName;

            using (var conn = new OleDbConnection(connectionString))
            {
                using (var cmd = new OleDbCommand(GetOleDbTableCreationString(dt), conn))
                {
                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }

            var insCmd = GetOleDbValueInsertParameterizedCommand(dt);
            
            using (var con2 = new OleDbConnection(connectionString))
            {
                using (var cmd2 = new OleDbCommand(insCmd, con2))
                {
                    AddOleDbParameters(cmd2, dt);
                    try
                    {
                        con2.Open();
                        foreach (DataRow dr in dt.Rows)
                        {
                            SetOleDbParameters(cmd2, dr);
                            cmd2.ExecuteNonQuery();
                        }
                    }
                    catch { throw; }
                    finally { con2.Close(); }
                }
            }
        }

        public static string GetOleDbTableCreationString(DataTable dt)
        {
            var result = new StringBuilder("");

            result.AppendFormat("CREATE TABLE [{0}](", dt.TableName);
            bool isFirstItem = true;
            foreach (DataColumn col in dt.Columns)
            {
                var strDType = resolveOleDbExcelType(col.DataType);
                if (isFirstItem)
                {
                    isFirstItem = false;
                }
                else
                {
                    result.Append(", ");
                }
                result.AppendFormat("[{0}] {1}", col.ColumnName, strDType);
            }
            result.Append(")");
            
            return result.ToString();
        }

        public static string GetOleDbTableDeletionString(string TableName)
        {
            var result = new StringBuilder("");

            result.AppendFormat("DROP TABLE [{0}]", TableName);

            return result.ToString();
        }


        public static string GetOleDbValueInsertParameterizedCommand(DataTable dt)
        {
            var result = new StringBuilder("");

            result.AppendFormat("Insert into [{0}] Values (", dt.TableName);
            bool isFirstItem = true;
            foreach (DataColumn col in dt.Columns)
            {
                if (isFirstItem) { isFirstItem = false; }
                else { result.Append(", "); }

                result.AppendFormat("@Col{0}", col.Ordinal);
            }
            result.Append(")");

            return result.ToString();
        }

        public static void AddOleDbParameters(OleDbCommand cmd, DataTable dt)
        {
            foreach (DataColumn col in dt.Columns)
            {
                var pname = string.Format("@Col{0}", col.Ordinal);
                if (!cmd.Parameters.Contains(pname))
                {
                    cmd.Parameters.Add(pname, resolveOleDbTypeEnum(col.DataType));
                }
            }
        }

        public static void SetOleDbParameters(OleDbCommand cmd, DataRow drow)
        {
            for (int idx = 0; idx < cmd.Parameters.Count; idx++ )
            {
                var pname = string.Format("@Col{0}", idx);
                cmd.Parameters[pname].Value = drow[idx];
            }
        }

        private static string resolveOleDbExcelType(Type type)
        {
            if (type.Name == "Decimal"
                || type.Name == "Double")
            { return "double"; }
            else
            { return "varchar(255)"; }
        }

        private static OleDbType resolveOleDbTypeEnum(Type type)
        {
            if (type.Name == "Decimal"
                || type.Name == "Double")
            { return OleDbType.Double; }
            else
            { return OleDbType.VarChar; }
        }

        private IEnumerable<string> getMissingRequiredColumns(IEnumerable<ColumnDef> cols, string schemaName)
        {
            var result = new List<string>();

            if (schemaName == "TaskImport")
            {
                result.Add("Phase");
                result.Add("Task");
                result.Add("Tracking ID");
            }

            foreach (var column in cols)
            {
                if (result.Contains(column.Name))
                {
                    result.Remove(column.Name);
                }
            }

            return result;

        }

        private int calculateSchemaPoints(IEnumerable<ColumnDef> cols, string schemaName)
        {
            var result = 0;
            var schemaCols = new List<string>();

            if (schemaName == "TaskImport")
            {
                schemaCols.Add("Phase");
                schemaCols.Add("Task");
                schemaCols.Add("Tracking ID");
                schemaCols.Add("System");
                schemaCols.Add("Area");
                schemaCols.Add("Craft");
                schemaCols.Add("Commodity");
                schemaCols.Add("Object Type");

            }

            foreach (var column in cols)
            {
                if (schemaCols.Contains(column.Name))
                {
                    result++;
                }
            }

            return result;


        }

        private IEnumerable<RecordsetSummary> GetExcelCandidateRecordsets(string filename, string schemaName)
        {
            var result = new List<RecordsetSummary>();

            var names = getSheetNames(filename);
            int position = 0;
            foreach (var sname in names)
            {
                var recordSetTable = getExcelData(filename, sname).Tables["Imported"];
                IEnumerable<ColumnDef> cols = convertToColumnList(recordSetTable.Columns);
                var missing = getMissingRequiredColumns(cols, schemaName);
                var points = calculateSchemaPoints(cols, schemaName);
                result.Add(new RecordsetSummary
                {
                    Name = sname,
                    Position = position,
                    Columns = cols,
                    RowCount = recordSetTable.Rows.Count,
                    MissingRequiredColumns = missing,
                    SchemaPoints = points
                });
                position++;
            }

            return result;
        }

        private IEnumerable<ColumnDef> convertToColumnList(DataColumnCollection dataColumnCollection)
        {
            var result = new List<ColumnDef>();

            foreach (DataColumn dc in dataColumnCollection)
            {
                result.Add(new ColumnDef { Name = dc.ColumnName, Datatype = dc.DataType.Name });
            }

            return result;
        }


        private static ITOPControlLibrary getRoot()
        {
            return new TOPControlLibrary(new TOPControlSQLDAL(new TOPControlDataContext()));
        }

    }
}
