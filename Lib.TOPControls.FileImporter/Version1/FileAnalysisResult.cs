﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Lib.TOPControls.FileImporter.Version1
{
    [DataContract]
    public class FileAnalysisResult
    {
        [DataMember]
        public string Filename { get; set; }

        [DataMember]
        public FileFormat Format { get; set; }

        [DataMember]
        public DateTime DateAnalyzed { get; set; }

        [DataMember]
        public string SchemaName { get; set; }

        [DataMember]
        public IEnumerable<RecordsetSummary> CandidateRecordsets { get; set; }


    }
}
