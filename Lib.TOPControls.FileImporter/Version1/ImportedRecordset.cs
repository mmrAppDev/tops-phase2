﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Lib.TOPControls.FileImporter.Version1
{
    [DataContract]
    public class ImportedRecordset
    {
        [DataMember]
        public string RecordsetName { get; set; }

        [DataMember]
        public long RecordsetID { get; set; }

        [DataMember]
        public int RowCount { get; set; }

        [DataMember]
        public IEnumerable<ColumnDef> Columns { get; set; }

        
    }
}
